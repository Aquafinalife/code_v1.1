<div class="page_header">
    <h1 class="fleft">Cơ sơ y tế</h1>
    <small class="fleft">"Thêm/sửa Cơ sơ y tế"</small>
    <span class="fright"><a href="<?php echo PRODUCTS_STYLE_ADMIN_BASE_URL; ?>" class="button close" style="padding:5px;" title="Đóng"><em></em><span>Đóng</span></a></span>
    <span class="fright"><a class="button add" href="<?php echo PRODUCTS_STYLE_ADMIN_ADD_URL; ?>"><em>&nbsp;</em>Thêm Cơ sơ y tế</a></span>
    <br class="clear"/>
</div>
<?php
echo form_open('', array('id' => 'submit_form'), array('id' => 0, 'from_list' => TRUE));
echo isset($uri) ? form_hidden('uri', $uri) : NULL;
echo form_hidden('back_url', PRODUCTS_STYLE_ADMIN_BASE_URL);
echo form_close();
?>
<div class="form_content">
    <table class="list" style="width: 100%;margin: 10px 0;">
        <?php $this->load->view('powercms/message'); ?>
        <tr>
            <th class="left">ID</th>
            <th class="left">CƠ SỞ Y TẾ</th>
            <th class="left">ĐỊA CHỈ</th>
            <th class="left">ĐIỆN THOẠI</th>
            <th class="left">EMAIL</th>
            <th class="left">LIÊN HỆ</th>
            <th class="center" style="width: 5%">CHỨC NĂNG</th>
        </tr>
        <?php
        if (isset($csyt)) {
            $stt = 0;
            foreach ($csyt as $index):
                $style = $stt++ % 2 == 0 ? 'even' : 'odd';
                $id= $index->id;
                $name = $index->name;
                $username = $index->username;
                $email = $index->email;
                $phone = $index->phone;
                $address = $index->address;
                $contact = $index->contact;
                ?>
                <tr class="<?php echo $style; ?>">
                    <td class="left"><?php echo 'CSYT-'.$id; ?></td>
                    <td class="left"><?php echo $name; ?></td>
                    <td class="left"><?php echo $address; ?></td>
                    <td class="left"><?php echo $phone; ?></td>
                    <td class="left"><?php echo $email; ?></td>
                    <td class="left"><?php echo $contact; ?></td>
                    <td class="center">
                        <a class="edit" title="Sửa" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id; ?>, '<?php echo PRODUCTS_STYLE_ADMIN_EDIT_URL; ?>', 'edit');"><em>&nbsp;</em></a>
                        <a class="del" title="Xóa" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id; ?>, '<?php echo PRODUCTS_STYLE_ADMIN_DELETE_URL; ?>', 'delete');"><em>&nbsp;</em></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php } ?>
    </table>
    <br class="clear"/>&nbsp;
</div>