<?php

class Homepage extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->kh_check_login();

        $this->_layout = 'layout/custom_layout';
        $this->_view_data = array();
    }

    function index() {
        //load cache

        $this->load->view($this->_layout, $view_data, FALSE);
    }

}

?>
