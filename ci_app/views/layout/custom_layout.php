<?php $this->load->view('common/custom_header'); ?>

<header class="site-header">
    <?php $this->load->view('common/custom_header_sliderbar'); ?>    
</header>
<div class="mobile-menu-left-overlay"></div>
<nav class="side-menu">
    <?php $this->load->view('common/custom_sliderbar'); ?>    
</nav>

<div class="page-content">
    <div class="container-fluid">
        <?php
        if (isset($main_content)) {
            echo $main_content;
        }
        ?>
    </div><!--.container-fluid-->
</div><!--.page-content-->
<?php $this->load->view('common/custom_footer'); ?>