<div class="box">
    <p style="text-align: center; margin-bottom: 30px; margin-top: 30px;">
        <img src="/images/process-cart.png" border="0" style="max-width: 100%;" />
    </p>
    <h1 class="title"><?php echo $title; ?></h1>
    <div class="cart">
        <?php $this->load->view('common/message'); ?>
        <?php
        $lang = get_language();
        if ($this->cart->total_items() != 0):
            ?>
            <div class="table-responsive">
                <table class="table table-bordered" style="border:none;">
                    <thead style="background-color: #de4c4a;color: white;">
                        <tr style="height: 40px;">
                            <th style="width: 5%;text-align: center;vertical-align: middle;"><?php echo __('IP_cart_image'); ?></th>
                            <th style="width: 50%;text-align: center;vertical-align: middle;"><?php echo __('IP_products'); ?></th>
                            <th style="width: 15%;text-align: center;vertical-align: middle;"><?php echo __('IP_cart_price'); ?></th>
                            <th style="width: 15%;text-align: center;vertical-align: middle;"><?php echo __('IP_quantum'); ?></th>
                            <th style="width: 15%;text-align: center;vertical-align: middle;"><?php echo __('IP_price_total'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $carts = $this->cart->contents();
                        foreach ($carts as $cart):
                            if(SLUG_ACTIVE==0){
                                $uri = get_base_url() . url_title($cart['name'], 'dash', TRUE) . '-ps' . $cart['id'];
                            }else{
                                $uri = get_base_url() . $cart['slug'];
                            }
                            ?>
                            <tr>
                                <td>
                                    <a href="<?php echo $uri; ?>" title="<?php echo $cart['name']; ?>">
                                        <img src="/images/products/smalls/<?php echo $cart['images']; ?>" title="<?php echo $cart['name']; ?>" height="70px" />
                                    </a>
                                </td>
                                <td style="vertical-align: middle;">
                                    <a href="<?php echo $uri; ?>" title="<?php echo $cart['name']; ?>" style="color: #000;"><?php echo $cart['name']; ?><?php if(!empty($cart['size'])){ ?> <br />- Size: <b><?php echo $cart['size']; ?></b><?php } ?></a>
                                </td>
                                <td style="vertical-align: middle;text-align: center;">
                                    <span class="cart_price"><?php echo get_price_in_vnd($cart['price']) . ' VND'; ?></span>
                                </td>
                                <td style="vertical-align: middle;text-align: center;">
                                    <input style="text-align: center" type="text" name="<?php echo $cart['rowid']; ?>" maxlength="2" size="2" value="<?php echo $cart['qty']; ?>" onchange="update_cart('<?php echo $cart['rowid']; ?>', '<?php echo get_uri_by_lang($lang, 'cart'); ?>');">
                                    <a rel="nofollow" href="javascript:void(0);" onclick="remove_cart('<?php echo $cart['rowid']; ?>', '<?php echo get_uri_by_lang($lang, 'cart'); ?>', '<?php echo $lang; ?>');" title="<?php echo __('IP_cart_shopping_delete'); ?>">
                                        <img src="/images/delete-cart.gif" alt="Delete" class="icon">
                                    </a>
                                </td>
                                <td style="vertical-align: middle;text-align: center;">
                                    <span class="cart_price"><?php echo get_price_in_vnd($cart['subtotal']) . ' VND'; ?></span>
                                </td>
                            </tr>
    <?php endforeach; ?>
                    </tbody>
                    <tfoot style="background-color: rgb(245, 245, 245);">
                        <tr>
                            <td colspan="4" style="text-align: right;<?php if(empty($total_discount) || $total_discount == 0){ ?>text-transform: uppercase;font-weight: bold;<?php } ?>">
                                <?php echo __('IP_total'); ?>:
                            </td>
                            <td class="total_price"><?php echo get_price_in_vnd($this->cart->total()) . ' VND'; ?> </td>
                        </tr>
                        
                        <?php if(!empty($total_discount) && $total_discount > 0){ 
                            $total = $this->cart->total();
                            $discount = $total - $total_discount;
                            ?>
                        <tr>
                            <td colspan="4" style="text-align: right;">
                                Giảm giá <?php if(!empty($coupon_code)){ ?>(mã: <?php echo $coupon_code; ?>)<?php } ?>:
                            </td>
                            <td class="total_price"><?php echo '-'.get_price_in_vnd($discount) . ' VND'; ?> </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: right;text-transform: uppercase;font-weight: bold;">
                                Thành tiền:
                            </td>
                            <td class="total_price"><?php echo get_price_in_vnd($total_discount) . ' VND'; ?> </td>
                        </tr>
                        <?php } ?>
                        
                        <?php if ($lang == 'vi') { ?>
                        <?php if(!empty($total_discount) && $total_discount > 0){ ?>
                        <tr>
                            <td colspan="5" style="text-align: right;" class="total_price"><?php echo DocTienBangChu($total_discount); ?> </td>
                        </tr>
                        <?php }else{ ?>
                        <tr>
                            <td colspan="5" style="text-align: right;" class="total_price"><?php echo DocTienBangChu($this->cart->total()); ?> </td>
                        </tr>
                        <?php } ?>
                        <?php } ?>
                    </tfoot>
                </table>
            </div>
        
            <div class="coupon">
                <?php echo form_open('/gio-hang'); ?>
                    <input type="text" name="coupon_code" class="form-control" placeholder="Nhập mã giảm giá" />
                    <input type="submit" name="" class="btn" value="Áp dụng mã" />
                <?php echo form_close(); ?>
            </div>
        
        <?php else: ?>
            <?php $this->load->view('common/message'); ?>
            <h4 class="alert alert-warning"><?php echo __('IP_cart_shopping_empty'); ?></h4>
<?php endif; ?>
    </div>

    <div class="pull-right" style="padding-bottom: 20px;">
        <a href="/" class="btn btn-primary" title="Thêm sản phẩm">Thêm sản phẩm</a>
        <?php if ($this->cart->total_items() != 0){ ?>
        <a href="/thong-tin-khach-hang" class="btn btn-success" title="Thanh toán">Thanh toán</a>
        <?php } ?>
    </div>
    <div class="clearfix"></div>

</div>
