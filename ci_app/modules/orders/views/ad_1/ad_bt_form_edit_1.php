<?php
echo form_open($submit_uri);
$fullname = isset($fullname) ? $fullname : '';
$email = isset($email) ? $email : '';
$created_date = isset($created_date) ? $created_date : '';
$submit_uri = isset($submit_uri) ? $submit_uri : '';
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'orders_cat');
?>

<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Nội dung bồi thường"</small>
    <span class="fright"><a class="button close" href="<?php echo ORDER_ADMIN_BASE_URL; ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>

<?php
$a_loai_benh_THAISAN_QLNT = get_a_loai_benh_THAISAN_QLNT();
$a_loai_benh_THAISAN_MDL = get_a_loai_benh_THAISAN_MDL();
$a_loai_benh_DTNT_OB = get_a_loai_benh_DTNT_OB();
$a_loai_benh_DTNGT_OB = get_a_loai_benh_DTNGT_OB();
$a_loai_benh_NHAKHOA_MDL = get_a_loai_benh_NHAKHOA_MDL();

$a_status_bt = get_a_orders_status();
$a_loai_benh_sn = get_a_loai_benh_sn();
$a_loai_benh_slk = get_a_loai_benh_slk();
$a_status_bt1 = get_a_status_bt1();
$disabled = 'disabled = true';
$disabled_bt = $disabled;
$a_ct_selected = array();


if (isset($bt) && is_object($bt)) {
    $id = $bt->id;
    $chungtu_bosung = $bt->chungtu_bosung;
    $chungtu_bosung_txt = $bt->chungtu_bosung_txt;
    $phanloai_bt = $bt->phanloai_bt;
    $phanloai_bt_txt = get_phanloai_bt($phanloai_bt);
    $so_tien_ycbt = $bt->so_tien_ycbt;
    $so_tien_dbt = $bt->so_tien_dbt;
    $so_ngay_ycbt = $bt->so_ngay_ycbt;
    $ngay_ycbt = substr($bt->ngay_ycbt, 0, 10);
    $message = $bt->message;
    $so_lan_kham = $bt->so_lan_kham;
    $loai_benh = $bt->loai_benh;
    $loai_benh_txt = get_ten_qlbt($loai_benh);
    $status = $bt->order_status;
    $status_txt = get_orders_status($status);
    if ($status == BT_CHODUYET) {
        $disabled_bt = '';
    }
    $chungtu_bosung = $bt->chungtu_bosung;
    if ($chungtu_bosung != '') {
        $a_ct_selected = explode(',', $chungtu_bosung);
    }
    $chungtu_bosung_txt = $bt->chungtu_bosung_txt;
    // Check loại bệnh tổng
    if (in_array($loai_benh, $a_loai_benh_THAISAN_QLNT) || in_array($loai_benh, $a_loai_benh_DTNT_OB)) {
        $loai_benh_t_txt = 'Giới hạn bảo hiểm trong Quyền Lợi Nội Trú';
    }
    if (in_array($loai_benh, $a_loai_benh_DTNGT_OB)) {
        $loai_benh_t_txt = 'Giới hạn bảo hiểm trong Quyền Lợi Ngoại Trú';
    }
}
echo form_hidden('bt_id', $id);
echo form_hidden('id', $id);
$loai_benh_txt = isset($loai_benh_txt) ? $loai_benh_txt : '';
//end
//$slk_bh = $sn_bh = 'Error';
if (isset($bh) && is_object($bh)) {
    $kh_dbh = $bh->kh_dbh;
    $congty_canhan = $bh->congty_canhan;
    $mst_cmt = $bh->mst_cmt;
    $goi_bh = $bh->goi_bh;
    $goi_bh_dm = $bh->goi_bh_dm;
    //end
    // Nếu đã chọn ql bh
//    if ($loai_benh != '') {
//        $st_bh_bt = $bh->$loai_benh;
//        $slk_bh_field = get_loai_benh_slk_field($loai_benh);
//        if ($slk_bh_field != '') {
//            $slk_bh = $goi_bh->$slk_bh_field;
//        }
//        $sn_bh_field = get_loai_benh_sn_field($loai_benh);
//        if ($sn_bh_field != '') {
//            $sn_bh = $goi_bh->$sn_bh_field;
//        }
//    }
    //end
    $st_cl_tkbt = '';
    $st_bt_gh_tt = isset($st_bt_gh_tt) ? $st_bt_gh_tt : 0;
    $st_bt_gh_t = isset($st_bt_gh_t) ? $st_bt_gh_t : 0;
    $st_bh = isset($st_bh) ? $st_bh : 0;
    $st_dbt = isset($st_dbt) ? $st_dbt : 0;
    $so_tien_ycbt = isset($so_tien_ycbt) ? $so_tien_ycbt : 0;
    $slk_bh = isset($slk_bh) ? $slk_bh : 'Empty';
    $slk_dbt = isset($slk_dbt) ? $slk_dbt : 0;
    $sn_bh = isset($sn_bh) ? $sn_bh : 'Empty';
    $sn_dbt = isset($sn_dbt) ? $sn_dbt : 0;
    $sn_bh_cl = $sn_bh - $sn_dbt;
    $sn_bt_cl = (int) $sn_bh - (int) $sn_dbt;
    $st_cl_tbt = (int) $st_bh - (int) $st_dbt;
    $st_cl_sbt = (int) $st_bh - (int) $st_dbt - (int) $so_tien_ycbt;

    //echo form_hidden('kh_dbh', $kh_dbh);
    ?>
    <div class="form_content">
        <?php $this->load->view('powercms/message'); ?>
        <ul class="tabs">
            <li><a href="#tab1">Thông tin người được bảo hiểm</a></li>
            <!--<li><a href="#tab2">Quyền lợi bảo hiểm tai nạn</a></li>-->
            <!--<li><a href="#tab3">Tình trạng bồi thường</a></li>-->
        </ul>
        <div class="tab_container">
            <div id="tab1" class="tab_content">
                <div class="col-sm-6 col-xs-12">
                    <table>
                        <tr style="display: none;"><td class="title">Ngôn ngữ: </td></tr>
                        <tr style="display: none;">                            
                        </tr>
                        <tr>
                            <td class="title fl w250">Người được bồi thường:</td>
                            <td class="fl">
                                <?php echo $congty_canhan ?>
                                <input class="select_kh_dbh_bt" type="hidden" name="kh_dbh" value="<?php echo $kh_dbh ?>"/>
    <!--                                <select name="kh_dbh" class="hide select_kh_dbh_bt">
                                    <option selected="" value="<?php echo $kh_dbh ?>"><?php echo $congty_canhan ?></option>
                                </select>-->
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">CMT/Hộ chiếu:</td>
                            <td class="fl"><?php echo $mst_cmt ?></td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Loại bồi thường:</td>
                            <td class="fl">
                                <?php echo $phanloai_bt_txt ?>
                                <select name="phanloai_bt" class="hide select_loai_bt">
                                    <option selected="" value="<?php echo $phanloai_bt ?>"><?php echo $phanloai_bt_txt ?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Quyền lợi bồi thường:</td>
                            <td class="fl">
                                <?php echo $loai_benh_txt ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Gói bảo hiểm mua:</td>
                            <td class="fl">
                                <?php echo $goi_bh_dm ?>
                                <input type="hidden" name="goi_bh" value="<?php echo $goi_bh ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Tình trạng hồ sơ:</td>
                            <td class="fl">
                                <span class="btn btn-warning btn-sm"><?php echo $status_txt ?></span>
                                <?php
                                if ($status != BT_DATRATIEN) {
                                    ?>
        <!--                                    <select name="order_status" class="js-example-disabled-results">
                                            <option value="">-- Tình trạng bồi thường --</option>
                                    <?php
                                    if (isset($a_status_bt) && !empty($a_status_bt)) {
                                        foreach ($a_status_bt as $k => $v) {
                                            $selected = ($k == $status) ? 'selected="selected"' : '';
                                            ?>
                                                            <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                        </select>-->
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Ngày yêu cầu bồi thường:</td>
                            <td class="fl">
                                <input <?php // echo $disabled_bt                                                           ?> class="show_date" type="text" name="ngay_ycbt" value="<?php echo $ngay_ycbt ?>"/>
                                <?php // echo form_input(array('name' => 'ngay_ycbt', 'size' => '50', 'class' => 'show_date', 'maxlength' => '255', 'style' => 'width:_560px;', 'value' => isset($ngay_ycbt) ? $ngay_ycbt : set_value('ngay_ycbt')));   ?>
                            </td>
                        </tr>
                        <?php
                        if ($status == BT_CHOBOSUNG) {
                            ?>
                            <tr>
                                <td class="title fl w250">Chứng từ chờ bổ sung:</td>
                                <td class="fl">
                                    <div class="box_chungtu_bt_list col-sm-12 mb15">
                                        <h3 class="main_header">Chứng từ hồ sơ bồi thường</h3>
                                        <?php
                                        if (isset($ct) && !empty($ct)) {
                                            $i = 1;
                                            foreach ($ct as $index) {
                                                $id = $index->id;
                                                $name = $index->name;
                                                if (in_array($id, $a_ct_selected)) {
                                                    $checked = 'checked';
                                                } else {
                                                    $checked = '';
                                                }
                                                ?>
                                                <div class="col-sm-11 col-xs-10">
                                                    <ul class="list_hsbt_bs">
                                                        <li>
                                                            <?php echo $i ?>. <?php echo $name ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-1 col-xs-2">
                                                    <ul class="list_hsbt_bs">
                                                        <li>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input <?php echo $checked ?> class="checkbox_ct" name="ct_bt[]" type="checkbox" value="<?php echo $id ?>">
                                                                </label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="clear-both clearfix"></div>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="btn btn-warning btn_check_ct">Kiểm tra chứng từ</div> <i>(Click kiểm tra chứng từ trước khi lưu dữ liệu.)</i>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr><td class="title" style="vertical-align: top">Ghi chú chứng từ chờ bổ sung:</td></tr>
                        <tr>
                            <td>
                                <?php echo form_textarea(array('id' => 'chungtu_bosung_txt', 'name' => 'chungtu_bosung_txt', 'style' => 'width:560px; height: 100px;', 'value' => isset($chungtu_bosung_txt) ? $chungtu_bosung_txt : set_value('chungtu_bosung_txt'))); ?>
                            </td>
                        </tr>

                    </table>
                </div>
                <div class="clear-both"></div>
                <div class="col-sm-12">
                    <div class="form-group box_quyenloi_bt" style="display: _none">
                        <?php
                        if (isset($bh_html)) {
                            echo $bh_html;
                        }
                        ?>
                    </div>
                    <?php
                    $cl = ($status == BT_CHOBOSUNG) ? 'display: none' : 'display: none';
                    $so_tien_ycbt_fm = ($status == BT_THANHTOAN || $status == BT_DATRATIEN) ? 'readonly="readonly"' : '';
                    $so_tien_dbt_fm = ($status == BT_THANHTOAN || $status == BT_DATRATIEN) ? 'readonly="readonly"' : '';
                    $so_ngay_ycbt_fm = ($status == BT_THANHTOAN || $status == BT_DATRATIEN) ? 'readonly="readonly"' : '';
                    ?>
                    <div class="box_content_bt1" style="<?php echo $cl  ?>">
                        <div class="form-group">
                            <div class="col-sm-3 col-xs-12">
                                <label for="exampleInputEmail1">Số tiền yêu cầu bồi thường</label>
                            </div>
                            <div class="col-sm-5 col-xs-12">
                                <?php // echo form_input(array('name' => 'so_tien_ycbt', 'readonly' => 'readonly', 'size' => '50', 'class' => 'tien_bt', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($so_tien_ycbt) ? $so_tien_ycbt : set_value('so_tien_ycbt'))); ?>
                                <input type="text" min="0" <?php echo $so_tien_ycbt_fm ?> value="<?php echo $so_tien_ycbt ?>" name="so_tien_ycbt" class="_form-control tien_bt" id="exampleInputEmail1" placeholder="Số tiền yêu cầu bồi thường">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        if (in_array($loai_benh, $a_loai_benh_sn)) {
                            $so_tien_dbt_af = (int) $so_tien_dbt / (int) $so_ngay_ycbt;
                        } else {
                            $so_tien_dbt_af = $so_tien_dbt;
                        }
                        ?>
                        <div class="form-group mt10">
                            <div class="col-sm-3 col-xs-12">
                                <label for="exampleInputEmail1">Số ngày yêu cầu bồi thường</label>
                            </div>
                            <div class="col-sm-5 col-xs-12">
                                <?php // echo form_input(array('name' => 'so_ngay_ycbt', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($so_ngay_ycbt) ? $so_ngay_ycbt : set_value('so_ngay_ycbt'))); ?>
                                <input type="text" min="0" <?php echo $so_ngay_ycbt_fm ?> value="<?php echo $so_ngay_ycbt ?>" name="so_ngay_ycbt" class="form-control" id="exampleInputEmail1" placeholder="">
                                <i style="font-size: 13px;">(Nếu quyền lợi không có số ngày, ghi là 0)</i>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!--                        <div class="form-group">
                                                    <label for="exampleInputEmail1">Số lần khám</label>
                        <?php echo form_input(array('name' => 'so_lan_kham', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($so_lan_kham) ? $so_lan_kham : set_value('so_lan_kham'))); ?>
                                                    <i style="font-size: 13px;">(Chỉ điền khi quyền lợi có số lần khám)</i>
                                                </div>-->
                        <div class="form-group mt10">
                            <div class="col-sm-3 col-xs-12">
                                <label for="exampleInputEmail1">Ghi chú</label>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <?php echo form_textarea(array('id' => 'message', 'name' => 'message', 'value' => ($message != '') ? $message : set_value('message'))); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--            <div id="tab3" class="tab_content">
                            <table>
                                <tr><td class="title">Tình trạng bồi thường: </td></tr>
                                <tr>
                                    <td id="category">
                                        <select name="orders_status" class="form-control">
            <?php
            if (isset($a_status_bt) && !empty($a_status_bt)) {
                foreach ($a_status_bt as $k => $v) {
//                                        $selected= ($status== $k)?'selected=""';
                    ?>
                                                                                                                                                                                                            <option value="<?php echo $k ?>"><?php echo $v ?></option>
                    <?php
                }
            }
            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr><td class="title">Xác nhận thay đổi số tiền còn lại: </td></tr>
                                <tr><td><i style="font-size: 13px;">(Chỉ được tích khi xác nhận chính xác)</i></td></tr>
                                <tr>
                                    <td id="category"><label> <input type="checkbox" name="xac_nhan" value="1"> Xác nhận </label></td>
                                </tr>
                            </table>
                        </div>-->
        </div>
        <br class="clear"/>
        <div style="margin-top: 10px;"></div>
        <?php
        if ($status != BT_DATRATIEN) {
            echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn'));
        }
        ?>
        <a style="margin-left: 20px;" class="btn btn-primary" href="<?php echo site_url(ORDER_ADMIN_BASE_URL) ?>">Quay lại <span class="glyphicon glyphicon-share-alt"></span></a>
        <br class="clear"/>&nbsp;
    </div>
    <?php
}
?>
<?php echo form_close(); ?>
<div class="modal fade bs-example-modal-lg modal_ct_bt" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">

</div>