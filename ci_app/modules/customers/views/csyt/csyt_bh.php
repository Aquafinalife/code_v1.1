<?php
$G_cmt_key = isset($_GET['cmt_key']) ? $_GET['cmt_key'] : '';
?>
<div class="box_filter" style="margin-bottom: 20px">
    <form action="<?php echo site_url(URL_CSYT_BH) ?>" method="get">
        <div class="col-sm-3 col-xs-12">
            <input placeholder="CMT/Hộ chiếu..." style="" class="form-control" type="text" name="cmt_key" value="<?php echo $G_cmt_key ?>"/>
        </div>
        <div class="col-sm-4">
            <input style="margin-top: 7px;margin-left: 20px;" class="btn btn btn-inline btn-sm" type="submit" value="Tìm kiếm"/>
            <a href="#"  style="margin-top: -1px;margin-left: 10px;" class="btn btn btn-success btn-sm">Yêu cầu bảo lãnh</a>
        </div>
    </form>
</div>

<?php
if (isset($bh) && !empty($bh)) {
    foreach ($bh as $index) {
        $id = $index->id;
        $kh_mbh = $index->kh_mbh;
        $congty_canhan = $index->congty_canhan;
        $mst_cmt = $index->mst_cmt;
        $ngaysinh = substr($index->ngaysinh, 0,10);
        $email = $index->email;
        $didong = $index->didong;
        $parent_id = $index->parent_id;
        $phongban = $index->phongban;
        $ngay_hieuluc = $index->ngay_hieuluc;
        $ngay_ketthuc = $index->ngay_ketthuc;
        $goi_bh_dm = $index->goi_bh_dm;
        $so_ngay_ycbt = $index->so_ngay_ycbt;
        $ten_dn_bbh = $index->ten_dn_bbh;
        $TV_TTTBVV_TN = $index->TV_TTTBVV_TN;
        $TCL_NN = $index->TCL_NN;
        $tcl_nn_sn= $index->tcl_nn_sn;
        $CPYT_TN = $index->CPYT_TN;
        $TV_TTTBVV_OB = $index->TV_TTTBVV_OB;
        $DTNT_OB = $index->DTNT_OB;
        $dtnt_dbh = $index->dtnt_dbh;
        $dtnt_tvpn = $index->dtnt_tvpn;
        $dtnt_tvpn_sn = $index->dtnt_tvpn_sn;
        $dtnt_tgpn = $index->dtnt_tgpn;
        $dtnt_tgpn_sn = $index->dtnt_tgpn_sn;
        $dtnt_cppt = $index->dtnt_cppt;
        $dtnt_tk_nv = $index->dtnt_tk_nv;
        $dtnt_sk_nv = $index->dtnt_sk_nv;
        $dtnt_cpyt_tn = $index->dtnt_cpyt_tn;
        $dtnt_cpyt_tn_sn = $index->dtnt_cpyt_tn_sn;
        $dtnt_tcvp = $index->dtnt_tcvp;
        $dtnt_tcvp_sn = $index->dtnt_tcvp_sn;
        $dtnt_tcmt = $index->dtnt_tcmt;
        $dtnt_xct = $index->dtnt_xct;
        $THAISAN_QLNT = $index->THAISAN_QLNT;
        $tsqlnt_ktdk = $index->tsqlnt_ktdk;
        $tsqlnt_st = $index->tsqlnt_st;
        $tsqlnt_sm = $index->tsqlnt_sm;
        $tsqlnt_dn = $index->tsqlnt_dn;
        $DTNGT_OB = $index->DTNGT_OB;
        $dtngt_dbh = $index->dtngt_dbh;
        $dtngt_st1lk = $index->dtngt_st1lk;
        $dtngt_slk = $index->dtngt_slk;
        $dtngt_nk = $index->dtngt_nk;
        $dtngt_cvr = $index->dtngt_cvr;
        $THAISAN_MDL = $index->THAISAN_MDL;
        $tsmdl_dbh = $index->tsmdl_dbh;
        $tsmdl_ktdk = $index->tsmdl_ktdk;
        $tsmdl_st = $index->tsmdl_st;
        $tsmdl_sm = $index->tsmdl_sm;
        $tsmdl_dn = $index->tsmdl_dn;
        $NHAKHOA_MDL = $index->NHAKHOA_MDL;
        $nkmdl_dbh = $index->nkmdl_dbh;
        $nkmdl_cb = $index->nkmdl_cb;
        $nkmdl_db = $index->nkmdl_db;
//        echo '<pre>';
//        print_r($bh);
//        die;
        ?>
        <br><br><br>
        <div class="clearfix"></div>
        <table id="_table-edit" class="table table-bordered table-hover">
            <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
                <header class="box-typical-header panel-heading ui-sortable-handle">
                    <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Thông tin bảo hiểm:</h3>
                </header>
            </section>
            <thead>
                <tr>
                    <th>Thông tin</th>
                    <th>Giá trị</th>
                    <th>Thông tin</th>
                    <th>Giá trị</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Họ và tên </td>
                    <td><?php echo $congty_canhan ?></td>
                    <td>Gói BH </td>
                    <td><?php echo $goi_bh_dm ?></td>
                </tr>
                <tr>
                    <td>CMT/Hộ chiếu </td>
                    <td><?php echo $mst_cmt ?></td>
                    <td>Công ty BH </td>
                    <td><?php echo $ten_dn_bbh ?></td>
                </tr>
                <tr>
                    <td>Ngày sinh </td>
                    <td><?php echo $ngaysinh ?></td>
                    <td>Ngày hiệu lực</td>
                    <td><?php echo $ngay_hieuluc ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Ngày hết hạn</td>
                    <td><?php echo $ngay_ketthuc ?></td>
                </tr>
            </tbody>
        </table>
        <br><br><br>
        <br><br>
        <table id="_table-edit" class="table table-bordered table-hover">
            <thead class="thead1">
                <tr>
                    <th class="text-danger">Quyền lợi bảo hiểm</th>
                    <th class="text-success">Hạn mức BH (STBH)</th>
                    <!--<th class="text-info">Số tiền đã BT</th>-->
                    <!--<th class="text-warning">Số tiền BH còn lại</th>-->
                    <!--<th class="text-danger">Ghi chú</th>-->
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1. Tử vong, TTTBVV (Do tai nạn)</br> Thương tật bộ phận </td>
                    <td><?php echo get_price_in_vnd($TV_TTTBVV_TN) ?> </br> Theo tỷ lệ</td>
                    <!--<td>-</td>-->
                    <!--<td>-</td>-->
                    <!--<td></td>-->
                </tr>
                <tr>
                    <td>2. Trợ cấp lương ngày nghỉ <br> Số tiền <br> Số ngày </td>
                    <td><?php echo get_price_in_vnd($TCL_NN) ?> <br> <?php echo $tcl_nn_sn ?></td>
<!--                    <td>-</td>
                    <td>-</td>
                    <td></td>-->
                </tr>
                <tr>
                    <td>3. Chi phí y tế do tai nạn </td>
                    <td><?php echo get_price_in_vnd($CPYT_TN) ?></td>
<!--                    <td>-</td>
                    <td>-</td>
                    <td></td>-->
                </tr>
                <tr>
                    <td>4. Tử vong, TTTBVV (Do ốm bệnh) <br> Theo tỷ lệ</td>
                    <td><?php echo get_price_in_vnd($TV_TTTBVV_OB) ?></td>
<!--                    <td>-</td>
                    <td>-</td>
                    <td></td>-->
                </tr>
                <tr>
                    <td>
                        5. Điều trị nội trú (Do ốm bệnh) 
<!--                        <br> 5.1 Tiền viện phí ngày <br> - Số ngày nằm viện
                        <br> 5.2 Tiền giường, phòng / ngày <br> - Số ngày
                        <br> 5.3 Chi phí phẫu thuật
                        <br> 5.4 Quyền lợi khám trước khi nhập viện
                        <br> 5.5 Quyền lợi khám sau khi nhập viện
                        <br> 5.6 Chi phí y tá tại nhà
                        <br> 5.7 Trợ cấp viện phí <br> - Số ngày trợ cấp
                        <br> 5.8 Trợ cấp mai táng
                        <br> 5.9 Xe cứu thương
                        <br> 5.10 THAI SẢN (Trong quyền lợi nội trú):
                        <br> - Chi phí khám thai định kỳ
                        <br> - Sinh thường
                        <br> - Sinh mổ
                        <br> - Dưỡng nhi-->
                    </td>
                    <td>
                        <?php echo get_price_in_vnd($DTNT_OB) ?>
<!--                        <br> <?php echo get_price_in_vnd($dtnt_tvpn) ?>
                        <br> <?php echo ($dtnt_tvpn_sn) ?>
                        <br> <?php echo get_price_in_vnd($dtnt_tgpn) ?>
                        <br> <?php echo ($dtnt_tgpn_sn) ?>
                        <br> <?php echo get_price_in_vnd($dtnt_cppt) ?>
                        <br> <?php echo get_price_in_vnd($dtnt_tk_nv) ?>
                        <br> <?php echo get_price_in_vnd($dtnt_sk_nv) ?>
                        <br> <?php echo get_price_in_vnd($CPYT_TN) ?>
                        <br> <?php echo get_price_in_vnd($dtnt_tcvp) ?>
                        <br> <?php echo ($dtnt_tcvp_sn) ?>
                        <br> <?php echo get_price_in_vnd($dtnt_tcmt) ?>
                        <br> <?php echo get_price_in_vnd($dtnt_xct) ?>
                        <br> <?php echo get_price_in_vnd($THAISAN_QLNT) ?>
                        <br> <?php echo get_price_in_vnd($tsqlnt_ktdk) ?>
                        <br> <?php echo get_price_in_vnd($tsqlnt_st) ?>
                        <br> <?php echo get_price_in_vnd($tsqlnt_sm) ?>
                        <br> <?php echo get_price_in_vnd($tsqlnt_dn) ?>
                        <br>-->
                    </td>
<!--                    <td>
                        --
                        <br> --
                        <br> --
                        <br> --
                        <br> --
                        <br> --
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    </td>
                    <td>
                        --
                        <br> --
                        <br> --
                        <br> --
                        <br> --
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    </td>
                    <td>-</td>-->
                </tr>
                <tr>
                    <td>
                        6. ĐIỀU TRỊ NGOẠI TRÚ (Do ốm bệnh) 
<!--                        <br> 6.1 Số tiền cho 1 lần khám
                        <br> 6.2 Số lần khám
                        <br> 6.3 Nha khoa (trong OP)
                        <br> 6.4 Cạo vôi răng-->
                    </td>
                    <td>
                        <?php echo get_price_in_vnd($DTNGT_OB) ?>
<!--                        <br> <?php echo get_price_in_vnd($dtngt_st1lk) ?>
                        <br> <?php echo ($dtngt_slk) ?>
                        <br> <?php echo get_price_in_vnd($dtngt_nk) ?>
                        <br> <?php echo get_price_in_vnd($dtngt_cvr) ?>
                        <br>-->
                    </td>
<!--                    <td>
                        --
                        <br> --
                        <br> --
                        <br>
                        <br> 
                        <br> 
                    </td>
                    <td>
                        --
                        <br> --
                        <br> --
                        <br>
                        <br> 
                        <br>
                    </td>
                    <td></td>-->
                </tr>
                <tr>
                    <td>
                        7. THAI SẢN (Mua độc lập)
<!--                        <br> 7.1 Chi phí khám thai định kỳ
                        <br> 7.2 Sinh thường
                        <br> 7.3 Sinh mổ
                        <br> 7.4 Dưỡng nhi-->
                    </td>
                    <td>
                        <?php echo get_price_in_vnd($THAISAN_MDL) ?>
<!--                        <br> <?php echo get_price_in_vnd($tsmdl_ktdk) ?>
                        <br> <?php echo get_price_in_vnd($tsmdl_st) ?>
                        <br> <?php echo get_price_in_vnd($tsmdl_sm) ?>
                        <br> <?php echo get_price_in_vnd($tsmdl_dn) ?>
                        <br>-->
                    </td>
<!--                    <td></td>
                    <td></td>
                    <td></td>-->
                </tr>
                <tr>
                    <td>
                        8. NHA KHOA (Mua độc lập)
<!--                        <br> 8.1 Nha khoa cơ bản
                        <br> 8.2 Nha khoa đặc biệt-->
                    </td>
                    <td>
                        <?php echo get_price_in_vnd($NHAKHOA_MDL) ?>
<!--                        <br> <?php echo get_price_in_vnd($nkmdl_cb) ?>
                        <br> <?php echo get_price_in_vnd($nkmdl_db) ?>
                        <br>-->
                    </td>
<!--                    <td></td>
                    <td></td>-->
                </tr>
            </tbody>
        </table>
        <?php
    }
} else {
    ?>
    <br><br><br>
    <div class="clearfix"></div>
    <table id="_table-edit" class="table table-bordered table-hover">
        <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
            <header class="box-typical-header panel-heading ui-sortable-handle">
                <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Thông tin bảo hiểm:</h3>
            </header>
        </section>
        <tbody>
            <tr>
                <td colspan="10" class="text-danger">Không có dữ liệu.</td>
            </tr>
        </tbody>
    </table>
    <?php
}
?>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>