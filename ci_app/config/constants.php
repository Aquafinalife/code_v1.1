<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Mặc định của PowerCMS */
define('DEFAULT_ADMIN_KEYWORDS',      'PowerCMS');
define('DEFAULT_ADMIN_DESCRIPTION',   'Hệ quản trị thông tin PowerCMS do công ty ASIAN FINANCE cung cấp');
define('DEFAULT_ADMIN_TITLE',         'Quản trị hệ thống');
define('DEFAULT_ADMIN_TITLE_SUFFIX',  ' | PowerCMS');

define('DOMAIN_NAME', 'lamwebchuanseo.com');
define('DOMAIN_URL', 'http://lamwebchuanseo.com'); //not / at end

define('DEFAULT_TITLE_SUFFIX',  '');
define('DEFAULT_CACHE_PREFIX',  'ip_');

define('DEFAULT_FOOTER', 'Copyright © 2014 <a href="http://lamwebchuanseo.com" title="lamwebchuanseo.com">lamwebchuanseo.com</a>');

//default email nhan don dat hang
define('ORDER_EMAIL' , 'lamwebchuanseo@gmail.com');
define('EMAIL_NO_REPLY' , 'lamwebchuanseo@gmail.com');
//default email nhan tin nhan lien he
define('CONTACT_EMAIL' , 'lamwebchuanseo@gmail.com');

define('DEFAULT_COMPANY', 'exahoi');

//Status active
define('STATUS_ACTIVE', 1);
define('STATUS_INACTIVE', 0);
define('STATE_ACTIVE', 2);
define('STATE_INACTIVE', 1);

//url friendly active, 1:active (url khong id), 0:inactive (url co id)
define('SLUG_ACTIVE', 1);

//default language - cai dat cho da ngon ngu vi,en,cn,fr... (can sua lai ca routes)
define('DEFAULT_LANGUAGE','vi');

//ẩn danh sách menu admin trong danh sách menu (1:ẩn, 0:hiện)
define('HIDE_ADMIN_MENU', 0);

define('CITY_ID',        1);

// operations permission
define('OPERATION_MANAGE',      2);
define('OPERATION_NEWS_MANAGE_CAT',  20);

define('OPERATION_ADMIN',  21);


define('ROLE_GUESS'     ,        -1);
define('ROLE_ADMINISTRATOR',      0);
define('ROLE_MANAGER',            2);

define('DEFAULT_COMBO_VALUE',   -1);
define('DEFAULT_PAGE',          1);
define('DEFAULT_OFFSET',        0);
define('PAGINATION_NUM_LINKS',  2);
define('ROOT_CATEGORY_ID'     , 0);

define('ONE_BILLION',           1000000000);
define('ONE_MILLION',           1000000);
define('ONE_THOUSAND',          1000);
define('ONE_USD',               1);
define('UNIT_VND',              0);
define('UNIT_USD',              1);
define('SHOW_ZERO',             TRUE);
define('NOT_SHOW_ZERO',         FALSE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */

define('DB_PREFIX',		'itnl_');
//==============================================================================

define('LIMIT_DEFAULT',		'20');
//==============================================================================

define('BT_TUCHOI_TOANBO',		'100');
//==============================================================================

define('ACTIVE',		'1');
define('DEACTIVE',		'2');
//==============================================================================
define('CANHAN',		'1');
define('DOANHNGHIEP',		'2');
//==============================================================================
define('NOITRU',		'1');
define('NGOAITRU',		'2');
//==============================================================================
define('NMBH',		'1');
define('NDBH',		'2');
//==============================================================================
define('VIP',		'1');
define('THUONG',		'2');
//==============================================================================
define('TIEN_MAT',                   '1');
define('CHUYEN_KHOAN',		'2');
//==============================================================================
define('BANTHAN',		'1');
define('NGUOITHAN',		'2');
define('NHANVIEN',		'3');
//==============================================================================
define('NAM',		'1');
define('NU',		'2');
define('KHAC',		'3');
//==============================================================================
define('SINH_THUONG',		'1');
define('SINH_MO',		'2');

//==============================================================================
define('BH_CHUA_CO_HIEULUC',		'1');
define('BH_CO_HIEULUC',                 '2');
define('BH_HET_HIEULUC',		'3');
//==============================================================================
define('BT_PARENT',                                 '1');
define('BT_CASE',                                   '2');
//==============================================================================
define('BT_HOANTRA',                              '1');
define('BT_BAOLANH_VIENPHI',                      '2');
//==============================================================================
define('BT_CHUYENKHOAN',                                '1');
define('BT_TIENMAT',                                    '2');
//==============================================================================
define('NHS_TRUCTIEP',                                      '1');
define('NHS_ONSITE',                                        '2');
define('NHS_BUUDIEN',                                       '3');
//==============================================================================
define('DN_NV',                                 '1');
define('DN_NT',                                 '2');
define('DN_NT_BM',                                 '5');
define('DN_NT_CC',                                 '6');
define('DN_NT_VC',                                 '7');

define('CN_CN',                                 '3');
define('CN_NT',                                 '4');
//==============================================================================
define('BT_CHOBOSUNG',                              '10');
define('BT_CHODUYET',                               '1');
define('BT_DADUYET',                                '2');
define('BT_THANHTOAN',                              '3');
define('BT_DATRATIEN',                              '4');
define('BT_PHEDUYET',                               '5');
define('BT_DONG',                                   '6');
define('BT_HUY',                                    '7');
//------------------------------------------------------------------------------
define('BT_PHEDUYET_CANCEL',                               '20');
define('BT_THANHTOAN_CANCEL',                               '21');
//==============================================================================
define('BT_DTNT_OB',                         '1');
define('BT_DTNGT_OB',                       '2');
define('BT_THAISAN_QLNT',                        '3');
define('BT_THAISAN_MDL',                        '4');
define('BT_TVTTTBVV_TN',                        '5');
define('BT_TCL_NN',                        '6');
define('BT_CPYT_TN',                        '7');
define('BT_TVTTTBVV_OB',                        '8');
define('BT_NHAKHOA_MDL',                        '9');

//==============================================================================
define('DTNT_OB',                               'DTNT_OB');
define('dtnt_tvpn',                             'dtnt_tvpn');
define('dtnt_tvpn_sn',                          'dtnt_tvpn_sn');
define('dtnt_tgpn',                             'dtnt_tgpn');
define('dtnt_tgpn_sn',                          'dtnt_tgpn_sn');
define('dtnt_cppt',                             'dtnt_cppt');
define('dtnt_tk_nv',                            'dtnt_tk_nv');
define('dtnt_sk_nv',                            'dtnt_sk_nv');
define('dtnt_cpyt_tn',                          'dtnt_cpyt_tn');
define('dtnt_tcvp',                             'dtnt_tcvp');
define('dtnt_tcvp_sn',                          'dtnt_tcvp_sn');
define('dtnt_tcmt',                             'dtnt_tcmt');
define('dtnt_xct',                              'dtnt_xct');
define('THAISAN_QLNT',                          'THAISAN_QLNT');
define('tsqlnt_ktdk',                           'tsqlnt_ktdk');
define('tsqlnt_st',                             'tsqlnt_st');
define('tsqlnt_sm',                             'tsqlnt_sm');
define('tsqlnt_dn',                             'tsqlnt_dn');
define('DTNGT_OB',                              'DTNGT_OB');
define('dtngt_st1lk',                           'dtngt_st1lk');
define('dtngt_slk',                             'dtngt_slk');
define('dtngt_nkngt',                           'dtngt_nkngt');
define('dtngt_nk',                              'dtngt_nk');
define('dtngt_cvr',                             'dtngt_cvr');
define('THAISAN_MDL',                           'THAISAN_MDL');
define('tsmdl_ktdk',                            'tsmdl_ktdk');
define('tsmdl_st',                              'tsmdl_st');
define('tsmdl_sm',                              'tsmdl_sm');
define('tsmdl_dn',                              'tsmdl_dn');
define('TV_TTTBVV_TN',                          'TV_TTTBVV_TN');
define('tv_tttbvv_tn_tv',                       'tv_tttbvv_tn_tv');
define('tv_tttbvv_tn_tt',                       'tv_tttbvv_tn_tt');
define('TV_TTTBVV_OB',                          'TV_TTTBVV_OB');
define('tv_tttbvv_ob_tv',                       'tv_tttbvv_ob_tv');
define('tv_tttbvv_ob_tt',                       'tv_tttbvv_ob_tt');
define('TCL_NN',                                'TCL_NN');
define('tcl_nn_sn',                             'tcl_nn_sn');
define('CPYT_TN',                               'CPYT_TN');
define('NHAKHOA_MDL',                           'NHAKHOA_MDL');
define('nkmdl_db',                              'nkmdl_db');
define('nkmdl_cb',                              'nkmdl_cb');
define('nkmdl_cvr',                             'nkmdl_cvr');
//==============================================================================

define('QL_CHECKED',                             '2');
//==============================================================================

//define('DA_BOI_THUONG',		'1');
//define('CHO_DUYET',		'2');

define('URL_AD_KH',                                 'dashboard/kh');
//define('ad_url',                                    site_url('dashboard'));
define('URL_ADD_KH_CN',                             'dashboard/customers/add-kh-cn');
//define('URL_ADD_KH_CN',                             'customers/add-kh-cn');
define('URL_ADD_KH_DN',                             'dashboard/customers/add-kh-dn');


define('URL_DASHBOARD',                             'dashboard');
define('URL_KH_DANGNHAP',                           'kh_login');
define('URL_KH_LOGOUT',                             'kh_logout');

define('URL_BBH_DANGNHAP',                          'bbh');
define('URL_BBH_DASHBOARD',                         'bbh-dashboard');
define('URL_BBH_BH',                                'bbh-bh');
define('URL_BBH_BT',                                'bbh-bt');

define('URL_CSYT_DANGNHAP',                         'csyt');
define('URL_CSYT_DASHBOARD',                        'csyt-dashboard');
define('URL_CSYT_BH',                               'csyt-bh');
define('URL_CSYT_BT',                               'csyt-bt');

define('URL_KH_DASHBOARD',                          'quan-tri-khach-hang');


define('URL_BT_CN',                                 'boi-thuong-cn');
define('URL_BT_DN',                                 'boi-thuong-dn');

define('URL_BH_CN',                                 'bao-hiem-cn');
define('URL_BH_DN',                                 'bao-hiem-dn');

define('REPORTS_URL',                           'dashboard/reports');

define('TBL_PRODUCTS',                          'products');
define('TBL_PRODUCTS_SIZE',                     'products_size'); // table: gói BH
define('TBL_PRODUCTS_STYLE',                    'products_style'); // talbe: cơ sở y tế
define('TBL_CSYT',                              'products_style'); // talbe: cơ sở y tế
define('TBL_MABENH',                            'products_origin'); // table: mã bệnh
define('TBL_PHONGBAN',                          'products_color'); // table: phòng ban
define('TBL_CITIES',                            'products_state'); // table: tỉnh thành
define('TBL_DN_BBH',                            'products_trademark'); // table: dn bán bh
define('TBL_CHUNGTU',                           'products_coupon'); // table: dn bán bh
define('TBL_NGANHANG',                          'products_material'); // table: dn bán bh
define('TBL_CUSTOMERS',                         'customers');
define('TBL_ORDERS',                            'orders');
define('TBL_CL',                                'clbh');
define('TBL_QLBT',                              'quyenloi_bt');
define('TBL_USERS',                             'users');