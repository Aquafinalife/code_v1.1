<?php echo $this->load->view('common/header'); ?>
<body class="home option6">
<?php
$this->load->library('Mobile_Detect');
$detect = new Mobile_Detect();
    if($detect->isMobile() && !$detect->isTablet()){
        $isMobile = TRUE;
    } else {
        $isMobile = FALSE;
    }
$config = get_cache('configurations_' . get_language());
?>
<!-- HEADER -->
<?php echo $this->load->view('common/top'); ?>

<!-- end header -->
<!-- Home slideder-->
<div id="home-slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2" style="    padding-right: 14px;
    padding-left: 87px;">
                <div class="homeslider">
                    <?php  echo modules::run('advs/get_advs_slide_show'); ?>
                    
                </div>
            </div>
            
            <div class="col-sm-2 group-banner-slider hidden-xs" style="padding-left: 2px; margin-left:-15px; padding-right: 0;">
                <?php $page1 = modules::run('pages/get_page', '/thanh-thoi-mua-sam.html'); ?>
                <?php if (!empty($page1)) {
                    echo $page1['content'];
                } ?>
               
            </div>
            <div class="col-sm-12 header-top-right">
                <div class="group-banner-slide">
                    <div class="col-left">
                        
                    </div>
                    <div class="col-right">
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="content-page">
    <div class="container">
        <!-- featured category fashion -->
      
        <?php echo  $this->load->view('common/home_block'); ?>
        <!-- end featured category fashion -->
        
    </div>
</div>

<div id="content-wrap hidden-xs">
    <div class="container hidden-xs">
         <?php $page2 = modules::run('pages/get_page', '/cham-soc-khach-hang.html'); ?>
                <?php if (!empty($page2)) {
                    echo $page2['content'];
                } ?>
       
    </div> <!-- /.container -->
</div>

<!-- Footer -->
<?php echo $this->load->view('common/partner'); ?>
<?php echo $this->load->view('common/footer'); ?>

</body>

</html>