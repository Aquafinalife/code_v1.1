<?php
if (!empty($product)) {
    if (SLUG_ACTIVE == 0) {
        $uri = get_base_url() . url_title(trim($product->product_name), 'dash', TRUE) . '-ps' . $product->id;
        $uri2 = get_base_url() . url_title(trim($category), 'dash', TRUE) . '-p' . $product->categories_id;
    } else {
        $uri = get_base_url() . $product->slug;
        $uri2 = get_base_url() . $category_slug;
    }
//    $image = is_null($product->image_name) ? base_url().'images/no-image.png' : base_url().'images/products/'.$product->image_name;
//    $image_thumb = is_null($product->image_name) ? base_url().'images/no-image.png' : base_url().'images/products/thumbnails/'.$product->image_name;
    $price = $product->price != 0 ? get_price_in_vnd($product->price) . ' ₫' : get_price_in_vnd($product->price);
    $price_old = $product->price_old != 0 ? get_price_in_vnd($product->price_old) . ' ₫' : 0;
    $summary = limit_text($product->summary, 1000);
//    $specifications = $product->specifications;
    $description = $product->description;
    $code = $product->code;
    echo form_hidden('id', $product->id);
    echo form_hidden('addUri', get_uri_by_lang(get_language(), 'cart'));
    echo form_hidden('lang', get_language());

    $this->load->library('Mobile_Detect');
    $detect = new Mobile_Detect();
    if ($detect->isMobile() && !$detect->isTablet()) {
        $isMobile = TRUE;
    } else {
        $isMobile = FALSE;
    }
    $price_count = $product->price_old - $product->price;
    $saleoff = ($price_count / $product->price_old) * 100;
    $saleoff = round($saleoff, 0);
    $tinhtrang = $product->state_id;
   
    if ($tinhtrang == 1) {
        $tinhtrang = 'Còn hàng';
    } else {
        $tinhtrang = 'Hết hàng';
    }
    ?>

    <!-- row -->
    <div class="row">

        <!-- Center colunm-->
        <div class="center_column col-xs-12 col-sm-12" id="center_column">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>" title="<?php echo DEFAULT_COMPANY; ?>"><?php echo __('IP_home_page'); ?></a></li>
                <li><a href="<?php echo get_url_by_lang(get_language(),'products'); ?>" title="<?php echo __('IP_products'); ?>"><?php echo __('IP_products'); ?></a></li>
                <?php if(!empty($parent_category) && !empty($parent_category_id)){
                    $parent_category_url = get_base_url() . url_title(trim($parent_category), 'dash', TRUE) . '-p' . $parent_category_id;
                ?>
                    <li><a href="<?php echo $parent_category_url; ?>" title="<?php echo $parent_category; ?>"><?php echo $parent_category; ?></a></li>
                <?php } ?>
                <li><a href="<?php echo $uri2; ?>" title="<?php echo $category; ?>"><?php echo $category; ?></a></li>
                <li class="active"><?php echo $product->product_name; ?></li>
            </ol>
            <!-- Product -->
            <div id="product">
                <div class="primary-box row">
                    <div class="pb-left-column col-xs-12 col-sm-6">
                        <!-- product-imge-->
                        <div class="image">
                                <?php if (!empty($products_images)) { ?>
                                <div class="html5gallery" data-skin="light" data-width="300" data-height="210" data-slideshadow="false" data-resizemode="fill" data-responsive="true">
                                    <?php
                                    foreach ($products_images as $key => $value) {
                                        $value_image = is_null($value->image_name) ? base_url() . 'images/no-image.png' : base_url() . 'images/products/' . $value->image_name;
                                        $value_image_thumb = is_null($value->image_name) ? base_url() . 'images/no-image.png' : base_url() . 'images/products/thumbnails/' . $value->image_name;
                                        ?>
                                        <a href="<?php echo $value_image; ?>" title="<?php echo $product->product_name; ?>">
                                            <img title="<?php echo $product->product_name; ?>" src="<?php echo $value_image_thumb; ?>" alt="<?php echo $product->product_name; ?>">
                                        </a>
                                <?php } ?>
                                </div>
                            <?php } else { ?>
                                <img title="<?php echo $product->product_name; ?>" src="/images/no-image.png" />
    <?php } ?>
                        </div>


                        <!-- product-imge-->
                    </div>
                    <div class="pb-right-column col-xs-12 col-sm-6">
                        <h1 class="product-name"><?php echo $product->product_name; ?></h1>
                        <div class="product-comments">
                            <div class="product-star">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </div>

                        </div>
                        <div class="product-price-group">
                            <?php if (!empty($price) && $price <> 0) { ?>
                                <span class="price"><?php echo $price; ?></span>
                            <?php } ?>
                            <?php if (!empty($price_old) && $price_old <> 0) { ?>
                                <span class="old-price"><?php echo $price_old; ?></span>
                            <?php } ?>
                            <?php if (!empty($saleoff) && $saleoff <> 0) { ?>
                                <span class="discount"><?php echo '-' . $saleoff . '%'; ?></span>
    <?php } ?>

                        </div>
                        <div class="info-orther">
                            <?php if (!empty($product->code)) { ?>
                                <p>Mã sản phẩm: <?php echo $code; ?></p>
    <?php } ?>

                            <p>Tình trạng: <span class="in-stock"><?php echo $tinhtrang; ?></span></p>

                        </div>
                        <div class="product-desc">
                            <?php if (!empty($product->summary)) { ?>
                                <div><h2><?php echo $summary; ?></h2></div>
    <?php } ?> 
                        </div>

                        <?php if(!empty($product->size)){ ?>
            <div>
                <!--<input type="hidden" name="size_selected" value="" />-->
                <p>Chọn kích cỡ: </p>
            <?php 
                $size_arr = @explode(",", $product->size);
                $size = modules::run('products/get_size',$size_arr);
                foreach($size as $k => $v){
                if($k==0){
            ?>
                <span class="sizebox sizebox_selected" title="<?php echo $v; ?>" onclick="select_size();$(this).addClass('sizebox_selected');"><?php echo $v; ?></span>
            <?php }else{ ?>
                <span class="sizebox" title="<?php echo $v; ?>" onclick="select_size();$(this).addClass('sizebox_selected');"><?php echo $v; ?></span>
            <?php }} ?>
            </div>
            <?php } ?>
                        
                        <div class="form-action">
    <?php if ($product->price > 0) { ?>
                                <div class="button-group">
                                    <button type="button" class="addtocart btn-add-cart" id="addtocart">&nbsp;<?php echo __('IP_products_add_cart'); ?></button>
                                </div>
    <?php } else { ?>
                                <form method="post" action="<?php echo base_url() . CONTACT_HOME_URL; ?>">
                                    <input type="hidden" name="id" value="<?php echo $product->id; ?>" />
                                    <input type="submit" class="contact_order_submit btn-add-cart" style="margin-top: 15px;" value="<?php echo __('IP_products_contact'); ?>" />
                                </form>
    <?php } ?>
                        </div>
                      
                    </div>
                </div>
                <!-- tab product -->
                
                <div class="product-tab">
                    <ul class="nav-tab">
                        <li class="active">
                            <a aria-expanded="false" data-toggle="tab" href="#product-detail">Thông tin sản phẩm </a>
                        </li>
                      

                    </ul>
                    <div class="tab-container">
                        <div id="product-detail" class="tab-panel active">
                            <!-- Button trigger modal -->

    <?php echo $description; ?>
                            <div class="post-share">
                                <span class="share-text">
                                    Share 
                                </span>
                                <!--sharing-->
                                <div class="sharing">
                                    <div class="sharing_tab sharing_g">
                                        <script src="https://apis.google.com/js/platform.js" async defer></script>
                                        <g:plusone size="medium" data-annotation="bubble"></g:plusone>
                                    </div>
                                    <div class="sharing_tab sharing_t">
                                        <a href="https://twitter.com/share" class="twitter-share-button" data-via="">Tweet</a>
                                        <script>!function (d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = p + '://platform.twitter.com/widgets.js';
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, 'script', 'twitter-wjs');</script>
                                    </div>
                                    <div class="sharing_tab sharing_f">

                                        <div class="fb-like" data-href="<?php echo current_url(); ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                                    </div>
                                </div>
                            </div>

                            <div id="comments" class="comments-area">
                                <div id="respond" class="comment-respond">
                                    <!--facebook comment-->
                                    <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-numposts="10" data-colorscheme="light" style="margin: 20px 0"></div>
                                </div>
                            </div>
                        </div>
                  

                    </div>
                </div>
              
               
                <!-- ./tab product -->
                <!-- box product -->
    <?php if (isset($products_same)) {
        echo $products_same;
    } ?>

                <!-- ./box product -->

            </div>
            <!-- Product -->
        </div>
        <!-- ./ Center colunm -->

    </div>
    <!-- ./row-->



<?php } else {
    echo '<h4 class="alert alert-info">' . __('IP_comming_soon') . '</h4>';
} ?>