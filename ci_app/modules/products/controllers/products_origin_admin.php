<?php

class Products_Origin_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        modules::run('auth/auth/validate_login');
        $this->_layout = 'admin_ui/layout/main';
    }

    function browse() {
        $options = array();

        $mb = $this->products_origin_model->get_products_origin();
        if (!empty($mb)) {
            $options['mb'] = $mb;
        }
        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $options['options'] = $options;
        }
        //======================================================================
        $tpl = 'ad/ma_benh/mabenh_list';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Mã bệnh' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function add() {
        $options = array();
        // Xu ly viec them vao db
        if ($this->is_postback()) {
            if (!$this->_do_add()) {
                $options['error'] = validation_errors();
                $options['options'] = $options;
            }
        }
        //======================================================================
        $tpl = 'ad/ma_benh/mabenh_form_creat';
        $options['name'] = $this->input->post('name');
        $options['header'] = 'Thêm mã bệnh';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_ORIGIN_ADMIN_ADD_URL;
        //======================================================================
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Thêm mã bệnh' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _do_add() {
        $this->form_validation->set_rules('name', 'Tên bệnh', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('ma_benh', 'Mã bệnh', 'trim|required|xss_clean|max_length[255]');
        if ($this->form_validation->run()) {
            $data = array(
                'name' => my_trim($this->input->post('name', TRUE)),
                'ma_benh' => my_trim($this->input->post('ma_benh', TRUE)),
                'status' => STATUS_ACTIVE,
                'editor' => $this->phpsession->get('user_id'),
            );
            //==================================================================
            $mb_id = $this->input->post('id');
            if ($mb_id > 0) {
                $data['id'] = $mb_id;
                $this->products_origin_model->update($data);
            } else {
                $data['creator'] = $this->phpsession->get('user_id');
                $this->products_origin_model->insert($data);
            }
            //==================================================================
            redirect(PRODUCTS_ORIGIN_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //==========================================================================
    function edit() {
        $options = array();
        if ($this->is_postback()) {
            if (!$this->input->post('from_list')) {
                if (!$this->_do_add()) {
                    $options['error'] = validation_errors();
                    $options['options'] = $options;
                }
            }
        }
        $mb_id = $this->input->post('id', TRUE);
        //======================================================================
        // Lấy mã bệnh
        $data_get = array(
            'id' => $mb_id,
            'get_row' => TRUE,
        );
        $mb = $this->products_origin_model->CommonGet(TBL_MABENH, $data_get);
        if (is_object($mb)) {
            $options['mb'] = $mb;
        }
        //======================================================================
        $options['header'] = 'Sửa mã bệnh';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_ORIGIN_ADMIN_EDIT_URL;
        //======================================================================
        $tpl = 'ad/ma_benh/mabenh_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Sửa mã bệnh' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================

    function delete() {
        if ($this->is_postback() && $this->input->post('from_list')) {
            $id = $this->input->post('id');
            $check1 = $this->products_model->get_products_count(array('origin_id' => $id));
            if (empty($check1)) {
                $this->products_origin_model->delete($id);
            }
            redirect(PRODUCTS_ORIGIN_ADMIN_BASE_URL);
        }
    }

}
