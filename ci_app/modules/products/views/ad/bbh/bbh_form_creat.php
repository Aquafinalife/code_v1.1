<?php
if (isset($dn_bbh) && is_object($dn_bbh)) {
    $name = $dn_bbh->name;
    $dn_parent_id = $dn_bbh->parent_id;
    $dn_id = $id = $dn_bbh->id;
    $email = $dn_bbh->email;
    $password = $dn_bbh->password;
}
//==============================================================================
echo form_open($submit_uri);
if (isset($id))
    echo form_hidden('id', $id);
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'products_trademark');
?>

<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Thêm/sửa Nhà cung cấp BH"</small>
    <span class="fright"><a href="<?php echo PRODUCTS_TRADEMARK_ADMIN_BASE_URL; ?>" class="button close" style="padding:5px;" title="Đóng"><em></em><span>Đóng</span></a></span>
    <br class="clear"/>
</div>

<div class="form_content">
    <?php $this->load->view('powercms/message'); ?>
    <ul class="tabs">
        <li><a href="#tab1">Nội dung</a></li>
    </ul>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <table>
                <tr><td class="title">Tên Nhà cung cấp BH: (<span>*</span>)</td></tr>
                <tr>
                    <td><?php echo form_input(array('name' => 'name', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($name) ? $name : set_value('name'))); ?></td>
                </tr>
                <tr><td class="title">Email: (<span>*</span>)</td></tr>
                <tr>
                    <td><?php echo form_input(array('name' => 'email', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($email) ? $email : set_value('email'))); ?></td>
                </tr>
                <tr><td class="title">Password: (<span>*</span>)</td></tr>
                <tr>
                    <td><?php echo form_input(array('name' => 'password', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($password) ? $password : set_value('password'))); ?></td>
                </tr>
                <tr><td class="title">Trực thuộc công ty: (<span>*</span>)</td></tr>
                <tr>
                    <td>
                        <select  name="categories" class="">
                            <option value="0">Công ty/doanh nghiệp riêng</option>
                            <?php
                            if (!empty($a_dn_bbh)) {
                                foreach ($a_dn_bbh as $index) {
                                    $name = $index->name;
                                    $parent_id = $index->parent_id;
                                    $id = $index->id;
                                    $selected = ($id == $dn_parent_id) ? 'selected="selected"' : '';
                                    if ($id != $dn_id) {
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>

    </div>
    <br class="clear"/>
    <div style="margin-top: 10px;"></div>
    <?php echo form_submit(array('name' => 'btnSubmit', 'value' => 'Lưu dữ liệu', 'class' => 'btn')); ?>
    <a style="margin-left: 20px;" class="btn btn-primary" href="<?php echo site_url(PRODUCTS_TRADEMARK_ADMIN_BASE_URL)?>">Quay lại <span class="glyphicon glyphicon-share-alt"></span></a>
    <br class="clear"/>&nbsp;
</div>
<?php echo form_close(); ?>