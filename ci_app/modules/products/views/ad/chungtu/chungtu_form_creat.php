<?php
echo form_open($submit_uri);
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'products_chứng từ');
//==============================================================================
$a_phanloai_bt = get_a_phanloai_bt();
if (isset($ct) && is_object($ct)) {
    $ct_id = $ct->id;
    $name = $ct->name;
    $parent_id = $ct->parent_id;
}
//==============================================================================
$parent_id = isset($parent_id) ? $parent_id : '';
if (isset($ct_id) && $ct_id > 0) {
    echo form_hidden('id', $ct_id);
}
//==============================================================================
?>
<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Thêm/sửa Chứng từ"</small>
    <span class="fright"><a href="<?php echo PRODUCTS_COUPON_ADMIN_BASE_URL; ?>" class="button close" style="padding:5px;" title="Đóng"><em></em><span>Đóng</span></a></span>
    <br class="clear"/>
</div>

<div class="form_content">
    <?php $this->load->view('powercms/message'); ?>
    <ul class="tabs">
        <li><a href="#tab1">Nội dung</a></li>
    </ul>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <table>
                <tr><td class="title">Tên chứng từ: (<span>*</span>)</td></tr>
                <tr>
                    <td><?php echo form_input(array('name' => 'name', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($name) ? $name : set_value('name'))); ?></td>
                </tr>
                <tr><td class="title">Thuộc loại bồi thường: (<span>*</span>)</td></tr>
                <tr>
                    <td>
                        <select name="categories" class="form-control js-example-disabled-results">
                            <option value="">-- Chọn loại bồi thường --</option>
                            <?php
                            if (isset($a_phanloai_bt) && !empty($a_phanloai_bt)) {
                                foreach ($a_phanloai_bt as $k => $v) {
                                    $selected = ($parent_id == $k) ? 'selected="selected"' : '';
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>

    </div>
    <br class="clear"/>
    <div style="margin-top: 10px;"></div>
    <?php echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn')); ?>
    <a style="margin-left: 20px;" class="btn btn-primary" href="<?php echo site_url(PRODUCTS_COUPON_ADMIN_BASE_URL)?>">Quay lại <span class="glyphicon glyphicon-share-alt"></span></a>
    <br class="clear"/>&nbsp;
</div>
<?php echo form_close(); ?>