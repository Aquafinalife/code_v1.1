<?php
$a_customers_type = get_a_customers_type();
$this->load->view('admin/products/product_nav');
echo form_open('', array('id' => 'submit_form'), array('id' => 0, 'from_list' => TRUE));
echo isset($uri) ? form_hidden('uri', $uri) : NULL;
echo form_hidden('back_url', PRODUCTS_ADMIN_BASE_URL);
echo form_close();
?>
<div class="form_content">

    <div class="filter">   
        <?php
        $a_customers_type = get_a_customers_type();
        $GET_phanloai_kh = isset($_GET['phanloai_kh']) ? $_GET['phanloai_kh'] : '';
        $GET_key = isset($_GET['key']) ? $_GET['key'] : '';
        ?>
        <table style="width: 100%;">
            <tbody>
                <tr>
                    <td style="
                        width: 700px;
                        ">
                        <form method="get" action ="<?php echo PRODUCTS_ADMIN_BASE_URL; ?>">
                            <div class="list_box_filter">
                                <input style="width: 250px" name="key" value="<?php echo str_replace('\\', '', $GET_key); ?>" placeholder="Tên khách hàng / CMTND / Số hợp đồng..."/>
                            </div>
                            <div class="list_box_filter">
                                Loại KH: 
                                <select parent_id="0" selectbox_name="dn" selectbox_id="filter_doanhnghiep" name="phanloai_kh" class="filter_phanloai_kh js-example-disabled-results">
                                    <option value="">Tất cả khách hàng</option>
                                    <?php
                                    if (!empty($a_customers_type)) {
                                        foreach ($a_customers_type as $k => $v) {
                                            $selected = ($k == $GET_phanloai_kh) ? 'selected="selected"' : '';
                                            ?>
                                            <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="list_box_filter box_filter_doanhnghiep">
                                <?php
                                if (isset($selectbox_dn)) {
                                    echo $selectbox_dn;
                                }
                                ?>
                            </div>
                            <div class="list_box_filter box_filter_chinhanh">
                                <?php
                                if (isset($selectbox_cn)) {
                                    echo $selectbox_cn;
                                }
                                ?>
                            </div>
                            <div class="list_box_filter">
                                <input type="submit" value="Tìm kiếm" class="btn"/>
                            </div>
                        </form>
                    </td>

                </tr>
            </tbody>
        </table>
    </div>
    <table class="list" style="width: 100%; margin-bottom: 10px;">
        <?php $this->load->view('powercms/message'); ?>
        <tr>
            <th class="left" style="width: 5%" >SỐ HỢP ĐỒNG</th>
            <!--<th class="center" style="width: 5%">BÊN MUA BH</th>-->
            <th class="center" style="width: 5%">NGƯỜI ĐƯỢC BH</th>
            <th class="center" style="width: 5%">CMTND</th>
            <th class="center" style="width: 5%">CHỨC NĂNG</th>
        </tr>
        <?php
        if (isset($bh)) {
//            echo '<pre>';
//            print_r($bh);
//            die;
            $stt = 0;
            foreach ($bh as $index):
                $kh_dbh_txt = $kh_mbh_txt = '';

                $bh_id = $index->BH_id;
                $kh_mbh = $index->kh_mbh;
                $kh_dbh = $index->kh_dbh;
                $kh_dbh_txt = $index->congty_canhan;
                $mst_cmt = $index->mst_cmt;
                $sohopdong = $index->sohopdong;

//                if (isset($kh) && !empty($kh)) {
//                    foreach ($kh as $index) {
//                        $kh_id = $index->id;
//                        $congty_canhan = $index->congty_canhan;
//                        if ($kh_id == $kh_dbh) {
//                            $kh_dbh_txt = $congty_canhan;
//                        }
//                        if ($kh_id == $kh_mbh) {
//                            $kh_mbh_txt = $congty_canhan;
//                        }
//                    }
//                }
                ?>
                <tr class="<?php echo $style ?>">
                    <td style="width:1%;"><?php echo $sohopdong; ?></td>
                    <td class="left" style="word-wrap:break-word;">
                        <?php echo $kh_dbh_txt; ?>
                    </td>
                    <td>
                        <?php echo $mst_cmt; ?>
                    </td>
                    <td class="center" style="white-space:nowrap;" class="action">
                        <a class="edit" title="Sửa thông tin bảo hiểm" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $bh_id ?>, '<?php echo PRODUCTS_ADMIN_EDIT_URL; ?>', 'edit');"><em>&nbsp;</em></a>
                        <a class="del" title="Xóa bảo hiểm" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $bh_id ?>, '<?php echo PRODUCTS_ADMIN_DELETE_URL; ?>', 'delete');"><em>&nbsp;</em></a>
                        <a class="up" title="Cập nhật (up) bảo hiểm lên đầu trang" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $bh_id ?>, '<?php echo PRODUCTS_ADMIN_UP_URL; ?>', 'up');"><em>&nbsp;</em></a>
                    </td>
                </tr>

            <?php endforeach; ?>
        <?php } ?>
        <?php $left_page_links = 'Tổng số: ' . $total_rows . ' bồi thường</span>'; ?>
        <tr class="list-footer">
            <th colspan="9">
                <div style="float:left; margin-top: 9px;"><?php echo $left_page_links; ?></div>
                <div class="col-sm-12 text-center">
                    <div class="pagination">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>
            </th>
        </tr>
    </table>
    <br class="clear"/>&nbsp;
</div>