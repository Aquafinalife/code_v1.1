<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
$a_bt_thanhtoan = get_a_bt_thanhtoan();
//end
?>
<div class="box_filter" style="margin-bottom: 20px">
</div>
<table id="_table-edit" class="table table-bordered table-hover">    
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Báo cáo phân tích theo quyền lợi bồi thường:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th>Loại bồi thường</th>
            <th>Tổng số HSBT</th>
            <th>Tổng số tiền YCBT</th>
            <th>Tổng số tiền ĐBT</th>
            <th>Tỷ lệ ST-ĐBT / ST-YCBT</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $st_ycbt_nkmdl = $st_ycbt_dtnt = $st_ycbt_dtngt = $st_ycbt_tvtttbvv_tn = $st_ycbt_tvtttbvv_ob = $st_ycbt_tsmdl = $st_ycbt_tsqlnt = $st_ycbt_tcl_nn = $st_ycbt_cpyt_tn = 0;
        $st_dbt_nkmdl = $st_dbt_dtnt = $st_dbt_dtngt = $st_dbt_tvtttbvv_tn = $st_dbt_tvtttbvv_ob = $st_dbt_tsmdl = $st_dbt_tsqlnt = $st_dbt_tcl_nn = $st_dbt_cpyt_tn = 0;
        $t_hsbt_nkmdl = $t_hsbt_dtnt = $t_hsbt_dtngt = $t_hsbt_tvtttbvv_tn = $t_hsbt_tvtttbvv_ob = $t_hsbt_tsmdl = $t_hsbt_tsqlnt = $t_hsbt_tcl_nn = $t_hsbt_cpyt_tn = 0;
        if (isset($bt) && !empty($bt)) {
            $j = 1;
            foreach ($bt as $index) {
                $BT_id = $index->id;
                $OD_id = $index->OD_id;
                $KH_id = $index->KH_id;
                $kh_mbh = $index->kh_mbh;
                $phanloai_bt = $index->phanloai_bt;
                $so_tien_ycbt = $index->so_tien_ycbt;
                $so_tien_dbt = $index->so_tien_dbt;
                $bt_status = $index->order_status;
                //==============================================================                
                if ($phanloai_bt == BT_DTNT_OB) {
                    $t_hsbt_dtnt = $t_hsbt_dtnt + 1;
                    $st_ycbt_dtnt = $st_ycbt_dtnt + $so_tien_ycbt;
                    if (in_array($bt_status, $a_bt_thanhtoan)) {
                        $st_dbt_dtnt = $st_dbt_dtnt + $so_tien_dbt;
                    }
                }
                if ($phanloai_bt == BT_DTNGT_OB) {
                    $t_hsbt_dtngt = $t_hsbt_dtngt + 1;
                    $st_ycbt_dtngt = $st_ycbt_dtngt + $so_tien_ycbt;
                    if (in_array($bt_status, $a_bt_thanhtoan)) {
                        $st_dbt_dtngt = $st_dbt_dtngt + $so_tien_dbt;
                    }
                }
                if ($phanloai_bt == BT_TVTTTBVV_TN) {
                    $t_hsbt_tvtttbvv_tn = $t_hsbt_tvtttbvv_tn + 1;
                    $st_ycbt_tvtttbvv_tn = $st_ycbt_tvtttbvv_tn + $so_tien_ycbt;
                    if (in_array($bt_status, $a_bt_thanhtoan)) {
                        $st_dbt_tvtttbvv_tn = $st_dbt_tvtttbvv_tn + $so_tien_dbt;
                    }
                }
                if ($phanloai_bt == BT_TVTTTBVV_OB) {
                    $t_hsbt_tvtttbvv_ob = $t_hsbt_tvtttbvv_ob + 1;
                    $st_ycbt_tvtttbvv_ob = $st_ycbt_tvtttbvv_ob + $so_tien_ycbt;
                    if (in_array($bt_status, $a_bt_thanhtoan)) {
                        $st_dbt_tvtttbvv_ob = $st_dbt_tvtttbvv_ob + $so_tien_dbt;
                    }
                }
                if ($phanloai_bt == BT_THAISAN_MDL) {
                    $t_hsbt_tsmdl = $t_hsbt_tsmdl + 1;
                    $st_ycbt_tsmdl = $st_ycbt_tsmdl + $so_tien_ycbt;
                    if (in_array($bt_status, $a_bt_thanhtoan)) {
                        $st_dbt_tsmdl = $st_dbt_tsmdl + $so_tien_dbt;
                    }
                }
                if ($phanloai_bt == BT_THAISAN_QLNT) {
                    $t_hsbt_tsqlnt = $t_hsbt_tsqlnt + 1;
                    $st_ycbt_tsqlnt = $st_ycbt_tsqlnt + $so_tien_ycbt;
                    if (in_array($bt_status, $a_bt_thanhtoan)) {
                        $st_dbt_tsqlnt = $st_dbt_tsqlnt + $so_tien_dbt;
                    }
                }
                if ($phanloai_bt == BT_TCL_NN) {
                    $t_hsbt_tcl_nn = $t_hsbt_tcl_nn + 1;
                    $st_ycbt_tcl_nn = $st_ycbt_tcl_nn + $so_tien_ycbt;
                    if (in_array($bt_status, $a_bt_thanhtoan)) {
                        $st_dbt_tcl_nn = $st_dbt_tcl_nn + $so_tien_dbt;
                    }
                }

                if ($phanloai_bt == BT_CPYT_TN) {
                    $t_hsbt_cpyt_tn = $t_hsbt_cpyt_tn + 1;
                    $st_ycbt_cpyt_tn = $st_ycbt_cpyt_tn + $so_tien_ycbt;
                    if (in_array($bt_status, $a_bt_thanhtoan)) {
                        $st_dbt_cpyt_tn = $st_dbt_cpyt_tn + $so_tien_dbt;
                    }
                }

                if ($phanloai_bt == BT_NHAKHOA_MDL) {
                    $t_hsbt_nkmdl = $t_hsbt_nkmdl + 1;
                    $st_ycbt_nkmdl = $st_ycbt_nkmdl + $so_tien_ycbt;
                    if (in_array($bt_status, $a_bt_thanhtoan)) {
                        $st_dbt_nkmdl = $st_dbt_nkmdl + $so_tien_dbt;
                    }
                }
                ?>

                <?php
            }
            $t_hsbt = $t_hsbt_nkmdl + $t_hsbt_dtnt + $t_hsbt_dtngt + $t_hsbt_tvtttbvv_tn + $t_hsbt_tvtttbvv_ob + $t_hsbt_tsmdl + $t_hsbt_tsqlnt + $t_hsbt_tcl_nn + $t_hsbt_cpyt_tn;
            $t_st_ycbt = $st_ycbt_nkmdl + $st_ycbt_dtnt + $st_ycbt_dtngt + $st_ycbt_tvtttbvv_tn + $st_ycbt_tvtttbvv_ob + $st_ycbt_tsmdl + $st_ycbt_tsqlnt + $st_ycbt_tcl_nn + $st_ycbt_cpyt_tn;
            $t_st_dbt = $st_dbt_nkmdl + $st_dbt_dtnt + $st_dbt_dtngt + $st_dbt_tvtttbvv_tn + $st_dbt_tvtttbvv_ob + $st_dbt_tsmdl + $st_dbt_tsqlnt + $st_dbt_tcl_nn + $st_dbt_cpyt_tn;
        } else {
            ?>
            <!--            <tr>
                            <td colspan="10" class="text-danger">Không có dữ liệu.</td>
                        </tr>-->
            <?php
        }
        ?>
        <tr>
            <td>Điều trị nội trú</td>
            <td><?php echo $t_hsbt_dtnt; ?></td>
            <td><?php echo get_price_in_vnd($st_ycbt_dtnt); ?></td>
            <td><?php echo get_price_in_vnd($st_dbt_dtnt); ?></td>
            <td><?php echo round((($st_dbt_dtnt / $st_ycbt_dtnt) * 100), 2) . '%'; ?></td>
        </tr>
        <tr>
            <td>Điều trị ngoại trú</td>
            <td><?php echo $t_hsbt_dtngt; ?></td>
            <td><?php echo get_price_in_vnd($st_ycbt_dtngt); ?></td>
            <td><?php echo get_price_in_vnd($st_dbt_dtngt); ?></td>
            <td><?php echo round((($st_dbt_dtngt / $st_ycbt_dtngt) * 100), 2) . '%'; ?></td>
        </tr>
        <tr>
            <td>Thai sản trong QLNT</td>
            <td><?php echo $t_hsbt_tsqlnt; ?></td>
            <td><?php echo get_price_in_vnd($st_ycbt_tsqlnt); ?></td>
            <td><?php echo get_price_in_vnd($st_dbt_tsqlnt); ?></td>
            <td><?php echo round((($st_dbt_tsqlnt / $st_ycbt_tsqlnt) * 100), 2) . '%'; ?></td>
        </tr>

        <tr>
            <td>Thai sản mua độc lập</td>
            <td><?php echo $t_hsbt_tsmdl; ?></td>
            <td><?php echo get_price_in_vnd($st_ycbt_tsmdl); ?></td>
            <td><?php echo get_price_in_vnd($st_dbt_tsmdl); ?></td>
            <td><?php echo round((($st_dbt_tsmdl / $st_ycbt_tsmdl) * 100), 2) . '%'; ?></td>
        </tr>

        <tr>
            <td>Tử vong,TTTBVV do tai nạn</td>
            <td><?php echo $t_hsbt_tvtttbvv_tn; ?></td>
            <td><?php echo get_price_in_vnd($st_ycbt_tvtttbvv_tn); ?></td>
            <td><?php echo get_price_in_vnd($st_dbt_tvtttbvv_tn); ?></td>
            <td><?php echo round((($st_dbt_tvtttbvv_tn / $st_ycbt_tvtttbvv_tn) * 100), 2) . '%'; ?></td>
        </tr>

        <tr>
            <td>Tử vong,TTTBVV do ốm bệnh</td>
            <td><?php echo $t_hsbt_tvtttbvv_ob; ?></td>
            <td><?php echo get_price_in_vnd($st_ycbt_tvtttbvv_ob); ?></td>
            <td><?php echo get_price_in_vnd($st_dbt_tvtttbvv_ob); ?></td>
            <td><?php echo round((($st_dbt_tvtttbvv_ob / $st_ycbt_tvtttbvv_ob) * 100), 2) . '%'; ?></td>
        </tr>

        <tr>
            <td>Trợ cấp lương ngày nghỉ</td>
            <td><?php echo $t_hsbt_tcl_nn; ?></td>
            <td><?php echo get_price_in_vnd($st_ycbt_tcl_nn); ?></td>
            <td><?php echo get_price_in_vnd($st_dbt_tcl_nn); ?></td>
            <td><?php echo round((($st_dbt_tcl_nn / $st_ycbt_tcl_nn) * 100), 2) . '%'; ?></td>
        </tr>

        <tr>
            <td>Chi phí y tế do tai nạn</td>
            <td><?php echo $t_hsbt_cpyt_tn; ?></td>
            <td><?php echo get_price_in_vnd($st_ycbt_cpyt_tn); ?></td>
            <td><?php echo get_price_in_vnd($st_dbt_cpyt_tn); ?></td>
            <td><?php echo round((($st_dbt_cpyt_tn / $st_ycbt_cpyt_tn) * 100), 2) . '%'; ?></td>
        </tr>
        <tr>
            <td>Nha khoa mua độc lập</td>
            <td><?php echo $t_hsbt_nkmdl; ?></td>
            <td><?php echo get_price_in_vnd($st_ycbt_nkmdl); ?></td>
            <td><?php echo get_price_in_vnd($st_dbt_nkmdl); ?></td>
            <td><?php echo round((($st_dbt_nkmdl / $st_ycbt_nkmdl) * 100), 2) . '%'; ?></td>
        </tr>
        <tr>
            <td><strong>Tổng cộng</strong></td>
            <td><strong><?php echo count($bt); ?></strong></td>
            <td><strong><?php echo get_price_in_vnd($t_st_ycbt); ?></strong></td>
            <td><strong><?php echo get_price_in_vnd($t_st_dbt); ?></strong></td>
            <td><strong><?php echo round((((int)$t_st_dbt / (int)$t_st_ycbt) * 100), 2) . '%'; ?></strong></td>
        </tr>
    </tbody>
</table>
<div class="col-sm-12 text-center">
    <div class="pagination">
        <?php // echo $this->pagination->create_links();  ?>
    </div>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>