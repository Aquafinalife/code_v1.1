<?php $data = modules::run('gallery/get_projects'); ?>
<?php if(!empty($data)) { ?>
<div class="homeprojects">
    <h3><?php echo __('IP_projects_typical'); ?></h3>
    <div class="homeprojects_inner">
        <?php foreach($data as $key => $value){
            $image_url = is_null($value->image_name) ? base_url().'images/no-image.png' : base_url().'images/gallery/thumbnails/'.$value->image_name;
            $short_gallery_name = limit_text($value->gallery_name, 100);
            if(SLUG_ACTIVE==0){
                $uri = get_base_url() . url_title(trim($value->gallery_name), 'dash', TRUE) . '-gs' . $value->id;    
            }else{
                $uri = get_base_url() . $value->slug;
            }
        ?>
        <div class="hp_item">            
            <img src="<?php echo $image_url; ?>" alt="">
            <div class="textbox">
                <p class="text">
                    <a href="<?php echo $uri; ?>" title="<?php echo $value->gallery_name; ?>">
                        <?php echo $short_gallery_name; ?>
                        <br /><i class="fa fa-plus-circle"></i>
                    </a>
                </p>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
