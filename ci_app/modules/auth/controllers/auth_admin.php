<?php

class Auth_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        //modules::run('auth/auth/validate_login');
        // Khoi tao cac bien
        $this->_layout = 'admin_ui/layout/main';
        $this->ad_check_login();
        //======================================================================
        $ss_ql_all = $this->phpsession->get('ss_ql_all');
        $ss_ql_users = $this->phpsession->get('ss_ql_users');
        if ($ss_ql_all != QL_CHECKED) {
            if ($ss_ql_users != QL_CHECKED) {
                redirect('dashboard');
            }
        }
        
    }

    public function permission_denied() {
        $this->_view_data['title'] = 'Permission denied!';
        $this->_view_data['main_content'] = $this->load->view('auth/permission_denied', array(), TRUE);
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function get_data_auto_complate_custom_ctv() {

        $options = array();
        $options['search'] = $this->input->post('search_custom_ctv');
        $options['lang'] = 'vi';

        $view_data = array();
        $auth = $this->users_model->get_users($options);


        if (!empty($auth)) {
            foreach ($auth as $x => $item) {

                $title = '<strong >' . $options['search'] . '</strong>';
                $location = '<strong >' . $options['search'] . '</strong>';
                $title_st = str_ireplace($options['search'], $title, $item->fullname);
                $loca_st = str_ireplace($options['search'], $location, $item->phone);
                $id_cus = $item->id;
                ?>
                <div class="show" align="left">
                    <span class="custom_name_ctv"><?php echo $title_st; ?></span>
                    <span class="id_cus_ctv" style="display: none"><?php echo $id_cus; ?></span>
                </div> 	
                <?php
            }
        } else {
            ?>
            <div class="show" align="left">
                Rất tiêc. Chúng tôi không tìm thấy khách hàng nào              
            </div>
            <?php
        }
    }

    //==========================================================================
    public function change_password() {
        $options = array();
        // Chuan bi thong bao loi neu co        
        if ($this->is_postback())
            if ($this->_do_change_password())
                $options['succeed'] = $this->_last_message;
            else
                $options['error'] = $this->_last_message;

        // Chuan bi cac the META
        $this->_view_data['title'] = 'Thay đổi mật khẩu';
        // Chuan bi du lieu chinh de hien thi
        $this->_view_data['main_content'] = $this->load->view('auth/admin/change_password_form', array('options' => $options), TRUE);

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _do_change_password() {
        $params = array();
        $this->form_validation->set_rules('password', 'Mật khẩu cũ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('new_password', 'Mật khẩu mới', 'trim|required|min_length[5]|max_length[50]|xss_clean');
        $this->form_validation->set_rules('new_password2', 'Xác nhận mật khẩu mới', 'trim|required|min_length[5]|max_length[50]|matches[new_password]|xss_clean');
        $this->form_validation->set_rules('security_code', 'Mã an toàn', 'trim|required|matches_value[' . $this->phpsession->get('captcha') . ']|xss_clean');

        if ($this->form_validation->run()) {
            $params['user_id'] = $this->phpsession->get('user_id');
            $params['password'] = md5($this->input->post('password'));
            $params['new_password'] = md5($this->input->post('new_password'));
            $params['new_password2'] = md5($this->input->post('new_password2'));

            // Neu nguoi dung ton tai thi moi thuc hien viec thay doi mat khau
            $contact = $this->users_model->is_user_existed(array('user_id' => $params['user_id']));
            if (is_object($contact)) {
                if ($contact->password === $params['password']) {
                    $this->users_model->update_user(array('id' => $params['user_id'], 'password' => $params['new_password']));
                    $this->_last_message = '<p>Bạn đã thay đổi mật khẩu thành công.</p>';
                    return TRUE;
                } else {
                    $this->_last_message = '<p>Mật khẩu cũ không chính xác.</p>';
                    return FALSE;
                }
            }

            $this->_last_message = $this->contact_model->get_last_message();
            return FALSE;
        }

        $this->_last_message = validation_errors();
        return FALSE;
    }

    //==========================================================================
    public function __browse($para1 = DEFAULT_LANGUAGE, $para2 = 1) {
        //modules::run('auth/auth/check_permission', $this->router->fetch_module());
        $options = array('lang' => switch_language($para1), 'page' => $para2);
        $options = array_merge($options, $this->_get_data_from_filter());
        $this->phpsession->save('users_lang', $options['lang']);

        $total_row = $this->users_model->get_users_count($options);
        $total_pages = (int) ($total_row / AUTH_USERS_ADMIN_POST_PER_PAGE);
        if ($total_pages * AUTH_USERS_ADMIN_POST_PER_PAGE < $total_row)
            $total_pages++;
        if ((int) $options['page'] > $total_pages)
            $options['page'] = $total_pages;

        $options['offset'] = $options['page'] <= DEFAULT_PAGE ? DEFAULT_OFFSET : ((int) $options['page'] - 1) * AUTH_USERS_ADMIN_POST_PER_PAGE;
        $options['limit'] = AUTH_USERS_ADMIN_POST_PER_PAGE;

        $config = prepare_pagination(
                array(
                    'total_rows' => $total_row,
                    'per_page' => $options['limit'],
                    'offset' => $options['offset'],
                    'js_function' => 'change_page_admin'
                )
        );
        $this->pagination->initialize($config);

        $options['users'] = $this->users_model->get_users($options);
        $options['total_rows'] = $total_row;
        $options['total_pages'] = $total_pages;
        $options['page_links'] = $this->pagination->create_ajax_links();

        if ($options['lang'] <> DEFAULT_LANGUAGE) {
            $options['uri'] = AUTH_USERS_ADMIN_BASE_URL . '/' . $options['lang'];
        } else {
            $options['uri'] = AUTH_USERS_ADMIN_BASE_URL;
        }
        //======================================================================
        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $options['options'] = $options;
        }
        $this->_view_data['main_content'] = $this->load->view('admin/list', $options, TRUE);
        $this->_view_data['title'] = 'Quản lý người dùng' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    public function browse($offset = 0, $options = array()) {
        $per_page = LIMIT_DEFAULT;
        // Đếm tổng số bt
        $total_rows = $this->users_model->CommonCount(TBL_USERS, $options);
        //$total_rows = 20;
        $options['total_rows'] = $total_rows;
        //end
        //======================================================================
        //echo $this->uri->segment(3).'????';
        $paging_config = array(
            'base_url' => site_url('dashboard/') . '/' . $this->uri->segment(2),
            'total_rows' => $total_rows,
            'per_page' => $per_page,
            'uri_segment' => 3,
            'use_page_numbers' => TRUE,
            'first_link' => __('IP_paging_first'),
            'last_link' => __('IP_paging_last'),
            'num_links' => 1,
        );
        if (isset($_GET) && $_GET != '') {
            $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($paging_config);
        $options['pagination'] = $this->pagination->create_links();

        //======================================================================
        $options['offset'] = ($offset > 0) ? ($offset - 1) * ($per_page) : $offset;
        $options['limit'] = $per_page;
        //======================================================================
        $users = $this->users_model->CommonGet(TBL_USERS, $options);

        if (!empty($users)) {
            $options['users'] = $users;
        }
        //======================================================================
        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $options['options'] = $options;
        }
        //======================================================================
        $options['scripts'] = $this->scripts_select2();
        $tpl = 'ad/users_list';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Chứng từ' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _get_data_from_filter() {
        $options = array();

        if ($this->is_postback()) {
            $options['search'] = $this->db->escape_str($this->input->post('search', TRUE));
            $this->phpsession->save('users_search_options', $options);
            //search with lang
            $options['lang'] = $this->input->post('lang');
        } else {
            $temp_options = $this->phpsession->get('users_search_options');
            if (is_array($temp_options)) {
                $options['search'] = $temp_options['search'];
            } else {
                $options['search'] = '';
            }
        }
//        $options['offset'] = $this->uri->segment(3);
        return $options;
    }

    //==========================================================================
    function add() {
        //modules::run('auth/auth/check_permission', $this->router->fetch_module());
        $options = array();
        if ($this->is_postback()) {
            if (!$this->_do_add())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        //======================================================================
        //$options += $this->_get_add_form_data();
        //======================================================================
        $options['header'] = 'Thêm người dùng';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = AUTH_USERS_ADMIN_BASE_URL . '/add';
        //======================================================================
        $tpl = 'ad/users_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Thêm người dùng' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _get_add_form_data() {
        $options['username'] = my_trim($this->input->post('username'));
        $options['password'] = my_trim($this->input->post('password'));
        $options['fullname'] = my_trim($this->input->post('fullname'));
        $options['email'] = my_trim($this->input->post('email'));
        $options['tel'] = my_trim($this->input->post('tel'));
        $options['products_trademark'] = $this->products_trademark_model->get_products_trademark_combo(array('products_trademark' => $this->input->post('products_trademark'), 'extra' => 'class="btn"'));

        $options['roles_combobox'] = $this->roles_model->get_roles_combo(array('roles_combobox' => $this->input->post('roles_combobox'), 'extra' => ' class="btn"'));
        $options['header'] = 'Thêm người dùng';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = AUTH_USERS_ADMIN_BASE_URL . '/add';
        return $options;
    }

    //==========================================================================
    private function _do_add() {
        $user_id = $this->input->post('user_id', TRUE);
        if ($user_id > 0) {
            $this->form_validation->set_rules('password', 'Mật khẩu', 'trim|xss_clean|max_length[50]');
        } else {
            $this->form_validation->set_rules('password', 'Mật khẩu', 'trim|required|xss_clean|max_length[50]');
        }
        //======================================================================
        $this->form_validation->set_rules('username', 'Tên đăng nhập', 'trim|required|xss_clean|max_length[30]');
        $this->form_validation->set_rules('fullname', 'Họ tên', 'trim|xss_clean|max_length[50]');
        $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|max_length[255]');
        $this->form_validation->set_rules('tel', 'Điện thoại', 'trim|xss_clean|max_length[25]');
        //======================================================================
        if ($this->form_validation->run()) {
            $password = $this->input->post('password', TRUE);
            $post_data = array(
                'username' => my_trim($this->input->post('username', TRUE)),
                'fullname' => my_trim($this->input->post('fullname', TRUE)),
                'email' => my_trim($this->input->post('email', TRUE)),
                'tel' => my_trim($this->input->post('tel', TRUE)),
                'active' => STATUS_ACTIVE,
                'level' => AUTH_LEVEL_ADMIN,
                'editor' => $this->phpsession->get('user_id'),
                'date_updated' => date('Y-m-d H:i:s'),
                'time_updated' => strtotime(date('Y-m-d')),
            );
            //==================================================================
            // Check khi sửa
            if ($user_id > 0) {
                if ($password != '') {
                    $post_data['password'] = md5($password);
                }
            } else {
                $post_data['password'] = md5($password);
            }
            //==================================================================
            $ql_all = $this->input->post('ql_all', TRUE);
            // Nếu chọn all ql
            if ($ql_all == QL_CHECKED) {
                $post_data['ql_all'] = $post_data['ql_bbh'] = $post_data['ql_goi_bh'] = $post_data['ql_kh'] = $post_data['ql_bh'] = QL_CHECKED;
                $post_data['ql_users'] = QL_CHECKED;
                $post_data['ql_bt'] = $post_data['ql_bt_nhap_hs'] = $post_data['ql_bt_xuly'] = $post_data['ql_bt_duyet'] = $post_data['ql_bt_thanhtoan'] = QL_CHECKED;
            } else {
                $post_data['ql_all'] = TRUE;
                //==============================================================
                $ql_users = $this->input->post('ql_users', TRUE);
                if ($ql_users == QL_CHECKED) {
                    $post_data['ql_users'] = QL_CHECKED;
                } else {
                    $post_data['ql_users'] = TRUE;
                }
                //==============================================================
                $ql_bbh = $this->input->post('ql_bbh', TRUE);
                if ($ql_bbh == QL_CHECKED) {
                    $post_data['ql_bbh'] = QL_CHECKED;
                } else {
                    $post_data['ql_bbh'] = TRUE;
                }
                $ql_goi_bh = $this->input->post('ql_goi_bh', TRUE);
                if ($ql_goi_bh == QL_CHECKED) {
                    $post_data['ql_goi_bh'] = QL_CHECKED;
                } else {
                    $post_data['ql_goi_bh'] = TRUE;
                }
                $ql_kh = $this->input->post('ql_kh', TRUE);
                if ($ql_kh == QL_CHECKED) {
                    $post_data['ql_kh'] = QL_CHECKED;
                } else {
                    $post_data['ql_kh'] = TRUE;
                }
                $ql_bh = $this->input->post('ql_bh', TRUE);
                if ($ql_bh == QL_CHECKED) {
                    $post_data['ql_bh'] = QL_CHECKED;
                } else {
                    $post_data['ql_bh'] = TRUE;
                }
                //==============================================================
                $ql_bt = $this->input->post('ql_bt', TRUE);
                if ($ql_bt == QL_CHECKED) {
                    $post_data['ql_bt'] = QL_CHECKED;
                } else {
                    $post_data['ql_bt'] = TRUE;
                }
                $ql_bt_nhap_hs = $this->input->post('ql_bt_nhap_hs', TRUE);
                if ($ql_bt_nhap_hs == QL_CHECKED) {
                    $post_data['ql_bt_nhap_hs'] = QL_CHECKED;
                } else {
                    $post_data['ql_bt_nhap_hs'] = TRUE;
                }
                $ql_bt_xuly = $this->input->post('ql_bt_xuly', TRUE);
                if ($ql_bt_xuly == QL_CHECKED) {
                    $post_data['ql_bt_xuly'] = QL_CHECKED;
                } else {
                    $post_data['ql_bt_xuly'] = TRUE;
                }
                $ql_bt_duyet = $this->input->post('ql_bt_duyet', TRUE);
                if ($ql_bt_duyet == QL_CHECKED) {
                    $post_data['ql_bt_duyet'] = QL_CHECKED;
                } else {
                    $post_data['ql_bt_duyet'] = TRUE;
                }
                $ql_bt_thanhtoan = $this->input->post('ql_bt_thanhtoan', TRUE);
                if ($ql_bt_thanhtoan == QL_CHECKED) {
                    $post_data['ql_bt_thanhtoan'] = QL_CHECKED;
                } else {
                    $post_data['ql_bt_thanhtoan'] = TRUE;
                }
            }
            //==================================================================
            if ($user_id > 0) {
//                echo $user_id . '///';
//                echo '<pre>';
//                print_r($post_data);
//                die;
                $this->users_model->CommonUpdate(TBL_USERS, $user_id, $post_data);
            } else {
                $post_data['creator'] = $this->phpsession->get('user_id');
                $post_data['date_created'] = date('Y-m-d H:i:s');
                $post_data['time_created'] = strtotime(date('Y-m-d'));
                //==============================================================
                $this->users_model->CommonCreat(TBL_USERS, $post_data);
            }
            //==================================================================
            redirect(AUTH_USERS_ADMIN_BASE_URL . '/' . $lang);
        }
        return FALSE;
    }

    //==========================================================================
    private function _get_posted_data() {
        $post_data = array(
            'username' => my_trim($this->input->post('username', TRUE)),
//            'password' => my_trim($this->input->post('password', TRUE)),
            'fullname' => my_trim($this->input->post('fullname', TRUE)),
            'email' => my_trim($this->input->post('email', TRUE)),
            'tel' => my_trim($this->input->post('tel', TRUE)),
            'role_id' => my_trim($this->input->post('roles_combobox', TRUE)),
            'active' => STATUS_ACTIVE,
            'joined_date' => date('Y-m-d H:i:s', now()),
            'editor' => $this->phpsession->get('user_id'),
            'trademark_id' => $this->input->post('products_trademark', TRUE),
        );
        return $post_data;
    }

    //==========================================================================
    function edit() {
        //modules::run('auth/auth/check_permission', $this->router->fetch_module());
        $options = array();

        if (!$this->is_postback())
            redirect(AUTH_USERS_ADMIN_BASE_URL);
        //======================================================================
        if ($this->is_postback() && !$this->input->post('from_list')) {
            if (!$this->_do_add())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        //======================================================================
        $user_id = $this->input->post('id');
        // Lấy thông tin user
        $data_get = array(
            'id' => $user_id,
            'get_row' => TRUE,
        );
        $user = $this->users_model->CommonGet(TBL_USERS, $data_get);
        if (is_object($user)) {
            $options['user'] = $user;
        }
        //$options += $this->_get_edit_form_data();
        //======================================================================
        $options['header'] = 'Sửa người dùng';
        $options['button_name'] = 'Sửa người dùng';
        $options['submit_uri'] = AUTH_USERS_ADMIN_BASE_URL . '/edit';
        //======================================================================
        $tpl = 'ad/users_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Sửa người dùng' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================

    public function delete() {
        modules::run('auth/auth/check_permission', $this->router->fetch_module());
        $options = array();
        if ($this->is_postback()) {
            $id = $this->input->post('id');
            $this->users_model->delete($id);
            $options['warning'] = 'Đã xóa thành công';
        }
        $lang = $this->phpsession->get('users_lang');
        redirect(AUTH_USERS_ADMIN_BASE_URL . '/' . $lang);
    }

    function change_status() {
        modules::run('auth/auth/check_permission', $this->router->fetch_module());
        $id = $this->input->post('id');
        $user = $this->users_model->get_users(array('id' => $id));
        $status = $user->active == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
        $this->users_model->update(array('id' => $id, 'active' => $status));
    }

}
?>