<?php

class Baocao extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->kh_check_login();

        $this->_layout = 'layout/custom_layout';
        $this->_view_data = array();
        $this->load->model('customers/customers_model');
        $this->load->model('orders/orders_model');
    }

    function index() {
        $data = array();

        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        if ($ss_kh_phanloai_kh != DOANHNGHIEP) {
            redirect(URL_KH_DASHBOARD);
        }
        //======================================================================
        // Lấy hsbt
        $data_get = array(
            'join_' . TBL_PRODUCTS . '_dbh' => TRUE,
            'BH_kh_mbh' => $ss_kh_id,
            'get_bt_thanhtoan' => TRUE,
        );
        $bt = $this->orders_model->get_orders_by_options($data_get);

        if (!empty($bt)) {
            $data['bt'] = $bt;
        }
        //======================================================================
        $tpl = 'baocao/baocao_theo_phanloai_bt';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function top10hsbt() {
        $data = array();

        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        if ($ss_kh_phanloai_kh != DOANHNGHIEP) {
            redirect(URL_KH_DASHBOARD);
        }
        //======================================================================
        // Lấy mã bệnh
        $this->load->model('products/products_origin_model');
        $ma_benh = $this->products_origin_model->get_products_origin();
        if (!empty($ma_benh)) {
            $data['a_ma_benh'] = $ma_benh;
        }
        //end
        //======================================================================
        // Lấy hsbt
        $data_get = array(
            'join_' . TBL_PRODUCTS . '_dbh' => TRUE,
            'BH_kh_mbh' => $ss_kh_id,
            'order_by' => TBL_ORDERS . '.so_tien_dbt DESC ',
            'get_bt_thanhtoan' => TRUE,
            //'parent_id' => FALSE,
            'limit' => 10,
        );
        $bt = $this->orders_model->get_orders_by_options($data_get);

        if (!empty($bt)) {
            $data['bt'] = $bt;
        }
        //======================================================================
        $tpl = 'baocao/baocao_top10hsbt';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function top10csyt() {
        $data = array();

        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        if ($ss_kh_phanloai_kh != DOANHNGHIEP) {
            redirect(URL_KH_DASHBOARD);
        }
        //======================================================================
        // Lấy csyt
        $data_get = array();
        $csyt = $this->orders_model->CommonGet(TBL_CSYT, $data_get);
        if (!empty($csyt)) {
            $data['a_csyt'] = $csyt;
        }
        //end
        //======================================================================
        // Lấy hsbt
        $data_get = array(
            'join_' . TBL_PRODUCTS . '_dbh' => TRUE,
            'BH_kh_mbh' => $ss_kh_id,
            'order_by' => TBL_ORDERS . '.st_dbt_t DESC, ' . TBL_ORDERS . '.st_ycbt_t DESC',
            'get_bt_thanhtoan' => TRUE,
            //'parent_id' => FALSE,
            'limit' => 10,
        );
        $bt = $this->orders_model->get_orders_by_options($data_get);

        if (!empty($bt)) {
            $data['bt'] = $bt;
        }
        //======================================================================
        $tpl = 'baocao/baocao_top10hsbt';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function solieu_bt_chung() {
        $data = array();

        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        if ($ss_kh_phanloai_kh != DOANHNGHIEP) {
            redirect(URL_KH_DASHBOARD);
        }
        //======================================================================
        // Filter
        $G_bc = $_GET['bc'];
        //======================================================================
        // Lấy gói BH
        $data_get = array(
            'where' => array(
                'dn_mbh' => $ss_kh_id,
            )
        );
        $goi_bh = $this->customers_model->CommonGet(TBL_PRODUCTS_SIZE, $data_get);
        if (!empty($goi_bh)) {
            $data['a_goi_bh'] = $goi_bh;
        }
        //======================================================================
        // Lấy kh dbh của dn
        $data_get = array(
            'where' => array(
                'kh_mbh' => $ss_kh_id,
            ),
        );
        //----------------------------------------------------------------------
        if ($G_bc != '' && $_GET['bc'] > 0) {
            $data_get['where']['mqh'] = $G_bc;
        }
        //----------------------------------------------------------------------
        $kh_dbh = $this->customers_model->CommonGet(TBL_PRODUCTS, $data_get);
        if (!empty($kh_dbh)) {
            $data['a_kh_dbh'] = $kh_dbh;
        }
        //======================================================================
        // Lấy hsbt
        $data_get = array(
            'join_' . TBL_PRODUCTS . '_dbh' => TRUE,
            //'join_' . TBL_PRODUCTS_SIZE => TRUE,
            'BH_kh_mbh' => $ss_kh_id,
        );
        //----------------------------------------------------------------------
        if ($G_bc != '' && $_GET['bc'] > 0) {
            $data_get['mqh'] = $G_bc;
        }
        //----------------------------------------------------------------------
        $bt = $this->orders_model->get_orders_by_options($data_get);
        if (!empty($bt)) {
            $data['bt'] = $bt;
        }
        //======================================================================
        $G_bc = isset($_GET['bc']) ? $_GET['bc'] : '';
        if ($G_bc == DN_NV) {
            $tpl = 'baocao/baocao_solieu_bt_chung_nv';
        } elseif ($G_bc == DN_NT) {
            $tpl = 'baocao/baocao_solieu_bt_chung_nt';
        } else {
            $tpl = 'baocao/baocao_solieu_bt_chung';
        }

        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function bcnb() {
        $data = array();

        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        if ($ss_kh_phanloai_kh != DOANHNGHIEP) {
            redirect(URL_KH_DASHBOARD);
        }
        //======================================================================
        // Lấy mã bệnh
        $data_get = array();
        $ma_benh = $this->orders_model->CommonGet(TBL_MABENH, $data_get);
        if (!empty($ma_benh)) {
            $data['a_ma_benh'] = $ma_benh;
        }
        //end
        //======================================================================
        // Lấy hsbt
        $data_get = array(
            'join_' . TBL_PRODUCTS . '_dbh' => TRUE,
            'BH_kh_mbh' => $ss_kh_id,
            'get_bt_thanhtoan' => TRUE,
        );
        $bt = $this->orders_model->get_orders_by_options($data_get);
        if (!empty($bt)) {
            $data['bt'] = $bt;
        }
        //======================================================================
        $tpl = 'baocao/baocao_bcnb';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
}

?>