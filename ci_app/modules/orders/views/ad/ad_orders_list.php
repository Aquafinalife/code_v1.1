<?php
// Check permission
$ss_ql_all = $this->phpsession->get('ss_ql_all');
$ss_ql_bt = $this->phpsession->get('ss_ql_bt');
$ss_ql_bt_nhap_hs = $this->phpsession->get('ss_ql_bt_nhap_hs');
$ss_ql_bt_xuly = $this->phpsession->get('ss_ql_bt_xuly');
$ss_ql_bt_duyet = $this->phpsession->get('ss_ql_bt_duyet');
$ss_ql_bt_thanhtoan = $this->phpsession->get('ss_ql_bt_thanhtoan');
//==============================================================================
$a_phanloai_bt = get_a_phanloai_bt();
$a_orders_status = $a_orders_status_filter = get_a_orders_status();
$a_bt_huy = get_a_bt_huy();
//==============================================================================
// Permission filter
if ($ss_ql_all != QL_CHECKED && $ss_ql_bt_thanhtoan != QL_CHECKED) {
    if ($ss_ql_bt_duyet == QL_CHECKED) {
        unset($a_orders_status_filter[BT_THANHTOAN]);
        unset($a_orders_status_filter[BT_DATRATIEN]);
    } elseif ($ss_ql_bt_xuly == QL_CHECKED) {
        unset($a_orders_status_filter[BT_DADUYET]);
        unset($a_orders_status_filter[BT_THANHTOAN]);
        unset($a_orders_status_filter[BT_DATRATIEN]);
    } elseif ($ss_ql_bt_nhap_hs == QL_CHECKED) {
        unset($a_orders_status_filter[BT_PHEDUYET]);
        unset($a_orders_status_filter[BT_DADUYET]);
        unset($a_orders_status_filter[BT_THANHTOAN]);
        unset($a_orders_status_filter[BT_DATRATIEN]);
    }
}
//==============================================================================
?>
<div class="page_header">
    <h1 class="fleft">Danh sách bồi thường</h1>
    <small class="fleft">"Thêm/sửa bồi thường"</small>
    <span class="fright"><a class="button add" href="<?php echo ADMIN_BASE_URL; ?>"><em>&nbsp;</em>Đóng</a></span>
    <span class="fright"><a class="button add" href="<?php echo ORDERS_ADMIN_BASE_URL; ?>/add" ><em>&nbsp;</em>Thêm đơn bồi thường</a></span>
    <br class="clear"/>
</div>
<?php
$selected_df = 'selected="selected"';
echo form_open('', array('id' => 'submit_form'), array('id' => 0, 'from_list' => TRUE));
echo isset($uri) ? form_hidden('uri', $uri) : NULL;
echo form_hidden('back_url', ORDER_ADMIN_BASE_URL);
echo form_close();
?>
<div class="form_content">
    <div class="filter col-sm-12">
        <form action="<?php echo site_url('dashboard/orders') ?>" method="get">
            <?php
            $filter_kh_dbt = isset($_GET['kh_dbt']) ? $_GET['kh_dbt'] : '';
            $filter_hsbt = isset($_GET['hsbt']) ? $_GET['hsbt'] : '';
            $filter_status = isset($_GET['status']) ? $_GET['status'] : '';
            $filter_pl_bt = isset($_GET['pl_bt']) ? $_GET['pl_bt'] : '';
            $filter_user = isset($_GET['ft_user']) ? $_GET['ft_user'] : '';
//            echo $filter_kh_dbt.'//'.$filter_status.'???'.$filter_pl_bt;
//            die;
            ?>
            <div class="col-lg-2 col-sm-2 col-xs-12">
                <input type="text" name="kh_dbt" value="<?php echo $filter_kh_dbt ?>" placeholder="CMTND / Hộ Chiếu..."/>
            </div>
            <div class="col-sm-2 col-xs-12">
                <input type="text" name="hsbt" value="<?php echo $filter_hsbt ?>" placeholder="Mã HSBT / Case HSBT..."/>
            </div>
            <!--=============================================================-->
            <div class="col-sm-3 col-xs-12">
                <select name="status" class="js-example-disabled-results">
                    <option value="">-- Trạng thái bồi thường --</option>
                    <?php
                    if (isset($a_orders_status_filter) && !empty($a_orders_status_filter)) {
                        foreach ($a_orders_status_filter as $k => $v) {
                            $selected = ($filter_status == $k) ? $selected_df : '';
                            ?>
                            <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <!--=============================================================-->
            <div class="col-sm-3 col-xs-12">
                <select name="pl_bt" class="js-example-disabled-results">
                    <option value="">-- Loại bồi thường --</option>
                    <?php
                    if (isset($a_phanloai_bt) && !empty($a_phanloai_bt)) {
                        foreach ($a_phanloai_bt as $k => $v) {
                            $selected = ($filter_pl_bt == $k) ? $selected_df : '';
                            ?>
                            <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <!--=============================================================-->
            <div class="clearfix"></div>
            <div class="col-sm-3 col-xs-12 mt20">
                <select name="ft_user" class="js-example-disabled-results">
                    <option value="">-- Người dùng --</option>
                    <?php
                    if (isset($users) && !empty($users)) {
                        foreach ($users as $index) {
                            $id = $index->id;
                            $fullname = $index->fullname;
                            $selected = ($filter_user == $id) ? $selected_df : '';
                            ?>
                            <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $fullname ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <!--=============================================================-->            
            <span class="fright"><a class="button fr" href="/dashboard/orders/export"><em>&nbsp;</em>Xuất excel</a></span>
            <input type="submit" name="submit" value="Tìm kiếm" class="btn fr mr20" />
        </form>

    </div>
    <!--end-->
    <table class="list" style="width: 100%; margin-bottom: 10px;">
        <?php $this->load->view('powercms/message'); ?>
        <tr>
            <th class="left" style="width: 10%">HSBT</th>
            <th class="left" style="width: 10%">MÃ CASE BT</th>
            <th class="left" style="width: 20%">KHÁCH HÀNG</th>
            <th class="left" style="width: 20%">CMT</th>
            <th class="left" style="width: 20%">NGUYÊN NHÂN</th>
            <th class="center" style="width: 10%">TIỀN YÊU CẦU BT</th>
            <th class="center" style="width: 2%">TIỀN BT</th>
            <th class="center" style="width: 10%">NGÀY NHẬN HS</th>
            <th class="center" style="width: 10%">TRẠNG THÁI</th>
            <th class="center" style="width: 10%">USERS</th>
            <th class="center" style="width: 10%">CHỨC NĂNG</th>
        </tr>

        <?php
        if (isset($bt) && !empty($bt)) {
//            echo '<pre>';
//            print_r($bt);
//            die;
            $stt = 0;
            foreach ($bt as $index):
                $bt_user1_name = '';
                //--------------------------------------------------------------
                $id = $index->id;
                $bt_code = $index->bt_code;
                $case = $index->case;
                $congty_canhan = $index->congty_canhan;
                $mst_cmt = $index->mst_cmt;
                //--------------------------------------------------------------
                $user1 = $index->user1;
                if (isset($users) && !empty($users)) {
                    foreach ($users as $index1) {
                        $us_id = $index1->id;
                        $us_fullname = $index1->fullname;
                        if ($us_id == $user1) {
                            $bt_user1_name = $us_fullname;
                            break;
                        }
                    }
                }
                //--------------------------------------------------------------
                $so_tien_ycbt = get_price_in_vnd($index->so_tien_ycbt);
                $so_tien_dbt = get_price_in_vnd($index->so_tien_dbt);
                $quyenloi_bt = $index->quyenloi_bt;
                $quyenloi_bt_txt = get_ten_qlbt($quyenloi_bt);
                $order_status = $index->order_status;
                $order_status_txt = get_orders_status($order_status);
                $date_nhan_bt = substr($index->date_nhan_bt, 0, 10);
                $style = $stt++ % 2 == 0 ? 'even' : 'odd';
                //end
                ?>
                <tr class="<?php echo $style ?>">
                    <td><?php echo $bt_code; ?></td>
                    <td><?php echo $case; ?></td>
                    <td><?php echo $congty_canhan ?></td>
                    <td style="white-space:nowrap;"><?php echo $mst_cmt ?></td>
                    <td style="white-space:nowrap;"><?php echo $quyenloi_bt_txt; ?></td>
                    <td class="center" style="white-space:nowrap;"><?php echo $so_tien_ycbt ?></td>
                    <td style="white-space:nowrap;"><?php echo $so_tien_dbt ?></td>
                    <td class="center" style="white-space:nowrap;"><?php echo $date_nhan_bt ?></td>
                    <td class="center" style="white-space:nowrap;"><?php echo $order_status_txt ?></td>
                    <td class="center" style="white-space:nowrap;"><?php echo $bt_user1_name ?></td>
                    
                    <td class="center" style="white-space:nowrap;" class="action">
                        <?php
                        if (in_array($order_status, $a_bt_huy)) {
                            ?>
                            <a class="edit1" title="Hủy bồi thường" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id ?>, '/dashboard/orders/huy-bt', 'huy_bt');">
                                <span style="float: left;margin-left: 10px;margin-right: 10px;" class="glyphicon glyphicon-trash text-danger"></span>
                            </a>
                            <?php
                        }
                        ?>
                        <?php
                        //if ($order_status == BT_CHODUYET) {
                        if ($order_status != BT_CHOBOSUNG) {
                            if ($ss_ql_all == QL_CHECKED || $ss_ql_bt_nhap_hs == QL_CHECKED) {
                                ?>
                                <a class="edit1 mr15" title="Sửa thông tin bồi thường" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id ?>, '/dashboard/orders/edit-bt', 'edit_bt');">
                                    <span style="float: left;margin-left: 10px;margin-right: 0px;"  class="glyphicon glyphicon-edit text-primary"></span>
                                </a>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="" style="margin-right: 30px"></div>
                            <?php
                        }
                        ?>                        
                        <a class="edit_" title="Xử lý bồi thường" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id ?>, '<?php echo ORDERS_ADMIN_EDIT_URL; ?>', 'edit');">
                            <span style="float: left;margin-left: 10px;margin-right: 0px;"  class="glyphicon glyphicon-list-alt text-success"></span>
                        </a>
                        <!--<a class="del" title="Xóa bồi thường" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id ?>, '<?php echo ORDERS_ADMIN_DELETE_URL; ?>', 'delete');"><em>&nbsp;</em></a>-->
                        <!--<a class="excel" title="Xuất excel" href="javascript:void(0);" onclick="submit_action_admin(<?php // echo $index->id                                   ?>,'<?php // echo ORDERS_ADMIN_EXPORT;                                   ?>','export');"><em>&nbsp;</em></a>-->
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php } ?>
        <?php $left_page_links = 'Tổng số: ' . $total_rows . ' bồi thường</span>'; ?>
        <tr class="list-footer">
            <th colspan="11">
                <div style="float:left; margin-top: 9px;"><?php echo $left_page_links; ?></div>
                <div class="col-sm-12 text-center">
                    <div class="pagination">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div>
            </th>
        </tr>
    </table>
    <br class="clear"/>&nbsp;
</div>
