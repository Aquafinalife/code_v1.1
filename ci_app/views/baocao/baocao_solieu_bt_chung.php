<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
$a_bt_thanhtoan = get_a_bt_thanhtoan();
//end
//==============================================================================
$G_bc = isset($_GET['bc']) ? $_GET['bc'] : '';
if ($G_bc == DN_NV) {
    $header = 'Báo cáo phân tích số liệu bồi thường NHÂN VIÊN';
} elseif ($G_bc == DN_NT) {
    $header = 'Báo cáo phân tích số liệu bồi thường NGƯỜI THÂN';
} else {
    $header = 'Báo cáo phân tích số liệu bồi thường CHUNG';
}
?>
<div class="box_filter" style="margin-bottom: 20px">
</div>
<table id="_table-edit" class="table table-bordered table-hover">
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> <?php echo $header ?>:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th>Chương trình bảo hiểm</th>
            <th>Tổng số NĐBH</th>
            <th>Tổng số HSBT</th>
            <th>Phí BH</th>
            <th>Số tiền bồi thường</th>
            <th>Tỷ lệ bồi thường (%)</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($a_goi_bh) && !empty($a_goi_bh)) {
            $tt_kh_dbh = $tt_hsbt = $tt_phi_bh = $tt_st_dbt = 0;
            $tt_st_dbt_cd = $tt_st_ycbt_cd = 0;

            $ttt_st_dbt = $ttt_hsbt = 0;

            foreach ($a_goi_bh as $index1) {

                $GOI_id = $index1->id;
                $GOI_name = $index1->name;
                $goi_gia_tien = $index1->gia_tien;
                //==============================================================
                $t_kh_dbh = $t_hsbt = $t_phi_bh = $t_st_dbt = 0;
                $tt_hsbt_cd = 0;

                //$a_kh_dbh = array();
                //==============================================================

                if (isset($a_kh_dbh) && !empty($a_kh_dbh)) {
                    foreach ($a_kh_dbh as $index2) {
                        $KH_GOI_ID = $index2->goi_bh;
                        //------------------------------------------------------
                        if ($KH_GOI_ID == $GOI_id) {
                            $t_kh_dbh = $t_kh_dbh + 1;
                        }
                    }
                }
                //==============================================================
                if (isset($bt) && !empty($bt)) {
                    $j = 1;
                    foreach ($bt as $index) {
                        $BT_id = $index->id;
                        $KH_id = $index->KH_id;
                        $so_tien_ycbt = $index->so_tien_ycbt;
                        $so_tien_dbt = $index->so_tien_dbt;
                        $bt_status = $index->order_status;
                        //------------------------------------------------------
                        $BH_GOI_ID = $index->BH_GOI_ID;
                        //======================================================
                        if (in_array($bt_status, $a_bt_thanhtoan)) {
                            if ($GOI_id == $BH_GOI_ID) {
                                $tt_hsbt = $tt_hsbt + 1;
                                $t_hsbt = $t_hsbt + 1;
                                //----------------------------------------------
                                $t_st_dbt = $t_st_dbt + (int) $so_tien_dbt;
                                $tt_st_dbt = $tt_st_dbt + (int) $so_tien_dbt;
                                //----------------------------------------------
                            }
                        } else {
                            $tt_hsbt_cd = $tt_hsbt_cd + 1;
                            $tt_st_ycbt_cd = $tt_st_ycbt_cd + (int) $so_tien_ycbt;
                        }
                        ?>

                        <?php
                    }
                    //==========================================================
                    $t_phi_bh = $t_kh_dbh * $goi_gia_tien;
                    $tt_phi_bh = $tt_phi_bh + $t_phi_bh;
                    //==========================================================
                    //==========================================================
                    $ttt_st_dbt = $tt_st_ycbt_cd + $tt_st_dbt;
                    //$ttt_hsbt = $tt_hsbt + $tt_hsbt_cd;
                }
                ?>
                <tr>
                    <td><?php echo $GOI_name ?></td>
                    <td><?php echo $t_kh_dbh ?></td>
                    <td><?php echo $t_hsbt ?></td>
                    <td><?php echo get_price_in_vnd($t_phi_bh) ?></td>
                    <td><?php echo get_price_in_vnd($t_st_dbt) ?></td>
                    <td><?php echo round((($t_st_dbt / $t_phi_bh) * 100), 2) . '%'; ?></td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td><strong>Tổng cộng</strong></td>
                <td><strong><?php echo count($a_kh_dbh) ?></strong></td>
                <td><strong><?php echo $tt_hsbt ?></strong></td>
                <td><strong><?php echo get_price_in_vnd($tt_phi_bh) ?></strong></td>
                <td><strong><?php echo get_price_in_vnd($tt_st_dbt) ?></strong></td>
                <td><strong><?php echo round((($tt_st_dbt / $tt_phi_bh) * 100), 2) . '%'; ?></strong></td>
            </tr>
            <tr>
                <td><strong>Hồ sơ chờ duyệt</strong></td>
                <td></td>
                <td><strong><?php echo $tt_hsbt_cd ?></strong></td>
                <td>-</td>
                <td>0</td>
                <td>-</td>
            </tr>
            <tr>
                <td><strong>Tổng cộng (Gồm cả HSCD)</strong></td>
                <td></td>
                <td><strong><?php echo count($bt) ?></strong></td>
                <td><strong><?php echo get_price_in_vnd($tt_phi_bh) ?></strong></td>
                <td><strong><?php echo get_price_in_vnd($ttt_st_dbt) ?></strong></td>
                <td><strong><?php echo round((($ttt_st_dbt / $tt_phi_bh) * 100), 2) . '%'; ?></strong></td>
            </tr>
            <?php
        } else {
            ?>
            <tr>
                <td class="text-danger" colspan="6">Chưa có dữ liệu</td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<div class="col-sm-12 text-center">
    <div class="pagination">
        <?php // echo $this->pagination->create_links();      ?>
    </div>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>