$(document).ready(function () {
    $('body').on("click", '.btn_check_ctbs', function () {
        var id = $(this).attr('id');
        if (id > 0) {
            $.ajax({
                type: "post",
                url: "/ajax/ajax_get_ctbs",
                data: {
                    id: id,
                },
                success: function (res) {
                    $('.modal_bt').html(res);
                    $('.bs-example-modal-lg').modal('show');
                }
            });
        }
    });
});