<?php

class Products_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        //modules::run('auth/auth/validate_login', $this->router->fetch_module());
        $this->_layout = 'admin_ui/layout/main';
        $this->_view_data['url'] = PRODUCTS_ADMIN_BASE_URL;
        $this->ad_check_login();
        //======================================================================
        $ss_ql_all = $this->phpsession->get('ss_ql_all');
        $ss_ql_bh = $this->phpsession->get('ss_ql_bh');
        if ($ss_ql_all != QL_CHECKED) {
            if ($ss_ql_bh != QL_CHECKED) {
                redirect('dashboard');
            }
        }
    }

    //==========================================================================
    function browse($offset = 0, $options = array()) {
        $data = array();
        $options['user_id'] = $this->phpsession->get('user_id');
        $options['join_' . TBL_CUSTOMERS] = TRUE;
        //end
        //======================================================================
        // Filter
        $key = isset($_GET['key']) ? $_GET['key'] : '';
        if ($key != '') {
            $filter_key_search = "(`" . DB_PREFIX . TBL_CUSTOMERS . "`.`congty_canhan` LIKE '%" . $key . "%'";
            $filter_key_search .= " OR `" . DB_PREFIX . TBL_CUSTOMERS . "`.`mst_cmt` LIKE '%" . $key . "%'";
            $filter_key_search .= " OR `" . DB_PREFIX . TBL_PRODUCTS . "`.`sohopdong` LIKE '%" . $key . "%')";

            $options['like'] = $filter_key_search;
        }
        //======================================================================
        if (isset($_GET['phanloai_kh']) && $_GET['phanloai_kh'] != '') {
            $options['phanloai_kh'] = $_GET['phanloai_kh'];
        }
        // Filter doanh nghiệp
        if ((isset($_GET['dn']) && $_GET['dn'] != '') || (isset($_GET['phanloai_kh']) && $_GET['phanloai_kh'] == DOANHNGHIEP)) {
            // Lấy thông tin kh
            $kh = $this->customers_model->get_kh();
//            echo '<pre>';
//            print_r($kh);
//            die;
            if (!empty($kh)) {
                $data['kh'] = $kh;
            }
            //==================================================================
            $dn_id = $_GET['dn'];
            $options['kh_mbh'] = $dn_id;
            $str_dn_id = $dn_id . ',';
            // Lấy các dn
            //==================================================================
            if (!empty($kh)) {
                $option_dn = '';
                foreach ($kh as $index) {
                    $id = $index->id;
                    $congty_canhan = $index->congty_canhan;
                    $parent_id = $index->parent_id;
                    $phanloai_kh = $index->phanloai_kh;
                    $selected = ($_GET['dn'] == $id) ? 'selected="selected"' : '';
                    if ($parent_id == FALSE && $phanloai_kh == DOANHNGHIEP) {
                        $option_dn .= '<option ' . $selected . ' value="' . $id . '">' . $congty_canhan . '</option>';
                    }
                }
                $data['selectbox_dn'] = '<select type="2" selectbox_name="dn" name="dn" class="js-example-disabled-results" id="filter_doanhnghiep">
                            <option value="">Tất cả doanh nghiệp BH</option>
                            ' . $option_dn . '
                        </select>';
            }
            //end
            //==================================================================
            if ((isset($_GET['cn']) && $_GET['cn'] != '') || (isset($_GET['dn']) && $_GET['dn'] != '')) {
                if (isset($_GET['dn']) && $_GET['dn'] != '') {
                    $dn_id = $_GET['dn'];
                }
                if (isset($_GET['cn']) && $_GET['cn'] != '') {
                    $options['kh_mbh'] = $_GET['cn'];
                }
                // Lấy các chi nhánh
                if (!empty($kh)) {
                    $option_cn = '';
                    foreach ($kh as $index) {
                        $id = $index->id;
                        $congty_canhan = $index->congty_canhan;
                        $parent_id = $index->parent_id;
                        $selected = ($_GET['cn'] == $id) ? 'selected="selected"' : '';
                        if ($dn_id == $parent_id) {
                            $option_cn .= '<option ' . $selected . ' value="' . $id . '">' . $congty_canhan . '</option>';
                        }
                    }
                }
                $data['selectbox_cn'] = '<select type="2" selectbox_name="cn" name="cn" class="js-example-disabled-results" id="filter_chinhanh">
                            <option value="">-- Các công ty con/Chi nhánh BH --</option>
                            ' . $option_cn . '
                        </select>';
            }
            //==================================================================
        }
        //end
        //======================================================================
        // pagination
        $per_page = LIMIT_DEFAULT;
        // Đếm tổng số bt
        $total_rows = $this->products_model->count_products_by_options($options);
        //$total_rows = 20;
        $data['total_rows'] = $total_rows;
        //end
        //======================================================================
        $paging_config = array(
            'base_url' => site_url('dashboard/') . '/' . $this->uri->segment(2),
            'total_rows' => $total_rows,
            'per_page' => $per_page,
            'uri_segment' => 3,
            'use_page_numbers' => TRUE,
            'first_link' => __('IP_paging_first'),
            'last_link' => __('IP_paging_last'),
            'num_links' => 1,
        );
        if (isset($_GET) && $_GET != '') {
            $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($paging_config);
        $options['offset'] = ($offset > 0) ? ($offset - 1) * ($per_page) : $offset;
        $options['limit'] = $per_page;
        //======================================================================
        $bh = $this->products_model->get_products_by_options($options);
//        echo '<pre>';
//        print_r($bh);
//        die;
        if (!empty($bh)) {
            $data['bh'] = $bh;
        }
        $data['total_rows'] = $total_rows;
        $data['total_pages'] = $total_pages;
        //======================================================================
        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $options['options'] = $options;
        }
        $options['scripts'] = $this->scripts_select2();
        //======================================================================
        $tpl = 'ad/bh/bh_list';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Quản lý bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function add() {
        $options = array();

        if ($this->is_postback()) {
            if (!$this->_do_add_products())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        //$options += $this->_get_add_products_form_data();

        $options += $this->_get_products_form_data();
        // end
        //======================================================================
        // Chuan bi du lieu chinh de hien thi
        $options['submit_uri'] = PRODUCTS_ADMIN_ADD_URL;
        $options['submit_uri_cus'] = CUSTOM_ADMIN_BASE_URL . '/add_from_pro';
        $options['header'] = 'Thêm thông tin bảo hiểm';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['scripts'] = $this->scripts_select2();
        //======================================================================
        $tpl = 'ad/bh/bh_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Thêm đơn bảo hiểm' . DEFAULT_TITLE_SUFFIX;

        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function scripts() {
        $scripts = '<script type="text/javascript">
                    $(".js-example-disabled-results").select2();
                    RunDatePicker();
                    </script>';
        return $scripts;
    }

    //==========================================================================
    private function _get_products_form_data($options = array()) {
        // Lấy thông tin các dn bán BH
        $data_get = array(
            'parent_id' => FALSE,
        );
        $dn_bbh = $this->products_trademark_model->get_products_trademark($data_get);
        if (!empty($dn_bbh)) {
            $options['a_dn_bbh'] = $dn_bbh;
        }
        //end
        //======================================================================
        // Lấy thông tin khách hàng
        $kh = $this->customers_model->get_kh();
        if (!empty($kh)) {
            $options['kh'] = $kh;
        }
        //end
        //======================================================================
        // Lấy thông tin bh
        $bh = $this->products_model->get_products_by_options();
        if (!empty($bh)) {
            $a_cn_dbh = array();
            foreach ($bh as $index) {
                $cn_dbh_id = $index->kh_dbh;
                $a_cn_dbh[] = $cn_dbh_id;
            }
            $options['a_cn_dbh'] = array_unique($a_cn_dbh);
        }
        //======================================================================
        return $options;
    }

    //==========================================================================
    private function _do_add_products() {

        $this->form_validation->set_rules('kh_mbh', 'Bên mua BH', 'required|trim|xss_clean');
        $this->form_validation->set_rules('kh_dbh', 'Bên được BH', 'required|trim|xss_clean');
        $this->form_validation->set_rules('mqh', 'Mối quan hệ', 'required|trim|xss_clean');
        $this->form_validation->set_rules('dn_bbh', 'Công ty Bảo Hiểm', 'required|trim|xss_clean');
        $this->form_validation->set_rules('cn_bbh', 'Chi nhánh', 'trim|xss_clean');
        $this->form_validation->set_rules('goi_bh', 'Chọn gói BH', 'required|trim|xss_clean');
        $this->form_validation->set_rules('sohopdong', 'Số hợp đồng bảo hiểm', 'required|trim|xss_clean');
        $this->form_validation->set_rules('giay_chungnhan', 'Giấy chứng nhận', 'trim|xss_clean');
        $this->form_validation->set_rules('ngay_hieuluc', 'Ngày hiệu lực', 'required|trim|xss_clean');
        $this->form_validation->set_rules('ngay_batdau', 'Ngày bắt đầu', 'required|trim|xss_clean');
        $this->form_validation->set_rules('ngay_ketthuc', 'Ngày kết thúc', 'required|trim|xss_clean');
        $this->form_validation->set_rules('ngay_thanhtoan', 'Ngày thanh toán', 'trim|xss_clean');
        $this->form_validation->set_rules('daily', 'Bên môi giới, đại lý', 'trim|xss_clean');
        $this->form_validation->set_rules('ghichu', 'Ghi chú bảo hiểm', 'trim|xss_clean');
        //======================================================================
        if ($this->form_validation->run($this)) {
            $ngay_batdau = $this->input->post('ngay_batdau', TRUE);
            $ngay_hieuluc = $this->input->post('ngay_hieuluc', TRUE);
            $ngay_ketthuc = $this->input->post('ngay_ketthuc', TRUE);
            $ngay_thanhtoan = $this->input->post('ngay_thanhtoan', TRUE);
            //==================================================================
            $data_bh = array(
                'kh_mbh' => $this->input->post('kh_mbh', TRUE),
                'kh_dbh' => $this->input->post('kh_dbh', TRUE),
                'mqh' => $this->input->post('mqh', TRUE),
                'dn_bbh' => $this->input->post('dn_bbh', TRUE),
                'cn_bbh' => $this->input->post('cn_bbh', TRUE),
                'goi_bh' => $this->input->post('goi_bh', TRUE),
                'sohopdong' => $this->input->post('sohopdong', TRUE),
                'giay_chungnhan' => $this->input->post('giay_chungnhan', TRUE),
                'ngay_batdau' => $ngay_batdau,
                'time_batdau' => strtotime($ngay_batdau),
                'ngay_hieuluc' => $ngay_hieuluc,
                'time_hieuluc' => strtotime($ngay_hieuluc),
                'ngay_ketthuc' => $ngay_ketthuc,
                'time_ketthuc' => strtotime($ngay_ketthuc),
                'ngay_thanhtoan' => $ngay_thanhtoan,
                'time_thanhtoan' => strtotime($ngay_thanhtoan),
                'daily' => $this->input->post('daily', TRUE),
                'ghichu' => $this->input->post('ghichu', TRUE),
                'user_id' => $this->phpsession->get('user_id'),
            );
            //==================================================================
            $time_hieuluc = strtotime($ngay_hieuluc);
            $time_now = strtotime(date('d-m-Y'));
            if ($time_hieuluc <= $time_now) {
                $data_bh['status'] = BH_CO_HIEULUC;
            } else {
                $data_bh['status'] = BH_CHUA_CO_HIEULUC;
            }
            $time_ketthuc = $time_hieuluc + 365 * 24 * 60 * 60;
            $date_ketthuc = date('Y-m-d', $time_ketthuc);
            //$data_bh['ngay_ketthuc'] = $date_ketthuc;
            //$data_bh['time_ketthuc'] = $time_ketthuc;
            //==================================================================
            $bh_id = $this->input->post('bh_id', TRUE);
            //==================================================================
            // Khi sửa
            if ($bh_id > 0) {
                $data_bh['id'] = $bh_id;
                $data_bh['date_updated'] = date('Y-m-d H:i:s');
                $data_bh['time_updated'] = strtotime(date('d-m-Y'));
                $data_bh['editor'] = $this->phpsession->get('user_id');
                $this->products_model->update($data_bh);
            } else {
                // Khi thêm
                //$data_bh['time_batdau'] = strtotime($ngay_batdau);
                //$data_bh['time_hieuluc'] = strtotime($ngay_hieuluc);
                //$data_bh['time_thanhtoan'] = strtotime($ngay_thanhtoan);
                //==============================================================
                $data_bh['date_created'] = date('Y-m-d H:i:s');
                $data_bh['time_created'] = strtotime(date('d-m-Y'));
                $data_bh['date_updated'] = date('Y-m-d H:i:s');
                $data_bh['time_updated'] = strtotime(date('d-m-Y'));
                $data_bh['creator'] = $this->phpsession->get('user_id');
                //$data_bh['editor'] = $this->phpsession->get('user_id');
//                echo '<pre>';
//                print_r($data_bh);
//                die;
                $this->products_model->insert($data_bh);
            }

            redirect(PRODUCTS_ADMIN_BASE_URL);
        }
        $this->_last_message = validation_errors();
        return FALSE;
    }

    //==========================================================================
    function edit($options = array()) {

        if ($this->is_postback() && !$this->input->post('from_list')) {
            //if (!$this->_do_edit_product())
            if (!$this->_do_add_products())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        //======================================================================
        if (!isset($options['id'])) {
            $options['id'] = NULL;
        }
        //======================================================================
        $options += $this->_get_products_form_data();
        //end
        //======================================================================
        $bh_id = $this->input->post('id');
        // Lấy thông tin BH
        $data_get = array(
            'join_' . TBL_CUSTOMERS => TRUE,
            'BH_id' => $bh_id,
            'get_row' => TRUE,
        );
        $bh = $this->products_model->get_products_by_options($data_get);
        if (is_object($bh)) {
            $options['bh'] = $bh;
            $dn_bbh_id = $bh->dn_bbh;
            //==================================================================
            // Lấy thông tin các dn bán BH
            $data_get = array(
                    //'parent_id' => FALSE,
            );
            $dn_bbh = $this->products_trademark_model->get_products_trademark($data_get);
            if (!empty($dn_bbh)) {
                $options['a_dn_bbh'] = $dn_bbh;
            }
            //==================================================================
            // Lấy các gói BH của cn bbh
            $data_get = array(
                'dn_bbh' => $dn_bbh_id,
            );
            $this->load->model('products/products_size_model');
            $a_goi_bh = $this->products_size_model->get_products_size($data_get);
            if (!empty($a_goi_bh)) {
                $options['a_goi_bh'] = $a_goi_bh;
            }
            //==================================================================
        }
        //end
        //======================================================================
        // Chuan bi du lieu chinh de hien thi
        $this->phpsession->save('product_id', $bh_id);
        $options['submit_uri'] = PRODUCTS_ADMIN_EDIT_URL;
        $options['submit_uri_cus'] = CUSTOM_ADMIN_BASE_URL . '/add_from_pro';
        $options['header'] = 'Sửa thông tin bảo hiểm';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['scripts'] = $this->scripts_select2();
        $tpl = 'ad/bh/bh_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Sửa bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    //==========================================================================
    function delete() {
        $options = array();
        if ($this->is_postback()) {
            $product_id = $this->input->post('id');
            if (SLUG_ACTIVE > 0) {
                $check_slug = $this->slug_model->get_slug(array('type_id' => $product_id, 'type' => SLUG_TYPE_PRODUCTS, 'onehit' => TRUE));
                if (!empty($check_slug)) {
                    $this->slug_model->delete($check_slug->id);
                }
            }
            $this->products_images_model->delete_all_images($product_id, './images/products/');
            $this->products_model->delete($product_id);
            $options['warning'] = 'Đã xóa thành công';
        }
        $lang = $this->phpsession->get("product_lang");
        //return $this->browse($options);
        redirect(PRODUCTS_ADMIN_BASE_URL . '/' . $lang);
    }

    //==========================================================================

    public function up() {
        $id = $this->input->post('id');
        $this->products_model->item_to_sort_products(array('id' => $id));
        $lang = $this->phpsession->get("product_lang");
        redirect(PRODUCTS_ADMIN_BASE_URL . '/' . $lang);
    }

    //==========================================================================
    function change_status() {
        $id = $this->input->post('id');
        $product = $this->products_model->get_products(array('id' => $id, 'is_admin' => TRUE));
        $status = $product->status == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
        $this->products_model->update(array('id' => $id, 'status' => $status));
    }

    function change_state() {
        $id = $this->input->post('id');

        $product = $this->products_model->get_products(array('id' => $id, 'is_admin' => TRUE));
        $state = $product->state_id == STATE_ACTIVE ? STATE_INACTIVE : STATE_ACTIVE;
        $this->products_model->update(array('id' => $id, 'state_id' => $state));
    }

    function change_home() {
        $id = $this->input->post('id');
        $product = $this->products_model->get_products(array('id' => $id));
        $home = $product->home == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
        $this->products_model->update(array('id' => $id, 'home' => $home));
    }

    function export($options = array()) {

        $options['user_id'] = $this->phpsession->get('user_id');
        $options['role_id'] = $this->phpsession->get('role_id');
        $options['trademark_id'] = $this->phpsession->get('trademark_id');
        $temp_options = $this->phpsession->get('orders_search_options');
        if (is_array($temp_options)) {

            $options['start_date_m'] = $temp_options['start_date_m'];
            $options['end_date_m'] = $temp_options['end_date_m'];
        } else {
            $options['start_date_m'] = '';
            $options['end_date_m'] = '';
        }

        $options['is_admin'] = TRUE;

        $orders = $this->products_model->get_products($options);


        if (count($orders) > 0) {
            //load our new PHPExcel library
            $this->load->library('excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Báo cáo bảo hiểm');
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A1', 'STT');
            $this->excel->getActiveSheet()->setCellValue('B1', 'NHÂN VIÊN');
            $this->excel->getActiveSheet()->setCellValue('C1', 'CỘNG TÁC VIÊN');
            $this->excel->getActiveSheet()->setCellValue('D1', 'NGÀY BÁO CÁO');
            $this->excel->getActiveSheet()->setCellValue('E1', 'NGƯỜI MUA BH');
            $this->excel->getActiveSheet()->setCellValue('F1', 'NGƯỜI ĐƯỢC BH');
            $this->excel->getActiveSheet()->setCellValue('G1', 'PHONE');
            $this->excel->getActiveSheet()->setCellValue('H1', 'EMAIL');
            $this->excel->getActiveSheet()->setCellValue('I1', 'DOB');
            $this->excel->getActiveSheet()->setCellValue('J1', 'ĐỊA CHỈ');
            $this->excel->getActiveSheet()->setCellValue('K1', 'NGUỒN KH');
            $this->excel->getActiveSheet()->setCellValue('L1', 'SỐ HĐ');
            $this->excel->getActiveSheet()->setCellValue('M1', 'NGÀY HL');
            $this->excel->getActiveSheet()->setCellValue('N1', 'NHÀ BH');
            $this->excel->getActiveSheet()->setCellValue('O1', 'LOẠI BH');
            $this->excel->getActiveSheet()->setCellValue('P1', 'PHÍ CHUẨN');
            $this->excel->getActiveSheet()->setCellValue('Q1', 'PHÍ PHẢI THU KH');
            $this->excel->getActiveSheet()->setCellValue('R1', 'HOA HỒNG');
            $this->excel->getActiveSheet()->setCellValue('S1', 'SỐ TIỀN');
            $this->excel->getActiveSheet()->setCellValue('T1', 'HÌNH THỨC TT');
            $this->excel->getActiveSheet()->setCellValue('U1', 'NGÀY TT');
            $this->excel->getActiveSheet()->setCellValue('V1', 'CÒN LẠI');
            $this->excel->getActiveSheet()->setCellValue('W1', 'GHI CHÚ');

            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('Q1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('R1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('S1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('T1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('U1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('V1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('W1')->getFont()->setBold(true);


            $row = 2;
            foreach ($orders as $order):

                $options['kh_dbh'] = $order->kh_dbh;
                $custom_duoc = $this->products_model->get_name_dh($options);

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $order->id);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $order->creator_username);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $order->product_name);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $order->created_date);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $order->fullname_buy);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $custom_duoc->fullname_duoc);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $custom_duoc->phone_duoc);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $custom_duoc->email_duoc);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $custom_duoc->DOB_buy);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $custom_duoc->address_buy);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $order->origin_name);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $order->code);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, $order->contract_date);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(13, $row, $order->trademark_name);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(14, $row, $order->category);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(15, $row, $order->price);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(16, $row, $order->price_old);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(17, $row, $order->commission);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(18, $row, $order->price_real);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(19, $row, $order->style_name);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(20, $row, $order->date_of_payment);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(21, $row, $order->re_costs);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(22, $row, $order->description);

                $row++;
            endforeach;

            $filename = 'bao_cao_bao_hiem.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
        }else {
            //hoa don khong co san pham
            return $this->browse(array('error' => 'Mục bạn đã chọn hiện thời không có !'));
        }
    }

    function import($options = array()) {

        if (!empty($_FILES)) {

            //upload products excel file
            $config = array();
            $config['upload_path'] = './images/products-data/';
            $config['allowed_types'] = 'xls|xlsx|csv';
            $config['max_size'] = '5048';
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $options['error'] = $this->upload->display_errors();
            } else {
                $data = $this->upload->data();

                //read excel file
                $this->load->library('excel');
                $inputFileName = './images/products-data/' . $data['file_name'];

                //  Read your Excel workbook
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                }

                //  Get worksheet dimensions
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //  Loop through each row of the worksheet in turn
                for ($row = 1; $row <= $highestRow; $row++) {
                    //  Read a row of data into an array
                    $rowData = $sheet->ToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    //  Insert row data array into your database of choice here
                }

                foreach ($rowData as $index => $data) {
                    if ($index != 0) {
                        $id = $data[0];
                        $creator = $data[1];
                        $product_name = $data[2];
                        $created_date = $data[3];
                        $custom_buy_id = $data[4];
                        $kh_dbh = $data[5];
                        $origin_id = $data[6];
                        $code = $data[7];
                        $contract_date = $data[8];
                        $trademark_id = $data[9];
                        $categories_id = $data[10];
                        $price = $data[11];
                        $price_old = $data[12];
                        $commission = $data[13];
                        $price_real = $data[14];
                        $style_id = $data[15];
                        $date_of_payment = $data[16];
                        $re_costs = $data[17];
                        $description = $data[18];


                        $options['code'] = $code;

                        $products = $this->products_model->get_products($options);

                        //if productsname doesn't exist in system
                        if (count($products) == 0) {
                            //insert to products table
                            $products_data = array(
                                'creator' => $creator,
                                'product_name' => $product_name,
                                'created_date' => $created_date,
                                'custom_buy_id ' => $custom_buy_id,
                                'kh_dbh ' => $kh_dbh,
                                'origin_id' => $origin_id,
                                'code' => $code,
                                'contract_date' => $contract_date,
                                'trademark_id' => $trademark_id,
                                'categories_id' => $categories_id,
                                'price' => $price,
                                'price_old' => $price_old,
                                'commission' => $commission,
                                'price_real' => $price_real,
                                'style_id ' => $style_id,
                                'date_of_payment' => $date_of_payment,
                                're_costs' => $re_costs,
                                'description' => $description,
                            );
                            $position_add = $this->products_model->position_to_add_products(array('lang' => $data['lang']));

                            $products_data['position'] = $position_add;

                            $products_id = $this->products_model->insert($products_data);
                        } else {


                            $products_id = $products[0]->id;
                            foreach ($products as $value) {

                                $this->products_model->update(array(
                                    'id' => $value->id,
                                    'creator' => $creator,
                                    'product_name' => $product_name,
                                    'created_date' => $created_date,
                                    'custom_buy_id ' => $custom_buy_id,
                                    'kh_dbh ' => $kh_dbh,
                                    'origin_id ' => $origin_id,
                                    'code' => $code,
                                    'contract_date ' => $contract_date,
                                    'trademark_id ' => $trademark_id,
                                    'categories_id ' => $categories_id,
                                    'price ' => $price,
                                    'price_old' => $price_old,
                                    'commission  ' => $commission,
                                    'price_real' => $price_real,
                                    'style_id ' => $style_id,
                                    'date_of_payment' => $date_of_payment,
                                    're_costs' => $re_costs,
                                    'description ' => $description,
                                ));
                            }
                        }
                    }
                }
                $options['succeed'] = 'Bạn đã nhập dữ liệu thành công';
            }

            $this->browse();
        } else {
            $options['error'] = validation_errors();
            $options['header'] = 'Nhập bảo hiểm bằng excel';

            if (isset($options['error']) || isset($options['succeed']) || isset($options['warning']))
                $options['options'] = $options;

            // Chuan bi du lieu chinh de hien thi
            $this->_view_data['main_content'] = $this->load->view('admin/products/add_product_form_excel', $options, TRUE);
            // Chuan bi cac the META
            $this->_view_data['title'] = 'Nhập bảo hiểm bằng excel' . DEFAULT_TITLE_SUFFIX;
            // Tra lai view du lieu cho nguoi su dung
            $this->load->view($this->_layout, $this->_view_data);
        }
        return FALSE;
//        else
//        {
//            $options['error'] = 'Bạn vui lòng chọn file để upload';
//        }
//        $options['page'] = 1;
//        return $this->browse($options);
    }

}

?>