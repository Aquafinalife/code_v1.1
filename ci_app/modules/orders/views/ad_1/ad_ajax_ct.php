<?php
$checked = 'checked="checked"';
$disabled_check = isset($disabled) ? $disabled : '';
//end
?>
<div class="box_chungtu_bt_list col-sm-12 mb15">
    <h3 class="main_header">Chứng từ hồ sơ bồi thường</h3>
    <?php
    if (isset($ct) && !empty($ct)) {
        $i = 1;
        foreach ($ct as $index) {
            $id = $index->id;
            $name = $index->name;
            ?>
            <div class="col-sm-11 col-xs-10">
                <ul class="list_hsbt_bs">
                    <li>
                        <?php echo $i ?>. <?php echo $name ?>
                    </li>
                </ul>
            </div>
            <div class="col-sm-1 col-xs-2">
                <ul class="list_hsbt_bs">
                    <li>
                        <div class="checkbox">
                            <label>
                                <input class="checkbox_ct" name="ct_bt[]" type="checkbox" value="<?php echo $id ?>">
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="clear-both clearfix"></div>
            <?php
            $i++;
        }
    }
    ?>
</div>
<div class="btn btn-warning btn_check_ct">Kiểm tra chứng từ</div> <i>(Click kiểm tra chứng từ trước khi lưu dữ liệu.)</i>
<!--<div class="btn btn-warning btn_xem_qlbt">Xem quyền lợi bồi thường</div>-->
