<?php

class Products_Coupon_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        modules::run('auth/auth/validate_login');
        $this->_layout = 'admin_ui/layout/main';
    }

    function browse($offset = 0, $options = array()) {
        $options = array('lang' => switch_language($para1), 'page' => $para2);
        //======================================================================
        // Filter
        $G_pl_bt = $_GET['pl_bt'];
        if ($G_pl_bt != '') {
            //$options['phanloai_bt'] = $G_pl_bt;
            $options['where']['parent_id'] = $G_pl_bt;
        }
        //======================================================================

        $per_page = LIMIT_DEFAULT;
        // Đếm tổng số bt
        $total_rows = $this->products_coupon_model->CommonCount(TBL_CHUNGTU, $options);
        //$total_rows = 20;
        $options['total_rows'] = $total_rows;
        //end
        //======================================================================
        //echo $this->uri->segment(3).'????';
        $paging_config = array(
            'base_url' => site_url('dashboard/products/') . '/' . $this->uri->segment(3),
            'total_rows' => $total_rows,
            'per_page' => $per_page,
            'uri_segment' => 4,
            'use_page_numbers' => TRUE,
            'first_link' => __('IP_paging_first'),
            'last_link' => __('IP_paging_last'),
            'num_links' => 1,
        );
        if (isset($_GET) && $_GET != '') {
            $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($paging_config);
        $options['pagination'] = $this->pagination->create_links();

        //======================================================================
        $options['offset'] = ($offset > 0) ? ($offset - 1) * ($per_page) : $offset;
        $options['limit'] = $per_page;        
        //======================================================================
        $chungtu = $this->products_coupon_model->CommonGet(TBL_CHUNGTU, $options);
        if (!empty($chungtu)) {
            $options['chungtu'] = $chungtu;
        }
        //======================================================================
        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $options['options'] = $options;
        }
        //======================================================================
        $options['scripts'] = $this->scripts_select2();
        $tpl = 'ad/chungtu/chungtu_list';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Chứng từ' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function add() {
        $options = array();
        // Xu ly viec them vao db
        if ($this->is_postback()) {
            if (!$this->_do_add()) {
                $options['error'] = validation_errors();
                $options['options'] = $options;
            }
        }
        //======================================================================
        $options['header'] = 'Thêm Chứng từ';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_COUPON_ADMIN_ADD_URL;
        $options['scripts'] = $this->scripts_select2();
        //======================================================================
        $tpl = 'ad/chungtu/chungtu_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Thêm Chứng từ' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _get_add_form_data(&$options = array()) {
        $options['name'] = $this->input->post('name');
        $options['discount_type'] = $this->input->post('discount_type');
        $options['discount'] = $this->input->post('discount');
        $options['price_min'] = $this->input->post('price_min');
//        $options['number'] = $this->input->post('number');
        $options['count'] = $this->input->post('count');
        if ($this->is_postback()) {
            $end_date = datetimepicker_array2($this->input->post('end_date', TRUE));
            $options['end_date'] = date('d-m-Y H:i', mktime($end_date['hour'], $end_date['minute'], $end_date['second'], $end_date['month'], $end_date['day'], $end_date['year']));
        } else {
            $options['end_date'] = date('d-m-Y H:i');
        }
        $options['header'] = 'Thêm Chứng từ';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_COUPON_ADMIN_ADD_URL;
    }

    //==========================================================================
    private function _do_add() {
        $this->form_validation->set_rules('name', 'Tên chứng từ', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('categories', 'Loại bồi thường', 'trim|required|xss_clean');
        //======================================================================
        if ($this->form_validation->run()) {
            $data = array(
                'name' => my_trim($this->input->post('name')),
                'parent_id' => my_trim($this->input->post('categories')),
                'status' => STATUS_ACTIVE,
                'creator' => $this->phpsession->get('user_id'),
                'editor' => $this->phpsession->get('user_id'),
            );
            //==================================================================
            $insert_id = $this->products_coupon_model->insert($data);

            redirect(PRODUCTS_COUPON_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //==========================================================================
    function edit() {
        $options = array();
        if ($this->is_postback()) {
            if (!$this->input->post('from_list')) {
                if (!$this->_do_edit()) {
                    $options['error'] = validation_errors();
                    $options['options'] = $options;
                }
            }
        }
        //$this->_get_edit_form_data($options);
        // Chuan bi du lieu chinh de hien thi
        $id = $this->input->post('id');
        // Lấy thông tin
        $ct = $this->products_coupon_model->get_products_coupon(array('id' => $id));
        if (is_object($ct)) {
            $options['ct'] = $ct;
        }
        $options['id'] = $id;
        $options['header'] = 'Sửa Chứng từ';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_COUPON_ADMIN_EDIT_URL;
        $options['is_edit'] = TRUE;
        $this->_view_data['main_content'] = $this->load->view('admin/products_coupon/form', $options, TRUE);
        // Chuan bi cac the META
        $this->_view_data['title'] = 'Sửa Chứng từ' . DEFAULT_TITLE_SUFFIX;
        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _get_edit_form_data(&$options = array()) {
        $id = $this->input->post('id');
        if ($this->input->post('from_list')) {
            $coupon = $this->products_coupon_model->get_products_coupon(array('id' => $id));
            $options['name'] = $coupon->name;
            $options['discount_type'] = $coupon->discount_type;
            $options['discount'] = $coupon->discount;
            $options['price_min'] = $coupon->price_min;
            $options['end_date'] = date('d-m-Y H:i', strtotime($coupon->end_date));
//            $options['number'] = $coupon->number;
            $options['count'] = $coupon->count;
        } else {
            $options['name'] = $this->input->post('name');
            $options['discount_type'] = $this->input->post('discount_type');
            $options['discount'] = $this->input->post('discount');
            $options['price_min'] = $this->input->post('price_min');
            $end_date = datetimepicker_array2($this->input->post('end_date', TRUE));
            $options['end_date'] = date('Y-m-d H:i', mktime($end_date['hour'], $end_date['minute'], $end_date['second'], $end_date['month'], $end_date['day'], $end_date['year']));
//            $options['number'] = $this->input->post('number');
            $options['count'] = $this->input->post('count');
        }
        $options['id'] = $id;
        $options['header'] = 'Sửa Chứng từ';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_COUPON_ADMIN_EDIT_URL;
        $options['is_edit'] = TRUE;
    }

    private function _do_edit() {
        $this->form_validation->set_rules('name', 'Tên chứng từ', 'trim|required|xss_clean|max_length[255]');
        //$this->form_validation->set_rules('discount', 'Giảm giá', 'trim|required|xss_clean|is_numeric');
        //$this->form_validation->set_rules('discount_type', 'Hình thức giảm giá', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('price_min', 'Số tiền tối thiểu của đơn hàng', 'trim|xss_clean|is_numeric');
        //$this->form_validation->set_rules('end_date', 'Ngày hết hạn', 'trim|required|xss_clean');
        $this->form_validation->set_rules('categories', 'Loại bồi thường', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('count', 'Số lượng mã', 'trim|xss_clean|is_numeric');
        if ($this->form_validation->run()) {
            $data = array(
                'id' => $this->input->post('id'),
                'name' => my_trim($this->input->post('name', TRUE)),
                //'discount' => my_trim($this->input->post('discount', TRUE)),
                //'discount_type' => my_trim($this->input->post('discount_type', TRUE)),
                //'price_min' => my_trim($this->input->post('price_min', TRUE)),
                'parent_id' => my_trim($this->input->post('categories', TRUE)),
                //'number' => 1,
                //'count' => my_trim($this->input->post('count', TRUE)),
                'editor' => $this->phpsession->get('user_id'),
            );
            $this->products_coupon_model->update($data);
            redirect(PRODUCTS_COUPON_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    function delete() {
        if ($this->is_postback() && $this->input->post('from_list')) {
            $id = $this->input->post('id');
            $this->products_coupon_model->delete($id);
            $this->products_coupon_item_model->delete(array('coupon_id' => $id));
            redirect(PRODUCTS_COUPON_ADMIN_BASE_URL);
        }
    }

    function change_status() {
        $id = $this->input->post('id');
        $data = $this->products_coupon_model->get_products_coupon(array('id' => $id));
        $status = $data->status == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
        $this->products_coupon_model->update(array('id' => $id, 'status' => $status));
    }

    function get_string_all_coupon() {
        $all_coupon = $this->products_coupon_item_model->get_products_coupon_item(array('status' => STATUS_ACTIVE));
        if (!empty($all_coupon)) {
            $all_string = '';
            foreach ($all_coupon as $key => $value) {
                $all_string .= $value->code . ', ';
            }
        } else {
            $all_string = '';
        }
        return rtrim($all_string, ", ");
    }

}
