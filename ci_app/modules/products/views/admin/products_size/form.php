<?php
if (isset($size) && is_object($size)) {
    $id = $size->id;
    $name = $size->name;
    $code = $size->code;
    $gia_tien = $size->gia_tien;
    $TV_TTTBVV_TN = $size->TV_TTTBVV_TN;
    $TCL_NN = $size->TCL_NN;
    $tcl_nn_sn = $size->tcl_nn_sn;
    $CPYT_TN = $size->CPYT_TN;
    $TV_TTTBVV_OB = $size->TV_TTTBVV_OB;
    $DTNT_OB = $size->DTNT_OB;
    $dtnt_dbh = $size->dtnt_dbh;
    $dtnt_tvpn = $size->dtnt_tvpn;
    $dtnt_tvpn_sn = $size->dtnt_tvpn_sn;
    $dtnt_tgpn = $size->dtnt_tgpn;
    $dtnt_tgpn_sn = $size->dtnt_tgpn_sn;
    $dtnt_cppt = $size->dtnt_cppt;
    $dtnt_tk_nv = $size->dtnt_tk_nv;
    $dtnt_sk_nv = $size->dtnt_sk_nv;
    $dtnt_cpyt_tn = $size->dtnt_cpyt_tn;
    $dtnt_cpyt_tn_sn = $size->dtnt_cpyt_tn_sn;
    $dtnt_tcvp = $size->dtnt_tcvp;
    $dtnt_tcvp_sn = $size->dtnt_tcvp_sn;
    $dtnt_tcmt = $size->dtnt_tcmt;
    $dtnt_xct = $size->dtnt_xct;
    $THAISAN_QLNT = $size->THAISAN_QLNT;
    $tsqlnt_ktdk = $size->tsqlnt_ktdk;
    $tsqlnt_st = $size->tsqlnt_st;
    $tsqlnt_sm = $size->tsqlnt_sm;
    $tsqlnt_dn = $size->tsqlnt_dn;
    $DTNGT_OB = $size->DTNGT_OB;
    $dtngt_dbh = $size->dtngt_dbh;
    $dtngt_st1lk = $size->dtngt_st1lk;
    $dtngt_slk = $size->dtngt_slk;
    $dtngt_nk = $size->dtngt_nk;
    $dtngt_cvr = $size->dtngt_cvr;
    $THAISAN_MDL = $size->THAISAN_MDL;
    $tsmdl_dbh = $size->tsmdl_dbh;
    $tsmdl_ktdk = $size->tsmdl_ktdk;
    $tsmdl_st = $size->tsmdl_st;
    $tsmdl_sm = $size->tsmdl_sm;
    $tsmdl_dn = $size->tsmdl_dn;
    $NHAKHOA_MDL = $size->NHAKHOA_MDL;
    $nkmdl_dbh = $size->nkmdl_dbh;
    $nkmdl_cb = $size->nkmdl_cb;
    $nkmdl_db = $size->nkmdl_db;
    //
    $dn_bbh = $size->dn_bbh;
    $cn_bbh = $size->cn_bbh;
    $moigioi = $size->moigioi;
}

$id = isset($id) ? $id : '0';
$dn_bbh = isset($dn_bbh) ? $dn_bbh : '0';
$cn_bbh = isset($cn_bbh) ? $cn_bbh : '0';
//echo '<pre>';
//print_r($cn_bbh);
//die;
echo form_open($submit_uri);
if (isset($id))
    echo form_hidden('id', $id);
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'products_size');
?>

<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Thêm/sửa tình trạng"</small>
    <span class="fright"><a href="<?php echo PRODUCTS_SIZE_ADMIN_BASE_URL; ?>" class="button close" style="padding:5px;" title="Đóng"><em></em><span>Đóng</span></a></span>
    <br class="clear"/>
</div>

<div class="form_content">
    <ul class="tabs">
        <li><a href="#tab1">Nội dung</a></li>
    </ul>
    <?php $this->load->view('powercms/message'); ?>
    <div class="tab_container">
        <div id="tab1" class="tab_content">

            <div class="col-sm-6 col-xs-12">
                <table>
                    <tr><td class="title">Gói bảo hiểm: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'name', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($name) ? $name : set_value('name'))); ?></td>
                    </tr>
                    <tr><td class="title">Code gói BH: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'code', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($code) ? $code : set_value('code'))); ?></td>
                    </tr>
                    <tr><td class="title">Giá tiền: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'gia_tien', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($gia_tien) ? $gia_tien : set_value('gia_tien'))); ?></td>
                    </tr>
                    <tr><td class="title">Tử vong, TTTBVV (Do tai nạn): (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'TV_TTTBVV_TN', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($TV_TTTBVV_TN) ? $TV_TTTBVV_TN : set_value('TV_TTTBVV_TN'))); ?></td>
                    </tr>

                    <tr><td class="title">Trợ cấp lương ngày nghỉ: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'TCL_NN', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($TCL_NN) ? $TCL_NN : set_value('TCL_NN'))); ?></td>
                    </tr>
                    <tr><td class="title">Số ngày: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'tcl_nn_sn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($tcl_nn_sn) ? $tcl_nn_sn : set_value('tcl_nn_sn'))); ?></td>
                    </tr>
                    <tr><td class="title">Chi phí y tế do tai nạn: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'CPYT_TN', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($CPYT_TN) ? $CPYT_TN : set_value('CPYT_TN'))); ?></td>
                    </tr>
                    <tr><td class="title">Tử vong, TTTBVV (Do ốm bệnh): (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'TV_TTTBVV_OB', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($TV_TTTBVV_OB) ? $TV_TTTBVV_OB : set_value('TV_TTTBVV_OB'))); ?></td>
                    </tr>
                    <tr><td class="title">Điều trị nội trú (Do ốm bệnh): (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'DTNT_OB', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($DTNT_OB) ? $DTNT_OB : set_value('DTNT_OB'))); ?></td>
                    </tr>
                    <tr><td class="title">Đồng bảo hiểm: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_dbh', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_dbh) ? $dtnt_dbh : set_value('dtnt_dbh'))); ?></td>
                    </tr>
                    <tr><td class="title">Tiền viện phí ngày: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_tvpn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_tvpn) ? $dtnt_tvpn : set_value('dtnt_tvpn'))); ?></td>
                    </tr>
                    <tr><td class="title">Số ngày: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_tvpn_sn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_tvpn_sn) ? $dtnt_tvpn_sn : set_value('dtnt_tvpn_sn'))); ?></td>
                    </tr>
                    <tr><td class="title">Tiền giường, phòng / ngày: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_tgpn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_tgpn) ? $dtnt_tgpn : set_value('dtnt_tgpn'))); ?></td>
                    </tr>                    
                    <tr><td class="title">Số ngày: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_tgpn_sn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_tgpn_sn) ? $dtnt_tgpn_sn : set_value('dtnt_tgpn_sn'))); ?></td>
                    </tr>
                    <tr><td class="title">Chi phí phẫu thuật: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_cppt', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_cppt) ? $dtnt_cppt : set_value('dtnt_cppt'))); ?></td>
                    </tr>
                    <tr><td class="title">Quyền lợi khám trước khi nhập viện: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_tk_nv', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_tk_nv) ? $dtnt_tk_nv : set_value('dtnt_tk_nv'))); ?></td>
                    </tr>
                    <tr><td class="title">Quyền lợi khám sau khi nhập viện: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_sk_nv', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_sk_nv) ? $dtnt_sk_nv : set_value('dtnt_sk_nv'))); ?></td>
                    </tr>
                    <tr><td class="title">Chi phí y tá tại nhà: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_cpyt_tn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_cpyt_tn) ? $dtnt_cpyt_tn : set_value('dtnt_cpyt_tn'))); ?></td>
                    </tr>
                    <tr><td class="title">Số ngày: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_cpyt_tn_sn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_cpyt_tn_sn) ? $dtnt_cpyt_tn_sn : set_value('dtnt_cpyt_tn_sn'))); ?></td>
                    </tr>
                    <tr><td class="title">Trợ cấp viện phí: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_tcvp', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_tcvp) ? $dtnt_tcvp : set_value('dtnt_tcvp'))); ?></td>
                    </tr>
                    <tr><td class="title">Số ngày: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_tcvp_sn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_tcvp_sn) ? $dtnt_tcvp_sn : set_value('dtnt_tcvp_sn'))); ?></td>
                    </tr>
                    <tr><td class="title">Trợ cấp mai táng: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_tcmt', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_tcmt) ? $dtnt_tcmt : set_value('dtnt_tcmt'))); ?></td>
                    </tr>
                    <tr><td class="title">Xe cứu thương: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtnt_xct', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtnt_xct) ? $dtnt_xct : set_value('dtnt_xct'))); ?></td>
                    </tr>                    
                </table>
            </div>

            <div class="col-sm-6 col-xs-12">
                <table>
                    <tr><td class="title">THAI SẢN (Trong quyền lợi nội trú): (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'TV_TTTBVV_TN', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($TV_TTTBVV_TN) ? $TV_TTTBVV_TN : set_value('TV_TTTBVV_TN'))); ?></td>
                    </tr>
                    <tr><td class="title">Chi phí khám thai định kỳ: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'tsqlnt_ktdk', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($tsqlnt_ktdk) ? $tsqlnt_ktdk : set_value('tsqlnt_ktdk'))); ?></td>
                    </tr>
                    <tr><td class="title">Sinh thường: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'tsqlnt_st', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($tsqlnt_st) ? $tsqlnt_st : set_value('tsqlnt_st'))); ?></td>
                    </tr>
                    <tr><td class="title">Sinh mổ: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'tsqlnt_sm', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($tsqlnt_sm) ? $tsqlnt_sm : set_value('tsqlnt_sm'))); ?></td>
                    </tr>
                    <tr><td class="title">Dưỡng nhi: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'tsqlnt_dn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($tsqlnt_dn) ? $tsqlnt_dn : set_value('tsqlnt_dn'))); ?></td>
                    </tr>
                    <tr><td class="title">ĐIỀU TRỊ NGOẠI TRÚ (Do ốm bệnh): (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'DTNGT_OB', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($DTNGT_OB) ? $DTNGT_OB : set_value('DTNGT_OB'))); ?></td>
                    </tr>
                    <tr><td class="title">Đồng bảo hiểm: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtngt_dbh', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtngt_dbh) ? $dtngt_dbh : set_value('dtngt_dbh'))); ?></td>
                    </tr>
                    <tr><td class="title">Số tiền cho 1 lần khám: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtngt_st1lk', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtngt_st1lk) ? $dtngt_st1lk : set_value('dtngt_st1lk'))); ?></td>
                    </tr>
                    <tr><td class="title">Số lần khám: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtngt_slk', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtngt_slk) ? $dtngt_slk : set_value('dtngt_slk'))); ?></td>
                    </tr>
                    <tr><td class="title">Nha khoa: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtngt_nk', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtngt_nk) ? $dtngt_nk : set_value('dtngt_nk'))); ?></td>
                    </tr>
                    <tr><td class="title">Cạo vôi răng: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dtngt_cvr', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($dtngt_cvr) ? $dtngt_cvr : set_value('dtngt_cvr'))); ?></td>
                    </tr>
                    <tr><td class="title">THAI SẢN (Mua độc lập): (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'THAISAN_MDL', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($THAISAN_MDL) ? $THAISAN_MDL : set_value('THAISAN_MDL'))); ?></td>
                    </tr>
                    <tr><td class="title">Đồng bảo hiểm: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'tsmdl_dbh', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($tsmdl_dbh) ? $tsmdl_dbh : set_value('tsmdl_dbh'))); ?></td>
                    </tr>
                    <tr><td class="title">Chi phí khám thai định kỳ: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'tsmdl_ktdk', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($tsmdl_ktdk) ? $tsmdl_ktdk : set_value('tsmdl_ktdk'))); ?></td>
                    </tr>
                    <tr><td class="title">Sinh thường: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'tsmdl_st', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($tsmdl_st) ? $tsmdl_st : set_value('tsmdl_st'))); ?></td>
                    </tr>
                    <tr><td class="title">Sinh mổ: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'tsmdl_sm', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($tsmdl_sm) ? $tsmdl_sm : set_value('tsmdl_sm'))); ?></td>
                    </tr>
                    <tr><td class="title">Dưỡng nhi: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'tsmdl_dn', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($tsmdl_dn) ? $tsmdl_dn : set_value('tsmdl_dn'))); ?></td>
                    </tr>
                    <tr><td class="title">NHA KHOA (Mua độc lập): (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'NHAKHOA_MDL', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($NHAKHOA_MDL) ? $NHAKHOA_MDL : set_value('NHAKHOA_MDL'))); ?></td>
                    </tr>
                    <tr><td class="title">Đồng bảo hiểm: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'nkmdl_dbh', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($nkmdl_dbh) ? $nkmdl_dbh : set_value('nkmdl_dbh'))); ?></td>
                    </tr>
                    <tr><td class="title">Nha khoa cơ bản (tiền khám chữa răng,trám răng,cạo vôi răng): (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'nkmdl_cb', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($nkmdl_cb) ? $nkmdl_cb : set_value('nkmdl_cb'))); ?></td>
                    </tr>
                    <tr><td class="title">Nha khoa đặc biệt (tiền làm răng thẩm mỹ,cầu răng,đồng bảo hiểm 50%): (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'nkmdl_db', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($nkmdl_db) ? $nkmdl_db : set_value('nkmdl_db'))); ?></td>
                    </tr>
                </table>
            </div>
            <div class="clear-both"></div>
            <div class="col-sm-6 col-xs-12">
                <table>
                    <tr><td class="title">Công ty Bảo Hiểm: (<span>*</span>)</td></tr>
                    <tr>
                        <td>
                            <select  name="dn_bbh" class="filter_dn_bbh">
                                <option value="">--Chọn Công ty/doanh nghiệp BH--</option>
                                <?php
                                if (!empty($a_dn_bbh)) {
                                    foreach ($a_dn_bbh as $index) {
                                        $name = $index->name;
                                        $parent_id = $index->parent_id;
                                        $id = $index->id;
                                        $selected = ($id == $dn_bbh) ? 'selected="selected"' : '';
                                        if ($parent_id == FALSE) {
                                            ?>
                                            <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <!--<tr><td class="title">Chi nhánh/phòng ban: (<span>*</span>)</td></tr>-->
<!--                    <tr>
                        <td class="">
                            <div class="box_filter_cn_bbh">
                                <select name="cn_bbh" class="">
                                <option value="">--Chọn chi nhánh/phòng ban BH--</option>
                                <?php
//                                if ($cn_bbh > 0) {
//                                    if (!empty($a_dn_bbh)) {
//                                        foreach ($a_dn_bbh as $index) {
//                                            $name = $index->name;
//                                            $parent_id = $index->parent_id;
//                                            $id = $index->id;
//                                            $selected = ($id == $cn_bbh) ? 'selected="selected"' : '';
//                                            if ($dn_bbh == $parent_id) {
//                                                ?>
                                                <option //<?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                                                //<?php
//                                            }
//                                        }
//                                    }
//                                }
                                ?>
                            </select>
                            </div>
                            
                        </td>
                    </tr>-->
                    <tr><td class="title">Môi giới: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'moigioi', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($moigioi) ? $moigioi : set_value('moigioi'))); ?></td>
                    </tr>
                </table>
            </div>

        </div>

    </div>
    <br class="clear"/>
    <div style="margin-top: 10px;"></div>
    <?php echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn')); ?>
    <input type="reset" value="Làm lại" class="btn" />
    <br class="clear"/>&nbsp;
</div>
<?php echo form_close(); ?>