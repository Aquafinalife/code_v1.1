<?php

class Products_Material_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->_layout = 'admin_ui/layout/main';
    }

    function browse() {
        $options = array();

        $nh = $this->products_material_model->CommonGet(TBL_NGANHANG);
        if (!empty($nh)) {
            $options['a_nh'] = $nh;
        }
        //======================================================================
        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $options['options'] = $options;
        }
        //======================================================================
        $tpl = 'ad/nganhang/nganhang_list';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Quản lý Ngân hàng' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function add() {
        $options = array();

        if ($this->is_postback()) {
            if (!$this->_do_add()) {
                $options['error'] = validation_errors();
                $options['options'] = $options;
            }
        }

        //======================================================================
        $options['header'] = 'Thêm Ngân hàng';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_MATERIAL_ADMIN_ADD_URL;
        //======================================================================
        $tpl = 'ad/nganhang/nganhang_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Thêm Ngân hàng' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _do_add() {
        $this->form_validation->set_rules('name', 'Ngân hàng', 'trim|required|xss_clean|max_length[255]');
        if ($this->form_validation->run()) {
            $data = array(
                'name' => my_trim($this->input->post('name')),
                'status' => STATUS_ACTIVE,
                'editor' => $this->phpsession->get('user_id'),
            );
            //==================================================================
            $id = $_POST['id'];
            if ($id > 0) {
                $this->products_material_model->CommonUpdate(TBL_NGANHANG, $id, $data);
            } else {
                $data['creator'] = $this->phpsession->get('user_id');
                $this->products_material_model->CommonCreat(TBL_NGANHANG, $data);
            }
            redirect(PRODUCTS_MATERIAL_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //==========================================================================
    function edit() {
        $options = array();
        if ($this->is_postback()) {
            if (!$this->input->post('from_list')) {
                if (!$this->_do_add()) {
                    $options['error'] = validation_errors();
                    $options['options'] = $options;
                }
            }
        }
        //======================================================================
        $id = $_POST['id'];
        if ($id > 0) {
            // Lấy nh
            $data_get = array(
                'where' => array(
                    'id' => $id,
                ),
                'get_row' => TRUE,
            );
            $nh = $this->products_material_model->CommonGet(TBL_NGANHANG, $data_get);
            if (is_object($nh)) {
                $options['nh'] = $nh;
            }
        } else {
            redirect(PRODUCTS_MATERIAL_ADMIN_BASE_URL);
        }
        //======================================================================
        $options['header'] = 'Sửa Ngân hàng';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_MATERIAL_ADMIN_EDIT_URL;
        $tpl = 'ad/nganhang/nganhang_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Sửa Ngân hàng' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function delete() {
        if ($this->is_postback() && $this->input->post('from_list')) {
            $id = $this->input->post('id');
            $check1 = $this->products_model->get_products_count(array('material_id' => $id));
            if (empty($check1)) {
                $this->products_material_model->delete($id);
            }
            redirect(PRODUCTS_MATERIAL_ADMIN_BASE_URL);
        }
    }

}
