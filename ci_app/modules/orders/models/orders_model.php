<?php

class Orders_Model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function get_quyenloi_bt($options = array()) {

        $this->filter_data_quyenloi_bt($options);

        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_QLBT, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_QLBT)->first_row();
            $q = $this->db->get(TBL_QLBT)->result();
//            $a = $this->db->last_query();
//            echo '<pre>';
//            print_r($a);
//            die;
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            $this->db->order_by(TBL_QLBT . '.id asc');
            return $q = $this->db->get(TBL_QLBT)->result();
//            $q = $this->db->get(TBL_QLBT)->result();
//            $a = $this->db->last_query();
//            echo '<pre>';
//            print_r($a);
//            die;
        }

        $q->free_result();
    }

    //end
    //==========================================================================
    function filter_data_quyenloi_bt($options = array()) {
        // join
        $select = '' . TBL_QLBT . '.*,';

        if (isset($options['join_' . TBL_CUSTOMERS]) || isset($options['join_' . TBL_CUSTOMERS . '_mbh']) || isset($options['join_' . TBL_CUSTOMERS . '_dbh'])) {
            $select .= '' . TBL_CUSTOMERS . '.id as KH_id,
                        ' . TBL_CUSTOMERS . '.congty_canhan,
                        ' . TBL_CUSTOMERS . '.mst_cmt,
                        ' . TBL_CUSTOMERS . '.parent_id,
                        ' . TBL_CUSTOMERS . '.phongban,
                        ' . TBL_CUSTOMERS . '.phongban,';
        }
        if (isset($options['join_' . TBL_PRODUCTS]) || isset($options['join_' . TBL_PRODUCTS . '_dbh'])) {
            $select .= '' . TBL_PRODUCTS . '.id as BH_id,
                        ' . TBL_PRODUCTS . '.kh_dbh,
                        ' . TBL_PRODUCTS . '.kh_mbh,';
        }

        $this->db->select($select);
        //======================================================================
        // Join
        if (isset($options['join_' . TBL_CUSTOMERS])) {
            $this->db->join(TBL_CUSTOMERS, TBL_ORDERS . '.OD_kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }

        if (isset($options['join_' . TBL_PRODUCTS . '_dbh'])) {
            $this->db->join(TBL_PRODUCTS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_ORDERS . '.OD_kh_dbh', 'left');
        }

        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $this->db->join(TBL_PRODUCTS_SIZE, TBL_PRODUCTS . '.goi_bh = ' . TBL_PRODUCTS_SIZE . '.id', 'left');
        }
        //end
        //======================================================================
        // where
        if (isset($options['QLBT_KH_id'])) {
            $this->db->where(TBL_QLBT . '.QLBT_KH_id = ', $options['QLBT_KH_id']);
        }
        if (isset($options['QLBT_BT_id'])) {
            $this->db->where(TBL_QLBT . '.QLBT_BT_id = ', $options['QLBT_BT_id']);
        }
        //======================================================================
        if (isset($options['BH_kh_mbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_mbh = ', $options['BH_kh_mbh']);
        }
        //======================================================================
        //end
    }

    //end
    //==========================================================================
    function get_orders_by_options($options = array()) {

        $this->filter_data_orders_by_options($options);

        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_ORDERS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_ORDERS)->first_row();
//            $q = $this->db->get(TBL_ORDERS)->result();
//            $a = $this->db->last_query();
//            echo '<pre>';
//            print_r($a);
//            die;
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            $this->db->order_by('orders.id desc');
            return $q = $this->db->get(TBL_ORDERS)->result();
            //$q = $this->db->get(TBL_ORDERS)->result();
            //$a = $this->db->last_query();
        }

        $q->free_result();
    }

    //end
    //==========================================================================
    function filter_data_orders_by_options($options = array()) {
        // join
        $select = '' . TBL_ORDERS . '.*,';

        if (isset($options['join_' . TBL_CUSTOMERS]) || isset($options['join_' . TBL_CUSTOMERS . '_mbh']) || isset($options['join_' . TBL_CUSTOMERS . '_dbh'])) {
            $select .= '' . TBL_CUSTOMERS . '.id as KH_id,
                        ' . TBL_CUSTOMERS . '.congty_canhan,
                        ' . TBL_CUSTOMERS . '.mst_cmt,
                        ' . TBL_CUSTOMERS . '.parent_id,
                        ' . TBL_CUSTOMERS . '.phongban,
                        ' . TBL_CUSTOMERS . '.ndbh_quanhe,';
        }

        if (isset($options['join_' . TBL_PRODUCTS]) || isset($options['join_' . TBL_PRODUCTS . '_dbh'])) {
            $select .= '' . TBL_PRODUCTS . '.id as BH_id,
                        ' . TBL_PRODUCTS . '.goi_bh as BH_GOI_ID,
                        ' . TBL_PRODUCTS . '.mqh,
                        ' . TBL_PRODUCTS . '.kh_dbh,
                        ' . TBL_PRODUCTS . '.kh_mbh,';
        }

        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $select .= '' . TBL_PRODUCTS_SIZE . '.name as goi_bh_dm,
                                ' . TBL_PRODUCTS_SIZE . '.gia_tien as goi_gia_tien,
                                ' . TBL_PRODUCTS_SIZE . '.id as GOI_ID,
                                ' . TBL_PRODUCTS_SIZE . '.name,
                                ' . TBL_PRODUCTS_SIZE . '.TV_TTTBVV_TN,
                                ' . TBL_PRODUCTS_SIZE . '.TCL_NN,
                                ' . TBL_PRODUCTS_SIZE . '.tcl_nn_sn,
                                ' . TBL_PRODUCTS_SIZE . '.CPYT_TN,
                                ' . TBL_PRODUCTS_SIZE . '.TV_TTTBVV_OB,
                                ' . TBL_PRODUCTS_SIZE . '.DTNT_OB,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_dbh,                                    
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tvpn,                                    
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tvpn_sn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tgpn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tgpn_sn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_cppt,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tk_nv,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_sk_nv,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_cpyt_tn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_cpyt_tn_sn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tcvp,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tcvp_sn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tcmt,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_xct,
                                ' . TBL_PRODUCTS_SIZE . '.THAISAN_QLNT,
                                ' . TBL_PRODUCTS_SIZE . '.tsqlnt_ktdk,
                                ' . TBL_PRODUCTS_SIZE . '.tsqlnt_st,
                                ' . TBL_PRODUCTS_SIZE . '.tsqlnt_sm,
                                ' . TBL_PRODUCTS_SIZE . '.tsqlnt_dn,
                                ' . TBL_PRODUCTS_SIZE . '.DTNGT_OB,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_dbh,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_st1lk,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_slk,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_nk,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_cvr,
                                ' . TBL_PRODUCTS_SIZE . '.THAISAN_MDL,
                                ' . TBL_PRODUCTS_SIZE . '.tsmdl_dbh,
                                ' . TBL_PRODUCTS_SIZE . '.tsmdl_ktdk,
                                ' . TBL_PRODUCTS_SIZE . '.tsmdl_st,
                                ' . TBL_PRODUCTS_SIZE . '.tsmdl_sm,
                                ' . TBL_PRODUCTS_SIZE . '.tsmdl_dn,
                                ' . TBL_PRODUCTS_SIZE . '.NHAKHOA_MDL,
                                ' . TBL_PRODUCTS_SIZE . '.nkmdl_dbh,
                                ' . TBL_PRODUCTS_SIZE . '.nkmdl_cb,
                                ' . TBL_PRODUCTS_SIZE . '.nkmdl_db,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tvpn,';
        }

        $this->db->select($select);
        //======================================================================
        // Join
        if (isset($options['join_' . TBL_PRODUCTS . '_dbh'])) {
            $this->db->join(TBL_PRODUCTS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_ORDERS . '.OD_kh_dbh', 'left');
        }

        if (isset($options['join_' . TBL_CUSTOMERS])) {
            $this->db->join(TBL_CUSTOMERS, TBL_ORDERS . '.OD_kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        //==========================================================================
        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $this->db->join(TBL_PRODUCTS_SIZE, TBL_PRODUCTS . '.goi_bh = ' . TBL_PRODUCTS_SIZE . '.id', 'left');
        }
        //end
        //======================================================================
        //======================================================================
        // WHERE
        if (isset($options['BH_kh_mbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_mbh = ', $options['BH_kh_mbh']);
        }
        if (isset($options['mqh'])) {
            $this->db->where(TBL_PRODUCTS . '.mqh = ', $options['mqh']);
        }
        //======================================================================
        if (isset($options['bt_code'])) {
            $this->db->where(TBL_ORDERS . '.bt_code = ', $options['bt_code']);
        }
        if (isset($options['kh_dbh'])) {
            $this->db->where(TBL_ORDERS . '.OD_kh_dbh = ', $options['kh_dbh']);
        }
        if (isset($options['order_status'])) {
            $this->db->where(TBL_ORDERS . '.order_status = ', $options['order_status']);
            //------------------------------------------------------------------
            if (isset($options['OW_order_status'])) {
                $this->db->or_where(TBL_ORDERS . '.order_status = ', $options['OW_order_status']);
            }
            if (isset($options['OW1_order_status'])) {
                $this->db->or_where(TBL_ORDERS . '.order_status = ', $options['OW1_order_status']);
            }
        }
        if (isset($options['phanloai_bt'])) {
            $this->db->where(TBL_ORDERS . '.phanloai_bt = ', $options['phanloai_bt']);
        }
        if (isset($options['BT_id'])) {
            $this->db->where(TBL_ORDERS . '.id = ', $options['BT_id']);
        }
        if (isset($options['parent_id'])) {
            $this->db->where(TBL_ORDERS . '.parent_id = ', $options['parent_id']);
        }
        if (isset($options['get_bt_thanhtoan'])) {
            $a_bt_thanhtoan = array(BT_THANHTOAN,BT_DATRATIEN,BT_DONG);
            $this->db->where_in(TBL_ORDERS . '.order_status', $a_bt_thanhtoan);
        }
        //======================================================================
        if (isset($options['phanloai_kh'])) {
            $this->db->where(TBL_CUSTOMERS . '.phanloai_kh = ', $options['phanloai_kh']);
        }
        if (isset($options['mst_cmt'])) {
            $this->db->where(TBL_CUSTOMERS . '.mst_cmt = ', $options['mst_cmt']);
        }
        //======================================================================
        
        if (isset($options['or_where']) && !empty($options['or_where'])) {
            $this->db->or_where($options['or_where']);
        }
        
        if (isset($options['like']) && !empty($options['like'])) {
            $this->db->where($options['like']);
        }
        if (isset($options['ft_user'])) {
            $this->db->where(TBL_ORDERS . '.user1 = ', $options['ft_user']);
        }
        //======================================================================
        if (isset($options['order_by'])) {
            $this->db->order_by($options['order_by']);
        }
        //end
    }

    //end
    //==========================================================================
    function count_orders_by_options($options = array()) {
        $this->filter_data_orders_by_options($options);
        return $q = $this->db->count_all_results(TBL_ORDERS);
        $q->free_result();
    }

    //end
    //==========================================================================    
}

?>