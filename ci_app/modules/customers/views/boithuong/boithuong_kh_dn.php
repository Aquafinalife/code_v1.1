<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
//end
$a_phanloai_bt = get_a_phanloai_bt()
?>
<div class="box_filter" style="margin-bottom: 20px">
    <form action="<?php echo site_url(URL_BT_DN) ?>" method="get">
        <?php
        $G_hsbt = isset($_GET['hsbt']) ? $_GET['hsbt'] : '';
        $G_kh_dbh = isset($_GET['kh_dbh']) ? $_GET['kh_dbh'] : '';
        $G_goi_bh = isset($_GET['goi_bh']) ? $_GET['goi_bh'] : '';
        $G_csyt = isset($_GET['csyt']) ? $_GET['csyt'] : '';
        //$G_qlbh = isset($_GET['qlbh']) ? $_GET['qlbh'] : '';
        $G_qlbh = isset($_GET['phanloai_bt']) ? $_GET['phanloai_bt'] : '';
        $G_ma_benh = isset($_GET['ma_benh']) ? $_GET['ma_benh'] : '';
        ?>
<!--        <select name="hsbt" class="js-example-disabled-results" style="width: 160px;">
            <option value="">-- Mã số HSBT --</option>            
            <?php
            if (isset($a_ma_hsbt) && !empty($a_ma_hsbt)) {
                //$a_dn = array();
                foreach ($a_ma_hsbt as $k => $v) {
                    $selected = ($k == $G_hsbt) ? 'selected="selected"' : '';
                    ?>
                    <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                    <?php
                    //$a_dn[] = $congty_canhan;
                }
            }
            ?>
        </select>-->
        <div class="col-sm-3 col-xs-12 pl0">
            <input name="hsbt" placeholder="-- Họ tên | CMT --" value="<?php echo $G_hsbt ?>" class="form-control" style="padding: 4px;"/>
        </div>  
<!--        <select name="kh_dbh" class="js-example-disabled-results" style="margin-left: 10px !important; padding-left: 10px !important">
            <option value="">-- Họ và tên | CMT/Hộ Chiếu --</option>            
            <?php
            if (isset($a_congty_canhan) && !empty($a_congty_canhan)) {
                //$a_dn = array();
                foreach ($a_congty_canhan as $k => $v) {
                    $selected = ($k == $G_kh_dbh) ? 'selected="selected"' : '';
                    ?>
                    <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                    <?php
                    //$a_dn[] = $congty_canhan;
                }
            }
            ?>
        </select>-->
        <select name="goi_bh" class="js-example-disabled-results">
            <option value="">-- Tất cả Gói BH --</option>            
            <?php
            if (isset($goi_bh) && !empty($goi_bh)) {
                //$a_dn = array();
                foreach ($goi_bh as $index) {
                    $id = $index->id;
                    $name = $index->name;                    
                    $selected = ($id == $G_goi_bh) ? 'selected="selected"' : '';
                    ?>
                    <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                    <?php
                    //$a_dn[] = $congty_canhan;
                }
            }
            ?>
        </select>
        <select name="phanloai_bt" class="js-example-disabled-results">
            <option value="">-- Loại bồi thường --</option>            
            <?php
            if (isset($a_phanloai_bt) && !empty($a_phanloai_bt)) {
                //$a_dn = array();
                foreach ($a_phanloai_bt as $k => $v) {
                    $selected = ($k == $G_qlbh) ? 'selected="selected"' : '';
                    ?>
                    <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                    <?php
                    //$a_dn[] = $congty_canhan;
                }
            }
            ?>
        </select>
        <div class="mt20">
            <select name="csyt" class="js-example-disabled-results">
                <option value="">-- Cơ sở y tế --</option>            
                <?php
                if (isset($csyt) && !empty($csyt)) {
                    //$a_dn = array();
                    foreach ($csyt as $index) {
                        $id = $index->id;
                        $name = $index->name;
                        $csyt_code = $index->csyt_code;
                        $selected = ($id == $G_csyt) ? 'selected="selected"' : '';
                        ?>
                        <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $csyt_code.' - '.$name ?></option>
                        <?php
                        //$a_dn[] = $congty_canhan;
                    }
                }
                ?>
            </select>
        </div>

        <div class="" style="margin-top: 20px">
            <select name="ma_benh" class="js-example-disabled-results" style="margin-top: 20px;padding-top: 20px">
                <option value="">-- Mã bệnh --</option>            
                <?php
                if (isset($ma_benh) && !empty($ma_benh)) {
                    //$a_dn = array();
                    foreach ($ma_benh as $index) {
                        $id = $index->id;
                        $name = $index->name;
                        $ma_benh = $index->ma_benh;
                        $selected = ($ma_benh == $G_ma_benh) ? 'selected="selected"' : '';
                        ?>
                        <option <?php echo $selected ?> value="<?php echo $ma_benh ?>"><?php echo $ma_benh . ' - ' . $name ?></option>
                        <?php
                        //$a_dn[] = $congty_canhan;
                    }
                }
                ?>
            </select>
            <input style="margin-top: 7px;margin-left: 20px;" class="btn btn btn-inline btn-sm" type="submit" value="Tìm kiếm"/>
            <a href="/export_dn<?php echo $url_export; ?>"  style="margin-top: -1px;margin-left: 10px;" class="btn btn btn-success btn-sm"/>Xuất execel</a>
            <!--<input style="margin-top: -1px;margin-left: 20px;" class="btn btn btn-info btn-sm" type="button" value="Thêm bồi thường"/>-->
            <div></div>
        </div>
    </form>
</div>
<table id="_table-edit" class="table table-bordered table-hover">    
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Chi tiết bồi thường:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th>Mã số HSBT</th>
            <th>Khách hàng ĐBH</th>
            <th>CMT/Hộ chiếu</th>
            <?php
            if ($ss_kh_parent_id > 0) {
                ?>
    <!--                <th>Công ty chi nhánh</th>
                    <th>Phòng ban</th>-->
                <?php
            }
            ?>
            <!--<th>Bên mua BH</th>-->
            <th>Gói BH</th>
            <th>Loại BT</th>
            <th>Hình thức NTBT</th>
            <th>Ghi chú NTBT</th>
            <th>Tình trạng HSBT</th>
            <!--<th>Chứng từ chờ BS</th>-->
            <th>Ngày xảy ra tổn thất</th>
            <th>Ngày nhận HSBT</th>
            <th>Ngày bổ sung CT</th>
            <th>Ngày xử lý HSBT</th>
            <th>Ngày duyệt HSBT</th>
            <th>Ngày thông báo KH</th>
            <th>Ngày thanh toán</th>
            <th>Phương thức BT</th>
            <th>CSYT</th>
            <th>Chuẩn đoán bệnh</th>
            <th>Mã bệnh</th>
            <th>Số tiền YCBT</th>
            <th>Số ngày YCBT</th>
            <th>Số tiền ĐBT</th>
            <!--<th>Số tiền từ chối BT</th>-->
            <th>Lý do KBT</th>
            <th>Ghi chú lý do KBT</th>
            <th>Ghi chú TT</th>
            <!--<th>Hồ sơ scan</th>-->
            <!--end-->
            <!--<th>Xuất PDF</th>-->
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($bt) && !empty($bt)) {
            $j = 1;
            foreach ($bt as $index) {
                $OD_id = $index->id;
                $BT_code = $index->bt_code;
                $case = $index->case;
                //$OD_id = $index->OD_id;
                $KH_id = $index->KH_id;
                $kh_mbh = $index->kh_mbh;
                $loai_benh = $index->loai_benh;
                $loai_benh_txt = get_ma_benh($loai_benh);
                $cn_dbh = $index->congty_canhan;
                $mst_cmt = $index->mst_cmt;
                $ngaysinh = $index->ngaysinh;
                $parent_id = $index->parent_id;
                $phongban = $index->phongban;
                $goi_bh_dm = $index->goi_bh_dm;
                $so_ngay_ycbt = $index->so_ngay_ycbt;
                $so_tien_ycbt = get_price_in_vnd($index->so_tien_ycbt);
                $so_ngay_ycbt = $index->so_ngay_ycbt;
                $so_tien_dbt = get_price_in_vnd($index->so_tien_dbt);
                $phanloai_bt = $index->phanloai_bt;
                $phanloai_bt_txt = get_phanloai_bt($phanloai_bt);
                $hinhthuc_nhantien_bt = $index->hinhthuc_nhantien_bt;
                $hinhthuc_nhantien_bt_txt = get_hinhthuc_nhantien_bt($hinhthuc_nhantien_bt);

                //==============================================================
                $ghichu_hinhthuc_nhantien_bt = $index->ghichu_hinhthuc_nhantien_bt;
                $ghichu_hoso = $index->ghichu_hoso;
                $ghichu_xuly_bt = $index->ghichu_xuly_bt;
                $ghichu_khong_bt = $index->ghichu_khong_bt;
                $ghichu_pheduyet_bt = $index->ghichu_pheduyet_bt;
                $ghichu_thanhtoan_bt = $index->ghichu_thanhtoan_bt;
                //==============================================================
                $date_xayra_tonthat = substr($index->date_xayra_tonthat, 0, 10);
                $date_nhan_bt = substr($index->date_nhan_bt, 0, 10);
                $date_xuly_bt = substr($index->date_xuly_bt, 0, 10);
                $date_bosung_ct = substr($index->date_bosung_ct, 0, 10);
                $date_duyet_bt = substr($index->date_duyet_bt, 0, 10);
                $date_thongbao_kh = substr($index->date_thongbao_kh, 0, 10);
                $date_thanhtoan = substr($index->date_thanhtoan, 0, 10);
                $date_tratien = substr($index->date_tratien, 0, 10);
                //==============================================================
                $order_status = $index->order_status;
                $order_status_txt = get_orders_status($order_status);
                if($order_status== BT_THANHTOAN){
                    $order_status_txt1 = '<button class="btn btn-danger btn-sm" href="#">' . $order_status_txt . '</button>';
                }elseif ($order_status == BT_CHOBOSUNG) {
                    $order_status_txt1 = '<button class="btn btn-warning btn-sm btn_check_ctbs" href="#" data-toggle="modal" id="'.$OD_id.'" data-target=".bs-example-modal-lg">' . $order_status_txt . '</button>';
                } else {
                    $order_status_txt1 = '<button class="btn btn-primary btn-sm" href="#">' . $order_status_txt . '</button>';
                }
                $chungtu_bosung = $index->chungtu_bosung;
                if ($chungtu_bosung != '') {
                    $chungtu_bosung1 = '<a class="btn btn-warning btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg' . $OD_id . '" href="#">Chứng từ BS</a>';
                } else {
                    $chungtu_bosung1 = '';
                }
                $ndbh_quanhe = $index->ndbh_quanhe;
                $ndbh_quanhe_txt = get_ndbh_quanhe($ndbh_quanhe);
                $phuongthuc_bt = $index->phuongthuc_bt;
                $phuongthuc_bt_txt = get_phuongthuc_bt($phuongthuc_bt);
                $chuandoan_benh = $index->chuandoan_benh;
                $OD_ma_benh = $index->OD_ma_benh;
                $lydo_khong_bt = $index->lydo_khong_bt;
                $lydo_khong_bt_txt = get_lydo_khong_bt($lydo_khong_bt);
                $goi_gia_tien = ($index->goi_gia_tien);
                $csyt = $index->csyt;
                $ten_csyt = $index->ten_csyt;
                $ten_dn_bbh = $index->ten_dn_bbh;
                $hoso_scan = $index->hoso_scan;
                ?>
                <tr>
                    <td><?php echo $BT_code ?></td>
                    <td><?php echo $cn_dbh ?></td>
                    <td><?php echo $mst_cmt ?></td>
                    <?php
                    if ($parent_id > 0) {
                        ?>
            <!--                        <td>
                        <?php
                        if (isset($dn) && !empty($dn)) {
                            foreach ($dn as $index) {
                                $dn_ten = $index->congty_canhan;
                                $dn_id = $index->id;
                                if ($dn_id == $parent_id) {
                                    echo $dn_ten;
                                    break;
                                }
                            }
                        }
                        ?>
                            </td>-->
            <!--                        <td>
                        <?php
                        if (isset($a_phongban) && !empty($a_phongban)) {
                            foreach ($a_phongban as $index) {
                                $pb_name = $index->name;
                                $pb_id = $index->id;
                                if ($pb_id == $phongban) {
                                    echo $pb_name;
                                    break;
                                }
                            }
                        }
                        ?>
                            </td>-->
                        <?php
                    }
                    ?>
        <!--                    <td>
                    <?php
                    if (isset($kh) && !empty($kh)) {
                        foreach ($kh as $index) {
                            $kh_mbh_id = $index->id;
                            $kh_mbh_ten = $index->congty_canhan;
                            if ($kh_mbh_id == $kh_mbh) {
                                echo $kh_mbh_ten;
                                break;
                            }
                        }
                    }
                    ?>
                    </td>-->
                    <td><?php echo $goi_bh_dm ?></td>
                    <td><?php echo $phanloai_bt_txt; ?></td>
                    <td><?php echo $hinhthuc_nhantien_bt_txt ?></td>
                    <td><?php echo $ghichu_hinhthuc_nhantien_bt ?></td>
                    <td><?php echo $order_status_txt1 ?></td>
                    <td><?php echo $date_xayra_tonthat ?></td>
                    <td><?php echo $date_nhan_bt ?></td>
                    <td><?php echo $date_bosung_ct ?></td>
                    <td><?php echo $date_xuly_bt ?></td>
                    <td><?php echo $date_duyet_bt ?></td>
                    <td><?php echo $date_thongbao_kh ?></td>
                    <td><?php echo $date_thanhtoan ?></td>
                    <td><?php echo $phuongthuc_bt_txt ?></td>
                    <td><?php echo $ten_csyt ?></td>
                    <td><?php echo $chuandoan_benh ?></td>
                    <td>
                        <?php
                        if (isset($a_ma_benh) && !empty($a_ma_benh)) {
                            foreach ($a_ma_benh as $mb) {
                                $mb_id = $mb->id;
                                $mb_code = $mb->ma_benh;
                                if ($mb_id == $OD_ma_benh) {
                                    echo $mb_code;
                                    break;
                                }
                            }
                        }
                        ?>
                    </td>
                    <td><?php echo $so_tien_ycbt ?></td>
                    <td><?php echo $so_ngay_ycbt ?></td>
                    <td><?php echo $so_tien_dbt ?></td>
                    <td><?php echo $lydo_khong_bt_txt ?></td>
                    <td><?php echo $ghichu_khong_bt ?></td>
                    <td><?php echo $ghichu_thanhtoan_bt ?></td>
                    <!--<td><?php // echo $hoso_scan   ?></td>-->
                    <!--end-->
        <!--                    <td>
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm">
                            Xuất PDF
                        </a>
                    </td>-->
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="10" class="text-danger">Không có dữ liệu.</td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<div class="col-sm-12 text-center">
    <div class="total_row mt10">
        <strong>
            <?php
            if (isset($total_rows)) {
                echo 'Tổng số bồi thường: ' . $total_rows;
            }
            ?>
        </strong>
    </div>
    <div class="pagination">
        <?php echo $this->pagination->create_links(); ?>
    </div>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box_about_home _col-sm-12">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button> 
                <h3 class="modal-title mb0" id="myLargeModalLabel">Danh sách chứng từ chờ bổ sung.</h3> 
            </div>
            <div class="modal-body modal_bt">
                
            </div>
            <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button></div>
        </div>
    </div>
</div>