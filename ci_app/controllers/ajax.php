<?php

class Ajax extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('customers/customers_model');
        $this->load->model('products/products_model');
        $this->load->model('products/products_coupon_model');
    }

    function index() {
        
    }

    //==========================================================================

    function ajax_get_ctbs($options = array()) {
        $html = '';
        $data = $view_data = array();

        $bt_id = $this->input->post('id');

        if ($bt_id > 0) {
            // Lấy bt
            $data_get = array(
                'where' => array(
                    'id' => $bt_id,
                ),
                'get_one' => TRUE,
            );
            $bt = $this->customers_model->CommonGet(TBL_ORDERS, $data_get);

            //==================================================================
            if (is_object($bt)) {
                $phanloai_bt = $bt->phanloai_bt;
                $chungtu_bosung = $bt->chungtu_bosung;
                //--------------------------------------------------------------
                $data['ctbs'] = $chungtu_bosung;
                //==============================================================
                // Lấy chứng từ
                $data_get = array(
                    'where' => array(
                        'parent_id' => $phanloai_bt,
                    ),
                );
                $ct = $this->customers_model->CommonGet(TBL_CHUNGTU, $data_get);
                if (!empty($ct)) {
                    $data['ct'] = $ct;
                }
                //==============================================================
                $data['bt'] = $bt;
                $html .= $this->load->view('ajax/ajax_get_ctbs', $data, TRUE);
            }
        }

        echo $html;
        exit();
//        echo json_encode($res);
//        exit();
    }

    //end
    //==========================================================================

    function ajax_get_kh_dbh($options = array()) {
        $html = '';
        $data = array();
        $kh_id = $options['kh_dbh'] = $this->input->post('kh_id');
        $phanloai_bt = $data['phanloai_bt'] = $this->input->post('phanloai_bt');

        $options['lang'] = 'vi';

        $view_data = array();

        //$bh_cl     = $this->products_model->get_bh_con_lai($options);
        if ($phanloai_bt > 0) {
            // Lấy thông tin bh
            $data_get = array(
                'kh_dbh' => $kh_id,
                'join_' . TBL_PRODUCTS_SIZE => TRUE,
                'join_' . TBL_CUSTOMERS . '_dbh' => TRUE,
                'get_one' => TRUE,
            );
            $bh = $this->customers_model->get_bh($data_get);
//        echo '<pre>';
//        print_r($bh);
//        die;
            if (is_object($bh)) {
                $data['bh'] = $bh;
                $html .= $this->load->view('orders/ad/ad_ajax_bh', $data, TRUE);
            }
        }

        echo $html;
        exit();
//        echo json_encode($res);
//        exit();
    }

    //end
    //==========================================================================
    function ajax_get_chungtu($options = array()) {
        $html = '';
        $pl_bt_id = $this->input->post('pl_bt_id');
        if ($pl_bt_id > 0) {
            // Lấy dữ liệu chứng từ
//            if ($pl_bt_id == BT_TVTTTBVV_OB) {
//                // Hai loại bt chung chứng từ
//                $pl_bt_id = BT_TVTTTBVV_TN;
//            }
            //==================================================================
            $data_get = array(
                'parent_id' => $pl_bt_id,
            );
            $ct = $this->products_coupon_model->get_products_coupon($data_get);
            if (!empty($ct)) {
                $data['ct'] = $ct;
                $html .= $this->load->view('orders/ad/ad_ajax_ct', $data, TRUE);
            }
        }
        echo $html;
        exit();
    }

    //end
    //==========================================================================
    function ajax_get_plct($options = array()) {
        $html = '';
        $a_pl_bt = get_a_phanloai_bt();
        if (!empty($a_pl_bt)) {
            $html .= '<label class="col-sm-4 col-xs-12" for="exampleInputEmail1">Chọn loại bồi thường:</label>';
            $html .= '<select name="phanloai_bt" class="js-example-disabled-results select_loai_bt">';
            $html .= '<option value="">-- Chọn loại bồi thường --</option> ';
            foreach ($a_pl_bt as $k => $v) {
                $html .= '<option value="' . $k . '">' . $v . '</option> ';
            }
            $html .= '</select>';
        }
        echo $html;
        exit();
    }

    //end
    //==========================================================================
    function ajax_get_form_sent_email_ct_bt($options = array()) {
        $html = '';
        $selected_value = $this->input->post('selected_value', TRUE);
        $data['a_ct_selected'] = $selected_value;

        $pl_bt = $this->input->post('pl_bt', TRUE);
        // Lấy dữ liệu chứng từ
//        if ($pl_bt == BT_TVTTTBVV_OB) {
//            // Hai loại bt chung chứng từ
//            $pl_bt = BT_TVTTTBVV_TN;
//        }
        //======================================================================
        $data_get = array(
            'parent_id' => $pl_bt,
        );
        $ct = $this->products_coupon_model->get_products_coupon($data_get);
        if (!empty($ct)) {
            $data['ct'] = $ct;

            $html .= $this->load->view('orders/ad/ad_ajax_sent_email_ct_bt', $data, TRUE);
        }
        echo $html;
        exit();
    }

    //end
}

?>