<?php
$cl_ql_bt_2 = '.box_nhap_thongtin_bt_';
$checked = 'checked="checked"';
$disabled_df = '';
//==============================================================================
$a_bt_status1 = get_a_bt_status1();
$a_quyenloi_bt1 = get_a_phanloai_bt1();
$a_quyenloi_bt2 = get_a_phanloai_bt2();
//==============================================================================
if (isset($bt) && is_object($bt)) {
    $quyenloi_bt = $bt->quyenloi_bt;
    $phanloai_bt = $bt->phanloai_bt;
    $so_tien_ycbt = $bt->so_tien_ycbt;
    $so_tien_dbt = $bt->so_tien_dbt;
    $so_ngay_ycbt = $bt->so_ngay_ycbt;
    $so_ngay_dbt = $bt->so_ngay_dbt;
    $bt_status = $bt->order_status;
    if (in_array($bt_status, $a_bt_status1)) {
        $disabled_df = 'disabled = true';
    }
}
//==============================================================================
$disabled_check = isset($disabled) ? $disabled : '';
$phanloai_bt = isset($phanloai_bt) ? $phanloai_bt : '';
//end
//==============================================================================
if (isset($bh) && is_object($bh)) {
    $TV_TTTBVV_TN = $bh->TV_TTTBVV_TN;
    $TCL_NN = $bh->TCL_NN;
    $tcl_nn_sn = $bh->tcl_nn_sn;
    $CPYT_TN = $bh->CPYT_TN;
    $TV_TTTBVV_OB = $bh->TV_TTTBVV_OB;
    $DTNT_OB = $bh->DTNT_OB;
    $dtnt_dbh = $bh->dtnt_dbh;
    $dtnt_tvpn = $bh->dtnt_tvpn;
    $dtnt_tvpn_sn = $bh->dtnt_tvpn_sn;
    $dtnt_tgpn = $bh->dtnt_tgpn;
    $dtnt_tgpn_sn = $bh->dtnt_tgpn_sn;
    $dtnt_cppt = $bh->dtnt_cppt;
    $dtnt_tk_nv = $bh->dtnt_tk_nv;
    $dtnt_sk_nv = $bh->dtnt_sk_nv;
    $dtnt_cpyt_tn = $bh->dtnt_cpyt_tn;
    $dtnt_cpyt_tn_sn = $bh->dtnt_cpyt_tn_sn;
    $dtnt_tcvp = $bh->dtnt_tcvp;
    $dtnt_tcvp_sn = $bh->dtnt_tcvp_sn;
    $dtnt_tcmt = $bh->dtnt_tcmt;
    $dtnt_xct = $bh->dtnt_xct;
    $THAISAN_QLNT = $bh->THAISAN_QLNT;
    $tsqlnt_ktdk = $bh->tsqlnt_ktdk;
    $tsqlnt_st = $bh->tsqlnt_st;
    $tsqlnt_sm = $bh->tsqlnt_sm;
    $tsqlnt_dn = $bh->tsqlnt_dn;
    $DTNGT_OB = $bh->DTNGT_OB;
    $dtngt_dbh = $bh->dtngt_dbh;
    $dtngt_st1lk = $bh->dtngt_st1lk;
    $dtngt_slk = $bh->dtngt_slk;
    $dtngt_nk = $bh->dtngt_nk;
    $dtngt_cvr = $bh->dtngt_cvr;
    $THAISAN_MDL = $bh->THAISAN_MDL;
    $tsmdl_dbh = $bh->tsmdl_dbh;
    $tsmdl_ktdk = $bh->tsmdl_ktdk;
    $tsmdl_st = $bh->tsmdl_st;
    $tsmdl_sm = $bh->tsmdl_sm;
    $tsmdl_dn = $bh->tsmdl_dn;
    $NHAKHOA_MDL = $bh->NHAKHOA_MDL;
    $nkmdl_dbh = $bh->nkmdl_dbh;
    $nkmdl_cb = $bh->nkmdl_cb;
    $nkmdl_db = $bh->nkmdl_db;
    $st_bh_gh = $bh->$quyenloi_bt;
    ?>
    <?php
    //==========================================================================
//    if (isset($qlbt) && count($qlbt) == 1) {
//        foreach ($qlbt as $index) {
//            $QLBT_name = $index->QLBT_name;
//            $qlbt_st_ycbt = $index->qlbt_st_ycbt;
//            $qlbt_st_dbt = $index->qlbt_st_dbt;
//        }
//    }
//    $QLBT_name = isset($QLBT_name) ? $QLBT_name : '';
//    $qlbt_st_ycbt = isset($qlbt_st_ycbt) ? $qlbt_st_ycbt : '';
//    $qlbt_st_dbt = isset($qlbt_st_dbt) ? $qlbt_st_dbt : '0';
    //==========================================================================
    $bt_nkmdl_db = $bt_nkmdl_cb = $bt_nkmdl_cvr = '';
    $nkmdl_db_st_ycbt = $nkmdl_cb_st_ycbt = $nkmdl_cvr_st_ycbt = '';
    $nkmdl_db_st_dbt = $nkmdl_cb_st_dbt = $nkmdl_cvr_st_dbt = '';
    // Lấy các qlbt con được chọn
    if (isset($qlbt) && !empty($qlbt)) {
        foreach ($qlbt as $index) {
            $QLBT_name_2 = $index->QLBT_name;
            $qlbt_st_ycbt_2 = $index->qlbt_st_ycbt;
            $qlbt_st_dbt_2 = $index->qlbt_st_dbt;
            //==================================================================
            if ($QLBT_name_2 == nkmdl_cb) {
                $bt_nkmdl_cb = 1;
                $nkmdl_cb_st_ycbt = $qlbt_st_ycbt_2;
                $nkmdl_cb_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == nkmdl_db) {
                $bt_nkmdl_db = 1;
                $nkmdl_db_st_ycbt = $qlbt_st_ycbt_2;
                $nkmdl_db_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == nkmdl_cvr) {
                $bt_nkmdl_cvr = 1;
                $nkmdl_cvr_st_ycbt = $qlbt_st_ycbt_2;
                $nkmdl_cvr_st_dbt = $qlbt_st_dbt_2;
            }
        }
    }
    //==========================================================================    
    //==========================================================================
    // Nếu đã bồi thường
    if (isset($qlbt_cl) && !empty($qlbt_cl)) {
        foreach ($qlbt_cl as $index) {
            $CL_quyenloi_bt = $index->CL_quyenloi_bt;
            $CL_st_bh_cl = $index->CL_st_bh_cl;
            $CL_sn_bh_cl = $index->CL_sn_bh_cl;
            //==================================================================
            if ($quyenloi_bt == $CL_quyenloi_bt) {
                $CL_st_bh_cl_t = $CL_st_bh_cl;
            }
        }
    } else {
        // Nếu chưa bt lần nào
        // Nếu bt có số ngày
        if (in_array($quyenloi_bt, $a_qlbt_sn)) {
            $field_quyenloi_bt_sn = strtolower($quyenloi_bt) . '_sn';
            $sn_bh_gh = $bh->$field_quyenloi_bt_sn;
            $st_bh_gh_sn = (int) $st_bh_gh * (int) $sn_bh_gh;
            //==================================================================            
        }
    }
    //end
    //==========================================================================
    $st_bh_cl_t = isset($CL_st_bh_cl_t) ? $CL_st_bh_cl_t : (isset($st_bh_gh_sn) ? $st_bh_gh_sn : (isset($st_bh_gh) ? $st_bh_gh : 0));
    echo $st_bh_cl_t . '/';
    //==========================================================================
    ?>
    <div class="goi_bao_hiem">
        <div class="col-sm-12 mb20 mt20">
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_TVTTTBVV_TN) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            TỬ VONG, TTTBVV do tai nạn
                        </div>
                        <!--                        <div class="col-xs-2 col-sm-2 text-primary">
                                                    Số tiền bh còn lại
                                                </div>-->
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền yêu cầu bt
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Số tiền bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            <input 
                            <?php
                            if ($QLBT_name == 'tv_tttbvv_tn_tv') {
                                echo $checked;
                            } else {
                                echo $disabled_df;
                            }
                            ?>
                                type="radio" class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tv_tttbvv_tn_tv' ?>" name="quyenloi_bt_2" <?php echo $disabled_check ?> value="tv_tttbvv_tn_tv"> 
                            Tử vong
                        </div>
                        <?php
                        if ($QLBT_name == 'tv_tttbvv_tn_tv') {
                            ?>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tv_tttbvv_tn_tv">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($so_tien_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($qlbt_st_dbt) ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            <input <?php
                            if ($QLBT_name == 'tv_tttbvv_tn_tt') {
                                echo $checked;
                            } else {
                                echo $disabled_df;
                            }
                            ?>  type="radio" class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tv_tttbvv_tn_tt' ?>" name="quyenloi_bt_2" <?php echo $disabled_check ?> value="tv_tttbvv_tn_tt"> 
                            Thương tật
                        </div>

                        <?php
                        if ($QLBT_name == 'tv_tttbvv_tn_tt') {
                            ?>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tv_tttbvv_tn_tt">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($so_tien_ycbt) ?>
                                    <!--<input type="number" name="st_ycbt_2"  <?php echo $disabled_df ?> value="<?php echo get_price_in_vnd($TV_TTTBVV_TN_tt_cl) ?>" />-->
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($qlbt_st_dbt) ?>
                                    <!--<input type="number" class="st_dbt_2" name="tv_tttbvv_tn_tt_st_dbt" value="" max="<?php echo $TV_TTTBVV_TN_cl ?>" min="0"/>-->
                                </div>
                            </div> 
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_NHAKHOA_MDL) {
                    ?>
                    <?php
                    if ($st_bh_cl_t == 0) {
                        $st_dbt_fm = 'disabled="disabled"';
                        ?>
                        <div class="alert alert-warning">
                            <strong class="text-danger"><label class="glyphicon glyphicon-remove"></label>Đã hết quyền lợi bồi thường</strong>
                        </div>
                        <?php
                    } else {
                        $st_dbt_fm = '';
                        ?>
                        <div class="alert alert-success">
                            <strong class="text-success">
                                <label class="glyphicon glyphicon-ok"></label>
                                Số tiền bồi thường giới hạn: <span class="btn btn-success"><?php echo get_price_in_vnd($st_bh_cl_t) ?></span>
                            </strong>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Nha khoa mua độc lập
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền yêu cầu bt
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Số tiền bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <?php
                        if ($bt_nkmdl_cb == 1) {
                            ?>
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" name="quyenloi_bt_2" value="nkmdl_cb" checked="checked"/>
                                Nha khoa cơ bản
                            </div>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_nkmdl_cb" style="display: _none">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_cb_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_cb_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="nkmdl_cb_st_dbt" value="<?php echo $nkmdl_cb_st_dbt ?>" max="<?php echo $nkmdl_cb_st_dbt ?>" min="<?php echo $nkmdl_cb_st_dbt ?>"/>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_nkmdl_db == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" name="quyenloi_bt_2" value="nkmdl_db" checked="checked"/>
                                Nha khoa đặc biệt
                            </div>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_nkmdl_db" style="display: _none">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_db_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_db_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="nkmdl_db_st_dbt" value="<?php echo $nkmdl_db_st_dbt ?>" max="<?php echo $nkmdl_db_st_dbt ?>" min="<?php echo $nkmdl_db_st_dbt ?>"/>
                                </div>
                            </div> 
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_nkmdl_cvr == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" name="" checked="checked" disabled="disabled" value="1" class="checkbox_nhap_qlbt" cl="<?php echo $cl_ql_bt_2 . 'nkmdl_cvr' ?>" />
                                <input type="hidden" name="nkmdl_cvr" value="1"/>
                                Cạo vôi răng
                            </div>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_nkmdl_cvr" style="display: _none">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_cvr_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_cvr_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="nkmdl_cvr_st_dbt" value="<?php echo $nkmdl_cvr_st_dbt ?>" max="<?php echo $nkmdl_cvr_st_dbt ?>" min="<?php echo $nkmdl_cvr_st_dbt ?>"/>
                                </div>
                            </div> 

                        </div>
                        <?php
                    }
                    ?>

                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_TVTTTBVV_OB) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            TỬ VONG, TTTBVV do ốm bệnh
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền yêu cầu bt
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Số tiền bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            <input 
                            <?php
                            if ($QLBT_name == 'tv_tttbvv_ob_tv') {
                                echo $checked;
                            } else {
                                echo $disabled_df;
                            }
                            ?>
                                type="radio" class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tv_tttbvv_ob_tv' ?>" name="quyenloi_bt_2" <?php echo $disabled_check ?> value="tv_tttbvv_ob_tv"> 
                            Tử vong
                        </div>
                        <?php
                        if ($QLBT_name == 'tv_tttbvv_ob_tv') {
                            ?>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tv_tttbvv_ob_tv">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($so_tien_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($qlbt_st_dbt) ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            <input <?php
                            if ($QLBT_name == 'tv_tttbvv_ob_tt') {
                                echo $checked;
                            } else {
                                echo $disabled_df;
                            }
                            ?>  type="radio" class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tv_tttbvv_ob_tt' ?>" name="quyenloi_bt_2" <?php echo $disabled_check ?> value="tv_tttbvv_ob_tt"> 
                            Thương tật
                        </div>

                        <?php
                        if ($QLBT_name == 'tv_tttbvv_ob_tt') {
                            ?>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tv_tttbvv_ob_tt">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($so_tien_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($qlbt_st_dbt) ?>
                                </div>
                            </div> 
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_TCL_NN) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Trợ cấp lương ngày nghỉ
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Giá trị yêu cầu bồi thường
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Giá trị chấp nhận bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Số tiền
                        </div>
                        <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tcl_nn" style="display: _none">
                            <div class="col-xs-2 col-sm-2">
                                <?php echo get_price_in_vnd($so_tien_ycbt) ?>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <?php echo get_price_in_vnd($qlbt_st_dbt) ?>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Số ngày
                        </div>
                        <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tcl_nn_sn" style="display: _none">
                            <div class="col-xs-2 col-sm-2">
                                <?php echo ($so_ngay_ycbt) ?> ngày
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <?php echo ($so_ngay_dbt) ?> ngày
                            </div>
                        </div> 

                    </div>
                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_CPYT_TN) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Chi phí y tế do tai nạn
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Giá trị yêu cầu bồi thường
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Giá trị chấp nhận bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Số tiền
                        </div>
                        <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tcl_nn" style="display: _none">
                            <div class="col-xs-2 col-sm-2">
                                <?php echo get_price_in_vnd($so_tien_ycbt) ?>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <?php echo get_price_in_vnd($qlbt_st_dbt) ?>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
        </div>
    </div>
    <?php
}
?>