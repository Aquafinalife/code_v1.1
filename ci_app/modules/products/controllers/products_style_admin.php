<?php

class Products_Style_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        //modules::run('auth/auth/validate_login');
        $this->_layout = 'admin_ui/layout/main';
    }
    //==========================================================================
    function browse() {
        $options = array();

        $csyt = $this->products_style_model->CommonGet(TBL_CSYT);
        if (!empty($csyt)) {
            $options['csyt'] = $csyt;
        }
        //======================================================================
        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $options['options'] = $options;
        }
        //======================================================================
        $tpl = 'ad/csyt/csyt_list';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Quản lý Cơ sơ y tế' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function add() {
        $options = array();
        // Xu ly viec them vao db
        if ($this->is_postback()) {
            if (!$this->_do_add()) {
                $options['error'] = validation_errors();
                $options['options'] = $options;
            }
        }
        //======================================================================
        // Lấy tỉnh thành
        $data_get = array();
        $cities = $this->products_style_model->CommonGet(products_state);
        if (!empty($cities)) {
            $options['a_cities'] = $cities;
        }
        //======================================================================
        $options['scripts'] = $this->scripts_select2();
        $options['header'] = 'Thêm Cơ sơ y tế';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_STYLE_ADMIN_ADD_URL;
        //======================================================================
        $tpl = 'ad/csyt/csyt_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Thêm Cơ sơ y tế' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _do_add() {
        $this->form_validation->set_rules('name', 'Cơ sơ y tế', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('name_en', 'Cơ sơ y tế - TA', 'trim|xss_clean|max_length[255]');
        $this->form_validation->set_rules('csyt_code', 'Mã cơ sơ y tế', 'trim|xss_clean|max_length[255]');
        $this->form_validation->set_rules('type', 'Phân loại', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('address', 'Địa chỉ', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('address_en', 'Địa chỉ - TA', 'trim|xss_clean|max_length[255]');
        $this->form_validation->set_rules('cities', 'Tỉnh thành', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('username', 'Tên đăng nhập', 'trim|xss_clean|max_length[255]|is_unique[products_style.username]');
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|xss_clean|max_length[255]');
        $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|max_length[255]');
        $this->form_validation->set_rules('phone', 'Điện thoại', 'trim|xss_clean|max_length[255]');
        $this->form_validation->set_rules('hotline', 'Hotline', 'trim|xss_clean|max_length[255]');
        $this->form_validation->set_rules('fax', 'Fax', 'trim|xss_clean|max_length[255]');
        $this->form_validation->set_rules('website', 'Website', 'trim|xss_clean|max_length[255]');
        $this->form_validation->set_rules('contact', 'Người liên hệ', 'trim|xss_clean|max_length[255]');
        //======================================================================
        if ($this->form_validation->run()) {
            $data = array(
                'name' => my_trim($this->input->post('name')),
                'name_en' => my_trim($this->input->post('name_en')),
                'csyt_code' => my_trim($this->input->post('csyt_code')),
                'type_id' => my_trim($this->input->post('type')),
                'city_id' => my_trim($this->input->post('cities')),
                'username' => my_trim($this->input->post('username')),
                'email' => my_trim($this->input->post('email')),
                'password' => (my_trim($this->input->post('password'))),
                'password_fm' => md5(my_trim($this->input->post('password'))),
                'parent_id' => my_trim($this->input->post('categories')),
                //
                'phone' => my_trim($this->input->post('phone')),
                'hotline' => my_trim($this->input->post('hotline')),
                'fax' => my_trim($this->input->post('fax')),
                'website' => my_trim($this->input->post('website')),
                'address' => my_trim($this->input->post('address')),
                'address_en' => my_trim($this->input->post('address_en')),
                'contact' => my_trim($this->input->post('contact')),
                //
                'status' => STATUS_ACTIVE,
                'editor' => $this->phpsession->get('user_id'),
            );
//            echo '<pre>';
//            print_r($data);
//            die;
            $csyt_id = $this->input->post('id',TRUE);
            if($csyt_id > 0){
                $data['id']= $csyt_id;
                $this->products_style_model->update($data);
            }else{
                $data['creator'] = $this->phpsession->get('user_id');
                $this->products_style_model->insert($data);
            }
            redirect(PRODUCTS_STYLE_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //==========================================================================
    function edit() {
        $options = array();
        if ($this->is_postback()) {
            if (!$this->input->post('from_list')) {
                if (!$this->_do_add()) {
                    $options['error'] = validation_errors();
                    $options['options'] = $options;
                }
            }
        }
        //======================================================================
        $id = $this->input->post('id');
        if ($id > 0) {
            $csyt = $this->products_style_model->get_products_style(array('id' => $id));
            $options['csyt'] = $csyt;
        } else {
            redirect(PRODUCTS_STYLE_ADMIN_BASE_URL);
        }
        //======================================================================
        // Lấy tỉnh thành
        $data_get = array();
        $cities = $this->products_style_model->CommonGet(products_state);
        if (!empty($cities)) {
            $options['a_cities'] = $cities;
        }
        //======================================================================
        $options['scripts'] = $this->scripts_select2();
        $options['header'] = 'Sửa Cơ sơ y tế';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_STYLE_ADMIN_EDIT_URL;

        //======================================================================
        $tpl = 'ad/csyt/csyt_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Sửa Cơ sơ y tế' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _get_edit_form_data(&$options = array()) {
        $id = $this->input->post('id');
        if ($this->input->post('from_list')) {
            $products_style = $this->products_style_model->get_products_style(array('id' => $id));
            $options['name'] = $products_style->name;
        } else {
            $options['name'] = $this->input->post('name');
        }
        $options['id'] = $id;
        $options['header'] = 'Sửa Cơ sơ y tế';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_STYLE_ADMIN_EDIT_URL;
    }

    //==========================================================================
    private function _do_edit() {
        $this->form_validation->set_rules('name', 'Cơ sơ y tế', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('username', 'Tên đăng nhập', 'trim|required|xss_clean|max_length[255]|callback_check_username');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|max_length[255]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('phone', 'Điện thoại', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('address', 'Địa chỉ', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('contact', 'Người liên hệ', 'trim|required|xss_clean|max_length[255]');
        //======================================================================
        if ($this->form_validation->run($this)) {
            $data = array(
                'id' => $this->input->post('id'),
                'name' => my_trim($this->input->post('name')),
                'username' => my_trim($this->input->post('username')),
                'email' => my_trim($this->input->post('email')),
                'password' => (my_trim($this->input->post('password'))),
                'password_fm' => md5(my_trim($this->input->post('password'))),
                'parent_id' => my_trim($this->input->post('categories')),
                //
                'phone' => my_trim($this->input->post('phone')),
                'address' => my_trim($this->input->post('address')),
                'contact' => my_trim($this->input->post('contact')),
                //
                'editor' => $this->phpsession->get('user_id'),
            );
            //==================================================================
//            echo '<pre>';
//            print_r($data);
//            die;
            $this->products_style_model->update($data);
            redirect(PRODUCTS_STYLE_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //end
    //==========================================================================
    public function check_username($str) {
        if ($str != '') {
            // Lấy thông tin csyt
            $data_get = array(
                'email' => $str,
            );
            $count_dn_bbh = $this->products_trademark_model->get_products_trademark_count($data_get);
            if ($count_dn_bbh >= 2) {
                $this->form_validation->set_message('check_email', 'Email đã tồn tại');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    //end
    //==========================================================================
    function delete() {
        if ($this->is_postback() && $this->input->post('from_list')) {
            $id = $this->input->post('id');
            $check1 = $this->products_model->get_products_count(array('style_id' => $id));
            if (empty($check1)) {
                $this->products_style_model->delete($id);
            }
            redirect(PRODUCTS_STYLE_ADMIN_BASE_URL);
        }
    }

}
