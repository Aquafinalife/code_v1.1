<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// FRONT-END
$route['^thong-tin-ca-nhan$']                       = 'customers/get_infomation_customers';
$route['^quan-ly-don-hang$']                        = 'customers/manager_order/1';
$route['^quan-ly-don-hang/trang-(\d+)$']            = 'customers/manager_order/$1';
$route['^quan-ly-don-hang/chi-tiet-don-hang$']      = 'customers/detail_order_ajax';

$route['^tao-tai-khoan|sign-up$']                   = 'customers/customers_sign_up';
$route['^dang-nhap-thanh-vien|customers-login$']    = 'customers/login_customers';
$route['^dang-xuat$']                               = 'customers/logout';

$route['^get_data_auto_complate_custom$']                               = 'customers/get_data_auto_complate_custom';
$route['^get_data_auto_complate_custom_duoc$']                          = 'customers/get_data_auto_complate_custom_duoc';


$route['^dashboard/customers/add-kh-cn']                            = 'customers/customers_admin/add_kh_cn';
$route['^dashboard/customers/add-kh-dn']                            = 'customers/customers_admin/add_kh_dn';

// BACK-END


$route['^dashboard/kh']                                            = 'customers/customers_admin/get_kh/0';
$route['^dashboard/kh/(\d+)$']                                     = 'customers/customers_admin/get_kh/$1';

// Pagination

$route['^bbh-dashboard']                                            = 'customers/bbh_dashboard/0';
$route['^bbh-dashboard/(\d+)$']                                     = 'customers/bbh_dashboard/$1';

$route['^bbh-bh']                                                   = 'customers/get_bbh_bh/0';
$route['^bbh-bh/(\d+)$']                                            = 'customers/get_bbh_bh/$1';

$route['^bbh-bt']                                                   = 'customers/get_bbh_bt/0';
$route['^bbh-bt/(\d+)$']                                            = 'customers/get_bbh_bt/$1';

// Cơ sở y tế

$route['^csyt-dashboard']                                            = 'customers/csyt_dashboard/0';
$route['^csyt-dashboard/(\d+)$']                                     = 'customers/csyt_dashboard/$1';

$route['^csyt-bh']                                                   = 'customers/get_csyt_bh/0';
$route['^csyt-bh/(\d+)$']                                            = 'customers/get_csyt_bh/$1';

$route['^csyt-bt']                                                   = 'customers/get_csyt_bt/0';
$route['^csyt-bt/(\d+)$']                                            = 'customers/get_csyt_bt/$1';

//end
//==============================================================================
//FRONTEND

$route['^change-password']                                                      = 'customers/change_password/0';


$route['^quan-tri-khach-hang']                                                  = 'customers/quan_tri_khach_hang/0';
$route['^quan-tri-khach-hang/(\d+)$']                                           = 'customers/quan_tri_khach_hang/$1';


// Bồi thường
$route['^boi-thuong-cn']                                                        = 'customers/get_bt_cn/0';
$route['^boi-thuong-cn/(\d+)$']                                                 = 'customers/get_bt_cn/$1';

$route['^boi-thuong-dn']                                                        = 'customers/get_bt_dn/0';
$route['^boi-thuong-dn/(\d+)$']                                                 = 'customers/get_bt_dn/$1';

// Bảo hiểm
$route['^bao-hiem-cn']                                                          = 'customers/get_bh_cn/0';
$route['^bao-hiem-cn/(\d+)$']                                                   = 'customers/get_bh_cn/$1';

$route['^bao-hiem-dn']                                                          = 'customers/get_bh_dn/0';
$route['^bao-hiem-dn/(\d+)$']                                                   = 'customers/get_bh_dn/$1';

$route['^export_dn']                                                            = 'customers/export_dn';

$route['^export_cn']                                                            = 'customers/export_cn';

$route['^export_bh_dn']                                                         = 'customers/export_bh_dn';