<?php
class Menus extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function get_main_menus($options = array())
    {
        $this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
        if ($detect->isMobile() && !$detect->isTablet()) {
            $isMobile = true;
        } else {
            $isMobile = false;
        }
        if(isset($options['menu_type']) && $options['menu_type'] <> BACK_END_MENU_CAT_ID){
            $options['lang'] = switch_language($this->uri->segment(1));
        }else{
            $options['lang'] = 'vi';
        }
        $menus = get_cache('menus');
        if(!$menus){
            $options['menus'] = $this->menus_model->get_menus($options);
        }
        if(isset($menus)){
            //check da dang nhap hay chua, neu da dang nhap thi se hien menu duoc danh dau private(noi bo)
            $is_logged_in = modules::run('auth/auth/is_logged_in');
            if(!$is_logged_in){
                foreach($menus as $menu){
                    if($menu['lang'] == $options['lang'] && $menu['private'] == STATUS_INACTIVE){
                        $options['menus'][] = $menu;
                    }
                }
            }else{
                foreach($menus as $menu){
                    if($menu['lang'] == $options['lang']){
                        $options['menus'][] = $menu;
                    }
                }
            }
        }

        //$options['menus'] = $menus;
        $options['parent_id'] = MENU_PARENT_ID;
        if(isset($options['menu_type']) && $options['menu_type'] == BACK_END_MENU_CAT_ID){
            $options['css'] = 'menu';
            $top_menus = '<ul id="nav">';
        }elseif(isset($options['menu_type']) && $options['menu_type'] == FRONT_END_MENU_TOP_CAT_ID && !isset($options['footer_menu'])){
            
            if(empty($options['menu_mega'])){ 
                $top_menus = '<ul class="nav navbar-nav">';
            }else{
                $top_menus = '<ul class="vertical-menu-list">';
            }
            
//            $active = ((isset($options['current_menu']) && $options['current_menu'] == '/') || (isset($params['active_menu']) && $options['active_menu'] == '/')) ? ' class="active"' : '';
//            $top_menus .= '<li'.$active.'><a href="/" class="fa fa-home top_menu_home"></a></li>';
        }elseif(isset($options['menu_type']) && $options['menu_type'] == FRONT_END_SIDE_LEFT_CAT_ID && !isset($options['footer_menu'])){
            $top_menus = '<ul id="menuside" class="smf smf-vertical sm-style sm-style-vertical">';
        }elseif(isset($options['menu_type']) && $options['menu_type'] == FRONT_END_SIDE_RIGHT_CAT_ID && !isset($options['footer_menu'])){
            $top_menus = '<ul id="menuside" class="smf smf-vertical sm-style sm-style-vertical">';
        }else{
            $top_menus = '<ul>';
        }
        if(!empty($options['menu_mega'])){
            $top_menus .= $this->_visit_mega_menus($options);
        }else{
            $top_menus .= $this->_visit_main_menus($options);
        }
        
        $top_menus .= '</ul>';
        if(isset($options['menu_type']) && $options['menu_type'] == FRONT_END_MENU_TOP_CAT_ID && !isset($options['footer_menu'])){
            $top_menus  = str_replace('<ul class="dropdown-menu"></ul>', '', $top_menus);
            $top_menus  = str_replace('<ul></ul>', '', $top_menus);
            $top_menus  = str_replace('<div class="vertical-dropdown-menu"><div class="vertical-groups col-sm-12"></div></div>', '', $top_menus);
        }else{
            $top_menus  = str_replace('<ul class="navsub"></ul>', '', $top_menus);
        }
        return $top_menus;
    }

    private function _visit_main_menus($params = array())
    {
        $this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
        if ($detect->isMobile() && !$detect->isTablet()) {
            $isMobile = 1;
        } else {
            $isMobile = 2;
        }
        
        $output                 = '';
        $sub_menus              = $this->_get_sub_menus($params);
        //admin menus check roles
        if($this->phpsession->get('is_logged_in') == TRUE && isset($params['menu_type']) && $params['menu_type'] == BACK_END_MENU_CAT_ID){
            $roles_menus = modules::run('auth/auth/get_roles_menus_disabled');
        }
        if(isset($sub_menus)){
            foreach($sub_menus as $menu)
            {
                $params['parent_id'] = $menu['id'];
                $active = ((isset($params['current_menu']) && $menu['url_path']===$params['current_menu']) || (isset($params['active_menu']) && $menu['url_path']===$params['active_menu'])) ? ' class="active"' : '';
                $check = $this->_check_sub_menus($params);
                $hash = ($check)? ' class="hash"' : '';
                if(isset($params['menu_type']) && $params['menu_type'] == BACK_END_MENU_CAT_ID){
                    //admin menus check roles
                    if(!in_array_r($menu['url_path'], $roles_menus)){
                        if (!is_null($menu['css'])){$css = '<i class="fa '.$menu['css'].'"></i> ';}else{$css = '';}
                        $output .= '<li><a href="' . $menu['url_path'] . '">' . $css . $menu['caption'] . '</a>';
                        $output .= '<ul>';
                        $output .= $this->_visit_main_menus($params);
                        $output .= '</ul></li>';
                    }
                }else{
                    $menu_lang = ($menu['lang']<>DEFAULT_LANGUAGE)?'/'.$menu['lang']:'';
                    $output .= '<li' . $active . $hash . '><a' . $active . ' title="' . $menu['caption'] .'" href="' . $menu_lang . $menu['url_path'] . '">' . $menu['caption'] . '</a>';
                    if(!isset($params['footer_menu'])){
                        if(isset($params['menu_type']) && $params['menu_type'] == FRONT_END_MENU_TOP_CAT_ID && !isset($params['footer_menu'])){
                            if($isMobile == 1){ 
                                $output .= '<ul class="dropdown-menu">';
                            }else{
                                $output .= '<ul>';
                            }
                            
                        }else{
                            $output .= '<ul class="navsub">';
                        }
                        $output .= $this->_visit_main_menus($params);
                        $output .= '</ul></li>';
                    }else {
                        $output .= '</li>';
                    }
                }
            }
        }
        return $output;
    }

    private function _get_sub_menus($params = array())
    {
        $menus      = $params['menus'];
        $sub_menus  = array();
        if(isset($menus)){
            foreach($menus as $index => $menu)
            {
                if ($menu['parent_id'] == $params['parent_id'] && $menu['cat_id'] == $params['menu_type']) {
                    $sub_menus[$index] = $menu;
                }
            }
        }
        return $sub_menus;
    }
    
    private function _check_sub_menus($params = array())
    {
        $menus      = $params['menus'];
        $sub_menus  = array();
        if(isset($menus)){
            foreach($menus as $index => $menu)
            {
                if ($menu['parent_id'] == $params['parent_id'] && $menu['cat_id'] == $params['menu_type']) {
                    $sub_menus[$index] = $menu;
                }
            }
        }
        return (!empty($sub_menus))?TRUE:FALSE;
    }
    
    function get_menu_data($options = array())
    {
        $options['is_cache'] = TRUE;
        $menu = $this->menus_model->get_menus($options);
        return $menu;
    }
    
    private function _visit_mega_menus($params = array()) {
        $output = '';
        // lay ra tat ca menu cung cap va cung loai
        $sub_menus = $this->_get_sub_menus($params);
        if (isset($sub_menus)) {
            foreach ($sub_menus as $menu) {
                $params['parent_id'] = $menu['id'];
                $style = ((isset($params['current_menu']) && $menu['url_path'] === $params['current_menu']) || (isset($params['active_menu']) && $menu['url_path'] === $params['active_menu'])) ? ' class="active"' : '';
                // kiem tra tat ca thang nao co parent_id = id cua menu cha neu co tra lai true || false
                $check = $this->_check_sub_menus($params);
                $hash = ($check) ? ' class="parent"' : '';
                $icon = $menu['icon'];
                if(!empty($icon)){
                    $image_icon = '<img class="icon-menu" alt="'.$menu['caption'].'" src="'.$icon.'">';
                }else{
                    $image_icon = '';
                }
                if ($menu['level'] == 0) {
                    $output .= '<li' . $style . $hash . '><a' . $style . ' href="' . $menu['url_path'] . '">' .$image_icon. $menu['caption'] . '</a>';
                    $output .= '<div class="vertical-dropdown-menu"><div class="vertical-groups col-sm-12">';
                    $output .= $this->_visit_mega_menus($params);
                    $output .= '</div></div></li>';
                } elseif ($menu['level'] == 1) {
                    $output .= '<div class="mega-group col-sm-4">';
                    $output .= '<h4 class="mega-group-header"><a' . $style . ' href="' . $menu['url_path'] . '">' . $menu['caption'] . '</a></h4>';
                    $output .= '<ul class="group-link-default">';
                    $output .= $this->_visit_mega_menus($params);
                    $output .= '</ul>';
                    $output .= '</div>';
                } elseif ($menu['level'] == 2) {
                    $output .= '<li' . $style . $hash . '><a' . $style . ' href="' . $menu['url_path'] . '">' . $menu['caption'] . '</a>';
                    $output .= $this->_visit_mega_menus($params);
                    $output .= '</li>';
                }
            }
        }
        return $output;
    }

}
