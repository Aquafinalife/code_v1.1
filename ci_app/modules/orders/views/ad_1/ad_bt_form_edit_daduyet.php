<?php
echo form_open($submit_uri);
$fullname = isset($fullname) ? $fullname : '';
$email = isset($email) ? $email : '';
$created_date = isset($created_date) ? $created_date : '';
$submit_uri = isset($submit_uri) ? $submit_uri : '';
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'orders_cat');
?>

<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Nội dung bồi thường"</small>
    <span class="fright"><a class="button close" href="<?php echo ORDER_ADMIN_BASE_URL; ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>

<?php
$a_loai_benh_THAISAN_QLNT = get_a_loai_benh_THAISAN_QLNT();
$a_loai_benh_THAISAN_MDL = get_a_loai_benh_THAISAN_MDL();
$a_loai_benh_DTNT_OB = get_a_loai_benh_DTNT_OB();
$a_loai_benh_DTNGT_OB = get_a_loai_benh_DTNGT_OB();
$a_loai_benh_NHAKHOA_MDL = get_a_loai_benh_NHAKHOA_MDL();

$a_status_bt = get_a_orders_status();
$a_loai_benh_sn = get_a_loai_benh_sn();
$a_loai_benh_slk = get_a_loai_benh_slk();
$a_status_bt1 = get_a_status_bt1();
$disabled = 'disabled = true';
$disabled_bt = $disabled;
$a_ct_selected = array();


if (isset($bt) && is_object($bt)) {
    $id = $bt->id;
    $chungtu_bosung = $bt->chungtu_bosung;
    $chungtu_bosung_txt = $bt->chungtu_bosung_txt;
    $phanloai_bt = $bt->phanloai_bt;
    $quyenloi_bt = $bt->quyenloi_bt;
    $phanloai_bt_txt = get_phanloai_bt($phanloai_bt);
    $so_tien_ycbt = $bt->so_tien_ycbt;
    $so_tien_dbt = $bt->so_tien_dbt;
    $so_ngay_ycbt = $bt->so_ngay_ycbt;
    $so_ngay_dbt = $bt->so_ngay_dbt;
    $ngay_ycbt = substr($bt->ngay_ycbt, 0, 10);
    $message = $bt->message;
    $so_lan_kham = $bt->so_lan_kham;
    $loai_benh = $bt->loai_benh;
    $loai_benh_txt = get_ten_qlbt($loai_benh);
    $status = $bt->order_status;
    $status_txt = get_orders_status($status);
    if ($status == BT_CHODUYET) {
        $disabled_bt = '';
    }
    $chungtu_bosung = $bt->chungtu_bosung;
    if ($chungtu_bosung != '') {
        $a_ct_selected = explode(',', $chungtu_bosung);
    }
}
echo form_hidden('bt_id', $id);
echo form_hidden('id', $id);
$loai_benh_txt = isset($loai_benh_txt) ? $loai_benh_txt : '';
//end
//$slk_bh = $sn_bh = 'Error';
if (isset($bh) && is_object($bh)) {
    $kh_dbh = $bh->kh_dbh;
    $congty_canhan = $bh->congty_canhan;
    $mst_cmt = $bh->mst_cmt;
    $goi_bh = $bh->goi_bh;
    $goi_bh_dm = $bh->goi_bh_dm;
    //==========================================================================
    $st_bh_gh = $bh->$quyenloi_bt;

//    echo '<pre>';
//    print_r($bh);
//    echo $quyenloi_bt;
//    die;
    //end
    //==========================================================================    
    // Nếu đã bồi thường
//    if (isset($qlbt_cl) && is_object($qlbt_cl)) {
//        $CL_st_bh_cl = $qlbt_cl->CL_st_bh_cl;
//        $CL_sn_bh_cl = $qlbt_cl->CL_sn_bh_cl;
//    } else {
//        // Nếu chưa bt lần nào
//        // Nếu bt có số ngày
//        if (in_array($quyenloi_bt, $a_qlbt_sn)) {
//            $field_quyenloi_bt_sn = strtolower($quyenloi_bt) . '_sn';
//            $sn_bh_gh = $bh->$field_quyenloi_bt_sn;
//            $st_bh_gh_sn = (int) $st_bh_gh * (int) $sn_bh_gh;
//            //==================================================================            
//        }
//    }
    //end
    //==========================================================================    
    // Nếu đã bồi thường:
    if (isset($qlbt_cl) && !empty($qlbt_cl)) {
        foreach ($qlbt_cl as $index) {
            $CL_quyenloi_bt = $index->CL_quyenloi_bt;
            $CL_st_bh_cl = $index->CL_st_bh_cl;
            $CL_sn_bh_cl = $index->CL_sn_bh_cl;
            //==================================================================
            if ($quyenloi_bt == $CL_quyenloi_bt) {
                $CL_st_bh_cl_t = $CL_st_bh_cl;
            }
        }
    } else {
        // Nếu chưa bt lần nào
        // Nếu bt có số ngày
        if (in_array($quyenloi_bt, $a_qlbt_sn)) {
            $field_quyenloi_bt_sn = strtolower($quyenloi_bt) . '_sn';
            $sn_bh_gh = $bh->$field_quyenloi_bt_sn;
            $st_bh_gh_sn = (int) $st_bh_gh * (int) $sn_bh_gh;
            //==================================================================            
        }
    }
    //end
    //==========================================================================
    echo $CL_st_bh_cl_t . '?';
    // Lấy số tiền tổng còn lại
    $st_bh_cl_t = isset($CL_st_bh_cl_t) ? $CL_st_bh_cl_t : (isset($st_bh_gh_sn) ? $st_bh_gh_sn : (isset($st_bh_gh) ? $st_bh_gh : 0));
//    echo $st_bh_cl_t.'>';
    //==========================================================================
    // Nếu bồi thường có số ngày
    if (in_array($quyenloi_bt, $a_qlbt_sn)) {
        $sn_bh_cl = isset($CL_sn_bh_cl) ? $CL_sn_bh_cl : (isset($sn_bh_gh) ? $sn_bh_gh : 0);
    }

    //echo form_hidden('kh_dbh', $kh_dbh);
    ?>
    <div class="form_content">
        <?php $this->load->view('powercms/message'); ?>
        <ul class="tabs">
            <li><a href="#tab1">Thông tin người được bảo hiểm</a></li>
            <!--<li><a href="#tab2">Quyền lợi bảo hiểm tai nạn</a></li>-->
            <!--<li><a href="#tab3">Tình trạng bồi thường</a></li>-->
        </ul>
        <div class="tab_container">
            <div id="tab1" class="tab_content">
                <div class="col-sm-6 col-xs-12">
                    <table>
                        <tr style="display: none;"><td class="title">Ngôn ngữ: </td></tr>
                        <tr style="display: none;">                            
                        </tr>
                        <tr>
                            <td class="title fl w250">Người được bồi thường:</td>
                            <td class="fl">
                                <?php echo $congty_canhan ?>
                                <input class="select_kh_dbh_bt" type="hidden" name="kh_dbh" value="<?php echo $kh_dbh ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">CMT/Hộ chiếu:</td>
                            <td class="fl"><?php echo $mst_cmt ?></td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Loại bồi thường:</td>
                            <td class="fl">
                                <?php echo $phanloai_bt_txt ?>
                                <select name="phanloai_bt" class="hide select_loai_bt">
                                    <option selected="" value="<?php echo $phanloai_bt ?>"><?php echo $phanloai_bt_txt ?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Gói bảo hiểm mua:</td>
                            <td class="fl">
                                <?php echo $goi_bh_dm ?>
                                <input type="hidden" name="goi_bh" value="<?php echo $goi_bh ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Tình trạng hồ sơ:</td>
                            <td class="fl">
                                <span class="btn btn-warning btn-sm"><?php echo $status_txt ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Ngày yêu cầu bồi thường:</td>
                            <td class="fl">
                                <input <?php echo $disabled_bt ?> class="show_date" type="text" name="ngay_ycbt" value="<?php echo $ngay_ycbt ?>"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clear-both"></div>
                <div class="col-sm-6 col-xs-12">
                    <div class="row">
                        <label class="col-sm-6 col-xs-12">Số tiền bảo hiểm còn lại:</label>
                        <div class="col-sm-4 col-xs-12">
                            <span class="btn btn-primary btn-sm">
                                <?php echo get_price_in_vnd($st_bh_cl_t) ?>
                            </span>
                        </div>
                    </div>
                    <?php
                    if (in_array($quyenloi_bt, $a_qlbt_sn)) {
                        ?>
                        <div class="row mt10">
                            <label class="col-sm-6 col-xs-12">Số tiền bảo hiểm / ngày:</label>
                            <div class="col-sm-4 col-xs-12">
                                <span class="btn btn-success btn-sm"><?php echo get_price_in_vnd($st_bh_gh) ?></span>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="row mt10">
                        <label class="col-sm-6 col-xs-12">Số tiền yêu cầu bồi thường:</label>
                        <div class="col-sm-4 col-xs-12">
                            <span class="btn btn-danger btn-sm"><?php echo get_price_in_vnd($so_tien_ycbt) ?></span>
                        </div>
                    </div>
                    <?php
                    if ($status == BT_DATRATIEN || $status == BT_THANHTOAN || $status == BT_DADUYET) {
                        if ($status == BT_DATRATIEN) {
                            $txt = 'Đã thanh toán';
                        } else if ($status == BT_THANHTOAN) {
                            $txt = 'Số tiền được bồi thường';
                        } else {
                            $txt = 'Số tiền chấp nhận bồi thường';
                        }
                        ?>
                        <div class="row mt10">
                            <label class="col-sm-6 col-xs-12"><?php echo $txt ?>:</label>
                            <div class="col-sm-4 col-xs-12">
                                <span class="btn btn-success btn-sm"><?php echo get_price_in_vnd($so_tien_dbt) ?></span>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <!--=====================================================-->
                    <?php
                    if (in_array($quyenloi_bt, $a_qlbt_sn)) {
                        ?>
                        <div class="row mt20">
                            <label class="col-sm-6 col-xs-12">Số ngày bảo hiểm còn lại:</label>
                            <div class="col-sm-4 col-xs-12">
                                <span class="btn btn-primary btn-sm"><?php echo ($sn_bh_cl) ?> - ngày</span>
                            </div>
                        </div>
                        <div class="row mt10">
                            <label class="col-sm-6 col-xs-12">Số ngày yêu cầu bồi thường:</label>
                            <div class="col-sm-4 col-xs-12">
                                <span class="btn btn-danger btn-sm"><?php echo ($so_ngay_ycbt) ?> - ngày</span>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="clear-both"></div>
                <div class="col-sm-12">
                    <div class="form-group box_quyenloi_bt" style="display: _none">
                        <?php
                        if (isset($bh_html)) {
                            echo $bh_html;
                        }
                        ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-2 col-xs-12">
                        <label>Ghi chú:</label>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <textarea class="fw" rows="6" name="message" value="<?php echo $message ?>"><?php echo $message ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <br class="clear"/>
        <div style="margin-top: 10px;"></div>
        <?php
        if ($status == BT_DADUYET) {
            $action = 'Xác nhận thanh toán bồi thường';
        } else {
            $action = 'Xác nhận trả tiền bồi thường';
        }
        ?>
        <?php
        if ($status != BT_DATRATIEN) {
            ?>
            <button class="btn btn-success" type="submit"><?php echo $action ?></button>
            <?php
        }
        ?>
        <a style="margin-left: 20px;" class="btn btn-primary" href="<?php echo site_url(ORDER_ADMIN_BASE_URL) ?>">Quay lại <span class="glyphicon glyphicon-share-alt"></span></a>
        <br class="clear"/>&nbsp;
    </div>
    <?php
}
?>
<?php echo form_close(); ?>
<div class="modal fade bs-example-modal-lg modal_ct_bt" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">

</div>