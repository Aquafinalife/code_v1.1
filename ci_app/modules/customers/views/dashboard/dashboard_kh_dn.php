<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
//end
?>
<?php
$a_bt_thanhtoan = array(BT_THANHTOAN, BT_DATRATIEN);
$so_hsbt = $t_sotien_ycbt = $t_sotien_dbt = $t_sotien_bt_nv = $t_sotien_bt_nt = $t_goi_gia_tien = $t_hs_dbt = $t_hs_cbt = 0;
if (isset($bt) && !empty($bt)) {
    $so_hsbt = count($bt);
    foreach ($bt as $index) {
//        echo '<pre>';
//        print_r($bt);
//        die;
        $ndbh_quanhe = $index->ndbh_quanhe;
        $so_tien_ycbt = ($index->so_tien_ycbt);
        $so_tien_dbt = ($index->so_tien_dbt);
        $goi_gia_tien = ($index->goi_gia_tien);
        $order_status = $index->order_status;
        $mqh = $index->mqh;
        //
        $t_sotien_ycbt = (int) ($t_sotien_ycbt) + (int) $so_tien_ycbt;

        $t_goi_gia_tien = (int) ($t_goi_gia_tien) + (int) $goi_gia_tien;
        if ($mqh == DN_NV) {
            $t_sotien_bt_nv = (($t_sotien_bt_nv) + $so_tien_dbt);
        }
        if ($mqh == DN_NT) {
            $t_sotien_bt_nt = (($t_sotien_bt_nt) + $so_tien_dbt);
        }
        if (in_array($order_status, $a_bt_thanhtoan)) {
            $t_hs_dbt = ($t_hs_dbt + 1);
            $t_sotien_dbt = (int) ($t_sotien_dbt) + (int) $so_tien_dbt;
        } else {
            $t_hs_cbt = ($t_hs_cbt + 1);
        }
    }
}
//echo $t_sotien_bt_nt.'?';
?>

<?php
$tt_kh_dbh = $tt_hsbt = $tt_phi_bh = $tt_st_dbt = 0;
$tt_hsbt_cd = $tt_st_dbt_cd = $tt_st_ycbt_cd = 0;
$ttt_st_dbt = $ttt_hsbt = 0;

if (isset($a_goi_bh) && !empty($a_goi_bh)) {

    foreach ($a_goi_bh as $index1) {

        $GOI_id = $index1->id;
        $GOI_name = $index1->name;
        $goi_gia_tien = $index1->gia_tien;
        //==============================================================
        $t_kh_dbh = $t_hsbt = $t_phi_bh = $t_st_dbt = 0;

        //$a_kh_dbh = array();
        //==============================================================

        if (isset($a_kh_dbh) && !empty($a_kh_dbh)) {
            foreach ($a_kh_dbh as $index2) {
                $KH_GOI_ID = $index2->goi_bh;
                //------------------------------------------------------
                if ($KH_GOI_ID == $GOI_id) {
                    $t_kh_dbh = $t_kh_dbh + 1;
                }
            }
        }
        //==============================================================
        //==========================================================
        $t_phi_bh = $t_kh_dbh * $goi_gia_tien;
        $tt_phi_bh = $tt_phi_bh + $t_phi_bh;
        //==========================================================
        $ttt_st_dbt = $tt_st_ycbt_cd + $tt_st_dbt;
    }
}
?>

<div class="row">
    <div class="col-xl-6">
        <div class="chart-statistic-box">            
            <div class="embed-responsive embed-responsive-16by9 video_db">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/dF3Dcol5XLg"></iframe>
            </div>

        </div><!--.chart-statistic-box-->
    </div><!--.col-->
    <div class="col-xl-6">
        <div class="row">
            <div class="col-sm-6">
                <article class="statistic-box red">
                    <div>
                        <div class="number"><?php echo $so_hsbt ?></div>
                        <div class="caption"><div>Tổng số HSBT</div></div>
                    </div>
                </article>
            </div><!--.col-->
            <div class="col-sm-6">
                <article class="statistic-box purple">
                    <div>
                        <div class="number"><?php echo get_price_in_vnd($t_sotien_dbt) ?></div>
                        <div class="caption"><div>Số tiền đã được bồi thường</div></div>
                    </div>
                </article>
            </div><!--.col-->
            <div class="col-sm-6">
                <article class="statistic-box yellow">
                    <div>
                        <div class="number"><?php echo $t_hs_dbt ?></div>
                        <div class="caption"><div>Hồ sơ đã giải quyết</div></div>                        
                    </div>
                </article>
            </div><!--.col-->
            <div class="col-sm-6">
                <article class="statistic-box green">
                    <div>
                        <div class="number"><?php echo $t_hs_cbt ?></div>
                        <div class="caption"><div>Hồ sơ đang giải quyết</div></div>
                    </div>
                </article>
            </div><!--.col-->
        </div><!--.row-->
    </div><!--.col-->
</div>
<table id="table-edit_" class="table table-bordered table-hover">
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Thông tin khách hàng:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th style="width: 200px;">Chỉ tiêu</th>
            <th style="width: 325px;">Giá trị</th>
            <th>Chỉ tiêu</th>
            <th>Giá trị</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Tên khách hàng</td>
            <td><?php echo $ss_kh_name ?></td>
            <td>ĐKKD - MST</td>
            <td><?php echo $ss_kh_mst_cmt ?></td>
        </tr>
        <tr>
            <td>Mã khách hàng</td>
            <td><?php echo 'MKH-' . $ss_kh_id ?></td>     
<!--            <td>Loại khách hàng</td>
            <td><?php echo $ss_kh_phanloai_kh1_txt ?></td>-->
            <td>Điện thoại</td>
            <td><?php echo $ss_kh_dienthoai ?></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><?php echo $ss_kh_email ?></td>
            <td>Di động</td>
            <td><?php echo $ss_kh_didong ?></td>
<!--            <td>Điện thoại</td>
            <td><?php echo $ss_kh_dienthoai ?></td>-->
        </tr>
        <tr>
            <td>Địa chỉ</td>
            <td><?php echo $ss_kh_diachi ?></td>     
<!--            <td>Di động</td>
            <td><?php echo $ss_kh_didong ?></td>-->
        </tr>
        <tr>            
        </tr>
    </tbody>
</table>
<br><br>
<table id="table-edit_" class="table table-bordered table-hover">
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Báo cáo bồi thường:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th style="width: 200px;">Chỉ tiêu</th>
            <th style="width: 325px;">Giá trị</th>
            <th>Chỉ tiêu</th>
            <th>Giá trị</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Tổng số HSBT</td>
            <td><?php echo $so_hsbt ?></td>
            <td>Số tiền bồi thường NV</td>
            <td><?php echo get_price_in_vnd($t_sotien_bt_nv) ?></td>
        </tr>
        <tr>
            <!--<td>Tổng số tiền YCBT</td>-->
            <!--<td><?php // echo get_price_in_vnd($t_sotien_ycbt)   ?></td>-->    
            <td>Tổng số phí BH</td>
            <td><?php echo get_price_in_vnd($tt_phi_bh) ?></td>
            <td>Số tiền bồi thường NT</td>
            <td><?php echo get_price_in_vnd($t_sotien_bt_nt) ?></td>
        </tr>
        <tr>
            <td>Tổng số tiền ĐBT</td>
            <td><?php echo get_price_in_vnd($t_sotien_dbt) ?></td>
            <td>Tỷ lệ BTUT</td>
            <td><?php // echo $ss_kh_noicap                  ?></td>
        </tr>
        <tr>
            <td>Tỷ lệ BT hiện tại</td>
            <td><?php echo round((($t_sotien_dbt / $tt_phi_bh) * 100), 2) . '%'; ?></td>            
        </tr>
    </tbody>
</table>
<br><br>

<!--<div class="table-responsive">-->
<table id="_table-edit" class="table table-bordered table-hover">    
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Chi tiết bồi thường:</h3>
            <a href="<?php echo site_url(URL_BT_DN) ?>" class="pull-right btn btn-inline btn-primary btn-sm ladda-button">Xem chi tiết</a>
        </header>
    </section>
    <thead>
        <tr>
            <th>Mã số HSBT</th>
            <th>Khách hàng ĐBH</th>
            <th>CMT/Hộ chiếu</th>
            <?php
            if ($ss_kh_parent_id > 0) {
                ?>
                                <!--                <th>Công ty chi nhánh</th>
                                                <th>Phòng ban</th>-->
                <?php
            }
            ?>
            <!--<th>Bên mua BH</th>-->
            <th>Gói BH</th>
            <th>Loại BT</th>
            <th>Hình thức NTBT</th>
            <th>Ghi chú NTBT</th>
            <th>Tình trạng HSBT</th>
            <!--<th>Chứng từ chờ BS</th>-->
            <th>Ngày xảy ra tổn thất</th>
            <th>Ngày nhận HSBT</th>
            <th>Ngày bổ sung CT</th>
            <th>Ngày xử lý HSBT</th>
            <th>Ngày duyệt HSBT</th>
            <th>Ngày thông báo KH</th>
            <th>Ngày thanh toán</th>
            <th>Phương thức BT</th>
            <th>CSYT</th>
            <th>Chuẩn đoán bệnh</th>
            <th>Mã bệnh</th>
            <th>Số tiền YCBT</th>
            <th>Số ngày YCBT</th>
            <th>Số tiền ĐBT</th>
            <!--<th>Số tiền từ chối BT</th>-->
            <th>Lý do KBT</th>
            <th>Ghi chú lý do KBT</th>
            <th>Ghi chú TT</th>
            <!--<th>Hồ sơ scan</th>-->
            <!--end-->
            <!--<th>Xuất PDF</th>-->
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($bt) && !empty($bt)) {
//            echo '<pre>';
//            print_r($bt);
//            die;
            $j = 1;
            foreach ($bt as $index) {
                $OD_id = $index->id;
                //$OD_id = $index->OD_id;
                $BT_code = $index->bt_code;
                $case = $index->case;
                $KH_id = $index->KH_id;
                $kh_mbh = $index->kh_mbh;
                $loai_benh = $index->loai_benh;
                $loai_benh_txt = get_ma_benh($loai_benh);
                $cn_dbh = $index->congty_canhan;
                $mst_cmt = $index->mst_cmt;
                $ngaysinh = $index->ngaysinh;
                $parent_id = $index->parent_id;
                $phongban = $index->phongban;
                $goi_bh_dm = $index->goi_bh_dm;
                $so_ngay_ycbt = $index->so_ngay_ycbt;
                $so_tien_ycbt = get_price_in_vnd($index->so_tien_ycbt);
                $so_ngay_ycbt = $index->so_ngay_ycbt;
                $so_tien_dbt = get_price_in_vnd($index->so_tien_dbt);
                $phanloai_bt = $index->phanloai_bt;
                $phanloai_bt_txt = get_phanloai_bt($phanloai_bt);
                $hinhthuc_nhantien_bt = $index->hinhthuc_nhantien_bt;
                $hinhthuc_nhantien_bt_txt = get_hinhthuc_nhantien_bt($hinhthuc_nhantien_bt);

                //==============================================================
                $ghichu_hinhthuc_nhantien_bt = $index->ghichu_hinhthuc_nhantien_bt;
                $ghichu_hoso = $index->ghichu_hoso;
                $ghichu_xuly_bt = $index->ghichu_xuly_bt;
                $ghichu_khong_bt = $index->ghichu_khong_bt;
                $ghichu_pheduyet_bt = $index->ghichu_pheduyet_bt;
                $ghichu_thanhtoan_bt = $index->ghichu_thanhtoan_bt;
                //==============================================================
                $date_xayra_tonthat = substr($index->date_xayra_tonthat, 0, 10);
                $date_nhan_bt = substr($index->date_nhan_bt, 0, 10);
                $date_xuly_bt = substr($index->date_xuly_bt, 0, 10);
                $date_bosung_ct = substr($index->date_bosung_ct, 0, 10);
                $date_duyet_bt = substr($index->date_duyet_bt, 0, 10);
                $date_thongbao_kh = substr($index->date_thongbao_kh, 0, 10);
                $date_thanhtoan = substr($index->date_thanhtoan, 0, 10);
                $date_tratien = substr($index->date_tratien, 0, 10);
                //==============================================================
                $order_status = $index->order_status;
                $order_status_txt = get_orders_status($order_status);
                if ($order_status == BT_THANHTOAN) {
                    $order_status_txt1 = '<button class="btn btn-danger btn-sm" href="#">' . $order_status_txt . '</button>';
                } elseif ($order_status == BT_CHOBOSUNG) {
                    $order_status_txt1 = '<button class="btn btn-warning btn-sm btn_check_ctbs" href="#" data-toggle="modal" id="'.$OD_id.'" data-target=".bs-example-modal-lg">' . $order_status_txt . '</button>';
                } else {
                    $order_status_txt1 = '<button class="btn btn-primary btn-sm" href="#">' . $order_status_txt . '</button>';
                }
                $chungtu_bosung = $index->chungtu_bosung;
                if ($chungtu_bosung != '') {
                    $chungtu_bosung1 = '<a class="btn btn-warning btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg" href="#">Chứng từ BS</a>';
                } else {
                    $chungtu_bosung1 = '';
                }
                $ndbh_quanhe = $index->ndbh_quanhe;
                $ndbh_quanhe_txt = get_ndbh_quanhe($ndbh_quanhe);
                $phuongthuc_bt = $index->phuongthuc_bt;
                $phuongthuc_bt_txt = get_phuongthuc_bt($phuongthuc_bt);
                $chuandoan_benh = $index->chuandoan_benh;
                $OD_ma_benh = $index->OD_ma_benh;
                $lydo_khong_bt = $index->lydo_khong_bt;
                $lydo_khong_bt_txt = get_lydo_khong_bt($lydo_khong_bt);
                $goi_gia_tien = ($index->goi_gia_tien);
                $csyt = $index->csyt;
                $ten_csyt = $index->ten_csyt;
                $ten_dn_bbh = $index->ten_dn_bbh;
                $hoso_scan = $index->hoso_scan;
                ?>
                <tr>
                    <td><?php echo $BT_code ?></td>
                    <td><?php echo $cn_dbh ?></td>
                    <td><?php echo $mst_cmt ?></td>
                    <?php
                    if ($parent_id > 0) {
                        ?>
                                        <!--                        <td>
                        <?php
                        if (isset($dn) && !empty($dn)) {
                            foreach ($dn as $index) {
                                $dn_ten = $index->congty_canhan;
                                $dn_id = $index->id;
                                if ($dn_id == $parent_id) {
                                    echo $dn_ten;
                                    break;
                                }
                            }
                        }
                        ?>
                                                        </td>-->
                                        <!--                        <td>
                        <?php
                        if (isset($a_phongban) && !empty($a_phongban)) {
                            foreach ($a_phongban as $index) {
                                $pb_name = $index->name;
                                $pb_id = $index->id;
                                if ($pb_id == $phongban) {
                                    echo $pb_name;
                                    break;
                                }
                            }
                        }
                        ?>
                                                        </td>-->
                        <?php
                    }
                    ?>
        <!--                    <td>
                    <?php
                    if (isset($kh) && !empty($kh)) {
                        foreach ($kh as $index) {
                            $kh_mbh_id = $index->id;
                            $kh_mbh_ten = $index->congty_canhan;
                            if ($kh_mbh_id == $kh_mbh) {
                                echo $kh_mbh_ten;
                                break;
                            }
                        }
                    }
                    ?>
                    </td>-->
                    <td><?php echo $goi_bh_dm ?></td>
                    <td><?php echo $phanloai_bt_txt; ?></td>
                    <td><?php echo $hinhthuc_nhantien_bt_txt ?></td>
                    <td><?php echo $ghichu_hinhthuc_nhantien_bt ?></td>
                    <td><?php echo $order_status_txt1 ?></td>
                    <td><?php echo $date_xayra_tonthat ?></td>
                    <td><?php echo $date_nhan_bt ?></td>
                    <td><?php echo $date_bosung_ct ?></td>
                    <td><?php echo $date_xuly_bt ?></td>
                    <td><?php echo $date_duyet_bt ?></td>
                    <td><?php echo $date_thongbao_kh ?></td>
                    <td><?php echo $date_thanhtoan ?></td>
                    <td><?php echo $phuongthuc_bt_txt ?></td>
                    <td><?php echo $ten_csyt ?></td>
                    <td><?php echo $chuandoan_benh ?></td>
                    <td>
                        <?php
                        if (isset($a_ma_benh) && !empty($a_ma_benh)) {
                            foreach ($a_ma_benh as $mb) {
                                $mb_id = $mb->id;
                                $mb_code = $mb->ma_benh;
                                if ($mb_id == $OD_ma_benh) {
                                    echo $mb_code;
                                    break;
                                }
                            }
                        }
                        ?>
                    </td>
                    <td><?php echo $so_tien_ycbt ?></td>
                    <td><?php echo $so_ngay_ycbt ?></td>
                    <td><?php echo $so_tien_dbt ?></td>
                    <td><?php echo $lydo_khong_bt_txt ?></td>
                    <td><?php echo $ghichu_khong_bt ?></td>
                    <td><?php echo $ghichu_thanhtoan_bt ?></td>
                    <!--<td><?php // echo $hoso_scan          ?></td>-->
                    <!--end-->
        <!--                    <td>
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm">
                            Xuất PDF
                        </a>
                    </td>-->
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="10" class="text-danger">Không có dữ liệu.</td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<!--</div>-->
<div class="paging">
    <?php // echo $this->pagination->create_links(); ?>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content box_about_home _col-sm-12">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button> 
                <h3 class="modal-title mb0" id="myLargeModalLabel">Danh sách chứng từ chờ bổ sung.</h3> 
            </div>
            <div class="modal-body modal_bt">
                
            </div>
            <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button></div>
        </div>
    </div>
</div>      