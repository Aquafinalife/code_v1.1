<?php
class Emails_Model extends MY_Model
{
    function __construct()
    {
            parent::__construct();
    }

    public function get_emails($options = array())
    {
        
        if (isset($options['id']))
            $this->db->where('id', $options['id']);
        // Loc theo trang
        if (isset($options['limit']) && isset($options['offset']))
            $this->db->limit($options['limit'], $options['offset']);
        else if (isset($options['limit']))
            $this->db->limit($options['limit']);

        $this->db->order_by('registry_date desc');

        $query = $this->db->get('emails');

        if (isset($options['id']) || isset($options['uri']))
            return $query->row(0);
        return $query->result();
    }

    public function get_emails_count($options = array())
    {
        return count($this->get_emails($options));
    }
    
    public function add_email($data = array())
    {
        $this->db->insert('emails', $data);
    }
    
    function delete_email($id=0)
    {
        $this->db->where('id', $id);
        $this->db->delete('emails');
    }
}
?>