<div class="page_header">
    <h1 class="fleft">Danh sách khách hàng</h1>
    <small class="fleft">"Thêm/sửa khách hàng"</small>
    <!--<span class="fright"><a class="button close" href= "<?php echo ADMIN_BASE_URL; ?>"><em>&nbsp;</em>Đóng</a></span>-->
    <!--<span class="fright"><a class="button add" href= "<?php echo site_url(URL_ADD_KH_CN); ?>"><em>&nbsp;</em>Thêm khách hàng</a></span>-->
    <span class="fright"><a class="button add" href= "<?php echo site_url(URL_ADD_KH_CN); ?>"><em>&nbsp;</em>Thêm khách hàng cá nhân</a></span>
    <span class="fright"><a class="button add" href= "<?php echo site_url(URL_ADD_KH_DN); ?>"><em>&nbsp;</em>Thêm khách hàng doanh nghiệp</a></span>
    <br class="clear"/>
</div>
<?php
//$this->load->view('admin/customers_nav');
echo form_open('', array('id' => 'submit_form'), array('id' => 0, 'from_list' => TRUE));
//echo isset($uri) ? form_hidden('uri', $uri) : NULL;
echo form_hidden('back_url', CUSTOM_ADMIN_BASE_URL);
echo form_close();
$GET_kh = isset($_GET['kh']) ? $_GET['kh'] : '';
$GET_key = isset($_GET['key']) ? $_GET['key'] : '';
$GET_pl_kh = isset($_GET['pl_kh']) ? $_GET['pl_kh'] : '';
?>
<div class="form_content">
    <div class="filter">
        <form action="<?php echo site_url(URL_AD_KH) ?>" method="get" accept-charset="utf-8">    

            <div class="list_box_filter">
                <input name="key" value="<?php echo $GET_key?>" placeholder="Tên khách hàng / CMTND-MST..." style="width: 250px">
<!--                <select parent_id="0" selectbox_name="kh" name="kh" class="js-example-disabled-results">
                    <option value="">Tất cả khách hàng</option>
                <?php
//                    if (!empty($kh_all)) {
//                        foreach ($kh_all as $index) {
//                            $KH_id = $index->KH_id;
//                            $congty_canhan = $index->congty_canhan;
//                            $mst_cmt = $index->mst_cmt;
//                            $selected = ($KH_id == $GET_kh) ? 'selected="selected"' : '';
//                            
                ?>
                            <option //<?php echo $selected ?> value="<?php echo $KH_id ?>"><?php echo $congty_canhan . ' - ' . $mst_cmt ?></option>
                <?php
//                        }
//                    }
                ?>
                </select>-->
            </div>
            <div class="list_box_filter">
                Phân loại KH: 
                <select parent_id="0" selectbox_name="kh" name="pl_kh" class="js-example-disabled-results">
                    <option value="">Tất cả khách hàng</option>
                    <?php
                    $a_phanloai_kh = get_a_customers_type();
                    if (!empty($a_phanloai_kh)) {
                        foreach ($a_phanloai_kh as $k => $v) {
                            $selected = ($k == $GET_pl_kh) ? 'selected="selected"' : '';
                            ?>
                            <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <input style="margin-top: -3px;margin-left: 20px;" class="btn btn btn-inline btn-sm btn-primary" type="submit" value="Tìm kiếm">
            <!--<input type="submit" name="submit" value="Tìm kiếm" class="btn">-->
            <span class="fright"><a class="button" href="/dashboard/customers/export"><em>&nbsp;</em>Xuất excel</a></span>
        </form> 
    </div>
    <table class="list" style="width: 100%; margin-bottom: 10px;">
<?php $this->load->view('powercms/message'); ?>
        <tr>
            <th class="left" style="width: 5%">MÃ KH</th>
            <th class="left" style="width: 20%">KHÁCH HÀNG</th>
            <th class="left" style="width: 20%">SỐ THẺ BH</th>
            <th class="left" style="width: 20%">EMAIL</th>
            <th class="left" style="width: 10%">ĐIỆN THOẠI</th>
            <th class="center" style="width: 10%">MST/CMT</th>
            <th class="left" style="width: 20%">LOẠI KH</th>
            <th class="center" style="width: 5%">CHỨC NĂNG</th>
        </tr>

        <?php
        if (isset($kh) && !empty($kh)) {
            $i = 1;
            foreach ($kh as $index) {
//                echo '<pre>';
//                print_r($kh);
//                die;
                $id = $index->KH_id;
                $sothe_bh = $index->sothe_bh;
                
                
                $created_date = get_vndate_string($index->created_date);
                $style = $i % 2 == 0 ? 'even' : 'odd';
                $congty_canhan = $index->congty_canhan;
                $email = $index->email;
                $mst_cmt = $index->mst_cmt;
                $phanloai_kh = $index->phanloai_kh;
                $phanloai_kh_txt = get_customers_type($phanloai_kh);
                $didong = $index->didong;
                ?>
                <tr class="<?php echo $style ?>">
                    <td><?php echo 'MKH-' . $id; ?></td>
                    <td>
                        <?php
                        echo $congty_canhan;
//                            echo '<a href="/dashboard/orders?email=' . $email . '" style="text-decoration: underline;">' . $congty_canhan . '</a>';
                        ?>
                    </td>
                    <td style="white-space:nowrap;"><?php echo $sothe_bh ?></td>
                    <td style="white-space:nowrap;"><?php echo $email ?></td>
                    <td style="white-space:nowrap;"><?php echo $didong ?></td>
                    <td style="white-space:nowrap;"><?php echo $mst_cmt ?></td>
                    <td style="white-space:nowrap;"><?php echo $phanloai_kh_txt ?></td>
                    <!--<td class="center" style="white-space:nowrap;"><?php // echo $created_date       ?></td>-->
                    <td class="center" style="white-space:nowrap;" class="action">
                        <a class="edit" title="Xem chi tiết khách hàng này" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id ?>, '<?php echo CUSTOM_ADMIN_EDIT_URL; ?>', 'edit');"><em>&nbsp;</em></a>
                        <a class="del" title="Xóa khách hàng" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id ?>, '<?php echo CUSTOM_ADMIN_DELETE_URL; ?>', 'delete');"><em>&nbsp;</em></a>

                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
    <?php $left_page_links = 'Trang ' . $page . ' / ' . $total_pages . ' (<span>Tổng số: ' . $total_rows . ' khách hàng</span>)'; ?>
            <tr class="list-footer">
                <th colspan="8">
                    <div style="float:left; margin-top: 9px;"><?php echo $left_page_links; ?></div>
                    <div style="float:right;" class="pagination">
    <?php echo $this->pagination->create_links(); ?>
                    </div>
                </th>
            </tr>
            <?php
        }

        if (isset($customerss)) {
            $stt = 0;
            foreach ($customerss as $index => $customers):
                $row_uri = get_base_url() . url_title(trim($customers->fullname), 'dash', TRUE) . '-qs' . $customers->id;
                $created_date = get_vndate_string($customers->created_date);
                $updated_date = get_vndate_string($customers->updated_date);
                $check = $customers->status == STATUS_ACTIVE ? 'checked' : '';
                $style = $stt++ % 2 == 0 ? 'even' : 'odd';
                $congty_canhan = $customers->congty_canhan;
                $didong = $customers->didong;
                $email = $customers->email;
                ?>
                <tr class="<?php echo $style ?>">
                    <td><?php echo '#' . $customers->id; ?></td>
                    <td>
                        <?php
                        echo $congty_canhan;
//                            echo '<a href="/dashboard/orders?email=' . $email . '" style="text-decoration: underline;">' . $congty_canhan . '</a>';
                        ?>
                    </td>
                    <td style="white-space:nowrap;"><?php echo $email ?></td>
                    <td style="white-space:nowrap;"><?php echo $didong ?></td>
                    <td class="center" style="white-space:nowrap;"><?php echo $created_date ?></td>
                    <td style="white-space:nowrap;">
                        <?php
                        if ($customers->count_order <= 0)
                            echo '';
                        else
                            echo '<a href="/dashboard/orders?email=' . $customers->email . '" style="text-decoration: underline;">' . $customers->count_order . '</a>';
                        ?>
                    </td>
                    <td class="center" style="white-space:nowrap;" class="action">
                        <a class="edit" title="Xem chi tiết khách hàng này" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $customers->id ?>, '<?php echo CUSTOM_ADMIN_EDIT_URL; ?>', 'edit');"><em>&nbsp;</em></a>
                        <a class="del" title="Xóa khách hàng" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $customers->id ?>, '<?php echo CUSTOM_ADMIN_DELETE_URL; ?>', 'delete');"><em>&nbsp;</em></a>

                    </td>
                </tr>
            <?php endforeach; ?>
<?php } ?>

    </table>
    <br class="clear"/>&nbsp;
</div>