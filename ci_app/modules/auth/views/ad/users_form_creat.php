<?php
if (isset($user) && is_object($user)) {
    $user_id = $user->id;
    $username = $user->username;
    $fullname = $user->fullname;
    $email = $user->email;
    $tel = $user->tel;
    //==========================================================================
    $ql_all = $user->ql_all;
    $ql_users = $user->ql_users;
    $ql_bbh = $user->ql_bbh;
    $ql_goi_bh = $user->ql_goi_bh;
    $ql_kh = $user->ql_kh;
    $ql_bh = $user->ql_bh;
    $ql_bt = $user->ql_bt;
    $ql_bt_nhap_hs = $user->ql_bt_nhap_hs;
    $ql_bt_xuly = $user->ql_bt_xuly;
    $ql_bt_duyet = $user->ql_bt_duyet;
    $ql_bt_thanhtoan = $user->ql_bt_thanhtoan;
    //==========================================================================
//    echo '<pre>';
//    print_r($_SESSION);
//    die;
}
//==============================================================================
$checked_df = 'checked="checked"';
$ck1 = $ck2 = $ck3 = $ck4 = $ck5 = $ck6 = $ck7 = $ck8 = $ck9 = $ck10 = $ck11 = '';
//==============================================================================
echo form_open($submit_uri);
if (isset($user_id)) {
    echo form_hidden('user_id', $user_id);
}
//==============================================================================
$username = isset($username) ? $username : '';
$password = isset($password) ? $password : '';
$fullname = isset($fullname) ? $fullname : '';
$email = isset($email) ? $email : '';
$tel = isset($tel) ? $tel : '';
$submit_uri = isset($submit_uri) ? $submit_uri : '';
?>
<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Thông tin người dùng"</small>
    <span class="fright"><a class="button close" href="<?php echo AUTH_USERS_ADMIN_BASE_URL; ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>

<div class="form_content">
    <?php $this->load->view('powercms/message'); ?>
    <table>
        <tr><td class="title">Tên đăng nhập: (<span>*</span>)</td></tr>
        <tr>
            <td><?php echo form_input(array('name' => 'username', 'size' => '50', 'maxlength' => '30', 'style' => 'width:560px;', 'value' => $username)); ?></td>
        </tr>
        <?php if (!isset($is_edit)) { ?>
            <tr><td class="title">Mật khẩu: (<span>*</span>)</td></tr>
            <tr>
                <td><?php echo form_password(array('name' => 'password', 'size' => '50', 'maxlength' => '50', 'style' => 'width:560px;', 'value' => $password)); ?></td>
            </tr>
        <?php } else { ?>
            <tr><td class="title">Mật khẩu: (<span>*</span>)</td></tr>
            <tr>
                <td><?php echo form_password(array('name' => 'password', 'size' => '50', 'maxlength' => '50', 'style' => 'width:560px;', 'value' => $password)); ?> <span style="color:#999;">(để trống nếu không thay đổi)</span></td>
            </tr>
        <?php } ?>
        <tr><td class="title">Họ tên: </td></tr>
        <tr>
            <td><?php echo form_input(array('name' => 'fullname', 'size' => '50', 'maxlength' => '50', 'style' => 'width:560px;', 'value' => $fullname)); ?></td>
        </tr>
        <tr><td class="title">Email: </td></tr>
        <tr>
            <td><?php echo form_input(array('name' => 'email', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => $email)); ?></td>
        </tr>
        <tr><td class="title">Điện thoại: </td></tr>
        <tr>
            <td><?php echo form_input(array('name' => 'tel', 'size' => '50', 'maxlength' => '25', 'style' => 'width:560px;', 'value' => $tel)); ?></td>
        </tr>
        <tr><td class="title">Quyền hạn: (<span>*</span>)</td></tr>
        <tr>
            <td id="category">
                <div class="box_permission">
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_all) && $ql_all == QL_CHECKED) {
                                    $ck1 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck1 ?> type="checkbox" value="2" name="ql_all">
                                <strong>Tất cả</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_users) && $ql_users == QL_CHECKED) {
                                    $ck11 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck11 ?> type="checkbox" value="2" name="ql_users">
                                <strong>Thành viên</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_bbh) && $ql_bbh == QL_CHECKED) {
                                    $ck2 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck2 ?> type="checkbox" value="2" name="ql_bbh">
                                Đơn vị cung cấp BH
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_goi_bh) && $ql_goi_bh == QL_CHECKED) {
                                    $ck3 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck3 ?> type="checkbox" value="2" name="ql_goi_bh">
                                Gói BH
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_kh) && $ql_kh == QL_CHECKED) {
                                    $ck9 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck9 ?> type="checkbox" value="2" name="ql_kh">
                                Khách hàng
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_bh) && $ql_bh == QL_CHECKED) {
                                    $ck4 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck4 ?> type="checkbox" value="2" name="ql_bh">
                                Bảo hiểm
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_bt) && $ql_bt == QL_CHECKED) {
                                    $ck8 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck8 ?> type="checkbox" value="2" name="ql_bt">
                                <strong>Bồi thường</strong>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_bt_nhap_hs) && $ql_bt_nhap_hs == QL_CHECKED) {
                                    $ck5 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck5 ?> type="checkbox" value="2" name="ql_bt_nhap_hs">
                                Bồi thường: Nhập hồ sơ
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_bt_xuly) && $ql_bt_xuly == QL_CHECKED) {
                                    $ck6 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck6 ?> type="checkbox" value="2" name="ql_bt_xuly">
                                Bồi thường: Xử lý - Phương án bồi thường
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_bt_duyet) && $ql_bt_duyet == QL_CHECKED) {
                                    $ck7 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck7 ?> type="checkbox" value="2" name="ql_bt_duyet">
                                Bồi thường: Duyệt
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <?php
                                if (isset($ql_bt_thanhtoan) && $ql_bt_thanhtoan == QL_CHECKED) {
                                    $ck10 = $checked_df;
                                }
                                ?>
                                <input <?php echo $ck10 ?> type="checkbox" value="2" name="ql_bt_thanhtoan">
                                Bồi thường: Thanh toán
                            </label>
                        </div>

                    </div>

                </div>
            </td>
        </tr>
    </table>
    <br class="clear"/>
    <div style="margin-top: 10px;"></div>
    <?php echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn')); ?>
    <a style="margin-left: 20px;" class="btn btn-primary" href="<?php echo site_url(AUTH_USERS_ADMIN_BASE_URL) ?>">Quay lại <span class="glyphicon glyphicon-share-alt"></span></a>
    <br class="clear"/>&nbsp;
</div>
<?php echo form_close(); ?>