<?php
// Check permission
$ss_ql_all = $this->phpsession->get('ss_ql_all');
$ss_ql_bt = $this->phpsession->get('ss_ql_bt');
$ss_ql_bt_nhap_hs = $this->phpsession->get('ss_ql_bt_nhap_hs');
$ss_ql_bt_xuly = $this->phpsession->get('ss_ql_bt_xuly');
$ss_ql_bt_duyet = $this->phpsession->get('ss_ql_bt_duyet');
$ss_ql_bt_thanhtoan = $this->phpsession->get('ss_ql_bt_thanhtoan');
//==============================================================================
?>
<?php
echo form_open($submit_uri);
$fullname = isset($fullname) ? $fullname : '';
$email = isset($email) ? $email : '';
$created_date = isset($created_date) ? $created_date : '';
$submit_uri = isset($submit_uri) ? $submit_uri : '';
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'orders_cat');
?>

<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Nội dung bồi thường"</small>
    <span class="fright"><a class="button close" href="<?php echo ORDER_ADMIN_BASE_URL; ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>

<?php
$a_qlbt_sn = get_a_loai_benh_sn();
$a_lydo_khong_bt = get_a_lydo_khong_bt();
$disabled = 'disabled = true';
$disabled_bt = $disabled;
$a_ct_selected = array();
//==============================================================================
if (isset($bt) && is_object($bt)) {
    $id = $bt->id;
    $bt_code = $bt->bt_code;
    $case = $bt->case;
    $chungtu_bosung = $bt->chungtu_bosung;
    $chungtu_bosung_txt = $bt->chungtu_bosung_txt;
    $phanloai_bt = $bt->phanloai_bt;
    $phanloai_bt_txt = get_phanloai_bt($phanloai_bt);
    $case_bt = $bt->parent_id;
    $quyenloi_bt = $bt->quyenloi_bt;
    $phanloai_thaisan = $bt->phanloai_thaisan;
    $phanloai_thaisan_txt = get_phanloai_thaisan($phanloai_thaisan);
    $phuongthuc_bt = $bt->phuongthuc_bt;
    $phuongthuc_bt_txt = get_phuongthuc_bt($phuongthuc_bt);
    $csyt_bt = $bt->csyt;
    $hinhthuc_nhantien_bt = $bt->hinhthuc_nhantien_bt;
    $hinhthuc_nhantien_bt_txt = get_hinhthuc_nhantien_bt($hinhthuc_nhantien_bt);
    $ghichu_hinhthuc_nhantien_bt = $bt->ghichu_hinhthuc_nhantien_bt;
    $ghichu_xuly_bt = $bt->ghichu_xuly_bt;
    $ghichu_pheduyet_bt = $bt->ghichu_pheduyet_bt;
    $ghichu_thanhtoan_bt = $bt->ghichu_thanhtoan_bt;
    $so_tien_ycbt = $bt->so_tien_ycbt;
    $so_tien_dbt = $bt->so_tien_dbt;
    $so_ngay_ycbt = $bt->so_ngay_ycbt;
    $so_lan_kham = $bt->so_lan_kham;
    $status = $bt->order_status;
    $ghichu_hoso = $bt->ghichu_hoso;
    $status_txt = get_orders_status($status);
    if ($status == BT_CHODUYET) {
        $disabled_bt = '';
    }
    $chungtu_bosung = $bt->chungtu_bosung;
    if ($chungtu_bosung != '') {
        $a_ct_selected = explode(',', $chungtu_bosung);
    }
    $chungtu_bosung_txt = $bt->chungtu_bosung_txt;
    $lydo_khong_bt = $bt->lydo_khong_bt;
    $ghichu_khong_bt = $bt->ghichu_khong_bt;
    $bt_ma_benh = $bt->OD_ma_benh;
    $chuandoan_benh = $bt->chuandoan_benh;

    $date_xayra_tonthat = substr($bt->date_xayra_tonthat, 0, 10);
    $date_nhan_bt = substr($bt->date_nhan_bt, 0, 10);
    $date_xuly_bt = substr($bt->date_xuly_bt, 0, 10);
    //==========================================================================
}
echo form_hidden('bt_id', $id);
echo form_hidden('id', $id);
$loai_benh_txt = isset($loai_benh_txt) ? $loai_benh_txt : '';
//end
//==============================================================================
if (isset($bh) && is_object($bh)) {
    $kh_dbh = $bh->kh_dbh;
    $congty_canhan = $bh->congty_canhan;
    $mst_cmt = $bh->mst_cmt;
    $goi_bh = $bh->goi_bh;
    $goi_bh_dm = $bh->goi_bh_dm;
    //==========================================================================
    $st_bh_gh = $bh->$quyenloi_bt;
    //end
    //==========================================================================
    //==========================================================================
    // Nếu đã bồi thường
    if (isset($qlbt_cl) && !empty($qlbt_cl)) {
        foreach ($qlbt_cl as $index) {
            $CL_quyenloi_bt = $index->CL_quyenloi_bt;
            $CL_st_bh_cl = $index->CL_st_bh_cl;
            $CL_sn_bh_cl = $index->CL_sn_bh_cl;
            //==================================================================
            if ($quyenloi_bt == $CL_quyenloi_bt) {
                $CL_st_bh_cl_t = $CL_st_bh_cl;
            }
        }
    } else {
        // Nếu chưa bt lần nào
        // Nếu bt có số ngày
        if (in_array($quyenloi_bt, $a_qlbt_sn)) {
            $field_quyenloi_bt_sn = strtolower($quyenloi_bt) . '_sn';
            $sn_bh_gh = $bh->$field_quyenloi_bt_sn;
            $st_bh_gh_sn = (int) $st_bh_gh * (int) $sn_bh_gh;
            //==================================================================
        }
    }
    //end
    //==========================================================================
//    echo $CL_st_bh_cl_t . '?';
    $st_bh_cl_t = isset($CL_st_bh_cl_t) ? $CL_st_bh_cl_t : (isset($st_bh_gh_sn) ? $st_bh_gh_sn : (isset($st_bh_gh) ? $st_bh_gh : 0));
//    echo $st_bh_cl_t.'>';
    //==========================================================================
    // Nếu bồi thường có số ngày
    if (in_array($quyenloi_bt, $a_qlbt_sn)) {
        $sn_bh_cl = isset($CL_sn_bh_cl) ? $CL_sn_bh_cl : (isset($sn_bh_gh) ? $sn_bh_gh : 0);
    }
    //==========================================================================
    //echo form_hidden('kh_dbh', $kh_dbh);
    ?>
    <div class="form_content">
        <?php $this->load->view('powercms/message'); ?>
        <ul class="tabs">
            <li><a href="#tab1">Thông tin người được bảo hiểm</a></li>
            <!--<li><a href="#tab2">Quyền lợi bảo hiểm tai nạn</a></li>-->
            <!--<li><a href="#tab3">Tình trạng bồi thường</a></li>-->
        </ul>
        <div class="tab_container">
            <div id="tab1" class="tab_content">
                <div class="col-sm-6 col-xs-12">
                    <div class="row">
                        <label class="col-sm-4">Người được BT:</label>
                        <?php echo $congty_canhan ?>
                        <input class="select_kh_dbh_bt" type="hidden" name="kh_dbh" value="<?php echo $kh_dbh ?>"/>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">CMT/Hộ chiếu:</label>
                        <?php echo $mst_cmt ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Loại bồi thường:</label>
                        <?php echo $phanloai_bt_txt ?>
                        <select name="phanloai_bt" class="hide select_loai_bt">
                            <option selected="" value="<?php echo $phanloai_bt ?>"><?php echo $phanloai_bt_txt ?></option>
                        </select>
                    </div>

                    <?php
                    if ($phanloai_bt == BT_THAISAN_QLNT) {
                        ?>
                        <div class="row mt20">
                            <label class="col-sm-4">Loại thai sản:</label>
                            <?php echo $phanloai_thaisan_txt ?>
                            <select name="phanloai_thaisan" class="hide">
                                <option selected="" value="<?php echo $phanloai_thaisan ?>"><?php echo $phanloai_thaisan_txt ?></option>
                            </select>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="row mt20">
                        <label class="col-sm-4">Gói bảo hiểm mua:</label>
                        <?php echo $goi_bh_dm ?>
                        <input type="hidden" name="goi_bh" value="<?php echo $goi_bh ?>"/>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Cơ sở y tế:</label>
                        <?php
                        if (isset($a_csyt) && !empty($a_csyt)) {
                            foreach ($a_csyt as $index) {
                                $csyt_id = $index->id;
                                $csyt_name = $index->name;
                                if ($csyt_bt == $csyt_id) {
                                    echo $csyt_name;
                                    break;
                                }
                            }
                        }
                        ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Tình trạng hồ sơ:</label>
                        <span class="btn btn-warning btn-sm"><?php echo $status_txt ?></span>
                        <input type="hidden" name="goi_bh" value="<?php echo $goi_bh ?>"/>
                    </div>

                    <?php
                    if ($chungtu_bosung != '') {
                        ?>
                        <div class="row mt20">
                            <label class="col-sm-4">Ghi chú chứng từ chờ bổ sung:</label>
                            <?php echo form_textarea(array('disabled' => TRUE, 'rows' => 5, 'id' => 'chungtu_bosung_txt', 'name' => 'chungtu_bosung_txt', 'style' => '', 'value' => isset($chungtu_bosung_txt) ? $chungtu_bosung_txt : set_value('chungtu_bosung_txt'))); ?>
                        </div>
                        <?php
                    } else {
                        //
                    }
                    ?>

                    <div class="row mt20">
                        <label class="col-sm-4">Phương thức BT:</label>
                        <?php echo $phuongthuc_bt_txt ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Hình thức nhận BT:</label>
                        <?php echo $hinhthuc_nhantien_bt_txt ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Thông tin tài khoản:</label>
                        <?php echo form_textarea(array('disabled' => TRUE, 'rows' => 5, 'id' => 'ghichu_hinhthuc_nhantien_bt', 'name' => 'ghichu_hinhthuc_nhantien_bt', 'style' => '', 'value' => isset($ghichu_hinhthuc_nhantien_bt) ? $ghichu_hinhthuc_nhantien_bt : set_value('ghichu_hinhthuc_nhantien_bt'))); ?>
                    </div>

                </div>
                <div class="col-sm-6 col-xs-12">
                    
                    <div class="row mt20">
                        <label class="col-sm-4">Mã HSBT:</label>
                        <?php echo $bt_code ?>
                    </div>
                    
                    <div class="row mt20">
                        <label class="col-sm-4">Mã Case bồi thường:</label>
                        <?php echo $case ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Ngày xảy ra tổn thất:</label>
                        <?php echo $date_xayra_tonthat ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Ngày nhận hồ sơ BT:</label>
                        <?php echo $date_nhan_bt ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Ngày xử lý hồ sơ BT:</label>
                        <?php echo $date_xuly_bt ?>
                    </div>
                    <hr>
                    <div class="row mt20">
                        <label class="col-sm-6 col-xs-12">Số tiền bảo hiểm còn lại:</label>
                        <div class="col-sm-4 col-xs-12">
                            <span class="btn btn-primary btn-sm">
                                <?php echo get_price_in_vnd($st_bh_cl_t) ?>
                            </span>
                        </div>
                    </div>
                    <?php
                    if (in_array($quyenloi_bt, $a_qlbt_sn)) {
                        ?>
                        <div class="row mt10">
                            <label class="col-sm-6 col-xs-12">Số tiền bảo hiểm / ngày:</label>
                            <div class="col-sm-4 col-xs-12">
                                <span class="btn btn-success btn-sm"><?php echo get_price_in_vnd($st_bh_gh) ?></span>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="row mt10">
                        <label class="col-sm-6 col-xs-12">Số tiền yêu cầu bồi thường:</label>
                        <div class="col-sm-4 col-xs-12">
                            <span class="btn btn-danger btn-sm"><?php echo get_price_in_vnd($so_tien_ycbt) ?></span>
                        </div>
                    </div>
                    <?php
                    if (in_array($quyenloi_bt, $a_qlbt_sn)) {
                        ?>
                        <div class="row mt20">
                            <label class="col-sm-6 col-xs-12">Số ngày bảo hiểm còn lại:</label>
                            <div class="col-sm-4 col-xs-12">
                                <span class="btn btn-primary btn-sm"><?php echo ($sn_bh_cl) ?> - ngày</span>
                            </div>
                        </div>
                        <div class="row mt10">
                            <label class="col-sm-6 col-xs-12">Số ngày yêu cầu bồi thường:</label>
                            <div class="col-sm-4 col-xs-12">
                                <span class="btn btn-danger btn-sm"><?php echo ($so_ngay_ycbt) ?> - ngày</span>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="clear-both"></div>
                <div class="col-sm-12">
                    <div class="form-group box_quyenloi_bt" style="display: _none">
                        <?php
                        if (isset($bh_html)) {
                            echo $bh_html;
                        }
                        ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-6 col-xs-12">
                    <div class="row mt20">
                        <label class="col-sm-4">Mã bệnh:</label>
                        <select name="ma_benh" class="js-example-disabled-results col-sm-6">
                            <option value="">-- Mã bệnh --</option>
                            <?php
                            if (isset($ma_benh) && !empty($ma_benh)) {
                                foreach ($ma_benh as $index) {
                                    $id = $index->id;
                                    $name = $index->name;
                                    $ma_benh = $index->ma_benh;
                                    $selected = ($id == $bt_ma_benh) ? 'selected="selected"' : '';
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $ma_benh . ' - ' . $name ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <a target="_blank" href="<?php echo site_url('dashboard/products/origin/add') ?>" class="btn btn-primary btn-sm">Thêm mã bệnh</a>
                    </div>
                    <div class="row mt20">
                        <label class="col-sm-4">Chuẩn đoán bệnh:</label>
                        <?php echo form_textarea(array('rows' => 5, 'id' => 'chuandoan_benh', 'name' => 'chuandoan_benh', 'style' => '', 'value' => isset($chuandoan_benh) ? $chuandoan_benh : set_value('chuandoan_benh'))); ?>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12">
                    <div class="row mt20">
                        <label class="col-sm-4">Lý do không BT:</label>
                        <select name="lydo_khong_bt" class="js-example-disabled-results col-sm-6">
                            <option value="">-- Lý do không bồi thường --</option>
                            <?php
                            if (isset($a_lydo_khong_bt) && !empty($a_lydo_khong_bt)) {
                                foreach ($a_lydo_khong_bt as $k => $v) {
                                    $selected = ($k == $lydo_khong_bt) ? 'selected="selected"' : '';
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Chi tiết không BT:</label>
                        <?php echo form_textarea(array('rows' => 5, 'id' => 'ghichu_khong_bt', 'name' => 'ghichu_khong_bt', 'style' => '', 'value' => isset($ghichu_khong_bt) ? $ghichu_khong_bt : set_value('ghichu_khong_bt'))); ?>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="row mt20">
                    <div class="col-sm-2 col-xs-12">
                        <label>Ghi chú xử lý BT:</label>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <textarea class="fw" rows="6" name="ghichu_xuly_bt" value="<?php echo isset($ghichu_xuly_bt) ? $ghichu_xuly_bt : '' ?>"><?php echo isset($ghichu_xuly_bt) ? $ghichu_xuly_bt : '' ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <br class="clear"/><hr>
        <div style="margin-top: 10px;"></div>
        <?php
        if ($status == BT_CHOBOSUNG) {
            $action = 'Xác nhận chờ bổ sung';
        } else {
            $action = 'Xác nhận phương án bồi thường';
        }
        ?>
        <?php
        //if ($status != BT_DATRATIEN) {
        if ($ss_ql_all == QL_CHECKED || $ss_ql_bt_xuly == QL_CHECKED) {
            ?>
            <button class="btn btn-success mr20" type="submit"><?php echo $action ?></button>
            <?php
        }
        //}
        ?>
        <a class="btn btn-primary" href="<?php echo site_url(ORDER_ADMIN_BASE_URL) ?>">Quay lại <span class="glyphicon glyphicon-share-alt"></span></a>
        <br class="clear"/>&nbsp;
    </div>
    <?php
}
?>
<?php echo form_close(); ?>
<div class="modal fade bs-example-modal-lg modal_ct_bt" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">

</div>