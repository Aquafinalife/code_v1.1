$(window).load(function() {

    gototop();

});

function gototop()
{
    $(".gotop").hide();
    $(window).scroll(function() {
        if ($(this).scrollTop() > 10) {
            $('.gotop').fadeIn();
        } else {
            $('.gotop').fadeOut();
        }
    });
    $('.gotop').click(function() {
        $('body,html').animate({scrollTop: 4}, 300);
        return false;
    });
}
function send_acount() {

    var email = $('input[name=email]').val();
    emaildn = email;
    qu = /\w+(\@)[a-zA-Z]{2,6}(\.)[a-zA-Z]{2,5}|\w+(\@)[a-zA-Z]{2,6}(\.)[a-zA-Z]{2,5}(\.)[a-zA-Z]{2,5}$/;
    if (emaildn == "" || !qu.test(emaildn))
    {
        $('input[name=email]').focus();
        alert('Email không hợp lệ !')
        return false;
    }
    if (email === '' || email === null)
    {
        $('input[name=email]').focus();
        return false;
    }

    $.ajax({
        type: 'post',
        url: '/send-account-email',
        data: {
            'is-ajax': 1,
            'email': email,
        },
        beforeSend: function()
        {
            if ($('#send_email_account'))
                $('#send_email_account').html('<img src="/images/9.gif"> Đang gửi email')
        },
        success: function(responseText) {
            if (responseText === 'ok')
            {

                if ($('#send_email_account'))
                    $('#send_email_account').html('<p class="send_sucess">Bạn đã gửi email thành công</p>');
            }
        }
    });
}
function imagebox()
{
    $('.imagebox').fancybox({
         'autoSize' : false
    });
}

function fancyshow(id)
{
    $(".ft-"+id).fancybox({
        loop        : true,
        autoPlay    : true,
        playSpeed   : 4000,
        nextSpeed   : 500,
        prevSpeed   : 500,
        openSpeed   : 500,
        speedOut    : 500,
        openEffect  : "fade", 
        closeEffect : "fade",
        prevEffect	: 'fade', //fade none
        nextEffect	: 'fade', //fade none
        helpers	: {
            title	: {
                type: 'inside'
            },
            thumbs	: {
                width	: 50,
                height	: 50
            }
        }
    });
}

function update_cart(id,uri)
{
    var quantity = $('input[name=' + id + ']').val();
    $.ajax(
    {
        type: 'post',
        url: '/updatecart',
        data: {
            'id': id,
            'quantity': quantity,
        },
        success: function()
        {
            location.href = uri;
        }
    });
}

function remove_cart(row_id,uri,lang)
{
    if(lang == 'vi'){
        question = confirm("Bạn có muốn xóa?");
    }else{
        question = confirm("Are you sure you want delete?");
    }
    if (question)
    {
        $.ajax(
        {
            type: 'post',
            url: '/removecart',
            data: {
                //'is-ajax': 1,
                'id': row_id,
            },
            success: function(responseText)
            {
                location.href = uri;
            }
        });
    }
    else
    {
        return false;
    }
}

function sort_by()
{
    var sortby = $("select[name=sortby]").val();
    var redirect_url = $("input[name=redirect_url]").val();
    $.ajax({
        type:'post',
        url : '/products_sort_by',
        data:{
            'sortby' : sortby
        },
        success: function()
        {
            location.href = redirect_url;
        }
    });
}

function filter_add(attr,val)
{
    var redirect_url = $("input[name=redirect_url]").val();
    $.ajax({
        type:'post',
        url : '/products_filter',
        data:{
            'filter_attr' : attr,
            'filter_val'  : val
        },
        success: function()
        {
            location.href = redirect_url;
        }
    });
}

function filter_clear(attr)
{
    var redirect_url = $("input[name=redirect_url]").val();
    $.ajax({
        type:'post',
        url : '/products_filter_clear',
        data:{
            'filter_attr' : attr
        },
        success: function()
        {
            location.href = redirect_url;
        }
    });
}

function select_size()
{
    if($('span.sizebox').hasClass('sizebox_selected')) {
        $('span.sizebox').removeClass('sizebox_selected');
    }
}

function get_newsletter()
{
    var email = $('input[name=email]').val();
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
//    alert(email);return;
    if(email == '')
    {
        $('#error').html('Xin mời điền email của bạn vào đây');
    }
    else if(!emailReg.test(email))
    {
        $('#error').html('Email bạn điền không đúng');
    }
    else
    {
        $.ajax(
        {
            type:   'post',
            url:    '/news_letter',
            data:   {
                'email'        : email
            },
            success: function(responseText)
            {
                $('.news_letter').html(responseText);
                $('#error').html('Bạn đã đăng ký thành công');
            }
        });
    }
}
function fast_addtocart(id)
{
    var addUri = '/gio-hang';
    $.ajax(
    {
        type:'post',
            url : '/addtocart',
        data:{
            'id' : id
        },
        success: function(data)
            {
                location.href = addUri;
            }
    });
}
//function fast_addtocart()
//{
//   
//        var id = $('a.fast_addtocart').attr('data-url');
//        var addUri = '/gio-hang';
//        var lang = $('input[name=lang]').val();
//        alert(id);
//        $.ajax(
//        {
//            type:'post',
//            url : '/addtocart',
//            data:{
//                'id' : id,
//                
//            },
//            beforeSend: function(){
//                
//            },
//            success: function(data)
//            {
//                location.href = addUri;
//            }
//        });
   
//}
(function($){
    
    $('.addtocart').click(function(){
        var id = $('input[name=id]').val();
        var addUri = $('input[name=addUri]').val();
        var lang = $('input[name=lang]').val();
        var size = $('span.sizebox_selected').attr('title');
        $.ajax(
        {
            type:'post',
            url : '/addtocart',
            data:{
                'id' : id,
                'size' : size
            },
            beforeSend: function(){
                $('.addtocart').prop('disabled', true);
                if(lang == 'vi'){
                    $('.addtocart').html("Đang thêm vào giỏ...");
                }else{
                    $('.addtocart').html("Please wait...");
                }
            },
            success: function(data)
            {
                location.href = addUri;
            }
        });
    });
    
    

    jQuery(".partner_inner").owlCarousel({
        navigation : false,
        items: 8,
        itemsCustom: [[0, 1], [479, 2], [768, 5], [979, 8], [1199, 8], [1688, 8]],
        slideSpeed: 1000,
        rewindSpeed: 2000,
        paginationSpeed: 1000,
        autoPlay: 5000,
        pagination: true
    });
    
//    jQuery(window).on('load', function(){
//        $('.trademark').masonry({
//            itemSelector: '.trademark_item',
//            //columnWidth: 120
//        });
//    })

//    if($('span.sizebox').hasClass('sizebox_selected')) {
//        var size = $('span.sizebox_selected').attr('title');
//        $('input[name="size_selected"]').val(size);
//    }

})(jQuery);

// search cho product    
    $(function(){
$(".search").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
if(searchid!='')
{
    $.ajax({
    type: "POST",
    url: "/get_data_auto_complate_product",
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#result").html(html).show();
    }
    });
}return false;    
});


$('#searchid').click(function(){
    jQuery("#result").fadeIn();
});
});


function contact_quick()
{
    var fullname = $("#contact_quick_fullname").val();
    var email = $("#contact_quick_email").val();
    var tel = $("#contact_quick_tel").val();
    var message = $("#contact_quick_message").val();
    if(fullname === ''){
        $( "#contact_quick_fullname" ).animate({
            borderColor: "1px solid #dd0000"
        }, 'fast' );
        $("#contact_quick_fullname").delay(500).animate({
            borderColor: "1px solid #ccc"
        }, 'slow');
    }else if(email === ''){
        $("#contact_quick_email").animate({
            borderColor: "1px solid #dd0000"
        }, 'fast' );
        $("#contact_quick_email").delay(500).animate({
            borderColor: "1px solid #ccc"
        }, 'slow');
    }else if(tel === ''){
        $("#contact_quick_tel").animate({
            borderColor: "1px solid #dd0000"
        }, 'fast' );
        $("#contact_quick_tel").delay(500).animate({
            borderColor: "1px solid #ccc"
        }, 'slow');
//    }else if(message === ''){
//        $("#contact_quick_message").animate({
//            borderColor: "1px solid #dd0000"
//        }, 'fast' );
//        $("#contact_quick_message").delay(500).animate({
//            borderColor: "1px solid #ccc"
//        }, 'slow');
    }else{
        $.ajax(
        {
            type:'post',
            url : '/contact/contact_quick',
            data:{
                'fullname' : fullname,
                'email' : email,
                'tel' : tel,
                'message' : message
            },
//            dataType: "html",
            beforeSend: function(){
                $("#contact_quick_submit").attr("disabled", "disabled");
            },
            success: function(data)
            {
                if(data === '2'){
                    alert('Bạn đã gửi thông tin. Xin mời quay lại sau ít phút.');
                }else if(data === '1'){
                    alert('Cảm ơn bạn đã gửi thông tin.');
                }else if(data === '0'){
                    alert('Đã xảy ra lỗi.');
                }
                $("#contact_quick_submit").removeAttr("disabled");
                $("#contact_quick_fullname").val('');
                $("#contact_quick_email").val('');
                $("#contact_quick_tel").val('');
                $("#contact_quick_message").val('');
            }
        });
    }
}
