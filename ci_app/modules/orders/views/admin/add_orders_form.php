<?php
echo form_open($submit_uri);
if (isset($id))
    echo form_hidden('id', $id);
$fullname = isset($fullname) ? $fullname : '';
$email = isset($email) ? $email : '';
$created_date = isset($created_date) ? $created_date : '';
$submit_uri = isset($submit_uri) ? $submit_uri : '';
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'orders_cat');
?>

<?php
$a_phanloai_bt = get_a_phanloai_bt();
?>

<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Nội dung bồi thường"</small>
    <span class="fright"><a class="button close" href="<?php echo ORDER_ADMIN_BASE_URL; ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>

<div class="form_content">
    <?php $this->load->view('powercms/message'); ?>
    <ul class="tabs">
        <li><a href="#tab1">Thông tin người được bảo hiểm</a></li>
        <!--<li><a href="#tab2">Quyền lợi bảo hiểm tai nạn</a></li>-->
        <li><a href="#tab3">Tình trạng bồi thường</a></li>
    </ul>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <div class="form-group">
                <label for="exampleInputEmail1">Chọn theo CMT/Tên của người được bảo hiểm:</label>
                <select name="kh_dbh" class="js-example-disabled-results select_kh_dbh_bt">
                    <option value="">-- Chọn theo CMT/Tên của người được BH --</option>            
                    <?php
                    if (isset($kh_dbh) && !empty($kh_dbh)) {
                        //$a_dn = array();
                        foreach ($kh_dbh as $index) {
                            $congty_canhan = $index->congty_canhan;
                            $mst_cmt = $index->mst_cmt;
                            $id = $index->id;
                            //$selected = ($id == $kh_mbh) ? 'selected="selected"' : '';
                            ?>
                            <option <?php // echo $selected                     ?> value="<?php echo $id ?>"><?php echo $congty_canhan . ' - CMT: ' . $mst_cmt ?></option>
                            <?php
                            //$a_dn[] = $congty_canhan;
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="form-group box_selectbox_plbt" style="">
            </div>

            <div class="form-group box_selectbox_thaisan" style="display: none">
                <label for="exampleInputEmail1">Chọn loại thai sản:</label>
                <select name="qlbt_thaisan" class="js-example-disabled-results select_qlbt_thaisan">
                    <option value="">-- Chọn loại bồi thường Thai Sản --</option>            
                    <?php
                    $a_thaisan = array(
                        SINH_THUONG => 'Sinh thường',
                        SINH_MO => 'Sinh mổ',
                    );
                    if (isset($a_thaisan) && !empty($a_thaisan)) {
                        foreach ($a_thaisan as $k => $v) {
                            ?>
                            <option value="<?php echo $k ?>"><?php echo $v; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Ngày yêu cầu bồi thường</label>
                <?php echo form_input(array('class' => 'show_date', 'id' => 'news_created_date', 'name' => 'ngay_ycbt', 'size' => '50', 'maxlength' => '10', 'value' => isset($ngay_ycbt) ? $ngay_ycbt : set_value('ngay_ycbt'))); ?>
            </div>
            <div class="form-group box_chungtu_bt" style="display: none">
            </div>
            <div class="form-group box_ctbs_txt" style="display: none">
                <label for="exampleInputEmail1">Ghi chú chứng từ chờ bổ sung:</label>
                <?php echo form_textarea(array('id' => 'chungtu_bosung_txt', 'name' => 'chungtu_bosung_txt', 'style' => 'width:560px; height: 80px;', 'value' => ($chungtu_bosung_txt != '') ? $chungtu_bosung_txt : set_value('chungtu_bosung_txt'))); ?>
            </div>
            <div class="form-group box_quyenloi_bt" style="display: none">
            </div>
            <div class="box_content_bt1" style="display: none">
                <div class="form-group">
                    <div class="col-sm-3 col-xs-12">
                        <label for="exampleInputEmail1">Số tiền yêu cầu bồi thường</label>
                    </div>
                    <div class="col-sm-5 col-xs-12">
                        <!--<input value="" name="so_ngay_ycbt" type="number" class="form-control" id="exampleInputEmail1" placeholder="Số ngày yêu cầu bồi thường">-->
                        <?php echo form_input(array('name' => 'so_tien_ycbt', 'size' => '50', 'class' => 'tien_bt', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($so_tien_ycbt) ? $so_tien_ycbt : set_value('so_tien_ycbt'))); ?>
                    </div>
                    <!--<input type="text" class="_form-control" id="exampleInputEmail1" placeholder="Số tiền yêu cầu bồi thường">-->
                </div>
                <div class="clearfix"></div>
                <div class="form-group mt10">
                    <div class="col-sm-3 col-xs-12">
                        <label for="exampleInputEmail1">Số ngày yêu cầu bồi thường</label>
                    </div>
                    <div class="col-sm-5 col-xs-12">
                        <input min="0" value="" name="so_ngay_ycbt" type="number" class="form-control" id="exampleInputEmail1" placeholder="Số ngày yêu cầu bồi thường">
                        <i style="font-size: 13px;">(Chỉ điền khi quyền lợi yêu cầu số ngày)</i>
                    </div>
                    <?php // echo form_input(array('name' => 'so_ngay_ycbt', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($so_ngay_ycbt) ? $so_ngay_ycbt : set_value('so_ngay_ycbt'))); ?>

                </div>
                <div class="clearfix"></div>
                <!--                <div class="form-group">
                                    <label for="exampleInputEmail1">Số lần khám</label>
                <?php echo form_input(array('name' => 'so_lan_kham', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($so_lan_kham) ? $so_lan_kham : set_value('so_lan_kham'))); ?>
                                    <i style="font-size: 13px;">(Chỉ điền khi quyền lợi có số lần khám)</i>
                                </div>                -->
                <div class="form-group">
                    <label for="exampleInputEmail1">Ghi chú</label>
                    <?php echo form_textarea(array('id' => 'message', 'name' => 'message', 'style' => 'width:560px; height: 80px;', 'value' => ($message != '') ? $message : set_value('message'))); ?>
                </div>
            </div>

        </div>
        <div id="tab2" class="tab_content">
            <table class="list" style="width: 100%; margin-bottom: 10px;">
                <tr>
                    <th class="left" style="width: 5%">MÃ ĐH</th>
                    <th class="left" style="width: 5%">MÃ SP</th>
                    <th class="left" style="width: 40%">SẢN PHẨM</th>
                    <th class="left" style="width: 10%">KÍCH CỠ</th>
                    <th class="left" style="width: 10%">SỐ LƯỢNG</th>
                    <th class="left" style="width: 10%">GIÁ BÁN</th>
                    <th class="left" style="width: 10%">TỔNG</th>
                </tr>
                <?php
                if (isset($order_detail)) {
                    $stt = 0;
                    $amount = 0;
                    foreach ($order_detail as $index => $orders):
                        $style = $stt++ % 2 == 0 ? 'even' : 'odd';
                        $price = $orders->price != 0 ? get_price_in_vnd($orders->price) . ' VND' : get_price_in_vnd($orders->price);
                        $tong = $orders->price * $orders->quantity;
                        $total = $tong != 0 ? get_price_in_vnd($tong) . ' VND' : get_price_in_vnd($tong);
                        $amount = $amount + $tong;
                        ?>
                        <tr class="<?php echo $style ?>">
                            <td><?php echo '#' . $orders->order_id; ?></td>
                            <td><?php echo '#' . $orders->product_id ?></td>
                            <td style="white-space:nowrap;"><?php echo $orders->product_name; ?></td>
                            <td style="white-space:nowrap;"><?php echo $orders->size; ?></td>
                            <td style="white-space:nowrap;"><?php echo $orders->quantity; ?></td>
                            <td style="white-space:nowrap;color: red;text-align: right;"><?php echo $price; ?></td>
                            <td style="white-space:nowrap;color: red;text-align: right;"><?php echo $total; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr class="odd">
                        <?php $amounts = $amount != 0 ? get_price_in_vnd($amount) . ' VND' : get_price_in_vnd($amount); ?>
                        <td class="right" colspan="6" style="white-space:nowrap;"><b>Tổng giá trị đơn hàng</b></td>
                        <td class="right" style="white-space:nowrap;color: red;"><?php echo $amounts; ?></td>
                    </tr>
                    <tr class="odd">
                        <td colspan="7" style="text-align: right; color: red;font-weight: bold" class="total_price"><?php echo DocTienBangChu($amount); ?> </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <div id="tab3" class="tab_content">
            <table>
                <tr><td class="title">Khách hàng chọn hình thức thanh toán: </td></tr>
                <tr>
                    <td id="category"> <?php echo get_form_orders_icon($kind_pay) ?></td>
                </tr>
                <tr><td class="title">Tình trạng hóa đơn: </td></tr>
                <tr>
                    <td id="category"><?php if (isset($combo_order)) echo $combo_order; ?></td>
                </tr>
            </table>
        </div>
    </div>
    <br class="clear"/>
    <div style="margin-top: 10px;"></div>
    <?php echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn')); ?>
    <br class="clear"/>&nbsp;
</div>
<?php echo form_close(); ?>
<div class="modal fade bs-example-modal-lg modal_ct_bt" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">

</div>