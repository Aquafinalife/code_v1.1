<?php
echo form_open($submit_uri);
if (isset($id))
    echo form_hidden('id', $id);
$fullname = isset($fullname) ? $fullname : '';
$email = isset($email) ? $email : '';
$created_date = isset($created_date) ? $created_date : '';
$submit_uri = isset($submit_uri) ? $submit_uri : '';
?>

<?php
$a_phanloai_bt = get_a_phanloai_bt();
$a_phuongthuc_bt = get_a_phuongthuc_bt();
$a_nhantien_bt = get_a_nhantien_bt();
$a_nhan_hoso = get_a_nhan_hoso();
?>
<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Nội dung bồi thường"</small>
    <span class="fright"><a class="button close" href="<?php echo ORDER_ADMIN_BASE_URL; ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>

<div class="form_content">
    <?php $this->load->view('powercms/message'); ?>
    <ul class="tabs col-sm-12 mt10">
        <li><a href="#tab1">Thông tin bồi thường</a></li>
        <!--<li><a href="#tab2">Thông tin người được bảo hiểm</a></li>-->
        <!--<li><a href="#tab3">Thông tin thanh toán</a></li>-->
    </ul>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <div class="form-group">
                <label class="col-sm-4 col-xs-12" for="exampleInputEmail1">Chọn theo CMT/Tên của người được bảo hiểm:</label>
                <select name="kh_dbh" class="js-example-disabled-results select_kh_dbh_bt">
                    <option value="">-- Chọn theo CMT/Tên của người được BH --</option>            
                    <?php
                    if (isset($kh_dbh) && !empty($kh_dbh)) {
                        //$a_dn = array();
                        foreach ($kh_dbh as $index) {
                            $congty_canhan = $index->congty_canhan;
                            $mst_cmt = $index->mst_cmt;
                            $id = $index->id;
                            //$selected = ($id == $kh_mbh) ? 'selected="selected"' : '';
                            ?>
                            <option <?php // echo $selected                                       ?> value="<?php echo $id ?>"><?php echo $congty_canhan . ' - CMT: ' . $mst_cmt ?></option>
                            <?php
                            //$a_dn[] = $congty_canhan;
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label class="col-sm-4 col-xs-12" for="exampleInputEmail1">Mã Hồ sơ bồi thường:</label>
                <div class="">
                    <input type="text" name="bt_code" value=""/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 col-xs-12" for="exampleInputEmail1">Mã Case bồi thường:</label>
                <div class="">
                    <input type="text" name="case" value=""/>
                    <i>(Nhập mã Case nếu bồi thường là Case con)</i>
                </div>
            </div>

            <div class="form-group box_selectbox_plbt" style="">
            </div>

            <div class="form-group box_selectbox_thaisan" style="display: none">
                <label class="col-sm-4 col-xs-12" for="exampleInputEmail1">Chọn loại thai sản:</label>
                <select name="qlbt_thaisan" class="js-example-disabled-results select_qlbt_thaisan">
                    <option value="">-- Chọn loại bồi thường Thai Sản --</option>            
                    <?php
                    $a_thaisan = array(
                        SINH_THUONG => 'Sinh thường',
                        SINH_MO => 'Sinh mổ',
                    );
                    if (isset($a_thaisan) && !empty($a_thaisan)) {
                        foreach ($a_thaisan as $k => $v) {
                            ?>
                            <option value="<?php echo $k ?>"><?php echo $v; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="form-group box_chungtu_bt" style="display: none">
            </div>
            <div class="form-group box_ctbs_txt" style="display: none">
                <label class="col-sm-4 col-xs-12" for="exampleInputEmail1">Ghi chú chứng từ chờ bổ sung:</label>
                <?php echo form_textarea(array('id' => 'chungtu_bosung_txt', 'name' => 'chungtu_bosung_txt', 'style' => 'width:560px; height: 80px;', 'value' => ($chungtu_bosung_txt != '') ? $chungtu_bosung_txt : set_value('chungtu_bosung_txt'))); ?>
            </div>
            <div class="form-group box_quyenloi_bt" style="display: none">
            </div>

            <div class="box_content_bt1 row" style="display: none">
                <hr>
                <div class="col-sm-6 col-xs-12">
                    <div class="row mt20">
                        <label class="col-sm-4">Số tiền yêu cầu bồi thường:</label>
                        <?php echo form_input(array('name' => 'so_tien_ycbt', 'class' => 'tien_bt', 'maxlength' => '255', 'value' => isset($so_tien_ycbt) ? $so_tien_ycbt : set_value('so_tien_ycbt'))); ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Số ngày yêu cầu bồi thường:</label>
                        <input min="0" value="0" name="so_ngay_ycbt" type="number" class="" id="exampleInputEmail1" placeholder="">
                        <br>
                        <i style="font-size: 13px;">(Chỉ điền khi quyền lợi yêu cầu số ngày)</i>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">  
                    <div class="row mt20">
                        <label class="col-sm-4 col-xs-12">Ghi chú:</label>
                        <textarea name="ghichu_hoso" cols="35" rows="6"></textarea>
                        <?php // echo form_textarea(array('id' => 'ghichu_hoso', 'name' => 'ghichu_hoso', 'value' => ($ghichu_hoso != '') ? $ghichu_hoso : set_value('ghichu_hoso'))); ?>
                    </div>
                </div>
                <div class="clear-both"></div><hr>
            </div>

            <!--            <div class="box_content_bt2 row" style="display: none">
                            <hr>
                            <div class="col-sm-6 col-xs-12">
                                <div class="row mt20">
                                    <label class="col-sm-4">Số tiền yêu cầu bồi thường:</label>
            <?php // echo form_input(array('name' => 'so_tien_ycbt', 'class' => 'tien_bt', 'maxlength' => '255', 'value' => isset($so_tien_ycbt) ? $so_tien_ycbt : set_value('so_tien_ycbt'))); ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">  
                                <div class="row mt20">
                                    <label class="col-sm-4">Số ngày yêu cầu bồi thường:</label>
                                    <input min="0" value="0" name="so_ngay_ycbt" type="number" class="" id="exampleInputEmail1" placeholder="">
                                    <br>
                                    <i style="font-size: 13px;">(Chỉ điền khi quyền lợi yêu cầu số ngày)</i>
                                </div>
                            </div>
                            <div class="clear-both"></div><hr>
                        </div>-->

            <div class="box_content_info_bt row" style="display: none">    
                <hr>
                <div class="col-sm-6 col-xs-12">
                    <div class="row mt20">
                        <label class="col-sm-4">Phương thức bồi thường:</label>
                        <select name="phuongthuc_bt" class="">
                            <option value="">-- Chọn phương thức bồi thường --</option>
                            <?php
                            if (isset($a_phuongthuc_bt) && !empty($a_phuongthuc_bt)) {
                                foreach ($a_phuongthuc_bt as $k => $v) {
                                    ?>
                                    <option value="<?php echo $k ?>"><?php echo $v ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="row mt20">
                        <label class="col-sm-4">Chọn cơ sở y tế:</label>
                        <select name="csyt" class="js-example-disabled-results">
                            <option value="">-- Cơ sở y tế --</option>
                            <?php
                            if (isset($a_csyt) && !empty($a_csyt)) {
                                foreach ($a_csyt as $index) {
                                    $csyt_id = $index->id;
                                    $csyt_name = $index->name;
                                    $csyt_code = $index->csyt_code;
                                    ?>
                                    <option value="<?php echo $csyt_id ?>"><?php echo $csyt_code . ' - ' . $csyt_name ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <a href="<?php echo site_url('dashboard/products/style/add') ?>" class="btn btn-primary btn-sm">Thêm CSYT</a>
                    </div>
                    <div class="row mt20">
                        <label class="col-sm-4">Ngày xảy ra tổn thất:</label>
                        <input type="text" name="date_xayra_tonthat" class="show_datepicker" placeholder=""/>
                    </div>
                    <div class="row mt20">
                        <!--                        <label class="col-sm-4">Ngày nhận hồ sơ bồi thường:</label>-->
                        <label class="col-sm-4">Ngày yêu cầu bồi thường:</label>
                        <input type="text" name="date_nhan_bt" class="show_datepicker" placeholder="" value="<?php echo date('Y-m-d') ?>"/>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Ngày xử lý bồi thường:</label>
                        <input type="text" name="date_xuly_bt" class="show_datepicker" placeholder="" value="<?php echo date('Y-m-d') ?>"/>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">

                    <div class="row mt20">
                        <label class="col-sm-4 col-xs-12">Hình thức nhận hồ sơ:</label>
                        <select name="phuongthuc_nhan_hs" class="">
                            <option value="">-- Hình thức nhận hồ sơ --</option>
                            <?php
                            if (isset($a_nhan_hoso) && !empty($a_nhan_hoso)) {
                                foreach ($a_nhan_hoso as $k => $v) {
                                    ?>
                                    <option value="<?php echo $k ?>"><?php echo $v ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4 col-xs-12">Hình thức nhận tiền bồi thường:</label>
                        <select name="hinhthuc_nhantien_bt" class="">
                            <option value="">-- Hình thức nhận tiền bồi thường --</option>
                            <?php
                            if (isset($a_nhantien_bt) && !empty($a_nhantien_bt)) {
                                foreach ($a_nhantien_bt as $k => $v) {
                                    ?>
                                    <option value="<?php echo $k ?>"><?php echo $v ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <!--                    <div class="row mt20">
                                            <label class="col-sm-4 col-xs-12">Thông tin tài khoản:</label>
                                            <textarea name="ghichu_nhantien" cols="35" rows="6"></textarea>
                                        </div>-->
                </div>
                <div class="col-sm-12"><hr></div>
                <div class="col-sm-12">
                    <h2 class="text-center">Thông tin người thụ hưởng</h2>
                    <div class="col-sm-6 col-xs-12">
                        <div class="row mt20">
                            <label class="col-sm-4">Họ và tên:</label>
                            <input type="text" name="nth_fullname" class="" placeholder=""/>
                        </div>
                        <div class="row mt20">
                            <label class="col-sm-4">Ngân hàng:</label>
                            <select name="nth_bank" class="js-example-disabled-results">
                                <option value="">-- Chọn ngân hàng --</option>
                                <?php
                                if (isset($a_nh) && !empty($a_nh)) {
                                    foreach ($a_nh as $index) {
                                        $nh_id = $index->id;
                                        $nh_name = $index->name;
                                        $nh_code = $index->code;
                                        ?>
                                        <option value="<?php echo $nh_id ?>"><?php echo $nh_code . ' - ' . $nh_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="row mt20">
                            <label class="col-sm-4">Số tài khoản:</label>
                            <input type="text" name="nth_stk" class="" placeholder=""/>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12">                    
                        <div class="row mt20">
                            <label class="col-sm-4 col-xs-12">Ghi chú tài khoản người thụ hưởng:</label>
                            <textarea name="nth_bank_address" cols="35" rows="6"></textarea>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div id="tab2" class="tab_content">

        </div>
        <!--=================================================================-->
        <div id="tab3" class="tab_content">
            <div class="col-sm-6 col-xs-12">

            </div>
        </div>
        <!--=================================================================-->
    </div>
    <br class="clear"/>
    <div style="margin-top: 10px;"></div>
    <?php echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn')); ?>
    <br class="clear"/>&nbsp;
</div>
<?php echo form_close(); ?>
<div class="modal fade bs-example-modal-lg modal_ct_bt" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">

</div>