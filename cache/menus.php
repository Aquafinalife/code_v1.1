<?php $ip_menus = array (
  0 => 
  array (
    'id' => '152',
    'caption' => 'Trang chủ',
    'url_path' => '/',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => 'fa-home',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  1 => 
  array (
    'id' => '1',
    'caption' => 'Dashboard',
    'url_path' => '/dashboard',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => 'fa-dashboard',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  2 => 
  array (
    'id' => '742',
    'caption' => 'Gói bảo hiểm',
    'url_path' => '/dashboard/products/size',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '2',
    'position' => '4',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  3 => 
  array (
    'id' => '729',
    'caption' => 'Khách hàng',
    'url_path' => '/dashboard/kh',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '2',
    'position' => '10',
    'active' => '1',
    'css' => 'fa-users',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  4 => 
  array (
    'id' => '55',
    'caption' => 'Bảo hiểm',
    'url_path' => '/dashboard/products',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '2',
    'position' => '11',
    'active' => '1',
    'css' => 'fa-gift',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  5 => 
  array (
    'id' => '728',
    'caption' => 'Bồi thường',
    'url_path' => '/dashboard/orders',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '2',
    'position' => '18',
    'active' => '1',
    'css' => 'fa-bar-chart-o',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  6 => 
  array (
    'id' => '636',
    'caption' => 'Người dùng',
    'url_path' => '/dashboard/auth',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '2',
    'position' => '19',
    'active' => '1',
    'css' => 'fa-users',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  7 => 
  array (
    'id' => '101',
    'caption' => 'Giao diện',
    'url_path' => '#',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '2',
    'position' => '20',
    'active' => '1',
    'css' => 'fa-pencil',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  8 => 
  array (
    'id' => '100',
    'caption' => 'Cài đặt',
    'url_path' => '#',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '2',
    'position' => '21',
    'active' => '1',
    'css' => 'fa-cog',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  9 => 
  array (
    'id' => '906',
    'caption' => 'Báo cáo',
    'url_path' => '/dashboard/reports',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '2',
    'position' => '23',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  10 => 
  array (
    'id' => '126',
    'caption' => 'Tất cả bài viết',
    'url_path' => '/dashboard/news',
    'parent_id' => '14',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  11 => 
  array (
    'id' => '62',
    'caption' => 'Thêm bài viết',
    'url_path' => '/dashboard/news/add',
    'parent_id' => '14',
    'level' => '1',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  12 => 
  array (
    'id' => '45',
    'caption' => 'Phân loại bài viết',
    'url_path' => '/dashboard/news/cat',
    'parent_id' => '14',
    'level' => '1',
    'cat_id' => '2',
    'position' => '3',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  13 => 
  array (
    'id' => '129',
    'caption' => 'Tất cả menu',
    'url_path' => '/dashboard/menus',
    'parent_id' => '38',
    'level' => '2',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  14 => 
  array (
    'id' => '130',
    'caption' => 'Thêm menu',
    'url_path' => '/dashboard/menus/add',
    'parent_id' => '38',
    'level' => '2',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  15 => 
  array (
    'id' => '135',
    'caption' => 'Tất cả hỗ trợ',
    'url_path' => '/dashboard/supports',
    'parent_id' => '43',
    'level' => '2',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  16 => 
  array (
    'id' => '136',
    'caption' => 'Thêm hỗ trợ',
    'url_path' => '/dashboard/supports/add',
    'parent_id' => '43',
    'level' => '2',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  17 => 
  array (
    'id' => '132',
    'caption' => 'Tất cả banner',
    'url_path' => '/dashboard/advs',
    'parent_id' => '46',
    'level' => '2',
    'cat_id' => '2',
    'position' => '4',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  18 => 
  array (
    'id' => '133',
    'caption' => 'Thêm banner',
    'url_path' => '/dashboard/advs/add',
    'parent_id' => '46',
    'level' => '2',
    'cat_id' => '2',
    'position' => '5',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  19 => 
  array (
    'id' => '127',
    'caption' => 'Tất cả trang tin',
    'url_path' => '/dashboard/pages',
    'parent_id' => '50',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  20 => 
  array (
    'id' => '128',
    'caption' => 'Thêm trang tin',
    'url_path' => '/dashboard/pages/add',
    'parent_id' => '50',
    'level' => '1',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  21 => 
  array (
    'id' => '4',
    'caption' => 'Tất cả bảo hiểm',
    'url_path' => '/dashboard/products',
    'parent_id' => '55',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  22 => 
  array (
    'id' => '713',
    'caption' => 'Nhà cung cấp BH',
    'url_path' => '/dashboard/products/trademark',
    'parent_id' => '55',
    'level' => '1',
    'cat_id' => '2',
    'position' => '5',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  23 => 
  array (
    'id' => '714',
    'caption' => 'Mã bệnh',
    'url_path' => '/dashboard/products/origin',
    'parent_id' => '55',
    'level' => '1',
    'cat_id' => '2',
    'position' => '6',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  24 => 
  array (
    'id' => '744',
    'caption' => 'Cơ sở y tế',
    'url_path' => '/dashboard/products/style',
    'parent_id' => '55',
    'level' => '1',
    'cat_id' => '2',
    'position' => '10',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  25 => 
  array (
    'id' => '745',
    'caption' => 'Quản lý chứng từ',
    'url_path' => '/dashboard/products/coupon',
    'parent_id' => '55',
    'level' => '1',
    'cat_id' => '2',
    'position' => '11',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  26 => 
  array (
    'id' => '907',
    'caption' => 'Báo cáo',
    'url_path' => '/dashboard/reports',
    'parent_id' => '55',
    'level' => '1',
    'cat_id' => '2',
    'position' => '12',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  27 => 
  array (
    'id' => '87',
    'caption' => 'Thay đổi mật khẩu',
    'url_path' => '/dashboard/auth/change_password',
    'parent_id' => '100',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  28 => 
  array (
    'id' => '910',
    'caption' => 'Tỉnh thành',
    'url_path' => '/dashboard/products/state',
    'parent_id' => '100',
    'level' => '1',
    'cat_id' => '2',
    'position' => '3',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  29 => 
  array (
    'id' => '38',
    'caption' => 'Hệ thống Menu',
    'url_path' => '/dashboard/menus',
    'parent_id' => '101',
    'level' => '1',
    'cat_id' => '2',
    'position' => '4',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  30 => 
  array (
    'id' => '151',
    'caption' => 'Tất cả liên hệ',
    'url_path' => '/dashboard/contact',
    'parent_id' => '150',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  31 => 
  array (
    'id' => '199',
    'caption' => 'Tất cả Tài liệu',
    'url_path' => '/dashboard/download',
    'parent_id' => '175',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  32 => 
  array (
    'id' => '225',
    'caption' => 'Thêm Tài liệu',
    'url_path' => '/dashboard/download/add',
    'parent_id' => '175',
    'level' => '1',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  33 => 
  array (
    'id' => '200',
    'caption' => 'Phân loại Tài liệu',
    'url_path' => '/dashboard/download/cat',
    'parent_id' => '175',
    'level' => '1',
    'cat_id' => '2',
    'position' => '3',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  34 => 
  array (
    'id' => '222',
    'caption' => 'Tất cả tranh ảnh',
    'url_path' => '/dashboard/gallery',
    'parent_id' => '221',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  35 => 
  array (
    'id' => '223',
    'caption' => 'Thêm tranh ảnh',
    'url_path' => '/dashboard/gallery/add',
    'parent_id' => '221',
    'level' => '1',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  36 => 
  array (
    'id' => '224',
    'caption' => 'Phân loại tranh ảnh',
    'url_path' => '/dashboard/gallery/cat',
    'parent_id' => '221',
    'level' => '1',
    'cat_id' => '2',
    'position' => '3',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  37 => 
  array (
    'id' => '312',
    'caption' => 'Tất cả hỏi đáp',
    'url_path' => '/dashboard/faq',
    'parent_id' => '311',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  38 => 
  array (
    'id' => '313',
    'caption' => 'Thêm hỏi đáp',
    'url_path' => '/dashboard/faq/add',
    'parent_id' => '311',
    'level' => '1',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  39 => 
  array (
    'id' => '314',
    'caption' => 'Phân loại hỏi đáp',
    'url_path' => '/dashboard/faq/cat',
    'parent_id' => '311',
    'level' => '1',
    'cat_id' => '2',
    'position' => '3',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  40 => 
  array (
    'id' => '712',
    'caption' => 'Chuyên gia tư vấn',
    'url_path' => '/dashboard/faq/pro',
    'parent_id' => '311',
    'level' => '1',
    'cat_id' => '2',
    'position' => '4',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  41 => 
  array (
    'id' => '633',
    'caption' => 'Tất cả Videos',
    'url_path' => '/dashboard/videos',
    'parent_id' => '632',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  42 => 
  array (
    'id' => '634',
    'caption' => 'Thêm Videos',
    'url_path' => '/dashboard/videos/add',
    'parent_id' => '632',
    'level' => '1',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  43 => 
  array (
    'id' => '635',
    'caption' => 'Phân loại Videos',
    'url_path' => '/dashboard/videos/cat',
    'parent_id' => '632',
    'level' => '1',
    'cat_id' => '2',
    'position' => '3',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  44 => 
  array (
    'id' => '637',
    'caption' => 'Tất cả người dùng',
    'url_path' => '/dashboard/auth',
    'parent_id' => '636',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  45 => 
  array (
    'id' => '638',
    'caption' => 'Thêm người dùng',
    'url_path' => '/dashboard/auth/add',
    'parent_id' => '636',
    'level' => '1',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '0',
    'editor' => '0',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  46 => 
  array (
    'id' => '674',
    'caption' => 'Vai trò',
    'url_path' => '/dashboard/auth/roles',
    'parent_id' => '636',
    'level' => '1',
    'cat_id' => '2',
    'position' => '3',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  47 => 
  array (
    'id' => '683',
    'caption' => 'Quản lý phòng ban',
    'url_path' => '/dashboard/products/color',
    'parent_id' => '729',
    'level' => '1',
    'cat_id' => '2',
    'position' => '1',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  48 => 
  array (
    'id' => '743',
    'caption' => 'Danh sách Ngân Hàng',
    'url_path' => '/dashboard/products/material',
    'parent_id' => '729',
    'level' => '1',
    'cat_id' => '2',
    'position' => '2',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu quản trị',
  ),
  49 => 
  array (
    'id' => '748',
    'caption' => 'Thanh toán linh hoạt',
    'url_path' => '/thanh-toan-linh-hoat.html',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '6',
    'position' => '3',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu Hỗ trợ mua hàng',
  ),
  50 => 
  array (
    'id' => '753',
    'caption' => 'Giao hàng toàn quốc',
    'url_path' => '/giao-hang-toan-quoc.html',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '6',
    'position' => '8',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu Hỗ trợ mua hàng',
  ),
  51 => 
  array (
    'id' => '901',
    'caption' => 'Hướng Dẫn Mua Hàng',
    'url_path' => 'huong-dan-mua-hang',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '6',
    'position' => '9',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu Hỗ trợ mua hàng',
  ),
  52 => 
  array (
    'id' => '902',
    'caption' => 'Hướng Dẫn Thanh Toán',
    'url_path' => 'huong-dan-thanh-toan',
    'parent_id' => '0',
    'level' => '0',
    'cat_id' => '6',
    'position' => '10',
    'active' => '1',
    'css' => '',
    'thumbnail' => '',
    'icon' => '',
    'lang' => 'vi',
    'creator' => '1',
    'editor' => '1',
    'private' => '0',
    'name' => 'Menu Hỗ trợ mua hàng',
  ),
)?>