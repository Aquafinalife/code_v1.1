<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
$a_bt_thanhtoan = get_a_bt_thanhtoan();
//end
?>
<div class="box_filter" style="margin-bottom: 20px">
</div>
<table id="_table-edit" class="table table-bordered table-hover">    
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Báo cáo phân tích Top 10 HSBT có STBT cao nhất:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th>STT</th>
            <th>Loại bồi thường</th>
            <th>Loại bệnh</th>
            <th>Phương thức bồi thường</th>
            <th>Số tiền bồi thường</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($bt) && !empty($bt)) {
            $i = 1;
            foreach ($bt as $index) {
                $BT_id = $index->id;
                $OD_id = $index->OD_id;
                $KH_id = $index->KH_id;
                $kh_mbh = $index->kh_mbh;
                $phanloai_bt = $index->phanloai_bt;
                $phanloai_bt_txt = get_phanloai_bt($phanloai_bt);
                $OD_ma_benh = $index->OD_ma_benh;
                $ma_benh_txt = 'Empty';
                if (isset($a_ma_benh)) {
                    foreach ($a_ma_benh as $index1) {
                        $mb_id = $index1->id;
                        $mb_name = $index1->name;
                        if ($mb_id == $OD_ma_benh) {
                            $ma_benh_txt = $mb_name;
                            break;
                        }
                    }
                }
                $phuongthuc_bt = $index->phuongthuc_bt;
                $phuongthuc_bt_txt = get_phuongthuc_bt($phuongthuc_bt);
                $so_tien_dbt = $index->so_tien_dbt;
                $bt_status = $index->order_status;
                //==============================================================
                ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $phanloai_bt_txt ?></td>
                    <td><?php echo $ma_benh_txt ?></td>
                    <td><?php echo $phuongthuc_bt_txt ?></td>
                    <td><?php echo get_price_in_vnd($so_tien_dbt) ?></td>
                </tr>            
                <?php
                $i++;
            }
        } else {
            ?>
            <tr>
                <td colspan="5" class="text-danger">Chưa có HSBT nào thanh toán</td>
            </tr>  
            <?php
        }
        ?>
    </tbody>
</table>
<div class="col-sm-12 text-center">
    <div class="pagination">
        <?php // echo $this->pagination->create_links();   ?>
    </div>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>