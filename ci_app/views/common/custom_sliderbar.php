<ul class="side-menu-list">
    <?php
    $cmt_key = isset($_GET['cmt_key']) ? $_GET['cmt_key'] : '';
    $ss_kh_id = $this->phpsession->get('ss_kh_id');
    $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
    $ss_bbh_id = $this->phpsession->get('ss_bbh_id');
    $ss_csyt_id = $this->phpsession->get('ss_csyt_id');
    if ($ss_csyt_id > 0) {
        $url_dashboard = URL_CSYT_DASHBOARD . '?cmt_key=' . $cmt_key;
    } else if ($ss_bbh_id > 0) {
        $url_dashboard = URL_BBH_DASHBOARD;
    } else {
        $url_dashboard = URL_KH_DASHBOARD;
    }
    ?>
    <a href="<?php echo site_url($url_dashboard) ?>">
        <i class="font-icon font-icon-dashboard"></i>
        <span class="lbl">Thông tin chung</span>
    </a>
    <li class="brown with-sub">
        <span>
            <i class="font-icon glyphicon glyphicon-tint"></i>
            <span class="lbl">Bảo hiểm</span>
        </span>
        <ul>
            <?php
            if ($ss_csyt_id > 0 && $ss_csyt_id != '') {
                ?>
                <li><a href="<?php echo site_url(URL_CSYT_BH) . '?cmt_key=' . $cmt_key ?>"><span class="lbl">Thông tin bảo hiểm</span></a></li>
                <?php
            } elseif ($ss_bbh_id > 0 && $ss_bbh_id != '') {
                ?>
                <li><a href="<?php echo site_url(URL_BBH_BH) ?>"><span class="lbl">Thông tin bảo hiểm</span></a></li>
                <?php
            } else if ($ss_kh_phanloai_kh == CANHAN) {
                ?>
                <li><a href="<?php echo site_url(URL_BH_CN) ?>"><span class="lbl">Thông tin bảo hiểm</span></a></li>
                <?php
            } else {
                ?>
                <li><a href="<?php echo site_url(URL_BH_DN) ?>"><span class="lbl">Thông tin bảo hiểm</span></a></li>
                <?php
            }
            ?>
<!--            <li><a href="theme-side-caesium-dark-caribbean.html"><span class="lbl">Caesium Dark Caribbean</span></a></li>
<li><a href="theme-side-tin.html"><span class="lbl">Tin</span></a></li>
<li><a href="theme-side-litmus-blue.html"><span class="lbl">Litmus Blue</span></a></li>
<li><a href="theme-rebecca-purple.html"><span class="lbl">Rebecca Purple</span></a></li>
<li><a href="theme-picton-blue.html"><span class="lbl">Picton Blue</span></a></li>
<li><a href="theme-picton-blue-white-ebony.html"><span class="lbl">Picton Blue White Ebony</span></a></li>-->
        </ul>
    </li>
    <li class="purple with-sub">
        <span>
            <i class="font-icon font-icon-comments active"></i>
            <span class="lbl">Bồi thường</span>
        </span>
        <ul>
            <?php
            if ($ss_csyt_id > 0 && $ss_csyt_id != '') {
                ?>
                        <!--<li><a href="<?php // echo site_url(URL_CSYT_BT).'?cmt_key='.$cmt_key   ?>"><span class="lbl">Thông tin bồi thường</span></a></li>-->
                <?php
            } elseif ($ss_bbh_id > 0 && $ss_bbh_id != '') {
                ?>
                <li><a href="<?php echo site_url(URL_BBH_BT) ?>"><span class="lbl">Thông tin bồi thường</span></a></li>
                <?php
            } else if ($ss_kh_phanloai_kh == CANHAN) {
                ?>
                <li><a href="<?php echo site_url(URL_BT_CN) ?>"><span class="lbl">Thông tin bồi thường</span></a></li>
                <?php
            } else {
                ?>
                <li><a href="<?php echo site_url(URL_BT_DN) ?>"><span class="lbl">Thông tin bồi thường</span></a></li>
                <?php
            }
            ?>                        
        </ul>
    </li>
    <?php
    if ($ss_kh_phanloai_kh == DOANHNGHIEP) {
        ?>
        <li class="blue with-sub">
            <span>
                <i class="glyphicon glyphicon-folder-open"></i>
                <span class="lbl">Báo cáo </span>
            </span>
            <ul>
                <li><a href="<?php echo site_url('slbtc') ?>"><span class="lbl">Số liệu bồi thường chung</span></a></li>
                <li><a href="<?php echo site_url('slbtc?bc=' . DN_NV . '&plbt=nv') ?>"><span class="lbl">Số liệu bồi thường nhân viên</span></a></li>
                <li><a href="<?php echo site_url('slbtc?bc=' . DN_NT . '&plbt=nt') ?>"><span class="lbl">Số liệu bồi thường người thân</span></a></li>
                <li><a href="<?php echo site_url('bcplbt') ?>"><span class="lbl">Phân tích HSBT theo quyền lợi</span></a></li>
                <li><a href="<?php echo site_url('bcnb') ?>"><span class="lbl">Phân tích HSBT theo nhóm bệnh</span></a></li>
                <!--<li><a href="<?php // echo site_url('top10csyt') ?>"><span class="lbl">Top 10 CSYT có BT cao nhất</span></a></li>-->
                <li><a href="<?php echo site_url('top10hsbt') ?>"><span class="lbl">Top 10 HSBT cao nhất</span></a></li>

    <!--            <li><a href="#"><span class="lbl">Số liệu bồi thường chung</span></a></li>
    <li><a href="#"><span class="lbl">Bồi thường nhân viên</span></a></li>
    <li><a href="#"><span class="lbl">Phân tích theo quyền lợi</span></a></li>
    <li><a href="#"><span class="lbl">Top 10 HSBT cao nhất</span></a></li>-->
            </ul>
        </li>
        <?php
    }
    ?>
    <li class="blue with-sub">
        <span>
            <i class="font-icon font-icon-user"></i>
            <span class="lbl">Profile</span>
        </span>
        <ul>
            <li><a href="<?php echo site_url('profile')?>"><span class="lbl">Thông tin cá nhân</span></a></li>
            <li><a href="<?php echo site_url('change-password')?>"><span class="lbl">Thay đổi mật khẩu</span></a></li>
        </ul>
    </li>
    <li>        
    </li>
</ul>
<section>
    <div class="_col-sm-12 text-center mb20">
        <button style="background-color: #fd6533;" class="context-menu-one btn btn-warning fw text-warning">Hotline: 1900 6364 38</button>    
    </div>
    <div class="list_box_gt">
        <ul>
            <li>
                <a target="_blank" style="width: 80px" href="http://ftcclaims.com/huong-dan-boi-thuong-bao-hiem-suc-khoe.html"><img class="fw" src="http://ftcclaims.com/images/source/tpaftc/BHSK.jpg" alt="bao hiem suc khoe" data-pin-nopin="true"></a>
            </li>
            <li>
                <a target="_blank" style="width: 80px" href="http://ftcclaims.com/tai-nguyen-mau-don-yeu-cau-boi-thuong.html"><img class="fw" src="http://ftcclaims.com/images/source/tpaftc/DOWNLOAD.jpg" alt="tai nguyen, download" data-pin-nopin="true"></a>
            </li>
            <li>
                <a target="_blank" style="width: 80px" href="http://ftcclaims.com/danh-sach-benh-vien-bao-lanh.html"><img class="fw" src="http://ftcclaims.com/images/source/tpaftc/BVBAOLANH.jpg" alt="BENH VIEN bao lanh" data-pin-nopin="true"></a>
            </li>
            <li>
                <a target="_blank" style="width: 80px" href="http://ftcclaims.com/danh-sach-nha-thuoc-co-hoa-don-vat.html"><img class="fw" src="http://ftcclaims.com/images/source/tpaftc/NHATHUOC.jpg" alt="BENH VIEN bao lanh" data-pin-nopin="true"></a>
            </li>
        </ul>
    </div>
</section>

<!--<section>
    <header class="side-menu-title">Tags</header>
    <ul class="side-menu-list">
        <li>
            <a href="#">
                <i class="tag-color green"></i>
                <span class="lbl">Website</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="tag-color grey-blue"></i>
                <span class="lbl">Bugs/Errors</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="tag-color red"></i>
                <span class="lbl">General Problem</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="tag-color pink"></i>
                <span class="lbl">Questions</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="tag-color orange"></i>
                <span class="lbl">Ideas</span>
            </a>
        </li>
    </ul>
</section>-->