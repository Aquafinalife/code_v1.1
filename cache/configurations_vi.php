<?php $ip_configurations_vi = array (
  'id' => '1',
  'lang' => 'vi',
  'contact_email' => 'zodinestore@gmail.com',
  'order_email' => 'zodinestore@gmail.com',
  'meta_title' => 'Kênh mua hàng trực tuyến các sản phẩm tốt nhất với giá cạnh tranh nhất',
  'meta_keywords' => 'Kênh mua hàng trực tuyến các sản phẩm tốt nhất với giá cạnh tranh nhất',
  'meta_description' => 'Kênh mua hàng trực tuyến các sản phẩm tốt nhất với giá cạnh tranh nhất',
  'favicon' => 'favicon_16c47.png',
  'logo' => 'logo25f7d_27556.gif',
  'news_per_page' => '20',
  'products_per_page' => '10',
  'number_products_per_home' => '6',
  'number_news_per_home' => '0',
  'number_news_per_side' => '0',
  'products_side_per_page' => '0',
  'image_per_page' => '10',
  'google_tracker' => '<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-68843777-1\', \'auto\');
  ga(\'send\', \'pageview\');

</script>',
  'webmaster_tracker' => '<meta name="google-site-verification" content="0AVtK2FLShiEj0rFGC8JEJ8lP0XUrdllC7kBHauwIoo" />',
  'order_email_content' => '<p>Xin k&iacute;nh ch&agrave;o qu&yacute; kh&aacute;ch: <strong>{ten_nguoi_dat}</strong></p>
<p>Xin ch&uacute;c mừng qu&yacute; kh&aacute;ch đ&atilde; đặt h&agrave;ng th&agrave;nh c&ocirc;ng.</p>
<ul>
<li>M&atilde; số đơn h&agrave;ng: <strong>{ma_don_hang}</strong></li>
<li>Thời gian đặt h&agrave;ng: Ng&agrave;y <strong>{ngay_dat}</strong>, v&agrave;o l&uacute;c <strong>{gio_dat}</strong></li>
<li>Địa chỉ: <strong>{dia_chi}</strong></li>
<li>Điện thoại li&ecirc;n hệ: <strong>{dien_thoai}</strong></li>
<li>Email li&ecirc;n hệ: <strong>{email}</strong></li>
</ul>
<p>Đơn h&agrave;ng:</p>
<p>{danh_sach_san_pham}</p>
<p>Ch&uacute;ng t&ocirc;i sẽ li&ecirc;n lạc lại với bạn trong thời gian gần nhất.</p>
<p><span style="color: #ff0000;"><strong>Z&Ocirc; ĐI N&Egrave;</strong></span><br /> <br /><strong>Địa chỉ :&nbsp;Tầng 4, T&ograve;a nh&agrave; 110 Nguyễn Ngọc Nại, Q. Thanh Xu&acirc;n, H&agrave; Nội.</strong></p>
<p><strong>Tel : 04.6275.0383 - 04.3736.9083 - 0906.043.689</strong></p>
<p><strong>Website:<a href="http://zodine.com/"> <span style="color: #ff0000;">www.zodine.com</span></a></strong></p>',
  'footer_infomation' => '0',
  'footer_contact' => '<p><strong>ZODINE.COM: Tầng 4, T&ograve;a nh&agrave; 110 Nguyễn Ngọc Nại, Q. Thanh Xu&acirc;n, H&agrave; Nội</strong></p>
<p><strong>Điện thoại:&nbsp;</strong>+84 979996422 -<strong> Email:</strong>&nbsp;<a href="mailto:zodinestore@gmail.com">zodinestore@gmail.com</a> -&nbsp;<strong>Website:&nbsp;</strong><a href="http://zodine.com/">http://zodine.com</a>&nbsp;- <strong>Facebook: zodine</strong></p>',
  'footer_logo' => '',
  'footer_link_list' => '0',
  'company_infomation' => '<div class="top-bar-social"><a href="#"><span class="fa fa-facebook">&nbsp;</span></a> <a href="#"><span class="fa fa-twitter">&nbsp;</span></a> <a href="#"><span class="fa fa-pinterest">&nbsp;</span></a> <a href="#"><span class="fa fa-google-plus">&nbsp;</span></a> <a href="#"><span class="fa fa-phone">&nbsp;</span> 096768 1011</a></div>',
  'contact_infomation' => '<p style="text-align: left;"><strong><span style="color: #ff0000; font-size: 24pt;">Z&Ocirc; ĐI N&Egrave;</span></strong></p>
<p style="text-align: left;"><span style="font-size: 10pt;"><strong>Địa chỉ:&nbsp;Tầng 4, T&ograve;a nh&agrave; 110 Nguyễn Ngọc Nại, Q. Thanh Xu&acirc;n, H&agrave; Nội.</strong></span></p>
<p>Điện thoại: 097 898 1797</p>
<p><strong>Website:</strong> <a href="http://www.zodine.com">www.zodine.com</a></p>
<p><strong>Email:</strong>&nbsp;zodinestore@gmail.com</p>',
  'google_map_code' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1862.0839299510064!2d105.82870018071584!3d21.025968487079048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1437055027307" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
  'telephone' => '04.6275.0383 - 04.3736.9083 - 0906.043.689',
  'facebook_id' => '',
  'number_products_per_side' => '0',
  'editor' => '1',
  'slogan' => '',
  'pay_bank' => '<p class="compname">C&Aacute;C T&Agrave;I KHOẢN NG&Acirc;N H&Agrave;NG</p>
<table style="height: 104px;" width="746">
<tbody>
<tr>
<td class="imgbank" rowspan="4" valign="middle"><img src="https://www.abay.vn/images/AbayV3/Payment/bank-logo-VCB.gif" alt="" /></td>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td class="column1">T&ecirc;n t&agrave;i khoản : DỊCH THU NGUYỆT</td>
<td class="Tkname">&nbsp;</td>
</tr>
<tr>
<td class="column1">Số t&agrave;i khoản : 0611000188171</td>
<td>&nbsp;</td>
</tr>
<tr>
<td class="column1">Chi nh&aacute;nh: KIM M&Atilde;, BA Đ&Igrave;NH, H&Agrave; NỘI</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<table style="height: 104px;" width="743">
<tbody>
<tr>
<td class="imgbank" rowspan="4" valign="middle"><img src="https://www.abay.vn/images/AbayV3/Payment/bank-logo-TCB.gif" alt="" /></td>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td class="column1">T&ecirc;n t&agrave;i khoản :</td>
<td class="Tkname">&nbsp;</td>
</tr>
<tr>
<td class="column1">Số t&agrave;i khoản :</td>
<td>&nbsp;</td>
</tr>
<tr>
<td class="column1">Chi nh&aacute;nh:</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>',
  'pay_people' => '<p>Ch&uacute;ng t&ocirc;i sẽ cho nh&acirc;n vi&ecirc;n đến tận văn ph&ograve;ng hoặc nh&agrave; bạn để thu tiền thuận tiện cho bạn</p>',
  'pay_info' => '<p>Xin mời bạn đến theo địa chỉ của c&ocirc;ng ty để thanh to&aacute;n:</p>
<p><br /><strong>Địa chỉ :&nbsp;Tầng 4, T&ograve;a nh&agrave; 110 Nguyễn Ngọc Nại, Q. Thanh Xu&acirc;n, H&agrave; Nội.</strong></p>
<p><strong>Tel :&nbsp;097 898 1797</strong></p>',
  'success_order' => '<p><strong>Ch&uacute;c mừng bạn đ&atilde; đặt h&agrave;ng th&agrave;nh c&ocirc;ng! Ch&uacute;ng t&ocirc;i sẽ li&ecirc;n lạc với bạn trong thời gian sớm nhất.</strong></p>
<p><strong>Mời bạn&nbsp;<a title="Quay lại trang chủ" href="http://zodine.com/">Click v&agrave;o đ&acirc;y</a> để quay lại trang chủ.</strong></p>',
  'livechat' => '',
)?>