<?php
$cl_ql_bt_2 = '.box_nhap_thongtin_bt_';
$checked = 'checked="checked"';
$disabled = 'disabled="disabled"';
$disabled_df = '';
//==============================================================================
$a_qlbt_sn = get_a_loai_benh_sn();
$a_qlbt_slk = get_a_loai_benh_slk();
//==============================================================================
$a_bt_status1 = get_a_bt_status1();
$a_quyenloi_bt1 = get_a_phanloai_bt1();
$a_quyenloi_bt2 = get_a_phanloai_bt2();
//==============================================================================
if (isset($bt) && is_object($bt)) {
    $quyenloi_bt = $bt->quyenloi_bt;
    $phanloai_bt = $bt->phanloai_bt;
    $phanloai_thaisan = $bt->phanloai_thaisan;
    $so_tien_ycbt = $bt->so_tien_ycbt;
    $so_tien_dbt = $bt->so_tien_dbt;
    $so_ngay_ycbt = $bt->so_ngay_ycbt;
    $so_ngay_dbt = $bt->so_ngay_dbt;
    $bt_status = $bt->order_status;
    if (in_array($bt_status, $a_bt_status1)) {
        $disabled_df = 'disabled = true';
    }
}
//==============================================================================
$disabled_check = isset($disabled) ? $disabled : '';
$phanloai_bt = isset($phanloai_bt) ? $phanloai_bt : '';
//end
//==============================================================================
if (isset($bh) && is_object($bh)) {
   
    $CL_tv_tttbvv_tn_st_cl = $TV_TTTBVV_TN = $bh->TV_TTTBVV_TN;
    $CL_tv_tttbvv_tn_tv_st_cl = $CL_tv_tttbvv_tn_tt_st_cl = $TV_TTTBVV_TN;
    //$CL_tv_tttbvv_tn_tv_st_cl = $tv_tttbvv_tn_tv = $bh->tv_tttbvv_tn_tv;
    //$CL_tv_tttbvv_tn_tt_st_cl = $tv_tttbvv_tn_tt = $bh->tv_tttbvv_tn_tt;
    $CL_tcl_nn_st = $TCL_NN = $bh->TCL_NN;
    $CL_tcl_nn_sn_cl = $tcl_nn_sn = $bh->tcl_nn_sn;
    $CL_tcl_nn_st_cl = (int) $CL_tcl_nn_st * (int) $CL_tcl_nn_sn_cl;
    $CL_cpyt_tn_st_cl = $CPYT_TN = $bh->CPYT_TN;
    $CL_tv_tttbvv_ob_st_cl = $TV_TTTBVV_OB = $bh->TV_TTTBVV_OB;
    $CL_tv_tttbvv_ob_tv_st_cl = $CL_tv_tttbvv_ob_tt_st_cl= $TV_TTTBVV_OB;
    $CL_dtnt_ob_st_cl = $DTNT_OB = $bh->DTNT_OB;
    $CL_dtnt_ob_dbh_st_cl = $dtnt_dbh = $bh->dtnt_dbh;
    $CL_dtnt_ob_tvpn = $dtnt_tvpn = $bh->dtnt_tvpn;
    $CL_dtnt_ob_tvpn_sn_cl = $dtnt_tvpn_sn = $bh->dtnt_tvpn_sn;
    $CL_dtnt_ob_tgpn = $dtnt_tgpn = $bh->dtnt_tgpn;
    $CL_dtnt_ob_tgpn_sn_cl = $dtnt_tgpn_sn = $bh->dtnt_tgpn_sn;
    $CL_dtnt_ob_cppt_st_cl = $dtnt_cppt = $bh->dtnt_cppt;
    $CL_dtnt_ob_tk_nv_st_cl = $dtnt_tk_nv = $bh->dtnt_tk_nv;
    $CL_dtnt_ob_sk_nv_st_cl = $dtnt_sk_nv = $bh->dtnt_sk_nv;
    $CL_dtnt_ob_cpyt_tn_st_cl = $dtnt_cpyt_tn = $bh->dtnt_cpyt_tn;
    $CL_dtnt_ob_tcvp = $dtnt_tcvp = $bh->dtnt_tcvp;
    $CL_dtnt_ob_tcvp_sn_cl = $dtnt_tcvp_sn = $bh->dtnt_tcvp_sn;
    $CL_dtnt_ob_tcmt_st_cl = $dtnt_tcmt = $bh->dtnt_tcmt;
    $CL_dtnt_ob_xct_st_cl = $dtnt_xct = $bh->dtnt_xct;
    $CL_dtnt_ob_thaisan_st_cl = $THAISAN_QLNT = $bh->THAISAN_QLNT;
    $CL_dtnt_ob_ktdk_st_cl = $tsqlnt_ktdk = $bh->tsqlnt_ktdk;
    $CL_dtnt_ob_st_st_cl = $tsqlnt_st = $bh->tsqlnt_st;
    $CL_dtnt_ob_sm_st_cl = $tsqlnt_sm = $bh->tsqlnt_sm;
    $CL_dtnt_ob_dn_st_cl = $tsqlnt_dn = $bh->tsqlnt_dn;
    $CL_dtngt_ob_st_cl = $DTNGT_OB = $bh->DTNGT_OB;
    $dtngt_dbh = $bh->dtngt_dbh;
    $dtngt_st1lk = $bh->dtngt_st1lk;
    $CL_dtngt_ob_slk_sl_cl = $dtngt_slk = $bh->dtngt_slk;
    $CL_dtngt_ob_nkngt_st_cl = $dtngt_nkngt = $bh->dtngt_nkngt;
    $CL_dtngt_ob_nk_st_cl = $dtngt_nk = $bh->dtngt_nk;
    $CL_dtngt_ob_cvr_st_cl = $dtngt_cvr = $bh->dtngt_cvr;
    $CL_tsmdl_st_cl = $THAISAN_MDL = $bh->THAISAN_MDL;
    $tsmdl_dbh = $bh->tsmdl_dbh;
    $CL_tsmdl_ktdk_st_cl = $tsmdl_ktdk = $bh->tsmdl_ktdk;
    $CL_tsmdl_st_st_cl = $tsmdl_st = $bh->tsmdl_st;
    $CL_tsmdl_sm_st_cl = $tsmdl_sm = $bh->tsmdl_sm;
    $CL_tsmdl_dn_st_cl = $tsmdl_dn = $bh->tsmdl_dn;
    $CL_nhakhoa_mdl_st_cl = $NHAKHOA_MDL = $bh->NHAKHOA_MDL;
    $nkmdl_dbh = $bh->nkmdl_dbh;
    $CL_nkmdl_cb_st_cl = $nkmdl_cb = $bh->nkmdl_cb;
    $CL_nkmdl_db_st_cl = $nkmdl_db = $bh->nkmdl_db;
    $CL_nkmdl_cvr_st_cl = $nkmdl_cvr = $bh->nkmdl_cvr;
    //==========================================================================
    $st_bh_gh = $bh->$quyenloi_bt;
    //==========================================================================
    ?>
    <?php
    //==========================================================================
//    if (isset($qlbt) && count($qlbt) == 1) {
//        foreach ($qlbt as $index) {
//            $QLBT_name = $index->QLBT_name;
//            $qlbt_st_ycbt = $index->qlbt_st_ycbt;
//            $qlbt_st_dbt = $index->qlbt_st_dbt;
//        }
//    }
//    $QLBT_name = isset($QLBT_name) ? $QLBT_name : '';
//    $qlbt_st_ycbt = isset($qlbt_st_ycbt) ? $qlbt_st_ycbt : '';
//    $qlbt_st_dbt = isset($qlbt_st_dbt) ? $qlbt_st_dbt : '0';
    //==========================================================================
    $bt_tv_tttbvv_tn = $bt_tv_tttbvv_tn_tv = $bt_tv_tttbvv_tn_tt = $bt_tv_tttbvv_ob = $bt_tv_tttbvv_ob_tv = $bt_tv_tttbvv_ob_tt = '';
    $tv_tttbvv_tn_st_ycbt = $tv_tttbvv_tn_tv_st_ycbt = $tv_tttbvv_tn_tt_st_ycbt = $tv_tttbvv_ob_st_ycbt = $tv_tttbvv_ob_tv_st_ycbt = $tv_tttbvv_ob_tt_st_ycbt = '';
    $tv_tttbvv_tn_st_dbt = $tv_tttbvv_tn_tv_st_dbt = $tv_tttbvv_tn_tt_st_dbt = $tv_tttbvv_ob_st_dbt = $tv_tttbvv_ob_tv_st_dbt = $tv_tttbvv_ob_tt_st_dbt = '';
    //==========================================================================
    $bt_tcl_nn = '';
    $tcl_nn_st_ycbt = $tcl_nn_sn_ycbt = '';
    $tcl_nn_st_dbt = $tcl_nn_sn_dbt = '';
    //==========================================================================
    $bt_cpyt_tn = '';
    $cpyt_tn_st_ycbt = '';
    $cpyt_tn_st_dbt = '';
    //==========================================================================
    $bt_nkmdl_db = $bt_nkmdl_cb = $bt_nkmdl_cvr = '';
    $nkmdl_db_st_ycbt = $nkmdl_cb_st_ycbt = $nkmdl_cvr_st_ycbt = '';
    $nkmdl_db_st_dbt = $nkmdl_cb_st_dbt = $nkmdl_cvr_st_dbt = '';
    //==========================================================================
    $bt_tsmdl = $bt_tsmdl_ktdk = $bt_tsmdl_st = $bt_tsmdl_sm = $bt_tsmdl_dn = '';
    $tsmdl_st_ycbt = $tsmdl_ktdk_st_ycbt = $tsmdl_st_st_ycbt = $tsmdl_sm_st_ycbt = $tsmdl_dn_st_ycbt = '';
    $tsmdl_st_dbt = $tsmdl_ktdk_st_dbt = $tsmdl_st_st_dbt = $tsmdl_sm_st_dbt = $tsmdl_dn_st_dbt = '';
    //==========================================================================
    $bt_dtngt_ob = $bt_dtngt_ob_st1lk = $bt_dtngt_ob_nkngt = $bt_dtngt_ob_slk = $bt_dtngt_ob_nk = $bt_dtngt_ob_cvr = '';
    $dtngt_ob_st_ycbt = $dtngt_ob_st1lk_st_ycbt = $dtngt_ob_nkngt_st_ycbt = $dtngt_ob_nk_st_ycbt = $dtngt_ob_cvr_st_ycbt = '';
    $dtngt_ob_st_dbt = $dtngt_ob_st1lk_st_dbt = $dtngt_ob_nkngt_st_dbt = $dtngt_ob_nk_st_dbt = $dtngt_ob_cvr_st_dbt = '';
    //==========================================================================
    $bt_dtnt_ob = $bt_dtnt_ob_tvpn = $bt_dtnt_ob_tvpn_sn = $bt_dtnt_ob_tgpn = $bt_dtnt_ob_tgpn_sn = $bt_dtnt_ob_cppt = '';
    $bt_dtnt_ob_tk_nv = $bt_dtnt_ob_sk_nv = $bt_dtnt_ob_cpyt_tn = $bt_dtnt_tcvp = $bt_dtnt_ob_tcvp_sn = $bt_dtnt_ob_tcmt = $bt_dtnt_ob_xct = '';
    $bt_thaisan_qlnt = $bt_tsqlnt_ktdk = $bt_tsqlnt_st = $bt_tsqlnt_sm = $bt_tsqlnt_dn = '';
    //--------------------------------------------------------------------------
    $dtnt_ob_st_ycbt = $dtnt_ob_tvpn_st_ycbt = $dtnt_ob_tvpn_sn_ycbt = $dtnt_ob_tgpn_st_ycbt = $dtnt_ob_tgpn_sn_ycbt = $dtnt_ob_cppt_st_ycbt = '';
    $dtnt_ob_tk_nv_st_ycbt = $dtnt_ob_sk_nv_st_ycbt = $dtnt_ob_cpyt_tn_st_ycbt = $dtnt_ob_tcvp = $dtnt_ob_tcvp_sn_ycbt = $dtnt_ob_tcmt_st_ycbt = $dtnt_ob_xct_st_ycbt = '';
    $tsqlnt_st_ycbt = $tsqlnt_ktdk_st_ycbt = $tsqlnt_st_st_ycbt = $tsqlnt_sm_st_ycbt = $tsqlnt_dn_st_ycbt = '';
    //--------------------------------------------------------------------------
    $dtnt_ob_st_dbt = $dtnt_ob_tvpn_st_dbt = $dtnt_ob_tvpn_sn_dbt = $dtnt_ob_tgpn_st_dbt = $dtnt_ob_tgpn_sn_dbt = $dtnt_ob_cppt_st_dbt = '';
    $dtnt_ob_tk_nv_st_dbt = $dtnt_ob_sk_nv_st_dbt = $dtnt_ob_cpyt_tn_st_dbt = $dtnt_ob_tcvp = $dtnt_ob_tcvp_sn_dbt = $dtnt_ob_tcmt_st_dbt = $dtnt_ob_xct_st_dbt = '';
    $tsqlnt_st_dbt = $tsqlnt_ktdk_st_dbt = $tsqlnt_st_st_dbt = $tsqlnt_sm_st_dbt = $tsqlnt_dn_st_dbt = '';
    //==========================================================================
    if (isset($qlbt) && !empty($qlbt)) {
//        echo '<pre>';
//        print_r($qlbt);
//        die;
        foreach ($qlbt as $index) {
            $QLBT_name_2 = $index->QLBT_name;
            $qlbt_st_ycbt_2 = $index->qlbt_st_ycbt;
            $qlbt_st_dbt_2 = $index->qlbt_st_dbt;
            $qlbt_sn_ycbt_2 = $index->qlbt_sn_ycbt;
            $qlbt_sn_dbt_2 = $index->qlbt_sn_dbt;
            //==================================================================
            if ($QLBT_name_2 == TV_TTTBVV_TN) {
                $bt_tv_tttbvv_tn = 1;
                $tv_tttbvv_tn_st_ycbt = $qlbt_st_ycbt_2;
                $tv_tttbvv_tn_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tv_tttbvv_tn_tv) {
                $bt_tv_tttbvv_tn_tv = 1;
                $tv_tttbvv_tn_tv_st_ycbt = $qlbt_st_ycbt_2;
                $tv_tttbvv_tn_tv_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tv_tttbvv_tn_tt) {
                $bt_tv_tttbvv_tn_tt = 1;
                $tv_tttbvv_tn_tt_st_ycbt = $qlbt_st_ycbt_2;
                $tv_tttbvv_tn_tt_st_dbt = $qlbt_st_dbt_2;
            }
            //==================================================================
            if ($QLBT_name_2 == TV_TTTBVV_OB) {
                $bt_tv_tttbvv_ob = 1;
                $tv_tttbvv_ob_st_ycbt = $qlbt_st_ycbt_2;
                $tv_tttbvv_ob_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tv_tttbvv_ob_tv) {
                $bt_tv_tttbvv_ob_tv = 1;
                $tv_tttbvv_ob_tv_st_ycbt = $qlbt_st_ycbt_2;
                $tv_tttbvv_ob_tv_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tv_tttbvv_ob_tt) {
                $bt_tv_tttbvv_ob_tt = 1;
                $tv_tttbvv_ob_tt_st_ycbt = $qlbt_st_ycbt_2;
                $tv_tttbvv_ob_tt_st_dbt = $qlbt_st_dbt_2;
            }
            //==================================================================
            if ($QLBT_name_2 == TCL_NN) {
                $bt_tcl_nn = 1;
                $tcl_nn_st_ycbt = $qlbt_st_ycbt_2;
                $tcl_nn_st_dbt = $qlbt_st_dbt_2;
                $tcl_nn_sn_ycbt = $qlbt_sn_ycbt_2;
                $tcl_nn_sn_dbt = $qlbt_sn_dbt_2;
            }
            //==================================================================
            if ($QLBT_name_2 == CPYT_TN) {
                $bt_cpyt_tn = 1;
                $cpyt_tn_st_ycbt = $qlbt_st_ycbt_2;
                $cpyt_tn_st_dbt = $qlbt_st_dbt_2;
            }
            //==================================================================
            if ($QLBT_name_2 == NHAKHOA_MDL) {
                $bt_nkmdl = 1;
                $nkmdl_st_ycbt = $qlbt_st_ycbt_2;
                $nkmdl_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == nkmdl_cb) {
                $bt_nkmdl_cb = 1;
                $nkmdl_cb_st_ycbt = $qlbt_st_ycbt_2;
                $nkmdl_cb_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == nkmdl_db) {
                $bt_nkmdl_db = 1;
                $nkmdl_db_st_ycbt = $qlbt_st_ycbt_2;
                $nkmdl_db_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == nkmdl_cvr) {
                $bt_nkmdl_cvr = 1;
                $nkmdl_cvr_st_ycbt = $qlbt_st_ycbt_2;
                $nkmdl_cvr_st_dbt = $qlbt_st_dbt_2;
            }
            //==================================================================
            if ($QLBT_name_2 == THAISAN_MDL) {
                $bt_tsmdl = 1;
                $tsmdl_st_ycbt = $qlbt_st_ycbt_2;
                $tsmdl_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tsmdl_ktdk) {
                $bt_tsmdl_ktdk = 1;
                $tsmdl_ktdk_st_ycbt = $qlbt_st_ycbt_2;
                $tsmdl_ktdk_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tsmdl_st) {
                $bt_tsmdl_st = 1;
                $tsmdl_st_st_ycbt = $qlbt_st_ycbt_2;
                $tsmdl_st_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tsmdl_sm) {
                $bt_tsmdl_sm = 1;
                $tsmdl_sm_st_ycbt = $qlbt_st_ycbt_2;
                $tsmdl_sm_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tsmdl_dn) {
                $bt_tsmdl_dn = 1;
                $tsmdl_dn_st_ycbt = $qlbt_st_ycbt_2;
                $tsmdl_dn_st_dbt = $qlbt_st_dbt_2;
            }
            //==================================================================
            if ($QLBT_name_2 == DTNGT_OB) {
                $bt_dtngt_ob = 1;
                $dtngt_ob_st_ycbt = $qlbt_st_ycbt_2;
                $dtngt_ob_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == dtngt_st1lk) {
                $bt_dtngt_ob_st1lk = 1;
                $dtngt_ob_st1lk_st_ycbt = $qlbt_st_ycbt_2;
                $dtngt_ob_st1lk_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == dtngt_nkngt) {
                $bt_dtngt_ob_nkngt = 1;
                $dtngt_ob_nkngt_st_ycbt = $qlbt_st_ycbt_2;
                $dtngt_ob_nkngt_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == dtngt_nk) {
                $bt_dtngt_ob_nk = 1;
                $dtngt_ob_nk_st_ycbt = $qlbt_st_ycbt_2;
                $dtngt_ob_nk_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == dtngt_cvr) {
                $bt_dtngt_ob_cvr = 1;
                $dtngt_ob_cvr_st_ycbt = $qlbt_st_ycbt_2;
                $dtngt_ob_cvr_st_dbt = $qlbt_st_dbt_2;
            }
            //==================================================================
            if ($QLBT_name_2 == DTNT_OB) {
                $bt_dtnt_ob = 1;
                $dtngt_ob_st_ycbt = $qlbt_st_ycbt_2;
                $dtngt_ob_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == dtnt_tvpn) {
                $bt_dtnt_ob_tvpn = 1;
                $dtnt_ob_tvpn_st_ycbt = $qlbt_st_ycbt_2;
                $dtnt_ob_tvpn_st_dbt = $qlbt_st_dbt_2;
                $dtnt_ob_tvpn_sn_ycbt = $qlbt_sn_ycbt_2;
                $dtnt_ob_tvpn_sn_dbt = $qlbt_sn_dbt_2;
            }
            if ($QLBT_name_2 == dtnt_tgpn) {
                $bt_dtnt_ob_tgpn = 1;
                $dtnt_ob_tgpn_st_ycbt = $qlbt_st_ycbt_2;
                $dtnt_ob_tgpn_st_dbt = $qlbt_st_dbt_2;
                $dtnt_ob_tgpn_sn_ycbt = $qlbt_sn_ycbt_2;
                $dtnt_ob_tgpn_sn_dbt = $qlbt_sn_dbt_2;
            }
            if ($QLBT_name_2 == dtnt_cppt) {
                $bt_dtnt_ob_cppt = 1;
                $dtnt_ob_cppt_st_ycbt = $qlbt_st_ycbt_2;
                $dtnt_ob_cppt_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == dtnt_tk_nv) {
                $bt_dtnt_ob_tk_nv = 1;
                $dtnt_ob_tk_nv_st_ycbt = $qlbt_st_ycbt_2;
                $dtnt_ob_tk_nv_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == dtnt_sk_nv) {
                $bt_dtnt_ob_sk_nv = 1;
                $dtnt_ob_sk_nv_st_ycbt = $qlbt_st_ycbt_2;
                $dtnt_ob_sk_nv_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == dtnt_cpyt_tn) {
                $bt_dtnt_ob_cpyt_tn = 1;
                $dtnt_ob_cpyt_tn_st_ycbt = $qlbt_st_ycbt_2;
                $dtnt_ob_cpyt_tn_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == dtnt_tcvp) {
                $bt_dtnt_ob_tcvp = 1;
                $dtnt_ob_tcvp_st_ycbt = $qlbt_st_ycbt_2;
                $dtnt_ob_tcvp_st_dbt = $qlbt_st_dbt_2;
                $dtnt_ob_tcvp_sn_ycbt = $qlbt_sn_ycbt_2;
                $dtnt_ob_tcvp_sn_dbt = $qlbt_sn_dbt_2;
            }
            if ($QLBT_name_2 == dtnt_tcmt) {
                $bt_dtnt_ob_tcmt = 1;
                $dtnt_ob_tcmt_st_ycbt = $qlbt_st_ycbt_2;
                $dtnt_ob_tcmt_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == dtnt_xct) {
                $bt_dtnt_ob_xct = 1;
                $dtnt_ob_xct_st_ycbt = $qlbt_st_ycbt_2;
                $dtnt_ob_xct_st_dbt = $qlbt_st_dbt_2;
            }
            //==================================================================
            if ($QLBT_name_2 == THAISAN_QLNT) {
                $bt_thaisan_qlnt = 1;
                $tsqlnt_st_ycbt = $qlbt_st_ycbt_2;
                $tsqlnt_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tsqlnt_ktdk) {
                $bt_tsqlnt_ktdk = 1;
                $tsqlnt_ktdk_st_ycbt = $qlbt_st_ycbt_2;
                $tsqlnt_ktdk_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tsqlnt_st) {
                $bt_tsqlnt_st = 1;
                $tsqlnt_st_st_ycbt = $qlbt_st_ycbt_2;
                $tsqlnt_st_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tsqlnt_sm) {
                $bt_tsqlnt_sm = 1;
                $tsqlnt_sm_st_ycbt = $qlbt_st_ycbt_2;
                $tsqlnt_sm_st_dbt = $qlbt_st_dbt_2;
            }
            if ($QLBT_name_2 == tsqlnt_dn) {
                $bt_tsqlnt_dn = 1;
                $tsqlnt_dn_st_ycbt = $qlbt_st_ycbt_2;
                $tsqlnt_dn_st_dbt = $qlbt_st_dbt_2;
            }
            //==================================================================
        }
    }
    //==========================================================================
    if (isset($qlbt_cl) && !empty($qlbt_cl)) {
//        echo '<pre>';
//        print_r($qlbt_cl);
//        die;
        foreach ($qlbt_cl as $index) {
            $CL_quyenloi_bt = $index->CL_quyenloi_bt;
            $CL_st_bh_cl = $index->CL_st_bh_cl;
            $CL_sn_bh_cl = $index->CL_sn_bh_cl;
            $CL_slk_bh_cl = $index->CL_slk_bh_cl;
            //==================================================================
            if ($CL_quyenloi_bt == TV_TTTBVV_TN) {
                $CL_tv_tttbvv_tn_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tv_tttbvv_tn_tv) {
                $CL_tv_tttbvv_tn_tv_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tv_tttbvv_tn_tt) {
                $CL_tv_tttbvv_tn_tt_st_cl = $CL_st_bh_cl;
            }
            //==================================================================
            if ($CL_quyenloi_bt == TV_TTTBVV_OB) {
                $CL_tv_tttbvv_ob_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tv_tttbvv_ob_tv) {
                $CL_tv_tttbvv_ob_tv_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tv_tttbvv_ob_tt) {
                $CL_tv_tttbvv_ob_tt_st_cl = $CL_st_bh_cl;
            }
            //==================================================================
            if ($CL_quyenloi_bt == TCL_NN) {
                $CL_tcl_nn_st_cl = $CL_st_bh_cl;
                $CL_tcl_nn_sn_cl = $CL_sn_bh_cl;
            }
            //==================================================================
            if ($CL_quyenloi_bt == CPYT_TN) {
                $CL_cpyt_tn_st_cl = $CL_st_bh_cl;
            }
            //==================================================================
            if ($CL_quyenloi_bt == NHAKHOA_MDL) {
                $CL_nhakhoa_mdl_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == nkmdl_cb) {
                $CL_nkmdl_cb_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == nkmdl_db) {
                $CL_nkmdl_db_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == nkmdl_cvr) {
                $CL_nkmdl_cvr_st_cl = $CL_st_bh_cl;
            }
            //==================================================================
            if ($CL_quyenloi_bt == THAISAN_MDL) {
                $CL_tsmdl_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tsmdl_ktdk) {
                $CL_tsmdl_ktdk_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tsmdl_st) {
                $CL_tsmdl_st_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tsmdl_sm) {
                $CL_tsmdl_sm_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tsmdl_dn) {
                $CL_tsmdl_dn_st_cl = $CL_st_bh_cl;
            }
            //==================================================================
            if ($CL_quyenloi_bt == DTNGT_OB) {
                $CL_dtngt_ob_st_cl = $CL_st_bh_cl;
                $CL_dtngt_ob_slk_sl_cl = $CL_slk_bh_cl;
            }
            if ($CL_quyenloi_bt == dtngt_st1lk) {
                //$CL_dtngt_ob_st1lk_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == dtngt_nkngt) {
                $CL_dtngt_ob_nkngt_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == dtngt_nk) {
                $CL_dtngt_ob_nk_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == dtngt_cvr) {
                $CL_dtngt_ob_cvr_st_cl = $CL_st_bh_cl;
            }
            //==================================================================
            if ($CL_quyenloi_bt == DTNT_OB) {
                $CL_dtnt_ob_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == dtnt_tvpn) {
                $CL_dtnt_ob_tvpn_st_cl = $CL_st_bh_cl;
                $CL_dtnt_ob_tvpn_sn_cl = $CL_sn_bh_cl;
            }
            if ($CL_quyenloi_bt == dtnt_tgpn) {
                $CL_dtnt_ob_tgpn_st_cl = $CL_st_bh_cl;
                $CL_dtnt_ob_tgpn_sn_cl = $CL_sn_bh_cl;
            }
            if ($CL_quyenloi_bt == dtnt_cppt) {
                $CL_dtnt_ob_cppt_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == dtnt_tk_nv) {
                $CL_dtnt_ob_tk_nv_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == dtnt_sk_nv) {
                $CL_dtnt_ob_sk_nv_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == dtnt_cpyt_tn) {
                $CL_dtnt_ob_cpyt_tn_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == dtnt_tcvp) {
                $CL_dtnt_ob_tcvp_st_cl = $CL_st_bh_cl;
                $CL_dtnt_ob_tcvp_sn_cl = $CL_sn_bh_cl;
            }
            if ($CL_quyenloi_bt == dtnt_tcmt) {
                $CL_dtnt_ob_tcmt_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == dtnt_xct) {
                $CL_dtnt_ob_xct_st_cl = $CL_st_bh_cl;
            }
            //------------------------------------------------------------------
            if ($CL_quyenloi_bt == THAISAN_QLNT) {
                $CL_dtnt_ob_thaisan_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tsqlnt_ktdk) {
                $CL_dtnt_ob_ktdk_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tsqlnt_st) {
                $CL_dtnt_ob_st_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tsqlnt_sm) {
                $CL_dtnt_ob_sm_st_cl = $CL_st_bh_cl;
            }
            if ($CL_quyenloi_bt == tsqlnt_dn) {
                $CL_dtnt_ob_dn_st_cl = $CL_st_bh_cl;
            }
            //==================================================================
        }
    }
    //==========================================================================
    $a_TV_TTTBVV_TN = array($CL_tv_tttbvv_tn_st_cl, $CL_tv_tttbvv_tn_tv_st_cl, $CL_tv_tttbvv_tn_tt_st_cl);
    $TV_TTTBVV_TN_st_bt_max = min($a_TV_TTTBVV_TN);
    //==========================================================================
    $a_TV_TTTBVV_OB = array($CL_tv_tttbvv_ob_st_cl, $CL_tv_tttbvv_ob_tv_st_cl, $CL_tv_tttbvv_ob_tt_st_cl);
    $TV_TTTBVV_OB_st_bt_max = min($a_TV_TTTBVV_OB);
    //==========================================================================
    $tcl_nn_sn_bt_max = ($so_ngay_ycbt > $CL_tcl_nn_sn_cl) ? $CL_tcl_nn_sn_cl : $so_ngay_ycbt;
    //$tcl_nn_st_bt_max = ($so_tien_ycbt > $CL_tcl_nn_st_cl) ? $CL_tcl_nn_st_cl : $so_tien_ycbt;
    $tcl_nn_st_bt_max = (int) $tcl_nn_sn_bt_max * (int) $CL_tcl_nn_st;
    $tcl_nn_st_bt_rw = ($so_tien_ycbt > $tcl_nn_st_bt_max) ? $tcl_nn_st_bt_max : $so_tien_ycbt;
    //==========================================================================
    $cpyt_tn_st_bt_max = ($so_tien_ycbt > $CL_cpyt_tn_st_cl) ? $CL_cpyt_tn_st_cl : $so_tien_ycbt;
    //==========================================================================
    // Nếu ko có ql db
    if ($nkmdl_db == 0) {
        $a_NHAKHOA_MDL = array($CL_nhakhoa_mdl_st_cl, $CL_nkmdl_cb_st_cl, $CL_nkmdl_cvr_st_cl);
    }
    if ($nkmdl_cb == 0) {
        $a_NHAKHOA_MDL = array($CL_nhakhoa_mdl_st_cl, $CL_nkmdl_db_st_cl, $CL_nkmdl_cvr_st_cl);
    }
    $nkmdl_cvr_st_bt_max = min($a_NHAKHOA_MDL);
    $nkmdl_cb_st_bt_max = min(array($CL_nkmdl_cb_st_cl, $CL_nhakhoa_mdl_st_cl));
    $nkmdl_db_st_bt_max = min(array($CL_nkmdl_db_st_cl, $CL_nhakhoa_mdl_st_cl));
    //==========================================================================
    $tsmdl_ktdk_st_bt_max = ($CL_tsmdl_st_cl > $CL_tsmdl_ktdk_st_cl) ? $CL_tsmdl_ktdk_st_cl : $CL_tsmdl_st_cl;
    $tsmdl_st_st_bt_max = ($CL_tsmdl_st_cl > $CL_tsmdl_st_st_cl) ? $CL_tsmdl_st_st_cl : $CL_tsmdl_st_cl;
    $tsmdl_sm_st_bt_max = ($CL_tsmdl_st_cl > $CL_tsmdl_sm_st_cl) ? $CL_tsmdl_sm_st_cl : $CL_tsmdl_st_cl;
    $tsmdl_dn_st_bt_max = ($CL_tsmdl_st_cl > $CL_tsmdl_dn_st_cl) ? $CL_tsmdl_dn_st_cl : $CL_tsmdl_st_cl;
    //==========================================================================
    $dtngt_ob_st1lk_st_bt_max = ($CL_dtngt_ob_st_cl > $dtngt_st1lk) ? $dtngt_st1lk : $CL_dtngt_ob_st_cl;
    $dtngt_ob_slk_bt_max = ($CL_dtngt_ob_slk_sl_cl > 0) ? $CL_dtngt_ob_slk_sl_cl : 0;
    $a_dtngt_ob_nk = array($CL_dtngt_ob_st_cl, $CL_dtngt_ob_nkngt_st_cl, $CL_dtngt_ob_nk_st_cl);
    $dtngt_ob_nk_st_bt_max = min($a_dtngt_ob_nk);
    $a_dtngt_ob_cvr = array($CL_dtngt_ob_st_cl, $CL_dtngt_ob_nkngt_st_cl, $CL_dtngt_ob_cvr_st_cl);
    $dtngt_ob_cvr_st_bt_max = min($a_dtngt_ob_cvr);
    //==========================================================================
    $dtnt_ob_tvpn_sn_bt_max = ($so_ngay_ycbt > $CL_dtnt_ob_tvpn_sn_cl) ? $CL_dtnt_ob_tvpn_sn_cl : $so_ngay_ycbt;
    $dtnt_ob_tvpn_st_bt_max = (int) $dtnt_ob_tvpn_sn_bt_max * (int) $CL_dtnt_ob_tvpn;
    $dtnt_ob_tvpn_st_bt_rw = ($CL_dtnt_ob_st_cl > $dtnt_ob_tvpn_st_bt_max) ? $dtnt_ob_tvpn_st_bt_max : $CL_dtnt_ob_st_cl;
    //--------------------------------------------------------------------------
    $dtnt_ob_tgpn_sn_bt_max = ($so_ngay_ycbt > $CL_dtnt_ob_tgpn_sn_cl) ? $CL_dtnt_ob_tgpn_sn_cl : $so_ngay_ycbt;
    $dtnt_ob_tgpn_st_bt_max = (int) $dtnt_ob_tgpn_sn_bt_max * (int) $CL_dtnt_ob_tgpn;
    $dtnt_ob_tgpn_st_bt_rw = ($CL_dtnt_ob_st_cl > $dtnt_ob_tgpn_st_bt_max) ? $dtnt_ob_tgpn_st_bt_max : $CL_dtnt_ob_st_cl;
    //--------------------------------------------------------------------------
    $dtnt_ob_tcvp_sn_bt_max = ($so_ngay_ycbt > $CL_dtnt_ob_tcvp_sn_cl) ? $CL_dtnt_ob_tcvp_sn_cl : $so_ngay_ycbt;
    $dtnt_ob_tcvp_st_bt_max = (int) $dtnt_ob_tcvp_sn_bt_max * (int) $CL_dtnt_ob_tcvp;
    $dtnt_ob_tcvp_st_bt_rw = ($CL_dtnt_ob_st_cl > $dtnt_ob_tcvp_st_bt_max) ? $dtnt_ob_tcvp_st_bt_max : $CL_dtnt_ob_st_cl;
    //--------------------------------------------------------------------------
    $dtnt_ob_cppt_st_bt_max = ($CL_dtnt_ob_st_cl > $CL_dtnt_ob_cppt_st_cl) ? $CL_dtnt_ob_cppt_st_cl : $CL_dtnt_ob_st_cl;
    $dtnt_ob_tk_nv_st_bt_max = ($CL_dtnt_ob_st_cl > $CL_dtnt_ob_tk_nv_st_cl) ? $CL_dtnt_ob_tk_nv_st_cl : $CL_dtnt_ob_st_cl;
    $dtnt_ob_sk_nv_st_bt_max = ($CL_dtnt_ob_st_cl > $CL_dtnt_ob_sk_nv_st_cl) ? $CL_dtnt_ob_sk_nv_st_cl : $CL_dtnt_ob_st_cl;
    $dtnt_ob_cpyt_st_tn_bt_max = ($CL_dtnt_ob_st_cl > $CL_dtnt_ob_cpyt_tn_st_cl) ? $CL_dtnt_ob_cpyt_tn_st_cl : $CL_dtnt_ob_st_cl;
    $dtnt_ob_tcmt_st_bt_max = ($CL_dtnt_ob_st_cl > $CL_dtnt_ob_tcmt_st_cl) ? $CL_dtnt_ob_tcmt_st_cl : $CL_dtnt_ob_st_cl;
    $dtnt_ob_xct_st_bt_max = ($CL_dtnt_ob_st_cl > $CL_dtnt_ob_xct_st_cl) ? $CL_dtnt_ob_xct_st_cl : $CL_dtnt_ob_st_cl;
    //==========================================================================
    $tsqlnt_st_bt_max = ($CL_dtnt_ob_st_cl > $CL_dtnt_ob_thaisan_st_cl) ? $CL_dtnt_ob_thaisan_st_cl : $CL_dtnt_ob_st_cl;
    $a_tsqlnt_ktdk = array($CL_dtnt_ob_st_cl, $CL_dtnt_ob_thaisan_st_cl, $CL_dtnt_ob_ktdk_st_cl);
    $tsqlnt_ktdk_st_bt_max = min($a_tsqlnt_ktdk);
    $a_tsqlnt_st = array($CL_dtnt_ob_st_cl, $CL_dtnt_ob_thaisan_st_cl, $CL_dtnt_ob_st_st_cl);
    $tsqlnt_st_st_bt_max = min($a_tsqlnt_st);
    $a_tsqlnt_sm = array($CL_dtnt_ob_st_cl, $CL_dtnt_ob_thaisan_st_cl, $CL_dtnt_ob_sm_st_cl);
    $tsqlnt_sm_st_bt_max = min($a_tsqlnt_sm);
    $a_tsqlnt_dn = array($CL_dtnt_ob_st_cl, $CL_dtnt_ob_thaisan_st_cl, $CL_dtnt_ob_dn_st_cl);
    $tsqlnt_dn_st_bt_max = min($a_tsqlnt_dn);
    //==========================================================================
    // Nếu đã bồi thường
    if (isset($qlbt_cl) && !empty($qlbt_cl)) {
        foreach ($qlbt_cl as $index) {
            $CL_quyenloi_bt = $index->CL_quyenloi_bt;
            $CL_st_bh_cl = $index->CL_st_bh_cl;
            $CL_sn_bh_cl = $index->CL_sn_bh_cl;
            //==================================================================
            if ($quyenloi_bt == $CL_quyenloi_bt) {
                $CL_st_bh_cl_t = $CL_st_bh_cl;
            }
        }
    } else {
        // Nếu chưa bt lần nào
        // Nếu bt có số ngày
        if (in_array($quyenloi_bt, $a_qlbt_sn)) {
            $field_quyenloi_bt_sn = strtolower($quyenloi_bt) . '_sn';
            $sn_bh_gh = $bh->$field_quyenloi_bt_sn;
            $st_bh_gh_sn = (int) $st_bh_gh * (int) $sn_bh_gh;
            //==================================================================            
        }
    }
    //end
    //==========================================================================
    $st_bh_cl_t = isset($CL_st_bh_cl_t) ? $CL_st_bh_cl_t : (isset($st_bh_gh_sn) ? $st_bh_gh_sn : (isset($st_bh_gh) ? $st_bh_gh : 0));
//    echo $st_bh_cl_t . '/';
    //==========================================================================
    ?>
    <div class="goi_bao_hiem">
        <div class="col-sm-12 mb20 mt20">
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_TVTTTBVV_TN) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            TỬ VONG, TTTBVV do tai nạn
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền yêu cầu bt
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền còn lại
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Số tiền bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_tv_tttbvv_tn_tv == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tv_tttbvv_tn_tv' ?>" name="quyenloi_bt_2" value="tv_tttbvv_tn_tv"> 
                                Tử vong
                                <input type="hidden" name="quyenloi_bt_2" value="tv_tttbvv_tn_tv"/>
                            </div>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tv_tttbvv_tn_tv">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tv_tttbvv_tn_tv_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_tv_tttbvv_tn_tt_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tv_tttbvv_tn_tv_st_dbt) ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_tv_tttbvv_tn_tt == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tv_tttbvv_tn_tt' ?>" name="quyenloi_bt_2" value="tv_tttbvv_tn_tt"> 
                                Thương tật
                                <input type="hidden" name="quyenloi_bt_2" value="tv_tttbvv_tn_tt"/>
                            </div>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tv_tttbvv_tn_tt">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tv_tttbvv_tn_tt_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_tv_tttbvv_tn_tv_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tv_tttbvv_tn_tt_st_dbt) ?>
                                </div>
                            </div> 
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_TVTTTBVV_OB) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            TỬ VONG, TTTBVV do ốm bệnh
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền yêu cầu bt
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền còn lại
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Số tiền bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_tv_tttbvv_ob_tv == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tv_tttbvv_ob_tv' ?>" name="quyenloi_bt_2" value="tv_tttbvv_ob_tv"> 
                                Tử vong
                                <input type="hidden" name="quyenloi_bt_2" value="tv_tttbvv_ob_tv"/>
                            </div>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tv_tttbvv_ob_tv">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tv_tttbvv_ob_tv_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_tv_tttbvv_ob_tv_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tv_tttbvv_ob_tv_st_dbt) ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_tv_tttbvv_ob_tt == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tv_tttbvv_ob_tt' ?>" name="quyenloi_bt_2" value="tv_tttbvv_ob_tt"> 
                                Thương tật
                                <input type="hidden" name="quyenloi_bt_2" value="tv_tttbvv_ob_tt"/>
                            </div>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tv_tttbvv_ob_tt">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tv_tttbvv_ob_tt_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_tv_tttbvv_ob_tt_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tv_tttbvv_ob_tt_st_dbt) ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_TCL_NN) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Trợ cấp lương ngày nghỉ
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Giá trị yêu cầu bồi thường
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Giá trị còn lại
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Giá trị chấp nhận bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Số tiền
                        </div>
                        <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tcl_nn" style="display: _none">
                            <div class="col-xs-2 col-sm-2">
                                <?php echo get_price_in_vnd($tcl_nn_st_ycbt) ?>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <?php echo get_price_in_vnd($CL_tcl_nn_st_cl) ?>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <?php echo get_price_in_vnd($tcl_nn_st_dbt) ?>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Số ngày
                        </div>
                        <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tcl_nn_sn" style="display: _none">
                            <div class="col-xs-2 col-sm-2">
                                <?php echo ($tcl_nn_sn_ycbt) ?> ngày
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <?php echo ($CL_tcl_nn_sn_cl) ?> ngày
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <?php echo ($tcl_nn_sn_dbt) ?> ngày
                            </div>
                        </div> 

                    </div>
                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_CPYT_TN) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Chi phí y tế do tai nạn
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Giá trị yêu cầu bồi thường
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Giá trị còn lại
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Giá trị chấp nhận bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Số tiền
                        </div>
                        <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_tcl_nn" style="display: _none">
                            <div class="col-xs-2 col-sm-2">
                                <?php echo get_price_in_vnd($cpyt_tn_st_ycbt) ?>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <?php echo get_price_in_vnd($CL_cpyt_tn_st_cl) ?>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <?php echo get_price_in_vnd($cpyt_tn_st_dbt) ?>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_NHAKHOA_MDL) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Nha khoa mua độc lập
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền yêu cầu bt
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền còn lại
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Số tiền bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt col-sm-12">
                        <?php
                        if ($bt_nkmdl_cb == 1) {
                            ?>
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" name="quyenloi_bt_2" value="nkmdl_cb" checked="checked"/>
                                Nha khoa cơ bản
                            </div>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_nkmdl_cb" style="display: _none">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_cb_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_nkmdl_cb_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_cb_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="nkmdl_cb_st_dbt" value="<?php echo $nkmdl_cb_st_dbt ?>" max="<?php echo $nkmdl_cb_st_dbt ?>" min="<?php echo $nkmdl_cb_st_dbt ?>"/>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_nkmdl_db == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" name="quyenloi_bt_2" value="nkmdl_db" checked="checked"/>
                                Nha khoa đặc biệt
                            </div>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_nkmdl_db" style="display: _none">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_db_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_nkmdl_db_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_db_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="nkmdl_db_st_dbt" value="<?php echo $nkmdl_db_st_dbt ?>" max="<?php echo $nkmdl_db_st_dbt ?>" min="<?php echo $nkmdl_db_st_dbt ?>"/>
                                </div>
                            </div> 
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_nkmdl_cvr == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" name="" checked="checked" disabled="disabled" value="1" class="checkbox_nhap_qlbt" cl="<?php echo $cl_ql_bt_2 . 'nkmdl_cvr' ?>" />
                                <input type="hidden" name="nkmdl_cvr" value="1"/>
                                Cạo vôi răng
                            </div>
                            <div class="box_nhap_thongtin_bt box_nhap_thongtin_bt_nkmdl_cvr" style="display: _none">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_cvr_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_nkmdl_cvr_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($nkmdl_cvr_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="nkmdl_cvr_st_dbt" value="<?php echo $nkmdl_cvr_st_dbt ?>" max="<?php echo $nkmdl_cvr_st_dbt ?>" min="<?php echo $nkmdl_cvr_st_dbt ?>"/>
                                </div>
                            </div> 

                        </div>
                        <?php
                    }
                    ?>

                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->   
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_THAISAN_MDL) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Thai sản mua độc lập
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền yêu cầu bt
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền còn lại
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Số tiền bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_tsmdl_ktdk == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tsmdl_ktdk' ?>" name="quyenloi_bt_2" value="tsmdl_ktdk"> 
                                Khám thai định kỳ
                                <input type="hidden" name="quyenloi_bt_2" value="tsmdl_ktdk"/>
                            </div>
                            <div class="box_nhap_thongtin_bt">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsmdl_ktdk_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_tsmdl_ktdk_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsmdl_ktdk_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="tsmdl_ktdk_st_dbt" value="<?php echo $tsmdl_ktdk_st_dbt ?>" max="<?php echo $tsmdl_ktdk_st_dbt ?>" min="<?php echo $tsmdl_ktdk_st_dbt ?>"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_tsmdl_st == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" name="quyenloi_bt_2" value="tsmdl_st"> 
                                Sinh thường
                                <input type="hidden" name="quyenloi_bt_2" value="tsmdl_st"/>
                            </div>
                            <div class="box_nhap_thongtin_bt">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsmdl_st_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_tsmdl_st_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsmdl_st_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="tsmdl_st_st_dbt" value="<?php echo $tsmdl_st_st_dbt ?>" max="<?php echo $tsmdl_st_st_dbt ?>" min="<?php echo $tsmdl_st_st_dbt ?>"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_tsmdl_sm == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" name="quyenloi_bt_2" value="tsmdl_sm"> 
                                Sinh mổ
                                <input type="hidden" name="quyenloi_bt_2" value="tsmdl_sm"/>
                            </div>
                            <div class="box_nhap_thongtin_bt">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsmdl_sm_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_tsmdl_sm_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsmdl_sm_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="tsmdl_sm_st_dbt" value="<?php echo $tsmdl_sm_st_dbt ?>" max="<?php echo $tsmdl_sm_st_dbt ?>" min="<?php echo $tsmdl_sm_st_dbt ?>"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_tsmdl_dn == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" name="" checked="checked" disabled="disabled" value="1"/>
                                <input type="hidden" name="tsmdl_dn" value="1"/>
                                Dưỡng nhi
                            </div>
                            <div class="box_nhap_thongtin_bt" style="display: _none">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsmdl_dn_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_tsmdl_dn_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsmdl_dn_st_dbt) ?>
                                    <input style="display: none" type="number" name="tsmdl_dn_st_dbt" value="<?php echo $tsmdl_dn_st_dbt ?>" max="<?php echo $tsmdl_dn_st_dbt ?>" min="<?php echo $tsmdl_dn_st_dbt ?>"/>
                                </div>
                            </div> 

                        </div>
                        <?php
                    }
                    ?>
                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_DTNGT_OB) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Điều trị ngoại trú do ốm bệnh
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền yêu cầu bt
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Số tiền còn lại
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Số tiền bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtngt_ob_st1lk == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtngt_st1lk' ?>" name="quyenloi_bt_2" value="dtngt_st1lk"> 
                                Khám ngoại trú
                                <input type="hidden" name="quyenloi_bt_2" value="dtngt_st1lk"/>
                            </div>
                            <div class="box_nhap_thongtin_bt">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtngt_ob_st1lk_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtngt_st1lk) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtngt_ob_st1lk_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="dtngt_st1lk_st_dbt" value="<?php echo $dtngt_ob_st1lk_st_dbt ?>" max="<?php echo $dtngt_ob_st1lk_st_dbt ?>" min="<?php echo $dtngt_ob_st1lk_st_dbt ?>"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="clearfix"></div>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" name="quyenloi_bt_2" value="dtngt_nkngt"> 
                                Nha khoa trong ngoại trú
                                <input type="hidden" name="quyenloi_bt_2" value="dtngt_nkngt"/>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtngt_ob_nk == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" name="quyenloi_bt_3" value="dtngt_nk"> 
                                Tiền khám nha khoa
                                <input type="hidden" name="quyenloi_bt_3" value="dtngt_nk"/>
                            </div>
                            <div class="box_nhap_thongtin_bt">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtngt_ob_nk_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_dtngt_ob_nk_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtngt_ob_nk_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="dtngt_nk_st_dbt" value="<?php echo $dtngt_ob_nk_st_dbt ?>" max="<?php echo $dtngt_ob_nk_st_dbt ?>" min="<?php echo $dtngt_ob_nk_st_dbt ?>"/>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php // echo get_price_in_vnd($CL_dtngt_ob_nk_st_cl) ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtngt_ob_cvr == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" name="quyenloi_bt_3" value="dtngt_cvr"> 
                                Cạo vôi răng
                                <input type="hidden" name="quyenloi_bt_3" value="dtngt_cvr"/>
                            </div>
                            <div class="box_nhap_thongtin_bt">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtngt_ob_cvr_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($CL_dtngt_ob_cvr_st_cl) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtngt_ob_cvr_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2" name="dtngt_cvr_st_dbt" value="<?php echo $dtngt_ob_cvr_st_dbt ?>" max="<?php echo $dtngt_ob_cvr_st_dbt ?>" min="<?php echo $dtngt_ob_cvr_st_dbt ?>"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_DTNT_OB) {
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Điều trị trú do ốm bệnh
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Giá trị yêu cầu bt
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Giá trị bh còn lại
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Giá trị bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    //if ($bt_dtnt_ob_tvpn == 1) {
                    ?>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_tvpn' ?>" name="dtnt_tvpn" value="1"> 
                            Tiền viện phí/năm                            
                        </div>
                        <?php
                        if ($bt_dtnt_ob_tvpn == 1) {
                            ?>
                            <input type="hidden" name="dtnt_tvpn" value="1"/>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_tvpn_box mt10 col-sm-12">
                                <div class="col-xs-6 col-sm-6">
                                    Số ngày  <br><br>
                                    Số tiền<br>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tvpn_sn_ycbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tvpn_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_tvpn_sn_cl" disabled="disabled" value="<?php echo $CL_dtnt_ob_tvpn_sn_cl ?>" /><br><br>
                                    <input type="number" name="dtnt_tvpn_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_tvpn_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tvpn_sn_dbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tvpn_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tvpn_sn_dbt" name="dtnt_tvpn_sn_dbt" value="<?php echo $dtnt_ob_tvpn_sn_dbt ?>" max="<?php echo $dtnt_ob_tvpn_sn_bt_max ?>" min="0"/><br><br>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tvpn_st_dbt" name="dtnt_tvpn_st_dbt" value="<?php echo $dtnt_ob_tvpn_st_dbt ?>" max="<?php echo $dtnt_ob_tvpn_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    //}
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_tgpn == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_tgpn' ?>" name="" value="1"> 
                                Tiền giường,phòng/ngày
                                <input type="hidden" name="dtnt_tgpn" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_tgpn_box mt10 col-sm-12">
                                <div class="col-xs-6 col-sm-6">
                                    Số ngày  <br><br>
                                    Số tiền<br>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tgpn_sn_ycbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tgpn_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_tgpn_sn_cl" disabled="disabled" value="<?php echo $CL_dtnt_ob_tgpn_sn_cl ?>" /><br><br>
                                    <input type="number" name="dtnt_tgpn_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_tgpn_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tgpn_sn_dbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tgpn_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tgpn_sn_dbt" name="dtnt_tgpn_sn_dbt" value="<?php echo $dtnt_ob_tgpn_sn_dbt ?>" max="<?php echo $dtnt_ob_tgpn_sn_bt_max ?>" min="0"/><br><br>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tgpn_st_dbt" name="dtnt_tgpn_st_dbt" value="<?php echo $dtnt_ob_tgpn_st_dbt ?>" max="<?php echo $dtnt_ob_tgpn_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_cppt == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_cppt' ?>" name="" value="1"> 
                                Chi phí phẫu thuật
                                <input type="hidden" name="dtnt_cppt" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_cppt_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_cppt_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_cppt_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_cppt_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_cppt_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_cppt_st_dbt" name="dtnt_cppt_st_dbt" value="<?php echo $dtnt_ob_cppt_st_dbt ?>" max="<?php echo $dtnt_ob_cppt_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_tk_nv == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_tk_nv' ?>" name="" value="1"> 
                                Quyền lợi khám trước khi nhập viện
                                <input type="hidden" name="dtnt_tk_nv" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_tk_nv_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_tk_nv_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_tk_nv_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_tk_nv_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_tk_nv_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tk_nv_st_dbt" name="dtnt_tk_nv_st_dbt" value="<?php echo $dtnt_ob_tk_nv_st_dbt ?>" max="<?php echo $dtnt_ob_tk_nv_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_sk_nv == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_sk_nv' ?>" name="" value="1"> 
                                Quyền lợi khám sau khi nhập viện
                                <input type="hidden" name="dtnt_sk_nv" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_sk_nv_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_sk_nv_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_sk_nv_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_sk_nv_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_sk_nv_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_sk_nv_st_dbt" name="dtnt_sk_nv_st_dbt" value="<?php echo $dtnt_ob_sk_nv_st_dbt ?>" max="<?php echo $dtnt_ob_sk_nv_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_cpyt_tn == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_cpyt_tn' ?>" name="" value="1"> 
                                Chi phí y tế tại nhà
                                <input type="hidden" name="dtnt_cpyt_tn" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_cpyt_tn_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_cpyt_tn_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_cpyt_tn_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_cpyt_tn_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_cpyt_tn_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_cpyt_tn_st_dbt" name="dtnt_cpyt_tn_st_dbt" value="<?php echo $dtnt_ob_cpyt_tn_st_dbt ?>" max="<?php echo $dtnt_ob_cpyt_tn_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_tcvp == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_tcvp' ?>" name="" value="1"> 
                                Trợ cấp viện phí
                                <input type="hidden" name="dtnt_tcvp" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_tcvp_box mt10 col-sm-12">
                                <div class="col-xs-6 col-sm-6">
                                    Số ngày  <br><br>
                                    Số tiền<br>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tcvp_sn_ycbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tcvp_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_tcvp_sn_cl" disabled="disabled" value="<?php echo $CL_dtnt_ob_tcvp_sn_cl ?>" /><br><br>
                                    <input type="number" name="dtnt_tcvp_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_tcvp_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tcvp_sn_dbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tcvp_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tcvp_sn_dbt" name="dtnt_tcvp_sn_dbt" value="<?php echo $dtnt_ob_tcvp_sn_dbt ?>" max="<?php echo $dtnt_ob_tcvp_sn_bt_max ?>" min="0"/><br><br>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tcvp_st_dbt" name="dtnt_tcvp_st_dbt" value="<?php echo $dtnt_ob_tcvp_st_dbt ?>" max="<?php echo $dtnt_ob_tcvp_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_tcmt == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_tcmt' ?>" name="" value="1"> 
                                Trợ cấp mai táng
                                <input type="hidden" name="dtnt_tcmt" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_tcmt_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_tcmt_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_tcmt_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_tcmt_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_tcmt_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tcmt_st_dbt" name="dtnt_tcmt_st_dbt" value="<?php echo $dtnt_ob_tcmt_st_dbt ?>" max="<?php echo $dtnt_ob_tcmt_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_xct == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_xct' ?>" name="" value="1"> 
                                Chi phí y tế tại nhà
                                <input type="hidden" name="dtnt_xct" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_xct_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_xct_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_xct_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_xct_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_xct_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_xct_st_dbt" name="dtnt_xct_st_dbt" value="<?php echo $dtnt_ob_xct_st_dbt ?>" max="<?php echo $dtnt_ob_xct_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
            <div class="list_box_qlbt">
                <?php
                if ($phanloai_bt == BT_THAISAN_QLNT) {
                    // Nếu hết qlbt
                    if ($tsqlnt_st_bt_max == 0 || $CL_dtnt_ob_st_cl == 0) {
                        ?>
                        <div class="alert alert-warning">
                            <strong class="text-danger"><label class="glyphicon glyphicon-remove"></label>Đã hết quyền lợi bồi thường</strong>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="alert alert-success">
                            <strong class="text-success">
                                <label class="glyphicon glyphicon-ok"></label>
                                Số tiền bồi thường giới hạn: <span class="btn btn-success"><?php echo get_price_in_vnd($tsqlnt_st_bt_max) ?></span>
                            </strong>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="box_title_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            Thai sản trong quyền lợi nội trú
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Giá trị yêu cầu bt
                        </div>
                        <div class="col-xs-2 col-sm-2 text-danger">
                            Giá trị bh còn lại
                        </div>
                        <div class="col-xs-2 col-sm-2 text-success">
                            Giá trị bồi thường
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="list_con_qlbt _box_nhap_thongtin_bt col-sm-12">    
                        <?php
                        // Nếu là sinh thường
                        if ($phanloai_thaisan == SINH_THUONG) {
                            if ($tsqlnt_st_st_bt_max == 0) {
                                ?>
                                <div class="col-xs-6 col-sm-6">
                                    <input type="radio" name="tsqlnt_st" value="1" disabled="disabled" checked="checked" class="_checkbox_nhap_qlbt check_qlbt_tsqlnt_st" cl="<?php echo $cl_ql_bt_2 . 'tsqlnt_st' ?>" />
                                    Sinh thường                           
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-xs-6 col-sm-6">
                                    <input type="radio" name="quyenloi_bt_2" value="tsqlnt_st" disabled="disabled" checked="checked" class="_checkbox_nhap_qlbt _check_qlbt_tsqlnt check_qlbt_tsqlnt_st" cl="<?php echo $cl_ql_bt_2 . 'tsqlnt_st' ?>" />
                                    Sinh thường                  
                                </div>                                
                                <?php
                            }
                            ?>
                            <div class="box_con_nhap_thongtin_bt_dtnt box_nhap_thongtin_bt_tsqlnt_st" style="_display: none">
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="tsqlnt_st_st_ycbt" disabled="disabled" value="<?php echo $so_tien_ycbt ?>" /><br><br>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="tsqlnt_st_st_cl" disabled="disabled" value="<?php echo $CL_dtnt_ob_st_st_cl ?>" />
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        // Nếu là sinh mổ
                        if ($phanloai_thaisan == SINH_MO) {
                            if ($tsqlnt_sm_st_bt_max == 0) {
                                ?>
                                <div class="col-xs-6 col-sm-6">
                                    <input type="radio" name="tsqlnt_sm" value="1" checked="checked" disabled="disabled" class="_checkbox_nhap_qlbt check_qlbt_tsqlnt_sm" cl="<?php echo $cl_ql_bt_2 . 'tsqlnt_sm' ?>" />
                                    Sinh mổ                           
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-xs-6 col-sm-6">
                                    <input type="radio" name="quyenloi_bt_2" value="tsqlnt_sm" disabled="disabled" checked="checked" class="_checkbox_nhap_qlbt check_qlbt_tsqlnt check_qlbt_tsqlnt_sm" cl="<?php echo $cl_ql_bt_2 . 'tsqlnt_sm' ?>" />
                                    Sinh mổ                  
                                </div>                                
                                <?php
                            }
                            ?>
                            <div class="box_con_nhap_thongtin_bt_dtnt box_nhap_thongtin_bt_tsqlnt_sm" style="_display: none">
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="tsqlnt_sm_st_ycbt" disabled="disabled" value="<?php echo $so_tien_ycbt ?>" /><br><br>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="tsqlnt_sm_st_cl" disabled="disabled" value="<?php echo $CL_dtnt_ob_sm_st_cl ?>" />
                                </div> 
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="list_con_qlbt col-sm-12">
                        <div class="col-xs-6 col-sm-6">
                            <input type="radio" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_tvpn' ?>" name="dtnt_tvpn" value="1"> 
                            Tiền viện phí/năm                            
                        </div>
                        <?php
                        if ($bt_dtnt_ob_tvpn == 1) {
                            ?>
                            <input type="hidden" name="dtnt_tvpn" value="1"/>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_tvpn_box mt10 col-sm-12">
                                <div class="col-xs-6 col-sm-6">
                                    Số ngày  <br><br>
                                    Số tiền<br>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tvpn_sn_ycbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tvpn_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_tvpn_sn_cl" disabled="disabled" value="<?php echo $CL_dtnt_ob_tvpn_sn_cl ?>" /><br><br>
                                    <input type="number" name="dtnt_tvpn_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_tvpn_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tvpn_sn_dbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tvpn_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tvpn_sn_dbt" name="dtnt_tvpn_sn_dbt" value="<?php echo $dtnt_ob_tvpn_sn_dbt ?>" max="<?php echo $dtnt_ob_tvpn_sn_bt_max ?>" min="0"/><br><br>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tvpn_st_dbt" name="dtnt_tvpn_st_dbt" value="<?php echo $dtnt_ob_tvpn_st_dbt ?>" max="<?php echo $dtnt_ob_tvpn_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    //}
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_tgpn == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_tgpn' ?>" name="" value="1"> 
                                Tiền giường,phòng/ngày
                                <input type="hidden" name="dtnt_tgpn" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_tgpn_box mt10 col-sm-12">
                                <div class="col-xs-6 col-sm-6">
                                    Số ngày  <br><br>
                                    Số tiền<br>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tgpn_sn_ycbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tgpn_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_tgpn_sn_cl" disabled="disabled" value="<?php echo $CL_dtnt_ob_tgpn_sn_cl ?>" /><br><br>
                                    <input type="number" name="dtnt_tgpn_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_tgpn_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tgpn_sn_dbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tgpn_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tgpn_sn_dbt" name="dtnt_tgpn_sn_dbt" value="<?php echo $dtnt_ob_tgpn_sn_dbt ?>" max="<?php echo $dtnt_ob_tgpn_sn_bt_max ?>" min="0"/><br><br>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tgpn_st_dbt" name="dtnt_tgpn_st_dbt" value="<?php echo $dtnt_ob_tgpn_st_dbt ?>" max="<?php echo $dtnt_ob_tgpn_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_cppt == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_cppt' ?>" name="" value="1"> 
                                Chi phí phẫu thuật
                                <input type="hidden" name="dtnt_cppt" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_cppt_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_cppt_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_cppt_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_cppt_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_cppt_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_cppt_st_dbt" name="dtnt_cppt_st_dbt" value="<?php echo $dtnt_ob_cppt_st_dbt ?>" max="<?php echo $dtnt_ob_cppt_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_tk_nv == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_tk_nv' ?>" name="" value="1"> 
                                Quyền lợi khám trước khi nhập viện
                                <input type="hidden" name="dtnt_tk_nv" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_tk_nv_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_tk_nv_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_tk_nv_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_tk_nv_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_tk_nv_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tk_nv_st_dbt" name="dtnt_tk_nv_st_dbt" value="<?php echo $dtnt_ob_tk_nv_st_dbt ?>" max="<?php echo $dtnt_ob_tk_nv_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_sk_nv == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_sk_nv' ?>" name="" value="1"> 
                                Quyền lợi khám sau khi nhập viện
                                <input type="hidden" name="dtnt_sk_nv" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_sk_nv_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_sk_nv_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_sk_nv_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_sk_nv_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_sk_nv_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_sk_nv_st_dbt" name="dtnt_sk_nv_st_dbt" value="<?php echo $dtnt_ob_sk_nv_st_dbt ?>" max="<?php echo $dtnt_ob_sk_nv_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_cpyt_tn == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_cpyt_tn' ?>" name="" value="1"> 
                                Chi phí y tế tại nhà
                                <input type="hidden" name="dtnt_cpyt_tn" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_cpyt_tn_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_cpyt_tn_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_cpyt_tn_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_cpyt_tn_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_cpyt_tn_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_cpyt_tn_st_dbt" name="dtnt_cpyt_tn_st_dbt" value="<?php echo $dtnt_ob_cpyt_tn_st_dbt ?>" max="<?php echo $dtnt_ob_cpyt_tn_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_tcvp == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_tcvp' ?>" name="" value="1"> 
                                Trợ cấp viện phí
                                <input type="hidden" name="dtnt_tcvp" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_tcvp_box mt10 col-sm-12">
                                <div class="col-xs-6 col-sm-6">
                                    Số ngày  <br><br>
                                    Số tiền<br>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tcvp_sn_ycbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tcvp_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_tcvp_sn_cl" disabled="disabled" value="<?php echo $CL_dtnt_ob_tcvp_sn_cl ?>" /><br><br>
                                    <input type="number" name="dtnt_tcvp_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_tcvp_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo ($dtnt_ob_tcvp_sn_dbt) ?> ngày<br><br><br>
                                    <?php echo get_price_in_vnd($dtnt_ob_tcvp_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tcvp_sn_dbt" name="dtnt_tcvp_sn_dbt" value="<?php echo $dtnt_ob_tcvp_sn_dbt ?>" max="<?php echo $dtnt_ob_tcvp_sn_bt_max ?>" min="0"/><br><br>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tcvp_st_dbt" name="dtnt_tcvp_st_dbt" value="<?php echo $dtnt_ob_tcvp_st_dbt ?>" max="<?php echo $dtnt_ob_tcvp_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_tcmt == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_tcmt' ?>" name="" value="1"> 
                                Trợ cấp mai táng
                                <input type="hidden" name="dtnt_tcmt" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_tcmt_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_tcmt_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_tcmt_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_tcmt_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_tcmt_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_tcmt_st_dbt" name="dtnt_tcmt_st_dbt" value="<?php echo $dtnt_ob_tcmt_st_dbt ?>" max="<?php echo $dtnt_ob_tcmt_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_dtnt_ob_xct == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'dtnt_xct' ?>" name="" value="1"> 
                                Chi phí y tế tại nhà
                                <input type="hidden" name="dtnt_xct" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_dtnt_xct_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_xct_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="dtnt_xct_st_cl" disabled="disabled" value="<?php echo $dtnt_ob_xct_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($dtnt_ob_xct_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 dtnt_xct_st_dbt" name="dtnt_xct_st_dbt" value="<?php echo $dtnt_ob_xct_st_dbt ?>" max="<?php echo $dtnt_ob_xct_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_tsqlnt_ktdk == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tsqlnt_ktdk' ?>" name="" value="1"> 
                                Chi phí khám thai định kỳ
                                <input type="hidden" name="tsqlnt_ktdk" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_tsqlnt_ktdk_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsqlnt_ktdk_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="tsqlnt_ktdk_st_cl" disabled="disabled" value="<?php echo $tsqlnt_ktdk_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsqlnt_ktdk_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 tsqlnt_ktdk_st_dbt" name="tsqlnt_ktdk_st_dbt" value="<?php echo $tsqlnt_ktdk_st_dbt ?>" max="<?php echo $tsqlnt_ktdk_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    if ($bt_tsqlnt_dn == 1) {
                        ?>
                        <div class="list_con_qlbt col-sm-12">
                            <div class="col-xs-6 col-sm-6">
                                <input type="checkbox" <?php echo $disabled . ' ' . $checked ?> class="_quyenloi_bt_2" cl="<?php echo $cl_ql_bt_2 . 'tsqlnt_dn' ?>" name="" value="1"> 
                                Dưỡng nhi
                                <input type="hidden" name="tsqlnt_dn" value="1"/>
                            </div>
                            <div class="box_nhap_thongtin_bt_dtnt _box_nhap_thongtin_bt box_nhap_thongtin_bt_tsqlnt_dn_box mt10">
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsqlnt_dn_st_ycbt) ?>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <input type="number" name="tsqlnt_dn_st_cl" disabled="disabled" value="<?php echo $tsqlnt_dn_st_bt_max ?>" />
                                </div>                                
                                <div class="col-xs-2 col-sm-2">
                                    <?php echo get_price_in_vnd($tsqlnt_dn_st_dbt) ?>
                                    <input style="display: none" type="number" class="st_dbt_2 tsqlnt_dn_st_dbt" name="tsqlnt_dn_st_dbt" value="<?php echo $tsqlnt_dn_st_dbt ?>" max="<?php echo $tsqlnt_dn_st_bt_max ?>" min="0"/>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                }
                ?>
            </div>
            <!--=============================================================-->
        </div>
    </div>
    <?php
}
?>