<?php

class Orders_Admin extends MY_Controller {

    /**
     * Chuan bi cac bien co ban
     */
    function __construct() {
        parent::__construct();
        modules::run('auth/auth/validate_login', $this->router->fetch_module());
        // Khoi tao cac bien
        $this->_layout = 'admin_ui/layout/main';
        // Chuan bi link cho viec phan trang
        $this->_view_data['url'] = ORDER_ADMIN_BASE_URL;
        $this->output->enable_profiler(TRUE);
        $this->load->model('products/products_coupon_model');
    }

    //end
    function browse($para1 = DEFAULT_LANGUAGE, $para2 = 1) {

        //$options = array('lang' => switch_language($para1), 'page' => $para2);
        //$options = array_merge($options, $this->_get_data_from_filter());
        //$this->phpsession->save('orders_lang', $options['lang']);
        // join tbl customers
        $options = array(
            'join_' . TBL_CUSTOMERS => TRUE,
        );
        //end
        $total_row = $this->orders_model->count_orders_by_options($options);

        $total_pages = (int) ($total_row / FAQ_ADMIN_POST_PER_PAGE);
        if ($total_pages * FAQ_ADMIN_POST_PER_PAGE < $total_row)
            $total_pages++;
        if ((int) $options['page'] > $total_pages)
            $options['page'] = $total_pages;

        $options['offset'] = $options['page'] <= DEFAULT_PAGE ? DEFAULT_OFFSET : ((int) $options['page'] - 1) * FAQ_ADMIN_POST_PER_PAGE;
        $options['limit'] = FAQ_ADMIN_POST_PER_PAGE;

        $config = prepare_pagination(
                array(
                    'total_rows' => $total_row,
                    'per_page' => $options['limit'],
                    'offset' => $options['offset'],
                    'js_function' => 'change_page_admin'
                )
        );
        $this->pagination->initialize($config);

        $options['bt'] = $this->orders_model->get_orders_by_options($options);

        $options['post_uri'] = 'orders_admin';
        $options['total_rows'] = $total_row;
        $options['total_pages'] = $total_pages;
        $options['page_links'] = $this->pagination->create_ajax_links();

        $options['uri'] = ORDER_ADMIN_BASE_URL;

        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning']))
            $options['options'] = $options;

        //$options['combo_order'] = $this->utility_model->get_order_status_combo(array('combo_name' => 'order_status', 'order_status' => $options['order_status'], 'extra' => 'class="btn"'));

        $tpl = 'ad/ad_orders_list';

        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Quản lý bồi thường' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    /**
     * @desc: Hien thi danh sach cac bai viet
     *
     * @param type $options
     */
    function _browse($para1 = DEFAULT_LANGUAGE, $para2 = 1) {

        $options = array('lang' => switch_language($para1), 'page' => $para2);
        $options = array_merge($options, $this->_get_data_from_filter());

        $this->phpsession->save('orders_lang', $options['lang']);

        $total_row = $this->orders_model->get_orders_count($options);

        $total_pages = (int) ($total_row / FAQ_ADMIN_POST_PER_PAGE);
        if ($total_pages * FAQ_ADMIN_POST_PER_PAGE < $total_row)
            $total_pages++;
        if ((int) $options['page'] > $total_pages)
            $options['page'] = $total_pages;

        $options['offset'] = $options['page'] <= DEFAULT_PAGE ? DEFAULT_OFFSET : ((int) $options['page'] - 1) * FAQ_ADMIN_POST_PER_PAGE;
        $options['limit'] = FAQ_ADMIN_POST_PER_PAGE;

        $config = prepare_pagination(
                array(
                    'total_rows' => $total_row,
                    'per_page' => $options['limit'],
                    'offset' => $options['offset'],
                    'js_function' => 'change_page_admin'
                )
        );
        $this->pagination->initialize($config);

        $options['orderss'] = $this->orders_model->get_orders($options);

        $options['post_uri'] = 'orders_admin';
        $options['total_rows'] = $total_row;
        $options['total_pages'] = $total_pages;
        $options['page_links'] = $this->pagination->create_ajax_links();

        if ($options['lang'] <> DEFAULT_LANGUAGE) {
            $options['uri'] = ORDER_ADMIN_BASE_URL . '/' . $options['lang'];
        } else {
            $options['uri'] = ORDER_ADMIN_BASE_URL;
        }

        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning']))
            $options['options'] = $options;

        $options['combo_order'] = $this->utility_model->get_order_status_combo(array('combo_name' => 'order_status', 'order_status' => $options['order_status'], 'extra' => 'class="btn"'));

        $this->_view_data['main_content'] = $this->load->view('admin/orders_list_1', $options, TRUE);
        $this->_view_data['title'] = 'Quản lý bồi thường' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    /**
     * Lấy dữ liệu từ filter
     * @return string
     */
    private function _get_data_from_filter() {
        $options = array();

        if ($this->is_postback()) {
            $options['search'] = $this->db->escape_str($this->input->post('search', TRUE));
            $options['email'] = $this->db->escape_str($this->input->post('email', TRUE));
            $options['order_status'] = $this->input->post('order_status');
            $options['start_date'] = $this->input->post('start_date');
            $options['end_date'] = $this->input->post('end_date');
            if (isset($options['start_date']) && $options['start_date'] != '') {
                $options['start_date_m'] = strtotime($options['start_date']);
            }
            if (isset($options['end_date']) && $options['end_date'] != '') {
                $options['end_date_m'] = strtotime($options['end_date']);
            }
            //search with lang
            $options['lang'] = $this->input->post('lang');
            $this->phpsession->save('orders_search_options', $options);
        } else {
            $temp_options = $this->phpsession->get('orders_search_options');
            if (is_array($temp_options)) {
                $options['search'] = $temp_options['search'];
                $options['order_status'] = $temp_options['order_status'];
                $options['start_date'] = $temp_options['start_date'];
                $options['end_date'] = $temp_options['end_date'];
            } else {
                $options['search'] = '';
                $options['order_status'] = DEFAULT_COMBO_VALUE;
                $options['start_date'] = '';
                $options['end_date'] = '';
                $options['email'] = $this->db->escape_str($this->input->get('email', TRUE));
            }
        }
//        $options['offset'] = $this->uri->segment(3);
        return $options;
    }

//    private function _get_posted_orders_data()
//    {
//        $post_data = array(
//            'order_status' => my_trim($this->input->post('order_status', TRUE)),
//        );
//        return $post_data;
//    }

    function add() {
        $options = array();

        if ($this->is_postback()) {
            if (!$this->_do_add_orders())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        //$options += $this->_get_add_orders_form_data();
        // Chuan bi du lieu chinh de hien thi
        // Lấy kh được BH
        $data_get = array();
        $kh_dbh = $this->customers_model->get_canhan_dbh();
        if (!empty($kh_dbh)) {
            $options['kh_dbh'] = $kh_dbh;
        }
        //end
        $options['scripts'] = $this->scripts_select2();
        $options['scripts1'] = $this->_get_scripts();
        //end
        $options['header'] = 'Thêm bồi thường';
        $options['button_name'] = 'Lưu dữ liệu';
        $this->_view_data['main_content'] = $this->load->view('admin/add_orders_form', $options, TRUE);
        // Chuan bi dữ liệu        
        $options['submit_uri'] = ORDERS_ADMIN_BASE_URL . '/add';
        $this->_view_data['title'] = 'Thêm bồi thường' . DEFAULT_TITLE_SUFFIX;

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    private function _get_add_orders_form_data() {
        $options = array();
        $options['cmt_custom_duoc'] = my_trim($this->input->post('cmt_custom_duoc'));
        $options['price_bt'] = my_trim($this->input->post('price_bt'));
        $options['day_bt'] = $this->input->post('day_bt');
        $options['message'] = $this->input->post('message');
        $options['loai_benh'] = $this->input->post('loai_benh');

        if ($this->is_postback()) {
            $created_date = datetimepicker_array2($this->input->post('created_date', TRUE));
            $options['created_date'] = date('d-m-Y H:i', mktime($created_date['hour'], $created_date['minute'], $created_date['second'], $created_date['month'], $created_date['day'], $created_date['year']));
            $options['lang_combobox'] = $this->utility_model->get_lang_combo(array('lang' => $this->input->post('lang', TRUE), 'extra' => 'onchange="javascript:get_categories_by_lang();" class="btn"'));
//            $options['categories_combobox']   = $this->news_categories_model->get_news_categories_combo(array('categories_combobox' => $this->input->post('categories_combobox')
//                                                                                                        , 'lang'                => $this->input->post('lang', TRUE)
//                                                                                                        , 'extra' => 'class="btn"'
//                                                                                                        ));
        } else {
            $options['created_date'] = date('d-m-Y H:i');
            $options['lang_combobox'] = $this->utility_model->get_lang_combo(array('lang' => $this->phpsession->get('news_lang'), 'extra' => 'onchange="javascript:get_categories_by_lang();" class="btn"'));
        }

        $options['scripts'] = $this->_get_scripts();
        $options['header'] = 'Thêm bồi thường';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = ORDERS_ADMIN_BASE_URL . '/add';

        return $options;
    }

    private function _do_add_orders() {
        $data = array();
        $this->form_validation->set_rules('kh_dbh', 'CMT/Tên của người được bảo hiểm', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('phanloai_bt', 'Phân loại bồi thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('ngay_ycbt', 'Ngày yêu cầu bồi thường', 'trim|required|xss_clean|max_length[255]');

        $phanloai_bt = $this->input->post('phanloai_bt', TRUE);
        // Lấy chứng từ
        $data_get = array(
            'parent_id' => $phanloai_bt,
        );
        $ct = $this->products_coupon_model->get_products_coupon($data_get);
        if (!empty($ct)) {
            $ct_num = count($ct);
            //end
            $ct_bt = $this->input->post('ct_bt', TRUE);
//            echo $ct_num.'/'. count($ct_bt);
//            die;
            // Nếu đủ chứng từ
            if ($ct_num == count($ct_bt)) {
                $this->form_validation->set_rules('so_tien_ycbt', 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean|max_length[255]');
                $this->form_validation->set_rules('loai_benh', 'Quyền lợi bồi thường', 'trim|required|xss_clean|max_length[255]');

                $data['order_status'] = BT_CHODUYET;
                $data['loai_benh'] = $this->input->post('loai_benh');
                $data['so_tien_ycbt'] = $this->input->post('so_tien_ycbt');
                $data['so_ngay_ycbt'] = $this->input->post('so_ngay_ycbt');
                $data['so_lan_kham'] = $this->input->post('so_lan_kham');
                $data['message'] = $this->input->post('message');
            } else {
                $this->form_validation->set_rules('chungtu_bosung_txt', 'Chứng từ chờ bổ sung', 'trim|required|xss_clean');
                if (!empty($ct_bt)) {
                    $str_ct_bt_id = '';
                    foreach ($ct_bt as $k => $v) {
                        $str_ct_bt_id .= $v . ',';
                    }
                    $data['chungtu_bosung'] = substr($str_ct_bt_id, 0, -1);
                    $data['chungtu_bosung_txt'] = $this->input->post('chungtu_bosung_txt', TRUE);
                    $data['order_status'] = BT_CHOBOSUNG;
                }
            }
        }

        if ($this->form_validation->run()) {

            $post_data = array(
                //'price_cnbt' => my_trim($this->input->post('price_cnbt')),
                //'message' => $this->input->post('message'),
                'phanloai_bt' => $phanloai_bt,
                //'loai_benh' => $this->input->post('loai_benh'),
                'OD_kh_dbh' => $this->input->post('kh_dbh'),
                //'so_tien_ycbt' => $this->input->post('so_tien_ycbt'),
                //'so_ngay_ycbt' => $this->input->post('so_ngay_ycbt'),
                'editor' => $this->phpsession->get('user_id'),
            );

            $post_data = array_merge($post_data, $data);

            $post_data['updated_date'] = (date('Y-m-d H:i:s'));
            $post_data['updated_time'] = strtotime(date('Y-m-d'));
            $ngay_yc_bt = datetimepicker_array2($this->input->post('ngay_ycbt', TRUE));
            $post_data['ngay_ycbt'] = (date('Y-m-d H:i:s', mktime($ngay_yc_bt['hour'], $ngay_yc_bt['minute'], $ngay_yc_bt['second'], $ngay_yc_bt['month'], $ngay_yc_bt['day'], $ngay_yc_bt['year'])));

            $bt_id = $this->input->post('bt_id');

            // Khi sửa
            if ($bt_id != '' && $bt_id > 0) {
                $post_data['id'] = $bt_id;
                // Update data
                $this->orders_model->update($post_data);
                redirect(ORDERS_ADMIN_BASE_URL);
            } else {
                $post_data['creator'] = $this->phpsession->get('user_id');
                $post_data['created_date'] = (date('Y-m-d H:i:s'));
                $post_data['created_time'] = strtotime(date('Y-m-d'));

                // Thêm data
                $insert_id = $this->orders_model->insert($post_data);
                redirect(ORDERS_ADMIN_BASE_URL);
            }
            //redirect(ORDERS_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    function _get_posted_orders_data() {
//        $content = str_replace('&lt;', '<', $this->input->post('content'));
//        $content = str_replace('&gt;', '>', $content);
        $post_data = array(
            //'cmt_custom_duoc' => my_trim($this->input->post('cmt_custom_duoc')),
            //'fullname' => my_trim($this->input->post('fullname')),
            //'total' => my_trim($this->input->post('price_bt')),
            //'day_bt' => $this->input->post('day_bt'),
            'price_cnbt' => my_trim($this->input->post('price_cnbt')),
            'message' => $this->input->post('message'),
            'loai_benh' => $this->input->post('loai_benh'),
            'order_status' => STATUS_ACTIVE,
            //end
            'OD_kh_dbh' => $this->input->post('kh_dbh'),
            'so_tien_ycbt' => $this->input->post('so_tien_ycbt'),
        );

        $created_date = datetimepicker_array2($this->input->post('created_date', TRUE));
        $post_data['create_time'] = strtotime(date('Y-m-d H:i:s', mktime($created_date['hour'], $created_date['minute'], $created_date['second'], $created_date['month'], $created_date['day'], $created_date['year'])));
        //
        $ngay_yc_bt = datetimepicker_array2($this->input->post('ngay_yc_bt', TRUE));
        $post_data['ngay_ycbt'] = strtotime(date('Y-m-d H:i:s', mktime($ngay_yc_bt['hour'], $ngay_yc_bt['minute'], $ngay_yc_bt['second'], $ngay_yc_bt['month'], $ngay_yc_bt['day'], $ngay_yc_bt['year'])));
        //
        $post_data['update_time'] = strtotime(date('Y-m-d H:i:s'));
        $post_data['editor'] = $this->phpsession->get('user_id');

        // Xử lý data
        $bt_id = $this->input->post('id');
        if ($bt_id > 0 && $bt_id != '') {
            // Khi sửa
            $post_data['id'] = $bt_id;
            $this->orders_model->update($post_data);
        } else {
            // Khi thêm
            $insert_id = $this->orders_model->insert($post_data);
        }
        redirect(ORDERS_ADMIN_BASE_URL);
        //return $post_data;
    }

    // Sửa bồi thường
    function edit($id = 0) {
        $options = $data = array();
        $bt_id = $this->input->post('id');
        $bt_id = ($bt_id != '') ? $bt_id : $id;

        if ($this->is_postback() && !$this->input->post('from_list')) {
            if (!$this->do_edit_orders($bt_id)) {
                $data['error'] = validation_errors();
            }
            if (isset($data['error'])) {
                $data['options'] = $data;
            }
        }

        if ($id > 0 || ($bt_id != '' && $bt_id > 0)) {
            // Lấy thông tin bt
            $data_get = array(
                'BT_id' => $bt_id,
                'get_one' => TRUE,
            );
            $bt = $this->orders_model->get_orders_by_options($data_get);
            if (is_object($bt)) {
                $data['bt'] = $bt;
                $kh_dbh = $bt->OD_kh_dbh;
                $status = $bt->order_status;
                $phanloai_bt = $bt->phanloai_bt;
                $loai_benh = $bt->loai_benh;

                // Lấy thông tin bh
                $data_get = array(
                    'kh_dbh' => $kh_dbh,
                    'join_' . TBL_PRODUCTS_SIZE => TRUE,
                    'join_' . TBL_CUSTOMERS . '_dbh' => TRUE,
                    'get_one' => TRUE,
                );
                $bh = $this->customers_model->get_bh($data_get);

                if (is_object($bh)) {
                    $data['bh'] = $bh;
                    // Nếu đã chọn ql bh
                    if ($loai_benh != '') {
                        $st_bh_bt = $bh->$loai_benh;
                        $data['st_bh_bt'] = $st_bh_bt;
                    }
                    //end
                    if ($status != BT_CHOBOSUNG) {
                        // Lấy html thông tin gói bh
                        $bh_html = $this->load->view('ad/ad_ajax_bh', $data, TRUE);
                        $data['bh_html'] = $bh_html;
                    }
                }
                //end
                if ($status == BT_CHOBOSUNG) {
                    // Lấy chứng từ theo loại bt
                    $data_get = array(
                        'parent_id' => $phanloai_bt,
                    );
                    $ct = $this->products_coupon_model->get_products_coupon($data_get);
                    if (!empty($ct)) {
                        $data['ct'] = $ct;
                    }
                } else {
                    // Lấy thông tin còn lại bt
                    $data_get = array(
                        'CL_kh_id' => $kh_dbh,
                        'CL_loai_benh' => $loai_benh,
                        'get_row' => TRUE,
                    );
                    $ql_cl = $this->orders_model->get_ql_cl($data_get);

                    if (is_object($ql_cl)) {
                        $data['ql_cl'] = $ql_cl;
                        $ql_cl_id = $ql_cl->id;
                        $st_dbt = $ql_cl->st_dbt;
                        $sn_dbt = $ql_cl->sn_dbt;
                        $slk_dbt = $ql_cl->slk_dbt;
                    } else {
                        $sn_dbt = $slk_dbt = $st_dbt = 0;
                    }
                    $data['st_dbt'] = $st_dbt;
                    $data['slk_dbt'] = $slk_dbt;
                    $data['sn_dbt'] = $sn_dbt;
                    // end 
                }
            }
        }

        // Chuan bi du lieu chinh de hien thi
        $data['scripts'] = $this->scripts_select2();
        $data['scripts1'] = $this->_get_scripts();
        //end
        $options['header'] = 'Thêm bồi thường';
        $options['button_name'] = 'Lưu dữ liệu';
        $tpl = 'ad/ad_bt_form_edit';
        $data['header'] = 'Sửa bồi thường';
        $data['button_name'] = 'Sửa bồi thường';
        $data['submit_uri'] = ORDER_ADMIN_BASE_URL . '/edit';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        //$this->_view_data['main_content'] = $this->load->view('admin/edit_orders_form', $data, TRUE);
        // Chuan bi cac the META
        $this->_view_data['title'] = 'Xem thông tin bồi thường' . DEFAULT_TITLE_SUFFIX;

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }

    function do_edit_orders($bt_id = 0) {
        $data = $data_bt_cl = array();
        //$this->form_validation->set_rules('kh_dbh', 'CMT/Tên của người được bảo hiểm', 'trim|required|xss_clean|max_length[255]');
        //$this->form_validation->set_rules('phanloai_bt', 'Phân loại bồi thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('ngay_ycbt', 'Ngày yêu cầu bồi thường', 'trim|required|xss_clean|max_length[255]');

        $phanloai_bt = $this->input->post('phanloai_bt', TRUE);
        $kh_dbh = $this->input->post('kh_dbh', TRUE);
        $order_status = $this->input->post('order_status');

        // Lấy chứng từ
        $data_get = array(
            'parent_id' => $phanloai_bt,
        );
        $ct = $this->products_coupon_model->get_products_coupon($data_get);
        if (!empty($ct)) {
            $ct_num = count($ct);
            //end
            $ct_bt = $this->input->post('ct_bt', TRUE);
//            echo $ct_num.'/'. count($ct_bt);
//            die;
            // Nếu đủ chứng từ
            if ($ct_num == count($ct_bt) || $order_status == BT_THANHTOAN) {
//                echo 1;
//                die;
                $this->form_validation->set_rules('so_tien_ycbt', 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean|max_length[255]');
                $this->form_validation->set_rules('loai_benh', 'Quyền lợi bồi thường', 'trim|required|xss_clean|max_length[255]');

                $loai_benh = $this->input->post('loai_benh');
                $so_ngay_ycbt = $this->input->post('so_ngay_ycbt');
                $so_lan_kham = $this->input->post('so_lan_kham');
                $so_tien_ycbt = $this->input->post('so_tien_ycbt');
                // Check số tiền ycbt
                if ($so_tien_ycbt == '' || $so_tien_ycbt <= 0) {
                    $this->phpsession->flashsave('msg', 'Chưa chọn Số tiền yêu cầu bồi thường');
                    redirect(ORDERS_ADMIN_EDIT_URL . '/' . $bt_id);
                }
                //end
                // Check trạng trái
                if ($order_status == BT_CHOBOSUNG) {
                    $this->phpsession->flashsave('msg', 'Chưa thay đổi trạng thái bồi thường');
                    redirect(ORDERS_ADMIN_EDIT_URL . '/' . $bt_id);
                }
                //end
                // Nếu không chọn loại bệnh
                if ($loai_benh == '') {
                    $this->phpsession->flashsave('msg', 'Chưa chọn quyền lợi bồi thường');
                    redirect(ORDERS_ADMIN_EDIT_URL . '/' . $bt_id);
                }
                // end
                $a_loai_benh_sn = get_a_loai_benh_sn();
                $a_loai_benh_slk = get_a_loai_benh_slk();
                // Nếu bt có số ngày
                if (in_array($loai_benh, $a_loai_benh_sn)) {
                    $this->form_validation->set_rules('so_ngay_ycbt', 'Số ngày yêu cầu bồi thường', 'trim|required|xss_clean|max_length[255]');
                    if ($so_ngay_ycbt == '' || $so_ngay_ycbt <= 0) {
                        $this->phpsession->flashsave('msg', 'Chưa chọn Số ngày yêu cầu bồi thường');
                        redirect(ORDERS_ADMIN_EDIT_URL . '/' . $bt_id);
                    }
                }
                // Nếu bt có số lần khám
                if (in_array($loai_benh, $a_loai_benh_slk)) {
                    $this->form_validation->set_rules('so_lan_kham', 'Số lần khám', 'trim|required|xss_clean|max_length[255]');
                    if ($so_lan_kham == '' || $so_lan_kham <= 0) {
                        $this->phpsession->flashsave('msg', 'Chưa chọn Số lần khám');
                        redirect(ORDERS_ADMIN_EDIT_URL . '/' . $bt_id);
                    }
                }
                //end
                // Lấy thông tin gói bh
                $goi_bh_id = $this->input->post('goi_bh', TRUE);
                if ($goi_bh_id > 0) {
                    $data_get = array(
                        'id' => $goi_bh_id,
                    );
                    $this->load->model('products/products_size_model');
                    $goi_bh = $this->products_size_model->get_products_size($data_get);

                    if (is_object($goi_bh)) {

                        $st_bh = $goi_bh->$loai_benh;
                        // Nếu đã chọn ql bt
                        if ($loai_benh != '') {
                            // Lấy thông tin còn lại bt
                            $data_get = array(
                                'CL_kh_id' => $kh_dbh,
                                'CL_loai_benh' => $loai_benh,
                                'get_row' => TRUE,
                            );
                            $ql_cl = $this->orders_model->get_ql_cl($data_get);
                            if (is_object($ql_cl)) {
                                $data['ql_cl'] = $ql_cl;
                                $ql_cl_id = $ql_cl->id;
                                $st_dbt = $ql_cl->st_dbt;
                                $sn_dbt = $ql_cl->sn_dbt;
                                $slk_dbt = $ql_cl->slk_dbt;
                            } else {
                                $sn_dbt = $slk_dbt = $st_dbt = 0;
                            }
                            // end                        
                            // DOING
                            // Nếu bt có số ngày
                            if (in_array($loai_benh, $a_loai_benh_sn)) {
                                $this->form_validation->set_rules('so_ngay_ycbt', 'Số ngày yêu cầu bồi thường', 'trim|required|xss_clean|max_length[255]');

                                $sn_bh_field = get_loai_benh_sn_field($loai_benh);
                                if ($sn_bh_field != '') {
                                    $sn_bh = $goi_bh->$sn_bh_field;
                                    $sn_bh_cl = (int) $sn_bh - (int) $sn_dbt;
                                    // Nếu vượt quá số ngày
                                    //echo $so_ngay_ycbt.'/'.$sn_bh_cl; die;
                                    if ((int) $so_ngay_ycbt > $sn_bh_cl) {
                                        $this->form_validation->set_message('so_ngay_ycbt', 'Số ngày yêu cầu bồi thường vượt quá giới hạn');
                                        $this->phpsession->flashsave('msg', 'Số ngày yêu cầu bồi thường vượt quá giới hạn');
                                        //$this->form_validation->set_rules('so_ngay_ycbt', 'Số ngày yêu cầu bồi thường vượt quá giới hạn', 'trim|required|xss_clean|max_length[255]');
                                        //return FALSE;
                                        redirect(ORDERS_ADMIN_EDIT_URL . '/' . $bt_id);
                                    } else {
                                        $data_bt_cl['sn_dbt'] = (int) $sn_dbt + (int) $so_ngay_ycbt;
                                    }
                                }
                            }
                            //end
                            // Nếu bt có số lần khám
                            if (in_array($loai_benh, $a_loai_benh_slk)) {
                                $this->form_validation->set_rules('so_lan_kham', 'Số lần khám', 'trim|required|xss_clean|max_length[255]');

                                $slk_bh_field = get_loai_benh_slk_field($loai_benh);
                                if ($slk_bh_field != '') {
                                    $slk_bh = $goi_bh->$slk_bh_field;
                                    $slk_bh_cl = (int) $slk_bh - (int) $slk_dbt;

                                    // Nếu vượt quá số lần khám
                                    if ((int) $so_lan_kham > $slk_bh_cl) {
                                        $this->form_validation->set_message('so_lan_kham', 'Số lần khám yêu cầu bồi thường vượt quá giới hạn');
                                        $this->phpsession->flashsave('msg', 'Số lần khám yêu cầu bồi thường vượt quá giới hạn');
                                        //return FALSE;
                                        redirect(ORDERS_ADMIN_EDIT_URL . '/' . $bt_id);
                                    } else {
                                        $data_bt_cl['slk_dbt'] = (int) $slk_dbt + (int) $so_lan_kham;
                                    }
                                }
                            }
                            //end
                        }
                        //end
                        // Check số tiền ycbt
                        $st_bh_cl = (int) $st_bh - (int) $st_dbt;
                        // Nếu vượt quá số tiền đbh
                        if ($so_tien_ycbt > $st_bh_cl) {
                            $this->form_validation->set_message('so_tien_ycbt', 'Số tiền yêu cầu bồi thường vượt quá giới hạn');
                            $this->phpsession->flashsave('msg', 'Số tiền yêu cầu bồi thường vượt quá giới hạn');
                            //return FALSE;
                            redirect(ORDERS_ADMIN_EDIT_URL . '/' . $bt_id);
                        } else {
                            $data_bt_cl['st_dbt'] = (int) $st_dbt + (int) $so_tien_ycbt;
                        }
                        // end
                    }
                }
                // end
                $data['loai_benh'] = $this->input->post('loai_benh');
                $data['so_tien_ycbt'] = $this->input->post('so_tien_ycbt');
                $data['so_ngay_ycbt'] = $this->input->post('so_ngay_ycbt');
                $data['so_lan_kham'] = $this->input->post('so_lan_kham');
                $data['message'] = $this->input->post('message');
                $data['chungtu_bosung'] = '';

                // Check trạng thái bt                
                // Nếu thanh toán
                if ($order_status == BT_THANHTOAN) {
                    $so_tien_ycbt = $this->input->post('so_tien_ycbt');
                    $so_ngay_ycbt = $this->input->post('so_ngay_ycbt');
                    $so_lan_kham = $this->input->post('so_lan_kham');
                    $loai_benh = $this->input->post('loai_benh');
                    // Thêm data còn lại
                    $data_insert = array(
                        //'st_dbt' => $so_tien_ycbt,
                        //'sn_dbt' => $so_ngay_ycbt,
                        //'slk' => $so_lan_kham,
                        'CL_kh_id' => $kh_dbh,
                        'CL_loai_benh' => $loai_benh,
                        'date_created' => date('Y-m-d'),
                        'date_updated' => date('Y-m-d'),
                        'time_created' => strtotime(date('Y-m-d')),
                        'time_updated' => strtotime(date('Y-m-d')),
                    );
                    $data_insert = array_merge($data_insert, $data_bt_cl);
                    echo '<pre>';
                    print_r($data_insert);
                    die;
                    // Update data
                    if (isset($ql_cl_id) && $ql_cl_id > 0) {
                        $this->orders_model->CommonUpdate(TBL_CL, $ql_cl_id, $data_insert);
                    } else {
                        // Creat data
                        $this->orders_model->CommonCreat(TBL_CL, $data_insert);
                    }
                    //end
                }

                $data['order_status'] = $order_status;
                //$data['chungtu_bosung_txt'] = $this->input->post('chungtu_bosung_txt', TRUE);
            } else {
                $this->form_validation->set_rules('chungtu_bosung_txt', 'Chứng từ chờ bổ sung', 'trim|required|xss_clean');
                if (!empty($ct_bt)) {
                    $str_ct_bt_id = '';
                    foreach ($ct_bt as $k => $v) {
                        $str_ct_bt_id .= $v . ',';
                    }
                    $data['chungtu_bosung'] = substr($str_ct_bt_id, 0, -1);
                    //$data['chungtu_bosung_txt'] = $this->input->post('chungtu_bosung_txt', TRUE);
                    $data['order_status'] = BT_CHOBOSUNG;
                }
            }
        }
        // end
        if ($this->form_validation->run($this)) {

            $post_data = array(
                //'price_cnbt' => my_trim($this->input->post('price_cnbt')),
                'chungtu_bosung_txt' => $this->input->post('chungtu_bosung_txt', TRUE),
                'phanloai_bt' => $phanloai_bt,
                //'loai_benh' => $this->input->post('loai_benh'),
                //'OD_kh_dbh' => $this->input->post('kh_dbh'),
                //'so_tien_ycbt' => $this->input->post('so_tien_ycbt'),
                //'so_ngay_ycbt' => $this->input->post('so_ngay_ycbt'),
                'editor' => $this->phpsession->get('user_id'),
            );

            $post_data = array_merge($post_data, $data);

            $post_data['updated_date'] = (date('Y-m-d H:i:s'));
            $post_data['updated_time'] = strtotime(date('Y-m-d'));
            $ngay_yc_bt = datetimepicker_array2($this->input->post('ngay_ycbt', TRUE));
            $post_data['ngay_ycbt'] = (date('Y-m-d H:i:s', mktime($ngay_yc_bt['hour'], $ngay_yc_bt['minute'], $ngay_yc_bt['second'], $ngay_yc_bt['month'], $ngay_yc_bt['day'], $ngay_yc_bt['year'])));

            echo '<pre>';
            print_r($post_data);
            die;

            $bt_id = $this->input->post('bt_id');
            // Khi sửa
            if ($bt_id != '' && $bt_id > 0) {
                $post_data['id'] = $bt_id;

                // Update data
                $this->orders_model->update($post_data);
                redirect(ORDERS_ADMIN_BASE_URL);
            }
        }
        return FALSE;
    }

    /**
     * Chuẩn bị dữ liệu cho form sửa
     * @return type
     */
    private function _get_edit_form_data() {
        $id = $this->input->post('id');

        // khi vừa vào trang sửa
        if ($this->input->post('from_list')) {
            $orders = $this->orders_model->get_orders(array('id' => $id, 'one_cod' => true));
//            $orders_detail  = $this->orders_details_model->get_orders_details(array('id' => $id));
            $id = $orders->id;
            $cmt_custom_duoc = $orders->cmt_custom_duoc;
            $fullname = $orders->fullname;
            $message = $orders->message;
            $order_status = $orders->order_status;
            $price_bt = $orders->total;
            $day_bt = $orders->day_bt;
            $loai_benh = $orders->loai_benh;
            $price_cnbt = $orders->price_cnbt;
            $created_date = date('m/d/Y', $orders->create_time);
            $reserve_time = $orders->reserve_time;
            $lang = $orders->lang;
            $combo_order = $this->utility_model->get_order_status_combo(array('combo_name' => 'order_status', 'order_status' => $orders->order_status, 'extra' => 'class="btn"'));
        }
        // khi submit
        else {
            $id = $id;
            $cmt_custom_duoc = my_trim($this->input->post('cmt_custom_duoc', TRUE));
            $order_status = my_trim($this->input->post('order_status', TRUE));
            $price_bt = my_trim($this->input->post('price_bt', TRUE));
            $message = my_trim($this->input->post('message', TRUE));
            $day_bt = $this->input->post('day_bt', TRUE);
            $fullname = my_trim($this->input->post('fullname', TRUE));
            $loai_benh = my_trim($this->input->post('loai_benh', TRUE));
            $created_date = my_trim($this->input->post('create_time', TRUE));
            $reserve_time = $this->input->post('reserve_time');
            $price_cnbt = $this->input->post('price_cnbt');
            $combo_order = $this->utility_model->get_order_status_combo(array('combo_name' => 'order_status', 'order_status' => $this->input->post('order_status'), 'extra' => 'class="btn"'));
            $lang = $this->input->post('lang', TRUE);
        }
        $options = array();
        $options['id'] = $id;
        $options['cmt_custom_duoc'] = $cmt_custom_duoc;


        $option = array();
        $option['onehit'] = true;
        $option['cmt_custom'] = $cmt_custom_duoc;
        $option['loai_benh'] = $loai_benh;
        $customers = $this->products_model->get_products($option);
        $options['gh_tv_tn'] = $customers->tu_vong_tai_nan;

        $gia_tri_cl = $this->products_model->get_bh_con_lai($option);

        if (!empty($gia_tri_cl)) {
            $options['check_dbh'] = 1;
            $options['stbh_cl'] = $gia_tri_cl->stbh_cl;
            $options['st_dbt'] = $gia_tri_cl->st_dbt;
            $options['thong_qua'] = $options['stbh_cl'] - $price_bt;
        } else {
            $options['check_dbh'] = 0;
            $options['stbh_cl'] = $options['gh_tv_tn'];
            $options['st_dbt'] = '0 lần';
            $options['thong_qua'] = $options['stbh_cl'] - $price_bt;
        }


        if ($loai_benh == 'gh_tv_tn') {
            $options['st_gh_bh'] = $options['gh_tv_tn'];
        }


        $options['price_cnbt'] = $price_cnbt;
        $options['order_status'] = $order_status;
        $options['kind_pay'] = $kind_pay;
        $options['message'] = $message;
        $options['reserve_time'] = $reserve_time;
        $options['price_bt'] = $price_bt;
        $options['fullname'] = $fullname;
        $options['day_bt'] = $day_bt;
        $options['created_date'] = $created_date;
        $options['lang_combobox'] = $this->utility_model->get_lang_combo(array('lang' => $lang, 'extra' => 'onchange="javascript:get_categories_by_lang();" class="btn"'));
        $options['loai_benh'] = $loai_benh;
        $options['combo_order'] = $combo_order;
        $options['header'] = 'Sửa bồi thường';
        $options['button_name'] = 'Sửa bồi thường';
        $options['submit_uri'] = ORDER_ADMIN_BASE_URL . '/edit';
        $options['scripts'] = $this->_get_scripts();
        return $options;
    }

    /**
     *  sửa trong DB nếu Validate OK
     * @return type
     */
    private function _do_edit_orders() {
        $post_data = $this->_get_posted_orders_data();

        $post_data['id'] = $this->input->post('id');

        $this->orders_model->update($post_data);
//            echo '<pre>';
//            print_r($post_data);
//            echo '</pre>';
//            die();
        $stbh_cl_check = $this->input->post('stbh_cl');
        $st_gh_bh_check = $this->input->post('st_gh_bh');
        $check_dbh = $this->input->post('check_dbh');
        $xac_nhan = $this->input->post('xac_nhan');
        if (!empty($stbh_cl_check)) {
            $stbh_cl = $stbh_cl_check;
        } else {
            $stbh_cl = $st_gh_bh_check;
        }

        $options = array(
            'cmt_custom' => $post_data['cmt_custom_duoc'],
            'loai_benh' => $post_data['loai_benh'],
            'stbh_cl' => ($stbh_cl - $post_data['price_cnbt']),
            'st_dbt' => ($st_gh_bh_check - ($stbh_cl - $post_data['price_cnbt'])),
        );

        if ($xac_nhan == '1') {
            if ($post_data['order_status'] == 3 && $check_dbh == 1) {
                $this->orders_model->update_orders_cl($options);
            } elseif ($check_dbh == 0) {

                $this->orders_model->insert_cl($options);
            } else {
                echo 'Hình như có lỗi';
            }
        }

        $lang = $this->phpsession->get('orders_lang');
        redirect(ORDER_ADMIN_BASE_URL . '/' . $lang);
    }

    /**
     * Xóa tin
     */
    public function delete() {
        $options = array();
        if ($this->is_postback()) {
            $id = $this->input->post('id');
            $this->orders_model->delete($id);
            $options['warning'] = 'Đã xóa thành công';
        }
        $lang = $this->phpsession->get('orders_lang');
        redirect(ORDER_ADMIN_BASE_URL . '/' . $lang);
    }

    private function _get_scripts() {
        $scripts = '<script type="text/javascript" src="/plugins/tiny_mce/tiny_mce.js?v=20111006"></script>';
        $scripts .= '<script language="javascript" type="text/javascript" src="/plugins/tiny_mce/plugins/imagemanager/js/mcimagemanager.js?v=20111006"></script>';
        $scripts .= '<script type="text/javascript">enable_advanced_wysiwyg("wysiwyg");</script>';
        return $scripts;
    }

    function change_status() {
        $id = $this->input->post('id');
        $orders = $this->orders_model->get_orders(array('id' => $id));
        $status = $orders->status == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
        $this->orders_model->update(array('id' => $id, 'status' => $status));
    }

    public function up() {
        $orders_id = $this->input->post('id');
        $this->orders_model->update(array('id' => $orders_id, 'updated_time' => date('Y-m-d H:i:s')));
        $lang = $this->phpsession->get('orders_lang');
        redirect(ORDER_ADMIN_BASE_URL . '/' . $lang);
    }

    function export($options = array()) {
        $options = array();
//        if($this->phpsession->get('customer_name_search') != '')
//            $options['keyword'] = $this->phpsession->get('customer_name_search');
//        if(is_array($this->phpsession->get('date_filter'))){
//            $options = array_merge($options, $this->phpsession->get('date_filter'));
//        }
        $options = array_merge($options, $this->_get_data_from_filter());
        $options['id'] = $this->input->post('id');

        $orders = $this->orders_model->get_orders($options);

        if (count($orders) > 0) {

            //load our new PHPExcel library
            $this->load->library('excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Order excel');
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A1', 'STT');
            $this->excel->getActiveSheet()->setCellValue('B1', 'MÃ ĐƠN HÀNG');
            $this->excel->getActiveSheet()->setCellValue('C1', 'NGÀY MUA HÀNG');
            $this->excel->getActiveSheet()->setCellValue('D2', 'HỌ VÀ TÊN');
            $this->excel->getActiveSheet()->setCellValue('D1', 'THÔNG TIN KHÁCH HÀNG');
            $this->excel->getActiveSheet()->setCellValue('E2', 'ĐỊA CHỈ GIAO HÀNG');
            $this->excel->getActiveSheet()->setCellValue('F2', 'SỐ ĐIỆN THOẠI');
            $this->excel->getActiveSheet()->setCellValue('G2', 'EMAIL');
            $this->excel->getActiveSheet()->setCellValue('H2', 'NGÀY GIỜ YÊU CẦU GIAO HÀNG');
            $this->excel->getActiveSheet()->setCellValue('I2', 'GHI CHÚ');
            $this->excel->getActiveSheet()->setCellValue('J1', 'CÁC MẶT HÀNG/SỐ LƯỢNG');
            $this->excel->getActiveSheet()->setCellValue('K1', 'THÀNH TIỀN');
            $this->excel->getActiveSheet()->setCellValue('L1', 'HÌNH THỨC THANH TOÁN');
            $this->excel->getActiveSheet()->setCellValue('M1', 'TÌNH TRẠNG ĐƠN HÀNG');

            $this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('G2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('H2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('I2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('J2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('K2')->getFont()->setBold(true);
            //merge cell A1 until D1
            $this->excel->getActiveSheet()->mergeCells('D1:I1');
            //set aligment to center for that merged cell (A1 to D1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $stt = 1;
            $row = 3;
            $total_m = 0;

            foreach ($orders as $order):
                //các mặt hàng

                $order_details = $this->orders_details_model->get_orders_details(array('id' => $order->id));

                $order_detail_product = '';
                foreach ($order_details as $product):
                    $order_detail_product .= '[' . $product->product_name;
                    $order_detail_product .= '/' . $product->quantity . '] ';

                endforeach;
                $sale_date = date('d/m/Y', strtotime($order->sale_date));

                $delivery_form = '';
                if ($order->kind_pay == 1) {
                    $delivery_form = 'Thanh toán chuyển khoản';
                } elseif ($order->kind_pay == 2) {
                    $delivery_form = 'Thanh toán trực tiếp';
                } else {
                    $delivery_form = 'Thanh toán kiểu khác';
                }

                $total_m += $order->total;
                $total = get_price_in_vnd($order->total);

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $stt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $order->id);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $sale_date);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $order->fullname);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $order->address);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $order->tel);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $order->email);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $order->reserve_time);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $order->message);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $order_detail_product);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $total);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $delivery_form);

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, $order->order_status);

                $stt++;
                $row++;
            endforeach;
            $row++;
            $this->excel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->mergeCells('B' . $row . ':Z' . $row);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, "TỔNG TIỀN:");

            if ($total_m > 0)
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, get_price_in_vnd($total_m) . ' VNĐ');
            else
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, '0 VNĐ');

            $filename = 'dang_sach_hoa_don.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
        }else {
            //hoa don khong co san pham
            return $this->browse(array('error' => 'Mục bạn đã chọn hiện thời không có hóa đơn!'));
        }
    }

    function import($options = array()) {
        //load our new PHPExcel library
        $this->load->library('excel');

        $path = "test.xlsx";

        $objPHPExcel = PHPExcel_IOFactory::load($path);
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $worksheetTitle = $worksheet->getTitle();
            $highestRow = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            $nrColumns = ord($highestColumn) - 64;
            echo "<br>The worksheet " . $worksheetTitle . " has ";
            echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
            echo ' and ' . $highestRow . ' row.';
            echo '<br>Data: <table border="1"><tr>';
            for ($row = 1; $row <= $highestRow; ++$row) {
                echo '<tr>';
                for ($col = 0; $col < $highestColumnIndex; ++$col) {
                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                    $val = $cell->getValue();
                    $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                    echo '<td>' . $val . '<br>(Typ ' . $dataType . ')</td>';
                }
                echo '</tr>';
            }
            echo '</table>';
        }

        for ($row = 2; $row <= $highestRow; ++$row) {
            $val = array();
            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, $row);
                $val[] = $cell->getValue();
            }

            $Connection = "INSERT INTO " . $this->db->dbprefix . "customers (fullname, email, phone) VALUES ('" . $val[1] . "','" . $val[2] . "','" . $val[3] . "')";
        }
    }

    function btcn($options = array()) {
        $options = array('');
        $options = array_merge($options, $this->_get_data_from_filter_cmt());

        // tong so ho so boi thuong
        $options['total_orders'] = $this->orders_model->get_orders_count($options);
        // tong so tien yeu cau boi thuong
        $sum['cmt'] = $options['cmt'];
        $sum['total'] = 'total';
        $sum['onehit'] = true;
        $options['total_ycbt'] = $this->orders_model->sum_custom($sum);
        // tong so tien duoc boi thuong
        $sums['cmt'] = $options['cmt'];
        $sums['price_cnbt'] = 'price_cnbt';
        $sums['onehit'] = true;
        $options['price_cnbt'] = $this->orders_model->sum_custom($sums);

        $options['onehit'] = true;
        $options['custom'] = $this->customers_model->get_customers($options);

        $this->_view_data['main_content'] = $this->load->view('admin/bt_ca_nhan', $options, TRUE);
        $this->_view_data['title'] = 'Báo cáo bồi thường cá nhân' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    private function _get_data_from_filter_cmt() {
        $options = array();
        if ($this->is_postback()) {

            $options['cmt'] = $this->db->escape_str($this->input->post('cmt', TRUE));
            $this->phpsession->save('custom_cmt', $options);
        } else {
            $temp_options = $this->phpsession->get('custom_cmt');
            if (is_array($temp_options)) {
                $options['cmt'] = $temp_options['cmt'];
            } else {
                $options['cmt'] = '';
            }
        }
        return $options;
    }

    function btdn($options = array()) {
        $options = array('');
        //$options = array_merge($options, $this->_get_data_from_filter_doanh_nghiep());
        // tong so ho so boi thuong
        $options['total_orders'] = $this->orders_model->get_orders_count($options);
        // tong so tien yeu cau boi thuong
        $sum['cmt'] = $options['cmt'];
        $sum['total'] = 'total';
        $sum['onehit'] = true;
        $options['total_ycbt'] = $this->orders_model->sum_custom($sum);
        // tong so tien duoc boi thuong
        $sums['cmt'] = $options['cmt'];
        $sums['price_cnbt'] = 'price_cnbt';
        $sums['onehit'] = true;
        $options['price_cnbt'] = $this->orders_model->sum_custom($sums);

        $options['onehit'] = true;
        $options['custom'] = $this->customers_model->get_customers($options);

        // Lấy thông tin các doanh nghiệp đã mua BH
        $data_get = array(
            'phanloai_kh' => DOANHNGHIEP,
        );
        $a_dn_mbh = $this->customers_model->get_doanhnghiep_mbh($data_get);
//        echo '<pre>';
//        print_r($a_dn_mbh);
//        die;
        if (!empty($a_dn_mbh)) {
            $options['a_dn_mbh'] = $a_dn_mbh;
        }
//        //end
//        $key= isset($_GET['key'])?$_GET['key']:'';
//        if($key!=''){
//            // Lấy thông tin dn mua BH
//                $data_get = array(
//                    'id' => $dn_id,
//                    'get_one' => TRUE,
//                    'skip_join' => TRUE,
//                );
//                $dn_mbh = $this->customers_model->get_customers($data_get);
//                if(is_object($dn_mbh)){
//                    $data['dn_mbh']= $dn_mbh;
//                }
//        }

        $options['scripts'] = $this->scripts();
        $this->_view_data['main_content'] = $this->load->view('admin/bt_doanh_nghiep', $options, TRUE);
        $this->_view_data['title'] = 'Báo cáo bồi thường doanh nghiệp' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    private function scripts() {
        $scripts = '<script type="text/javascript">
                    $(".js-example-disabled-results").select2();
                    </script>';
        return $scripts;
    }

    private function _get_data_from_filter_doanh_nghiep() {
        $options = array();
        if ($this->is_postback()) {

            $options['congty_canhan'] = $this->db->escape_str($this->input->post('congty_canhan', TRUE));
            $this->phpsession->save('custom_cmt', $options);
        } else {
            $temp_options = $this->phpsession->get('custom_congty_canhan');
            if (is_array($temp_options)) {
                $options['congty_canhan'] = $temp_options['congty_canhan'];
            } else {
                $options['congty_canhan'] = '';
            }
        }
        return $options;
    }

    function thong_tin_bao_hiem($options = array()) {
        $options = array('');

        $this->_view_data['main_content'] = $this->load->view('admin/bc_thong_tin', $options, TRUE);
        $this->_view_data['title'] = 'Báo cáo bồi thường doanh nghiệp' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    function thong_tin_boi_thuong($options = array()) {
        $options = array('');

        $this->_view_data['main_content'] = $this->load->view('admin/bc_boi_thuong', $options, TRUE);
        $this->_view_data['title'] = 'Báo cáo bồi thường doanh nghiệp' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

}
