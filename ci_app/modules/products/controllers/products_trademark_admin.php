<?php

class Products_Trademark_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        //modules::run('auth/auth/validate_login');
        $this->_layout = 'admin_ui/layout/main';
        $this->ad_check_login();
        //======================================================================
        $ss_ql_all = $this->phpsession->get('ss_ql_all');
        $ss_ql_bbh = $this->phpsession->get('ss_ql_bbh');
        if ($ss_ql_all != QL_CHECKED) {
            if ($ss_ql_bbh != QL_CHECKED) {
                redirect('dashboard');
            }
        }
    }

    //==========================================================================
    function browse() {
        $options = array();

        $bbh = $this->products_trademark_model->CommonGet(TBL_DN_BBH);
        if (!empty($bbh)) {
            $options['dn_bbh'] = $bbh;
        }
        //======================================================================
        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $options['options'] = $options;
        }
        //======================================================================
        $tpl = 'ad/bbh/bbh_list';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Nhà cung cấp BH' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function add() {
        $options = array();
        //======================================================================
        if ($this->is_postback()) {
            if (!$this->_do_add()) {
                $options['error'] = validation_errors();
                $options['options'] = $options;
            }
        }
        $this->_get_add_form_data($options);
        //======================================================================
        $options['name'] = $this->input->post('name');
        $options['header'] = 'Thêm Nhà cung cấp BH';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_TRADEMARK_ADMIN_ADD_URL;
        //end
        //======================================================================
        $tpl = 'ad/bbh/bbh_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Thêm Nhà cung cấp BH' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _get_add_form_data(&$options = array()) {
        // Lấy thông tin các dn bán BH
        $data_get = array(
            'parent_id' => FALSE,
        );
        $dn_bbh = $this->products_trademark_model->get_products_trademark($data_get);
        if (!empty($dn_bbh)) {
            $options['a_dn_bbh'] = $dn_bbh;
        }
        //end
    }

    //==========================================================================
    private function _do_add() {
        $this->form_validation->set_rules('name', 'Nhà cung cấp BH', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|is_unique[products_trademark.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|max_length[255]');
        //======================================================================
        if ($this->form_validation->run()) {
            $data = array(
                'name' => my_trim($this->input->post('name')),
                'email' => my_trim($this->input->post('email')),
                'password' => (my_trim($this->input->post('password'))),
                'password_fm' => md5(my_trim($this->input->post('password'))),
                'parent_id' => my_trim($this->input->post('categories')),
                'status' => STATUS_ACTIVE,
                'editor' => $this->phpsession->get('user_id'),
            );
            //==================================================================
            $bbh_id = $this->input->post('id');
            if ($bbh_id > 0) {
                $data['id'] = $bbh_id;
                $this->products_trademark_model->update($data);
            } else {
                $data['creator'] = $this->phpsession->get('user_id');
                $this->products_trademark_model->insert($data);
            }
            //==================================================================
            redirect(PRODUCTS_TRADEMARK_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //==========================================================================
    function edit() {
        $options = array();
        if ($this->is_postback()) {
            if (!$this->input->post('from_list')) {
                if (!$this->_do_edit()) {
                    $options['error'] = validation_errors();
                    $options['options'] = $options;
                }
            }
        }
        //======================================================================
        $this->_get_add_form_data($options);
        $id = $this->input->post('id');
        //======================================================================
        // Lấy thông tin dn bán BH
        $dn_bbh = $this->products_trademark_model->get_products_trademark(array('id' => $id));
        if (is_object($dn_bbh)) {
            $options['dn_bbh'] = $dn_bbh;
        }
        //======================================================================
        $options['header'] = 'Sửa Nhà cung cấp BH';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_TRADEMARK_ADMIN_EDIT_URL;
        //======================================================================
        $tpl = 'ad/bbh/bbh_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Sửa Nhà cung cấp BH' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================

    private function _do_edit() {
        $this->form_validation->set_rules('name', 'Nhà cung cấp BH', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|callback_check_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|max_length[255]');
        if ($this->form_validation->run($this)) {

            $data = array(
                'id' => $this->input->post('id'),
                'name' => my_trim($this->input->post('name', TRUE)),
                'email' => my_trim($this->input->post('email')),
                'password' => (my_trim($this->input->post('password'))),
                'password_fm' => md5(my_trim($this->input->post('password'))),
                'parent_id' => my_trim($this->input->post('categories')),
                'editor' => $this->phpsession->get('user_id'),
            );
            //==================================================================
            $this->products_trademark_model->update($data);
            redirect(PRODUCTS_TRADEMARK_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //==========================================================================
    function _check_email($str) {

        $data = array(
            'user_id' => $this->input->post('user_id', TRUE),
            'email' => $this->input->post('email', TRUE)
        );
        // check email exits when edit
        if ($this->users_model->check_user_by_options($data) == FALSE) {
            $this->form_validation->set_message('check_email', $this->msg_valid_email);
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //==========================================================================
    public function check_email($str) {
        if ($str != '') {
            // Lấy thông tin dn bbh
            $data_get = array(
                'email' => $str,
            );
            $count_dn_bbh = $this->products_trademark_model->get_products_trademark_count($data_get);
            if ($count_dn_bbh >= 2) {
                $this->form_validation->set_message('check_email', 'Email đã tồn tại');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    //==========================================================================
    function delete() {
        if ($this->is_postback() && $this->input->post('from_list')) {
            $id = $this->input->post('id');
            $check1 = $this->products_model->get_products_count(array('trademark_id' => $id));
            if (empty($check1)) {
                $this->products_trademark_model->delete($id);
            }
            redirect(PRODUCTS_TRADEMARK_ADMIN_BASE_URL);
        }
    }

    //==========================================================================
}
