<?php 
    $config = get_cache('configurations_' .  get_language());
    if(!empty($config)){
//        $facebook_url = !empty($config['facebook_id'])?'https://www.facebook.com/'.$config['facebook_id']:'#';
        $logo = !empty($config['logo'])?base_url().UPLOAD_URL_LOGO.$config['logo']:base_url().'images/logo.png';
//        $telephone = !empty($config['telephone'])?$config['telephone']:'0963 833 868';
    }
    $this->load->library('Mobile_Detect');
    $detect = new Mobile_Detect();
    if($detect->isMobile() && !$detect->isTablet()){
        $isMobile = TRUE;
    } else {
        $isMobile = FALSE;
    }
?>
<nav role="navigation" class="navbar navbar-default">
    <div class="container top_nav">
        <div class="row">
            <div class="navbar-header">
                <button data-target="#myNavbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php // if($isMobile){ ?>
<!--                <a href="<?php // echo get_base_url(); ?>" class="logo" title="<?php // echo __('IP_DEFAULT_TITLE'); ?>" >
                    <img src="<?php // echo $logo; ?>" />
                </a>-->
                <?php // } ?>
                <?php if($isMobile){ ?>
                <a href="<?php echo get_base_url(); ?>" class="logo" title="<?php echo __('IP_DEFAULT_TITLE'); ?>" >
                    <img src="<?php echo $logo; ?>" />
                </a>
                <?php } ?>
            </div>
            <div id="myNavbar" class="navbar-collapse noPadding collapse" style="height: 1px;">
                <div class="menustyle">
                    <?php 
                        if(isset($active_menu)){$active_menu=$active_menu;}else{$active_menu=NULL;} 
                        if(isset($current_menu)){$current_menu=$current_menu;}else{$current_menu=NULL;}
                    ?>
                    <?php echo modules::run('menus/menus/get_main_menus',array('current_menu'=>$current_menu,'active_menu'=>$active_menu,'menu_type'=>FRONT_END_MENU_TOP_CAT_ID)); ?>
                </div>
            </div>
        </div>
    </div>
</nav>