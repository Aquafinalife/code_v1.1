<?php
class Products_Material_Model extends MY_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function get_products_material($options = array())
    {
        $this->db->select(' products_material.*,
                                
                                products_categories.category,
                                
                                products_trademark.name trademark_name,
                                
                               
                            ');
           
            $this->db->join('products_categories', 'products_material.cat_id = products_categories.id', 'left');
            $this->db->join('products_trademark', 'products_material.trade_id = products_trademark.id', 'left');
            
        if (isset($options['trade_id']))
            $this->db->where('products_material.trade_id', $options['trade_id']);
        
        if (isset($options['cat_id']))
            $this->db->where('products_material.cat_id', $options['cat_id']);
        
        if (isset($options['id']))
            $this->db->where('products_material.id', $options['id']);
        
        if (isset($options['name']))
            $this->db->where('products_material.name', $options['name']);
        
        if (isset($options['status']))
            $this->db->where('products_material.status', $options['status']);
        
        $this->db->order_by('products_material.id');

        $query = $this->db->get('products_material');
        if($query->num_rows() > 0){
            if (isset($options['id']) || isset($options['onehit'])) return $query->row(0);

            if(isset ($options['last_row']))
                return $query->last_row();

            return $query->result();
        }else{
            return NULL;
        }
    }

    public function get_products_material_count($options = array())
    {
        return count($this->get_products_material($options));
    }

    public function get_products_material_combo($options = array())
    {
        // Default categories name
        if ( ! isset($options['combo_name'])) {
            $options['combo_name'] = 'products_material';
        }
        if ( ! isset($options['extra'])){
            $options['extra'] = '';
        }
        if(! isset($options['is_add_edit_menu'])){
            if(isset($options['is_add_edit_cat']))
                $categories[ROOT_CATEGORY_ID] = 'Tất cả';
            else
                $categories[DEFAULT_COMBO_VALUE] = ' - Chọn';
        }
        
//        $categories = array();
        
        $this->db->where('status',STATUS_ACTIVE);
        $data = $this->db->get('products_material');
        if($data->num_rows() > 0){
            $data = $data->result_array();
            foreach($data as $id => $value){
                $id = $value['id'];
                $categories[$id] = $value['name'];
            }                                   
            if (!isset($options[$options['combo_name']])) 
                $options[$options['combo_name']] = DEFAULT_COMBO_VALUE;
            return form_dropdown($options['combo_name'], $categories, $options[$options['combo_name']], $options['extra']);
        }else{
            return NULL;
        }
        
    }

}