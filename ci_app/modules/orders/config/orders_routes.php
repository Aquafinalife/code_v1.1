<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['^dashboard/orders/btcn']            = 'orders/orders_admin/btcn';
$route['^dashboard/orders/btdn']            = 'orders/orders_admin/btdn';

$route['^dashboard/orders/thong_tin_bao_hiem']            = 'orders/orders_admin/thong_tin_bao_hiem';
$route['^dashboard/orders/thong_tin_boi_thuong']            = 'orders/orders_admin/thong_tin_boi_thuong';

$route['^ajax_get_kh_dbh']                      = 'orders/orders_admin/ajax_get_kh_dbh';

$route['dashboard/orders/edit/(:num)']                                          ='orders/orders_admin/edit/$1';

$route['^dashboard/orders']                                                     = 'orders/orders_admin/browse/0';
$route['^dashboard/orders/(\d+)$']                                              = 'orders/orders_admin/browse/$1';