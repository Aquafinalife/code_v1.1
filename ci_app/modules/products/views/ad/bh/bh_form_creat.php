<?php
if (isset($bh) && is_object($bh)) {
//    echo '<pre>';
//    print_r($bh);
//    die;
    $bh_id = $bh_id = $bh->BH_id;
    $congty_canhan = $bh->congty_canhan;
    //end
    $kh_mbh = $bh->kh_mbh;
    $kh_dbh = $bh->kh_dbh;
    $bh_mqh = $bh->mqh;
    $dn_bbh = $bh->dn_bbh;
    $cn_bbh = $bh->cn_bbh;
    $goi_bh = $bh->goi_bh;
    $sohopdong = $bh->sohopdong;
    $giay_chungnhan = $bh->giay_chungnhan;
    $daily = $bh->daily;
    $ghichu = $bh->ghichu;
    $ngay_hieuluc = $bh->ngay_hieuluc;
    $ngay_batdau = $bh->ngay_batdau;
    $ngay_ketthuc = $bh->ngay_ketthuc;
    $ngay_thanhtoan = $bh->ngay_thanhtoan;
}
//==============================================================================
echo form_open_multipart($submit_uri);
if (isset($bh_id)) {
    echo form_hidden('id', $bh_id);
    echo form_hidden('bh_id', $bh_id);
}
//==============================================================================
$a_mqh = get_a_mqh();
//==============================================================================
//echo '<pre>';
//print_r($kh);
//die;
?>

<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Thay đổi thông tin về bảo hiểm"</small>
    <span class="fright">
        <a class="button close" href="<?php echo PRODUCTS_ADMIN_BASE_URL; ?>"><em>&nbsp;</em>Đóng</a>
    </span>
    <br class="clear"/>
</div>

<div id="sort_success">Vị trí ảnh đã được cập nhật</div>
<div class="form_content">
    <?php $this->load->view('powercms/message'); ?>
    <ul class="nav nav-tabs tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Thêm đơn bảo hiểm</a></li>
    </ul>
    <div id="tabs" class="tab-content tab_container">
        <div id="tab1" class="tab_content">
            <div class="row">
                <div class="col-md-6">
                    <table>
                        <tr><td class="title">Công ty Bảo Hiểm: (<span>*</span>)</td></tr>
                        <tr>
                            <td id="">
                                <select  name="dn_bbh" class="filter_dn_bbh js-example-disabled-results" goi_bh="1">
                                    <option value="">--Chọn Công ty/doanh nghiệp BH--</option>
                                    <?php
                                    if (!empty($a_dn_bbh)) {
                                        foreach ($a_dn_bbh as $index) {
                                            $name = $index->name;
                                            $parent_id = $index->parent_id;
                                            $id = $index->id;
                                            $selected = ($id == $dn_bbh) ? 'selected="selected"' : '';
                                            if ($parent_id == FALSE) {
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr><td class="title">Chi nhánh: (<span>*</span>)</td></tr>
                        <tr>
                            <td class="">
                                <div class="box_filter_cn_bbh">
                                    <select name="cn_bbh" class="">
                                        <option value="">--Chọn chi nhánh--</option>
                                        <?php
                                        if ($bh_id > 0) {
                                            if (!empty($a_dn_bbh)) {
                                                foreach ($a_dn_bbh as $index) {
                                                    $name = $index->name;
                                                    $parent_id = $index->parent_id;
                                                    $id = $index->id;
                                                    $selected = ($id == $cn_bbh) ? 'selected="selected"' : '';
                                                    if ($dn_bbh == $parent_id) {
                                                        ?>
                                                        <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <table>
                        <tr><td class="title">Bên mua BH: (<span>*</span>)</td></tr>
                        <tr><td><i style="font-size: 13px;">(Gõ tên người mua nếu khách hàng đã tồn tại hoặc bấm vào thêm mới khách hàng)</i></td></tr>
                        <tr>
                            <td style="position:relative">
                                <select name="kh_mbh" class="js-example-disabled-results">
                                    <option value="">-- Điền Tên Doanh Nghiệp/Cá Nhân Mua BH --</option>            
                                    <?php
                                    if (isset($kh) && !empty($kh)) {
                                        foreach ($kh as $index) {
                                            $congty_canhan = $index->congty_canhan;
                                            $mst_cmt = $index->mst_cmt;
                                            $KH_id = $index->KH_id;
                                            $selected = ($KH_id == $kh_mbh) ? 'selected="selected"' : '';
                                            ?>
                                            <option <?php echo $selected ?> value="<?php echo $KH_id ?>"><?php echo $congty_canhan . ' - MST/CMT: ' . $mst_cmt ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr><td class="title">Người được BH: (<span>*</span>)</td></tr>
                        <tr><td><i style="font-size: 13px;">(Gõ tên người được bảo hiểm)</i></td></tr>
                        <tr>
                            <td >
                                <div id="result_duoc"></div>
                                <select name="kh_dbh" class="js-example-disabled-results">
                                    <option value="">-- Điền Tên Cá Nhân Được Mua BH --</option>            
                                    <?php
                                    // Khi sửa bh
                                    if ($bh_id > 0) {
                                        $a_cn_dbh = array_diff($a_cn_dbh, array($kh_dbh));
                                    }
                                    //==================================================
                                    if (isset($kh) && !empty($kh)) {
                                        $a_cn_dbh = isset($a_cn_dbh) ? $a_cn_dbh : array();
                                        foreach ($kh as $index) {
                                            $congty_canhan = $index->congty_canhan;
                                            $mst_cmt = $index->mst_cmt;
                                            $KH_id = $index->KH_id;
                                            $phanloai_kh = $index->phanloai_kh;
                                            $selected = ($KH_id == $kh_dbh) ? 'selected="selected"' : '';

                                            if (!in_array($KH_id, $a_cn_dbh) && $phanloai_kh == CANHAN) {
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $KH_id ?>"><?php echo $congty_canhan . ' - MST/CMT: ' . $mst_cmt ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr><td class="title">Mối quan hệ: (<span>*</span>)</td></tr>
                        <tr><td><i style="font-size: 13px;">(Mối quan hệ giữa người mua BH và người được BH)</i></td></tr>
                        <tr>
                            <td >
                                <div id="result_duoc"></div>
                                <select name="mqh" class="js-example-disabled-results">
                                    <option value="">-- Mối quan hệ giữa người mua BH và người được BH --</option>            
                                    <?php
                                    if (isset($a_mqh) && !empty($a_mqh)) {
                                        foreach ($a_mqh as $k => $v) {
                                            $selected = ($k == $bh_mqh) ? 'selected="selected"' : '';
                                            ?>
                                            <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <table>
                        <tr><td class="title">Chọn gói BH: (<span>*</span>)</td></tr>
                        <tr>
                            <td class="">
                                <div class="box_filter_goi_bh">
                                    <select name="goi_bh" class="js-example-disabled-results">
                                        <option value="">--Chọn gói BH--</option>
                                        <?php
                                        if ($bh_id > 0) {
                                            if (!empty($a_goi_bh)) {
                                                foreach ($a_goi_bh as $index) {
                                                    $name = $index->name;
                                                    $id = $index->id;
                                                    $selected = ($id == $goi_bh) ? 'selected="selected"' : '';
                                                    ?>
                                                    <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                            </td>
                        </tr>
                    </table>
                    <hr>
                    <table>
                        <tr><td class="title">Số hợp đồng bảo hiểm: </td></tr>
                        <tr><td><?php echo form_input(array('name' => 'sohopdong', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($sohopdong) ? $sohopdong : set_value('sohopdong'))); ?></td></tr>
                        <tr><td class="title">Giấy chứng nhận bảo hiểm: </td></tr>
                        <tr><td><?php echo form_input(array('name' => 'giay_chungnhan', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($giay_chungnhan) ? $giay_chungnhan : set_value('giay_chungnhan'))); ?></td></tr>
                        <tr><td class="title">Bên môi giới, đại lý: </td></tr>
                        <tr><td><?php echo form_input(array('name' => 'daily', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($daily) ? $daily : set_value('daily'))); ?></td></tr>
                    </table>
                    <hr>
                    <table>
                        <tr><td class="title">Ngày bắt đầu: </td></tr>
                        <tr><td><?php echo form_input(array('class' => 'show_datepicker', 'id' => 'news_created_date', 'name' => 'ngay_batdau', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($ngay_batdau) ? $ngay_batdau : set_value('ngay_batdau'))); ?></td></tr>
                        <tr><td class="title">Ngày hiệu lực: </td></tr>
                        <tr><td><?php echo form_input(array('class' => 'show_datepicker', 'id' => 'news_created_date', 'name' => 'ngay_hieuluc', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($ngay_hieuluc) ? $ngay_hieuluc : set_value('ngay_hieuluc'))); ?></td></tr>
                        <tr><td class="title">Ngày kết thúc: </td></tr>
                        <tr><td><?php echo form_input(array('class' => 'show_datepicker', 'id' => 'news_created_date', 'name' => 'ngay_ketthuc', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($ngay_ketthuc) ? $ngay_ketthuc : set_value('ngay_ketthuc'))); ?></td></tr>
                        <tr><td class="title">Ngày thanh toán:</td></tr>
                        <tr><td><?php echo form_input(array('class' => 'show_datepicker', 'id' => 'date_of_payment', 'name' => 'ngay_thanhtoan', 'size' => '30', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($ngay_thanhtoan) ? $ngay_thanhtoan : set_value('ngay_thanhtoan'))); ?></td></tr>
                        <tr><td class="title">Ghi chú bảo hiểm: </td></tr>
                        <tr><td><?php echo form_textarea(array('cols' => 65, 'rows' => 8, 'id' => 'content', 'name' => 'ghichu', 'value' => ($ghichu != '') ? $ghichu : set_value('ghichu'), 'class' => 'wysiwyg elm1')); ?></td></tr>
                    </table>
                    <br class="clear">
                    <div style="margin-top: 10px;"></div>
                </div>
                <div class="col-md-6">
                    <a class="btn btn-primary btn-sm" href="<?php echo site_url('dashboard/customers/add-kh-cn') ?>">Thêm khách hàng cá nhân</a>
                    <a class="btn btn-success ml15 btn-sm" href="<?php echo site_url('dashboard/customers/add-kh-dn') ?>">Thêm khách hàng doanh nghiệp</a>
                </div>
            </div>

            <?php
            $truong_phong = $this->phpsession->get('role_id');
            if ($truong_phong != 5) {
                ?>
                <input type="submit" name="btnSubmit" value="<?php if (isset($button_name)) echo $button_name; ?>" class="btn btn-danger" />
                <input type="reset" value="Làm lại" class="btn" />
            <?php } ?>
            <br class="clear">&nbsp;
        </div>
    </div>

</div>
<?php echo form_close(); ?>