<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
//end
?>
<div class="box_filter" style="margin-bottom: 20px">

    <form action="<?php echo site_url(URL_BT_CN) ?>" method="get">
    </form>
</div>
<div class="box-typical box-typical-padding">
    <h5 class="m-t-lg with-border">Thay đổi mật khẩu</h5>
    <form action="<?php echo site_url('change-password') ?>" method="post">
        <!--        <div class="form-group row">
                    <label class="col-sm-2 form-control-label">Mật khẩu cũ</label>
                    <div class="col-sm-6">
                        <p class="form-control-static"><input name="password_old" type="password" class="form-control" id="inputPassword" placeholder="Mật khẩu cũ"></p>
                    </div>
                </div>-->
        <div class="msg_errors col-sm-12 mb10">
            <?php echo validation_errors(); ?>
            <?php
            $msg = $this->phpsession->flashget('msg');
            if ($msg != '') {
                echo $msg;
            }
            ?>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label">Mật khẩu mới</label>
            <div class="col-sm-6">
                <p class="form-control-static"><input name="password_new" type="password" class="form-control" id="inputPassword" placeholder="Mật khẩu mới"></p>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 form-control-label">Nhập lại mật khẩu</label>
            <div class="col-sm-6">
                <p class="form-control-static"><input name="password_rp" type="password" class="form-control" id="inputPassword" placeholder="Nhập lại mật khẩu"></p>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-6">
                <button type="submit" class="btn btn-primary" name="submit">Xác nhận thay đổi mật khẩu</button>
            </div>
        </div>
    </form>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>