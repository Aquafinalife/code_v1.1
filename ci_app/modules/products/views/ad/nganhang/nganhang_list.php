<div class="page_header">
    <h1 class="fleft">Ngân hàng</h1>
    <small class="fleft">"Thêm/sửa Ngân hàng"</small>
    <span class="fright"><a href="<?php echo PRODUCTS_MATERIAL_ADMIN_BASE_URL; ?>" class="button close" style="padding:5px;" title="Đóng"><em></em><span>Đóng</span></a></span>
    <span class="fright"><a class="button add" href="<?php echo PRODUCTS_MATERIAL_ADMIN_ADD_URL; ?>"><em>&nbsp;</em>Thêm Ngân hàng</a></span>
    <br class="clear"/>
</div>
<?php
echo form_open('', array('id' => 'submit_form'), array('id' => 0, 'from_list' => TRUE));
echo isset($uri) ? form_hidden('uri', $uri) : NULL;
echo form_hidden('back_url', PRODUCTS_MATERIAL_ADMIN_BASE_URL);
echo form_close();
?>
<div class="form_content">
    <table class="list" style="width: 100%;margin: 10px 0;">
        <?php $this->load->view('powercms/message'); ?>
        <tr>
            <th class="left">STT</th>
            <th class="left">TÊN NGÂN HÀNG</th>
            <th class="center" style="width: 5%">CHỨC NĂNG</th>
        </tr>
        <?php
        if (isset($a_nh) && !empty($a_nh)) {
            $stt = 0;
            foreach ($a_nh as $index):
                $style = $stt++ % 2 == 0 ? 'even' : 'odd';
                $id = $index->id;
                $name = $index->name;
//            $uri = '/'.PRODUCTS_MATERIAL_SLUG.$value->name;
//            $url = get_base_url().PRODUCTS_MATERIAL_SLUG.$value->name;
                ?>
                <tr class="<?php echo $style; ?>">
                    <td class="left"><?php echo $id; ?></td>
                    <td class="left"><?php echo $name; ?></td>
                    <td class="center">
                        <a class="edit" title="Sửa" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id; ?>, '<?php echo PRODUCTS_MATERIAL_ADMIN_EDIT_URL; ?>', 'edit');"><em>&nbsp;</em></a>
                        <a class="del" title="Xóa" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id; ?>, '<?php echo PRODUCTS_MATERIAL_ADMIN_DELETE_URL; ?>', 'delete');"><em>&nbsp;</em></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php } ?>
    </table>
    <br class="clear"/>&nbsp;
</div>