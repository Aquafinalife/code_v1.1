<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH . "third_party/MX/Controller.php";

class MY_Controller extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->output->enable_profiler(TRUE);
    }

    //==========================================================================
    // Luu giu cac thong bao loi/thanh cong
    protected $_last_message = '';
    // Dat layout mac dinh, neu co thay doi thi se thay trong method tuong ung
    protected $_layout = '';
    // Chua du lieu de chuan bi cho giao dien
    protected $_view_data = array();
    protected $_options = array();

    //==========================================================================
    function is_postback() {
        if (!empty($_POST))
            return TRUE;
        return FALSE;
    }

    //==========================================================================
    function link_js($param) {
        if (is_array($param) && sizeof($param) > 0) {
            foreach ($param as $key => $value) {
                $this->output->link_js($value);
            }
        } else {
            if (is_string($param))
                $this->output->link_js($param);
        }
    }

    //==========================================================================
    function link_css() {
        if (is_array($param) && sizeof($param) > 0) {
            foreach ($param as $key => $value) {
                $this->output->link_css($value);
            }
        } else {
            if (is_string($param))
                $this->output->link_css($param);
        }
    }

    //==========================================================================
    function scripts_select2() {
        $scripts = '<script type="text/javascript">
                    $(".js-example-disabled-results").select2();
                    </script>';
        return $scripts;
    }

    //==========================================================================
    function ad_check_login() {
        $ss_ad_logged = $this->phpsession->get('is_logged_in');
        $ss_user_id = $this->phpsession->get('user_id');

        if ($ss_user_id > 0 && $ss_ad_logged == TRUE) {
            return TRUE;
        } else {
            $ss_kh_id = $this->phpsession->get('ss_kh_id');
            $ss_kh_email = $this->phpsession->get('ss_kh_email');
            //==================================================================
            if ($ss_kh_id > 0 && $ss_kh_email != '') {
                redirect('quan-tri-khach-hang');
            } else {
                redirect('login');
            }
        }
    }

    //==========================================================================
    function kh_check_login() {

        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_email = $this->phpsession->get('ss_kh_email');
        // ss dn bbh 
        $ss_bbh_id = $this->phpsession->get('ss_bbh_id');
        $ss_bbh_email = $this->phpsession->get('ss_bbh_email');
        // ss csyt
        $ss_csyt_id = $this->phpsession->get('ss_csyt_id');
        $ss_csyt_email = $this->phpsession->get('ss_csyt_email');
        // check dn bbh đăng nhập
        if ($ss_csyt_id > 0 && $ss_csyt_email != '') {
            return TRUE;
        } elseif ($ss_bbh_id > 0 && $ss_bbh_email != '') {
            return TRUE;
        } else if ($ss_kh_id > 0) {
            return TRUE;
        } else {
            redirect('');
        }
    }

    //==========================================================================
}
