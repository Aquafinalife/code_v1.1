<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_dob = substr($this->phpsession->get('ss_kh_dob'), 0, 10);
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = substr($this->phpsession->get('ss_kh_ngaycap'), 0, 10);
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');

$ss_kh_sothe_bh = $this->phpsession->get('ss_kh_sothe_bh');
$ss_kh_ma_nv = $this->phpsession->get('ss_kh_ma_nv');

$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
$ss_kh_parent_id = $this->phpsession->get('ss_kh_parent_id');
//end
?>
<div class="box_filter" style="margin-bottom: 20px">

    <form action="<?php echo site_url(URL_BT_CN) ?>" method="get">
    </form>
</div>
<table id="table-edit_" class="table table-bordered table-hover">
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Thông tin khách hàng:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th style="width: 200px;">Chỉ tiêu</th>
            <th style="width: 325px;">Giá trị</th>
            <th>Chỉ tiêu</th>
            <th>Giá trị</th>
        </tr>
    </thead>
    <?php
    if ($ss_kh_phanloai_kh == DOANHNGHIEP) {
        ?>
        <tbody>
            <tr>
                <td>Tên khách hàng</td>
                <td><?php echo $ss_kh_name ?></td>
                <td>ĐKKD - MST</td>
                <td><?php echo $ss_kh_mst_cmt ?></td>
            </tr>
            <tr>
                <td>Mã khách hàng</td>
                <td><?php echo 'MKH-' . $ss_kh_id ?></td>     
    <!--            <td>Loại khách hàng</td>
                <td><?php echo $ss_kh_phanloai_kh1_txt ?></td>-->
                <td>Điện thoại</td>
                <td><?php echo $ss_kh_dienthoai ?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><?php echo $ss_kh_email ?></td>
                <td>Di động</td>
                <td><?php echo $ss_kh_didong ?></td>
    <!--            <td>Điện thoại</td>
                <td><?php echo $ss_kh_dienthoai ?></td>-->
            </tr>
            <tr>
                <td>Địa chỉ</td>
                <td><?php echo $ss_kh_diachi ?></td>     
    <!--            <td>Di động</td>
                <td><?php echo $ss_kh_didong ?></td>-->
            </tr>
            <tr>            
            </tr>
        </tbody>
        <?php
    } else {
        ?>
        <tbody>
            <tr>
                <td>Tên khách hàng</td>
                <td><?php echo $ss_kh_name ?></td>
                <td>Mã nhân viên</td>
                <td><?php echo $ss_kh_ma_nv ?></td>
            </tr>
            <tr>
                <td>Mã khách hàng</td>
                <td><?php echo 'MKH-' . $ss_kh_id ?></td>
                <td>CMT/Hộ chiếu</td>
                <td><?php echo $ss_kh_mst_cmt ?></td>
            </tr>
            <tr>
                <td>Ngày sinh</td>
                <td><?php echo $ss_kh_dob ?></td>
                <td>Ngày cấp</td>
                <td><?php echo $ss_kh_ngaycap ?></td>
            </tr>
            <tr>
                <td>Số thẻ BH</td>
                <td><?php echo $ss_kh_sothe_bh ?></td>
                <td>Nơi cấp</td>
                <td><?php echo $ss_kh_noicap ?></td>
            </tr>
            <tr>
                <td>Địa chỉ</td>
                <td><?php echo $ss_kh_diachi ?></td>
                <td>Di động</td>
                <td><?php echo $ss_kh_didong ?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><?php echo $ss_kh_email ?></td>
                <td>Điện thoại</td>
                <td><?php echo $ss_kh_dienthoai ?></td>
            </tr>
        </tbody>
        <?php
    }
    ?>

</table>
<br><br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>