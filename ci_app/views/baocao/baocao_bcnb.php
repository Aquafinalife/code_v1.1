<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
$a_bt_thanhtoan = get_a_bt_thanhtoan();
//end
//==============================================================================
?>
<div class="box_filter" style="margin-bottom: 20px">
</div>
<table id="_table-edit" class="table table-bordered table-hover">
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Báo cáo phân tích số liệu bồi thường NHÓM BỆNH:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th>Chuẩn đoán bệnh</th>
            <th>Tổng số HSBT</th>
            <th>Tỷ lệ HSCĐB / HSBT</th>            
            <th>Số tiền bồi thường</th>
            <th>Số tiền bồi thường</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($a_ma_benh) && !empty($a_ma_benh)) {
            $tt_kh_dbh = $tt_hsbt = $tt_phi_bh = $tt_st_dbt = 0;

            foreach ($a_ma_benh as $index1) {

                $MB_id = $index1->id;
                $MB_name = $index1->name;
                $MB_code = $index1->ma_benh;
                //==============================================================
                $t_st_dbt = $t_st_ycbt = $t_hsbt = $tt_hsbt = 0;
                //==============================================================
                if (isset($bt) && !empty($bt)) {
                    $tt_hsbt = count($bt);
                    $j = 1;
                    foreach ($bt as $index) {
                        $BT_id = $index->id;
                        $so_tien_ycbt = $index->so_tien_ycbt;
                        $so_tien_dbt = $index->so_tien_dbt;
                        $BT_ma_benh = $index->OD_ma_benh;
                        $bt_status = $index->order_status;
                        //======================================================
                        if ($MB_id == $BT_ma_benh) {
                            $t_hsbt = $t_hsbt + 1;
                            //----------------------------------------------
                            $t_st_dbt = $t_st_dbt + (int) $so_tien_dbt;
                            $t_st_ycbt = $t_st_ycbt + (int) $so_tien_ycbt;
                            //$tt_st_dbt = $tt_st_dbt + (int) $so_tien_dbt;
                            //----------------------------------------------
                        }
                    }
                    //==========================================================
                }
                ?>
                <tr>
                    <td><?php echo $MB_name ?></td>
                    <td><?php echo $t_hsbt ?></td>
                    <td><?php echo round((($t_hsbt / $tt_hsbt) * 100), 2) . '%'; ?></td>
                    <td><?php echo get_price_in_vnd($t_st_ycbt) ?></td>
                    <td><?php echo get_price_in_vnd($t_st_dbt) ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
<div class="col-sm-12 text-center">
    <div class="pagination">
        <?php // echo $this->pagination->create_links();        ?>
    </div>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>