<div class="page_header">
    <h1 class="fleft">Mã bệnh</h1>
    <small class="fleft">"Thêm/sửa mã bệnh"</small>
    <span class="fright"><a href="<?php echo PRODUCTS_ORIGIN_ADMIN_BASE_URL; ?>" class="button close" style="padding:5px;" title="Đóng"><em></em><span>Đóng</span></a></span>
    <span class="fright"><a class="button add" href="<?php echo PRODUCTS_ORIGIN_ADMIN_ADD_URL; ?>"><em>&nbsp;</em>Thêm mã bệnh</a></span>
    <br class="clear"/>
</div>
<?php
echo form_open('', array('id' => 'submit_form'), array('id' => 0, 'from_list' => TRUE));
echo isset($uri) ? form_hidden('uri', $uri) : NULL;
echo form_hidden('back_url', PRODUCTS_ORIGIN_ADMIN_BASE_URL);
echo form_close();
?>
<div class="form_content">
    <table class="list" style="width: 100%;margin: 10px 0;">
        <?php $this->load->view('powercms/message'); ?>
        <tr>
            <th class="left">ID</th>
            <th class="left">MÃ BỆNH</th>
            <th class="left">TÊN BỆNH</th>
            <th class="center" style="width: 5%">CHỨC NĂNG</th>
        </tr>
        <?php
        if (isset($mb)) {
            $stt = 0;
            foreach ($mb as $index):
                $style = $stt++ % 2 == 0 ? 'even' : 'odd';
                $mb_id = $index->id;
                $name = $index->name;
                $ma_benh = $index->ma_benh;
                ?>
                <tr class="<?php echo $style; ?>">
                    <td class="left"><?php echo $mb_id; ?></td>
                    <td class="left"><?php echo $ma_benh; ?></td>
                    <td class="left"><?php echo $name; ?></td>
                    <td class="center">
                        <a class="edit" title="Sửa" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $mb_id; ?>, '<?php echo PRODUCTS_ORIGIN_ADMIN_EDIT_URL; ?>', 'edit');"><em>&nbsp;</em></a>
                        <a class="del" title="Xóa" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $mb_id; ?>, '<?php echo PRODUCTS_ORIGIN_ADMIN_DELETE_URL; ?>', 'delete');"><em>&nbsp;</em></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php } ?>
    </table>
    <br class="clear"/>&nbsp;
</div>