<?php

class Products_Color_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        modules::run('auth/auth/validate_login');
        $this->_layout = 'admin_ui/layout/main';
    }

    function browse() {
        $options = array();

        $pb = $this->products_color_model->CommonGet(TBL_PHONGBAN);
        if(!empty($pb)){
            $options['pb']= $pb;
        }
        //======================================================================
        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $options['options'] = $options;
        }
        //======================================================================
        $tpl = 'ad/phongban/phongban_list';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Quản lý Phòng ban' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function add() {
        $options = array();
        // Xu ly viec them vao db
        if ($this->is_postback()) {
            if (!$this->_do_add()) {
                $options['error'] = validation_errors();
                $options['options'] = $options;
            }
        }
        //======================================================================
        $options['header'] = 'Thêm Phòng ban';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_COLOR_ADMIN_ADD_URL;
        //======================================================================
        $tpl = 'ad/phongban/phongban_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Thêm Phòng ban' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _do_add() {
        $this->form_validation->set_rules('name', 'Tên phòng ban', 'trim|required|xss_clean|max_length[255]');
        if ($this->form_validation->run()) {
            $data = array(
                'name' => my_trim($this->input->post('name', TRUE)),
                'status' => STATUS_ACTIVE,
                'editor' => $this->phpsession->get('user_id'),
            );
            //==================================================================
            $pb_id = $this->input->post('id');

            if ($pb_id > 0) {
                $data['id'] = $pb_id;
                $this->products_color_model->update($data);
            } else {
                $data['creator'] = $this->phpsession->get('user_id');
                $this->products_color_model->insert($data);
            }
            //==================================================================
            redirect(PRODUCTS_COLOR_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //==========================================================================
    function edit() {
        $options = array();
        if ($this->is_postback()) {
            if (!$this->input->post('from_list')) {
                if (!$this->_do_add()) {
                    $options['error'] = validation_errors();
                    $options['options'] = $options;
                }
            }
        }
        $pb_id = $this->input->post('id', TRUE);
        //======================================================================
        // Lấy phòng ban
        $data_get = array(
            'id' => $pb_id,
            'get_row' => TRUE,
        );
        $pb = $this->products_origin_model->CommonGet(TBL_PHONGBAN, $data_get);
        if (is_object($pb)) {
            $options['pb'] = $pb;
            //==================================================================
            $options['header'] = 'Sửa Phòng ban';
            $options['button_name'] = 'Lưu dữ liệu';
            $options['submit_uri'] = PRODUCTS_COLOR_ADMIN_EDIT_URL;
            //==================================================================
            $tpl = 'ad/phongban/phongban_form_creat';
            $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
            $this->_view_data['title'] = 'Sửa Phòng ban' . DEFAULT_TITLE_SUFFIX;
            $this->load->view($this->_layout, $this->_view_data);
        }
    }

    //==========================================================================

    function delete() {
        if ($this->is_postback() && $this->input->post('from_list')) {
            $id = $this->input->post('id');
            $check1 = $this->products_model->get_products(array('colors' => $id));
            if (empty($check1)) {
                $this->products_color_model->delete($id);
            }
            redirect(PRODUCTS_COLOR_ADMIN_BASE_URL);
        }
    }

}
