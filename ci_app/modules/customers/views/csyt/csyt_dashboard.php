<?php
$ss_csyt_email = $this->phpsession->get('ss_csyt_email');
$ss_csyt_name = $this->phpsession->get('ss_csyt_name');
$ss_csyt_username = $this->phpsession->get('ss_csyt_username');
//end
$cmt_key = isset($_GET['cmt_key']) ? $_GET['cmt_key'] : '';
?>

<form class="form-inline">
    <div class="form-group">
        <label for="exampleInputName2" style="
               float: left;
               font-weight: bold;
               padding: 7px;
               padding-left: 0;
               padding-right: 10px;
               ">Tìm theo CMT - Họ tên:</label>
        <input value="<?php echo $cmt_key ?>" name="cmt_key" type="text" class="form-control" id="exampleInputName2" placeholder="Tìm theo CMT - Họ tên...">
    </div>
    <button type="submit" class="btn btn-primary btn-md mr20">Tìm kiếm</button>
    <?php
    if ($cmt_key != '') {
        ?>
        <a class="btn btn-warning ml20 mr10" href="<?php echo site_url(URL_CSYT_BH) . '?cmt_key=' . $cmt_key ?>">Thông tin bảo hiểm</a>
        <!--<a class="btn btn-info" href="<?php // echo site_url(URL_CSYT_BT) . '?cmt_key=' . $cmt_key ?>">Thông tin bồi thường</a>-->
        <?php
    }
    ?>
</form>

<!--<table id="table-edit_" class="table table-bordered table-hover">
    <form class="form-control form-inline" action="<?php echo site_url(URL_CSYT_DASHBOARD) ?>" method="get">
        <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
            <header class="box-typical-header panel-heading ui-sortable-handle">
                <h3 class="panel-title">
                    <label style="float: left">Tìm theo CMT - Họ tên:</label>
                    <input style="width: 200px;float: left;margin-left: 15px" class="form-control" type="text" name="cmt_key" value="" placeholder="Tìm theo CMT - Họ tên..."/>
                </h3>
            </header>
        </section>
    </form>
</table>-->
<br><br>
<table id="table-edit_" class="table table-bordered table-hover">
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Thông tin cơ sở y tế:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th>Chỉ tiêu</th>
            <th>Giá trị</th>
            <th>Chỉ tiêu</th>
            <th>Giá trị</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Tên cơ sở y tế</td>
            <td><?php echo $ss_csyt_name ?></td>
            <td>Email</td>
            <td><?php echo $ss_csyt_email ?></td>
        </tr>
        <tr>
            <td>Tên đăng nhập</td>
            <td><?php echo $ss_csyt_username ?></td>
        </tr>
    </tbody>
</table>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>