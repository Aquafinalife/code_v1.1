<?php $data = modules::run('advs/get_services'); ?>
<?php if(!empty($data)) { ?>
<div class="homeservices">
    <div class="container">
        <h3><?php echo __('IP_home_services'); ?></h3>
        <div class="row">
            <?php foreach($data as $key => $value){
                $url_path = ($value->url_path === "" || $value->url_path === "#") ? "" : $value->url_path;
                $image = isset($value->image_name)?base_url() . ADVS_IMAGE_URL . $value->image_name:base_url().'images/no-image.png';
                $value_title = limit_text($value->title, 100);
                $value_summary = limit_text($value->summary, 175);
            ?>
            <div class="col-sm-4 hs_item">
                <a class="hs_image" <?php if($url_path){ ?> href="<?php echo $url_path; ?>" <?php } ?> title="<?php echo $value->title; ?>">
                    <img alt="" src="<?php echo $image; ?>" title="<?php echo $value->title; ?>" >
                </a>
                <a class="hs_title" <?php if($url_path){ ?> href="<?php echo $url_path; ?>" <?php } ?> title="<?php echo $value->title; ?>">
                    <?php echo $value_title; ?>
                </a>
                <p class="hs_summary"><?php echo $value_summary; ?></p>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>