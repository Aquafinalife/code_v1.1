<?php

class Products_Style_Model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    //==========================================================================
    function get_csyt($options = array()) {

        $this->get_data_options_csyt($options);

        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS_STYLE, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_PRODUCTS_STYLE)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset'])) {
                $this->db->limit($options['limit'], $options['offset']);
            } else if (isset($options['limit'])) {
                $this->db->limit($options['limit']);
            }

            return $q = $this->db->get(TBL_PRODUCTS_STYLE)->result();
//            $q = $this->db->get(TBL_PRODUCTS_STYLE)->result();
//            $a = $this->db->last_query();
//            echo '<pre>';
//            print_r($a);
//            die;
        }

        $q->free_result();
    }

    //==========================================================================
    function get_data_options_csyt($options = array()) {

        $select = '' . TBL_PRODUCTS_STYLE . '.id,
                    ' . TBL_PRODUCTS_STYLE . '.name,
                    ' . TBL_PRODUCTS_STYLE . '.name_en,
                    ' . TBL_PRODUCTS_STYLE . '.csyt_code,
                    ' . TBL_PRODUCTS_STYLE . '.username,
                    ' . TBL_PRODUCTS_STYLE . '.email,
                    ' . TBL_PRODUCTS_STYLE . '.password,
                    ' . TBL_PRODUCTS_STYLE . '.password_fm,
                    ' . TBL_PRODUCTS_STYLE . '.parent_id,';

        if (isset($options['join_' . TBL_CUSTOMERS . '_mbh']) || isset($options['join_' . TBL_CUSTOMERS . '_dbh'])) {
            $select .= '' . TBL_CUSTOMERS . '.id as KH_id,
                        ' . TBL_CUSTOMERS . '.congty_canhan,
                        ' . TBL_CUSTOMERS . '.mst_cmt,
                        ' . TBL_CUSTOMERS . '.ngaysinh,
                        ' . TBL_CUSTOMERS . '.didong,
                        ' . TBL_CUSTOMERS . '.email,
                        ' . TBL_CUSTOMERS . '.parent_id,
                        ' . TBL_CUSTOMERS . '.phongban,
                        ' . TBL_CUSTOMERS . '.ndbh_quanhe,';
        }

        $this->db->select($select);
        //======================================================================
        // Join
        if (isset($options['join_' . TBL_CUSTOMERS . '_mbh'])) {
            $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS_STYLE . '.kh_mbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        //end
        //======================================================================
        // WHERE
        if (isset($options['username'])) {
            $this->db->where(TBL_PRODUCTS_STYLE . '.username', $options['username']);
        }
        if (isset($options['email'])) {
            $this->db->where(TBL_PRODUCTS_STYLE . '.email', $options['email']);
        }
        if (isset($options['password'])) {
            $this->db->where(TBL_PRODUCTS_STYLE . '.password', $options['password']);
        }
        //end
    }

    //==========================================================================
    // count csyt by options
    function count_csyt($options = array()) {
        $this->get_data_options_csyt($options);
        return $q = $this->db->count_all_results(TBL_PRODUCTS_STYLE);
//        $q = $this->db->get(TBL_ORDERS)->result();
//        $a = $this->db->last_query();
//        echo '<pre>';
//        print_r($a);
//        die;

        $q->free_result();
    }

    //==========================================================================
    public function get_products_style($options = array()) {
        if (isset($options['username'])) {
            $this->db->where('username', $options['username']);
        }
        if (isset($options['password'])) {
            $this->db->where('password', $options['password']);
        }
        if (isset($options['password_fm'])) {
            $this->db->where('password_fm', $options['password_fm']);
        }

        if (isset($options['id']))
            $this->db->where('id', $options['id']);

        if (isset($options['name']))
            $this->db->where('name', $options['name']);

        if (isset($options['status']))
            $this->db->where('status', $options['status']);

        $this->db->order_by('id');

        $query = $this->db->get('products_style');
        if ($query->num_rows() > 0) {
            if (isset($options['id']) || isset($options['onehit']) || isset($options['get_row'])) {
                return $query->row(0);
            }

            if (isset($options['last_row']))
                return $query->last_row();

            return $query->result();
        }else {
            return NULL;
        }
    }

    public function get_products_style_count($options = array()) {
        return count($this->get_products_style($options));
    }

    public function get_products_style_combo($options = array()) {
        // Default categories name
        if (!isset($options['combo_name'])) {
            $options['combo_name'] = 'products_style';
        }
        if (!isset($options['extra'])) {
            $options['extra'] = '';
        }
        if (!isset($options['is_add_edit_menu'])) {
            if (isset($options['is_add_edit_cat']))
                $categories[ROOT_CATEGORY_ID] = 'Tất cả';
            else
                $categories[DEFAULT_COMBO_VALUE] = ' - Chọn';
        }

//        $categories = array();

        $this->db->where('status', STATUS_ACTIVE);
        $data = $this->db->get('products_style');
        if ($data->num_rows() > 0) {
            $data = $data->result_array();
            foreach ($data as $id => $value) {
                $id = $value['id'];
                $categories[$id] = $value['name'];
            }
            if (!isset($options[$options['combo_name']]))
                $options[$options['combo_name']] = DEFAULT_COMBO_VALUE;
            return form_dropdown($options['combo_name'], $categories, $options[$options['combo_name']], $options['extra']);
        }else {
            return NULL;
        }
    }

}
