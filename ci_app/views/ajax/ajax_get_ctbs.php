<div class="row mb15">
<!--    <h3 class="main_header">Chứng từ hồ sơ bồi thường <span class="fr mr20 ml15">Không</span><span class="fr mr20">Có</span></h3>-->
    <h3 class="main_header">Chứng từ hồ sơ bồi thường chờ bổ sung</h3>
    <div class="col-sm-10 col-xs-10">
        <ul class="list_hsbt_bs">
            <?php
            $a_ctbs = array();
            if (isset($ctbs) && $ctbs != '') {
                $a_ctbs = explode(',', $ctbs);
            }
            //==================================================================
            if (isset($ct) && !empty($ct)) {
                $i=1;
                foreach ($ct as $index) {
                    $ct_id = $index->id;
                    $ct_name = $index->name;

                    if (in_array($ct_id, $a_ctbs)) {
                        ?>
                        <li>
                            <?php echo $i?>. <?php echo $ct_name?>
                        </li>
                        <?php
                        $i++;
                    }
                }
            }
            ?>
        </ul>
    </div>
<!--    <div class="col-sm-2 col-xs-2">
        <ul class="list_hsbt_bs">
            <li>
                <span class="glyphicon glyphicon-ok text-success"></span>
                <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
            </li>
            <li>
                <span class="glyphicon glyphicon-ok text-success"></span>
                <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
            </li>
            <li>
                <span class="glyphicon glyphicon-ok text-success"></span>
                <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
            </li>
            <li>
                <span class="glyphicon glyphicon-ok text-success"></span>
                <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
            </li>
            <li>
                <span class="glyphicon glyphicon-ok text-success"></span>
                <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
            </li>
            <li>
                <span class="glyphicon glyphicon-ok text-success"></span>
                <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
            </li>
            <li>
                <span class="glyphicon glyphicon-ok text-success"></span>
                <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
            </li>
            <li>
                <span class="glyphicon glyphicon-ok text-success"></span>
                <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
            </li>
        </ul>
    </div>-->
</div>