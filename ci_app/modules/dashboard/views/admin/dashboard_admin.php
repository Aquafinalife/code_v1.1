<div class="page_header" style="padding: 100px">
    <div class="row">
        <div class="col-sm-4 mt20">
            <a target="_blank" href="<?php echo site_url('dashboard/products/size/add') ?>" class="btn btn-primary btn-lg">Thêm Gói Bảo Hiểm</a>
        </div>
        <div class="col-sm-4 mt20">
            <a target="_blank" href="<?php echo site_url('dashboard/products/origin/add') ?>" class="btn btn-primary btn-lg">Thêm Mã Bệnh</a>
        </div>
        <div class="col-sm-4 mt20">
            <a target="_blank" href="<?php echo site_url('dashboard/products/trademark/add') ?>" class="btn btn-primary btn-lg">Nhà Cung Cấp Bảo Hiểm</a>
        </div>

        <div class="col-sm-4 mt20">
            <a target="_blank" href="<?php echo site_url('dashboard/products/add') ?>" class="btn btn-danger btn-lg">Thêm Đơn Bảo Hiểm</a>
        </div>
        <div class="col-sm-4 mt20">
            <a target="_blank" href="<?php echo site_url('dashboard/products/coupon/add') ?>" class="btn btn-danger btn-lg">Thêm Chứng Từ</a>
        </div>
        <div class="col-sm-4 mt20">
            <a target="_blank" href="<?php echo site_url('dashboard/customers/add-kh-cn') ?>" class="btn btn-danger btn-lg">Thêm Khách Hàng Cá Nhân</a>
        </div>

        <div class="col-sm-4 mt20">
            <a target="_blank" href="<?php echo site_url('dashboard/orders/add') ?>" class="btn btn-success btn-lg">Thêm Đơn Bồi Thường</a>
        </div>

        <div class="col-sm-4 mt20">
            <a target="_blank" href="<?php echo site_url('dashboard/products/style/add') ?>" class="btn btn-success btn-lg">Thêm Cơ Sở Y Tế</a>
        </div>

        <div class="col-sm-4 mt20">
            <a target="_blank" href="<?php echo site_url('dashboard/customers/add-kh-dn') ?>" class="btn btn-success btn-lg">Thêm Khách Hàng Doanh Nghiệp</a>
        </div>
    </div>
</div>
