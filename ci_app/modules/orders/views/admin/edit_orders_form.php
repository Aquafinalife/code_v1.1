<?php
echo form_open($submit_uri);
if (isset($id))
    echo form_hidden('id', $id);
$fullname = isset($fullname) ? $fullname : '';
$email = isset($email) ? $email : '';
$created_date = isset($created_date) ? $created_date : '';
$submit_uri = isset($submit_uri) ? $submit_uri : '';
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'orders_cat');
?>

<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Nội dung bồi thường"</small>
    <span class="fright"><a class="button close" href="<?php echo ORDER_ADMIN_BASE_URL; ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>

<?php
$a_status_bt = get_a_orders_status();
$disabled = 'disabled = true';
$disabled_bt = $disabled;
$a_ct_selected= array();
if (isset($bt) && is_object($bt)) {
    $id = $bt->id;
    $so_tien_ycbt = $bt->so_tien_ycbt;
    $so_tien_dbt = $bt->so_tien_dbt;
    $so_ngay_ycbt = $bt->so_ngay_ycbt;
    $ngay_ycbt = substr($bt->ngay_ycbt, 0, 10);
    $message = $bt->message;
    $loai_benh = $bt->loai_benh;
    $loai_benh_txt = get_ten_qlbt($loai_benh);
    $status = $bt->orders_status;
    if ($status == BT_CHODUYET) {
        $disabled_bt = '';
    }
    $chungtu_bosung= $bt->chungtu_bosung;
    if($chungtu_bosung!=''){
        $a_ct_selected= explode(',', $chungtu_bosung);
    }
    $chungtu_bosung_txt= $bt->chungtu_bosung_txt;
}
echo form_hidden('bt_id', $id);
$loai_benh_txt = isset($loai_benh_txt) ? $loai_benh_txt : '';

//end
if (isset($bh) && is_object($bh)) {
    $congty_canhan = $bh->congty_canhan;
    $mst_cmt = $bh->mst_cmt;
    $goi_bh_dm = $bh->goi_bh_dm;
    ?>
    <div class="form_content">
        <?php $this->load->view('powercms/message'); ?>
        <ul class="tabs">
            <li><a href="#tab1">Thông tin người được bảo hiểm</a></li>
            <!--<li><a href="#tab2">Quyền lợi bảo hiểm tai nạn</a></li>-->
            <li><a href="#tab3">Tình trạng bồi thường</a></li>
        </ul>
        <div class="tab_container">
            <div id="tab1" class="tab_content">
                <div class="col-sm-6 col-xs-12">
                    <table>
                        <tr style="display: none;"><td class="title">Ngôn ngữ: </td></tr>
                        <tr style="display: none;">
                            <td><?php if (isset($lang_combobox)) echo $lang_combobox; ?></td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Người được bồi thường:</td>
                            <td class="fl"><?php echo $congty_canhan ?></td>
                        </tr>
                        <tr>
                            <td class="title fl w250">CMT/Hộ chiếu:</td>
                            <td class="fl"><?php echo $mst_cmt ?></td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Gói bảo hiểm mua:</td>
                            <td class="fl"><?php echo $goi_bh_dm ?></td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Loại bệnh:</td>
                            <td class="fl"><?php echo $loai_benh_txt ?></td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Số tiền yêu cầu bồi thường:</td>
                            <td class="fl">
                                <input <?php echo $disabled_bt ?> type="text" name="so_tien_ycbt" value="<?php echo $so_tien_ycbt ?>"/>
                                <?php // echo form_input(array('disabled'=>'true','name' => 'so_tien_ycbt', 'size' => '50', 'class' => 'price_bt', 'maxlength' => '255', 'value' => isset($so_tien_ycbt) ? $so_tien_ycbt : set_value('so_tien_ycbt'))); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Số ngày yêu cầu bồi thường:</td>
                            <td class="fl">
                                <input <?php echo $disabled_bt ?> type="text" name="so_ngay_ycbt" value="<?php echo $so_ngay_ycbt ?>"/>
                                <?php // echo form_input(array('disabled'=>'false','name' => 'so_ngay_ycbt', 'size' => '50', 'class' => '', 'maxlength' => '255', 'style' => 'width:_560px;', 'value' => isset($so_ngay_ycbt) ? $so_ngay_ycbt : set_value('so_ngay_ycbt'))); ?>
                            </td>
                        </tr>
                        <tr><td><i style="font-size: 13px;">(Chỉ điền khi quyền lợi yêu cầu số ngày)</i></td></tr>
                        <tr>
                            <td class="title fl w250">Ngày yêu cầu bồi thường:</td>
                            <td class="fl">
                                <input <?php echo $disabled_bt ?> class="show_date" type="text" name="ngay_ycbt" value="<?php echo $ngay_ycbt ?>"/>
                                <?php // echo form_input(array('name' => 'ngay_ycbt', 'size' => '50', 'class' => 'show_date', 'maxlength' => '255', 'style' => 'width:_560px;', 'value' => isset($ngay_ycbt) ? $ngay_ycbt : set_value('ngay_ycbt'))); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="title fl w250">Số tiền chấp nhận bồi thường:</td>
                            <td class="fl">
                                <?php echo form_input(array('name' => 'so_tien_dbt', 'size' => '50', 'class' => '', 'maxlength' => '255', 'style' => 'width:_560px;', 'value' => isset($so_tien_dbt) ? $so_tien_dbt : set_value('so_tien_dbt'))); ?>
                            </td>
                        </tr>

                        <tr><td class="title" style="vertical-align: top">Ghi chú:</td></tr>
                        <tr>
                            <td>
                                <?php echo form_textarea(array('id' => 'message', 'name' => 'message', 'style' => 'width:560px; height: 100px;', 'value' => isset($message) ? $message : set_value('message'))); ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <table>
                        <tr>
                            <td id="ket_qua">
                                <?php
                                echo '<p class="bg-primary">Giá trị giới hạn gói bảo hiểm không vượt quá: ' . $st_gh_bh . '</p>'
                                . '<p class="bg-success">Đã sử dụng:' . $st_dbt . '</p>'
                                . '<p class="bg-info">Còn lại trước khi yêu cầu bồi thường: ' . $stbh_cl . '</p>'
                                . '<p class="bg-warning">Ước tính số tiền còn lại nếu áp dụng số tiền yêu cầu bồi thường: ' . $thong_qua . '</p>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <!--<td><button type="button" class="btn btn-danger check_bt">Kiểm tra</button></td>-->
                        </tr>
                    </table>
                </div>
                <div class="clear-both"></div>
                <div class="col-sm-12">
                    <table>
                        <tr>
                            <td >
                                <div id="bang_quyen_loi">
                                    <?php
                                    if (isset($bh_html)) {
                                        echo $bh_html;
                                    }
                                    ?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
            <div id="tab2" class="tab_content">
                <table class="list" style="width: 100%; margin-bottom: 10px;">
                    <tr>
                        <th class="left" style="width: 5%">MÃ ĐH</th>
                        <th class="left" style="width: 5%">MÃ SP</th>
                        <th class="left" style="width: 40%">SẢN PHẨM</th>
                        <th class="left" style="width: 10%">KÍCH CỠ</th>
                        <th class="left" style="width: 10%">SỐ LƯỢNG</th>
                        <th class="left" style="width: 10%">GIÁ BÁN</th>
                        <th class="left" style="width: 10%">TỔNG</th>
                    </tr>
                    <?php
                    if (isset($order_detail)) {
                        $stt = 0;
                        $amount = 0;
                        foreach ($order_detail as $index => $orders):
                            $style = $stt++ % 2 == 0 ? 'even' : 'odd';
                            $price = $orders->price != 0 ? get_price_in_vnd($orders->price) . ' VND' : get_price_in_vnd($orders->price);
                            $tong = $orders->price * $orders->quantity;
                            $total = $tong != 0 ? get_price_in_vnd($tong) . ' VND' : get_price_in_vnd($tong);
                            $amount = $amount + $tong;
                            ?>
                            <tr class="<?php echo $style ?>">
                                <td><?php echo '#' . $orders->order_id; ?></td>
                                <td><?php echo '#' . $orders->product_id ?></td>
                                <td style="white-space:nowrap;"><?php echo $orders->product_name; ?></td>
                                <td style="white-space:nowrap;"><?php echo $orders->size; ?></td>
                                <td style="white-space:nowrap;"><?php echo $orders->quantity; ?></td>
                                <td style="white-space:nowrap;color: red;text-align: right;"><?php echo $price; ?></td>
                                <td style="white-space:nowrap;color: red;text-align: right;"><?php echo $total; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="odd">
                            <?php $amounts = $amount != 0 ? get_price_in_vnd($amount) . ' VND' : get_price_in_vnd($amount); ?>
                            <td class="right" colspan="6" style="white-space:nowrap;"><b>Tổng giá trị đơn hàng</b></td>
                            <td class="right" style="white-space:nowrap;color: red;"><?php echo $amounts; ?></td>
                        </tr>
                        <tr class="odd">
                            <td colspan="7" style="text-align: right; color: red;font-weight: bold" class="total_price"><?php echo DocTienBangChu($amount); ?> </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <div id="tab3" class="tab_content">
                <table>
                    <tr><td class="title">Tình trạng bồi thường: </td></tr>
                    <tr>
                        <td id="category">
                            <select name="orders_status" class="form-control">
                                <?php
                                if (isset($a_status_bt) && !empty($a_status_bt)) {
                                    foreach ($a_status_bt as $k => $v) {
//                                        $selected= ($status== $k)?'selected=""';
                                        ?>
                                <option value="<?php echo $k?>"><?php echo $v?></option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr><td class="title">Xác nhận thay đổi số tiền còn lại: </td></tr>
                    <tr><td><i style="font-size: 13px;">(Chỉ được tích khi xác nhận chính xác)</i></td></tr>
                    <tr>
                        <td id="category"><label> <input type="checkbox" name="xac_nhan" value="1"> Xác nhận </label></td>
                    </tr>
                </table>
            </div>
        </div>
        <br class="clear"/>
        <div style="margin-top: 10px;"></div>
        <?php echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn')); ?>
        <br class="clear"/>&nbsp;
    </div>
    <?php
}
?>

<?php echo form_close(); ?>
