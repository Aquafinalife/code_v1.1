<?php

class Customers extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->_layout = 'layout/custom_layout';
        $this->_view_data = array();
        //modules::run('auth/auth/check_kh_dangnhap1');
        $this->kh_check_login();
    }

    //==========================================================================
    function get_kh_profile($options = array()) {

        $data = array();

        $tpl = 'setting/kh_profile';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Profile' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function csyt_dashboard($offset = 0, $options = array()) {

        $data = array();
        //$data['scripts'] = $this->scripts();
        // check ss csyt
        $ss_csyt_id = $this->phpsession->get('ss_csyt_id');

        $tpl = 'csyt/csyt_dashboard';

        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    // Lấy thông tin bh cn 
    function get_csyt_bh($offset = 0, $options = array()) {

        $ss_csyt_id = $this->phpsession->get('ss_csyt_id');

        $cmt_key = isset($_GET['cmt_key']) ? $_GET['cmt_key'] : '';

        if ($cmt_key != '') {
            // Lấy thông tin bh
            $data_get = array(
                'mst_cmt' => $cmt_key,
                'phanloai_kh' => CANHAN,
                //'limit' => $limit,
                //'join_' . TBL_ORDERS => TRUE,
                'join_' . TBL_CUSTOMERS . '_dbh' => TRUE,
                'join_' . TBL_PRODUCTS_SIZE => TRUE,
                //'join_' . TBL_PRODUCTS_STYLE => TRUE,
                'join_' . TBL_DN_BBH => TRUE,
            );
            // Lấy thông tin bh
            $bh = $this->customers_model->get_bh($data_get);
            if (!empty($bh)) {
                $data['bh'] = $bh;
            }
            //end
        } else {
            //redirect(URL_CSYT_DASHBOARD);
        }
        //======================================================================
        $tpl = 'csyt/csyt_bh';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    // Lấy thông tin bt kh là cá nhân
    function get_csyt_bt($offset = 0, $options = array()) {
        $this->check_csyt_dangnhap();
        $data = array();
        // check ss kh
        $ss_csyt_id = $this->phpsession->get('ss_csyt_id');

        $cmt_key = isset($_GET['cmt_key']) ? $_GET['cmt_key'] : '';

        if ($cmt_key != '') {
            $limit = LIMIT_DEFAULT;
            $data_get = array(
                'mst_cmt' => $cmt_key,
                //'limit' => $limit,
                'join_' . TBL_PRODUCTS => TRUE,
                'join_' . TBL_CUSTOMERS => TRUE,
                'join_' . TBL_PRODUCTS_SIZE => TRUE,
                'join_' . TBL_PRODUCTS_STYLE => TRUE,
                    //'join_' . TBL_DN_BBH => TRUE,
            );
            // Filter
            $plbt = isset($_GET['plbt']) ? $_GET['plbt'] : '';
            if ($plbt > 0 && $plbt != '') {
                $data_get['phanloai_bt'] = $plbt;
            }
            $ma_benh = isset($_GET['ma_benh']) ? $_GET['ma_benh'] : '';
            if ($ma_benh > 0 && $ma_benh != '') {
                $data_get['ma_benh'] = $ma_benh;
            }
            // end
            // Đếm tổng số bt
            $total_row = $this->customers_model->count_bt_by_options($data_get);

            //end
            // pagination
            $paging_config = array(
                'base_url' => site_url() . $this->uri->segment(1),
                'total_rows' => $total_row,
                'per_page' => $limit,
                'uri_segment' => 2,
                'use_page_numbers' => TRUE,
                'first_link' => __('IP_paging_first'),
                'last_link' => __('IP_paging_last'),
                'num_links' => 1,
            );
            if (isset($_GET) && $_GET != '') {
                $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
            }
            $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);
            $this->pagination->initialize($paging_config);
            $data_get['offset'] = ($offset > 0) ? ($offset - 1) * ($limit) : $offset;
            $data_get['limit'] = $limit;
            //end        
            $bt = $this->customers_model->get_bt_by_options($data_get);
            if (!empty($bt)) {
                $data['bt'] = $bt;
            }
            //end
            // Lấy thông tin dữ liệu bt
            $data_bt = $this->get_data_bt();
            $data = array_merge($data_bt, $data);
            //end
        }
        $data['scripts'] = $this->scripts_select2();
        //end        
        $tpl = 'csyt/csyt_bt';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end

    function bbh_dashboard($offset = 0, $options = array()) {

        $data = array();
        //$data['scripts'] = $this->scripts();
        // check ss dn bbh
        $ss_bbh_id = $this->phpsession->get('ss_bbh_id');

        $tpl = 'bbh/bbh_dashboard';

        // Lấy các gói bh
        $data_get = array(
            'dn_bbh' => $ss_bbh_id,
        );
        //$goi_bh = $this->products_size_model->get_products_size($data_get);
        $goi_bh = array();
        if (!empty($goi_bh)) {
            $data['goi_bh'] = $goi_bh;

            $str_goi_bh_id = '';
            foreach ($goi_bh as $index) {
                $id = $index->id;
                $str_goi_bh_id .= $id . ',';
            }
            $str_goi_bh_id .= 0;
            $a_goi_bh_id = array_unique(@explode(',', $str_goi_bh_id));
            $data['a_goi_bh'] = $a_goi_bh_id;
        }
        //end
        // Lấy thông tin kh dbh
        $data_get = array(
            //'dn_bbh' => $ss_bbh_id,
            'dn_bbh' => $ss_bbh_id,
            //'limit' => $limit,
            //'join_' . TBL_ORDERS => TRUE,
            'join_' . TBL_CUSTOMERS . '_dbh' => TRUE,
            'join_' . TBL_PRODUCTS_SIZE => TRUE,
        );
        $bh = $this->customers_model->get_bh($data_get);

        if (!empty($bh)) {
            $data['bh'] = $bh;
            $str_kh_dbh_id = '';
            foreach ($bh as $index) {
                $kh_dbh_id = $index->kh_dbh;
                $str_kh_dbh_id .= $kh_dbh_id . ',';
            }
            $str_kh_dbh_id .= 0;
            $a_kh_dbh_id = array_unique(@explode(',', $str_kh_dbh_id));

            // Lấy thông tin bồi thường
            $data_get = array(
//                //'a_kh_dbh' => $a_kh_dbh_id,
//                'dn_bbh' => $ss_bbh_id,
//                'join_' . TBL_PRODUCTS => TRUE,
//                'join_' . TBL_CUSTOMERS => TRUE,
//                    //'join_' . TBL_PRODUCTS_SIZE => TRUE,
//                    //'join_' . TBL_PRODUCTS_STYLE => TRUE,
//                    //'join_' . TBL_DN_BBH => TRUE,
                //end
                'dn_bbh' => $ss_bbh_id,
                //'limit' => $limit,
                'join_' . TBL_PRODUCTS => TRUE,
                'join_' . TBL_CUSTOMERS => TRUE,
                'join_' . TBL_PRODUCTS_SIZE => TRUE,
                'join_' . TBL_PRODUCTS_STYLE => TRUE,
            );
            $bt = $this->customers_model->get_bt_by_options($data_get);
            if (!empty($bt)) {
                $data['bt'] = $bt;
            }
            //end        
            // Lấy thông tin dn phòng ban
            $data_dn_pb = $this->get_data_dn_pb();
            $data = array_merge($data, $data_dn_pb);
            //end
        }
        //end

        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    // Lấy thông tin bbh bh
    function get_bbh_bh($offset = 0, $options = array()) {
        $data = array();
        // check ss dn bbh
        $ss_bbh_id = $this->phpsession->get('ss_bbh_id');
        // end        
        // Lấy các gói bh
        $data_get = array(
            'dn_bbh' => $ss_bbh_id,
        );
        $goi_bh = $this->products_size_model->get_products_size($data_get);
        if (!empty($goi_bh)) {
            $data['goi_bh'] = $goi_bh;
        }
        //end
        // Lấy thông tin bh
        $limit = LIMIT_DEFAULT;
        $data_get = array(
            'dn_bbh' => $ss_bbh_id,
            //'limit' => $limit,
            //'join_' . TBL_ORDERS => TRUE,
            'join_' . TBL_CUSTOMERS . '_dbh' => TRUE,
            'join_' . TBL_PRODUCTS_SIZE => TRUE,
                //'join_' . TBL_PRODUCTS_STYLE => TRUE,
                //'join_' . TBL_DN_BBH => TRUE,
        );
        //end
        // Lấy tất cả bh
        $bh_all = $this->customers_model->get_bh($data_get);
//        echo '<pre>';
//        print_r($bh_all);
//        die;
        if (!empty($bh_all)) {
            $data['bh_all'] = $bh_all;
        }
        // Filter
        //$data_filter = $this->get_data_filter_bt();
        $kh_dbh = isset($_GET['kh_dbh']) ? $_GET['kh_dbh'] : '';
        if ($kh_dbh != '' && $kh_dbh > 0) {
            $data_get['kh_dbh'] = $kh_dbh;
        }
        $cn = isset($_GET['cn']) ? $_GET['cn'] : '';
        if ($cn != '' && $cn > 0) {
            $data_get['kh_mbh'] = $cn;
            // Xóa array kh mbh
            unset($data_get['a_kh_mbh']);
        }
        $pb = isset($_GET['pb']) ? $_GET['pb'] : '';
        if ($pb != '' && $pb > 0) {
            $data_get['pb'] = $pb;
        }
        $goi_bh = isset($_GET['goi_bh']) ? $_GET['goi_bh'] : '';
        if ($goi_bh != '' && $goi_bh > 0) {
            $data_get['goi_bh'] = $goi_bh;
        }
        $data_get['url_export'] = '?kh_dbh=' . $kh_dbh . '&goi_bh=' . $goi_bh . '&cn=' . $cn . '&pb=' . $pb . '';
        //$data_get = array_merge($data_get, $data_filter);
        // end 
        $total_row = $this->customers_model->count_bh($data_get);
        //end
        // pagination
        $paging_config = array(
            'base_url' => site_url() . $this->uri->segment(1),
            'total_rows' => $total_row,
            'per_page' => $limit,
            'uri_segment' => 2,
            'use_page_numbers' => TRUE,
            'first_link' => __('IP_paging_first'),
            'last_link' => __('IP_paging_last'),
            'num_links' => 1,
        );
        if (isset($_GET) && $_GET != '') {
            $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($paging_config);
        $data_get['offset'] = ($offset > 0) ? ($offset - 1) * ($limit) : $offset;
        $data_get['limit'] = $limit;
        // Lấy thông tin bh
        $bh = $this->customers_model->get_bh($data_get);
        if (!empty($bh)) {
            $data['bh'] = $bh;
        }
        //end
        $data['url_export'] = $data_get['url_export'];
        $data['scripts'] = $this->scripts_select2();
        $tpl = 'bbh/bbh_bh';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    // Lấy thông tin bt bbh
    function get_bbh_bt($offset = 0, $options = array()) {
        $data = array();
        // check ss kh
        $ss_bbh_id = $this->phpsession->get('ss_bbh_id');
        // Lấy thông tin dn phòng ban
        $data_dn_pb = $this->get_data_dn_pb();
        $data = array_merge($data, $data_dn_pb);
        //end
        // Lấy thông tin bồi thường
        $limit = 15;
        $data_get = array(
            'dn_bbh' => $ss_bbh_id,
            //'limit' => $limit,
            'join_' . TBL_PRODUCTS => TRUE,
            'join_' . TBL_CUSTOMERS => TRUE,
            'join_' . TBL_PRODUCTS_SIZE => TRUE,
            'join_' . TBL_PRODUCTS_STYLE => TRUE,
                //'join_' . TBL_DN_BBH => TRUE,
        );
        // Lấy toàn bộ thông tin bt
        $bt_all = $this->customers_model->get_bt_by_options($data_get);
        if (!empty($bt_all)) {
            $data['bt_all'] = $bt_all;
        }
        //end
        // Filter
        $data_filter = $this->get_data_filter_bt();
        $data_get = array_merge($data_get, $data_filter);
        // end        
        // Đếm tổng số bt
        $total_row = $this->customers_model->count_bt_by_options($data_get);

        //end
        // pagination
        $paging_config = array(
            'base_url' => site_url() . $this->uri->segment(1),
            'total_rows' => $total_row,
            'per_page' => $limit,
            'uri_segment' => 2,
            'use_page_numbers' => TRUE,
            'first_link' => __('IP_paging_first'),
            'last_link' => __('IP_paging_last'),
            'num_links' => 1,
        );
        if (isset($_GET) && $_GET != '') {
            $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($paging_config);
        $data_get['offset'] = ($offset > 0) ? ($offset - 1) * ($limit) : $offset;
        $data_get['limit'] = $limit;
        //end       
        $bt = $this->customers_model->get_bt_by_options($data_get);
//        echo '<pre>';
//        print_r($bt);
//        die;
        if (!empty($bt)) {
            $data['bt'] = $bt;
        }
        //end
        // Lấy thông tin dữ liệu bt
        $data_get = array(
            'dn_bbh' => $ss_bbh_id,
        );
        $data_bt = $this->get_data_bt($data_get);
        $data = array_merge($data_bt, $data);
        //end
        $data['url_export'] = $data_get['url_export'];
        //$tpl = 'boithuong/boithuong_kh_dn';
        $tpl = 'bbh/bbh_bt';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    function quan_tri_khach_hang($offset = 0, $options = array()) {
        $data = array();
        //$data['scripts'] = $this->scripts();
        // check ss kh
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');

        if ($ss_kh_id > 0 && $ss_kh_id != '') {
            if ($ss_kh_phanloai_kh == CANHAN) {
                $tpl = 'dashboard/dashboard_kh_cn';
            } else {
                $tpl = 'dashboard/dashboard_kh_dn';
            }
        } else {
            $tpl = 'dashboard/dashboard_kh_cn';
        }
        // end
        // =====================================================================
        // Lấy thông tin dn phòng ban
        $data_dn_pb = $this->get_data_dn_pb();
        $data = array_merge($data, $data_dn_pb);
        //end
        //======================================================================
        // Lấy thông tin bồi thường
        if ($ss_kh_phanloai_kh == CANHAN) {
            $data_get = array(
                'OD_kh_dbh' => $ss_kh_id,
                //'join_' . TBL_ORDERS => TRUE,
                'join_' . TBL_PRODUCTS => TRUE,
                'join_' . TBL_CUSTOMERS => TRUE,
                'join_' . TBL_PRODUCTS_SIZE => TRUE,
                'join_' . TBL_PRODUCTS_STYLE => TRUE,
                'join_' . TBL_DN_BBH => TRUE,
            );
            //$bt = $this->orders_model->get_orders_by_options($data_get);
            //$bt = $this->customers_model->get_bt($data_get);
            $bt = $this->customers_model->get_bt_by_options($data_get);
        } else {
            $data_get = array(
                'kh_mbh' => $ss_kh_id,
                //'join_' . TBL_ORDERS => TRUE,
                'join_' . TBL_PRODUCTS => TRUE,
                'join_' . TBL_CUSTOMERS => TRUE,
                'join_' . TBL_PRODUCTS_SIZE => TRUE,
                'join_' . TBL_PRODUCTS_STYLE => TRUE,
                'join_' . TBL_DN_BBH => TRUE,
            );
            $bt = $this->customers_model->get_bt_by_options($data_get);
        }
        if (!empty($bt)) {
            $data['bt'] = $bt;
        }
        //end       
        //====================================================================== 
        // Lấy thông tin kh
        $kh = $this->customers_model->get_kh_by_options();
        if (!empty($kh)) {
            $data['kh'] = $kh;
        }
        //end 
        //======================================================================
        // Lấy mã bệnh
        $ma_benh = $this->products_origin_model->get_products_origin();
        if (!empty($ma_benh)) {
            $data['a_ma_benh'] = $ma_benh;
        }
        //======================================================================
        //======================================================================
        // Lấy gói BH
        $data_get = array(
            'where' => array(
                'dn_mbh' => $ss_kh_id,
            )
        );
        $goi_bh = $this->customers_model->CommonGet(TBL_PRODUCTS_SIZE, $data_get);
        if (!empty($goi_bh)) {
            $data['a_goi_bh'] = $goi_bh;
        }
        //======================================================================
        // Lấy kh dbh của dn
        $data_get = array(
            'where' => array(
                'kh_mbh' => $ss_kh_id,
            ),
        );
        $kh_dbh = $this->customers_model->CommonGet(TBL_PRODUCTS, $data_get);
        if (!empty($kh_dbh)) {
            $data['a_kh_dbh'] = $kh_dbh;
        }
        //======================================================================
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    // Lấy thông tin bt kh là cá nhân
    function get_bt_cn($offset = 0, $options = array()) {
        $this->check_kh_dang_nhap();
        $data = array();
        // check ss kh
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        $ss_kh_parent_id = $this->phpsession->get('ss_kh_parent_id');
        $ss_kh_phongban = $this->phpsession->get('ss_kh_phongban');
        //======================================================================
        // nếu kh là dn
        if ($ss_kh_phanloai_kh == DOANHNGHIEP) {
            redirect(URL_BT_DN);
        }
        // end
        //======================================================================
        if ($ss_kh_parent_id > 0) {
            // Lấy thông tin dn phòng ban
            $data_dn_pb = $this->get_data_dn_pb();
            $data = array_merge($data, $data_dn_pb);
        }
        //end
        //======================================================================
        $tpl = 'boithuong/boithuong_kh_cn';
        // Lấy thông tin bồi thường
        $limit = LIMIT_DEFAULT;
//        $data_get = array(
//            'OD_kh_dbh' => $ss_kh_id,
//            'join_' . TBL_ORDERS => TRUE,
//            'join_' . TBL_CUSTOMERS => TRUE,
//            'join_' . TBL_PRODUCTS_SIZE => TRUE,
//            'join_' . TBL_PRODUCTS_STYLE => TRUE,
//            'join_' . TBL_DN_BBH => TRUE,
//        );
//        // Lấy toàn bộ thông tin bt
//        $bt_all = $this->customers_model->get_bt($data_get);
//        if (!empty($bt_all)) {
//            $data['bt_all'] = $bt_all;
//        }
        //======================================================================
        $data_get = array(
            'OD_kh_dbh' => $ss_kh_id,
            'join_' . TBL_PRODUCTS => TRUE,
            'join_' . TBL_CUSTOMERS => TRUE,
            'join_' . TBL_PRODUCTS_SIZE => TRUE,
            'join_' . TBL_PRODUCTS_STYLE => TRUE,
            'join_' . TBL_DN_BBH => TRUE,
        );
        //end
        //======================================================================
        // Filter
        $data_filter = $this->get_data_filter_bt();
        $data_get = array_merge($data_get, $data_filter);
        // end
        //======================================================================
        // Đếm tổng số bt
        $total_rows = $this->customers_model->count_bt_by_options($data_get);
        $data['total_rows'] = $total_rows;
        //end
        // pagination
        $paging_config = array(
            'base_url' => site_url() . $this->uri->segment(1),
            'total_rows' => $total_rows,
            'per_page' => $limit,
            'uri_segment' => 2,
            'use_page_numbers' => TRUE,
            'first_link' => __('IP_paging_first'),
            'last_link' => __('IP_paging_last'),
            'num_links' => 1,
        );
        if (isset($_GET) && $_GET != '') {
            $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($paging_config);
        $data_get['offset'] = ($offset > 0) ? ($offset - 1) * ($limit) : $offset;
        $data_get['limit'] = $limit;
        //end        
        //======================================================================
        $bt = $this->customers_model->get_bt_by_options($data_get);
        if (!empty($bt)) {
            $data['bt'] = $bt;
        }
        //end
        //======================================================================
        // Lấy thông tin dữ liệu bt
        $data_bt = $this->get_data_bt();
        $data = array_merge($data_bt, $data);
        //end
        //======================================================================
        // Lấy thông tin kh
        $kh = $this->customers_model->get_kh_by_options();
        if (!empty($kh)) {
            $data['kh'] = $kh;
        }
        //======================================================================
        $data['url_export'] = $data_get['url_export'];
        //end        
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end    
    //==========================================================================
    // Lấy thông tin bt dn
    function get_bt_dn($offset = 0, $options = array()) {
        $this->check_kh_dang_nhap();
        $data = array();
        // check ss kh
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        // =====================================================================
        // nếu kh là cá nhân
        if ($ss_kh_phanloai_kh == CANHAN) {
            redirect(URL_BT_CN);
        }
        // end
        // =====================================================================
        // Lấy thông tin dn phòng ban
        $data_dn_pb = $this->get_data_dn_pb();
        $data = array_merge($data, $data_dn_pb);
        //end
        // =====================================================================
        // Lấy thông tin bồi thường
        $limit = LIMIT_DEFAULT;
        $data_get = array(
            'kh_mbh' => $ss_kh_id,
            //'limit' => $limit,
            'join_' . TBL_PRODUCTS => TRUE,
            'join_' . TBL_CUSTOMERS => TRUE,
            'join_' . TBL_PRODUCTS_SIZE => TRUE,
            'join_' . TBL_PRODUCTS_STYLE => TRUE,
            'join_' . TBL_DN_BBH => TRUE,
        );
        //======================================================================
        // Filter
        $data_filter = $this->get_data_filter_bt();
        $data_get = array_merge($data_get, $data_filter);
        // end        
        //======================================================================
        // Đếm tổng số bt
        //$total_row = $this->customers_model->count_bt($data_get);
        $total_rows = $this->customers_model->count_bt_by_options($data_get);
        $data['total_rows'] = $total_rows;
        //======================================================================
        // pagination
        $paging_config = array(
            'base_url' => site_url() . $this->uri->segment(1),
            'total_rows' => $total_rows,
            'per_page' => $limit,
            'uri_segment' => 2,
            'use_page_numbers' => TRUE,
            'first_link' => __('IP_paging_first'),
            'last_link' => __('IP_paging_last'),
            'num_links' => 1,
        );
        if (isset($_GET) && $_GET != '') {
            $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($paging_config);
        $data_get['offset'] = ($offset > 0) ? ($offset - 1) * ($limit) : $offset;
        $data_get['limit'] = $limit;
        //end       
        //======================================================================
        //$bt = $this->customers_model->get_bt($data_get);
        $bt = $this->customers_model->get_bt_by_options($data_get);
        if (!empty($bt)) {
            $data['bt'] = $bt;
        }
        //end
        //======================================================================
        // Lấy thông tin dữ liệu bt
        $data_bt = $this->get_data_bt();
        $data = array_merge($data_bt, $data);
        //end
        //======================================================================
        $data['url_export'] = $data_get['url_export'];
        $tpl = 'boithuong/boithuong_kh_dn';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    // Lấy thông tin dữ liệu bt
    function get_data_bt($options = array()) {
        $data = array();
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        // Lấy các gói bh
        $data_get = array(
            'where' => array(
                'dn_mbh' => $ss_kh_id,
            )
        );
        $goi_bh = $this->customers_model->CommonGet(TBL_PRODUCTS_SIZE, $data_get);
        if (!empty($goi_bh)) {
            $data['goi_bh'] = $goi_bh;
        }
        //end
        //======================================================================
        // Lấy các csyt
        $csyt = $this->products_style_model->get_products_style();
        if (!empty($csyt)) {
            $data['csyt'] = $csyt;
        }
        //end
        //======================================================================
        // Lấy các mã bệnh
        $ma_benh = $this->products_origin_model->get_products_origin();
        if (!empty($ma_benh)) {
            $data['ma_benh'] = $data['a_ma_benh'] = $ma_benh;
        }
        //end
        //======================================================================
        $data['scripts'] = $this->scripts_select2();
        return $data;
    }

    // end
    // =========================================================================
    // Lấy thông tin dn phòng ban
    function get_data_dn_pb($options = array()) {
        $data = array();
        if (isset($options['parent_id']) && $options['parent_id'] > 0) {
            // Lấy chi nhánh dn
            $data_get = array(
                'parent_id' => $options['parent_id'],
                'phanloai_kh' => DOANHNGHIEP,
            );
            $cn_dn = $this->customers_model->get_kh_by_options($data_get);
            if (!empty($cn_dn)) {
                $data['cn_dn'] = $cn_dn;
                $str_kh_mbh_id = '';
                foreach ($cn_dn as $index) {
                    $id = $index->id;
                    $str_kh_mbh_id .= $id . ',';
                }
                $str_kh_mbh_id .= $options['parent_id'];
                $a_kh_mbh_id = array_unique(@explode(',', $str_kh_mbh_id));
                $data['a_kh_mbh'] = $a_kh_mbh_id;
            }
        } else {
            // Lấy dn
            $data_get = array(
                'parent_id' => FALSE,
                'phanloai_kh' => DOANHNGHIEP,
            );
            $dn = $this->customers_model->get_kh_by_options($data_get);
            if (!empty($dn)) {
                $data['dn'] = $dn;
            }
        }
        //end
        //======================================================================
        // Lấy thông tin phongban 
        $data_get = array(
        );
        $phongban = $this->products_color_model->get_products_color($data_get);
        if (!empty($phongban)) {
            $data['a_phongban'] = $phongban;
        }
        return $data;
    }

    //end
    //==========================================================================
    // Filter bt
    function get_data_filter_bt($options = array()) {
        $data_get = $data = array();
        // Filter
        $hsbt = isset($_GET['hsbt']) ? $_GET['hsbt'] : '';
        if ($hsbt != '') {
            //$data_get['OD_id'] = $hsbt;
            $filter_key_search = "(`" . DB_PREFIX . TBL_CUSTOMERS . "`.`congty_canhan` LIKE '%" . $hsbt . "%'";
            $filter_key_search .= " OR `" . DB_PREFIX . TBL_CUSTOMERS . "`.`mst_cmt` LIKE '%" . $hsbt . "%'";
            $filter_key_search .= " OR `" . DB_PREFIX . TBL_ORDERS . "`.`id` LIKE '%" . $hsbt . "%')";

            $data_get['like'] = $filter_key_search;
        }
        //======================================================================
        $kh_dbh = isset($_GET['kh_dbh']) ? $_GET['kh_dbh'] : '';
        if ($kh_dbh != '' && $kh_dbh > 0) {
            $data_get['kh_dbh'] = $kh_dbh;
        }
        //======================================================================
        $goi_bh = isset($_GET['goi_bh']) ? $_GET['goi_bh'] : '';
        if ($goi_bh != '' && $goi_bh > 0) {
            $data_get['goi_bh'] = $goi_bh;
        }
        //======================================================================
        $csyt = isset($_GET['csyt']) ? $_GET['csyt'] : '';
        if ($csyt != '' && $csyt > 0) {
            $data_get['csyt'] = $csyt;
        }
        //======================================================================
        $ma_benh = isset($_GET['ma_benh']) ? $_GET['ma_benh'] : '';
        if ($ma_benh != '') {
            $data_get['ma_benh'] = $ma_benh;
        }
        //======================================================================
        $qlbh = isset($_GET['qlbh']) ? $_GET['qlbh'] : '';
        if ($qlbh != '' && $qlbh > 0) {
            $data_get['qlbh'] = $qlbh;
        }
        //======================================================================
        $phanloai_bt = isset($_GET['phanloai_bt']) ? $_GET['phanloai_bt'] : '';
        if ($phanloai_bt != '' && $phanloai_bt > 0) {
            $data_get['phanloai_bt'] = $phanloai_bt;
        }
        //======================================================================
        $data_get['url_export'] = '?hsbt=' . $hsbt . '&kh_dbh=' . $kh_dbh . '&goi_bh=' . $goi_bh . '&csyt=' . $csyt . '&qlbh=' . $qlbh . '&ma_benh=' . $ma_benh . '';
        // end 
        return $data_get;
    }

    //end
    //==========================================================================
    // Lấy thông tin bh cn
    function get_bh_cn($offset = 0, $options = array()) {
        $this->check_kh_dang_nhap();
        //======================================================================
        // check ss kh
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        // nếu kh là cá nhân
        if ($ss_kh_phanloai_kh == DOANHNGHIEP) {
            redirect(URL_BH_DN);
        }
        // end
        //======================================================================
        // Lấy thông tin bh
        //$limit = LIMIT_DEFAULT;
        $data_get = array(
            'kh_dbh' => $ss_kh_id,
            //'limit' => $limit,
            //'join_' . TBL_ORDERS => TRUE,
            'join_' . TBL_CUSTOMERS . '_mbh' => TRUE,
            'join_' . TBL_PRODUCTS_SIZE => TRUE,
            //'join_' . TBL_PRODUCTS_STYLE => TRUE,
            'join_' . TBL_DN_BBH => TRUE,
        );
        // Lấy thông tin bh
        $bh = $this->customers_model->get_bh($data_get);
        if (!empty($bh)) {
            $data['bh'] = $bh;
        }
        //end
        //======================================================================
        // Lấy qlcl của kh cn
        $data_get = array(
            'CL_kh_id' => $ss_kh_id,
        );
        $qlbt_cl = $this->customers_model->get_ql_cl($data_get);
        if (!empty($qlbt_cl)) {
            $data['qlbt_cl'] = $qlbt_cl;
        }
        //======================================================================
        $tpl = 'baohiem/baohiem_kh_cn';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    // check kh đăng nhập
    function check_kh_dang_nhap($options = array()) {
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        if ($ss_kh_id > 0) {
            return TRUE;
        } else {
            redirect(URL_KH_DANGNHAP);
        }
    }

    //end
    //==========================================================================
    // Lấy thông tin bh dn
    function get_bh_dn($offset = 0, $options = array()) {
        $data = array();
        // check ss kh
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        //======================================================================
        // nếu kh là cá nhân
        if ($ss_kh_phanloai_kh == CANHAN) {
            redirect(URL_BH_CN);
        }
        // end        
        //======================================================================
        // Lấy các chi nhánh và phòng ban
        $data_get = array(
            'parent_id' => $ss_kh_id,
        );
        $data = $this->get_data_dn_pb($data_get);
        //end
        //======================================================================
        // Lấy các gói bh
        $data_get = array(
            'where' => array(
                'dn_mbh' => $ss_kh_id,
            )
        );
        $goi_bh = $this->customers_model->CommonGet(TBL_PRODUCTS_SIZE, $data_get);
//        echo '<pre>';
//        print_r($goi_bh);
//        die;
        if (!empty($goi_bh)) {
            $data['goi_bh'] = $goi_bh;
        }
        //end
        //======================================================================
        // Lấy thông tin bh
        $limit = LIMIT_DEFAULT;
        $data_get = array(
            'kh_mbh' => $ss_kh_id,
            //'limit' => $limit,
            //'join_' . TBL_ORDERS => TRUE,
            'join_' . TBL_CUSTOMERS . '_dbh' => TRUE,
            'join_' . TBL_PRODUCTS_SIZE => TRUE,
            //'join_' . TBL_PRODUCTS_STYLE => TRUE,
            'join_' . TBL_DN_BBH => TRUE,
        );
        // Nếu công ty có chi nhánh
        if (isset($data['a_kh_mbh'])) {
            $data_get['a_kh_mbh'] = $data['a_kh_mbh'];
        }
        //end
        //======================================================================
        // Lấy tất cả bh
//        $bh_all = $this->customers_model->get_bh($data_get);
//        if (!empty($bh_all)) {
//            $data['bh_all'] = $bh_all;
//        }
        //======================================================================
        // Filter
        //$data_filter = $this->get_data_filter_bt();
        $kh_dbh = isset($_GET['kh_dbh']) ? $_GET['kh_dbh'] : '';
        if ($kh_dbh != '') {
            //$data_get['like_kh_dbh'] = $kh_dbh;
            $filter_key_search = "(`" . DB_PREFIX . TBL_CUSTOMERS . "`.`congty_canhan` LIKE '%" . $kh_dbh . "%'";
            //$filter_key_search .= " OR `" . DB_PREFIX . TBL_CUSTOMERS . "`.`mst_cmt` LIKE '%" . $kh_dbh . "%'";
            $filter_key_search .= " OR `" . DB_PREFIX . TBL_CUSTOMERS . "`.`mst_cmt` LIKE '%" . $kh_dbh . "%')";

            $data_get['like'] = $filter_key_search;
        }
        //----------------------------------------------------------------------
        $cn = isset($_GET['cn']) ? $_GET['cn'] : '';
        if ($cn != '' && $cn > 0) {
            $data_get['kh_mbh'] = $cn;
            // Xóa array kh mbh
            unset($data_get['a_kh_mbh']);
        }
        //----------------------------------------------------------------------
        $pb = isset($_GET['pb']) ? $_GET['pb'] : '';
        if ($pb != '' && $pb > 0) {
            $data_get['pb'] = $pb;
        }
        //----------------------------------------------------------------------
        $goi_bh = isset($_GET['goi_bh']) ? $_GET['goi_bh'] : '';
        if ($goi_bh != '' && $goi_bh > 0) {
            $data_get['goi_bh'] = $goi_bh;
        }
        $data_get['url_export'] = '?kh_dbh=' . $kh_dbh . '&goi_bh=' . $goi_bh . '&cn=' . $cn . '&pb=' . $pb . '';
        //$data_get = array_merge($data_get, $data_filter);
        // end 
        //======================================================================
        $total_rows = $this->customers_model->count_bh($data_get);
        $data['total_rows'] = $total_rows;
        //end
        // pagination
        $paging_config = array(
            'base_url' => site_url() . $this->uri->segment(1),
            'total_rows' => $total_rows,
            'per_page' => $limit,
            'uri_segment' => 2,
            'use_page_numbers' => TRUE,
            'first_link' => __('IP_paging_first'),
            'last_link' => __('IP_paging_last'),
            'num_links' => 1,
        );
        if (isset($_GET) && $_GET != '') {
            $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($paging_config);
        $data_get['offset'] = ($offset > 0) ? ($offset - 1) * ($limit) : $offset;
        $data_get['limit'] = $limit;
        // Lấy thông tin bh
        $bh = $this->customers_model->get_bh($data_get);
        if (!empty($bh)) {
            $data['bh'] = $bh;
        }
        //end
        //======================================================================
        $data['url_export'] = $data_get['url_export'];
        $data['scripts'] = $this->scripts_select2();
        $tpl = 'baohiem/baohiem_kh_dn';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    // check csyt đăng nhập
    function check_csyt_dangnhap($options = array()) {
        $ss_csyt_id = $this->phpsession->get('ss_csyt_id');
        if ($ss_csyt_id > 0) {
            return TRUE;
        } else {
            redirect(URL_CSYT_DANGNHAP);
        }
    }

    //end
    //==========================================================================
    // change password
    function change_password($options = array()) {
        $this->check_kh_dang_nhap();
        //======================================================================
        // check ss kh
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        $ss_kh_password = $this->phpsession->get('ss_kh_password');
        //======================================================================
        if (isset($_POST['submit'])) {
            //$this->form_validation->set_rules('password_old', 'Mật khẩu cũ', 'trim|required|xss_clean|max_length[12]');
            $this->form_validation->set_rules('password_new', 'Mật khẩu mới', 'trim|required|xss_clean|max_length[12]');
            $this->form_validation->set_rules('password_rp', 'Nhập lại mật khẩu', 'trim|required|xss_clean|max_length[12]|matches[password_new]');

            $password_old = $this->input->post('password_old', TRUE);
            if (md5($password_old) != $ss_kh_password) {
                
            }
            //==================================================================
            $password_new = $this->input->post('password_new', TRUE);
            if ($this->form_validation->run($this)) {
                $data_update = array(
                    'password' => md5($password_new),
                );
                $this->customers_model->CommonUpdate(TBL_CUSTOMERS, $ss_kh_id, $data_update);
                $msg = '<strong class="text-success success">Thay đổi mật khẩu thành công.</strong>';
                $this->phpsession->flashsave('msg', $msg);
                redirect(current_url());
            }
        }
        //======================================================================
        $tpl = 'setting/kh_change_password';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    //==========================================================================
    //==========================================================================
    function export_dn($options = array()) {
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $data_get = array(
            'kh_mbh' => $ss_kh_id,
            //'limit' => $limit,
            'join_' . TBL_ORDERS => TRUE,
            'join_' . TBL_CUSTOMERS => TRUE,
            'join_' . TBL_PRODUCTS_SIZE => TRUE,
            'join_' . TBL_PRODUCTS_STYLE => TRUE,
            'join_' . TBL_DN_BBH => TRUE,
        );

        $data_filter = $this->get_data_filter_bt();
        $data_get = array_merge($data_get, $data_filter);

        $orders = $this->customers_model->get_bt($data_get);


        if (count($orders) > 0) {
            //load our new PHPExcel library
            $this->load->library('excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Báo cáo bồi thường');
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A1', 'Mã số HSBT');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Khách hàng được BH');
            $this->excel->getActiveSheet()->setCellValue('C1', 'Công ty chi nhánh');
            $this->excel->getActiveSheet()->setCellValue('D1', 'Phòng ban');
            $this->excel->getActiveSheet()->setCellValue('E1', 'CMT/Hộ chiếu');
            $this->excel->getActiveSheet()->setCellValue('F1', 'Tình trạng GQBT');
            $this->excel->getActiveSheet()->setCellValue('G1', 'Tình trạng GQHS');
            $this->excel->getActiveSheet()->setCellValue('H1', 'Chứng từ chờ BS');
            $this->excel->getActiveSheet()->setCellValue('I1', 'Ngày BSHS');
            $this->excel->getActiveSheet()->setCellValue('J1', 'Ngày duyệt BT');
            $this->excel->getActiveSheet()->setCellValue('K1', 'Loại BT');
            $this->excel->getActiveSheet()->setCellValue('L1', 'Ngày XRTT');
            $this->excel->getActiveSheet()->setCellValue('M1', 'Mối quan hệ');
            $this->excel->getActiveSheet()->setCellValue('N1', 'Phương thức BT');
            $this->excel->getActiveSheet()->setCellValue('O1', 'Ngày nhận HS');
            $this->excel->getActiveSheet()->setCellValue('P1', 'Ngày XLBT');
            $this->excel->getActiveSheet()->setCellValue('Q1', 'Ngày TBKH');
            $this->excel->getActiveSheet()->setCellValue('R1', 'Chuẩn đoán bệnh');
            $this->excel->getActiveSheet()->setCellValue('S1', 'Mã bệnh');
            $this->excel->getActiveSheet()->setCellValue('T1', 'Số tiền TTTCSYT');
            $this->excel->getActiveSheet()->setCellValue('U1', 'Số tiền ĐBT');
            $this->excel->getActiveSheet()->setCellValue('V1', 'Số tiền từ chối');
            $this->excel->getActiveSheet()->setCellValue('W1', 'Lý do KBT');
            $this->excel->getActiveSheet()->setCellValue('X1', 'Hình thức NTBT');
            $this->excel->getActiveSheet()->setCellValue('Y1', 'Ghi chú TT');
            $this->excel->getActiveSheet()->setCellValue('Z1', 'Ngày TT');
            $this->excel->getActiveSheet()->setCellValue('AA1', 'Phí BH');
            $this->excel->getActiveSheet()->setCellValue('AB1', 'CSYT');
            $this->excel->getActiveSheet()->setCellValue('AC1', 'Công ty BH');
            $this->excel->getActiveSheet()->setCellValue('AD1', 'Hồ sơ scan');
            $this->excel->getActiveSheet()->setCellValue('AE1', 'Ngày xảy ra tổn thất');
            $this->excel->getActiveSheet()->setCellValue('AF1', 'Ngày điều trị');
            $this->excel->getActiveSheet()->setCellValue('AG1', 'Mã bệnh');
            $this->excel->getActiveSheet()->setCellValue('AH1', 'Quyền lợi BH');
            $this->excel->getActiveSheet()->setCellValue('AI1', 'Ngày YCBT');
            $this->excel->getActiveSheet()->setCellValue('AJ1', 'Số tiền YCBT');
            $this->excel->getActiveSheet()->setCellValue('AK1', 'Số ngày YCBT');
            $this->excel->getActiveSheet()->setCellValue('AL1', 'Số tiền được BT');
            $this->excel->getActiveSheet()->setCellValue('AM1', 'Thời gian giải quyết');



            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('Q1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('R1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('S1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('T1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('U1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('V1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('W1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('X1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('Y1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('Z1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AA1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AB1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AC1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AD1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AE1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AF1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AG1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AH1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AI1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AJ1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AK1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AL1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AM1')->getFont()->setBold(true);


            $row = 2;
            foreach ($orders as $order):


                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $order->OD_id);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $order->congty_canhan);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $order->mst_cmt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $order->tinhtrang_hoso);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $order->chungtu_bosung);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $order->ngay_bosung_hoso);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $order->ngay_duyet_bt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $order->ngay_xayra_tonthat);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(13, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(14, $row, $order->ngay_nhan_hoso);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(15, $row, $order->ngay_xuly_bt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(16, $row, $order->ngay_thongbao_kh);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(17, $row, $order->chuandoan_benh);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(18, $row, $order->OD_ma_benh);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(19, $row, $order->so_tien_thanhtoan_csyt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(20, $row, $order->so_tien_dbt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(21, $row, $order->so_tien_tuchoi);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(22, $row, $order->lydo_khong_bt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(23, $row, $order->ghichu_thanhtoan);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(24, $row, $order->hinhthuc_nhantien_bt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(25, $row, $order->OD_ngay_thanhtoan);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(26, $row, $order->goi_gia_tien);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(27, $row, $order->ten_csyt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(28, $row, $order->ten_dn_bbh);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(29, $row, $order->hoso_scan);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(30, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(31, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(32, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(33, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(34, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(35, $row, $order->so_tien_ycbt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(36, $row, $order->so_ngay_ycbt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(37, $row, $order->so_tien_dbt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(38, $row, '');


                $row++;
            endforeach;

            $filename = 'bao_cao_boi_thuong.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
        }else {
            //hoa don khong co san pham
            return $this->browse(array('error' => 'Mục bạn đã chọn hiện thời không có !'));
        }
    }

    function export_cn($options = array()) {
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        $ss_kh_parent_id = $this->phpsession->get('ss_kh_parent_id');
        $ss_kh_phongban = $this->phpsession->get('ss_kh_phongban');


        $limit = LIMIT_DEFAULT;
        $data_get = array(
            'OD_kh_dbh' => $ss_kh_id,
            'join_' . TBL_ORDERS => TRUE,
            'join_' . TBL_CUSTOMERS => TRUE,
            'join_' . TBL_PRODUCTS_SIZE => TRUE,
            'join_' . TBL_PRODUCTS_STYLE => TRUE,
            'join_' . TBL_DN_BBH => TRUE,
        );

        // Filter
        $data_filter = $this->get_data_filter_bt();
        $data_get = array_merge($data_get, $data_filter);



        $orders = $this->customers_model->get_bt($data_get);


        if (count($orders) > 0) {
            //load our new PHPExcel library
            $this->load->library('excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Báo cáo bồi thường');
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A1', 'Mã số HSBT');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Khách hàng được BH');
            $this->excel->getActiveSheet()->setCellValue('C1', 'Bên mua BH');
            $this->excel->getActiveSheet()->setCellValue('D1', 'Thời gian giải quyết');
            $this->excel->getActiveSheet()->setCellValue('E1', 'CMT/Hộ chiếu');
            $this->excel->getActiveSheet()->setCellValue('F1', 'Tình trạng GQBT');
            $this->excel->getActiveSheet()->setCellValue('G1', 'Tình trạng GQHS');
            $this->excel->getActiveSheet()->setCellValue('H1', 'Chứng từ chờ BS');
            $this->excel->getActiveSheet()->setCellValue('I1', 'Ngày BSHS');
            $this->excel->getActiveSheet()->setCellValue('J1', 'Ngày duyệt BT');
            $this->excel->getActiveSheet()->setCellValue('K1', 'Loại BT');
            $this->excel->getActiveSheet()->setCellValue('L1', 'Ngày XRTT');
            $this->excel->getActiveSheet()->setCellValue('M1', 'Mối quan hệ');
            $this->excel->getActiveSheet()->setCellValue('N1', 'Phương thức BT');
            $this->excel->getActiveSheet()->setCellValue('O1', 'Ngày nhận HS');
            $this->excel->getActiveSheet()->setCellValue('P1', 'Ngày XLBT');
            $this->excel->getActiveSheet()->setCellValue('Q1', 'Ngày TBKH');
            $this->excel->getActiveSheet()->setCellValue('R1', 'Chuẩn đoán bệnh');
            $this->excel->getActiveSheet()->setCellValue('S1', 'Mã bệnh');
            $this->excel->getActiveSheet()->setCellValue('T1', 'Số tiền TTTCSYT');
            $this->excel->getActiveSheet()->setCellValue('U1', 'Số tiền ĐBT');
            $this->excel->getActiveSheet()->setCellValue('V1', 'Số tiền từ chối');
            $this->excel->getActiveSheet()->setCellValue('W1', 'Lý do KBT');
            $this->excel->getActiveSheet()->setCellValue('X1', 'Hình thức NTBT');
            $this->excel->getActiveSheet()->setCellValue('Y1', 'Ghi chú TT');
            $this->excel->getActiveSheet()->setCellValue('Z1', 'Ngày TT');
            $this->excel->getActiveSheet()->setCellValue('AA1', 'Phí BH');
            $this->excel->getActiveSheet()->setCellValue('AB1', 'CSYT');
            $this->excel->getActiveSheet()->setCellValue('AC1', 'Công ty BH');
            $this->excel->getActiveSheet()->setCellValue('AD1', 'Hồ sơ scan');
            $this->excel->getActiveSheet()->setCellValue('AE1', 'Ngày xảy ra tổn thất');
            $this->excel->getActiveSheet()->setCellValue('AF1', 'Ngày điều trị');
            $this->excel->getActiveSheet()->setCellValue('AG1', 'Mã bệnh');
            $this->excel->getActiveSheet()->setCellValue('AH1', 'Quyền lợi BH');
            $this->excel->getActiveSheet()->setCellValue('AI1', 'Ngày YCBT');
            $this->excel->getActiveSheet()->setCellValue('AJ1', 'Số tiền YCBT');
            $this->excel->getActiveSheet()->setCellValue('AK1', 'Số ngày YCBT');
            $this->excel->getActiveSheet()->setCellValue('AL1', 'Số tiền được BT');
            $this->excel->getActiveSheet()->setCellValue('AM1', 'Thời gian giải quyết');



            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('Q1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('R1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('S1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('T1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('U1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('V1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('W1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('X1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('Y1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('Z1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AA1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AB1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AC1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AD1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AE1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AF1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AG1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AH1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AI1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AJ1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AK1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AL1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('AM1')->getFont()->setBold(true);


            $row = 2;
            foreach ($orders as $order):


                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $order->OD_id);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $order->congty_canhan);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $order->mst_cmt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $order->tinhtrang_hoso);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $order->chungtu_bosung);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $order->ngay_bosung_hoso);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $order->ngay_duyet_bt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $order->ngay_xayra_tonthat);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(13, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(14, $row, $order->ngay_nhan_hoso);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(15, $row, $order->ngay_xuly_bt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(16, $row, $order->ngay_thongbao_kh);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(17, $row, $order->chuandoan_benh);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(18, $row, $order->OD_ma_benh);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(19, $row, $order->so_tien_thanhtoan_csyt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(20, $row, $order->so_tien_dbt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(21, $row, $order->so_tien_tuchoi);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(22, $row, $order->lydo_khong_bt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(23, $row, $order->ghichu_thanhtoan);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(24, $row, $order->hinhthuc_nhantien_bt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(25, $row, $order->OD_ngay_thanhtoan);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(26, $row, $order->goi_gia_tien);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(27, $row, $order->ten_csyt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(28, $row, $order->ten_dn_bbh);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(29, $row, $order->hoso_scan);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(30, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(31, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(32, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(33, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(34, $row, '');
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(35, $row, $order->so_tien_ycbt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(36, $row, $order->so_ngay_ycbt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(37, $row, $order->so_tien_dbt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(38, $row, '');


                $row++;
            endforeach;

            $filename = 'bao_cao_boi_thuong.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
        }else {
            //hoa don khong co san pham
            return $this->browse(array('error' => 'Mục bạn đã chọn hiện thời không có !'));
        }
    }

    function export_bh_dn($options = array()) {
        $data = array();
        // check ss kh
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
        // nếu kh là cá nhân
        if ($ss_kh_phanloai_kh == CANHAN) {
            redirect(URL_BH_CN);
        }
        // end   
        //======================================================================     
        // Lấy các chi nhánh và phòng ban
        $data_get = array(
            'parent_id' => $ss_kh_id,
        );
        $data = $this->get_data_dn_pb($data_get);
        //end
        // Lấy các gói bh
        $goi_bh = $this->products_size_model->get_products_size();
        if (!empty($goi_bh)) {
            $data['goi_bh'] = $goi_bh;
        }
        //end
        // Lấy thông tin bh
        $limit = LIMIT_DEFAULT;
        $data_get = array(
            'kh_mbh' => $ss_kh_id,
            //'limit' => $limit,
            //'join_' . TBL_ORDERS => TRUE,
            'join_' . TBL_CUSTOMERS . '_dbh' => TRUE,
            'join_' . TBL_PRODUCTS_SIZE => TRUE,
            //'join_' . TBL_PRODUCTS_STYLE => TRUE,
            'join_' . TBL_DN_BBH => TRUE,
        );
        // Nếu công ty có chi nhánh
        if (isset($data['a_kh_mbh'])) {
            $data_get['a_kh_mbh'] = $data['a_kh_mbh'];
        }
        //end
        // Lấy tất cả bh
        $bh_all = $this->customers_model->get_bh($data_get);
        if (!empty($bh_all)) {
            $data['bh_all'] = $bh_all;
        }
        // Filter
        //$data_filter = $this->get_data_filter_bt();
        $kh_dbh = isset($_GET['kh_dbh']) ? $_GET['kh_dbh'] : '';
        if ($kh_dbh != '' && $kh_dbh > 0) {
            $data_get['kh_dbh'] = $kh_dbh;
        }
        $cn = isset($_GET['cn']) ? $_GET['cn'] : '';
        if ($cn != '' && $cn > 0) {
            $data_get['kh_mbh'] = $cn;
            // Xóa array kh mbh
            unset($data_get['a_kh_mbh']);
        }
        $pb = isset($_GET['pb']) ? $_GET['pb'] : '';
        if ($pb != '' && $pb > 0) {
            $data_get['pb'] = $pb;
        }
        $goi_bh = isset($_GET['goi_bh']) ? $_GET['goi_bh'] : '';
        if ($goi_bh != '' && $goi_bh > 0) {
            $data_get['goi_bh'] = $goi_bh;
        }

        // Lấy thông tin bh
        $orders = $this->customers_model->get_bh($data_get);



        if (count($orders) > 0) {
            //load our new PHPExcel library
            $this->load->library('excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Báo cáo bảo hiểm');
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A1', 'Mã số KH');
            $this->excel->getActiveSheet()->setCellValue('B1', 'Họ và tên KH');
            $this->excel->getActiveSheet()->setCellValue('C1', 'CMT/Hộ chiếu');
            $this->excel->getActiveSheet()->setCellValue('D1', 'Ngày hiệu lực');
            $this->excel->getActiveSheet()->setCellValue('E1', 'Ngày hết hạn');
            $this->excel->getActiveSheet()->setCellValue('F1', 'Gói BH');
            $this->excel->getActiveSheet()->setCellValue('G1', 'Công ty BH');
            $this->excel->getActiveSheet()->setCellValue('H1', 'Mối quan hệ');


            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);


            $row = 2;
            foreach ($orders as $order):
                $ndbh_quanhe = $order->ndbh_quanhe;
                $ndbh_quanhe_txt = get_ndbh_quanhe($ndbh_quanhe);

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $order->KH_id);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $order->congty_canhan);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $order->mst_cmt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $order->ngay_hieuluc);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $order->ngay_ketthuc);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $order->goi_bh_dm);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $order->ten_dn_bbh);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $ndbh_quanhe_txt);

                $row++;
            endforeach;

            $filename = 'bao_cao_bao_hiem.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
        }else {
            //hoa don khong co san pham
            return $this->browse(array('error' => 'Mục bạn đã chọn hiện thời không có !'));
        }
    }

}
