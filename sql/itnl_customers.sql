-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2016 at 10:23 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quanlybaohiem`
--

-- --------------------------------------------------------

--
-- Table structure for table `itnl_customers`
--

CREATE TABLE IF NOT EXISTS `itnl_customers` (
`id` int(11) NOT NULL COMMENT 'Ma KH',
  `fullname` varchar(30) NOT NULL COMMENT 'Ten cty/ ca nhan',
  `DOB` varchar(255) DEFAULT NULL COMMENT 'Date Of Birth',
  `address` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `phone2` varchar(15) DEFAULT NULL COMMENT 'Mobile',
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL COMMENT '	',
  `company` varchar(256) DEFAULT NULL COMMENT 'Cty con | phong ban ',
  `roles_id` int(11) DEFAULT NULL,
  `active` smallint(1) DEFAULT '1',
  `alias_name` varchar(15) DEFAULT NULL,
  `cities_id` int(11) NOT NULL,
  `joined_date` datetime DEFAULT NULL COMMENT 'Ngay cap',
  `avatar` varchar(256) DEFAULT NULL,
  `is_openid` tinyint(4) DEFAULT NULL,
  `sex` tinyint(4) DEFAULT NULL COMMENT 'Giới tính.',
  `city_id` tinyint(4) DEFAULT NULL COMMENT 'thành phố',
  `district_id` tinyint(4) DEFAULT NULL COMMENT 'Quận/huyện',
  `number_house` varchar(256) DEFAULT NULL COMMENT 'Số nhà, ngõ ngách',
  `road` varchar(256) DEFAULT NULL COMMENT 'Đường',
  `level` varchar(256) DEFAULT NULL COMMENT 'Lầu',
  `type` tinyint(4) DEFAULT '1' COMMENT 'Loai kh',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `editor` int(11) NOT NULL,
  `creator` int(11) NOT NULL,
  `so_hop_dong` varchar(255) NOT NULL COMMENT 'Hop dong BH',
  `cmt` varchar(255) NOT NULL COMMENT 'dkkd | cmt | ho chieu',
  `noi_cap` varchar(255) NOT NULL,
  `type_custom` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 thuong 1 vip',
  `customer_type` tinytext,
  `phanloai_kh` varchar(255) NOT NULL,
  `hopdong_baohiem` varchar(255) NOT NULL,
  `congty_canhan` varchar(255) NOT NULL,
  `congtycon_chinhanh` varchar(255) NOT NULL,
  `phongban` varchar(255) NOT NULL,
  `diachi` varchar(255) NOT NULL,
  `mst_cmt` varchar(255) NOT NULL,
  `ngaycap` datetime NOT NULL,
  `noicap` varchar(255) NOT NULL,
  `dienthoai` varchar(255) NOT NULL,
  `didong` varchar(255) NOT NULL,
  `phanloai_kh1` varchar(255) NOT NULL,
  `gioitinh` varchar(255) NOT NULL,
  `ndbh_phanloai_kh` varchar(255) NOT NULL,
  `ndbh_hopdong_baohiem` varchar(255) NOT NULL,
  `ndbh_congty_canhan` varchar(255) NOT NULL,
  `ndbh_congtycon_chinhanh` varchar(255) NOT NULL,
  `ndbh_phongban` varchar(255) NOT NULL,
  `ndbh_diachi` varchar(255) NOT NULL,
  `ndbh_mst_cmt` varchar(255) NOT NULL,
  `ndbh_ngaycap` datetime NOT NULL,
  `ndbh_noicap` varchar(255) NOT NULL,
  `ndbh_email` varchar(255) NOT NULL,
  `ndbh_dienthoai` varchar(255) NOT NULL,
  `ndbh_didong` varchar(255) NOT NULL,
  `ndbh_phanloai_kh1` varchar(255) NOT NULL,
  `ndbh_quanhe` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itnl_customers`
--

INSERT INTO `itnl_customers` (`id`, `fullname`, `DOB`, `address`, `email`, `phone`, `phone2`, `username`, `password`, `company`, `roles_id`, `active`, `alias_name`, `cities_id`, `joined_date`, `avatar`, `is_openid`, `sex`, `city_id`, `district_id`, `number_house`, `road`, `level`, `type`, `created_date`, `updated_date`, `status`, `editor`, `creator`, `so_hop_dong`, `cmt`, `noi_cap`, `type_custom`, `customer_type`, `phanloai_kh`, `hopdong_baohiem`, `congty_canhan`, `congtycon_chinhanh`, `phongban`, `diachi`, `mst_cmt`, `ngaycap`, `noicap`, `dienthoai`, `didong`, `phanloai_kh1`, `gioitinh`, `ndbh_phanloai_kh`, `ndbh_hopdong_baohiem`, `ndbh_congty_canhan`, `ndbh_congtycon_chinhanh`, `ndbh_phongban`, `ndbh_diachi`, `ndbh_mst_cmt`, `ndbh_ngaycap`, `ndbh_noicap`, `ndbh_email`, `ndbh_dienthoai`, `ndbh_didong`, `ndbh_phanloai_kh1`, `ndbh_quanhe`) VALUES
(1, 'Long', '11/08/88', 'ha noi', 'long@yahoo.com', '090909090', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-04-22 17:24:00', '2016-04-22 17:30:16', 1, 1, 1, '', '', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(2, 'Tuấn', '11/07/88', 'hà nội', 'lamwebchuanseo@gmail.com', '0979996422', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-04-24 11:33:00', '2016-04-24 11:33:13', 1, 1, 1, '', '', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(3, 'Long123', '', 'hà nội', 'longlazada@yahoo.com', '0987987966', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-04-29 08:51:05', 1, 1, 1, '', '', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(4, 'Long456', '', 'hà nội', 'longlazada@yahoo.com', '0987987966', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-04-29 09:36:07', 1, 1, 1, '', '', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(5, 'Long2367', '', 'hà nội', 'longlazada@yahoo.com', '0987987966', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-04-29 09:36:50', 1, 1, 1, '', '', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(6, 'Minh', '11/07/88', 'Tầng 4, 110 nguyễn ngọc nại', 'minh@yahoo.com', '094250', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-04-29 17:20:09', 1, 1, 1, '', '', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(7, 'Vương', '11/08/88', 'ha noi pho', 'vuong@yahoo.com', '0454534543', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-04-29 17:24:01', 1, 1, 1, '', '', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(8, 'Bạch cốt tinh', '11/07/88', 'hà nội', 'tinh@yahoo.com', '0987665654', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-05-02 00:00:00', '2016-05-02 16:24:24', 1, 1, 1, '', '', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(9, 'Lưu Hùng Kiên', '11/07/1988', 'hà nội', 'kien@gmail.com', '0909090909', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-09-25 15:17:00', '2016-09-25 15:18:36', 1, 1, 1, '', '012346789', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(10, 'Vũ Văn Thông', '11/07/1988', 'hà nội', 'thong@gmail.com', '0979435422', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-09-25 15:18:00', '2016-09-25 15:19:32', 1, 1, 1, '', '012789456', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(11, 'Lưu Hùng Kiên Định', '', '', 'jfdsf@gmai.com', '0687456', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-09-25 15:31:00', '2016-09-25 15:32:04', 1, 1, 1, '', '01245789', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(12, 'Cao Văn Cường', '', '', 'cuong@gmail.com', '09438477473', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-09-26 14:58:00', '2016-09-26 14:59:15', 1, 1, 1, '', '07865764533', '', 0, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', ''),
(13, '', NULL, NULL, 'bhs@gmail.com', NULL, NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-09-27 15:07:38', 1, 1, 1, '', '', '', 0, NULL, 'cá nhân', 'Bh2016TR', 'Công ty BH Eroscare', 'Công ty BH Eroscare', 'Phòng Ban BH QF', '110 Nguyễn Ngọc Nại HA', '9891284841', '2033-04-17 00:00:00', 'CA Nam Định', '0919244660', '0919244660', 'vip', 'nam', 'cá nhân', 'BHHD 2016R', 'Công ty BH Eroscare', 'Công ty BH Eroscare', 'Phòng BH', '110 Nguyễn Ngọc Nại - HN', '9828127173', '2033-04-17 00:00:00', 'CA Hà Nội', 'bg@gmail.com', '0919229399', '0919229399', 'doanh nghiệp', 'nhân viên');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `itnl_customers`
--
ALTER TABLE `itnl_customers`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `itnl_customers`
--
ALTER TABLE `itnl_customers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ma KH',AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
