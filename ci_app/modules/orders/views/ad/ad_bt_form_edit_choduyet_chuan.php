<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Nội dung bồi thường"</small>
    <span class="fright"><a class="button close" href="<?php echo ORDER_ADMIN_BASE_URL; ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>
<form action="<?php echo ORDER_ADMIN_BASE_URL . '/edit-bt'; ?>" method="post" accept-charset="utf-8">
    <?php
    $a_phanloai_bt = get_a_phanloai_bt();
    $a_phuongthuc_bt = get_a_phuongthuc_bt();
    $a_nhantien_bt = get_a_nhantien_bt();
    $a_nhan_hoso = get_a_nhan_hoso();
    //==========================================================================
    $selected_df = 'selected="selected"';
    $disabled = 'disabled = true';
    $disabled_bt = $disabled;
    $a_ct_selected = array();
    //==========================================================================
    if (isset($bt) && is_object($bt)) {
        $KH_id = $bt->KH_id;
        $kh_dbt = $bt->congty_canhan;
        $mst_cmt = $bt->mst_cmt;
        //======================================================================
        $BH_GOI_ID = $bt->BH_GOI_ID;
        //======================================================================
        $id = $bt->id;
        $bt_code = $bt->bt_code;
        $case = $bt->case;
        $chungtu_bosung = $bt->chungtu_bosung;
        $chungtu_bosung_txt = $bt->chungtu_bosung_txt;
        $phanloai_bt = $bt->phanloai_bt;
        $phanloai_bt_txt = get_phanloai_bt($phanloai_bt);
        $case_bt = $bt->parent_id;
        $quyenloi_bt = $bt->quyenloi_bt;
        $phanloai_thaisan = $bt->phanloai_thaisan;
        $phanloai_thaisan_txt = get_phanloai_thaisan($phanloai_thaisan);
        $phuongthuc_bt = $bt->phuongthuc_bt;
        $phuongthuc_bt_txt = get_phuongthuc_bt($phuongthuc_bt);
        $csyt_bt = $bt->csyt;
        $phuongthuc_nhan_hs = $bt->phuongthuc_nhan_hs;
        $hinhthuc_nhantien_bt = $bt->hinhthuc_nhantien_bt;
        $hinhthuc_nhantien_bt_txt = get_hinhthuc_nhantien_bt($hinhthuc_nhantien_bt);
        $ghichu_hinhthuc_nhantien_bt = $bt->ghichu_hinhthuc_nhantien_bt;
        $ghichu_xuly_bt = $bt->ghichu_xuly_bt;
        $ghichu_pheduyet_bt = $bt->ghichu_pheduyet_bt;
        $ghichu_thanhtoan_bt = $bt->ghichu_thanhtoan_bt;
        //----------------------------------------------------------------------
        $so_tien_ycbt = $bt->so_tien_ycbt;
        $so_tien_dbt = $bt->so_tien_dbt;
        $so_ngay_ycbt = $bt->so_ngay_ycbt;
        $so_lan_kham = $bt->so_lan_kham;
        $status = $bt->order_status;
        $ghichu_hoso = $bt->ghichu_hoso;
        $status_txt = get_orders_status($status);
        if ($status == BT_CHODUYET) {
            $disabled_bt = '';
        }
        $chungtu_bosung = $bt->chungtu_bosung;
        if ($chungtu_bosung != '') {
            $a_ct_selected = explode(',', $chungtu_bosung);
        }
        $chungtu_bosung_txt = $bt->chungtu_bosung_txt;
        $lydo_khong_bt = $bt->lydo_khong_bt;
        $ghichu_khong_bt = $bt->ghichu_khong_bt;
        $bt_ma_benh = $bt->OD_ma_benh;
        $chuandoan_benh = $bt->chuandoan_benh;
        //----------------------------------------------------------------------
        $nth_fullname = $bt->nth_fullname;
        $nth_bank = $bt->nth_bank;
        $nth_stk = $bt->nth_stk;
        $nth_bank_address = $bt->nth_bank_address;
        //----------------------------------------------------------------------
        $date_xayra_tonthat = substr($bt->date_xayra_tonthat, 0, 10);
        $date_nhan_bt = substr($bt->date_nhan_bt, 0, 10);
        $date_xuly_bt = substr($bt->date_xuly_bt, 0, 10);
    }
//echo '<pre>';
//print_r($bt);
//die;
    echo form_hidden('bt_id', $id);
    echo form_hidden('id', $id);
//==============================================================================
    $so_ngay_ycbt = isset($so_ngay_ycbt) ? $so_ngay_ycbt : 0;
//end
//$slk_bh = $sn_bh = 'Error';
    ?>
    <div class="form_content">
        <?php $this->load->view('powercms/message'); ?>
        <ul class="tabs">
            <li><a href="#tab1">Thông tin người được bảo hiểm</a></li>
            <!--<li><a href="#tab2">Quyền lợi bảo hiểm tai nạn</a></li>-->
            <!--<li><a href="#tab3">Tình trạng bồi thường</a></li>-->
        </ul>
        <div class="tab_container">
            <div id="tab1" class="tab_content">
                <div class="col-sm-6 col-xs-12">
                    <div class="row">
                        <label class="col-sm-4">Người được BT:</label>
                        <?php echo $kh_dbt ?>
                        <input class="select_kh_dbh_bt" type="hidden" name="kh_dbh" value="<?php echo $KH_id ?>"/>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">CMT/Hộ chiếu:</label>
                        <?php echo $mst_cmt ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Loại bồi thường:</label>
                        <?php echo $phanloai_bt_txt ?>
                        <select name="phanloai_bt" class="hide select_loai_bt">
                            <option selected="" value="<?php echo $phanloai_bt ?>"><?php echo $phanloai_bt_txt ?></option>
                        </select>
                    </div>

                    <?php
                    if ($phanloai_bt == BT_THAISAN_QLNT) {
                        ?>
                        <div class="row mt20">
                            <label class="col-sm-4">Loại thai sản:</label>
                            <?php echo $phanloai_thaisan_txt ?>
                            <select name="phanloai_thaisan" class="hide">
                                <option selected="" value="<?php echo $phanloai_thaisan ?>"><?php echo $phanloai_thaisan_txt ?></option>
                            </select>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="row mt20">
                        <label class="col-sm-4">Gói bảo hiểm mua:</label>
                        <?php // echo $goi_bh_dm ?>
                        <?php
                        if (isset($a_goi_bh) && !empty($a_goi_bh)) {
                            foreach ($a_goi_bh as $index) {
                                $goi_bh_id = $index->id;
                                $goi_bh_name = $index->name;
                                if ($BH_GOI_ID == $goi_bh_id) {
                                    echo $goi_bh_name;
                                    break;
                                }
                            }
                        }
                        ?>

                        <input type="hidden" name="goi_bh" value="<?php echo $goi_bh ?>"/>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Cơ sở y tế:</label>
                        <?php
                        if (isset($a_csyt) && !empty($a_csyt)) {
                            foreach ($a_csyt as $index) {
                                $csyt_id = $index->id;
                                $csyt_name = $index->name;
                                $csyt_code = $index->csyt_code;
                                if ($csyt_bt == $csyt_id) {
                                    echo $csyt_code . ' - ' . $csyt_name;
                                    break;
                                }
                            }
                        }
                        ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Phương thức BT:</label>
                        <?php echo $phuongthuc_bt_txt ?>
                    </div>

                    <hr>
                    <div class="row mt20">
                        <label class="col-sm-4">Hình thức nhận BT:</label>
                        <?php echo $hinhthuc_nhantien_bt_txt ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">NTH - Họ và tên: </label>
                        <?php echo $nth_fullname ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">NTH - Ngân hàng:</label>
                        <?php
                        if (isset($a_nh) && !empty($a_nh)) {
                            foreach ($a_nh as $index) {
                                $nh_id = $index->id;
                                $nh_name = $index->name;
                                if ($nh_id == $nth_bank) {
                                    echo $nh_name;
                                }
                            }
                        }
                        ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">NTH - STK:</label>
                        <?php echo $nth_stk ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">NTH - Ghi chú:</label>
                        <textarea cols="40" rows="6" readonly=""><?php echo $nth_bank_address ?></textarea>                        
                    </div>

                    <!--                    <div class="row mt20">
                                            <label class="col-sm-4">Thông tin tài khoản nhận BT:</label>
                    <?php // echo $ghichu_hinhthuc_nhantien_bt ?>
                    <?php // echo form_textarea(array('rows' => 5, 'id' => 'ghichu_hinhthuc_nhantien_bt', 'name' => 'ghichu_hinhthuc_nhantien_bt', 'style' => '', 'value' => isset($ghichu_hinhthuc_nhantien_bt) ? $ghichu_hinhthuc_nhantien_bt : set_value('ghichu_hinhthuc_nhantien_bt'))); ?>
                                        </div>-->

                    <?php
                    $cl = ($status == BT_CHOBOSUNG) ? 'display: none' : 'display: none';
                    $so_tien_ycbt_fm = ($status == BT_THANHTOAN || $status == BT_DATRATIEN) ? 'readonly="readonly"' : '';
                    $so_tien_dbt_fm = ($status == BT_THANHTOAN || $status == BT_DATRATIEN) ? 'readonly="readonly"' : '';
                    $so_ngay_ycbt_fm = ($status == BT_THANHTOAN || $status == BT_DATRATIEN) ? 'readonly="readonly"' : '';
                    ?>
                                    <!--                    <div class="box_content_bt1" style="<?php echo $cl ?>">
                                                            <div class="row mt20">
                                                                <label class="col-sm-4">Số tiền yêu cầu BT:</label>
                                                                <input type="text" min="0" <?php echo $so_tien_ycbt_fm ?> value="<?php echo $so_tien_ycbt ?>" name="so_tien_ycbt" class="_form-control tien_bt" id="exampleInputEmail1" placeholder="Số tiền yêu cầu bồi thường">
                                                            </div>
                                                            <div class="row mt20">
                                                                <label class="col-sm-4">Số ngày yêu cầu BT:</label>
                                                                <input type="text" min="0" <?php echo $so_ngay_ycbt_fm ?> value="<?php echo $so_ngay_ycbt ?>" name="so_ngay_ycbt" class="form-control" id="exampleInputEmail1" placeholder="">
                                                                <i style="font-size: 13px;">(Nếu quyền lợi không có số ngày, ghi là 0)</i>
                                                            </div>
                                                            <div class="row mt20">
                                                                <label class="col-sm-4">Ghi chú:</label>
                    <?php // echo form_textarea(array('rows' => 5, 'id' => 'ghichu_hoso', 'name' => 'ghichu_hoso', 'value' => ($ghichu_hoso != '') ? $ghichu_hoso : set_value('ghichu_hoso'))); ?>
                                                            </div>
                                                        </div>-->
                </div>

                <div class="col-sm-6 col-xs-12">

                    <div class="row mt20">
                        <label class="col-sm-4">Tình trạng hồ sơ:</label>
                        <span class="btn btn-warning btn-sm"><?php echo $status_txt ?></span>
                        <input type="hidden" name="goi_bh" value="<?php echo $goi_bh ?>"/>
                    </div>
                    
                    <div class="row mt20">
                        <label class="col-sm-4">Mã HSBT:</label>
                        <?php echo $bt_code ?>
                    </div>
                    
                    <div class="row mt20">
                        <label class="col-sm-4">Mã Case bồi thường:</label>
                        <?php echo $case ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Ngày xảy ra tổn thất:</label>
                        <?php echo $date_xayra_tonthat ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Ngày nhận hồ sơ BT:</label>
                        <?php echo $date_nhan_bt ?>
                    </div>
                    <?php
                    if ($status != BT_CHOBOSUNG) {
                        ?>
                        <div class="row mt20">
                            <label class="col-sm-4">Ngày xử lý hồ sơ BT:</label>
                            <?php echo $date_xuly_bt ?>
                        </div>
                        <?php
                    }
                    ?>
                    <hr>
                    <div class="row mt10">
                        <label class="col-sm-6 col-xs-12">Số tiền yêu cầu bồi thường:</label>
                        <div class="col-sm-4 col-xs-12">
                            <span class="btn btn-danger btn-sm"><?php echo get_price_in_vnd($so_tien_ycbt) ?></span>
                        </div>
                    </div>
                    <?php
                    if (in_array($quyenloi_bt, $a_qlbt_sn)) {
                        ?>
                        <div class="row mt20">
                            <label class="col-sm-6 col-xs-12">Số ngày bảo hiểm còn lại:</label>
                            <div class="col-sm-4 col-xs-12">
                                <span class="btn btn-primary btn-sm"><?php echo ($sn_bh_cl) ?> - ngày</span>
                            </div>
                        </div>
                        <div class="row mt10">
                            <label class="col-sm-6 col-xs-12">Số ngày yêu cầu bồi thường:</label>
                            <div class="col-sm-4 col-xs-12">
                                <span class="btn btn-danger btn-sm"><?php echo ($so_ngay_ycbt) ?> - ngày</span>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <div class="clear-both"></div>
                <div class="col-sm-12">
                    <div class="row mt20 box_quyenloi_bt" style="display: _none">
                        <?php
                        if (isset($bh_html)) {
                            echo $bh_html;
                        }
                        ?>
                    </div>         

                    <?php
                    if ($status == BT_CHOBOSUNG) {
                        ?>
                        <div class="row mt20">
                            <!--<label class="col-sm-4">Chứng từ chờ bổ sung:</label>-->
                            <div class="box_chungtu_bt_list col-sm-12 mb15 pt20">
                                <h3 class="main_header mt0 mb20">Chứng từ hồ sơ bồi thường</h3>
                                <?php
                                if (isset($ct) && !empty($ct)) {
                                    $i = 1;
                                    foreach ($ct as $index) {
                                        $id = $index->id;
                                        $name = $index->name;
                                        if (in_array($id, $a_ct_selected)) {
                                            $checked = 'checked';
                                        } else {
                                            $checked = '';
                                        }
                                        ?>
                                        <div class="col-sm-11 col-xs-10">
                                            <?php echo $i ?>. <?php echo $name ?>
                                        </div>
                                        <div class="col-sm-1 col-xs-2">
                                            <div class="checkbox">
                                                <label>
                                                    <input <?php echo $checked ?> class="checkbox_ct" name="ct_bt[]" type="checkbox" value="<?php echo $id ?>">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="clear-both clearfix"></div>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </div>
                            <div class="btn btn-danger btn_check_ct">Kiểm tra chứng từ</div> <i>(Click kiểm tra chứng từ trước khi lưu dữ liệu.)</i>
                        </div>

                        <div class="row mt20">
                            <label class="col-sm-4">Ghi chú chứng từ chờ bổ sung:</label>
                            <?php echo form_textarea(array('disabled' => TRUE, 'rows' => 5, 'cols' => 70, 'id' => 'chungtu_bosung_txt', 'name' => 'chungtu_bosung_txt', 'style' => '', 'value' => isset($chungtu_bosung_txt) ? $chungtu_bosung_txt : set_value('chungtu_bosung_txt'))); ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="clear-both col-sm-12"><hr></div>
                <div class="form-group">
                    <label class="col-sm-4 col-xs-12" for="exampleInputEmail1">Mã Case bồi thường:</label>
                    <div class="col-sm-8 col-xs-12 pl0">
                        <input type="text" name="case" value="<?php echo $case ?>"/>
                        <!--<i>(Nhập mã Case nếu bồi thường là Case con)</i>-->
                    </div>
                </div>

                <div class="clear-both col-sm-12"><hr></div>

                <div class="col-sm-6 col-xs-12">
                    <div class="row mt20">
                        <label class="col-sm-4">Số tiền yêu cầu bồi thường:</label>
                        <?php echo form_input(array('name' => 'so_tien_ycbt', 'class' => 'tien_bt', 'maxlength' => '255', 'value' => isset($so_tien_ycbt) ? $so_tien_ycbt : set_value('so_tien_ycbt'))); ?>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Số ngày yêu cầu bồi thường:</label>
                        <input min="0" value="<?php echo $so_ngay_ycbt ?>" name="so_ngay_ycbt" type="number" class="" id="exampleInputEmail1" placeholder="">
                        <br>
                        <i style="font-size: 13px;">(Chỉ điền khi quyền lợi yêu cầu số ngày)</i>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">  
                    <!--                    <div class="row mt20">
                                            <label class="col-sm-4 col-xs-12">Ghi chú:</label>
                                            <textarea name="ghichu_hoso" cols="35" rows="6"></textarea>
                    <?php // echo form_textarea(array('rows' => 6, 'id' => 'ghichu_hoso', 'name' => 'ghichu_hoso', 'value' => ($ghichu_hoso != '') ? $ghichu_hoso : set_value('ghichu_hoso'))); ?>
                                        </div>-->
                </div>
                <div class="clear-both col-sm-12"><hr></div>                
                <div class="col-sm-6 col-xs-12">
                    <div class="row mt20">
                        <label class="col-sm-4">Chọn cơ sở y tế:</label>
                        <select name="csyt" class="js-example-disabled-results" style="width: 400px">
                            <option value="">-- Cơ sở y tế --</option>
                            <?php
                            if (isset($a_csyt) && !empty($a_csyt)) {
                                foreach ($a_csyt as $index) {
                                    $csyt_id = $index->id;
                                    $csyt_name = $index->name;
                                    $csyt_code = $index->csyt_code;
                                    $selected = ($csyt_bt == $csyt_id) ? $selected_df : '';
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $csyt_id ?>"><?php echo $csyt_code . ' - ' . $csyt_name ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <!--<a href="<?php // echo site_url('dashboard/products/style/add')         ?>" class="btn btn-primary btn-sm">Thêm CSYT</a>-->
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Phương thức BT:</label>
                        <?php // echo $phuongthuc_bt_txt ?>
                        <select name="phuongthuc_bt" class="">
                            <option value="">-- Chọn phương thức bồi thường --</option>
                            <?php
                            if (isset($a_phuongthuc_bt) && !empty($a_phuongthuc_bt)) {
                                foreach ($a_phuongthuc_bt as $k => $v) {
                                    $selected = ($k == $phuongthuc_bt) ? $selected_df : '';
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="row mt20">
                        <label class="col-sm-4">Ghi chú hồ sơ BT:</label>
                        <?php echo form_textarea(array('rows' => 5, 'id' => 'ghichu_hoso', 'name' => 'ghichu_hoso', 'style' => '', 'value' => isset($ghichu_hoso) ? $ghichu_hoso : set_value('ghichu_hoso'))); ?>
                    </div>

                </div>

                <div class="col-sm-6 col-xs-12">
                    
                    <div class="row mt20">
                        <label class="col-sm-4 col-xs-12">Hình thức nhận hồ sơ:</label>
                        <select name="phuongthuc_nhan_hs" class="">
                            <option value="">-- Hình thức nhận hồ sơ --</option>
                            <?php
                            if (isset($a_nhan_hoso) && !empty($a_nhan_hoso)) {
                                foreach ($a_nhan_hoso as $k => $v) {
                                    $selected = ($phuongthuc_nhan_hs== $k)?$selected_df:'';
                                    ?>
                                    <option <?php echo $selected?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    
                    <div class="row mt20">
                        <label class="col-sm-4">Hình thức nhận BT:</label>
                        <?php // echo $hinhthuc_nhantien_bt_txt ?>
                        <select name="hinhthuc_nhantien_bt" class="">
                            <option value="">-- Hình thức nhận tiền bồi thường --</option>
                            <?php
                            if (isset($a_nhantien_bt) && !empty($a_nhantien_bt)) {
                                foreach ($a_nhantien_bt as $k => $v) {
                                    $selected = ($k == $hinhthuc_nhantien_bt) ? $selected_df : '';
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <!--                    <div class="row mt20">
                                            <label class="col-sm-4">Thông tin tài khoản nhận BT:</label>
                    <?php // echo form_textarea(array('rows' => 5, 'id' => 'ghichu_hinhthuc_nhantien_bt', 'name' => 'ghichu_hinhthuc_nhantien_bt', 'style' => '', 'value' => isset($ghichu_hinhthuc_nhantien_bt) ? $ghichu_hinhthuc_nhantien_bt : set_value('ghichu_hinhthuc_nhantien_bt'))); ?>
                                        </div>-->

                </div>

                <div class="col-sm-12"><hr></div>
                <div class="col-sm-12">
                    <h2 class="text-center">Thông tin người thụ hưởng</h2>
                    <div class="col-sm-6 col-xs-12">
                        <div class="row mt20">
                            <label class="col-sm-4">Họ và tên:</label>
                            <input type="text" name="nth_fullname" class="" value="<?php echo $nth_fullname ?>"/>
                        </div>
                        <div class="row mt20">
                            <label class="col-sm-4">Ngân hàng:</label>
                            <select name="nth_bank" class="js-example-disabled-results">
                                <option value="">-- Chọn ngân hàng --</option>
                                <?php
                                if (isset($a_nh) && !empty($a_nh)) {
                                    foreach ($a_nh as $index) {
                                        $nh_id = $index->id;
                                        $nh_name = $index->name;
                                        $nh_code = $index->code;
                                        $selected = ($nh_id == $nth_bank) ? $selected_df : '';
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $nh_id ?>"><?php echo $nh_code . ' - ' . $nh_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="row mt20">
                            <label class="col-sm-4">Số tài khoản:</label>
                            <input type="text" name="nth_stk" class="" value="<?php echo $nth_stk ?>"/>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12">                    
                        <div class="row mt20">
                            <label class="col-sm-4 col-xs-12">Ghi chú tài khoản người thụ hưởng:</label>
                            <textarea name="nth_bank_address" cols="35" rows="6"><?php echo $nth_bank_address ?></textarea>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <br class="clear"/>
        <hr>
        <input type="submit" name="submit" value="Sửa bồi thường" class="btn btn-default"/>
        <a style="margin-left: 20px;" class="btn btn-primary" href="<?php echo site_url(ORDER_ADMIN_BASE_URL) ?>">Quay lại <span class="glyphicon glyphicon-share-alt"></span></a>
        <br class="clear"/>&nbsp;
    </div>
</form>
<div class="modal fade bs-example-modal-lg modal_ct_bt" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
</div>