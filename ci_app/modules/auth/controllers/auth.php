<?php

class Auth extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->_layout = 'layout/content_layout';
        $this->_login_layout = 'layout/signin_layout';
        $this->_options['base_url'] = base_url();
        $this->_options['uri'] = base_url() . $this->uri->uri_string();
    }

    function send_email_account() {
        error_reporting(0);
        $this->load->library('email');
        $options['content'] = 'Email khách hàng: ' . $this->input->post('email');
        $title = 'Thư đăng ký nhận thư điện tử của khách hàng exahoi';
        $config = modules::run('configurations/get_configuration', array('array' => TRUE));
        $contact_email = ($config['contact_email'] != '' && $config['contact_email'] != NULL) ? $config['contact_email'] : CONTACT_EMAIL;

        $this->email->from(EMAIL_NO_REPLY, 'Khách hàng exahoi.vn');
        $this->email->subject($title);
        $this->email->message($options['content']);
        $this->email->to($contact_email);
        if (!$this->email->send())
            echo 'có lỗi xảy ra';
        else
            echo 'ok';
    }

    //==========================================================================
    function validate_login($module = NULL) {
        // check kh đăng nhập
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        if ($ss_kh_id > 0) {
            redirect(URL_KH_DASHBOARD);
        }
        //end
        if (!$this->is_logged_in()) {
            redirect(base_url());
        } else {
            if ($module == NULL) {
                return TRUE;
            } else {
                return $this->check_permission($module);
            }
        }
    }

    //==========================================================================
    function is_logged_in() {
        if ($this->phpsession->get('is_logged_in')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //==========================================================================
    function logout() {
        $this->phpsession->clear();
//        $this->cart->destroy(); // Xoa cac items trong gio hang

        redirect('login');
    }

    //==========================================================================
    public function captcha() {
        create_security_captcha(array('context' => $this));
    }

    //==========================================================================
    function login($options = array()) {
        // Kiem tra neu da dang nhap vao he thong roi thi tu dong chuyen den dashboard
        if ($this->is_logged_in())
            redirect('/dashboard');

        if ($this->is_postback())
            if (!$this->_do_login())
                $this->_options['error'] = $this->_options['options']['error'] = $this->_last_message;

        if (isset($this->_options['error']))
            $this->_view_data['options'] = $this->_options;

        $this->_view_data['submit_uri'] = get_form_submit_by_lang($this->_lang, 'login_form');

        // Chuan bi cac the META
        $this->_view_data['title'] = __("IP_log_in") . DEFAULT_TITLE_SUFFIX;
        $this->_view_data['keywords'] = $this->_title . ' ' . $this->_keywords;
        $this->_view_data['description'] = $this->_description;
        $this->_view_data['main_content'] = $this->load->view('auth/signin_form', $this->_options, TRUE);

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_login_layout, $this->_view_data);
    }

    //==========================================================================
    private function _do_login() {
        $this->_last_message = '';
        $options = array();

        $this->form_validation->set_rules('username', __("IP_user_name"), 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', __("IP_password"), 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            $options['username'] = $this->input->post('username', TRUE);
            $options['password'] = $this->input->post('password', TRUE);
            $options['active'] = ACTIVE;
            //$options['status'] = ACTIVE;
            $options['get_row'] = TRUE;

            $user = $this->users_model->get_users($options);

            $login = TRUE;
            if (is_object($user)) {
                $ql_all = $user->ql_all;
                $ql_users = $user->ql_users;
                $ql_bbh = $user->ql_bbh;
                $ql_goi_bh = $user->ql_goi_bh;
                $ql_kh = $user->ql_kh;
                $ql_bh = $user->ql_bh;
                $ql_bt = $user->ql_bt;
                $ql_bt_nhap_hs = $user->ql_bt_nhap_hs;
                $ql_bt_xuly = $user->ql_bt_xuly;
                $ql_bt_duyet = $user->ql_bt_duyet;
                $ql_bt_thanhtoan = $user->ql_bt_thanhtoan;
                //==============================================================
                if ((trim($options['username']) === trim($user->username)) && (md5(trim($options['password'])) === trim($user->password)) && ($user->active == 1)) {
                    $this->phpsession->save('is_logged_in', TRUE);
                    $this->phpsession->save('username', $user->username);
                    $this->phpsession->save('fullname', $user->fullname);
                    $this->phpsession->save('email', $user->email);
                    $this->phpsession->save('user_id', $user->id);
                    $this->phpsession->save('level', $user->level);
                    $this->phpsession->save('role_id', $user->role_id);
                    //==========================================================
                    $this->phpsession->save('ss_ql_all', $ql_all);
                    $this->phpsession->save('ss_ql_users', $ql_users);
                    $this->phpsession->save('ss_ql_bbh', $ql_bbh);
                    $this->phpsession->save('ss_ql_goi_bh', $ql_goi_bh);
                    $this->phpsession->save('ss_ql_kh', $ql_kh);
                    $this->phpsession->save('ss_ql_bh', $ql_bh);
                    $this->phpsession->save('ss_ql_bt', $ql_bt);
                    $this->phpsession->save('ss_ql_bt_nhap_hs', $ql_bt_nhap_hs);
                    $this->phpsession->save('ss_ql_bt_xuly', $ql_bt_xuly);
                    $this->phpsession->save('ss_ql_bt_duyet', $ql_bt_duyet);
                    $this->phpsession->save('ss_ql_bt_thanhtoan', $ql_bt_thanhtoan);
                    //==========================================================
//                    echo '<pre>';
//                    print_r($_SESSION);
//                    die;
                    redirect('/dashboard');
                } else {
                    $login = FALSE;
                }
            } else {
                $login = FALSE;
            }

            if (!$login) {
                $this->_last_message = '<p>' . __('IP_login_failed') . '</p>';
                return FALSE;
            }
        }
        $this->_last_message = validation_errors();
        return FALSE;
    }

    //==========================================================================
    public function check_permission($module = NULL) {
        if ($module == NULL) {
            return TRUE;
        }
        $roles_session = $this->phpsession->get('roles_roles');
        if (!empty($roles_session)) {
            if ($roles_session == AUTH_ROLES_ALL) {
                $roles = AUTH_ROLES_ALL;
                return TRUE;
            } else {
                $roles = json_decode($roles_session);
                $roles_list = array();
                foreach ($roles as $key => $value) {
                    $roles_menus = $this->roles_menus_model->get_roles_menus(array('id' => $value, 'status' => ACTIVE));
                    if (!empty($roles_menus)) {
                        $roles_list[] = $roles_menus->module;
                    }
                }
                if (!in_array($module, $roles_list)) {
                    return $this->permission_denied();
                } else {
                    return TRUE;
                }
            }
        } else {
            redirect(base_url());
        }
    }

    //==========================================================================
    public function get_roles_menus() {
        $roles_session = $this->phpsession->get('roles_roles');
        if (!empty($roles_session)) {
            if ($roles_session == AUTH_ROLES_ALL) {
                $roles = AUTH_ROLES_ALL;
                return TRUE;
            } else {
                $roles = json_decode($roles_session);
                $roles_list = array();
                foreach ($roles as $key => $value) {
                    $roles_menus = $this->roles_menus_model->get_roles_menus(array('id' => $value, 'status' => ACTIVE));
                    if (!empty($roles_menus)) {
                        $roles_list[$roles_menus->id] = convert_tags_to_array($roles_menus->url_path);
                    }
                }
                return $roles_list;
            }
        } else {
            return FALSE;
        }
    }

    //==========================================================================
    public function get_roles_menus_disabled() {
        $roles_menus = $this->get_roles_menus();
        if (!is_array($roles_menus) && $roles_menus == TRUE) {
            return array();
        } elseif (is_array($roles_menus)) {
            $roles_menus_key_enabled = array_keys($roles_menus);
            $all_roles_menus = $this->roles_menus_model->get_roles_menus(array('status' => ACTIVE));
            $roles_menus_key_disabled = array();
            if (!empty($all_roles_menus)) {
                foreach ($all_roles_menus as $key => $value) {
                    $roles_menus_key_disabled[] = $value->id;
                }
            }
            $list_menus_key = array_diff($roles_menus_key_disabled, $roles_menus_key_enabled);
            $list_menus = array();
            if (!empty($list_menus_key)) {
                foreach ($list_menus_key as $key => $value) {
                    $list_menu = $this->roles_menus_model->get_roles_menus(array('id' => $value));
                    $list_menu_array[$list_menu->id] = convert_tags_to_array($list_menu->url_path);
                }
            }
            return $list_menu_array;
        } else {
            return array();
        }
    }

    //==========================================================================
    public function permission_denied() {
//        echo "<pre>";
//        print_r('You do not have permission to access this page! <a href="javascript:window.history.back();">Click to Back</a>');
//        echo "</pre>";
//        exit();

        redirect('/dashboard/permission_denied');
    }

    // end
    //==========================================================================
    function csyt_dang_nhap($options = array()) {

        // Check csyt đã đăng nhập chưa   
        //$this->check_bbh_dangnhap();
        //end
        // When submit
        if ($this->is_postback()) {

            $this->form_validation->set_rules('username', 'Tên đăng nhập', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', __("IP_password"), 'trim|required|xss_clean');

            if ($this->form_validation->run()) {
                // Lấy dn bbh
                $pass = $this->input->post('password', TRUE);
                $data_get = array(
                    'username' => $this->input->post('username', TRUE),
                    'password' => ($pass),
                    'password_fm' => md5($pass),
                    'get_row' => TRUE,
                    'status' => ACTIVE,
                );

                $this->load->model('products/products_style_model');
                $csyt = $this->products_style_model->get_products_style($data_get);

                if (is_object($csyt)) {
                    $csyt_name = $csyt->name;
                    $csyt_username = $csyt->username;
                    $csyt_id = $csyt->id;
                    $status = $csyt->status;
                    $email = $csyt->email;
                    $password = $csyt->password;
                    $parent_id = $csyt->parent_id;

                    // Check status
                    if ($status == DEACTIVE) {
                        $msg = 'Tài khoản của bạn hiện đang bị khóa, vui lòng liên hệ với chúng tôi.';
                    } else if ($status == ACTIVE) {
                        // Lưu ss kh
                        $this->phpsession->save('ss_csyt_email', $email);
                        $this->phpsession->save('ss_csyt_password', $password);
                        $this->phpsession->save('ss_csyt_name', $csyt_name);
                        $this->phpsession->save('ss_csyt_username', $csyt_username);
                        $this->phpsession->save('ss_csyt_id', $csyt_id);
                        $this->phpsession->save('ss_csyt_parent_id', $parent_id);

                        //$this->phpsession->save('is_logged_in', TRUE);
                        //end

                        redirect(URL_CSYT_DASHBOARD);
                    } else {
                        $msg = 'Tài khoản của bạn hiện đang bị khóa, vui lòng liên hệ với chúng tôi.';
                    }
                } else {
                    $msg = 'Thông tin đăng nhập không chính xác, vui lòng kiểm tra lại.';
                }
            } else {
                $msg = validation_errors();
            }
        }

        $this->_options['error'] = $this->_options['options']['error'] = $msg;

        if (isset($this->_options['error']))
            $this->_view_data['options'] = $this->_options;

        //$this->_view_data['submit_uri'] = get_form_submit_by_lang($this->_lang, 'form_login_kh');
        // Chuan bi cac the META
        $this->_view_data['title'] = __("IP_log_in") . DEFAULT_TITLE_SUFFIX;
        $this->_view_data['keywords'] = $this->_title . ' ' . $this->_keywords;
        $this->_view_data['description'] = $this->_description;
        $this->_view_data['main_content'] = $this->load->view('auth/form_login_csyt', $this->_options, TRUE);

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_login_layout, $this->_view_data);
    }

    //==========================================================================
    function check_bbh_dangnhap() {

        $ss_bbh_id = $this->phpsession->get('ss_bbh_id');
        $ss_bbh_email = $this->phpsession->get('ss_bbh_email');

        if ($ss_bbh_id > 0 && $ss_bbh_email != '') {
            redirect(URL_BBH_DASHBOARD);
        } else {
            return FALSE;
        }
    }

    //==========================================================================
    function bbh_dang_nhap($options = array()) {

        // Kiem tra neu da dang nhap vao he thong roi thi tu dong chuyen den dashboard
        //if ($this->is_logged_in()) redirect('/dashboard');
        // Check kh đã đăng nhập chưa   
        $this->check_bbh_dangnhap();
        //end
        // When submit
        if ($this->is_postback()) {

            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', __("IP_password"), 'trim|required|xss_clean');

            if ($this->form_validation->run()) {
                // Lấy dn bbh
                $pass = $this->input->post('password', TRUE);
                $data_get = array(
                    'email' => $this->input->post('email', TRUE),
                    'password_fm' => md5($pass),
                    'get_row' => TRUE,
                    'status' => ACTIVE,
                );

                $this->load->model('products/products_trademark_model');

                $dn_bbh = $this->products_trademark_model->get_products_trademark($data_get);

                if (is_object($dn_bbh)) {
                    $dn_bbh_name = $dn_bbh->name;
                    $bbh_id = $dn_bbh->id;
                    $status = $dn_bbh->status;
                    $email = $dn_bbh->email;
                    $password = $dn_bbh->password;
                    $parent_id = $dn_bbh->parent_id;

                    // Check status
                    if ($status == DEACTIVE) {
                        $msg = 'Tài khoản của bạn hiện đang bị khóa, vui lòng liên hệ với chúng tôi.';
                    } else if ($status == ACTIVE) {
                        // Lưu ss kh
                        $this->phpsession->save('ss_bbh_email', $email);
                        $this->phpsession->save('ss_bbh_password', $password);
                        $this->phpsession->save('ss_bbh_name', $dn_bbh_name);
                        $this->phpsession->save('ss_bbh_id', $bbh_id);
                        $this->phpsession->save('ss_bbh_parent_id', $parent_id);

                        //$this->phpsession->save('is_logged_in', TRUE);
                        //end

                        redirect(URL_BBH_DASHBOARD);
                    } else {
                        $msg = 'Tài khoản của bạn hiện đang bị khóa, vui lòng liên hệ với chúng tôi.';
                    }
                } else {
                    $msg = 'Thông tin đăng nhập không chính xác, vui lòng kiểm tra lại.';
                }
            } else {
                $msg = validation_errors();
            }
        }

        $this->_options['error'] = $this->_options['options']['error'] = $msg;

        if (isset($this->_options['error']))
            $this->_view_data['options'] = $this->_options;

        //$this->_view_data['submit_uri'] = get_form_submit_by_lang($this->_lang, 'form_login_kh');
        // Chuan bi cac the META
        $this->_view_data['title'] = __("IP_log_in") . DEFAULT_TITLE_SUFFIX;
        $this->_view_data['keywords'] = $this->_title . ' ' . $this->_keywords;
        $this->_view_data['description'] = $this->_description;
        $this->_view_data['main_content'] = $this->load->view('auth/form_login_bbh', $this->_options, TRUE);

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_login_layout, $this->_view_data);
    }

    //==========================================================================
    function kh_dangnhap($options = array()) {
        // Check kh đã đăng nhập chưa   
        $this->check_kh_dangnhap();
        //======================================================================
        // When submit
        if ($this->is_postback()) {

            $this->form_validation->set_rules('email', 'Email / CMT', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', __("IP_password"), 'trim|required|xss_clean');

            if ($this->form_validation->run()) {
                // Lấy kh
                $pass = $this->input->post('password', TRUE);
                $email = $this->input->post('email', TRUE);
                $data_get = array(
                    'email' => $email,
                    'mst_cmt' => $email,
                    'password' => md5($pass),
                    'get_row' => TRUE,
                    'status' => ACTIVE,
                );
                //==============================================================
                $this->load->model('customers/customers_model');

                $kh = $this->customers_model->get_kh_by_options($data_get);
//                echo '<pre>';
//                print_r($kh);
//                die;
                if (is_object($kh)) {
//                    echo '<pre>';
//                    print_r($kh);
//                    die;
                    $congty_canhan = $kh->congty_canhan;
                    $kh_id = $kh->id;
                    $status = $kh->status;
                    $email = $kh->email;
                    $password = $kh->password;
                    $username = $kh->username;
                    $ngaysinh = $kh->ngaysinh;
                    $phanloai_kh = $kh->phanloai_kh;
                    $mst_cmt = $kh->mst_cmt;
                    $diachi = $kh->diachi;
                    $ngaycap = $kh->ngaycap;
                    $noicap = $kh->noicap;
                    $dienthoai = $kh->dienthoai;
                    $didong = $kh->didong;
                    $phanloai_kh1 = $kh->phanloai_kh1;
                    $parent_id = $kh->parent_id;
                    $phongban = $kh->phongban;
                    
                    $sothe_bh = $kh->sothe_bh;
                    $ma_nv = $kh->code;
                    // Check status
                    if ($status == DEACTIVE) {
                        $msg = 'Tài khoản của bạn hiện đang bị khóa, vui lòng liên hệ với chúng tôi.';
                    } else if ($status == ACTIVE) {
                        // Lưu ss kh
                        $this->phpsession->save('ss_kh_email', $email);
                        $this->phpsession->save('ss_kh_username', $username);
                        $this->phpsession->save('ss_kh_password', $password);
                        $this->phpsession->save('ss_kh_name', $congty_canhan);
                        $this->phpsession->save('ss_kh_id', $kh_id);
                        $this->phpsession->save('ss_kh_dob', $ngaysinh);
                        $this->phpsession->save('ss_kh_phanloai_kh', $phanloai_kh);
                        $this->phpsession->save('ss_kh_fullname', $congty_canhan);
                        $this->phpsession->save('ss_kh_mst_cmt', $mst_cmt);
                        $this->phpsession->save('ss_kh_diachi', $diachi);
                        $this->phpsession->save('ss_kh_ngaycap', $ngaycap);
                        $this->phpsession->save('ss_kh_noicap', $noicap);
                        $this->phpsession->save('ss_kh_dienthoai', $dienthoai);
                        $this->phpsession->save('ss_kh_didong', $didong);
                        $this->phpsession->save('ss_kh_phanloai_kh1', $phanloai_kh1);
                        $this->phpsession->save('ss_kh_parent_id', $parent_id);
                        $this->phpsession->save('ss_kh_phongban', $phongban);
                        $this->phpsession->save('ss_kh_sothe_bh', $sothe_bh);
                        $this->phpsession->save('ss_kh_ma_nv', $ma_nv);

                        $this->phpsession->save('is_logged_in', TRUE);
                        //end
                        redirect('quan-tri-khach-hang');
                    } else {
                        $msg = 'Tài khoản của bạn hiện đang bị khóa, vui lòng liên hệ với chúng tôi.';
                    }
                } else {
                    $msg = 'Thông tin đăng nhập không chính xác, vui lòng kiểm tra lại.';
                }
            } else {
                $msg = validation_errors();
            }
        }

        $this->_options['error'] = $this->_options['options']['error'] = $msg;

        if (isset($this->_options['error']))
            $this->_view_data['options'] = $this->_options;

        //$this->_view_data['submit_uri'] = get_form_submit_by_lang($this->_lang, 'form_login_kh');
        // Chuan bi cac the META
        $this->_view_data['title'] = __("IP_log_in") . DEFAULT_TITLE_SUFFIX;
        $this->_view_data['keywords'] = $this->_title . ' ' . $this->_keywords;
        $this->_view_data['description'] = $this->_description;
        $this->_view_data['main_content'] = $this->load->view('auth/form_login_kh', $this->_options, TRUE);

        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_login_layout, $this->_view_data);
    }

    //==========================================================================
    function check_kh_dangnhap() {

        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_email = $this->phpsession->get('ss_kh_email');

        if ($ss_kh_id > 0 && $ss_kh_email != '') {
            redirect(URL_DASHBOARD);
        } else {
            return FALSE;
        }
    }

    //==========================================================================
    function check_kh_dangnhap1() {

        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        $ss_kh_email = $this->phpsession->get('ss_kh_email');
        // ss dn bbh 
        $ss_bbh_id = $this->phpsession->get('ss_bbh_id');
        $ss_bbh_email = $this->phpsession->get('ss_bbh_email');
        // ss csyt
        $ss_csyt_id = $this->phpsession->get('ss_csyt_id');
        $ss_csyt_email = $this->phpsession->get('ss_csyt_email');
        // check dn bbh đăng nhập
        if ($ss_csyt_id > 0 && $ss_csyt_email != '') {
            return TRUE;
        } elseif ($ss_bbh_id > 0 && $ss_bbh_email != '') {
            return TRUE;
        } else if ($ss_kh_id > 0 && $ss_kh_email != '') {
            return TRUE;
        } else {
            redirect();
        }
    }

    //==========================================================================
    // KH thoát
    function kh_logout() {
        $ss_kh_id = $this->phpsession->get('ss_kh_id');
        //if ($ss_kh_id > 0) {
        $this->phpsession->clear();
        //redirect(URL_KH_DANGNHAP);
        redirect('');
        //}
        redirect(base_url());
    }

}

?>
