<?php
$a_ct_selected = isset($a_ct_selected) ? $a_ct_selected : array();
?>
<!--<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">-->
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content box_about_home col-sm-12">
        <div class="modal-header"> 
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button> 
            <h3 class="modal-title mb0" id="myLargeModalLabel">Gửi email cho khách hàng bổ sung chứng từ.</h3> 
        </div>
        <div class="modal-body">
            <textarea class="form-control wysiwyg elm1" rows="10" cols="7" wrap="off">
                    Chứng từ cần bổ sung:
                <?php
                //$a_ct_selected= array(7,9);
                if (isset($ct) && !empty($ct)) {
                    $i = 1;
                    foreach ($ct as $index) {
                        $id = $index->id;
                        $name = $index->name;
                        if (!in_array($id, $a_ct_selected)) {
                            ?>
                            <p><?php echo trim($i . '. ' . $name) ?></p>
                            <?php
                            $i++;
                        }else{
                        }
                    }
                }
                ?>
            </textarea>                          
        </div>
        <div class="modal-footer"> 
            <button type="button" class="btn btn-primary btn_sent_email_ct_bt" data-dismiss="modal">Gửi email tới khách hàng</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        </div>
    </div>
</div>
<!--</div>-->