<?php
$ss_bbh_email = $this->phpsession->get('ss_bbh_email');
$ss_bbh_name = $this->phpsession->get('ss_bbh_name');
//end
?>
<?php
$so_kh_dbh = $t_sotien_bh = $t_sotien_dbt = 0;
if (isset($bh) && !empty($bh)) {
    $so_kh_dbh = count($bh);
    foreach ($bh as $index) {
        $phi_bbh = $index->phi_bbh;
        $t_sotien_bh = (int) ($t_sotien_bh) + (int) $phi_bbh;
    }
}
?>
<?php
if (isset($bt) && !empty($bt)) {
//    echo '<pre>';
//    print_r($bt);
//    die;
    //$t_sotien_ycbt = $t_sotien_dbt = $t_sotien_bt_nv = $t_sotien_bt_nt = $t_goi_gia_tien = $t_hs_dbt = $t_hs_cbt = 0;
    foreach ($bt as $index) {
        //$so_tien_ycbt = ($index->so_tien_ycbt);
        $so_tien_dbt = ($index->so_tien_dbt);
        //
        //$t_sotien_ycbt = (int) ($t_sotien_ycbt) + (int) $so_tien_ycbt;
        //echo $t_sotien_ycbt.'//';
        $t_sotien_dbt = (int) ($t_sotien_dbt) + (int) $so_tien_dbt;
    }
}
?>
<div class="row">
    <div class="col-xl-6">
        <div class="chart-statistic-box">            
            <div class="embed-responsive embed-responsive-16by9 video_db">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/dF3Dcol5XLg"></iframe>
            </div>

        </div><!--.chart-statistic-box-->
    </div><!--.col-->
    <div class="col-xl-6">
        <div class="row">
            <div class="col-sm-6">
                <article class="statistic-box red">
                    <div>
                        <div class="number"><?php echo $so_kh_dbh ?></div>
                        <div class="caption"><div>Số người được Bảo Hiểm</div></div>
                    </div>
                </article>
            </div><!--.col-->
            <div class="col-sm-6">
                <article class="statistic-box purple">
                    <div>
                        <div class="number"><?php echo get_price_in_vnd($t_sotien_bh) ?></div>
                        <div class="caption"><div>Số tiền Bảo Hiểm</div></div>
                    </div>
                </article>
            </div><!--.col-->
            <div class="col-sm-6">
                <article class="statistic-box yellow">
                    <div>
                        <div class="number"><?php echo get_price_in_vnd($t_sotien_dbt) ?></div>
                        <div class="caption"><div>Số tiền Bồi Thường</div></div>                        
                    </div>
                </article>
            </div><!--.col-->
            <div class="col-sm-6">
                <article class="statistic-box green">
                    <div>
                        <div class="number"><?php echo round((($t_sotien_dbt / $t_sotien_bh) * 100), 2) . '%'; ?></div>
                        <div class="caption"><div>Phần trăm Bồi Thường</div></div>
                    </div>
                </article>
            </div><!--.col-->
        </div><!--.row-->
    </div><!--.col-->
</div>
<table id="table-edit_" class="table table-bordered table-hover">
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Thông tin khách hàng:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th>Chỉ tiêu</th>
            <th>Giá trị</th>
            <th>Chỉ tiêu</th>
            <th>Giá trị</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Tên khách hàng</td>
            <td><?php echo $ss_bbh_name ?></td>
            <td>Email</td>
            <td><?php echo $ss_bbh_email ?></td>
        </tr>
    </tbody>
</table>
<br><br>
<table id="_table-edit" class="table table-bordered table-hover">    
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Thông tin bảo hiểm:</h3>
            <a href="<?php echo site_url(URL_BBH_BH) ?>" class="pull-right btn btn-inline btn-primary btn-sm ladda-button">Xem chi tiết</a>
        </header>
    </section>
    <thead>
        <tr>
            <th>Mã số KH</th>
            <th>Họ và tên KH</th>
            <th>CMT/Hộ chiếu</th>
            <!--<th>Công ty</th>-->
            <!--<th>Phòng ban</th>-->
            <th>Ngày hiệu lực</th>
            <th>Ngày hết hạn</th>
            <th>Gói BH</th>
            <!--<th>Bên mua BH</th>-->
            <!--<th>Mối quan hệ</th>-->
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($bh) && !empty($bh)) {
            foreach ($bh as $index) {
                $KH_id = $index->KH_id;
                $ten_kh = $index->congty_canhan;
                $mst_cmt = $index->mst_cmt;
                $parent_id = $index->parent_id;
                $phongban = $index->phongban;
                $ngay_hieuluc = $index->ngay_hieuluc;
                $ngay_ketthuc = $index->ngay_ketthuc;
                $goi_bh_dm = $index->goi_bh_dm;
                //$ten_dn_bbh = $index->ten_dn_bbh;
                //$ndbh_quanhe = $index->ndbh_quanhe;
                //$ndbh_quanhe_txt = get_ndbh_quanhe($ndbh_quanhe);
                ?>
                <tr>
                    <td><?php echo 'MSKH-' . $KH_id ?></td>
                    <td><?php echo $ten_kh ?></td>
                    <td><?php echo $mst_cmt ?></td>
                    <td><?php echo $ngay_hieuluc ?></td>
                    <td><?php echo $ngay_ketthuc ?></td>
                    <td><?php echo $goi_bh_dm ?></td>
                    <!--<td><?php // echo $ten_dn_bbh  ?></td>-->
                    <!--<td><?php // echo $ndbh_quanhe_txt  ?></td>-->
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="10" class="text-danger">Không có dữ liệu.</td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<br><br>
<table id="_table-edit" class="table table-bordered table-hover">    
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Chi tiết bồi thường:</h3>
            <a href="<?php echo site_url(URL_BBH_BT) ?>" class="pull-right btn btn-inline btn-primary btn-sm ladda-button">Xem chi tiết</a>
        </header>
    </section>
    <thead>
        <tr>
            <th>Mã số HSBT</th>
            <th>Khách hàng được BH</th>
            <th>Công ty chi nhánh</th>
            <th>Phòng ban</th>
            <th>CMT/Hộ chiếu</th>
            <th>Tình trạng GQBT</th>
            <th>Tình trạng GQHS</th>
            <th>Chứng từ chờ BS</th>
            <th>Ngày BSHS</th>
            <th>Ngày duyệt BT</th>
            <th>Loại BT</th>
            <th>Ngày XRTT</th>
            <th>Mối quan hệ</th>
            <th>Phương thức BT</th>
            <th>Ngày nhận HS</th>
            <th>Ngày XLBT</th>
            <th>Ngày TBKH</th>
            <th>Chuẩn đoán bệnh</th>
            <th>Mã bệnh</th>
            <th>Số tiền TTTCSYT</th>
            <th>Số tiền ĐBT</th>
            <th>Số tiền từ chối</th>
            <th>Lý do KBT</th>
            <th>Hình thức NTBT</th>
            <th>Ghi chú TT</th>
            <th>Ngày TT</th>
            <th>Phí BH</th>
            <th>CSYT</th>
            <th>Công ty BH</th>
            <th>Hồ sơ scan</th>
            <!--end-->
            <!--<th>Ngày xảy ra tổn thất</th>-->
            <!--<th>Ngày điều trị</th>-->
            <!--<th>Cơ sở y tế</th>-->
            <!--<th>Loại bệnh</th>-->
            <!--<th>Quyền lợi BH</th>-->
            <!--<th>Ngày YCBT</th>-->
            <!--<th>Số tiền YCBT</th>-->
            <!--<th>Số ngày YCBT</th>-->
            <!--<th>Số tiền được BT</th>-->
            <!--<th>Thời gian giải quyết</th>-->
            <th>Xuất PDF</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($bt) && !empty($bt)) {
            $i = 1;
            foreach ($bt as $index) {
                $id = $index->id;
                $OD_id = $index->id;
                $loai_benh = $index->loai_benh;
                $loai_benh_txt = get_ma_benh($loai_benh);
                $cn_dbh = $index->congty_canhan;
                $ngaysinh = $index->ngaysinh;
                $parent_id = $index->parent_id;
                $phongban = $index->phongban;
                $goi_bh_dm = $index->goi_bh_dm;
                $so_ngay_ycbt = $index->so_ngay_ycbt;
                //$price = $product->price != 0 ? get_price_in_vnd($product->price) . ' ₫' : get_price_in_vnd($product->price);        
                $so_tien_ycbt = get_price_in_vnd($index->so_tien_ycbt);
                $so_ngay_ycbt = $index->so_ngay_ycbt;
                $so_tien_dbt = get_price_in_vnd($index->so_tien_dbt);
                $ngay_xayra_tonthat = $index->ngay_xayra_tonthat;
                $ngay_dieutri = $index->ngay_dieutri;
                $ngay_ycbt = $index->ngay_ycbt;
                $ngay_ycbt_txt = date('d/m/Y', strtotime($ngay_ycbt));
                $ngay_giaiquyet = $index->ngay_giaiquyet;
                $ngay_giaiquyet_txt = date('d/m/Y', strtotime($ngay_giaiquyet));
                $ngay_dieutri = $index->ngay_dieutri;
                $ngay_dieutri_txt = date('d/m/Y', strtotime($ngay_dieutri));
                $ngay_xayra_tonthat = $index->ngay_xayra_tonthat;
                $ngay_xayra_tonthat_txt = date('d/m/Y', strtotime($ngay_xayra_tonthat));
                //end
                $mst_cmt = $index->mst_cmt;
                $order_status = $index->order_status;
                $order_status_txt = get_orders_status($order_status);
                if ($order_status != DA_BOI_THUONG) {
                    $order_status_txt1 = '<button class="btn btn-warning btn-sm" href="#">' . $order_status_txt . '</button>';
                } else {
                    $order_status_txt1 = '<button class="btn btn-primary btn-sm" href="#">' . $order_status_txt . '</button>';
                }
                $tinhtrang_hoso = $index->tinhtrang_hoso;
                $chungtu_bosung = $index->chungtu_bosung;
                if ($chungtu_bosung != '') {
                    $chungtu_bosung1 = '<a class="btn btn-warning btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg' . $OD_id . '" href="#">Chứng từ BS</a>';
                } else {
                    $chungtu_bosung1 = '';
                }
                $ngay_bosung_hoso = $index->ngay_bosung_hoso;
                $ngay_duyet_bt = $index->ngay_duyet_bt;
                $ql_bh = $index->ql_bh;
                //$ql_bh_txt = ($ql_bh == NOITRU) ? 'Nội trú' : 'Ngoại trú';
                $ql_bh_txt = get_qlbh($ql_bh);
                $ngay_xayra_tonthat = $index->ngay_xayra_tonthat;
                $ndbh_quanhe = $index->ndbh_quanhe;
                $ndbh_quanhe_txt = get_ndbh_quanhe($ndbh_quanhe);
                $phuongthuc_bt = $index->phuongthuc_bt;
                $phuongthuc_bt_txt = get_phuongthuc_bt($ndbh_quanhe);
                $ngay_nhan_hoso = $index->ngay_nhan_hoso;
                $ngay_xuly_bt = $index->ngay_xuly_bt;
                $ngay_thongbao_kh = $index->ngay_thongbao_kh;
                $chuandoan_benh = $index->chuandoan_benh;
                $OD_ma_benh = $index->OD_ma_benh;
                $so_tien_thanhtoan_csyt = $index->so_tien_thanhtoan_csyt;
                $so_tien_tuchoi = $index->so_tien_tuchoi;
                $lydo_khong_bt = $index->lydo_khong_bt;
                $ghichu_thanhtoan = $index->ghichu_thanhtoan;
                $hinhthuc_nhantien_bt = $index->hinhthuc_nhantien_bt;
                $OD_ngay_thanhtoan = $index->OD_ngay_thanhtoan;
                $goi_gia_tien = ($index->goi_gia_tien);
                $csyt = $index->csyt;
                $ten_csyt = $index->ten_csyt;
                $ten_dn_bbh = $index->ten_dn_bbh;
                $hoso_scan = $index->hoso_scan;
                if ($i < 15) {
                    ?>
                    <tr>
                        <td><?php echo 'HSBT-' . $OD_id ?></td>
                        <td><?php echo $cn_dbh ?></td>
                        <td>
                            <?php
                            //echo $parent_id.'???';
                            if ($parent_id > 0) {
                                if (isset($dn) && !empty($dn)) {
                                    foreach ($dn as $index) {
                                        $dn_ten = $index->congty_canhan;
                                        $dn_id = $index->id;
                                        if ($dn_id == $parent_id) {
                                            echo $dn_ten;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                echo '';
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($parent_id > 0) {
                                if (isset($a_phongban) && !empty($a_phongban)) {
                                    foreach ($a_phongban as $index) {
                                        $pb_name = $index->name;
                                        $pb_id = $index->id;
                                        if ($pb_id == $phongban) {
                                            echo $pb_name;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                echo '';
                            }
                            ?>
                        </td>
                        <td><?php echo $mst_cmt ?></td>
                        <td>
                            <?php
                            echo $order_status_txt1;
                            ?>
                        </td>
                        <td><?php echo $tinhtrang_hoso ?></td>
                        <td>
                            <?php
                            echo $chungtu_bosung1;
                            if ($chungtu_bosung != '') {
                                ?>
                                <div class="modal fade bs-example-modal-lg<?php echo $OD_id ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content box_about_home _col-sm-12">
                                            <div class="modal-header"> 
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span></button> 
                                                <h3 class="modal-title mb0" id="myLargeModalLabel">Báo cáo tình trạng hồ sơ bồi thường.</h3> 
                                            </div>
                                            <div class="modal-body modal_bt">
                                                <div class="row mb15">
                                                    <h3 class="main_header">Thông tin bảo hiểm</h3>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="col-sm-4">
                                                            <strong>Mã KH:</strong>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <span>MKH-UIC <?php echo $KH_id; ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="col-sm-4">
                                                            <strong>Họ tên:</strong>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <span><?php echo $cn_dbh ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="col-sm-4">
                                                            <strong>CMT/Hộ Chiếu:</strong>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <span><?php echo $mst_cmt ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="col-sm-4">
                                                            <strong>Ngày sinh:</strong>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <span><?php echo $ngaysinh ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb15">
                                                    <h3 class="main_header">Thông tin bồi thường</h3>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="col-sm-4">
                                                            <strong>Mã HSBT:</strong>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <span>HSBT- <?php echo $id ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="col-sm-4">
                                                            <strong>Tình trạng:</strong>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <span><?php echo $order_status_txt ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="col-sm-4">
                                                            <strong>Ngày nhận:</strong>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <span><?php echo $ngay_nhan_hoso ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="col-sm-4">
                                                            <strong>Người GQ:</strong>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <span>Đỗ Thị Mùa</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb15">
                                                    <h3 class="main_header">Chứng từ hồ sơ bồi thường <span class="fr mr20 ml15">Không</span><span class="fr mr20">Có</span></h3>
                                                    <div class="col-sm-10 col-xs-10">
                                                        <ul class="list_hsbt_bs">
                                                            <li>
                                                                1. Giấy yêu cầu bồi thường đã điền đầy đủ thông tin
                                                            </li>
                                                            <li>
                                                                2. Bản gốc hóa đơn/phiếu thu (cần có hóa đơn VAT nếu chi phí trên 200.000 vnđ)
                                                            </li>
                                                            <li>
                                                                3. Bản kê chi tiết tiền viện phí (bao gồm thông tin chi tiết về từng loại thuốc và thủ thuật y tế)
                                                            </li>
                                                            <li>
                                                                4. Báo cáo của bác sỹ (nêu rõ ngày đầu tiên xuất hiện bệnh, tiền sử bệnh, chuẩn đoán,...)
                                                            </li>
                                                            <li>
                                                                5. Giấy xuất viện
                                                            </li>
                                                            <li>
                                                                6. Đơn thuốc (có chữ ký của bác sỹ và dấu của bệnh viện/phòng khám)
                                                            </li>
                                                            <li>
                                                                7. Bằng lái xe do Sở GTVT cấp (trong trường hợp tai nạn)
                                                            </li>
                                                            <li>
                                                                8. Biên bản tai nạn công an (trong trường hợp tai nạn)
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-sm-2 col-xs-2">
                                                        <?php
                                                        if ($OD_id % 2 == 0) {
                                                            ?>
                                                            <ul class="list_hsbt_bs">
                                                                <li>
                                                                    <span class="glyphicon glyphicon-ok text-success"></span>
                                                                    <!--<span class="glyphicon glyphicon-ok" style="margin-left: 60px;"></span>-->
                                                                </li>
                                                                <li>
                                                                    <span class="glyphicon glyphicon-ok text-success"></span>
                                                                    <!--<span class="glyphicon glyphicon-ok" style="margin-left: 60px;"></span>-->
                                                                </li>
                                                                <li>
                                                                    <!--<span class="glyphicon glyphicon-ok text-success"></span>-->
                                                                    <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
                                                                </li>
                                                                <li>
                                                                    <!--<span class="glyphicon glyphicon-ok text-success"></span>-->
                                                                    <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
                                                                </li>
                                                                <li>
                                                                    <!--<span class="glyphicon glyphicon-ok text-success"></span>-->
                                                                    <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
                                                                </li>
                                                                <li>
                                                                    <span class="glyphicon glyphicon-ok text-success"></span>
                                                                    <!--<span class="glyphicon glyphicon-ok" style="margin-left: 60px;"></span>-->
                                                                </li>
                                                                <li>
                                                                    <!--<span class="glyphicon glyphicon-ok text-success"></span>-->
                                                                    <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
                                                                </li>
                                                                <li>
                                                                    <span class="glyphicon glyphicon-ok text-success"></span>
                                                                    <!--<span class="glyphicon glyphicon-ok" style="margin-left: 60px;"></span>-->
                                                                </li>
                                                            </ul>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <ul class="list_hsbt_bs">
                                                                <li>
                                                                    <!--<span class="glyphicon glyphicon-ok text-success"></span>-->
                                                                    <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
                                                                </li>
                                                                <li>
                                                                    <span class="glyphicon glyphicon-ok text-success"></span>
                                                                    <!--<span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>-->
                                                                </li>
                                                                <li>
                                                                    <span class="glyphicon glyphicon-ok text-success"></span>
                                                                    <!--<span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>-->
                                                                </li>
                                                                <li>
                                                                    <span class="glyphicon glyphicon-ok text-success"></span>
                                                                    <!--<span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>-->
                                                                </li>
                                                                <li>
                                                                    <!--<span class="glyphicon glyphicon-ok text-success"></span>-->
                                                                    <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
                                                                </li>
                                                                <li>
                                                                    <span class="glyphicon glyphicon-ok text-success"></span>
                                                                    <!--<span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>-->
                                                                </li>
                                                                <li>
                                                                    <span class="glyphicon glyphicon-ok text-success"></span>
                                                                    <!--<span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>-->
                                                                </li>
                                                                <li>
                                                                    <!--<span class="glyphicon glyphicon-ok text-success"></span>-->
                                                                    <span class="glyphicon glyphicon-remove text-danger" style="margin-left: 60px;"></span>
                                                                </li>
                                                            </ul>
                                                            <?php
                                                        }
                                                        ?>

                                                    </div>
                                                </div>
                                                <div class="row mb15">
                                                    <h3 class="main_header">Thông tin xử lý</h3>
                                                    <div class="col-sm-12 col-xs-12">
                                                        <?php
                                                        if ($tinhtrang_hoso != '') {
                                                            echo $tinhtrang_hoso;
                                                        } else {
                                                            echo '<p>Hồ sơ thiếu giấy xuất viện, đã liên hệ với khách hàng bổ sung ngày 12/10/2016</p>';
                                                        }
                                                        ?>                                                    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button></div>
                                        </div>
                                    </div>
                                </div>                
                                <?php
                            }
                            ?>
                        </td>
                        <td><?php echo $ngay_bosung_hoso ?></td>
                        <td><?php echo $ngay_duyet_bt ?></td>
                        <td><?php echo $ql_bh_txt ?></td>
                        <td><?php echo $ngay_xayra_tonthat ?></td>
                        <td><?php echo $ndbh_quanhe_txt ?></td>
                        <td><?php echo $phuongthuc_bt_txt ?></td>
                        <td><?php echo $ngay_nhan_hoso ?></td>
                        <td><?php echo $ngay_xuly_bt ?></td>
                        <td><?php echo $ngay_thongbao_kh ?></td>
                        <td><?php echo $chuandoan_benh ?></td>
                        <td><?php echo $OD_ma_benh ?></td>
                        <td><?php echo $so_tien_thanhtoan_csyt ?></td>
                        <td><?php echo $so_tien_dbt ?></td>
                        <td><?php echo $so_tien_tuchoi ?></td>
                        <td><?php echo $lydo_khong_bt ?></td>
                        <td><?php echo $hinhthuc_nhantien_bt ?></td>
                        <td><?php echo $ghichu_thanhtoan ?></td>
                        <td><?php echo $OD_ngay_thanhtoan ?></td>
                        <td><?php echo $goi_gia_tien ?></td>
                        <td><?php echo $ten_csyt ?></td>
                        <td><?php echo $ten_dn_bbh ?></td>
                        <td><?php echo $hoso_scan ?></td>
                        <!--end-->
                        <!--<td><?php // echo $ngay_xayra_tonthat_txt  ?></td>-->
                        <!--<td><?php // echo $ngay_dieutri_txt  ?></td>-->
                        <!--<td><?php // echo $ten_csyt  ?></td>-->
                        <!--<td><?php // echo $loai_benh_txt  ?></td>-->
                        <!--<td><?php // echo $ql_bh_txt  ?></td>-->
                        <!--<td><?php // echo $ngay_ycbt_txt  ?></td>-->
                        <!--<td><?php // echo $so_tien_ycbt  ?></td>-->
                        <!--<td><?php // echo $so_ngay_ycbt  ?></td>-->
                        <!--<td><?php // echo $so_tien_dbt  ?></td>-->
                        <!--<td><?php // echo $ngay_giaiquyet_txt  ?></td>-->
                        <td>
                            <a href="javascript:void(0)" class="btn btn-primary btn-sm">
                                Xuất PDF
                            </a>
                        </td>
                    </tr>
                    <?php
                }
                $i++;
            }
        } else {
            ?>
            <tr>
                <td colspan="10" class="text-danger">Không có dữ liệu.</td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>