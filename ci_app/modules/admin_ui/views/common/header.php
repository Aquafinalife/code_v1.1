<?php echo doctype('xhtml1-trans'); ?>
<html>
    <meta name="keywords" content="<?php if (isset($meta_keywords)) echo $meta_keywords; else echo DEFAULT_ADMIN_KEYWORDS; ?>" />
    <meta name="description" content="<?php if (isset($meta_description)) echo $meta_description; else echo DEFAULT_ADMIN_DESCRIPTION; ?>" />
    <meta name="title" content="<?php if (isset($title)) echo $title; else echo DEFAULT_ADMIN_TITLE; ?>" />
    <meta name="robots" content="noindex,nofollow" />
    <meta name="author" content="www.lamwebchuanseo.com" />
    <meta name="copyright" content="Copyright by PowerCMS. All rights reserved." />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php if (isset($title)) echo $title; else echo DEFAULT_ADMIN_TITLE; ?></title>
    <?php $config = get_cache('configurations_' .  get_language()); ?>
    <?php if(!empty($config)){
        $favicon = !empty($config['favicon'])?base_url().UPLOAD_URL_FAVICON.$config['favicon']:'/powercms/images/favicon.png';
    } ?>
    
    <link rel="icon" href="<?php echo $favicon; ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo $favicon; ?>" type="image/x-icon">
    
    <link rel="stylesheet" type="text/css" href="/libs/bootstrap/css/bootstrap.min.css" media="screen" />    
    <link rel="stylesheet" type="text/css" href="/plugins/fa/css/font-awesome.min.css" />    
    
    <link rel="stylesheet" type="text/css" href="/plugins/xdsoft_datetimepicker/jquery.datetimepicker.css" />
    
    <link rel="stylesheet" type="text/css" href="/plugins/select2/css/select2.min.css" />
    
    <link rel="stylesheet" type="text/css" href="/powercms/css/admin.css" />
    
    {IMPORT_CSS}
</head>
<body>
