<?php

class Products_Size_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        //modules::run('auth/auth/validate_login');
        $this->_layout = 'admin_ui/layout/main';
        $this->ad_check_login();
        //======================================================================
        $ss_ql_all = $this->phpsession->get('ss_ql_all');
        $ss_ql_goi_bh = $this->phpsession->get('ss_ql_goi_bh');
        if ($ss_ql_all != QL_CHECKED) {
            if ($ss_ql_goi_bh != QL_CHECKED) {
                redirect('dashboard');
            }
        }
    }

    //==========================================================================
    function browse() {
        $options = array();

        $options['products_size'] = $this->products_size_model->get_products_size();

        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $options['options'] = $options;
        }
        //======================================================================
        $this->_view_data['main_content'] = $this->load->view('admin/products_size/list', $options, TRUE);
        $this->_view_data['title'] = 'Quản lý Gói bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function add() {
        $options = array();
        // Xu ly viec them vao db
        if ($this->is_postback()) {
            if (!$this->_do_add()) {
                $options['error'] = validation_errors();
                $options['options'] = $options;
            }
        }
        //======================================================================
        $this->_get_form_data($options);
        //======================================================================
        $options['header'] = 'Thêm Gói bảo hiểm';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_SIZE_ADMIN_ADD_URL;
        $tpl = 'ad/goi_bh/goi_bh_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        // Chuan bi cac the META
        $this->_view_data['title'] = 'Thêm Gói bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _get_form_data(&$options = array()) {

        // Lấy thông tin các dn bán BH
        $data_get = array();
        $dn_bbh = $this->products_trademark_model->get_products_trademark($data_get);

        if (!empty($dn_bbh)) {
            $options['a_dn_bbh'] = $dn_bbh;
        }
        //end
        //======================================================================
        // Lấy các KH doanh nghiệp
        $data_get = array(
            'phanloai_kh' => DOANHNGHIEP,
        );
        $kh_dn = $this->customers_model->get_kh($data_get);
        if(!empty($kh_dn)){
            $options['a_dn']= $kh_dn;
        }
        //======================================================================
        $options['scripts'] = $this->scripts_select2();
    }

    //==========================================================================
    private function _do_add() {

        $this->form_validation->set_rules('name', 'Gói bảo hiểm', 'trim|required|xss_clean');
        $this->form_validation->set_rules('code', 'Code gói BH', 'trim|required|xss_clean');
        $this->form_validation->set_rules('gia_tien', 'Giá tiền', 'trim|required|xss_clean');
        $this->form_validation->set_rules('TV_TTTBVV_TN', 'Tử vong,TTTBVV do tai nạn', 'trim|required|xss_clean');
        $this->form_validation->set_rules('TCL_NN', 'Trợ cấp lương ngày nghỉ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tcl_nn_sn', 'Số ngày - Trợ cấp lương ngày nghỉ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('CPYT_TN', 'Chi phí y tế do tai nạn', 'trim|required|xss_clean');
        $this->form_validation->set_rules('TV_TTTBVV_OB', 'Tử vong,TTTBVV do ốm bệnh', 'trim|required|xss_clean');
        $this->form_validation->set_rules('DTNT_OB', 'Điều trị nội trú (do ốm bệnh)', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('dtnt_dbh', 'Điều trị nội trú - Đồng bảo hiểm', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_tvpn', 'Tiền viện phí ngày', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_tvpn_sn', 'Số ngày / Tiền viện phí ngày', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_tgpn', 'Tiền giường,phòng / ngày', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_tgpn_sn', 'Số ngày / Tiền giường phòng', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_cppt', 'Chi phí phẫu thuật', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_tk_nv', 'Quyền lợi khám trước khi nhập viện', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_sk_nv', 'Quyền lợi khám sau khi nhập viện', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_cpyt_tn', 'Chi phí y tá tại nhà', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('dtnt_cpyt_tn_sn', 'Số ngày / Chi phí y tá tại nhà', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_tcvp', 'Trợ cấp viện phí', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_tcvp_sn', 'Số ngày / Trợ cấp viện phí', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_tcmt', 'Trợ cấp mai táng', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtnt_xct', 'Xe cứu thương', 'trim|required|xss_clean');
        $this->form_validation->set_rules('THAISAN_QLNT', 'THAI SẢN (Trong quyền lợi nội trú)', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tsqlnt_ktdk', 'Chi phí khám thai định kỳ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tsqlnt_st', 'Sinh thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tsqlnt_sm', 'Sinh mổ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tsqlnt_dn', 'Dưỡng nhi', 'trim|required|xss_clean');
        $this->form_validation->set_rules('DTNGT_OB', 'Điều trị ngoại trú (do ốm bệnh)', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('dtngt_dbh', 'Đồng bảo hiểm', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtngt_st1lk', 'Số tiền cho 1 lần khám', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtngt_slk', 'Số lần khám', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtngt_nkngt', 'Nha khoa trong Ngoại trú', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtngt_nk', 'Tiền khám nha khoa', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dtngt_cvr', 'Cạo vôi răng', 'trim|required|xss_clean');
        $this->form_validation->set_rules('THAISAN_MDL', 'THAI SẢN (mua độc lập)', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('tsmdl_dbh', 'Đồng bảo hiểm', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tsmdl_ktdk', 'Chi phí khám thai định kỳ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tsmdl_st', 'Sinh thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tsmdl_sm', 'Sinh mổ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tsmdl_dn', 'Dưỡng nhi', 'trim|required|xss_clean');
        $this->form_validation->set_rules('NHAKHOA_MDL', 'Nha khoa (mua độc lập)', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('nkmdl_dbh', 'Đồng bảo hiểm', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nkmdl_cb', 'Nha khoa cơ bản', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nkmdl_db', 'Nha khoa đặc biệt', 'trim|required|xss_clean');
        $this->form_validation->set_rules('nkmdl_cvr', 'Cạo vôi răng', 'trim|required|xss_clean');
        //
        $this->form_validation->set_rules('ql_bosung', 'Quyền lợi bổ sung', 'trim|xss_clean');
        //
        $this->form_validation->set_rules('tgc_tn', 'Thời gian chờ tai nạn', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tgc_om_tt', 'TGC Bệnh thông thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tgc_om_db', 'TGC Bệnh đặc biệt', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tgc_tv_tn', 'TGC Tử vong do tai nạn', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tgc_tv_ob', 'TGC Tử vong do ốm bệnh', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tgc_bcts', 'TGC Biến chứng thai sản', 'trim|xss_clean');
        $this->form_validation->set_rules('tgc_sinhcon', 'TGC Sinh con', 'trim|xss_clean');
        //
        $this->form_validation->set_rules('dn_bbh', 'Công ty Bảo Hiểm:', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cn_bbh', 'Chi nhánh/phòng ban', 'trim|xss_clean');
        $this->form_validation->set_rules('cn_bbh', 'Chi nhánh/phòng ban', 'trim|xss_clean');
        $this->form_validation->set_rules('dn_mbh', 'Doanh nghiệp mua BH', 'trim|xss_clean');
        //$this->form_validation->set_rules('moigioi', 'Môi giới', 'trim|required|xss_clean');
        //======================================================================
        $id = my_trim($this->input->post('id'));
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        if ($this->form_validation->run()) {
            $data = array(
                'name' => my_trim($this->input->post('name')),
                'code' => my_trim($this->input->post('code')),
                'gia_tien' => my_trim($this->input->post('gia_tien')),
                'status' => STATUS_ACTIVE,
                'creator' => $this->phpsession->get('user_id'),
                'editor' => $this->phpsession->get('user_id'),
                'TV_TTTBVV_TN' => my_trim($this->input->post('TV_TTTBVV_TN')),
                'TCL_NN' => my_trim($this->input->post('TCL_NN')),
                'tcl_nn_sn' => my_trim($this->input->post('tcl_nn_sn')),
                'CPYT_TN' => my_trim($this->input->post('CPYT_TN')),
                'TV_TTTBVV_OB' => my_trim($this->input->post('TV_TTTBVV_OB')),
                'DTNT_OB' => my_trim($this->input->post('DTNT_OB')),
                //'dtnt_dbh' => my_trim($this->input->post('dtnt_dbh')),
                'dtnt_tvpn' => my_trim($this->input->post('dtnt_tvpn')),
                'dtnt_tvpn_sn' => my_trim($this->input->post('dtnt_tvpn_sn')),
                'dtnt_tgpn' => my_trim($this->input->post('dtnt_tgpn')),
                'dtnt_tgpn_sn' => my_trim($this->input->post('dtnt_tgpn_sn')),
                'dtnt_cppt' => my_trim($this->input->post('dtnt_cppt')),
                'dtnt_tk_nv' => my_trim($this->input->post('dtnt_tk_nv')),
                'dtnt_sk_nv' => my_trim($this->input->post('dtnt_sk_nv')),
                'dtnt_cpyt_tn' => my_trim($this->input->post('dtnt_cpyt_tn')),
                //'dtnt_cpyt_tn_sn' => my_trim($this->input->post('dtnt_cpyt_tn_sn')),
                'dtnt_tcvp' => my_trim($this->input->post('dtnt_tcvp')),
                'dtnt_tcvp_sn' => my_trim($this->input->post('dtnt_tcvp_sn')),
                'dtnt_tcmt' => my_trim($this->input->post('dtnt_tcmt')),
                'dtnt_xct' => my_trim($this->input->post('dtnt_xct')),
                'THAISAN_QLNT' => my_trim($this->input->post('THAISAN_QLNT')),
                'tsqlnt_ktdk' => my_trim($this->input->post('tsqlnt_ktdk')),
                'tsqlnt_st' => my_trim($this->input->post('tsqlnt_st')),
                'tsqlnt_sm' => my_trim($this->input->post('tsqlnt_sm')),
                'tsqlnt_dn' => my_trim($this->input->post('tsqlnt_dn')),
                'DTNGT_OB' => my_trim($this->input->post('DTNGT_OB')),
                //'dtngt_dbh' => my_trim($this->input->post('dtngt_dbh')),
                'dtngt_st1lk' => my_trim($this->input->post('dtngt_st1lk')),
                'dtngt_slk' => my_trim($this->input->post('dtngt_slk')),
                'dtngt_nkngt' => my_trim($this->input->post('dtngt_nkngt')),
                'dtngt_nk' => my_trim($this->input->post('dtngt_nk')),
                'dtngt_cvr' => my_trim($this->input->post('dtngt_cvr')),
                'THAISAN_MDL' => my_trim($this->input->post('THAISAN_MDL')),
                //'tsmdl_dbh' => my_trim($this->input->post('tsmdl_dbh')),
                'tsmdl_ktdk' => my_trim($this->input->post('tsmdl_ktdk')),
                'tsmdl_st' => my_trim($this->input->post('tsmdl_st')),
                'tsmdl_sm' => my_trim($this->input->post('tsmdl_sm')),
                'tsmdl_dn' => my_trim($this->input->post('tsmdl_dn')),
                'NHAKHOA_MDL' => my_trim($this->input->post('NHAKHOA_MDL')),
                //'nkmdl_dbh' => my_trim($this->input->post('nkmdl_dbh')),
                'nkmdl_cb' => my_trim($this->input->post('nkmdl_cb')),
                'nkmdl_db' => my_trim($this->input->post('nkmdl_db')),
                'nkmdl_cvr' => my_trim($this->input->post('nkmdl_cvr')),
                //
                'ql_bosung' => my_trim($this->input->post('ql_bosung')),
                //
                'tgc_tn' => my_trim($this->input->post('tgc_tn')),
                'tgc_om_tt' => my_trim($this->input->post('tgc_om_tt')),
                'tgc_om_db' => my_trim($this->input->post('tgc_om_db')),
                'tgc_tv_tn' => my_trim($this->input->post('tgc_tv_tn')),
                'tgc_tv_ob' => my_trim($this->input->post('tgc_tv_ob')),
                'tgc_bcts' => my_trim($this->input->post('tgc_bcts')),
                'tgc_sinhcon' => my_trim($this->input->post('tgc_sinhcon')),
                //
                'dn_bbh' => my_trim($this->input->post('dn_bbh')),
                'cn_bbh' => my_trim($this->input->post('cn_bbh')),
                'dn_mbh' => my_trim($this->input->post('dn_mbh')),
                //'moigioi' => my_trim($this->input->post('moigioi')),
                'editor' => $this->phpsession->get('user_id'),
                'date_updated' => $datetime_now,
                'time_updated' => $time_now,
            );
            //==================================================================        
//            echo '<pre>';
//            print_r($data);
//            die;
            if ($id > 0) {
                $data['id'] = $id;
                $this->products_size_model->update($data);
            } else {
                $data['creator'] = $this->phpsession->get('user_id');
                $data['date_created'] = $datetime_now;
                $data['time_created'] = $time_now;
//                echo '<pre>';
//                print_r($data);
//                die;
                $this->products_size_model->insert($data);
            }
            //==================================================================
            redirect(PRODUCTS_SIZE_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //==========================================================================
    function edit() {
        $options = array();
        if ($this->is_postback()) {

            if (!$this->input->post('from_list')) {
                if (!$this->_do_add()) {
                    $options['error'] = validation_errors();
                    $options['options'] = $options;
                }
            }
        }
        //$this->_get_edit_form_data($options);
        //======================================================================
        $this->_get_form_data($options);
        $id = $this->input->post('id');
        // Lấy gói bh
        $goi_bh = $this->products_size_model->get_products_size(array('id' => $id));

        if (is_object($goi_bh)) {
            $options['goi_bh'] = $goi_bh;
        }
        //======================================================================
        $options['header'] = 'Sửa Gói bảo hiểm';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = PRODUCTS_SIZE_ADMIN_EDIT_URL;
        $tpl = 'ad/goi_bh/goi_bh_form_creat';
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Sửa Gói bảo hiểm' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================

    function delete() {
        if ($this->is_postback() && $this->input->post('from_list')) {
            $id = $this->input->post('id');
            $check1 = $this->products_model->get_products_count(array('size' => $id));
            if (empty($check1)) {
                $this->products_size_model->delete($id);
            }
            redirect(PRODUCTS_SIZE_ADMIN_BASE_URL);
        }
    }

}
