<?php

class Products_Trademark_Model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_products_trademark($options = array()) {
        if (isset($options['parent_id'])) {
            $this->db->where('parent_id', $options['parent_id']);
        }
        if (isset($options['email'])) {
            $this->db->where('email', $options['email']);
        }
        if (isset($options['password_fm'])) {
            $this->db->where('password_fm', $options['password_fm']);
        }

        if (isset($options['id']))
            $this->db->where('id', $options['id']);

        if (isset($options['name']))
            $this->db->where('name', $options['name']);

        if (isset($options['slug']))
            $this->db->where('slug', $options['slug']);

        if (isset($options['status']))
            $this->db->where('status', $options['status']);

        $this->db->order_by('id');

        $query = $this->db->get('products_trademark');
//        $sql = $this->db->last_query();
//        echo '<pre>';
//        print_r($sql);
//        die;
        if ($query->num_rows() > 0) {
            if (isset($options['id']) || isset($options['onehit']) || isset($options['get_row']))
                return $query->row(0);

            if (isset($options['last_row']))
                return $query->last_row();

            return $query->result();
        }else {
            return NULL;
        }
    }

    public function get_products_trademark_count($options = array()) {
        return count($this->get_products_trademark($options));
    }

    public function get_products_trademark_combo($options = array()) {
        // Default categories name
        if (!isset($options['combo_name'])) {
            $options['combo_name'] = 'products_trademark';
        }
        if (!isset($options['extra'])) {
            $options['extra'] = '';
        }
        if (!isset($options['is_add_edit_menu'])) {
            if (isset($options['is_add_edit_cat']))
                $categories[ROOT_CATEGORY_ID] = 'Tất cả';
            else
                $categories[DEFAULT_COMBO_VALUE] = ' - Chọn';
        }

        $this->db->where('status', STATUS_ACTIVE);
        $data = $this->db->get('products_trademark');
        if ($data->num_rows() > 0) {
            $data = $data->result_array();
            foreach ($data as $id => $value) {
                $id = $value['id'];
                $categories[$id] = $value['name'];
            }
            if (!isset($options[$options['combo_name']]))
                $options[$options['combo_name']] = DEFAULT_COMBO_VALUE;
            return form_dropdown($options['combo_name'], $categories, $options[$options['combo_name']], $options['extra']);
        }else {
            return NULL;
        }
    }

}
