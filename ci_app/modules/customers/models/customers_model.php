<?php

class Customers_Model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function count_bh($options = array()) {
        $this->get_data_options_bh($options);
        return $q = $this->db->count_all_results(TBL_PRODUCTS);
        $q->free_result();
    }

    //end
    //==========================================================================
    function get_bh($options = array()) {
        $this->get_data_options_bh($options);
//        echo '<pre>';
//        print_r($options);
//        die;
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS, $options['per_page'], $options['page'])->result();
        } elseif (isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_PRODUCTS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset'])) {
                $this->db->limit($options['limit'], $options['offset']);
            } else if (isset($options['limit'])) {
                $this->db->limit($options['limit']);
            }
            return $q = $this->db->get(TBL_PRODUCTS)->result();
//            $q = $this->db->get(TBL_PRODUCTS)->result();
//            $a = $this->db->last_query();
//            echo '<pre>';
//            print_r($a);
//            die;
        }

        $q->free_result();
    }

    //end
    //==========================================================================
    function get_data_options_bh($options = array()) {

        $select = '' . TBL_PRODUCTS . '.kh_mbh,
                    ' . TBL_PRODUCTS . '.goi_bh,
                    ' . TBL_PRODUCTS . '.dn_bbh,
                    ' . TBL_PRODUCTS . '.kh_dbh,
                    ' . TBL_PRODUCTS . '.id as BH_id,
                    ' . TBL_PRODUCTS . '.sohopdong,
                    ' . TBL_PRODUCTS . '.mqh,
                        
                    ' . TBL_PRODUCTS . '.daily,

                    ' . TBL_PRODUCTS . '.ngay_batdau,
                    ' . TBL_PRODUCTS . '.ngay_ketthuc,
                    ' . TBL_PRODUCTS . '.ngay_thanhtoan,
                    ' . TBL_PRODUCTS . '.ngay_hieuluc,
                    ' . TBL_PRODUCTS . '.ngay_ketthuc,';

        if (isset($options['join_' . TBL_CUSTOMERS . '_mbh']) || isset($options['join_' . TBL_CUSTOMERS . '_dbh'])) {
            $select .= '' . TBL_CUSTOMERS . '.id as KH_id,
                        ' . TBL_CUSTOMERS . '.congty_canhan,
                        ' . TBL_CUSTOMERS . '.mst_cmt,
                        ' . TBL_CUSTOMERS . '.ngaysinh,
                        ' . TBL_CUSTOMERS . '.didong,
                        ' . TBL_CUSTOMERS . '.email,
                        ' . TBL_CUSTOMERS . '.parent_id,
                            
                        ' . TBL_CUSTOMERS . '.sothe_bh,
                        ' . TBL_CUSTOMERS . '.code,

                        ' . TBL_CUSTOMERS . '.phongban,
                        ' . TBL_CUSTOMERS . '.phongban,';
        }
        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $select .= '' . TBL_PRODUCTS_SIZE . '.name as goi_bh_dm,
                                ' . TBL_PRODUCTS_SIZE . '.gia_tien as goi_gia_tien,
                                ' . TBL_PRODUCTS_SIZE . '.id as GOI_ID,
                                ' . TBL_PRODUCTS_SIZE . '.name,
                                ' . TBL_PRODUCTS_SIZE . '.TV_TTTBVV_TN,
                                ' . TBL_PRODUCTS_SIZE . '.tv_tttbvv_tn_tv,
                                ' . TBL_PRODUCTS_SIZE . '.tv_tttbvv_tn_tt,
                                ' . TBL_PRODUCTS_SIZE . '.TCL_NN,
                                ' . TBL_PRODUCTS_SIZE . '.tcl_nn_sn,
                                ' . TBL_PRODUCTS_SIZE . '.CPYT_TN,
                                ' . TBL_PRODUCTS_SIZE . '.TV_TTTBVV_OB,
                                ' . TBL_PRODUCTS_SIZE . '.tv_tttbvv_ob_tv,
                                ' . TBL_PRODUCTS_SIZE . '.tv_tttbvv_ob_tt,
                                ' . TBL_PRODUCTS_SIZE . '.DTNT_OB,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_dbh,                                    
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tvpn,                                    
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tvpn_sn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tgpn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tgpn_sn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_cppt,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tk_nv,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_sk_nv,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_cpyt_tn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_cpyt_tn_sn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tcvp,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tcvp_sn,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tcmt,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_xct,
                                ' . TBL_PRODUCTS_SIZE . '.THAISAN_QLNT,
                                ' . TBL_PRODUCTS_SIZE . '.tsqlnt_ktdk,
                                ' . TBL_PRODUCTS_SIZE . '.tsqlnt_st,
                                ' . TBL_PRODUCTS_SIZE . '.tsqlnt_sm,
                                ' . TBL_PRODUCTS_SIZE . '.tsqlnt_dn,
                                ' . TBL_PRODUCTS_SIZE . '.DTNGT_OB,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_dbh,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_st1lk,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_slk,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_nkngt,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_nk,
                                ' . TBL_PRODUCTS_SIZE . '.dtngt_cvr,
                                ' . TBL_PRODUCTS_SIZE . '.THAISAN_MDL,
                                ' . TBL_PRODUCTS_SIZE . '.tsmdl_dbh,
                                ' . TBL_PRODUCTS_SIZE . '.tsmdl_ktdk,
                                ' . TBL_PRODUCTS_SIZE . '.tsmdl_st,
                                ' . TBL_PRODUCTS_SIZE . '.tsmdl_sm,
                                ' . TBL_PRODUCTS_SIZE . '.tsmdl_dn,
                                ' . TBL_PRODUCTS_SIZE . '.NHAKHOA_MDL,
                                ' . TBL_PRODUCTS_SIZE . '.nkmdl_dbh,
                                ' . TBL_PRODUCTS_SIZE . '.nkmdl_cb,
                                ' . TBL_PRODUCTS_SIZE . '.nkmdl_db,
                                ' . TBL_PRODUCTS_SIZE . '.nkmdl_cvr,
                                ' . TBL_PRODUCTS_SIZE . '.dtnt_tvpn,';
        }
        if (isset($options['join_' . TBL_DN_BBH])) {
            $select .= '' . TBL_DN_BBH . '.name as ten_dn_bbh,';
        }
//        echo $select;
//        die;
        $this->db->select($select);
        //======================================================================
        // Join
        if (isset($options['join_' . TBL_CUSTOMERS . '_mbh'])) {
            $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_mbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        if (isset($options['join_' . TBL_CUSTOMERS . '_dbh'])) {
            $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $this->db->join(TBL_PRODUCTS_SIZE, TBL_PRODUCTS . '.goi_bh = ' . TBL_PRODUCTS_SIZE . '.id', 'left');
        }
        if (isset($options['join_' . TBL_DN_BBH])) {
            $this->db->join(TBL_DN_BBH, TBL_PRODUCTS . '.dn_bbh = ' . TBL_DN_BBH . '.id', 'left');
        }
        //end
        //======================================================================
        // WHERE
        if (isset($options['a_kh_mbh']) && !empty(isset($options['a_kh_mbh']))) {
            $this->db->where_in(TBL_PRODUCTS . '.kh_mbh', $options['a_kh_mbh']);
        } else {
            if (isset($options['kh_mbh'])) {
                $this->db->where(TBL_PRODUCTS . '.kh_mbh', $options['kh_mbh']);
            }
        }

        if (isset($options['dn_bbh'])) {
            $this->db->where(TBL_PRODUCTS . '.dn_bbh', $options['dn_bbh']);
        }
        if (isset($options['kh_dbh'])) {
//            echo 1;
//            die;
            $this->db->where(TBL_PRODUCTS . '.kh_dbh', $options['kh_dbh']);
        }
        if (isset($options['id'])) {
            $this->db->where(TBL_PRODUCTS . '.id', $options['id']);
        }
        //======================================================================
        if (isset($options['mst_cmt'])) {
            $this->db->where(TBL_CUSTOMERS . '.mst_cmt', $options['mst_cmt']);
        }
        if (isset($options['phanloai_kh']) && !empty($options['phanloai_kh'])) {
            $this->db->where(TBL_CUSTOMERS . '.phanloai_kh', $options['phanloai_kh']);
        }
        if (isset($options['pb'])) {
            $this->db->where(TBL_CUSTOMERS . '.phongban', $options['pb']);
        }
        if (isset($options['like_kh_dbh'])) {
            //$this->db->like(TBL_CUSTOMERS . '.congty_canhan', $options['like_kh_dbh']);
            //$this->db->or_like(TBL_CUSTOMERS . '.mst_cmt', $options['like_kh_dbh']);
        }
        //======================================================================
        if (isset($options['OD_kh_dbh'])) {
            $this->db->where(TBL_ORDERS . '.OD_kh_dbh', $options['OD_kh_dbh']);
        }
        if (isset($options['ma_benh'])) {
            $this->db->where(TBL_ORDERS . '.OD_ma_benh', $options['ma_benh']);
        }
        if (isset($options['qlbh'])) {
            $this->db->where(TBL_ORDERS . '.ql_bh', $options['qlbh']);
        }

        if (isset($options['OD_id'])) {
            $this->db->where(TBL_ORDERS . '.id', $options['OD_id']);
        }
        //======================================================================
        if (isset($options['goi_bh'])) {
            $this->db->where(TBL_PRODUCTS_SIZE . '.id', $options['goi_bh']);
        }
        if (isset($options['csyt'])) {
            $this->db->where(TBL_PRODUCTS_STYLE . '.id', $options['csyt']);
        }
        //======================================================================
        if (isset($options['like']) && !empty($options['like'])) {
            $this->db->where($options['like']);
        }
        //end
    }

    //end
    //==========================================================================
    // count bt by options
    function count_bt_by_options($options = array()) {
        $this->get_bt_data_by_options($options);
        return $q = $this->db->count_all_results(TBL_ORDERS);
//        $q = $this->db->get(TBL_ORDERS)->result();
//        $a = $this->db->last_query();
//        echo '<pre>';
//        print_r($a);
//        die;

        $q->free_result();
    }

    //end
    //==========================================================================
    function get_bt_data_by_options($options = array()) {
        $select = '' . TBL_ORDERS . '.*,';

        if (isset($options['join_' . TBL_CUSTOMERS]) || isset($options['join_' . TBL_CUSTOMERS . '_mbh']) || isset($options['join_' . TBL_CUSTOMERS . '_dbh'])) {
            $select .= '' . TBL_CUSTOMERS . '.id as KH_id,
                                ' . TBL_CUSTOMERS . '.congty_canhan,
                                ' . TBL_CUSTOMERS . '.phongban,
                                ' . TBL_CUSTOMERS . '.mst_cmt,
                                ' . TBL_CUSTOMERS . '.parent_id,
                                ' . TBL_CUSTOMERS . '.phongban,';
        }

        if (isset($options['join_' . TBL_PRODUCTS])) {
            $select .= '' . TBL_PRODUCTS . '.kh_mbh as KH_mbh,
                                ' . TBL_PRODUCTS . '.kh_dbh as KH_dbh,
                                ' . TBL_PRODUCTS . '.kh_dbh as KH_dbh,
                                ' . TBL_PRODUCTS . '.dn_bbh as DN_bbh,
                                ' . TBL_PRODUCTS . '.cn_bbh as CN_bbh,
                                ' . TBL_PRODUCTS . '.mqh,';
        }

        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $select .= '' . TBL_PRODUCTS_SIZE . '.name as goi_bh_dm,
                                ' . TBL_PRODUCTS_SIZE . '.gia_tien as goi_gia_tien,';
        }
        if (isset($options['join_' . TBL_PRODUCTS_STYLE])) {
            $select .= '' . TBL_PRODUCTS_STYLE . '.name as ten_csyt,';
        }
        if (isset($options['join_' . TBL_DN_BBH])) {
            $select .= '' . TBL_DN_BBH . '.name as ten_dn_bbh,';
        }

        $this->db->select($select);
        //======================================================================
        // Join
        if (isset($options['join_' . TBL_PRODUCTS])) {
            $this->db->join(TBL_PRODUCTS, TBL_ORDERS . '.OD_kh_dbh = ' . TBL_PRODUCTS . '.kh_dbh', 'left');
        }
        if (isset($options['join_' . TBL_CUSTOMERS])) {
            $this->db->join(TBL_CUSTOMERS, TBL_ORDERS . '.OD_kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $this->db->join(TBL_PRODUCTS_SIZE, TBL_PRODUCTS . '.goi_bh = ' . TBL_PRODUCTS_SIZE . '.id', 'left');
        }
        if (isset($options['join_' . TBL_PRODUCTS_STYLE])) {
            $this->db->join(TBL_PRODUCTS_STYLE, TBL_ORDERS . '.csyt = ' . TBL_PRODUCTS_STYLE . '.id', 'left');
        }
        if (isset($options['join_' . TBL_DN_BBH])) {
            $this->db->join(TBL_DN_BBH, TBL_PRODUCTS . '.dn_bbh = ' . TBL_DN_BBH . '.id', 'left');
        }
        //end
        //======================================================================
        // WHERE
        if (isset($options['mst_cmt'])) {
            $this->db->where(TBL_CUSTOMERS . '.mst_cmt', $options['mst_cmt']);
        }
        //======================================================================
        if (isset($options['dn_bbh'])) {
            $this->db->where(TBL_PRODUCTS . '.dn_bbh', $options['dn_bbh']);
        }
        if (isset($options['kh_mbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_mbh', $options['kh_mbh']);
        }
        if (isset($options['kh_dbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_dbh', $options['kh_dbh']);
        }
        if (isset($options['id'])) {
            $this->db->where(TBL_PRODUCTS . '.id', $options['id']);
        }
        //======================================================================
        if (isset($options['OD_kh_dbh'])) {
            $this->db->where(TBL_ORDERS . '.OD_kh_dbh', $options['OD_kh_dbh']);
        }
        if (isset($options['OD_id'])) {
            $this->db->where(TBL_ORDERS . '.id', $options['OD_id']);
        }
        if (isset($options['ma_benh'])) {
            $this->db->where(TBL_ORDERS . '.OD_ma_benh', $options['ma_benh']);
        }
        if (isset($options['phanloai_bt'])) {
            $this->db->where(TBL_ORDERS . '.phanloai_bt', $options['phanloai_bt']);
        }
        if (isset($options['qlbh'])) {
            $this->db->where(TBL_ORDERS . '.phanloai_bt', $options['qlbh']);
        }
        if (isset($options['phanloai_bt'])) {
            $this->db->where(TBL_ORDERS . '.phanloai_bt', $options['phanloai_bt']);
        }
        //======================================================================
        if (isset($options['goi_bh'])) {
            $this->db->where(TBL_PRODUCTS_SIZE . '.id', $options['goi_bh']);
        }
        if (isset($options['csyt'])) {
            $this->db->where(TBL_PRODUCTS_STYLE . '.id', $options['csyt']);
        }
        //======================================================================
        if (isset($options['like']) && !empty($options['like'])) {
            $this->db->where($options['like']);
        }
        //======================================================================
    }

    // end
    //==========================================================================
    function count_bt($options = array()) {
        $this->get_data_options_bt($options);
        return $q = $this->db->count_all_results(TBL_PRODUCTS);
        $q->free_result();
    }

    //end
    function get_data_options_bt($options = array()) {
        /*
         * $this->db->select(' ' . TBL_PRODUCTS . '.*,
          ' . TBL_CUSTOMERS . '.id as KH_id,
          ' . TBL_CUSTOMERS . '.congty_canhan,
          ' . TBL_CUSTOMERS . '.phongban,
          ' . TBL_CUSTOMERS . '.mst_cmt,
          ' . TBL_CUSTOMERS . '.parent_id,
          ' . TBL_CUSTOMERS . '.phongban,
          ' . TBL_CUSTOMERS . '.ngaysinh,
          ' . TBL_ORDERS . '.id as OD_id,
          ' . TBL_ORDERS . '.OD_kh_dbh,
          ' . TBL_ORDERS . '.loai_benh,
          ' . TBL_ORDERS . '.so_tien_ycbt,
          ' . TBL_ORDERS . '.so_tien_dbt,
          ' . TBL_ORDERS . '.ngay_ycbt,
          ' . TBL_ORDERS . '.so_ngay_ycbt,
          ' . TBL_ORDERS . '.ql_bh,
          ' . TBL_ORDERS . '.ngay_giaiquyet,
          ' . TBL_ORDERS . '.ngay_xayra_tonthat,
          ' . TBL_ORDERS . '.ngay_dieutri,
          ' . TBL_ORDERS . '.csyt,
          ' . TBL_ORDERS . '.order_status,
          ' . TBL_ORDERS . '.tinhtrang_hoso,
          ' . TBL_ORDERS . '.chungtu_bosung,
          ' . TBL_ORDERS . '.chungtu_bosung_txt,
          ' . TBL_ORDERS . '.ngay_bosung_hoso,
          ' . TBL_ORDERS . '.ngay_duyet_bt,
          ' . TBL_ORDERS . '.phuongthuc_bt,
          ' . TBL_ORDERS . '.ngay_nhan_hoso,
          ' . TBL_ORDERS . '.ngay_xuly_bt,
          ' . TBL_ORDERS . '.ngay_thongbao_kh,
          ' . TBL_ORDERS . '.chuandoan_benh,
          ' . TBL_ORDERS . '.OD_ma_benh,
          ' . TBL_ORDERS . '.so_tien_thanhtoan_csyt,
          ' . TBL_ORDERS . '.so_tien_tuchoi,
          ' . TBL_ORDERS . '.lydo_khong_bt,
          ' . TBL_ORDERS . '.ghichu_thanhtoan,
          ' . TBL_ORDERS . '.hinhthuc_nhantien_bt,
          ' . TBL_ORDERS . '.OD_ngay_thanhtoan,
          ' . TBL_ORDERS . '.hoso_scan,
          ' . TBL_PRODUCTS_SIZE . '.name as goi_bh_dm,
          ' . TBL_PRODUCTS_SIZE . '.gia_tien as goi_gia_tien,
          ' . TBL_PRODUCTS_STYLE . '.name as ten_csyt,
          ' . TBL_DN_BBH . '.name as ten_dn_bbh,
          ');
          // Join
          if (isset($options['join_' . TBL_ORDERS])) {
          $this->db->join(TBL_ORDERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_ORDERS . '.OD_kh_dbh', 'left');
          }
          if (isset($options['join_' . TBL_CUSTOMERS])) {
          $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
          }
          if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
          $this->db->join(TBL_PRODUCTS_SIZE, TBL_PRODUCTS . '.goi_bh = ' . TBL_PRODUCTS_SIZE . '.id', 'left');
          }
          if (isset($options['join_' . TBL_PRODUCTS_STYLE])) {
          $this->db->join(TBL_PRODUCTS_STYLE, TBL_ORDERS . '.csyt = ' . TBL_PRODUCTS_STYLE . '.id', 'left');
          }
          if (isset($options['join_' . TBL_DN_BBH])) {
          $this->db->join(TBL_DN_BBH, TBL_PRODUCTS . '.dn_bbh = ' . TBL_DN_BBH . '.id', 'left');
          }
          //end
          // WHERE
          if (isset($options['mst_cmt'])) {
          $this->db->where(TBL_CUSTOMERS . '.mst_cmt', $options['mst_cmt']);
          }
          if (isset($options['kh_mbh'])) {
          $this->db->where(TBL_PRODUCTS . '.kh_mbh', $options['kh_mbh']);
          }
          if (isset($options['kh_dbh'])) {
          $this->db->where(TBL_PRODUCTS . '.kh_dbh', $options['kh_dbh']);
          }
          if (isset($options['OD_kh_dbh'])) {
          $this->db->where(TBL_ORDERS . '.OD_kh_dbh', $options['OD_kh_dbh']);
          }
          if (isset($options['OD_id'])) {
          $this->db->where(TBL_ORDERS . '.id', $options['OD_id']);
          }
          if (isset($options['goi_bh'])) {
          $this->db->where(TBL_PRODUCTS_SIZE . '.id', $options['goi_bh']);
          }
          if (isset($options['csyt'])) {
          $this->db->where(TBL_PRODUCTS_STYLE . '.id', $options['csyt']);
          }
          if (isset($options['ma_benh'])) {
          //$this->db->where(TBL_ORDERS . '.OD_ma_benh', $options['ma_benh']);
          $this->db->where(TBL_ORDERS . '.OD_ma_benh', $options['ma_benh']);
          }
          if (isset($options['qlbh'])) {
          $this->db->where(TBL_ORDERS . '.ql_bh', $options['qlbh']);
          }
          if (isset($options['id'])) {
          $this->db->where(TBL_PRODUCTS . '.id', $options['id']);
          }
          if (isset($options['OD_id'])) {
          $this->db->where(TBL_ORDERS . '.id', $options['OD_id']);
          }
          //end
         */

        $select = '' . TBL_PRODUCTS . '.*,';

        if (isset($options['join_' . TBL_CUSTOMERS]) || isset($options['join_' . TBL_CUSTOMERS . '_mbh']) || isset($options['join_' . TBL_CUSTOMERS . '_dbh'])) {
            $select .= '' . TBL_CUSTOMERS . '.id as KH_id,
                                ' . TBL_CUSTOMERS . '.congty_canhan,
                                ' . TBL_CUSTOMERS . '.phongban,
                                ' . TBL_CUSTOMERS . '.mst_cmt,
                                ' . TBL_CUSTOMERS . '.parent_id,
                                ' . TBL_CUSTOMERS . '.phongban,';
        }
        if (isset($options['join_' . TBL_ORDERS])) {
            $select .= '' . TBL_ORDERS . '.id as OD_id,
                                ' . TBL_ORDERS . '.OD_kh_dbh,
                                ' . TBL_ORDERS . '.phanloai_bt,
                                ' . TBL_ORDERS . '.so_tien_ycbt,
                                ' . TBL_ORDERS . '.so_tien_dbt,
                                ' . TBL_ORDERS . '.so_ngay_ycbt,
                                ' . TBL_ORDERS . '.csyt,
                                ' . TBL_ORDERS . '.order_status,
                                ' . TBL_ORDERS . '.chungtu_bosung,
                                ' . TBL_ORDERS . '.chungtu_bosung_txt,
                                ' . TBL_ORDERS . '.phuongthuc_bt,
                                ' . TBL_ORDERS . '.chuandoan_benh,
                                ' . TBL_ORDERS . '.OD_ma_benh,
                                ' . TBL_ORDERS . '.lydo_khong_bt,
                                    
                                ' . TBL_ORDERS . '.ghichu_hoso,
                                ' . TBL_ORDERS . '.hinhthuc_nhantien_bt,
                                ' . TBL_ORDERS . '.ghichu_hinhthuc_nhantien_bt,
                                ' . TBL_ORDERS . '.ghichu_xuly_bt,
                                ' . TBL_ORDERS . '.ghichu_khong_bt,
                                ' . TBL_ORDERS . '.ghichu_pheduyet_bt,
                                ' . TBL_ORDERS . '.ghichu_thanhtoan_bt,
                                
                                ' . TBL_ORDERS . '.date_xayra_tonthat,
                                ' . TBL_ORDERS . '.date_nhan_bt,
                                ' . TBL_ORDERS . '.date_xuly_bt,
                                ' . TBL_ORDERS . '.date_bosung_ct,
                                ' . TBL_ORDERS . '.date_duyet_bt,
                                ' . TBL_ORDERS . '.date_thongbao_kh,
                                ' . TBL_ORDERS . '.date_thanhtoan,
                                ' . TBL_ORDERS . '.date_tratien,
                                    
                                ' . TBL_ORDERS . '.hoso_scan,';
        }
        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $select .= '' . TBL_PRODUCTS_SIZE . '.name as goi_bh_dm,
                                ' . TBL_PRODUCTS_SIZE . '.gia_tien as goi_gia_tien,';
        }
        if (isset($options['join_' . TBL_PRODUCTS_STYLE])) {
            $select .= '' . TBL_PRODUCTS_STYLE . '.name as ten_csyt,';
        }
        if (isset($options['join_' . TBL_DN_BBH])) {
            $select .= '' . TBL_DN_BBH . '.name as ten_dn_bbh,';
        }
        //======================================================================
        $this->db->select($select);
        //======================================================================
        // Join
        if (isset($options['join_' . TBL_ORDERS])) {
            $this->db->join(TBL_ORDERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_ORDERS . '.OD_kh_dbh', 'right');
        }
        if (isset($options['join_' . TBL_CUSTOMERS])) {
            $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $this->db->join(TBL_PRODUCTS_SIZE, TBL_PRODUCTS . '.goi_bh = ' . TBL_PRODUCTS_SIZE . '.id', 'left');
        }
        if (isset($options['join_' . TBL_PRODUCTS_STYLE])) {
            $this->db->join(TBL_PRODUCTS_STYLE, TBL_ORDERS . '.csyt = ' . TBL_PRODUCTS_STYLE . '.id', 'left');
        }
        if (isset($options['join_' . TBL_DN_BBH])) {
            $this->db->join(TBL_DN_BBH, TBL_PRODUCTS . '.dn_bbh = ' . TBL_DN_BBH . '.id', 'left');
        }
        //end
        //======================================================================
        // WHERE
        if (isset($options['dn_bbh'])) {
            $this->db->where(TBL_PRODUCTS . '.dn_bbh', $options['dn_bbh']);
        }
        if (isset($options['kh_mbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_mbh', $options['kh_mbh']);
        }
        if (isset($options['kh_dbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_dbh', $options['kh_dbh']);
        }
        if (isset($options['id'])) {
            $this->db->where(TBL_PRODUCTS . '.id', $options['id']);
        }
        //======================================================================
        if (isset($options['ma_benh'])) {
            $this->db->where(TBL_ORDERS . '.OD_ma_benh', $options['ma_benh']);
        }
        if (isset($options['phanloai_bt'])) {
            $this->db->where(TBL_ORDERS . '.phanloai_bt', $options['phanloai_bt']);
        }
        if (isset($options['OD_id'])) {
            $this->db->where(TBL_ORDERS . '.id', $options['OD_id']);
        }
        if (isset($options['OD_kh_dbh'])) {
            $this->db->where(TBL_ORDERS . '.OD_kh_dbh', $options['OD_kh_dbh']);
        }
        if (isset($options['OD_id'])) {
            $this->db->where(TBL_ORDERS . '.id', $options['OD_id']);
        }
        //======================================================================
        if (isset($options['goi_bh'])) {
            $this->db->where(TBL_PRODUCTS_SIZE . '.id', $options['goi_bh']);
        }
        if (isset($options['csyt'])) {
            $this->db->where(TBL_PRODUCTS_STYLE . '.id', $options['csyt']);
        }
    }

    //end
    //==========================================================================
    function get_bt_by_options($options = array()) {

        $this->get_bt_data_by_options($options);

        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_ORDERS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_ORDERS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_ORDERS)->result();
//             $q = $this->db->get(TBL_PRODUCTS)->result();
//             $a= $this->db->last_query();
//             echo '<pre>';
//             print_r($a);
//             die;
        }

        $q->free_result();
    }

    //end
    //==========================================================================
    function get_bt($options = array()) {

        $this->get_data_options_bt($options);

        //======================================================================
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_PRODUCTS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_PRODUCTS)->result();
//             $q = $this->db->get(TBL_PRODUCTS)->result();
//             $a= $this->db->last_query();
//             echo '<pre>';
//             print_r($a);
//             die;
        }

        $q->free_result();
    }

    //end
    //==========================================================================
    function get_canhan_dbh($options = array()) {
        $this->db->select('*');
        // Join
        if (isset($options['join_' . TBL_PRODUCTS])) {
            //$this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.custom_duoc_id = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        //end
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_PRODUCTS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_PRODUCTS)->result();
        }

        $q->free_result();
    }

    //end
    //==========================================================================
    function get_doanhnghiep_mbh($options = array()) {
        $this->db->select('*');
        $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_mbh = ' . TBL_CUSTOMERS . '.id', 'left');
        //end
        // WHERE
        if (isset($options['phanloai_kh']) && !empty($options['phanloai_kh'])) {
            $this->db->where(TBL_CUSTOMERS . '.phanloai_kh', $options['phanloai_kh']);
        }
        //end
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            echo 1;
            die;
            $q = $this->db->get(TBL_PRODUCTS)->first_row();
            $a = $this->db->last_query();
            echo '<pre>';
            print_r($a);
            die;
            return $q = $this->db->get(TBL_PRODUCTS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_PRODUCTS)->result();
        }

        $q->free_result();
    }

    //end
    //==========================================================================
    function get_kh_by_options($options = array()) {
        $this->db->select('*');
        //end
        // WHERE
        // Loại kh
        if (isset($options['phanloai_kh'])) {
            $this->db->where(TBL_CUSTOMERS . '.phanloai_kh', $options['phanloai_kh']);
        }
        // lấy dn 
        if (isset($options['parent_id'])) {
            $this->db->where(TBL_CUSTOMERS . '.parent_id', $options['parent_id']);
        }
        if (isset($options['email'])) {
            if (isset($options['mst_cmt'])) {
                //$this->db->where(TBL_CUSTOMERS . '.email', $options['email']);
                //$this->db->or_where(TBL_CUSTOMERS . '.mst_cmt', $options['mst_cmt']);
                $where = "(`" . DB_PREFIX . TBL_CUSTOMERS . "`.`email` ='" . $options['email'] . "'";
                //$filter_key_search .= " OR `" . TBL_POST . "`.`post_content` LIKE '%" . $key . "%'";
                //$filter_key_search .= " OR `" . TBL_POST . "`.`post_contact_mobile` LIKE '%" . $key . "%'";
                //$filter_key_search .= " OR `" . TBL_POST . "`.`post_contact_name` LIKE '%" . $key . "%'";
                $where .= " OR `" . DB_PREFIX . TBL_CUSTOMERS . "`.`mst_cmt` ='" . $options['email'] . "')";

                $this->db->where($where);
            } else {
                $this->db->where(TBL_CUSTOMERS . '.email', $options['email']);
            }
        }
        if (isset($options['password'])) {
            $this->db->where(TBL_CUSTOMERS . '.password', $options['password']);
        }
        if (isset($options['id'])) {
            $this->db->where(TBL_CUSTOMERS . '.id', $options['id']);
        }
        //END
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_CUSTOMERS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_CUSTOMERS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_CUSTOMERS)->result();
        }

        $q->free_result();
    }

    //end
    //==========================================================================
    // Count kh
    function count_kh($options = array()) {
        $this->get_kh_data_options($options);
        return $q = $this->db->count_all_results(TBL_CUSTOMERS);
        $q->free_result();
    }

    //end
    //==========================================================================
    function get_kh($options = array()) {
        $this->get_kh_data_options($options);
//        echo '<pre>';
//        print_r($options);
//        die;
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_CUSTOMERS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_CUSTOMERS)->first_row();
//            $a = $this->db->last_query();
//            echo '<pre>';
//            print_r($a);
//            die;
        } else {
            if (isset($options['limit']) && isset($options['offset'])) {
                $this->db->limit($options['limit'], $options['offset']);
            } else if (isset($options['limit'])) {
                $this->db->limit($options['limit']);
            }

            return $q = $this->db->get(TBL_CUSTOMERS)->result();
        }

        $q->free_result();
    }

    //end
    //==========================================================================
    function get_kh_data_options($options = array()) {

        $select = '' . TBL_CUSTOMERS . '.id as KH_id,
                        ' . TBL_CUSTOMERS . '.congty_canhan,
                        
                        ' . TBL_CUSTOMERS . '.code,
                        ' . TBL_CUSTOMERS . '.sothe_bh,
                        
                        ' . TBL_CUSTOMERS . '.tk_fullname,
                        ' . TBL_CUSTOMERS . '.tk_stk,
                        ' . TBL_CUSTOMERS . '.tk_bank,
                        ' . TBL_CUSTOMERS . '.tk_ghichu,

                        ' . TBL_CUSTOMERS . '.parent_id,
                        ' . TBL_CUSTOMERS . '.city_id,
                        ' . TBL_CUSTOMERS . '.mst_cmt,
                        ' . TBL_CUSTOMERS . '.ngaycap,
                        ' . TBL_CUSTOMERS . '.noicap,
                        ' . TBL_CUSTOMERS . '.phongban,
                        ' . TBL_CUSTOMERS . '.email,
                        ' . TBL_CUSTOMERS . '.diachi,
                        ' . TBL_CUSTOMERS . '.ngaysinh,
                        ' . TBL_CUSTOMERS . '.gioitinh,
                        ' . TBL_CUSTOMERS . '.dienthoai,
                        ' . TBL_CUSTOMERS . '.didong,
                        ' . TBL_CUSTOMERS . '.mst_cmt,
                        ' . TBL_CUSTOMERS . '.date_created,
                        ' . TBL_CUSTOMERS . '.date_updated,
                        ' . TBL_CUSTOMERS . '.phanloai_kh,
                        ' . TBL_CUSTOMERS . '.phanloai_kh1,
                        ' . TBL_CUSTOMERS . '.ghichu,
                        ' . TBL_CUSTOMERS . '.phongban,';

        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $select .= '' . TBL_PRODUCTS_SIZE . '.name as goi_bh_dm,
                                ' . TBL_PRODUCTS_SIZE . '.gia_tien as goi_gia_tien,
                                ' . TBL_PRODUCTS_SIZE . '.name,
                                ' . TBL_PRODUCTS_SIZE . '.TV_TTTBVV_TN,
                                ' . TBL_PRODUCTS_SIZE . '.TCL_NN,
                                ' . TBL_PRODUCTS_SIZE . '.CPYT_TN,
                                ' . TBL_PRODUCTS_SIZE . '.TV_TTTBVV_OB,
                                ' . TBL_PRODUCTS_SIZE . '.DTNT_OB,
                                ' . TBL_PRODUCTS_SIZE . '.THAISAN_QLNT,
                                ' . TBL_PRODUCTS_SIZE . '.THAISAN_MDL,
                                ' . TBL_PRODUCTS_SIZE . '.NHAKHOA_MDL,';
        }
        if (isset($options['join_' . TBL_DN_BBH])) {
            $select .= '' . TBL_DN_BBH . '.name as ten_dn_bbh,';
        }
//        echo $select;
//        die;
        $this->db->select($select);
        //======================================================================
        // Join
        if (isset($options['join_' . TBL_CUSTOMERS . '_dbh'])) {
            $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $this->db->join(TBL_PRODUCTS_SIZE, TBL_PRODUCTS . '.goi_bh = ' . TBL_PRODUCTS_SIZE . '.id', 'left');
        }
        if (isset($options['join_' . TBL_DN_BBH])) {
            $this->db->join(TBL_DN_BBH, TBL_PRODUCTS . '.dn_bbh = ' . TBL_DN_BBH . '.id', 'left');
        }
        //end
        //======================================================================
        // WHERE
        if (isset($options['mst_cmt'])) {
            $this->db->where(TBL_CUSTOMERS . '.mst_cmt', $options['mst_cmt']);
        }
        if (isset($options['kh_id'])) {
            $this->db->where(TBL_CUSTOMERS . '.id', $options['kh_id']);
        }
        if (isset($options['id'])) {
            $this->db->where(TBL_CUSTOMERS . '.id', $options['id']);
        }
        if (isset($options['skip_id'])) {
            $this->db->where(TBL_CUSTOMERS . '.id != ', $options['skip_id']);
        }
        if (isset($options['phanloai_kh'])) {
            $this->db->where(TBL_CUSTOMERS . '.phanloai_kh', $options['phanloai_kh']);
        }
        if (isset($options['parent_id'])) {
            $this->db->where(TBL_CUSTOMERS . '.parent_id', $options['parent_id']);
        }

        if (isset($options['email'])) {
            $this->db->where(TBL_CUSTOMERS . '.email', $options['email']);
        }
        if (isset($options['pb'])) {
            $this->db->where(TBL_CUSTOMERS . '.phongban', $options['pb']);
        }
        if (isset($options['like']) && !empty($options['like'])) {
            $this->db->where($options['like']);
        }
        //======================================================================
        if (isset($options['a_kh_mbh']) && !empty(isset($options['a_kh_mbh']))) {
            $this->db->where_in(TBL_PRODUCTS . '.kh_mbh', $options['a_kh_mbh']);
        } else {
            if (isset($options['kh_mbh'])) {
                $this->db->where(TBL_PRODUCTS . '.kh_mbh', $options['kh_mbh']);
            }
        }

        if (isset($options['dn_bbh'])) {
            $this->db->where(TBL_PRODUCTS . '.dn_bbh', $options['dn_bbh']);
        }
        if (isset($options['kh_dbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_dbh', $options['kh_dbh']);
        }
        //======================================================================        
        if (isset($options['goi_bh'])) {
            $this->db->where(TBL_PRODUCTS_SIZE . '.id', $options['goi_bh']);
        }
        if (isset($options['csyt'])) {
            $this->db->where(TBL_PRODUCTS_STYLE . '.id', $options['csyt']);
        }
        //======================================================================
        if (isset($options['OD_kh_dbh'])) {
            $this->db->where(TBL_ORDERS . '.OD_kh_dbh', $options['OD_kh_dbh']);
        }
        if (isset($options['ma_benh'])) {
            $this->db->where(TBL_ORDERS . '.OD_ma_benh', $options['ma_benh']);
        }
        if (isset($options['qlbh'])) {
            $this->db->where(TBL_ORDERS . '.ql_bh', $options['qlbh']);
        }
        if (isset($options['OD_id'])) {
            $this->db->where(TBL_ORDERS . '.id', $options['OD_id']);
        }
        //end
        //======================================================================
        $this->db->order_by(TBL_CUSTOMERS . '.id', 'DESC');
    }

    //==========================================================================  

    public function update_customers_view($id = 0) {
        $this->db->set('viewed', 'viewed + 1', FALSE);
        $this->db->where('id', $id);
        $this->db->update('customers');
    }

    //==========================================================================
    // check username khi đăng ký đã tồn tại chưa
    function is_available_username_customers($params = array()) {
        $this->db->where('email', $params['email']);

        $query = $this->db->get('customers');

        if (count($query->row()) > 0) {
            $this->_last_message = '<p>Email: <strong>' . $params['email'] . '</strong> đã tồn tại trong hệ thống.</p>';
            return FALSE;
        }
        return TRUE;
    }

    //==========================================================================
    function get_last_message() {
        return $this->_last_message;
    }

}

?>