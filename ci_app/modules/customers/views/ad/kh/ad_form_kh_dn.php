<?php
$selected_df = 'selected="selected"';
$a_customers_type = get_a_customers_type();
$a_customers_type1 = array(
    THUONG => 'Thường',
    VIP => 'Vip',
);
$a_gender = array(
    NAM => 'Nam',
    NU => 'Nữ',
    KHAC => 'Khác',
);
//==============================================================================
if (isset($kh)) {
    $id = $kh->KH_id;
    $parent_id = $kh->parent_id;
    $sothe_bh = $kh->sothe_bh;
    
    $phanloai_kh = $kh->phanloai_kh;
    $phanloai_bh = $kh->phanloai_bh;
    $hopdong_baohiem = $kh->hopdong_baohiem;
    $congty_canhan = $kh->congty_canhan;
    $ngaysinh = substr($kh->ngaysinh, 0, 10);
    $congtycon_chinhanh = $kh->parent_id;
    $phongban = $kh->phongban;
    $diachi = $kh->diachi;
    $city_id = $kh->city_id;
    $mst_cmt = $kh->mst_cmt;
    $ngaycap = substr($kh->ngaycap, 0, 10);
    $noicap = $kh->noicap;
    $email = $kh->email;
    $dienthoai = $kh->dienthoai;
    $didong = $kh->didong;
    $phanloai_kh1 = $kh->phanloai_kh1;
    $gioitinh = $kh->gioitinh;
}
$id = isset($id) ? $id : '0';
//==============================================================================
echo form_open($submit_uri);
echo form_hidden('id', $id);
$created_date = isset($created_date) ? $created_date : '';
$submit_uri = isset($submit_uri) ? $submit_uri : '';
//==============================================================================
?>
<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Chi tiết khách hàng"</small>
    <span class="fright"><a class="button close" href="<?php echo site_url(URL_AD_KH); ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>

<div class="form_content">
    <ul class="tabs">
        <li><a href="#tab1">Thông tin bên mua bảo hiểm</a></li>
        <!--<li><a href="#tab2">Thông tin người được bảo hiểm</a></li>-->
    </ul>
    <?php $this->load->view('powercms/message'); ?>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <div class="col-sm-6 col-xs-12">
                <input type="hidden" name="phanloai_kh" value="<?php echo DOANHNGHIEP?>"/>
                <table>
                    <tr><td class="title">Tên Công ty / Chi nhánh:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'congty_canhan', 'size' => '50', 'style' => 'width:560px;', 'value' => isset ($congty_canhan) ? $congty_canhan : set_value('congty_canhan'))); ?></td>
                    </tr>
                    <tr><td class="title">Số thẻ Bảo Hiểm:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'sothe_bh', 'size' => '50', 'style' => 'width:560px;', 'value' => isset($sothe_bh) ? $sothe_bh : set_value('sothe_bh'))); ?></td>
                    </tr>
                    <tr><td class="title">Trực thuộc công ty:</td></tr>
                    <tr>
                        <td>
                            <!--<select name="parent_id" class="btn">-->
                            <select name="parent_id" class="js-example-disabled-results" style="margin-left: 10px !important; padding-left: 10px !important">
                                <option value="0">-- Trực thuộc công ty --</option>
                                <?php
                                if (isset($kh_dn) && !empty($kh_dn)) {
                                    foreach ($kh_dn as $index) {
                                        $b_name = $index->congty_canhan;
                                        $b_id = $index->KH_id;
                                        $b_parent_id = $index->parent_id;
                                        $selected = ($b_id == $parent_id) ? $selected_df : '';
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $b_id ?>"><?php echo $b_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr><td class="title">Địa chỉ:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'diachi', 'size' => '50', 'style' => 'width:560px;','value' => isset ($diachi) ? $diachi : set_value('diachi'))); ?></td>
                    </tr>    
                    <tr><td class="title">Tỉnh thành:</td></tr>
                    <tr>
                        <td>
                            <select name="cities" class="js-example-disabled-results" style="margin-left: 10px !important; padding-left: 10px !important">
                                <option value="0">-- Tỉnh thành --</option>
                                <?php
                                if (isset($a_cities) && !empty($a_cities)) {
                                    foreach ($a_cities as $index) {
                                        $ct_id = $index->id;
                                        $ct_name = $index->name;
                                        $selected = ($ct_id === $city_id) ? $selected_df : '';
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $ct_id ?>"><?php echo $ct_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr><td class="title">Phân loại khách hàng:</td></tr>
                    <tr>
                        <td>
                            <select name="phanloai_kh1" class="btn">
                                <?php
                                if (!empty($a_customers_type1)) {
                                    foreach ($a_customers_type1 as $k => $v) {
                                        $selected = ($k == $phanloai_kh1) ? $selected_df : '';
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-sm-6 col-xs-12">
                <table>
                    <tr><td class="title">Mã số thuế:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'mst_cmt', 'size' => '50', 'style' => 'width:560px;','value' => isset ($mst_cmt) ? $mst_cmt : set_value('mst_cmt'))); ?></td>
                    </tr>
                    <tr><td class="title">Email:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'email', 'size' => '50', 'style' => 'width:560px;','value' => isset ($email) ? $email : set_value('email'))); ?></td>
                    </tr>

                    <tr><td class="title">Điện thoại:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'dienthoai', 'size' => '50', 'style' => 'width:560px;','value' => isset ($dienthoai) ? $dienthoai : set_value('dienthoai'))); ?></td>
                    </tr>

                    <tr><td class="title">Di động:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'didong', 'size' => '50', 'style' => 'width:560px;','value' => isset ($didong) ? $didong : set_value('didong'))); ?></td>
                    </tr>

                    <tr><td class="title">Ghi chú:</td></tr>
                    <tr>
                        <td>
                            <textarea name="ghichu" rows="6" cols="66" value="<?php echo $ghichu?>"><?php echo $ghichu?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <br class="clear"/>
    <div style="margin-top: 10px;"></div>
    <?php echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn')); ?>

    <br class="clear"/>&nbsp;
</div>
<?php echo form_close(); ?>