<div class="page_header">
    <br class="clear"/>
</div>
<div class="db_box">
    <?php
    $ss_kh_email = $this->phpsession->get('ss_kh_email');
    ?>
    <div class="row">
        <div class="col-md-9">
            <div class="form_content">

                <h3>1.Thông tin bảo hiểm</h3>
                <style>
                    .list{
                        clear: none;
                    }
                </style>
                <div style="width: 100%">
                    <table class="list" style="width: 50%; margin-bottom: 10px; float: left;">
                        <thead>
                            <tr>
                                <th style="width: 40%">Option</th>
                                <th>Value</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><strong>MÃ KH</strong></td>
                                <td>11</td>

                            </tr>
                            <tr>
                                <td><strong>KHÁCH HÀNG</strong></td>
                                <td>Nguyễn Cao Cường</td>

                            </tr>
                            <tr>
                                <td><strong>CMT</strong></td>
                                <td>01245789</td>

                            </tr>
                            <tr>
                                <td><strong>HỢP ĐỒNG BH</strong></td>
                                <td>FTC-001</td>

                            </tr>					
                            <tr>
                                <td><strong>CTY</strong></td>
                                <td>Asian Finance</td>

                            </tr>
                            <tr>
                                <td><strong>ĐỊA CHỈ</strong></td>
                                <td>Hà Nội</td>

                            </tr>
                            <tr>
                                <td><strong>ĐKKD-Mst/CMT-HỘ CHIẾU</strong></td>
                                <td>012535488</td>

                            </tr>

                        </tbody>
                    </table>
                    <table class="list" style="width: 50%; margin-bottom: 10px;float:left;">
                        <thead>
                            <tr>
                                <th style="width: 40%">Option</th>
                                <th>Value</th>

                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td><strong>NGÀY CẤP</strong></td>
                                <td>2000-07-11 00:00:00</td>

                            </tr>
                            <tr>
                                <td><strong>NƠI CẤP</strong></td>
                                <td>Hà Nội</td>

                            </tr>
                            <tr>
                                <td><strong>EMAIL</strong></td>
                                <td>nhanvien@gmai.com</td>

                            </tr>
                            <tr>
                                <td><strong>PHONE</strong></td>
                                <td>094687456</td>

                            </tr>
                            <tr>
                                <td><strong>MOBILE</strong></td>
                                <td>0432984793</td>

                            </tr>
                            <tr>
                                <td><strong>PHÂN LOẠI KH</strong></td>
                                <td>VIP</td>

                            </tr>
                            <tr>
                                <td><strong>Quan hệ</strong></td>
                                <td>Người mua</td>

                            </tr>

                        </tbody>
                    </table>
                </div>
                <br class="clear">&nbsp;
                <h3>2.Thông tin bồi thường</h3>
                <table class="list" style="width: 50%; margin-bottom: 10px; float: left;">
                    <thead>
                        <tr>
                            <th style="width: 40%">Option</th>
                            <th>Default</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><strong>TỔNG SỐ HSBT</strong></td>
                            <td>5</td>

                        </tr>
                        <tr>
                            <td><strong>TỔNG SỐ TIỀN YÊU CẦU BT</strong></td>
                            <td>200.000đ</td>

                        </tr>
                        <tr>
                            <td><strong>TỔNG SỐ TIỀN BT</strong></td>
                            <td>200.000đ</td>

                        </tr>
                        <tr>
                            <td><strong>TỶ LỆ BT HIỆN TẠI</strong></td>
                            <td>0,66%</td>

                        </tr>	

                    </tbody>
                </table>
                <table class="list" style="width: 50%; margin-bottom: 10px; float: left;">
                    <thead>
                        <tr>
                            <th style="width: 40%">Option</th>
                            <th>Default</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><strong>Option</strong></td>
                            <td>Default</td>

                        </tr>
                        <tr>
                            <td><strong>Option</strong></td>
                            <td>Default</td>

                        </tr>
                        <tr>
                            <td><strong>Option</strong></td>
                            <td>Default</td>

                        </tr>
                        <tr>
                            <td><strong>Option</strong></td>
                            <td>Default</td>

                        </tr>	

                    </tbody>
                </table>
                <br class="clear">&nbsp;
                <h3>3. Chi tiết bồi thường</h3>
                <table class="list" style="width: 100%; margin-bottom: 10px;">
                    <thead>
                        <tr>
                            <th>Mã số HSBT</th>
                            <th>Họ và tên</th>

                            <th>Ngày xảy ra tổn thất</th>

                            <th>Ngày điều trị</th>
                            <th>Mã bệnh</th>
                            <th>Số tiền YCBT</th>
                            <th>Ngày BT</th>
                            <th>Số tiền được BT</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>UIC-2016.01</td>
                            <td>Nguyễn Cao Cường</td>
                            <td>12-03-2016</td>

                            <td>12-03-2016</td>
                            <td>Bệnh dạ dày</td>
                            <td>3.000.000đ</td>
                            <td>18-03-2016</td>
                            <td>2.500.000đ</td>

                        </tr>
                        <tr>
                            <td>UIC-2016.03</td>
                            <td>Nguyễn Cao Cường</td>
                            <td>12-04-2016</td>

                            <td>12-04-2016</td>
                            <td>Bệnh gan</td>
                            <td>3.000.000đ</td>
                            <td>18-04-2016</td>
                            <td>2.500.000đ</td>

                        </tr>
                        <tr>
                            <td>UIC-2016.05</td>
                            <td>Nguyễn Cao Cường</td>
                            <td>12-05-2016</td>

                            <td>12-05-2016</td>
                            <td>Bệnh mật</td>
                            <td>3.000.000đ</td>
                            <td>18-05-2016</td>
                            <td>2.500.000đ</td>

                        </tr>
                        <tr>
                            <td>UIC-2016.07</td>
                            <td>Nguyễn Cao Cường</td>
                            <td>12-05-2016</td>

                            <td>12-05-2016</td>
                            <td>Bệm viên phổi</td>
                            <td>3.000.000đ</td>
                            <td>18-05-2016</td>
                            <td>2.500.000đ</td>

                        </tr>					
                        <tr>
                            <td>UIC-2016.30</td>
                            <td>Nguyễn Cao Cường</td>
                            <td>12-06-2016</td>

                            <td>12-06-2016</td>
                            <td>Bệnh cảm cúm</td>
                            <td>3.000.000đ</td>
                            <td>18-06-2016</td>
                            <td>2.500.000đ</td>

                        </tr>
                        <tr>
                            <td colspan="5" style="text-align: center;"><strong>Tổng cộng</strong></td>

                            <td><strong>15.000.000đ</strong></td>
                            <td></td>
                            <td><strong>12.500.000đ</strong> </td>
                        </tr>


                    </tbody>
                </table>
                <br class="clear">&nbsp;
            </div>
        </div>
        <div class="col-md-3">
            <div class="db_box_item">
                <h5><i class="fa fa-gift fa-2x"></i>Videos</h5>
                <div class="db_box_content">
                    <iframe width="271" height="215" src="https://www.youtube.com/embed/dF3Dcol5XLg" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>

            <div class="db_box_item">
                <div class="content_html_txt">
                    <p><a href="http://ftcclaims.com/huong-dan-boi-thuong-bao-hiem-suc-khoe.html"><img class="-fw" src="http://ftcclaims.com/images/source/tpaftc/BHSK.jpg" alt="bao hiem suc khoe" data-pin-nopin="true"></a></p>

                    <p><a href="http://ftcclaims.com/tai-nguyen-mau-don-yeu-cau-boi-thuong.html"><img class="-fw" src="http://ftcclaims.com/images/source/tpaftc/DOWNLOAD.jpg" alt="tai nguyen, download" data-pin-nopin="true"></a></p>
                    <p><a href="http://ftcclaims.com/danh-sach-benh-vien-bao-lanh.html"><img class="-fw" src="http://ftcclaims.com/images/source/tpaftc/BVBAOLANH.jpg" alt="BENH VIEN bao lanh" data-pin-nopin="true"></a></p>
                    <p><a href="http://ftcclaims.com/danh-sach-benh-vien-bao-lanh.html"><img class="-fw" src="http://ftcclaims.com/images/source/tpaftc/NHATHUOC.jpg" alt="BENH VIEN bao lanh" data-pin-nopin="true"></a></p>                    
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="thong_tin_das">
            <p class="sty_flat_blue" style="color: white;
               font-size: 22px;
               width: 300px;
               text-align: center;
               display: inline-block;"><a href="/dashboard/orders/thong_tin_bao_hiem" target="_blank">Thông tin bảo hiểm</a></p>
            <p class="sty_flat_blue" style="color: white;
               font-size: 22px;
               width: 300px;
               text-align: center;
               display: inline-block;"><a href="/dashboard/orders/thong_tin_boi_thuong" target="_blank">Thông tin bồi thường</a></p>
            <p class="sty_flat_blue" style="color: white;
               font-size: 22px;
               width: 300px;
               text-align: center;
               display: inline-block;">Báo cáo</p>
            <p class="sty_flat_orange" style="color: white;
               font-size: 22px;
               width: 300px;
               text-align: center;
               display: inline-block;">1900 636438</p>
        </div>
    </div>
    <!--    <div class="row">
            <div class="col-md-6">
                <img src="/images/1.jpg" style="width: 100%;"/>
            </div>
            <div class="col-md-6">
                <img src="/images/2.jpg" style="width: 100%;"/>
            </div>
        </div>-->
    <div class="bao_cao_bh">

        <style>

            #myCarousel .carousel-caption {
                left:0;
                right:0;
                bottom:0;
                text-align:left;
                padding:10px;
                background:rgba(0,0,0,0.6);
                text-shadow:none;
            }

            #myCarousel .list-group {
                position:absolute;
                top:0;
                right:0;
            }
            #myCarousel .list-group-item {
                border-radius:0px;
                cursor:pointer;
            }
            #myCarousel .list-group .active {
                background-color:#eee;	
            }
            .list-group-item h4{
                margin: 0px;
            }
            .carousel-inner {
                position: relative;
                width: 100%;
                overflow: hidden;
                height: 520px;
            }
            .list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
                z-index: 2;
                color: #00badb;
                background-color: #337ab7;
                border-color: #337ab7;
            }

            @media (min-width: 992px) { 
                #myCarousel {padding-right:33.3333%;}
                #myCarousel .carousel-controls {display:none;} 	
            }
            @media (max-width: 991px) { 
                .carousel-caption p,
                #myCarousel .list-group {display:none;} 
            }

        </style>

        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner">

                <!--        <div class="item active">
                          <img src="/images/1.jpg">
                          
                        </div> End Item -->

                <div class="item active">
                    <img src="/images/2.jpg">

                </div><!-- End Item -->

                <div class="item">
                    <img src="/images/3.jpg">

                </div><!-- End Item -->

                <div class="item">
                    <img src="/images/4.jpg">

                </div><!-- End Item -->

                <div class="item">
                    <img src="/images/5.jpg">

                </div><!-- End Item -->

                <div class="item">
                    <img src="/images/6.jpg">

                </div><!-- End Item -->

                <div class="item">
                    <img src="/images/7.jpg">

                </div><!-- End Item -->

                <div class="item">
                    <img src="/images/8.jpg">

                </div><!-- End Item -->

                <div class="item">
                    <img src="/images/9.jpg">

                </div><!-- End Item -->

                <div class="item">
                    <img src="/images/10.jpg">

                </div><!-- End Item -->

                <div class="item">
                    <img src="/images/11.jpg">

                </div><!-- End Item -->       
            </div><!-- End Carousel Inner -->


            <ul class="list-group col-sm-5">
                <li data-target="#myCarousel" data-slide-to="0" class="list-group-item active"><h4>Số liệu bồi thường chung</h4></li>
                <li data-target="#myCarousel" data-slide-to="1" class="list-group-item"><h4>Số liệu bồi thường cho chương trình Nhân viên</h4></li>
                <li data-target="#myCarousel" data-slide-to="2" class="list-group-item"><h4>Số liệu bồi thường cho chương trình người thân</h4></li>
                <li data-target="#myCarousel" data-slide-to="3" class="list-group-item"><h4>Phân tích bồi thường theo quyền lợi</h4></li>
                <li data-target="#myCarousel" data-slide-to="4" class="list-group-item"><h4>Phân tích lý do từ chối một phần vượt giới hạn</h4></li>
                <li data-target="#myCarousel" data-slide-to="5" class="list-group-item"><h4>Phân tích lý do từ chối một phần do thuộc điểm loại trừ</h4></li>
                <li data-target="#myCarousel" data-slide-to="6" class="list-group-item"><h4>Phân tích bồi thường theo nhóm bệnh</h4></li>
                <li data-target="#myCarousel" data-slide-to="7" class="list-group-item"><h4>Phân tích bồi thường theo nhóm bệnh</h4></li>
                <li data-target="#myCarousel" data-slide-to="8" class="list-group-item"><h4>Phân tích bồi thường theo cơ sở y tế</h4></li>
                <li data-target="#myCarousel" data-slide-to="9" class="list-group-item"><h4>Top 10 HSBT có số tiền bồi thường cao nhất</h4></li>

            </ul>

            <!-- Controls -->
            <div class="carousel-controls">
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>

        </div><!-- End Carousel -->
    </div>
</div>
