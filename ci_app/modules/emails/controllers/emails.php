<?php
class Emails extends MY_Controller 
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function add_email()
    {
        $data = array('emails' => $this->input->post('email')
                    , 'registry_date' => date('Y-m-d H:i:s'));
        $this->emails_model->insert($data);
        echo 'Chúng tôi sẽ gửi thư cho bạn khi có thông tin mới';
    }
}
?>