<?php

class Orders_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        //modules::run('auth/auth/validate_login', $this->router->fetch_module());
        $this->_layout = 'admin_ui/layout/main';
        $this->_view_data['url'] = ORDER_ADMIN_BASE_URL;
        $this->ad_check_login();
        //======================================================================
        $ss_ql_all = $this->phpsession->get('ss_ql_all');
        $ss_ql_bt = $this->phpsession->get('ss_ql_bt');
        if ($ss_ql_all != QL_CHECKED) {
            if ($ss_ql_bt != QL_CHECKED) {
                redirect('dashboard');
            }
        }
        $this->load->model('products/products_coupon_model');
    }

    //end
    //==========================================================================
    function browse($offset = 0, $options = array()) {
        // Lấy các user
        $data_get = array();
        $users= $this->orders_model->CommonGet(TBL_USERS,$data_get);
        if(!empty($users)){
            $data['users']= $users;
        }
        //======================================================================
        $data_get = array(
            'join_' . TBL_CUSTOMERS => TRUE,
        );
        //======================================================================
        // Check permission
        $ss_ql_all = $this->phpsession->get('ss_ql_all');
        $ss_ql_bt = $this->phpsession->get('ss_ql_bt');
        $ss_ql_bt_nhap_hs = $this->phpsession->get('ss_ql_bt_nhap_hs');
        $ss_ql_bt_xuly = $this->phpsession->get('ss_ql_bt_xuly');
        $ss_ql_bt_duyet = $this->phpsession->get('ss_ql_bt_duyet');
        $ss_ql_bt_thanhtoan = $this->phpsession->get('ss_ql_bt_thanhtoan');
        //======================================================================
        // Filter
        $filter_status = $_GET['status'];
        if ($filter_status != '') {
            $data_get['order_status'] = $filter_status;
        } else {
            if ($ss_ql_bt_thanhtoan == QL_CHECKED || $ss_ql_all == QL_CHECKED) {
//                $data_get['order_status'] = BT_DATRATIEN;
//                $data_get['OW_order_status'] = BT_THANHTOAN;
//                $data_get['OW1_order_status'] = BT_DADUYET;
            } elseif ($ss_ql_bt_duyet == QL_CHECKED) {
                $data_get['order_status'] = BT_DADUYET;
                $data_get['OW_order_status'] = BT_PHEDUYET;
            } elseif ($ss_ql_bt_xuly == QL_CHECKED) {
                $data_get['order_status'] = BT_PHEDUYET;
                $data_get['OW_order_status'] = BT_CHODUYET;
            } elseif ($ss_ql_bt_nhap_hs == QL_CHECKED) {
                $data_get['order_status'] = BT_CHOBOSUNG;
                $data_get['OW_order_status'] = BT_CHODUYET;
            }
            //==================================================================
        }
        //----------------------------------------------------------------------
        $key = isset($_GET['hsbt']) ? $_GET['hsbt'] : '';
        if ($key != '' && $key > 0) {
            $data_get['bt_code'] = $key;
            $data_get['or_where']['case'] = $key;
        }
        //----------------------------------------------------------------------
        $kh_dbt = isset($_GET['kh_dbt']) ? $_GET['kh_dbt'] : '';
        if ($kh_dbt != '') {
            $filter_key_search = "(`" . DB_PREFIX . TBL_CUSTOMERS . "`.`congty_canhan` LIKE '%" . $kh_dbt . "%'";
            //$filter_key_search .= " OR `" . DB_PREFIX . TBL_CUSTOMERS . "`.`mst_cmt` LIKE '%" . $key . "%'";
            $filter_key_search .= " OR `" . DB_PREFIX . TBL_CUSTOMERS . "`.`mst_cmt` LIKE '%" . $kh_dbt . "%')";

            $data_get['like'] = $filter_key_search;
        }
        //----------------------------------------------------------------------
        $filter_pl_bt = $_GET['pl_bt'];
        if ($filter_pl_bt != '' && $filter_pl_bt > 0) {
            $data_get['phanloai_bt'] = $filter_pl_bt;
        }
        //----------------------------------------------------------------------
        $filter_user = $_GET['ft_user'];
        if ($filter_user != '' && $filter_user > 0) {
            $data_get['ft_user'] = $filter_user;
        }
        // =====================================================================
        // pagination
        $per_page = 10;
        // Đếm tổng số bt
        $total_rows = $this->orders_model->count_orders_by_options($data_get);
        $data['total_rows'] = $total_rows;
        //end
        //======================================================================
        $paging_config = array(
            'base_url' => site_url('dashboard/') . '/' . $this->uri->segment(2),
            'total_rows' => $total_rows,
            'per_page' => $per_page,
            'uri_segment' => 3,
            'use_page_numbers' => TRUE,
            'first_link' => __('IP_paging_first'),
            'last_link' => __('IP_paging_last'),
            'num_links' => 1,
        );
        if (isset($_GET) && $_GET != '') {
            $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($paging_config);
        $data_get['offset'] = ($offset > 0) ? ($offset - 1) * ($per_page) : $offset;
        $data_get['limit'] = $per_page;
        //end
        $bt = $this->orders_model->get_orders_by_options($data_get);
        //======================================================================
        if (!empty($bt)) {
            $data['bt'] = $bt;
        }
        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) {
            $data['options'] = $options;
        }
        $data['uri'] = ORDER_ADMIN_BASE_URL;
        $tpl = 'ad/ad_orders_list';
        $data['scripts'] = $this->scripts_select2();
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Quản lý bồi thường' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    //==========================================================================
    function add() {
        // Check permission
        $ss_ql_all = $this->phpsession->get('ss_ql_all');
        $ss_ql_bt_nhap_hs = $this->phpsession->get('ss_ql_bt_nhap_hs');
        if ($ss_ql_all != QL_CHECKED) {
            if ($ss_ql_bt_nhap_hs != QL_CHECKED) {
                redirect(ORDERS_ADMIN_BASE_URL);
            }
        }
        //======================================================================
        $options = array();
        if ($this->is_postback()) {
            if (!$this->_do_add_orders())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        // =====================================================================
        // Lấy kh được BH
        $data_get = array();
        $kh_dbh = $this->customers_model->get_canhan_dbh();
        if (!empty($kh_dbh)) {
            $options['kh_dbh'] = $kh_dbh;
        }
        //end
        //======================================================================
        // Lấy cơ sở y tế
        $data_get = array();
        $csyt = $this->products_style_model->get_csyt($data_get);
        if (!empty($csyt)) {
            $options['a_csyt'] = $csyt;
        }
        //======================================================================
        // Lấy all bt cha
//        $data_get = array(
//            'parent_id' => TRUE,
//        );
//        $bt = $this->orders_model->get_orders_by_options($data_get);
//        if (!empty($bt)) {
//            $options['bt_all'] = $bt;
//        }
        //======================================================================
        // Lấy ngân hàng
        $data_get = array();
        $nh = $this->orders_model->CommonGet(TBL_NGANHANG, $data_get);
        if (!empty($nh)) {
            $options['a_nh'] = $nh;
        }
        //======================================================================
        $options['scripts'] = $this->scripts_select2();
        $options['scripts1'] = $this->_get_scripts();
        //end
        //======================================================================
        $options['header'] = 'Thêm bồi thường';
        $options['button_name'] = 'Lưu dữ liệu';
        $tpl = 'ad/ad_bt_form_creat';
        //======================================================================
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $options['submit_uri'] = ORDERS_ADMIN_BASE_URL . '/add';
        $this->_view_data['title'] = 'Thêm bồi thường' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    //Edit mở
    function edit_bt($id = 0) {
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        $bt_id = $this->input->post('id');
        if ($bt_id > 0) {
            // Lấy thông tin bt
            $data_get = array(
                'join_' . TBL_CUSTOMERS => TRUE,
                'join_' . TBL_PRODUCTS . '_dbh' => TRUE,
                'BT_id' => $bt_id,
                'get_one' => TRUE,
            );
            $bt = $this->orders_model->get_orders_by_options($data_get);
            if (is_object($bt)) {
                $data['bt'] = $bt;
                $kh_dbh = $bt->OD_kh_dbh;
                $status = $bt->order_status;
                //==============================================================
                $data['bt'] = $bt;
                //==============================================================
                // Nếu bt đang chờ duyệt
                //if ($status == BT_CHODUYET) {
                // Khi submit
                if (isset($_POST['submit'])) {
                    //======================================================
                    $this->form_validation->set_rules('so_tien_ycbt', 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                    $this->form_validation->set_rules('so_ngay_ycbt', 'Số ngày yêu cầu bồi thường', 'trim|required|integer|xss_clean|max_length[4]');

                    $this->form_validation->set_rules('phuongthuc_bt', 'Phương thức bồi thường', 'trim|required|xss_clean');
                    $this->form_validation->set_rules('csyt', 'Cơ sở y tế', 'trim|required|xss_clean');
                    //------------------------------------------------------
                    $this->form_validation->set_rules('phuongthuc_nhan_hs', 'Hình thức nhận hồ sơ', 'trim|required|xss_clean');
                    $this->form_validation->set_rules('hinhthuc_nhantien_bt', 'Hình thức nhận tiền bồi thường', 'trim|required|xss_clean');
                    //$this->form_validation->set_rules('ghichu_nhantien', 'Thông tin tài khoản', 'trim|xss_clean');
                    $this->form_validation->set_rules('nth_fullname', 'Người thụ hưởng: Họ và tên', 'trim|xss_clean');
                    $this->form_validation->set_rules('nth_bank', 'Người thụ hưởng: Ngân hàng', 'trim|xss_clean');
                    $this->form_validation->set_rules('nth_stk', 'Người thụ hưởng: Số tài khoản', 'trim|xss_clean');
                    $this->form_validation->set_rules('nth_bank_address', 'Người thụ hưởng: Ghi chú', 'trim|xss_clean');
                    //------------------------------------------------------
                    $case = $this->input->post('case', TRUE);
                    if ($case != '' && $case > 0) {
                        $this->form_validation->set_rules('case', 'Mã Case bồi thường', 'trim|required|xss_clean|max_length[6]|min_length[6]');
                    } else {
                        $this->form_validation->set_rules('case', 'Mã Case bồi thường', 'trim|xss_clean');
                    }
                    $this->form_validation->set_rules('bt_code', 'Mã Hồ sơ bồi thường', 'trim|required|xss_clean|max_length[12]|min_length[11]');
                    //------------------------------------------------------

                    if ($this->form_validation->run($this)) {
                        $so_tien_ycbt = $this->input->post('so_tien_ycbt', TRUE);
                        //--------------------------------------------------
                        $date_xayra_tonthat = $this->input->post('date_xayra_tonthat', TRUE);
                        $date_nhan_bt = $this->input->post('date_nhan_bt', TRUE);
                        $date_xuly_bt = $this->input->post('date_xuly_bt', TRUE);

                        $post_data = array(
                            'so_tien_ycbt' => $so_tien_ycbt,
                            'so_ngay_ycbt' => $this->input->post('so_ngay_ycbt', TRUE),
                            'csyt' => $this->input->post('csyt', TRUE),
                            'phuongthuc_bt' => $this->input->post('phuongthuc_bt', TRUE),
                            'phuongthuc_nhan_hs' => $this->input->post('phuongthuc_nhan_hs', TRUE),
                            'hinhthuc_nhantien_bt' => $this->input->post('hinhthuc_nhantien_bt', TRUE),
                            //'ghichu_hinhthuc_nhantien_bt' => $this->input->post('ghichu_hinhthuc_nhantien_bt', TRUE),
                            //----------------------------------------------
                            'bt_code' => $this->input->post('bt_code', TRUE),
                            'case' => $this->input->post('case', TRUE),
                            //----------------------------------------------
                            'date_xayra_tonthat' => $date_xayra_tonthat,
                            'time_xayra_tonthat' => strtotime($date_xayra_tonthat),
                            'date_nhan_bt' => $date_nhan_bt,
                            'time_nhan_bt' => strtotime($date_nhan_bt),
                            'date_xuly_bt' => $date_xuly_bt,
                            'time_xuly_bt' => strtotime($date_xuly_bt),
                            //----------------------------------------------
                            'nth_fullname' => $this->input->post('nth_fullname', TRUE),
                            'nth_bank' => $this->input->post('nth_bank', TRUE),
                            'nth_stk' => $this->input->post('nth_stk', TRUE),
                            'nth_bank_address' => $this->input->post('nth_bank_address', TRUE),
                            'user1' => $this->phpsession->get('user_id'),
                            'editor' => $this->phpsession->get('user_id'),
                            'updated_date' => $datetime_now,
                            'updated_time' => $time_now,
                        );
                        //==================================================                            
                        // Nếu là bt case con
                        if ($case != '' && $case > 0) {
                            $post_data['case'] = $case;
                        }
//                            echo $bt_id;
//                            echo '<pre>';
//                            print_r($post_data);
//                            die;
                        //==================================================
                        $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                        redirect(ORDERS_ADMIN_BASE_URL);
                    } else {
                        $data['error'] = validation_errors();
                        if (isset($data['error'])) {
                            $data['options'] = $data;
                        }
                    }
                }
                //==========================================================
//                } else {
//                    redirect(ORDERS_ADMIN_BASE_URL);
//                }
            } else {
                redirect(ORDERS_ADMIN_BASE_URL);
            }
        } else {
            redirect(ORDERS_ADMIN_BASE_URL);
        }
        //======================================================================
        // Lấy cơ sở y tế
        $data_get = array();
        $csyt = $this->products_style_model->get_csyt($data_get);
        if (!empty($csyt)) {
            $data['a_csyt'] = $csyt;
        }
        //======================================================================
        // Lấy gói BH
        $data_get = array(
        );
        $goi_bh = $this->orders_model->CommonGet(TBL_PRODUCTS_SIZE, $data_get);
        if (!empty($goi_bh)) {
            $data['a_goi_bh'] = $goi_bh;
        }
        //======================================================================
        // Lấy mã bệnh
        $ma_benh = $this->products_origin_model->get_products_origin();
        if (!empty($ma_benh)) {
            $data['ma_benh'] = $ma_benh;
        }
        //======================================================================
        // Lấy các ngân hàng
        $data_get = array();
        $nh = $this->orders_model->CommonGet(TBL_NGANHANG, $data_get);
        if (!empty($nh)) {
            $data['a_nh'] = $nh;
        }
        //======================================================================
        $tpl = 'ad/ad_bt_form_edit_choduyet';
        $data['scripts'] = $this->scripts_select2();
        //end
        $data['header'] = 'Sửa bồi thường';
        $data['button_name'] = 'Sửa bồi thường';
        $data['submit_uri'] = ORDER_ADMIN_BASE_URL . '/edit-bt';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        // =====================================================================    
        $this->_view_data['title'] = 'Xem thông tin bồi thường' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    function _edit_bt($id = 0) {
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        $bt_id = $this->input->post('id');
        if ($bt_id > 0) {
            // Lấy thông tin bt
            $data_get = array(
                'join_' . TBL_CUSTOMERS => TRUE,
                'join_' . TBL_PRODUCTS . '_dbh' => TRUE,
                'BT_id' => $bt_id,
                'get_one' => TRUE,
            );
            $bt = $this->orders_model->get_orders_by_options($data_get);
            if (is_object($bt)) {
                $data['bt'] = $bt;
                $kh_dbh = $bt->OD_kh_dbh;
                $status = $bt->order_status;
                //==============================================================
                $data['bt'] = $bt;
                //==============================================================
                // Nếu bt đang chờ duyệt
                if ($status == BT_CHODUYET) {
                    // Khi submit
                    if (isset($_POST['submit'])) {
                        //======================================================
                        $this->form_validation->set_rules('so_tien_ycbt', 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules('so_ngay_ycbt', 'Số ngày yêu cầu bồi thường', 'trim|required|integer|xss_clean|max_length[4]');

                        $this->form_validation->set_rules('phuongthuc_bt', 'Phương thức bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules('csyt', 'Cơ sở y tế', 'trim|required|xss_clean');
                        //------------------------------------------------------
                        $this->form_validation->set_rules('phuongthuc_nhan_hs', 'Hình thức nhận hồ sơ', 'trim|required|xss_clean');
                        $this->form_validation->set_rules('hinhthuc_nhantien_bt', 'Hình thức nhận tiền bồi thường', 'trim|required|xss_clean');
                        //$this->form_validation->set_rules('ghichu_nhantien', 'Thông tin tài khoản', 'trim|xss_clean');
                        $this->form_validation->set_rules('nth_fullname', 'Người thụ hưởng: Họ và tên', 'trim|xss_clean');
                        $this->form_validation->set_rules('nth_bank', 'Người thụ hưởng: Ngân hàng', 'trim|xss_clean');
                        $this->form_validation->set_rules('nth_stk', 'Người thụ hưởng: Số tài khoản', 'trim|xss_clean');
                        $this->form_validation->set_rules('nth_bank_address', 'Người thụ hưởng: Ghi chú', 'trim|xss_clean');
                        //------------------------------------------------------
                        $case = $this->input->post('case', TRUE);
                        if ($case != '' && $case > 0) {
                            $this->form_validation->set_rules('case', 'Mã Case bồi thường', 'trim|required|xss_clean|max_length[6]|min_length[6]');
                        } else {
                            $this->form_validation->set_rules('case', 'Mã Case bồi thường', 'trim|xss_clean');
                        }
                        //------------------------------------------------------

                        if ($this->form_validation->run($this)) {
                            $so_tien_ycbt = $this->input->post('so_tien_ycbt', TRUE);
                            $post_data = array(
                                'so_tien_ycbt' => $so_tien_ycbt,
                                'so_ngay_ycbt' => $this->input->post('so_ngay_ycbt', TRUE),
                                'csyt' => $this->input->post('csyt', TRUE),
                                'phuongthuc_bt' => $this->input->post('phuongthuc_bt', TRUE),
                                'phuongthuc_nhan_hs' => $this->input->post('phuongthuc_nhan_hs', TRUE),
                                'hinhthuc_nhantien_bt' => $this->input->post('hinhthuc_nhantien_bt', TRUE),
                                //'ghichu_hinhthuc_nhantien_bt' => $this->input->post('ghichu_hinhthuc_nhantien_bt', TRUE),
                                'nth_fullname' => $this->input->post('nth_fullname', TRUE),
                                'nth_bank' => $this->input->post('nth_bank', TRUE),
                                'nth_stk' => $this->input->post('nth_stk', TRUE),
                                'nth_bank_address' => $this->input->post('nth_bank_address', TRUE),
                                'user1' => $this->phpsession->get('user_id'),
                                'editor' => $this->phpsession->get('user_id'),
                                'updated_date' => $datetime_now,
                                'updated_time' => $time_now,
                            );
                            //==================================================                            
                            // Nếu là bt case con
                            if ($case != '' && $case > 0) {
                                $post_data['case'] = $case;
                            }
//                            echo $bt_id;
//                            echo '<pre>';
//                            print_r($post_data);
//                            die;
                            //==================================================
                            $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                            redirect(ORDERS_ADMIN_BASE_URL);
                        } else {
                            $data['error'] = validation_errors();
                            if (isset($data['error'])) {
                                $data['options'] = $data;
                            }
                        }
                    }
                    //==========================================================
                } else {
                    redirect(ORDERS_ADMIN_BASE_URL);
                }
            } else {
                redirect(ORDERS_ADMIN_BASE_URL);
            }
        } else {
            redirect(ORDERS_ADMIN_BASE_URL);
        }
        //======================================================================
        // Lấy cơ sở y tế
        $data_get = array();
        $csyt = $this->products_style_model->get_csyt($data_get);
        if (!empty($csyt)) {
            $data['a_csyt'] = $csyt;
        }
        //======================================================================
        // Lấy gói BH
        $data_get = array(
        );
        $goi_bh = $this->orders_model->CommonGet(TBL_PRODUCTS_SIZE, $data_get);
        if (!empty($goi_bh)) {
            $data['a_goi_bh'] = $goi_bh;
        }
        //======================================================================
        // Lấy mã bệnh
        $ma_benh = $this->products_origin_model->get_products_origin();
        if (!empty($ma_benh)) {
            $data['ma_benh'] = $ma_benh;
        }
        //======================================================================
        // Lấy các ngân hàng
        $data_get = array();
        $nh = $this->orders_model->CommonGet(TBL_NGANHANG, $data_get);
        if (!empty($nh)) {
            $data['a_nh'] = $nh;
        }
        //======================================================================
        $tpl = 'ad/ad_bt_form_edit_choduyet';
        $data['scripts'] = $this->scripts_select2();
        //end
        $data['header'] = 'Sửa bồi thường';
        $data['button_name'] = 'Sửa bồi thường';
        $data['submit_uri'] = ORDER_ADMIN_BASE_URL . '/edit-bt';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        // =====================================================================    
        $this->_view_data['title'] = 'Xem thông tin bồi thường' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    // Sửa bồi thường
    function edit($id = 0) {
        // Check permission
        $ss_ql_all = $this->phpsession->get('ss_ql_all');
        $ss_ql_bt = $this->phpsession->get('ss_ql_bt');
        $ss_ql_bt_nhap_hs = $this->phpsession->get('ss_ql_bt_nhap_hs');
        $ss_ql_bt_xuly = $this->phpsession->get('ss_ql_bt_xuly');
        $ss_ql_bt_duyet = $this->phpsession->get('ss_ql_bt_duyet');
        $ss_ql_bt_thanhtoan = $this->phpsession->get('ss_ql_bt_thanhtoan');
        //======================================================================
        if ($ss_ql_all != QL_CHECKED) {
            if ($ss_ql_bt != QL_CHECKED) {
                redirect(ORDERS_ADMIN_BASE_URL);
            }
        }
        //======================================================================
        $options = $data = array();
        $a_bt_status1 = get_a_bt_status1();
        $bt_id = $this->input->post('id');
        $bt_id = ($bt_id != '') ? $bt_id : $id;
        //======================================================================
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        if ($id > 0 || ($bt_id != '' && $bt_id > 0)) {
            // Lấy thông tin bt
            $data_get = array(
                'BT_id' => $bt_id,
                'get_one' => TRUE,
            );
            $bt = $this->orders_model->get_orders_by_options($data_get);
//            echo '<pre>';
//            print_r($bt);
//            die;
            if (is_object($bt)) {
                $data['bt'] = $bt;
                $kh_dbh = $bt->OD_kh_dbh;
                $status = $bt->order_status;
                $phanloai_bt = $bt->phanloai_bt;
                //$loai_benh = $bt->loai_benh;
                $quyenloi_bt = $bt->quyenloi_bt;
                $quyenloi_bt_t = get_qlbt_field_by_qlbt($quyenloi_bt);

                $a_loai_benh_sn = get_a_loai_benh_sn();
                $a_loai_benh_slk = get_a_loai_benh_slk();
                //==============================================================
                if ($ss_ql_all != QL_CHECKED) {
//                    echo $status;
//                    die;
                    // Nếu bt đã trả tiền
                    if ($status == BT_DATRATIEN) {
                        if ($ss_ql_bt_thanhtoan != QL_CHECKED) {
                            redirect(ORDERS_ADMIN_BASE_URL);
                        }
                    }
                    //==========================================================
                    // Nếu bt thanh toán
                    if ($status == BT_THANHTOAN) {
                        if ($ss_ql_bt_thanhtoan != QL_CHECKED) {
                            redirect(ORDERS_ADMIN_BASE_URL);
                        }
                    }
                    //==========================================================
                    // Nếu bt đã duyệt
                    if ($status == BT_DADUYET) {
                        if ($ss_ql_bt_duyet != QL_CHECKED && $ss_ql_bt_thanhtoan != QL_CHECKED) {
                            redirect(ORDERS_ADMIN_BASE_URL);
                        }
                    }
                    //==========================================================
                    // Nếu bt phê duyệt
                    if ($status == BT_PHEDUYET) {
                        if ($ss_ql_bt_duyet != QL_CHECKED && $ss_ql_bt_xuly != QL_CHECKED) {
                            redirect(ORDERS_ADMIN_BASE_URL);
                        }
                    }
                    //==========================================================
                    // Nếu bt nhập liệu
                    if ($status == BT_CHODUYET || $status == BT_CHOBOSUNG) {
                        if ($ss_ql_bt_nhap_hs != QL_CHECKED && $ss_ql_bt_xuly != QL_CHECKED) {
                            redirect(ORDERS_ADMIN_BASE_URL);
                        }
                    }
                    //==========================================================
                }
                //==============================================================
                // Lấy thông tin bh
                $data_get = array(
                    'kh_dbh' => $kh_dbh,
                    'join_' . TBL_PRODUCTS_SIZE => TRUE,
                    'join_' . TBL_CUSTOMERS . '_dbh' => TRUE,
                    'get_one' => TRUE,
                );
                $goi_bh = $this->customers_model->get_bh($data_get);
                if (is_object($goi_bh)) {
                    //$data['goi_bh'] = $goi_bh;
                    $data['bh'] = $goi_bh;
//                    echo '<pre>';
//                    print_r($a);
//                    die;
//                  //==========================================================
                    // Khi submit lưu dữ liệu
                    if ($this->is_postback() && !$this->input->post('from_list')) {
                        $order_status = $this->input->post('order_status', TRUE);
                        $data_push = array('bt' => $bt, 'goi_bh' => $goi_bh);
                        //======================================================
                        // Nếu bt thiếu ct
                        if ($status == BT_CHOBOSUNG) {
                            // Check permission
                            if ($ss_ql_bt_nhap_hs == QL_CHECKED) {
                                if (!$this->do_edit_orders_bosung($bt_id)) {
                                    $data['error'] = validation_errors();
                                }
                            } else {
                                redirect(ORDERS_ADMIN_BASE_URL);
                            }
                        } elseif ($status == BT_CHODUYET) {
                            // Check permission
                            if ($ss_ql_bt_xuly == QL_CHECKED) {
                                if ($phanloai_bt == BT_THAISAN_QLNT) {
                                    if (!$this->do_edit_orders_giaiquyet_tsqlnt($data_push)) {
                                        $data['error'] = validation_errors();
                                    }
                                } elseif ($phanloai_bt == BT_DTNT_OB) {
                                    if (!$this->do_edit_orders_giaiquyet_dtnt_ob($data_push)) {
                                        $data['error'] = validation_errors();
                                    }
                                } elseif ($phanloai_bt == BT_DTNGT_OB) {
                                    if (!$this->do_edit_orders_giaiquyet_dtngt_ob($data_push)) {
                                        $data['error'] = validation_errors();
                                    }
                                } elseif ($phanloai_bt == BT_THAISAN_MDL) {
                                    if (!$this->do_edit_orders_giaiquyet_thaisan_mdl(array('bt' => $bt, 'goi_bh' => $goi_bh))) {
                                        $data['error'] = validation_errors();
                                    }
                                } elseif ($phanloai_bt == BT_NHAKHOA_MDL) {
                                    if (!$this->do_edit_orders_giaiquyet_nhakhoa_mdl(array('bt' => $bt, 'goi_bh' => $goi_bh))) {
                                        $data['error'] = validation_errors();
                                    }
                                } else {
                                    if (!$this->do_edit_orders_giaiquyet(array('bt' => $bt))) {
                                        $data['error'] = validation_errors();
                                    }
                                }
                            } else {
                                redirect(ORDERS_ADMIN_BASE_URL);
                            }
                        } elseif ($status == BT_PHEDUYET) {
                            if ($ss_ql_bt_duyet == QL_CHECKED) {
                                // Nếu từ chối phê duyệt bt
                                if (isset($_POST['cancel']) && $_POST['cancel'] == BT_PHEDUYET_CANCEL) {
                                    // Update bt giải quyết
                                    $data_update = array(
                                        'user3' => $this->phpsession->get('user_id'),
                                        'order_status' => BT_CHODUYET,
                                        'ghichu_pheduyet_bt' => $this->input->post('ghichu_pheduyet_bt', TRUE),
                                        'updated_date' => $datetime_now,
                                        'updated_time' => $time_now,
                                        //'date_duyet_bt' => $datetime_now,
                                        //'time_duyet_bt' => $time_now,
                                        'editor' => $this->phpsession->get('user_id'),
                                    );
                                    //==========================================
//                                    echo '<pre>';
//                                    print_r($data_update);
//                                    die;
                                    $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $data_update);
                                } else {
                                    // Nếu bt chờ duyệt
                                    $data_update = array(
                                        'user3' => $this->phpsession->get('user_id'),
                                        'order_status' => BT_DADUYET,
                                        'ghichu_pheduyet_bt' => $this->input->post('ghichu_pheduyet_bt', TRUE),
                                        'updated_date' => $datetime_now,
                                        'updated_time' => $time_now,
                                        'date_duyet_bt' => $datetime_now,
                                        'time_duyet_bt' => $time_now,
                                        'date_thongbao_kh' => $datetime_now,
                                        'time_thongbao_kh' => $time_now,
                                        'editor' => $this->phpsession->get('user_id'),
                                    );
                                    //==========================================
//                                    echo '<pre>';
//                                    print_r($data_update);
//                                    die;
                                    $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $data_update);
                                }
                            }
                            redirect(ORDER_ADMIN_BASE_URL);
                        } elseif ($status == BT_DADUYET) {
                            if ($ss_ql_bt_thanhtoan == QL_CHECKED) {
                                //==============================================
                                // Nếu từ chối thanh toán bt
                                if (isset($_POST['cancel']) && $_POST['cancel'] == BT_THANHTOAN_CANCEL) {
                                    // Update bt giải quyết
                                    $data_update = array(
                                        'user4' => $this->phpsession->get('user_id'),
                                        'order_status' => BT_PHEDUYET,
                                        'ghichu_thanhtoan_bt' => $this->input->post('ghichu_thanhtoan_bt', TRUE),
                                        'updated_date' => $datetime_now,
                                        'updated_time' => $time_now,
                                        //'date_duyet_bt' => $datetime_now,
                                        //'time_duyet_bt' => $time_now,
                                        'editor' => $this->phpsession->get('user_id'),
                                    );
                                    //==========================================
//                                    echo '<pre>';
//                                    print_r($data_update);
//                                    die;
                                    $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $data_update);
                                    //redirect(ORDER_ADMIN_BASE_URL);
                                } else {
                                    if ($phanloai_bt == BT_THAISAN_QLNT) {
                                        if (!$this->do_edit_orders_daduyet_tsqlnt($data_push)) {
                                            $data['error'] = validation_errors();
                                        }
                                    } elseif ($phanloai_bt == BT_DTNT_OB) {
                                        if (!$this->do_edit_orders_daduyet_dtnt_ob($data_push)) {
                                            $data['error'] = validation_errors();
                                        }
                                    } elseif ($phanloai_bt == BT_DTNGT_OB) {
                                        if (!$this->do_edit_orders_daduyet_dtngt_ob($data_push)) {
                                            $data['error'] = validation_errors();
                                        }
                                    } elseif ($phanloai_bt == BT_THAISAN_MDL) {
                                        if (!$this->do_edit_orders_daduyet_thaisan_mdl(array('bt' => $bt, 'goi_bh' => $goi_bh))) {
                                            $data['error'] = validation_errors();
                                        }
                                    } elseif ($phanloai_bt == BT_NHAKHOA_MDL) {
                                        if (!$this->do_edit_orders_daduyet_nhakhoa_mdl(array('bt' => $bt, 'goi_bh' => $goi_bh))) {
                                            $data['error'] = validation_errors();
                                        }
                                    } else {
                                        if (!$this->do_edit_orders_daduyet(array('bt' => $bt, 'goi_bh' => $goi_bh))) {
                                            $data['error'] = validation_errors();
                                        }
                                    }
                                }
                                redirect(ORDER_ADMIN_BASE_URL);
                            } else {
                                redirect(ORDERS_ADMIN_BASE_URL);
                            }
                        } elseif ($status == BT_THANHTOAN) {
                            if ($ss_ql_bt_thanhtoan == QL_CHECKED) {
                                // Nếu bt đã thanh toán
                                $data_update = array(
                                    'user5' => $this->phpsession->get('user_id'),
                                    'order_status' => BT_DATRATIEN,
                                    'updated_date' => $datetime_now,
                                    'updated_time' => $time_now,
                                    'date_tratien' => $datetime_now,
                                    'time_tratien' => $time_now,
                                    'editor' => $this->phpsession->get('user_id'),
                                );
//                                echo '<pre>';
//                                print_r($data_update);
//                                die;
                                $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $data_update);
                            }
                            redirect(ORDER_ADMIN_BASE_URL);
                        } else {
                            // Nếu bt đã trả tiền
                        }
                        if (isset($data['error'])) {
                            $data['options'] = $data;
                        }
                    }
                    // end
                    //==========================================================
                    // Nếu đã chọn ql bh
                    if ($quyenloi_bt != '') {
                        $st_bh_bt = $goi_bh->$quyenloi_bt;
                        $data['st_bh_bt'] = $st_bh_bt;
                    }
                    //end
                    //==========================================================
                }
                //end
                //==============================================================
                // Nếu bt thiếu chứng từ
                if ($status == BT_CHOBOSUNG) {
                    // Lấy chứng từ theo loại bt
                    $data_get = array(
                        'parent_id' => $phanloai_bt,
                    );
                    $ct = $this->products_coupon_model->get_products_coupon($data_get);
                    if (!empty($ct)) {
                        $data['ct'] = $ct;
                    }
                    $tpl = 'ad/ad_bt_form_edit_chobosung';
                } elseif ($status == BT_CHODUYET) {
                    //==========================================================
                } else {
                    //Lấy quyền lợi bt
                    $data_get = array(
                        'QLBT_KH_id' => $kh_dbh,
                        'QLBT_BT_id' => $bt_id,
                    );
                    $qlbt = $this->orders_model->get_quyenloi_bt($data_get);
                    if (!empty($qlbt)) {
                        $data['qlbt'] = $qlbt;
                    }
                    //end
                    //==========================================================
                    // Lấy bt còn lại
                    $data_get = array(
                        'CL_kh_id' => $kh_dbh,
                            //'CL_quyenloi_bt' => $quyenloi_bt_t,
                            //'get_row' => TRUE,
                    );
                    $qlbt_cl = $this->orders_model->get_ql_cl($data_get);
                    if (!empty($qlbt_cl)) {
                        $data['qlbt_cl'] = $qlbt_cl;
                    }
                    //end
                    //==========================================================
                }
                if ($status == BT_CHODUYET) {
                    $tpl = 'ad/ad_bt_form_edit_giaiquyet';
                }
                // Nếu đã duyệt bt
                if (in_array($status, $a_bt_status1)) {
                    $tpl = 'ad/ad_bt_form_edit_daduyet';
                }
            }
        }
        //======================================================================
        // Lấy mã bệnh
        $ma_benh = $this->products_origin_model->get_products_origin();
        if (!empty($ma_benh)) {
            $data['ma_benh'] = $ma_benh;
        }
        //======================================================================
        // Lấy cơ sở y tế
        $data_get = array();
        $csyt = $this->products_style_model->get_csyt($data_get);
        if (!empty($csyt)) {
            $data['a_csyt'] = $csyt;
        }
        //======================================================================
        // Lấy các ngân hàng
        $data_get = array();
        $nh = $this->orders_model->CommonGet(TBL_NGANHANG, $data_get);
        if (!empty($nh)) {
            $data['a_nh'] = $nh;
        }
        //======================================================================
        // Lấy quyền lợi bh
        if ($status != BT_CHOBOSUNG) {
            // Lấy html thông tin gói bh
            if ($status == BT_CHODUYET) {
                $tpl_qlbt = 'ad/ad_form_qlbt_giaiquyet';
//                $tpl_qlbt = 'ad/ad_ajax_bh';
            } elseif (in_array($status, $a_bt_status1)) {
                $tpl_qlbt = 'ad/ad_form_qlbt_daduyet';
            } else {
                $tpl_qlbt = 'ad/ad_ajax_bh';
            }
            $bh_html = $this->load->view($tpl_qlbt, $data, TRUE);
            $data['bh_html'] = $bh_html;
        }
        // end
        // =====================================================================        
        $data['scripts'] = $this->scripts_select2();
        $data['scripts1'] = $this->_get_scripts();
        //end
        $options['header'] = 'Thêm bồi thường';
        $options['button_name'] = 'Lưu dữ liệu';
        $tpl = isset($tpl) ? $tpl : 'ad/ad_bt_form_edit_choduyet';
        $data['header'] = 'Sửa bồi thường';
        $data['button_name'] = 'Sửa bồi thường';
        $data['submit_uri'] = ORDER_ADMIN_BASE_URL . '/edit';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        // =====================================================================    
        $this->_view_data['title'] = 'Xem thông tin bồi thường' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    function do_edit_orders_bosung($bt_id = 0) {
        $this->form_validation->set_rules('phanloai_bt', 'Loại bồi thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('so_tien_ycbt', 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('so_ngay_ycbt', 'Số ngày yêu cầu bồi thường', 'trim|required|integer|xss_clean|max_length[4]');

        $this->form_validation->set_rules('phuongthuc_bt', 'Phương thức bồi thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('csyt', 'Cơ sở y tế', 'trim|required|xss_clean');
        //------------------------------------------------------
        $this->form_validation->set_rules('phuongthuc_nhan_hs', 'Hình thức nhận hồ sơ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('hinhthuc_nhantien_bt', 'Hình thức nhận tiền bồi thường', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('ghichu_nhantien', 'Thông tin tài khoản', 'trim|xss_clean');
        $this->form_validation->set_rules('nth_fullname', 'Người thụ hưởng: Họ và tên', 'trim|xss_clean');
        $this->form_validation->set_rules('nth_bank', 'Người thụ hưởng: Ngân hàng', 'trim|xss_clean');
        $this->form_validation->set_rules('nth_stk', 'Người thụ hưởng: Số tài khoản', 'trim|xss_clean');
        $this->form_validation->set_rules('nth_bank_address', 'Người thụ hưởng: Ghi chú', 'trim|xss_clean');
        //------------------------------------------------------
        $case = $this->input->post('case', TRUE);
        if ($case != '' && $case > 0) {
            $this->form_validation->set_rules('case', 'Mã Case bồi thường', 'trim|required|xss_clean|max_length[6]|min_length[6]');
        } else {
            $this->form_validation->set_rules('case', 'Mã Case bồi thường', 'trim|xss_clean');
        }
        $this->form_validation->set_rules('bt_code', 'Mã Hồ sơ bồi thường', 'trim|required|xss_clean|max_length[12]|min_length[11]');
        //----------------------------------------------------------------------
        $phanloai_bt = $this->input->post('phanloai_bt', TRUE);

        // Lấy chứng từ
        $data_get = array(
            'parent_id' => $phanloai_bt,
        );
        $ct = $this->products_coupon_model->get_products_coupon($data_get);
        if (!empty($ct) && $bt_id > 0) {
            $ct_num = count($ct);
            //end
            $ct_bt = $this->input->post('ct_bt', TRUE);
//            echo $ct_num.'/'. count($ct_bt);
//            die;
            // Nếu đủ chứng từ
            //==================================================================
            $datetime_now = date('Y-m-d H:i:s');
            $date_now = date('Y-m-d');
            $time_now = strtotime($date_now);
            //==================================================================
            if (!empty($ct_bt) && $ct_num == count($ct_bt)) {
                //$this->form_validation->set_rules('so_tien_ycbt', 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                //$this->form_validation->set_rules('so_ngay_ycbt', 'Số ngày yêu cầu bồi thường', 'trim|required|integer|xss_clean|max_length[4]');
                //$so_ngay_ycbt = $this->input->post('so_ngay_ycbt', TRUE);
                // Nếu số ngày < 0
                //if ($so_ngay_ycbt < 0) {
                //redirect(ORDER_ADMIN_BASE_URL);
                //}
                //end
                //==============================================================
                $data['order_status'] = BT_CHODUYET;
                //$data['so_tien_ycbt'] = $this->input->post('so_tien_ycbt');
                //$data['so_ngay_ycbt'] = $so_ngay_ycbt;
                $data['ghichu_hoso'] = $this->input->post('ghichu_hoso', TRUE);
                //==============================================================
                //$data['date_xuly_bt'] = $datetime_now;
                //$data['time_xuly_bt'] = $time_now;
            } else {
                //$this->form_validation->set_rules('chungtu_bosung_txt', 'Chứng từ chờ bổ sung', 'trim|required|xss_clean');
                if (!empty($ct_bt)) {
                    $str_ct_bt_id = '';
                    foreach ($ct_bt as $k => $v) {
                        $str_ct_bt_id .= $v . ',';
                    }
                    $data['chungtu_bosung'] = substr($str_ct_bt_id, 0, -1);
                }
                //$data['chungtu_bosung_txt'] = $this->input->post('chungtu_bosung_txt', TRUE);
                $data['order_status'] = BT_CHOBOSUNG;
            }
            // Xử lý
            //==================================================================
            if ($this->form_validation->run()) {
                $date_xayra_tonthat = $this->input->post('date_xayra_tonthat');
                $date_nhan_bt = $this->input->post('date_nhan_bt');
                $date_xuly_bt = $this->input->post('date_xuly_bt');
                $so_tien_ycbt = $this->input->post('so_tien_ycbt', TRUE);
                $post_data = array(
                    //'phanloai_bt' => $phanloai_bt,
                    //'quyenloi_bt' => get_phanloai_bt_txt($phanloai_bt),
                    //'OD_kh_dbh' => $this->input->post('kh_dbh', TRUE),
                    //'parent_id' => $this->input->post('case', TRUE),
                    'so_tien_ycbt' => $so_tien_ycbt,
                    //'st_ycbt_t' => $so_tien_ycbt,
                    'so_ngay_ycbt' => $this->input->post('so_ngay_ycbt', TRUE),
                    'csyt' => $this->input->post('csyt', TRUE),
                    'phuongthuc_bt' => $this->input->post('phuongthuc_bt', TRUE),
                    'hinhthuc_nhantien_bt' => $this->input->post('hinhthuc_nhantien_bt', TRUE),
                    //----------------------------------------------------------
                    'bt_code' => $this->input->post('bt_code', TRUE), // Đang mở test
                    //----------------------------------------------------------
                    'date_xayra_tonthat' => $date_xayra_tonthat,
                    'time_xayra_tonthat' => strtotime($date_xayra_tonthat),
                    'date_nhan_bt' => $date_nhan_bt,
                    'time_nhan_bt' => strtotime($date_nhan_bt),
                    'date_xuly_bt' => $date_xuly_bt,
                    'time_xuly_bt' => strtotime($date_xuly_bt),
                    //----------------------------------------------------------
                    'nth_fullname' => $this->input->post('nth_fullname', TRUE),
                    'nth_bank' => $this->input->post('nth_bank', TRUE),
                    'nth_stk' => $this->input->post('nth_stk', TRUE),
                    'nth_bank_address' => $this->input->post('nth_bank_address', TRUE),
                    //----------------------------------------------------------
                    'user1' => $this->phpsession->get('user_id'),
                    'editor' => $this->phpsession->get('user_id'),
                    'updated_date' => $datetime_now,
                    'updated_time' => $time_now,
                );
                //==============================================================
                // Nếu là bt case con
                if ($case != '' && $case > 0) {
                    $post_data['case'] = $case;
                } else {
//                    $date_now = date('Y-m-d');
//                    $code = substr($date_now, 2, 2) . substr($date_now, 5, 2) . substr($date_now, 8, 2) . get_code_bt($bt_id);
//                    $post_data['bt_code'] = $code;
                }
                //==============================================================
                $post_data = array_merge($post_data, $data);
//                echo $bt_id;
//                echo '<pre>';
//                print_r($post_data);
//                die;
                // Update data
                $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                redirect(ORDERS_ADMIN_BASE_URL);
            }
            return FALSE;
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
    }

    //end
    //==========================================================================
    function do_edit_orders_giaiquyet($options = array()) {
        $data = array();
        $a_qlbt_sn = get_a_loai_benh_sn();
        $a_qlbt_slk = get_a_loai_benh_slk();
        $a_quyenloi_bt_not_select = get_a_quyenloi_bt_not_select();
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt'])) {
            $bt = $options['bt'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $st_ycbt = $bt->so_tien_ycbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            $bt_status = $bt->order_status;
            //==================================================================
            if ($bt_status != BT_CHODUYET) {
                redirect(ORDERS_ADMIN_BASE_URL);
            }
            // end
            //==================================================================
            if (in_array($quyenloi_bt, $a_quyenloi_bt_not_select)) {
                //
            } else {
                // Phải chọn qlbt con
                $this->form_validation->set_rules('quyenloi_bt_2', 'Quyền lợi bồi thường', 'trim|required|xss_clean');
                $quyenloi_bt = $this->input->post('quyenloi_bt_2', TRUE);
            }
            //==================================================================
            if ($quyenloi_bt != '') {
                // Lấy số tiền dbt
                if (in_array($quyenloi_bt, $a_quyenloi_bt_not_select)) {
                    $this->form_validation->set_rules('st_dbt_2', 'Số tiền được bồi thường', 'trim|required|xss_clean');
                    $so_tien_dbt = $this->input->post('st_dbt_2', TRUE);
                } else {
                    $field_st_dbt = strtolower($quyenloi_bt) . '_st_dbt';
//                    echo $field_st_dbt;
//                    die;
                    $this->form_validation->set_rules($field_st_dbt, 'Số tiền được bồi thường', 'trim|required|xss_clean');
                    $so_tien_dbt = $this->input->post($field_st_dbt, TRUE);
                }
                //==============================================================
                // Nếu bt có số ngày
                if (in_array($quyenloi_bt, $a_qlbt_sn)) {
                    $this->form_validation->set_rules('sn_dbt_2', 'Số ngày được bồi thường', 'trim|required|xss_clean');
                    $so_ngay_dbt = $this->input->post('sn_dbt_2', TRUE);
                }
                //==============================================================
                $this->form_validation->set_rules('ma_benh', 'Mã bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('chuandoan_benh', 'Chuẩn đoán bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('lydo_khong_bt', 'Lý do không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_khong_bt', 'Ghi chú không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_xuly_bt', 'Ghi chú xử lý BT', 'trim|xss_clean');
                //==============================================================
                if ($so_tien_dbt > 0) {
                    $datetime_now = date('Y-m-d H:i:s');
                    $date_now = date('Y-m-d');
                    $time_now = strtotime($date_now);
                    //==========================================================
                    // Xử lý
                    if ($this->form_validation->run()) {
                        $bt_status_update = BT_PHEDUYET;

                        $post_data = array(
                            'user2' => $this->phpsession->get('user_id'),
                            'order_status' => $bt_status_update,
                            'so_tien_dbt' => $so_tien_dbt,
                            //'st_dbt_t' => $so_tien_dbt,
                            'OD_ma_benh' => $this->input->post('ma_benh', TRUE),
                            'chuandoan_benh' => $this->input->post('chuandoan_benh', TRUE),
                            'lydo_khong_bt' => $this->input->post('lydo_khong_bt', TRUE),
                            'ghichu_khong_bt' => $this->input->post('ghichu_khong_bt', TRUE),
                            'ghichu_xuly_bt' => $this->input->post('ghichu_xuly_bt', TRUE),
                        );
                        // Nếu bt có số ngày
                        if (isset($so_ngay_dbt) && $so_ngay_dbt != '') {
                            $post_data['so_ngay_dbt'] = $so_ngay_dbt;
                        }
                        //======================================================
                        $post_data['updated_date'] = $datetime_now;
                        $post_data['updated_time'] = $time_now;

//                        echo '<pre>';
//                        print_r($post_data);
//                        die;
                        // Update data bảng bt
                        $res = $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
//                        $res = TRUE;
                        if ($res == TRUE) {
                            // Update data bảng quyền lợi bt
                            $data_insert = array(
                                'QLBT_name' => $quyenloi_bt,
                                'QLBT_KH_id' => $kh_id,
                                'QLBT_BT_id' => $bt_id,
                                'qlbt_st_ycbt' => $st_ycbt,
                                'qlbt_st_dbt' => $so_tien_dbt,
                                'qlbt_status' => $bt_status_update,
                                'date_created' => $datetime_now,
                                'date_updated' => $datetime_now,
                                'time_created' => $time_now,
                                'time_updated' => $time_now,
                            );
                            // Nếu bt có số ngày
                            if (isset($so_ngay_dbt) && $so_ngay_dbt != '') {
                                $data_insert['qlbt_sn_ycbt'] = $sn_ycbt;
                                $data_insert['qlbt_sn_dbt'] = $so_ngay_dbt;
                            }
                            //==================================================
//                            echo '<pre>';
//                            print_r($post_data);
//                            print_r($data_insert);
//                            die;
                            $res = $this->orders_model->CommonCreat(TBL_QLBT, $data_insert);
                        }
                        redirect(ORDERS_ADMIN_BASE_URL);
                    }
                }

                return FALSE;
            }
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //end
    //==========================================================================
    function do_edit_orders_giaiquyet_nhakhoa_mdl($options = array()) {
        $data = array();
        $a_qlbt_sn = get_a_loai_benh_sn();
        $a_qlbt_slk = get_a_loai_benh_slk();
        $a_quyenloi_bt_not_select = get_a_quyenloi_bt_not_select();
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $st_ycbt = $bt->so_tien_ycbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            //==================================================================
            $st_bh_gh_t = $bh->$quyenloi_bt;
            // end
            //==================================================================
            if ($quyenloi_bt != '') {
                $this->form_validation->set_rules('ma_benh', 'Mã bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('chuandoan_benh', 'Chuẩn đoán bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('lydo_khong_bt', 'Lý do không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_khong_bt', 'Ghi chú không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_xuly_bt', 'Ghi chú xử lý BT', 'trim|xss_clean');
                //==============================================================
                // Nếu có cvr
                $nkmdl_cvr = $this->input->post('nkmdl_cvr', TRUE);
                // Nếu chọn cvr
                if (isset($nkmdl_cvr) && $nkmdl_cvr > 0) {
                    $this->form_validation->set_rules('nkmdl_cvr_st_ycbt', 'Cạo vôi răng số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                    $this->form_validation->set_rules('nkmdl_cvr_st_dbt', 'Cạo vôi răng số tiền được bồi thường', 'trim|required|xss_clean');
                    //==========================================================
                    $nkmdl_cvr_st_ycbt = $this->input->post('nkmdl_cvr_st_ycbt', TRUE);
                    $nkmdl_cvr_st_dbt = $this->input->post('nkmdl_cvr_st_dbt', TRUE);
                } else {
                    $nkmdl_cvr_st_ycbt = $nkmdl_cvr_st_dbt = 0;
                }
                //==============================================================
                $this->form_validation->set_rules('quyenloi_bt_2', 'Quyền lợi bồi thường', 'trim|xss_clean');
                $quyenloi_bt_2 = $this->input->post('quyenloi_bt_2', TRUE);
                //==============================================================
//                echo $quyenloi_bt_2;
//                die;
                if ($quyenloi_bt_2 != '') {

                    $field_st_ycbt = $quyenloi_bt_2 . '_st_ycbt';
                    $field_st_dbt = $quyenloi_bt_2 . '_st_dbt';
                    $this->form_validation->set_rules($field_st_ycbt, 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                    $this->form_validation->set_rules($field_st_dbt, 'Số tiền được bồi thường', 'trim|required|xss_clean');
                    //==========================================================
                    $st_ycbt_2 = $this->input->post($field_st_ycbt, TRUE);
                    $st_dbt_2 = $this->input->post($field_st_dbt, TRUE);
                    //==========================================================
//                    echo $st_ycbt_2.'/'.$st_dbt_2;
//                    die;
                } else {
                    $st_ycbt_2 = $st_dbt_2 = 0;
                }
                // Xử lý
                if ($this->form_validation->run()) {
                    // Check tổng số tiền dbt
                    if ((isset($nkmdl_cvr_st_dbt) && $nkmdl_cvr_st_dbt > 0) || (isset($st_dbt_2) && $st_dbt_2 > 0)) {
                        $bt_status_update = BT_PHEDUYET;
                        $so_tien_dbt_t = (int) $st_dbt_2 + (int) $nkmdl_cvr_st_dbt;
                        //======================================================
                        // Nếu tổng st_dbt < st_bh_gh
//                        echo $so_tien_dbt_t.'/'.$st_bh_gh_t;
//                        die;
                        if ($so_tien_dbt_t <= $st_bh_gh_t) {
                            //==================================================
                            $post_data = array(
                                'user2' => $this->phpsession->get('user_id'),
                                'order_status' => $bt_status_update,
                                'so_tien_dbt' => $so_tien_dbt_t,
                                //'st_dbt_t' => $so_tien_dbt_t,
                                'OD_ma_benh' => $this->input->post('ma_benh', TRUE),
                                'chuandoan_benh' => $this->input->post('chuandoan_benh', TRUE),
                                'lydo_khong_bt' => $this->input->post('lydo_khong_bt', TRUE),
                                'ghichu_khong_bt' => $this->input->post('ghichu_khong_bt', TRUE),
                                'ghichu_xuly_bt' => $this->input->post('ghichu_xuly_bt', TRUE),
                                'editor' => $this->phpsession->get('user_id'),
                            );
                            //==================================================
                            $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                            $post_data['updated_time'] = strtotime(date('Y-m-d'));

//                            echo '<pre>';
//                            print_r($post_data);
//                            die;
                            // Update data bảng bt
                            $res = $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                            $res = TRUE;
                            if ($res == TRUE) {
                                // Nếu có ql cvr
                                if (isset($nkmdl_cvr) && $nkmdl_cvr > 0) {
                                    // Update data bảng quyền lợi bt cvr
                                    $data_insert_2 = array(
                                        'QLBT_name' => nkmdl_cvr,
                                        'QLBT_KH_id' => $kh_id,
                                        'QLBT_BT_id' => $bt_id,
                                        'qlbt_st_ycbt' => $nkmdl_cvr_st_ycbt,
                                        'qlbt_st_dbt' => $nkmdl_cvr_st_dbt,
                                        'qlbt_status' => $bt_status_update,
                                        'date_created' => date('Y-m-d H:i:s'),
                                        'date_updated' => date('Y-m-d H:i:s'),
                                        'time_created' => strtotime(date('Y-m-d')),
                                        'time_updated' => strtotime(date('Y-m-d')),
                                    );
                                    //==========================================
                                    $res = $this->orders_model->CommonCreat(TBL_QLBT, $data_insert_2);
                                }
                                //==============================================
                                // Nếu chọn qlbt
                                if ($st_ycbt_2 > 0 && $st_dbt_2 > 0) {
                                    // Update data bảng quyền lợi bt
                                    $data_insert = array(
                                        'QLBT_name' => $quyenloi_bt_2,
                                        'QLBT_KH_id' => $kh_id,
                                        'QLBT_BT_id' => $bt_id,
                                        'qlbt_st_ycbt' => $st_ycbt_2,
                                        'qlbt_st_dbt' => $st_dbt_2,
                                        'qlbt_status' => $bt_status_update,
                                        'date_created' => date('Y-m-d H:i:s'),
                                        'date_updated' => date('Y-m-d H:i:s'),
                                        'time_created' => strtotime(date('Y-m-d')),
                                        'time_updated' => strtotime(date('Y-m-d')),
                                    );
                                    $res = $this->orders_model->CommonCreat(TBL_QLBT, $data_insert);
                                    //==========================================
                                }
//                                echo '<pre>';
//                                print_r($post_data);
//                                print_r($data_insert);
//                                print_r($data_insert_2);
//                                die;
                            }
                        }
                    }
                    //==========================================================

                    redirect(ORDERS_ADMIN_BASE_URL);
                } else {
                    
                }
                return FALSE;
            }
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //end
    //==========================================================================
    function do_edit_orders_giaiquyet_thaisan_mdl($options = array()) {
        $data = array();
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $st_ycbt = $bt->so_tien_ycbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            //==================================================================
            $st_bh_gh_t = $bh->$quyenloi_bt;
            // end
            //==================================================================
            if ($quyenloi_bt != '') {

                $this->form_validation->set_rules('quyenloi_bt_2', 'Quyền lợi bồi thường', 'trim|required|xss_clean');
                $quyenloi_bt_2 = $this->input->post('quyenloi_bt_2', TRUE);
                //==============================================================
//                echo $quyenloi_bt_2;
//                die;
                $this->form_validation->set_rules('ma_benh', 'Mã bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('chuandoan_benh', 'Chuẩn đoán bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('lydo_khong_bt', 'Lý do không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_khong_bt', 'Ghi chú không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_xuly_bt', 'Ghi chú xử lý BT', 'trim|xss_clean');
                //==============================================================
                if ($quyenloi_bt_2 != '') {

                    $field_st_ycbt = $quyenloi_bt_2 . '_st_ycbt';
                    $field_st_dbt = $quyenloi_bt_2 . '_st_dbt';
                    $this->form_validation->set_rules($field_st_ycbt, 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                    $this->form_validation->set_rules($field_st_dbt, 'Số tiền được bồi thường', 'trim|required|xss_clean');
                    //==========================================================
                    $st_ycbt_2 = $this->input->post($field_st_ycbt, TRUE);
                    $st_dbt_2 = $this->input->post($field_st_dbt, TRUE);
                    //==========================================================
//                    echo $st_ycbt_2.'/'.$st_dbt_2;
//                    die;
                    // Nếu qlbt là st hay sm
                    if ($quyenloi_bt_2 == tsmdl_st || $quyenloi_bt_2 == tsmdl_sm) {
                        // Nếu có dưỡng nhi
                        $tsmdl_dn = $this->input->post('tsmdl_dn', TRUE);
                        if ($tsmdl_dn == TRUE) {
                            $this->form_validation->set_rules('tsmdl_dn_st_ycbt', 'Số tiền yêu cầu bồi thường dưỡng nhi', 'trim|required|xss_clean');
                            $this->form_validation->set_rules('tsmdl_dn_st_dbt', 'Số tiền được bồi thường dưỡng nhi', 'trim|required|xss_clean');
                            //==================================================
                            $tsmdl_dn_st_ycbt = $this->input->post('tsmdl_dn_st_ycbt', TRUE);
                            $tsmdl_dn_st_dbt = $this->input->post('tsmdl_dn_st_dbt', TRUE);
                            //==================================================
                        }
                        //======================================================
                    }
                    // Xử lý
                    if ($this->form_validation->run()) {
                        // Check tổng số tiền dbt
                        if ((isset($st_dbt_2) && $st_dbt_2 > 0)) {
                            $bt_status_update = BT_PHEDUYET;
                            $so_tien_dbt_t = (int) $st_dbt_2;
                            //==================================================
                            // Lấy st dbt tổng - Nếu có dưỡng nhi
                            if ((isset($tsmdl_dn_st_dbt) && $tsmdl_dn_st_dbt > 0)) {
                                $so_tien_dbt_t = (int) $st_dbt_2 + (int) $tsmdl_dn_st_dbt;
                            }
                            //==================================================
                            // Nếu tổng st_dbt < st_bh_gh
//                            echo $so_tien_dbt_t . '/' . $st_bh_gh_t;
//                            die;
                            if ($so_tien_dbt_t <= $st_bh_gh_t) { // Chưa check trường hợp đã có bồi thường rồi
                                //==============================================
                                $post_data = array(
                                    'user2' => $this->phpsession->get('user_id'),
                                    'order_status' => $bt_status_update,
                                    'so_tien_dbt' => $so_tien_dbt_t,
                                    //'st_dbt_t' => $so_tien_dbt_t,
                                    'OD_ma_benh' => $this->input->post('ma_benh', TRUE),
                                    'chuandoan_benh' => $this->input->post('chuandoan_benh', TRUE),
                                    'lydo_khong_bt' => $this->input->post('lydo_khong_bt', TRUE),
                                    'ghichu_khong_bt' => $this->input->post('ghichu_khong_bt', TRUE),
                                    'ghichu_xuly_bt' => $this->input->post('ghichu_xuly_bt', TRUE),
                                    'editor' => $this->phpsession->get('user_id'),
                                );

                                //==============================================
                                $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                                $post_data['updated_time'] = strtotime(date('Y-m-d'));

//                                echo '<pre>';
//                                print_r($post_data);
//                                die;
                                // Update data bảng bt
                                $res = $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
//                                $res = TRUE;
                                if ($res == TRUE) {
                                    // Nếu có ql dưỡng nhi
                                    if (isset($tsmdl_dn) && $tsmdl_dn > 0) {
                                        // Update data bảng quyền lợi bt dưỡng nhi
                                        $data_insert_2 = array(
                                            'QLBT_name' => tsmdl_dn,
                                            'QLBT_KH_id' => $kh_id,
                                            'QLBT_BT_id' => $bt_id,
                                            'qlbt_st_ycbt' => $tsmdl_dn_st_ycbt,
                                            'qlbt_st_dbt' => $tsmdl_dn_st_dbt,
                                            'qlbt_status' => $bt_status_update,
                                            'date_created' => date('Y-m-d H:i:s'),
                                            'date_updated' => date('Y-m-d H:i:s'),
                                            'time_created' => strtotime(date('Y-m-d')),
                                            'time_updated' => strtotime(date('Y-m-d')),
                                        );
                                        //======================================
                                        $res = $this->orders_model->CommonCreat(TBL_QLBT, $data_insert_2);
                                    }
                                    //==========================================
                                    // Nếu chọn qlbt
                                    if ($st_ycbt_2 > 0 && $st_dbt_2 > 0) {
                                        // Update data bảng quyền lợi bt
                                        $data_insert = array(
                                            'QLBT_name' => $quyenloi_bt_2,
                                            'QLBT_KH_id' => $kh_id,
                                            'QLBT_BT_id' => $bt_id,
                                            'qlbt_st_ycbt' => $st_ycbt_2,
                                            'qlbt_st_dbt' => $st_dbt_2,
                                            'qlbt_status' => $bt_status_update,
                                            'date_created' => date('Y-m-d H:i:s'),
                                            'date_updated' => date('Y-m-d H:i:s'),
                                            'time_created' => strtotime(date('Y-m-d')),
                                            'time_updated' => strtotime(date('Y-m-d')),
                                        );
                                        $res = $this->orders_model->CommonCreat(TBL_QLBT, $data_insert);
                                        //======================================
                                    }
//                                echo '<pre>';
//                                print_r($post_data);
//                                print_r($data_insert);
//                                print_r($data_insert_2);
//                                die;
                                }
                            }
                        }
                        //======================================================
                        redirect(ORDERS_ADMIN_BASE_URL);
                    }
                    //==========================================================
                } else {
                    redirect(ORDERS_ADMIN_BASE_URL);
                }
                return FALSE;
            }
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //end
    //==========================================================================
    function do_edit_orders_giaiquyet_dtngt_ob($options = array()) {
        $data = array();
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $st_ycbt = $bt->so_tien_ycbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            //==================================================================
            $dtngt_st1lk = $bh->dtngt_st1lk;
            $dtngt_nkngt = $bh->dtngt_nkngt;
            $dtngt_nk = $bh->dtngt_nk;
            $dtngt_cvr = $bh->dtngt_cvr;
            //==================================================================
            $st_bh_gh_t = $bh->$quyenloi_bt;
            // end
            //==================================================================
            if ($quyenloi_bt != '') {

                $this->form_validation->set_rules('quyenloi_bt_2', 'Quyền lợi bồi thường', 'trim|required|xss_clean');
                $quyenloi_bt_2 = $this->input->post('quyenloi_bt_2', TRUE);
                //==============================================================
//                echo $quyenloi_bt_2;
//                die;
                $this->form_validation->set_rules('ma_benh', 'Mã bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('chuandoan_benh', 'Chuẩn đoán bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('lydo_khong_bt', 'Lý do không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_khong_bt', 'Ghi chú không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_xuly_bt', 'Ghi chú xử lý BT', 'trim|xss_clean');
                //==============================================================
                if ($quyenloi_bt_2 != '') {
                    // Nếu qlbt là khám ngoại trú
                    if ($quyenloi_bt_2 == dtngt_st1lk) {
                        $field_st_ycbt = $quyenloi_bt_2 . '_st_ycbt';
                        $field_st_dbt = $quyenloi_bt_2 . '_st_dbt';
                        $this->form_validation->set_rules($field_st_ycbt, 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules($field_st_dbt, 'Số tiền được bồi thường', 'trim|required|xss_clean');
                        //======================================================
                        $st_ycbt_2 = $this->input->post($field_st_ycbt, TRUE);
                        $st_dbt_2 = $this->input->post($field_st_dbt, TRUE);
                    } else {
                        // Nếu qlbt là nha khoa trong ngoại trú
                        $this->form_validation->set_rules('quyenloi_bt_3', 'Quyền lợi bồi thường nha khoa trong ngoại trú', 'trim|required|xss_clean');
                        $quyenloi_bt_3 = $this->input->post('quyenloi_bt_3', TRUE);
                        //======================================================
                        $field_st_ycbt_3 = $quyenloi_bt_3 . '_st_ycbt';
                        $field_st_dbt_3 = $quyenloi_bt_3 . '_st_dbt';
                        $this->form_validation->set_rules($field_st_ycbt_3, 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules($field_st_dbt_3, 'Số tiền được bồi thường', 'trim|required|xss_clean');
                        //======================================================
                        $st_ycbt_3 = $this->input->post($field_st_ycbt_3, TRUE);
                        $st_dbt_3 = $this->input->post($field_st_dbt_3, TRUE);
                    }
                    //==========================================================
//                    echo $st_ycbt_3.'/'.$st_dbt_3;
//                    die;
                    // Xử lý
                    if ($this->form_validation->run()) {
                        // Check tổng số tiền dbt
                        if ((isset($st_dbt_2) && $st_dbt_2 > 0) || (isset($st_dbt_3) && $st_dbt_3 > 0)) {
                            $bt_status_update = BT_PHEDUYET;
                            // Nếu qlbt là khám ngoại trú
                            if (isset($st_dbt_2) && $st_dbt_2 > 0) {
                                $so_tien_dbt_t = (int) $st_dbt_2;
                                //==============================================
                                if ($so_tien_dbt_t <= $st_bh_gh_t && $so_tien_dbt_t <= $dtngt_st1lk) { // Chưa check trường hợp đã có bồi thường rồi và đã có số lần khám
                                    // Update data bảng quyền lợi bt
                                    $data_insert = array(
                                        'QLBT_name' => $quyenloi_bt_2,
                                        'QLBT_KH_id' => $kh_id,
                                        'QLBT_BT_id' => $bt_id,
                                        'qlbt_st_ycbt' => $st_ycbt_2,
                                        'qlbt_st_dbt' => $st_dbt_2,
                                        'qlbt_status' => $bt_status_update,
                                        'date_created' => date('Y-m-d H:i:s'),
                                        'date_updated' => date('Y-m-d H:i:s'),
                                        'time_created' => strtotime(date('Y-m-d')),
                                        'time_updated' => strtotime(date('Y-m-d')),
                                    );
                                    //==========================================
                                }
                            }
                            //==================================================
                            // Nếu qlbt là nha khoa ngoại trú
                            if (isset($st_dbt_3) && $st_dbt_3 > 0) {
                                $so_tien_dbt_t = (int) $st_dbt_3;
                                //==============================================
                                // Số tiền bh gh của qlbt được chọn
                                $st_bt_gh_qlbt_3 = $bh->$quyenloi_bt_3;
                                //==============================================
//                                echo $so_tien_dbt_t.'/'.$st_bh_gh_t;
//                                die;
                                if ($so_tien_dbt_t <= $st_bh_gh_t) { // Chưa check trường hợp đã có bồi thường rồi và đã có số lần khám
                                    // Update data bảng quyền lợi bt
                                    $data_insert = array(
                                        'QLBT_name' => $quyenloi_bt_3,
                                        'QLBT_KH_id' => $kh_id,
                                        'QLBT_BT_id' => $bt_id,
                                        'qlbt_st_ycbt' => $st_ycbt_3,
                                        'qlbt_st_dbt' => $st_dbt_3,
                                        'qlbt_status' => $bt_status_update,
                                        'date_created' => date('Y-m-d H:i:s'),
                                        'date_updated' => date('Y-m-d H:i:s'),
                                        'time_created' => strtotime(date('Y-m-d')),
                                        'time_updated' => strtotime(date('Y-m-d')),
                                    );
                                    //==========================================
                                }
                            }
                            //==================================================
                            if (isset($data_insert) && !empty($data_insert)) {
                                // Thêm data qlbt
                                $res = $this->orders_model->CommonCreat(TBL_QLBT, $data_insert);
                                //==============================================
                                $post_data = array(
                                    'user2' => $this->phpsession->get('user_id'),
                                    'order_status' => $bt_status_update,
                                    'so_tien_dbt' => $so_tien_dbt_t,
                                    //'st_dbt_t' => $so_tien_dbt_t,
                                    'OD_ma_benh' => $this->input->post('ma_benh', TRUE),
                                    'chuandoan_benh' => $this->input->post('chuandoan_benh', TRUE),
                                    'lydo_khong_bt' => $this->input->post('lydo_khong_bt', TRUE),
                                    'ghichu_khong_bt' => $this->input->post('ghichu_khong_bt', TRUE),
                                    'ghichu_xuly_bt' => $this->input->post('ghichu_xuly_bt', TRUE),
                                    'editor' => $this->phpsession->get('user_id'),
                                );
                                //==============================================
                                $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                                $post_data['updated_time'] = strtotime(date('Y-m-d'));

//                                echo '<pre>';
//                                print_r($data_insert);
//                                print_r($post_data);
//                                die;
                                // Update data bảng bt
                                $res = $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                            }
                        } else {
                            redirect(ORDERS_ADMIN_BASE_URL);
                        }
                        //======================================================
                        redirect(ORDERS_ADMIN_BASE_URL);
                    }
                    //==========================================================
                } else {
                    redirect(ORDERS_ADMIN_BASE_URL);
                }
                return FALSE;
            }
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //end
    //==========================================================================
    function do_edit_orders_giaiquyet_dtnt_ob($options = array()) {
        $data = array();
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $st_ycbt = $bt->so_tien_ycbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            //==================================================================
            $st_bh_gh_t = $bh->$quyenloi_bt;
            // end
            //==================================================================
            if ($quyenloi_bt != '') {

                $this->form_validation->set_rules('dtnt_tvpn', 'Tiền viện phí/năm', 'trim|required|xss_clean');
                $dtnt_tvpn = $this->input->post('dtnt_tvpn', TRUE);
                //==============================================================
//                echo $dtnt_tvpn;
//                die;
                $this->form_validation->set_rules('ma_benh', 'Mã bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('chuandoan_benh', 'Chuẩn đoán bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('lydo_khong_bt', 'Lý do không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_khong_bt', 'Ghi chú không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_xuly_bt', 'Ghi chú xử lý BT', 'trim|xss_clean');
                //==============================================================
                if ($dtnt_tvpn > 0) {
                    // Nếu qlbt là Tiền viện phí hoặc Chi phí phẫu thuật
                    if ($dtnt_tvpn == TRUE) {
                        $this->form_validation->set_rules(dtnt_tvpn . '_st_ycbt', 'Tiền viện phí sô tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tvpn . '_st_dbt', 'Tiền viện phí Số tiền được bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tvpn . '_sn_ycbt', 'Tiền viện phí sô ngày yêu cầu bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tvpn . '_sn_dbt', 'Tiền viện phí Số ngày được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    // Nếu qlbt có tiền giường phòng ngày
                    $dtnt_tgpn = $this->input->post('dtnt_tgpn');
                    if ($dtnt_tgpn == TRUE) {
                        $this->form_validation->set_rules(dtnt_tgpn . '_st_ycbt', 'Tiền giường,phòng/ngày', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tgpn . '_st_dbt', 'Tiền giường,phòng/ngày Số tiền được bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tgpn . '_sn_ycbt', 'Tiền giường,phòng/ngày', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tgpn . '_sn_dbt', 'Tiền giường,phòng/ngày Số ngày được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có chi phí phẫu thuật
                    $dtnt_cppt = $this->input->post('dtnt_cppt');
                    if ($dtnt_cppt == TRUE) {
                        $this->form_validation->set_rules(dtnt_cppt . '_st_ycbt', 'Chi phí phẫu thuật', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_cppt . '_st_dbt', 'Chi phí phẫu thuật Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có Quyền lợi khám trước khi nhập viện
                    $dtnt_tk_nv = $this->input->post('dtnt_tk_nv');
                    if ($dtnt_tk_nv == TRUE) {
                        $this->form_validation->set_rules(dtnt_tk_nv . '_st_ycbt', 'Quyền lợi khám trước khi nhập viện', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tk_nv . '_st_dbt', 'Quyền lợi khám trước khi nhập viện Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có Quyền lợi khám sau khi nhập viện
                    $dtnt_sk_nv = $this->input->post('dtnt_sk_nv');
                    if ($dtnt_sk_nv == TRUE) {
                        $this->form_validation->set_rules(dtnt_sk_nv . '_st_ycbt', 'Quyền lợi khám sau khi nhập viện', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_sk_nv . '_st_dbt', 'Quyền lợi khám sau khi nhập viện Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có Chi phí y tế tại nhà
                    $dtnt_cpty_tn = $this->input->post('dtnt_cpty_tn');
                    if ($dtnt_cpty_tn == TRUE) {
                        $this->form_validation->set_rules(dtnt_cpty_tn . '_st_ycbt', 'Chi phí y tế tại nhà', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_cpty_tn . '_st_dbt', 'Chi phí y tế tại nhà Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Trợ cấp viện phí
                    $dtnt_tcvp = $this->input->post('dtnt_tcvp');
                    if ($dtnt_tcvp == TRUE) {
                        $this->form_validation->set_rules(dtnt_tcvp . '_st_ycbt', 'Trợ cấp viện phí', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tcvp . '_st_dbt', 'Trợ cấp viện phí Số tiền được bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tcvp . '_sn_ycbt', 'Trợ cấp viện phí', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tcvp . '_sn_dbt', 'Trợ cấp viện phí Số ngày được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có Trợ cấp mai táng
                    $dtnt_tcmt = $this->input->post('dtnt_tcmt');
                    if ($dtnt_tcmt == TRUE) {
                        $this->form_validation->set_rules(dtnt_tcmt . '_st_ycbt', 'Trợ cấp mai táng', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tcmt . '_st_dbt', 'Trợ cấp mai táng Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có Xe cứu thương
                    $dtnt_xct = $this->input->post('dtnt_xct');
                    if ($dtnt_xct == TRUE) {
                        $this->form_validation->set_rules(dtnt_xct . '_st_ycbt', 'Xe cứu thương', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_xct . '_st_dbt', 'Xe cứu thương Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Xử lý
                    if ($this->form_validation->run($this)) {
                        // Nếu còn qlbt tiền viện phí
                        if ($dtnt_tvpn == TRUE) {
                            $dtnt_tvpn_st_ycbt = $this->input->post(dtnt_tvpn . '_st_ycbt');
                            $dtnt_tvpn_st_dbt = $this->input->post(dtnt_tvpn . '_st_dbt');
                            $dtnt_tvpn_sn_ycbt = $this->input->post(dtnt_tvpn . '_sn_ycbt');
                            $dtnt_tvpn_sn_dbt = $this->input->post(dtnt_tvpn . '_sn_dbt');
                            //==================================================
                        } else {
                            $dtnt_tvpn_st_dbt = $dtnt_tvpn_sn_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt tiền giường phòng ngày
                        if ($dtnt_tgpn == TRUE) {
                            $dtnt_tgpn_st_ycbt = $this->input->post(dtnt_tgpn . '_st_ycbt');
                            $dtnt_tgpn_st_dbt = $this->input->post(dtnt_tgpn . '_st_dbt');
                            $dtnt_tgpn_sn_ycbt = $this->input->post(dtnt_tgpn . '_sn_ycbt');
                            $dtnt_tgpn_sn_dbt = $this->input->post(dtnt_tgpn . '_sn_dbt');
                        } else {
                            $dtnt_tgpn_st_dbt = $dtnt_tgpn_sn_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Chi phí phẫu thuật
                        if ($dtnt_cppt == TRUE) {
                            $dtnt_cppt_st_ycbt = $this->input->post(dtnt_cppt . '_st_ycbt');
                            $dtnt_cppt_st_dbt = $this->input->post(dtnt_cppt . '_st_dbt');
                        } else {
                            $dtnt_cppt_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Quyền lợi khám trước khi nhập viện
                        if ($dtnt_tk_nv == TRUE) {
                            $dtnt_tk_nv_st_ycbt = $this->input->post(dtnt_tk_nv . '_st_ycbt');
                            $dtnt_tk_nv_st_dbt = $this->input->post(dtnt_tk_nv . '_st_dbt');
                        } else {
                            $dtnt_tk_nv_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Quyền lợi khám sau khi nhập viện
                        if ($dtnt_sk_nv == TRUE) {
                            $dtnt_sk_nv_st_ycbt = $this->input->post(dtnt_sk_nv . '_st_ycbt');
                            $dtnt_sk_nv_st_dbt = $this->input->post(dtnt_sk_nv . '_st_dbt');
                        } else {
                            $dtnt_sk_nv_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Chi phí y tế tại nhà
                        if ($dtnt_cpty_tn == TRUE) {
                            $dtnt_cpty_tn_st_ycbt = $this->input->post(dtnt_cpty_tn . '_st_ycbt');
                            $dtnt_cpty_tn_st_dbt = $this->input->post(dtnt_cpty_tn . '_st_dbt');
                        } else {
                            $dtnt_cpty_tn_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Trợ cấp viện phí
                        if ($dtnt_tcvp == TRUE) {
                            $dtnt_tcvp_st_ycbt = $this->input->post(dtnt_tcvp . '_st_ycbt');
                            $dtnt_tcvp_st_dbt = $this->input->post(dtnt_tcvp . '_st_dbt');
                            $dtnt_tcvp_sn_ycbt = $this->input->post(dtnt_tcvp . '_sn_ycbt');
                            $dtnt_tcvp_sn_dbt = $this->input->post(dtnt_tcvp . '_sn_dbt');
                        } else {
                            $dtnt_tcvp_st_dbt = $dtnt_tcvp_sn_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Trợ cấp mai táng
                        if ($dtnt_tcmt == TRUE) {
                            $dtnt_tcmt_st_ycbt = $this->input->post(dtnt_tcmt . '_st_ycbt');
                            $dtnt_tcmt_st_dbt = $this->input->post(dtnt_tcmt . '_st_dbt');
                        } else {
                            $dtnt_tcmt_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Xe cứu thương
                        if ($dtnt_xct == TRUE) {
                            $dtnt_xct_st_ycbt = $this->input->post(dtnt_xct . '_st_ycbt');
                            $dtnt_xct_st_dbt = $this->input->post(dtnt_xct . '_st_dbt');
                        } else {
                            $dtnt_xct_st_dbt = 0;
                        }
                        //======================================================
                        // Check số tiền còn lại tổng DTNT
                        $data_get = array(
                            'CL_kh_id' => $kh_id,
                            'CL_quyenloi_bt' => DTNT_OB,
                            'get_row' => TRUE,
                        );
                        $ql_dbt_t = $this->orders_model->get_ql_cl($data_get);
                        if (is_object($ql_dbt_t)) {
                            $st_bh_gh_t = (int) $ql_dbt_t->CL_st_bh_cl;
                        }
                        //======================================================
                        // Nếu tổng số tiền bồi thường nhỏ hơn số tiền còn lại
                        $so_tien_dbt_t = $dtnt_tvpn_st_dbt + $dtnt_tgpn_st_dbt + $dtnt_cppt_st_dbt + $dtnt_tk_nv_st_dbt + $dtnt_sk_nv_st_dbt +
                                $dtnt_cpty_tn_st_dbt + $dtnt_tcvp_st_dbt + $dtnt_tcmt_st_dbt + $dtnt_xct_st_dbt;
                        if ((int) $so_tien_dbt_t < $st_bh_gh_t) {
                            $bt_status_update = BT_PHEDUYET;
                            //==================================================
                            // Tiền viện phí/năm
                            if ($dtnt_tvpn_st_dbt > 0 && $dtnt_tvpn_sn_dbt > 0) {
                                $data_tvpn = array(
                                    'QLBT_name' => dtnt_tvpn,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_tvpn_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_tvpn_st_dbt,
                                    'qlbt_sn_ycbt' => $dtnt_tvpn_sn_ycbt,
                                    'qlbt_sn_dbt' => $dtnt_tvpn_sn_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
                                //$this->orders_model->CommonCreat(TBL_QLBT, $data_tvpn);
                            }
                            //==================================================
                            // Tiền giường,phòng/ngày
                            if ($dtnt_tgpn_st_dbt > 0 && $dtnt_tgpn_sn_dbt > 0) {
                                $data_tgpn = array(
                                    'QLBT_name' => dtnt_tgpn,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_tgpn_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_tgpn_st_dbt,
                                    'qlbt_sn_ycbt' => $dtnt_tgpn_sn_ycbt,
                                    'qlbt_sn_dbt' => $dtnt_tgpn_sn_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tgpn);
                            }
                            //==================================================
                            // Chi phí phẫu thuật
                            if ($dtnt_cppt_st_dbt > 0) {
                                $data_cppt = array(
                                    'QLBT_name' => dtnt_cppt,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_cppt_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_cppt_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_cppt);
                            }
                            //==================================================
                            // Quyền lợi khám trước khi nhập viện
                            if ($dtnt_tk_nv_st_dbt > 0) {
                                $data_tk_nv = array(
                                    'QLBT_name' => dtnt_tk_nv,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_tk_nv_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_tk_nv_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tk_nv);
                            }
                            //==================================================
                            // Quyền lợi khám sau khi nhập viện
                            if ($dtnt_sk_nv_st_dbt > 0) {
                                $data_sk_nv = array(
                                    'QLBT_name' => dtnt_sk_nv,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_sk_nv_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_sk_nv_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_sk_nv);
                            }
                            //==================================================
                            // Chi phí y tế tại nhà
                            if ($dtnt_cpyt_tn_st_dbt > 0) {
                                $data_cpyt_tn = array(
                                    'QLBT_name' => dtnt_cpyt_tn,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_cpyt_tn_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_cpyt_tn_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_cpyt_tn);
                            }
                            //==================================================
                            // Trợ cấp viện phí
                            if ($dtnt_tcvp_st_dbt > 0 && $dtnt_tcvp_sn_dbt > 0) {
                                $data_tcvp = array(
                                    'QLBT_name' => dtnt_tcvp,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_tcvp_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_tcvp_st_dbt,
                                    'qlbt_sn_ycbt' => $dtnt_tcvp_sn_ycbt,
                                    'qlbt_sn_dbt' => $dtnt_tcvp_sn_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tcvp);
                            }
                            //==================================================
                            // Trợ cấp mai táng
                            if ($dtnt_tcmt_st_dbt > 0) {
                                $data_tcmt = array(
                                    'QLBT_name' => dtnt_tcmt,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_tcmt_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_tcmt_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tcmt);
                            }
                            //==================================================
                            // Xe cứu thương
                            if ($dtnt_xct_st_dbt > 0) {
                                $data_xct = array(
                                    'QLBT_name' => dtnt_xct,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_xct_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_xct_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_xct);
                            }
                            //==================================================
                            // Update bồi thường
                            $post_data = array(
                                'user2' => $this->phpsession->get('user_id'),
                                'order_status' => $bt_status_update,
                                'so_tien_dbt' => $so_tien_dbt_t,
                                //'st_dbt_t' => $so_tien_dbt_t,
                                'OD_ma_benh' => $this->input->post('ma_benh', TRUE),
                                'chuandoan_benh' => $this->input->post('chuandoan_benh', TRUE),
                                'lydo_khong_bt' => $this->input->post('lydo_khong_bt', TRUE),
                                'ghichu_khong_bt' => $this->input->post('ghichu_khong_bt', TRUE),
                                'ghichu_xuly_bt' => $this->input->post('ghichu_xuly_bt', TRUE),
                                'editor' => $this->phpsession->get('user_id'),
                            );
                            //==============================================
                            $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                            $post_data['updated_time'] = strtotime(date('Y-m-d'));

//                            echo '<pre>';
//                            print_r($data_tvpn);
//                            print_r($data_tgpn);
//                            print_r($data_cppt);
//                            print_r($data_tk_nv);
//                            print_r($data_sk_nv);
//                            print_r($data_cpyt_tn);
//                            print_r($data_tcvp);
//                            print_r($data_tcmt);
//                            print_r($data_xct);
//                            print_r($post_data);
//                            die;
                            if (isset($data_tvpn) && !empty($data_tvpn)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tvpn);
                            }
                            if (isset($data_tgpn) && !empty($data_tgpn)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tgpn);
                            }
                            if (isset($data_cppt) && !empty($data_cppt)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_cppt);
                            }
                            if (isset($data_tk_nv) && !empty($data_tk_nv)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tk_nv);
                            }
                            if (isset($data_sk_nv) && !empty($data_sk_nv)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_sk_nv);
                            }
                            if (isset($data_cpyt_tn) && !empty($data_cpyt_tn)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_cpyt_tn);
                            }
                            if (isset($data_tcvp) && !empty($data_tcvp)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tcvp);
                            }
                            if (isset($data_tcmt) && !empty($data_tcmt)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tcmt);
                            }
                            if (isset($data_xct) && !empty($data_xct)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_xct);
                            }

                            // Update data bảng bt
                            $res = $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                        }
                        //======================================================
                        redirect(ORDERS_ADMIN_BASE_URL);
                    }
                    //==========================================================
                } else {
                    redirect(ORDERS_ADMIN_BASE_URL);
                }
                return FALSE;
            }
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //end
    //==========================================================================
    function do_edit_orders_giaiquyet_tsqlnt($options = array()) {
        $data = array();
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $st_ycbt = $bt->so_tien_ycbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            $phanloai_thaisan = $bt->phanloai_thaisan;
            //==================================================================
            // Check phân loại thai sản và st còn lại của ql sinh thường hoặc sinh mổ
            $loai_thaisan = get_field_thaisan($phanloai_thaisan);
            $st_tsqlnt_ss = $bh->$loai_thaisan;
            //==================================================================
            // Số tiền bh gh tổng THAISAN QLNT
            $st_tsqlnt_bh_gh = $bh->$quyenloi_bt;
            //==================================================================
            // Số tiền bh gh tổng (DTNT)
            $filed_dtnt = DTNT_OB;
            $st_dtnt_bh_gh = $bh->$filed_dtnt;
            //==================================================================
            //==================================================================
            if ($quyenloi_bt != '') {

                $this->form_validation->set_rules('dtnt_tvpn', 'Tiền viện phí/năm', 'trim|required|xss_clean');
                $dtnt_tvpn = $this->input->post('dtnt_tvpn', TRUE);
                //==============================================================
//                echo $dtnt_tvpn;
//                die;
                $this->form_validation->set_rules('ma_benh', 'Mã bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('chuandoan_benh', 'Chuẩn đoán bệnh', 'trim|required|xss_clean');
                $this->form_validation->set_rules('lydo_khong_bt', 'Lý do không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_khong_bt', 'Ghi chú không bồi thường', 'trim|xss_clean');
                $this->form_validation->set_rules('ghichu_xuly_bt', 'Ghi chú xử lý BT', 'trim|xss_clean');
                //==============================================================
                if ($dtnt_tvpn > 0) {
                    // Nếu qlbt là Tiền viện phí hoặc Chi phí phẫu thuật
                    if ($dtnt_tvpn == TRUE) {
                        $this->form_validation->set_rules(dtnt_tvpn . '_st_ycbt', 'Tiền viện phí sô tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tvpn . '_st_dbt', 'Tiền viện phí Số tiền được bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tvpn . '_sn_ycbt', 'Tiền viện phí sô ngày yêu cầu bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tvpn . '_sn_dbt', 'Tiền viện phí Số ngày được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    // Nếu qlbt có tiền giường phòng ngày
                    $dtnt_tgpn = $this->input->post('dtnt_tgpn');
                    if ($dtnt_tgpn == TRUE) {
                        $this->form_validation->set_rules(dtnt_tgpn . '_st_ycbt', 'Tiền giường,phòng/ngày', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tgpn . '_st_dbt', 'Tiền giường,phòng/ngày Số tiền được bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tgpn . '_sn_ycbt', 'Tiền giường,phòng/ngày', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tgpn . '_sn_dbt', 'Tiền giường,phòng/ngày Số ngày được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có chi phí phẫu thuật
                    $dtnt_cppt = $this->input->post('dtnt_cppt');
                    if ($dtnt_cppt == TRUE) {
                        $this->form_validation->set_rules(dtnt_cppt . '_st_ycbt', 'Chi phí phẫu thuật', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_cppt . '_st_dbt', 'Chi phí phẫu thuật Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có Quyền lợi khám trước khi nhập viện
                    $dtnt_tk_nv = $this->input->post('dtnt_tk_nv');
                    if ($dtnt_tk_nv == TRUE) {
                        $this->form_validation->set_rules(dtnt_tk_nv . '_st_ycbt', 'Quyền lợi khám trước khi nhập viện', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tk_nv . '_st_dbt', 'Quyền lợi khám trước khi nhập viện Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có Quyền lợi khám sau khi nhập viện
                    $dtnt_sk_nv = $this->input->post('dtnt_sk_nv');
                    if ($dtnt_sk_nv == TRUE) {
                        $this->form_validation->set_rules(dtnt_sk_nv . '_st_ycbt', 'Quyền lợi khám sau khi nhập viện', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_sk_nv . '_st_dbt', 'Quyền lợi khám sau khi nhập viện Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có Chi phí y tế tại nhà
                    $dtnt_cpty_tn = $this->input->post('dtnt_cpty_tn');
                    if ($dtnt_cpty_tn == TRUE) {
                        $this->form_validation->set_rules(dtnt_cpty_tn . '_st_ycbt', 'Chi phí y tế tại nhà', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_cpty_tn . '_st_dbt', 'Chi phí y tế tại nhà Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Trợ cấp viện phí
                    $dtnt_tcvp = $this->input->post('dtnt_tcvp');
                    if ($dtnt_tcvp == TRUE) {
                        $this->form_validation->set_rules(dtnt_tcvp . '_st_ycbt', 'Trợ cấp viện phí', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tcvp . '_st_dbt', 'Trợ cấp viện phí Số tiền được bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tcvp . '_sn_ycbt', 'Trợ cấp viện phí', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tcvp . '_sn_dbt', 'Trợ cấp viện phí Số ngày được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có Trợ cấp mai táng
                    $dtnt_tcmt = $this->input->post('dtnt_tcmt');
                    if ($dtnt_tcmt == TRUE) {
                        $this->form_validation->set_rules(dtnt_tcmt . '_st_ycbt', 'Trợ cấp mai táng', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_tcmt . '_st_dbt', 'Trợ cấp mai táng Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có Xe cứu thương
                    $dtnt_xct = $this->input->post('dtnt_xct');
                    if ($dtnt_xct == TRUE) {
                        $this->form_validation->set_rules(dtnt_xct . '_st_ycbt', 'Xe cứu thương', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(dtnt_xct . '_st_dbt', 'Xe cứu thương Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có khám thai định kỳ
                    $tsqlnt_ktdk = $this->input->post('tsqlnt_ktdk');
                    if ($tsqlnt_ktdk == TRUE) {
                        $this->form_validation->set_rules(tsqlnt_ktdk . '_st_ycbt', 'Khám thai định kỳ Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(tsqlnt_ktdk . '_st_dbt', 'Khám thai định kỳ Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Nếu qlbt có dưỡng nhi
                    $tsqlnt_dn = $this->input->post('tsqlnt_dn');
                    if ($tsqlnt_dn == TRUE) {
                        $this->form_validation->set_rules(tsqlnt_dn . '_st_ycbt', 'Dưỡng nhi Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                        $this->form_validation->set_rules(tsqlnt_dn . '_st_dbt', 'Dưỡng nhi Số tiền được bồi thường', 'trim|required|xss_clean');
                        //==================================================
                    }
                    //======================================================
                    // Xử lý
                    if ($this->form_validation->run($this)) {
                        // Nếu còn qlbt tiền viện phí
                        if ($dtnt_tvpn == TRUE) {
                            $dtnt_tvpn_st_ycbt = $this->input->post(dtnt_tvpn . '_st_ycbt');
                            $dtnt_tvpn_st_dbt = $this->input->post(dtnt_tvpn . '_st_dbt');
                            $dtnt_tvpn_sn_ycbt = $this->input->post(dtnt_tvpn . '_sn_ycbt');
                            $dtnt_tvpn_sn_dbt = $this->input->post(dtnt_tvpn . '_sn_dbt');
                            //==================================================
                        } else {
                            $dtnt_tvpn_st_dbt = $dtnt_tvpn_sn_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt tiền giường phòng ngày
                        if ($dtnt_tgpn == TRUE) {
                            $dtnt_tgpn_st_ycbt = $this->input->post(dtnt_tgpn . '_st_ycbt');
                            $dtnt_tgpn_st_dbt = $this->input->post(dtnt_tgpn . '_st_dbt');
                            $dtnt_tgpn_sn_ycbt = $this->input->post(dtnt_tgpn . '_sn_ycbt');
                            $dtnt_tgpn_sn_dbt = $this->input->post(dtnt_tgpn . '_sn_dbt');
                        } else {
                            $dtnt_tgpn_st_dbt = $dtnt_tgpn_sn_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Chi phí phẫu thuật
                        if ($dtnt_cppt == TRUE) {
                            $dtnt_cppt_st_ycbt = $this->input->post(dtnt_cppt . '_st_ycbt');
                            $dtnt_cppt_st_dbt = $this->input->post(dtnt_cppt . '_st_dbt');
                        } else {
                            $dtnt_cppt_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Quyền lợi khám trước khi nhập viện
                        if ($dtnt_tk_nv == TRUE) {
                            $dtnt_tk_nv_st_ycbt = $this->input->post(dtnt_tk_nv . '_st_ycbt');
                            $dtnt_tk_nv_st_dbt = $this->input->post(dtnt_tk_nv . '_st_dbt');
                        } else {
                            $dtnt_tk_nv_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Quyền lợi khám sau khi nhập viện
                        if ($dtnt_sk_nv == TRUE) {
                            $dtnt_sk_nv_st_ycbt = $this->input->post(dtnt_sk_nv . '_st_ycbt');
                            $dtnt_sk_nv_st_dbt = $this->input->post(dtnt_sk_nv . '_st_dbt');
                        } else {
                            $dtnt_sk_nv_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Chi phí y tế tại nhà
                        if ($dtnt_cpty_tn == TRUE) {
                            $dtnt_cpty_tn_st_ycbt = $this->input->post(dtnt_cpty_tn . '_st_ycbt');
                            $dtnt_cpty_tn_st_dbt = $this->input->post(dtnt_cpty_tn . '_st_dbt');
                        } else {
                            $dtnt_cpty_tn_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Trợ cấp viện phí
                        if ($dtnt_tcvp == TRUE) {
                            $dtnt_tcvp_st_ycbt = $this->input->post(dtnt_tcvp . '_st_ycbt');
                            $dtnt_tcvp_st_dbt = $this->input->post(dtnt_tcvp . '_st_dbt');
                            $dtnt_tcvp_sn_ycbt = $this->input->post(dtnt_tcvp . '_sn_ycbt');
                            $dtnt_tcvp_sn_dbt = $this->input->post(dtnt_tcvp . '_sn_dbt');
                        } else {
                            $dtnt_tcvp_st_dbt = $dtnt_tcvp_sn_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Trợ cấp mai táng
                        if ($dtnt_tcmt == TRUE) {
                            $dtnt_tcmt_st_ycbt = $this->input->post(dtnt_tcmt . '_st_ycbt');
                            $dtnt_tcmt_st_dbt = $this->input->post(dtnt_tcmt . '_st_dbt');
                        } else {
                            $dtnt_tcmt_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt Xe cứu thương
                        if ($dtnt_xct == TRUE) {
                            $dtnt_xct_st_ycbt = $this->input->post(dtnt_xct . '_st_ycbt');
                            $dtnt_xct_st_dbt = $this->input->post(dtnt_xct . '_st_dbt');
                        } else {
                            $dtnt_xct_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt khám thai định kỳ
                        if ($tsqlnt_ktdk == TRUE) {
                            $tsqlnt_ktdk_st_ycbt = $this->input->post(tsqlnt_ktdk . '_st_ycbt');
                            $tsqlnt_ktdk_st_dbt = $this->input->post(tsqlnt_ktdk . '_st_dbt');
                        } else {
                            $tsqlnt_ktdk_st_dbt = 0;
                        }
                        //======================================================
                        // Nếu còn qlbt dưỡng nhi
                        if ($tsqlnt_dn == TRUE) {
                            $tsqlnt_dn_st_ycbt = $this->input->post(tsqlnt_dn . '_st_ycbt');
                            $tsqlnt_dn_st_dbt = $this->input->post(tsqlnt_dn . '_st_dbt');
                        } else {
                            $tsqlnt_dn_st_dbt = 0;
                        }
                        //======================================================
                        // Check số tiền còn lại tổng DTNT
                        $data_get = array(
                            'CL_kh_id' => $kh_id,
                            'CL_quyenloi_bt' => DTNT_OB,
                            'get_row' => TRUE,
                        );
                        $ql_dbt_t = $this->orders_model->get_ql_cl($data_get);
                        if (is_object($ql_dbt_t)) {
                            $st_dtnt_bh_gh = (int) $ql_dbt_t->CL_st_bh_cl;
                        }
                        //======================================================
                        // Check số tiền còn lại THAISAN QLNT
                        $data_get = array(
                            'CL_kh_id' => $kh_id,
                            'CL_quyenloi_bt' => THAISAN_QLNT,
                            'get_row' => TRUE,
                        );
                        $tsqlnt_dbt = $this->orders_model->get_ql_cl($data_get);
                        if (is_object($tsqlnt_dbt)) {
                            $st_tsqlnt_bh_gh = (int) $tsqlnt_dbt->CL_st_bh_cl;
                        }
                        //======================================================
                        // Lấy qlcl của ql sinh thường hoặc sinh mổ
                        $data_get = array(
                            'CL_quyenloi_bt' => $loai_thaisan,
                            'CL_kh_id' => $kh_id,
                            'get_row' => TRUE,
                        );
                        $qlbt_cl = $this->orders_model->get_ql_cl($data_get);
                        if (is_object($qlbt_cl)) {
                            $CL_ss_id = $qlbt_cl->id;
                            $CL_st_dbt = $qlbt_cl->CL_st_dbt;
                            $CL_st_bh_cl = $qlbt_cl->CL_st_bh_cl;
                            //--------------------------------------------------------------
                            $st_tsqlnt_ss = $CL_st_bh_cl;
                            //==============================================================
                        }
                        //======================================================
                        $a_st_bh_gh = array($st_dtnt_bh_gh, $st_tsqlnt_bh_gh, $st_tsqlnt_ss);
//                        echo '<pre>';
//                        print_r($a_st_bh_gh);
//                        die;
                        $st_bh_gh_max = min($a_st_bh_gh);
                        //======================================================
                        // Nếu tổng số tiền bồi thường nhỏ hơn số tiền còn lại
                        $so_tien_dbt_t = $dtnt_tvpn_st_dbt + $dtnt_tgpn_st_dbt + $dtnt_cppt_st_dbt + $dtnt_tk_nv_st_dbt + $dtnt_sk_nv_st_dbt +
                                $dtnt_cpty_tn_st_dbt + $dtnt_tcvp_st_dbt + $dtnt_tcmt_st_dbt + $dtnt_xct_st_dbt + $tsqlnt_ktdk_st_dbt + $tsqlnt_dn_st_dbt;
//                        echo $so_tien_dbt_t.'/'.$st_bh_gh_max;
//                        die;
                        if ((int) $so_tien_dbt_t < $st_bh_gh_max) {
                            $bt_status_update = BT_PHEDUYET;
                            //==================================================
                            // Tiền viện phí/năm
                            if ($dtnt_tvpn_st_dbt > 0 && $dtnt_tvpn_sn_dbt > 0) {
                                $data_tvpn = array(
                                    'QLBT_name' => dtnt_tvpn,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_tvpn_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_tvpn_st_dbt,
                                    'qlbt_sn_ycbt' => $dtnt_tvpn_sn_ycbt,
                                    'qlbt_sn_dbt' => $dtnt_tvpn_sn_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
                                //$this->orders_model->CommonCreat(TBL_QLBT, $data_tvpn);
                            }
                            //==================================================
                            // Tiền giường,phòng/ngày
                            if ($dtnt_tgpn_st_dbt > 0 && $dtnt_tgpn_sn_dbt > 0) {
                                $data_tgpn = array(
                                    'QLBT_name' => dtnt_tgpn,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_tgpn_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_tgpn_st_dbt,
                                    'qlbt_sn_ycbt' => $dtnt_tgpn_sn_ycbt,
                                    'qlbt_sn_dbt' => $dtnt_tgpn_sn_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tgpn);
                            }
                            //==================================================
                            // Chi phí phẫu thuật
                            if ($dtnt_cppt_st_dbt > 0) {
                                $data_cppt = array(
                                    'QLBT_name' => dtnt_cppt,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_cppt_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_cppt_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_cppt);
                            }
                            //==================================================
                            // Quyền lợi khám trước khi nhập viện
                            if ($dtnt_tk_nv_st_dbt > 0) {
                                $data_tk_nv = array(
                                    'QLBT_name' => dtnt_tk_nv,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_tk_nv_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_tk_nv_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tk_nv);
                            }
                            //==================================================
                            // Quyền lợi khám sau khi nhập viện
                            if ($dtnt_sk_nv_st_dbt > 0) {
                                $data_sk_nv = array(
                                    'QLBT_name' => dtnt_sk_nv,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_sk_nv_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_sk_nv_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_sk_nv);
                            }
                            //==================================================
                            // Chi phí y tế tại nhà
                            if ($dtnt_cpyt_tn_st_dbt > 0) {
                                $data_cpyt_tn = array(
                                    'QLBT_name' => dtnt_cpyt_tn,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_cpyt_tn_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_cpyt_tn_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_cpyt_tn);
                            }
                            //==================================================
                            // Trợ cấp viện phí
                            if ($dtnt_tcvp_st_dbt > 0 && $dtnt_tcvp_sn_dbt > 0) {
                                $data_tcvp = array(
                                    'QLBT_name' => dtnt_tcvp,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_tcvp_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_tcvp_st_dbt,
                                    'qlbt_sn_ycbt' => $dtnt_tcvp_sn_ycbt,
                                    'qlbt_sn_dbt' => $dtnt_tcvp_sn_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tcvp);
                            }
                            //==================================================
                            // Trợ cấp mai táng
                            if ($dtnt_tcmt_st_dbt > 0) {
                                $data_tcmt = array(
                                    'QLBT_name' => dtnt_tcmt,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_tcmt_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_tcmt_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tcmt);
                            }
                            //==================================================
                            // Xe cứu thương
                            if ($dtnt_xct_st_dbt > 0) {
                                $data_xct = array(
                                    'QLBT_name' => dtnt_xct,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $dtnt_xct_st_ycbt,
                                    'qlbt_st_dbt' => $dtnt_xct_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_xct);
                            }
                            //==================================================
                            // Khám thai định kỳ
//                            echo $tsqlnt_ktdk_st_dbt;
//                            die;
                            if ($tsqlnt_ktdk_st_dbt > 0) {
                                $data_ktdk = array(
                                    'QLBT_name' => tsqlnt_ktdk,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $tsqlnt_ktdk_st_ycbt,
                                    'qlbt_st_dbt' => $tsqlnt_ktdk_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_xct);
                            }
                            //==================================================
                            // Dưỡng nhi
                            if ($tsqlnt_dn_st_dbt > 0) {
                                $data_dn = array(
                                    'QLBT_name' => tsqlnt_dn,
                                    'QLBT_KH_id' => $kh_id,
                                    'QLBT_BT_id' => $bt_id,
                                    'qlbt_st_ycbt' => $tsqlnt_dn_st_ycbt,
                                    'qlbt_st_dbt' => $tsqlnt_dn_st_dbt,
                                    'qlbt_status' => $bt_status_update,
                                    'date_created' => date('Y-m-d H:i:s'),
                                    'date_updated' => date('Y-m-d H:i:s'),
                                    'time_created' => strtotime(date('Y-m-d')),
                                    'time_updated' => strtotime(date('Y-m-d')),
                                );
//                                $this->orders_model->CommonCreat(TBL_QLBT, $data_xct);
                            }
                            //==================================================
                            // Check phân loại thai sản
                            $loai_thaisan = get_field_thaisan($phanloai_thaisan);
//                            echo $loai_thaisan;
//                            die;
                            $data_tsqlnt = array(
                                'QLBT_name' => $loai_thaisan,
                                'QLBT_KH_id' => $kh_id,
                                'QLBT_BT_id' => $bt_id,
                                'qlbt_st_ycbt' => $st_ycbt,
                                'qlbt_st_dbt' => $so_tien_dbt_t,
                                'qlbt_status' => $bt_status_update,
                                'date_created' => date('Y-m-d H:i:s'),
                                'date_updated' => date('Y-m-d H:i:s'),
                                'time_created' => strtotime(date('Y-m-d')),
                                'time_updated' => strtotime(date('Y-m-d')),
                            );
                            //==================================================
                            // Update bồi thường
                            $post_data = array(
                                'user2' => $this->phpsession->get('user_id'),
                                'order_status' => $bt_status_update,
                                'so_tien_dbt' => $so_tien_dbt_t,
                                //'st_dbt_t' => $so_tien_dbt_t,
                                'OD_ma_benh' => $this->input->post('ma_benh', TRUE),
                                'chuandoan_benh' => $this->input->post('chuandoan_benh', TRUE),
                                'lydo_khong_bt' => $this->input->post('lydo_khong_bt', TRUE),
                                'ghichu_khong_bt' => $this->input->post('ghichu_khong_bt', TRUE),
                                'ghichu_xuly_bt' => $this->input->post('ghichu_xuly_bt', TRUE),
                                'editor' => $this->phpsession->get('user_id'),
                            );
                            //==============================================
                            $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                            $post_data['updated_time'] = strtotime(date('Y-m-d'));

//                            echo '<pre>';
//                            print_r($data_tvpn);
//                            print_r($data_tgpn);
//                            print_r($data_cppt);
//                            print_r($data_tk_nv);
//                            print_r($data_sk_nv);
//                            print_r($data_cpyt_tn);
//                            print_r($data_tcvp);
//                            print_r($data_tcmt);
//                            print_r($data_xct);
//                            print_r($data_ktdk);
//                            print_r($data_dn);
//                            print_r($data_tsqlnt);
//                            print_r($post_data);
//                            die;
                            if (isset($data_tvpn) && !empty($data_tvpn)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tvpn);
                            }
                            if (isset($data_tgpn) && !empty($data_tgpn)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tgpn);
                            }
                            if (isset($data_cppt) && !empty($data_cppt)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_cppt);
                            }
                            if (isset($data_tk_nv) && !empty($data_tk_nv)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tk_nv);
                            }
                            if (isset($data_sk_nv) && !empty($data_sk_nv)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_sk_nv);
                            }
                            if (isset($data_cpyt_tn) && !empty($data_cpyt_tn)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_cpyt_tn);
                            }
                            if (isset($data_tcvp) && !empty($data_tcvp)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tcvp);
                            }
                            if (isset($data_tcmt) && !empty($data_tcmt)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tcmt);
                            }
                            if (isset($data_xct) && !empty($data_xct)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_xct);
                            }
                            //==================================================
                            if (isset($data_ktdk) && !empty($data_ktdk)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_ktdk);
                            }
                            if (isset($data_dn) && !empty($data_dn)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_dn);
                            }
                            //==================================================
                            if (isset($data_tsqlnt) && !empty($data_tsqlnt)) {
                                $this->orders_model->CommonCreat(TBL_QLBT, $data_tsqlnt);
                            }
                            //==================================================
                            // Update data bảng bt
                            $res = $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                            //==================================================
                            redirect(ORDERS_ADMIN_BASE_URL);
                        } else {
                            return FALSE;
                        }
                        //======================================================
                        //redirect(ORDERS_ADMIN_BASE_URL);
                    }
                    //==========================================================
                } else {
                    redirect(ORDERS_ADMIN_BASE_URL);
                }
                return FALSE;
            }
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //==========================================================================
    function do_edit_orders_daduyet($options = array()) {
        $data = array();
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        $a_qlbt_sn = get_a_loai_benh_sn();
        $a_qlbt_slk = get_a_loai_benh_slk();
        $a_quyenloi_bt_not_select = get_a_quyenloi_bt_not_select();
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $goi_bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $st_ycbt = $bt->so_tien_ycbt;
            $st_dbt = $bt->so_tien_dbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $sn_dbt = $bt->so_ngay_dbt;
            $quyenloi_bt = $bt->quyenloi_bt;

            //$case_id = $bt->parent_id;
            //$st_ycbt_t = $bt->st_ycbt_t;
            //$st_dbt_t = $bt->st_dbt_t;
            //==================================================================
            if (in_array($quyenloi_bt, $a_quyenloi_bt_not_select)) {
                // qlbt là ql cha lúc chọn
            } else {
                // Phải chọn qlbt con
                $quyenloi_bt = $this->input->post('quyenloi_bt_2', TRUE);
            }
            //==================================================================
            // Check ql_bt
            if ($quyenloi_bt == tv_tttbvv_tn_tv || $quyenloi_bt == tv_tttbvv_tn_tt) {
                $quyenloi_bt = TV_TTTBVV_TN;
            }
            if ($quyenloi_bt == tv_tttbvv_ob_tv || $quyenloi_bt == tv_tttbvv_ob_tt) {
                $quyenloi_bt = TV_TTTBVV_OB;
            }
            //==================================================================

            if ($quyenloi_bt != '') {

                //==============================================================
                // Lấy st giới hạn qlbt của gói bh
                $st_bh_gh = $goi_bh->$quyenloi_bt;
                // Lấy field qlbt
                $quyenloi_bt_filed = get_qlbt_field_by_qlbt($quyenloi_bt);
//                echo $quyenloi_bt_filed;
//                die;
                // Nếu bt có số ngày: Số tiền bh gh = st/ngay * số ngày
                if (in_array($quyenloi_bt, $a_qlbt_sn)) {
                    $field_qlbt_sn = strtolower($quyenloi_bt) . '_sn';
                    $sn_bh_gh = $goi_bh->$field_qlbt_sn;
                    $st_bh_gh = (int) $st_bh_gh * (int) $sn_bh_gh;
                }
//                echo $st_bh_gh;
//                die;
                // end
                //==============================================================
                // Update bồi thường
                $post_data = array(
                    'user4' => $this->phpsession->get('user_id'),
                    'order_status' => BT_THANHTOAN,
                    'ghichu_thanhtoan_bt' => $this->input->post('ghichu_thanhtoan_bt', TRUE),
                    'date_thanhtoan' => $datetime_now,
                    'time_thanhtoan' => $time_now,
                    'editor' => $this->phpsession->get('user_id'),
                );

                $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                $post_data['updated_time'] = strtotime(date('Y-m-d'));
//                echo '<pre>';
//                print_r($post_data);
//                die;
//                end
                //==============================================================
                // Lấy data quyền lợi còn lại
                $data_get = array(
                    'CL_quyenloi_bt' => $quyenloi_bt_filed,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_cl = $this->orders_model->get_ql_cl($data_get);
//                echo '<pre>';
//                print_r($data_get);
//                die;
                if (is_object($qlbt_cl)) {
                    $CL_id = $qlbt_cl->id;
                    $CL_st_dbt = $qlbt_cl->CL_st_dbt;
                    $st_bh_gh = $CL_st_bh_cl = $qlbt_cl->CL_st_bh_cl;
                    $CL_sn_dbt = $qlbt_cl->CL_sn_dbt;
                    $CL_sn_bh_cl = $qlbt_cl->CL_sn_bh_cl;
                    // end
                    $data_cl['CL_st_dbt'] = (int) $CL_st_dbt + (int) $st_dbt;
                    $data_cl['CL_st_bh_cl'] = (int) $CL_st_bh_cl - (int) $st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_cl = array(
                        'CL_quyenloi_bt' => $quyenloi_bt_filed,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => $st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_gh - (int) $st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
                // Nếu bt có số ngày
                if (in_array($quyenloi_bt, $a_qlbt_sn)) {
                    // Nếu ql đã bồi thường
                    if (isset($CL_sn_bh_cl) && $CL_sn_bh_cl > 0) {
                        // Data bảng còn lại
                        $data_cl['CL_sn_dbt'] = (int) $CL_sn_dbt + (int) $sn_dbt;
                        $data_cl['CL_sn_bh_cl'] = (int) $CL_sn_bh_cl - (int) $sn_dbt;
                    } else {
                        // Bồi thường lần đầu
                        // Data bảng còn lại
                        $data_cl['CL_sn_dbt'] = (int) $sn_dbt;
                        $data_cl['CL_sn_bh_cl'] = (int) $sn_bh_gh - (int) $sn_dbt;
                    }
                }
                //==================================================================
                $data_cl['date_updated'] = date('Y-m-d H:i:s');
                $data_cl['time_updated'] = strtotime(date('Y-m-d'));
//                echo '<pre>';
//                echo $CL_id;
//                print_r($post_data);
//                die;
                //end
                //==============================================================
                // Nếu st_dbt < st_bh_gh
//                echo $st_dbt . '/' . $st_bh_gh;
//                die;
                if ($st_dbt <= $st_bh_gh) {
//                    echo $st_dbt.'/'.$st_bh_gh;
//                    die;
                    // Update bảng bt
                    $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                    //==========================================================
                    if (isset($CL_id) && $CL_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_id, $data_cl);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_cl);
                    }
                    //==========================================================
                    // Nếu bt là case con
                    /*
                     * if ($case_id > 0) {
                      // Lấy bt cha
                      $data_get = array(
                      'BT_id' => $case_id,
                      'get_one' => TRUE,
                      );
                      $bt_t = $this->orders_model->get_orders_by_options($data_get);
                      if (is_object($bt_t)) {
                      $t_st_ycbt_t = $bt_t->st_ycbt_t;
                      $t_st_dbt_t = $bt_t->st_dbt_t;
                      //==================================================
                      $data_update = array(
                      'st_ycbt_t' => $t_st_ycbt_t + $st_ycbt_t,
                      'st_ycbt_t' => $t_st_dbt_t + $st_dbt_t,
                      );
                      $this->orders_model->CommonUpdate(TBL_ORDERS, $case_id, $data_update);
                      }
                      }
                     */
                    //==========================================================
                }
                //==============================================================
                redirect(ORDER_ADMIN_BASE_URL);
            } else {
                redirect(ORDER_ADMIN_BASE_URL);
            }
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
    }

    //end
    //==========================================================================
    function do_edit_orders_daduyet_nhakhoa_mdl($options = array()) {
        $data = array();
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        $a_qlbt_sn = get_a_loai_benh_sn();
        $a_qlbt_slk = get_a_loai_benh_slk();
        $a_quyenloi_bt_not_select = get_a_quyenloi_bt_not_select();
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $goi_bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $bt_status = $bt->order_status;
            $st_ycbt = $bt->so_tien_ycbt;
            $st_dbt = $bt->so_tien_dbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $sn_dbt = $bt->so_ngay_dbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            //$case_id = $bt->parent_id;
            //$st_ycbt_t = $bt->st_ycbt_t;
            //$st_dbt_t = $bt->st_dbt_t;
            //==================================================================
            if ($bt_status == BT_THANHTOAN) {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
            // Số tiền bh gh tổng (NHAKHOA_MDL)
            $st_bh_gh = $goi_bh->$quyenloi_bt;
            // Số tiền cvr gh
            $field_cvr = nkmdl_cvr;
            $st_bh_gh_cvr = $goi_bh->$field_cvr;
            //==================================================================
            $quyenloi_bt_2 = $this->input->post('quyenloi_bt_2', TRUE);
            $nkmdl_cvr = $this->input->post('nkmdl_cvr', TRUE);
            //==================================================================
            // Nếu qlbt có cvr
//            echo $nkmdl_cvr;
//            die;
            if ($nkmdl_cvr == TRUE) {
                // St_dbt cvr
                $st_dbt_cvr = $this->input->post('nkmdl_cvr_st_dbt', TRUE);
                //==============================================================
            } else {
                $st_dbt_cvr = 0;
            }
            //==================================================================
            // Nếu có chọn qlbt con
//            echo $quyenloi_bt_2;
//            die;
            if ($quyenloi_bt_2 != '') {
                // Số tiền bh gh qlbt
                $st_bh_gh_2 = $goi_bh->$quyenloi_bt_2;
                //==============================================================
                // Lấy field st_dbt của qlbt
                $st_dbt_2_filed = $quyenloi_bt_2 . '_st_dbt';
                // Lấy st_dbt của qlbt con được chọn
                $st_dbt_2 = $this->input->post($st_dbt_2_filed, TRUE);
                //==============================================================
            } else {
                $st_dbt_2 = 0;
            }
//            echo $st_dbt_2;
//            die;
            //==================================================================
            // Lấy qlcl của ql bh cha (NHAKHOA_MDL)
            $data_get = array(
                'CL_quyenloi_bt' => $quyenloi_bt,
                'CL_kh_id' => $kh_id,
                'get_row' => TRUE,
            );
            $qlbt_cl = $this->orders_model->get_ql_cl($data_get);
            if (is_object($qlbt_cl)) {
                $CL_id = $qlbt_cl->id;
                $CL_st_dbt = $qlbt_cl->CL_st_dbt;
                $CL_st_bh_cl = $qlbt_cl->CL_st_bh_cl;
                $st_bh_gh = $CL_st_bh_cl;
                $CL_sn_dbt = $qlbt_cl->CL_sn_dbt;
                $CL_sn_bh_cl = $qlbt_cl->CL_sn_bh_cl;
                //==========================================================
                $data_cl['CL_st_dbt'] = (int) $CL_st_dbt + (int) $st_dbt;
                $data_cl['CL_st_bh_cl'] = (int) $CL_st_bh_cl - (int) $st_dbt;
            } else {
                // Thêm data bảng còn lại NHAKHOA_MDL
                $data_cl = array(
                    'CL_quyenloi_bt' => $quyenloi_bt,
                    'CL_kh_id' => $kh_id,
                    'CL_st_dbt' => (int) $st_dbt,
                    'CL_st_bh_cl' => (int) $st_bh_gh - (int) $st_dbt,
                    'date_created' => date('Y-m-d H:i:s'),
                    'time_created' => strtotime(date('Y-m-d')),
                );
            }
            $data_cl['date_updated'] = date('Y-m-d H:i:s');
            $data_cl['time_updated'] = strtotime(date('Y-m-d'));
            //==================================================================
            // Nếu st_dbt < st_bh_gh
//            echo $st_dbt.'/'.$st_bh_gh;
//            die;
            if ($st_dbt <= $st_bh_gh) {
                // Lấy qlcl của cvr nếu có qlbt cvr(NHAKHOA_MDL)
                if ($nkmdl_cvr == TRUE) {
                    $data_get = array(
                        'CL_quyenloi_bt' => nkmdl_cvr,
                        'CL_kh_id' => $kh_id,
                        'get_row' => TRUE,
                    );
                    $qlbt_cl_cvr = $this->orders_model->get_ql_cl($data_get);
                    if (is_object($qlbt_cl_cvr)) {

                        $CL_id_cvr = $qlbt_cl_cvr->id;
                        $CL_st_dbt_cvr = $qlbt_cl_cvr->CL_st_dbt;
                        $CL_st_bh_cl_cvr = $qlbt_cl_cvr->CL_st_bh_cl;
                        $st_bh_gh_cvr = $CL_st_bh_cl_cvr;
                        //======================================================
                        $CL_sn_dbt_cvr = $qlbt_cl_cvr->CL_sn_dbt;
                        $CL_sn_bh_cl_cvr = $qlbt_cl_cvr->CL_sn_bh_cl;
                        //======================================================
                        $data_cl_cvr['CL_st_dbt'] = (int) $CL_st_dbt_cvr + (int) $st_dbt_cvr;
                        $data_cl_cvr['CL_st_bh_cl'] = (int) $CL_st_bh_cl_cvr - (int) $st_dbt_cvr;
                    } else {
                        // Thêm data bảng còn lại cvr
                        $data_cl_cvr = array(
                            'CL_quyenloi_bt' => nkmdl_cvr,
                            'CL_kh_id' => $kh_id,
                            'CL_st_dbt' => (int) $st_dbt_cvr,
                            'CL_st_bh_cl' => (int) $st_bh_gh_cvr - (int) $st_dbt_cvr,
                            'date_created' => date('Y-m-d H:i:s'),
                            'time_created' => strtotime(date('Y-m-d')),
                        );
                    }
                    $data_cl_cvr['date_updated'] = date('Y-m-d H:i:s');
                    $data_cl_cvr['time_updated'] = strtotime(date('Y-m-d'));
                }
                //==============================================================
                //==============================================================
                // Nếu có chọn qlbt con
                if ($quyenloi_bt_2 != '') {
                    // Lấy data quyền lợi còn lại con của qlbt được chọn
                    $data_get = array(
                        'CL_quyenloi_bt' => $quyenloi_bt_2,
                        'CL_kh_id' => $kh_id,
                        'get_row' => TRUE,
                    );
                    $qlbt_cl_2 = $this->orders_model->get_ql_cl($data_get);

                    if (is_object($qlbt_cl_2)) {
                        $CL_id_2 = $qlbt_cl_2->id;
                        $CL_st_dbt_2 = $qlbt_cl_2->CL_st_dbt;
                        $CL_st_bh_cl_2 = $qlbt_cl_2->CL_st_bh_cl;
                        $st_bh_gh_2 = $CL_st_bh_cl_2;
                        $CL_sn_dbt_2 = $qlbt_cl_2->CL_sn_dbt;
                        $CL_sn_bh_cl_2 = $qlbt_cl_2->CL_sn_bh_cl;
                        // end
                        $data_cl_2['CL_st_dbt'] = (int) $CL_st_dbt_2 + (int) $st_dbt_2 + (int) $st_dbt_cvr;
                        $data_cl_2['CL_st_bh_cl'] = (int) $CL_st_bh_cl_2 - (int) $st_dbt_2 - (int) $st_dbt_cvr;
                    } else {
                        // Thêm data bảng còn lại
                        $data_cl_2 = array(
                            'CL_quyenloi_bt' => $quyenloi_bt_2,
                            'CL_kh_id' => $kh_id,
                            'CL_st_dbt' => (int) $st_dbt_2 + (int) $st_dbt_cvr,
                            'CL_st_bh_cl' => (int) $st_bh_gh_2 - (int) $st_dbt_2 - (int) $st_dbt_cvr,
                            'date_created' => date('Y-m-d H:i:s'),
                            'time_created' => strtotime(date('Y-m-d')),
                        );
                    }
                    //==========================================================
                    $data_cl_2['date_updated'] = date('Y-m-d H:i:s');
                    $data_cl_2['time_updated'] = strtotime(date('Y-m-d'));
                    //end
                }
                //==============================================================
                //==============================================================
                // Update bồi thường
                $post_data = array(
                    'user4' => $this->phpsession->get('user_id'),
                    'order_status' => BT_THANHTOAN,
                    'ghichu_thanhtoan_bt' => $this->input->post('ghichu_thanhtoan_bt', TRUE),
                    'date_thanhtoan' => $datetime_now,
                    'time_thanhtoan' => $time_now,
                    'editor' => $this->phpsession->get('user_id'),
                );

                $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                $post_data['updated_time'] = strtotime(date('Y-m-d'));
                //==============================================================
                //==============================================================
//                echo '<pre>';
//                print_r($data_cl_cvr);
//                print_r($data_cl_2);
//                print_r($data_cl);
//                print_r($post_data);
//                die;
                if ($nkmdl_cvr == TRUE) {
                    if ($st_dbt_cvr <= $st_bh_gh_cvr) {
                        if (isset($CL_id_cvr) && $CL_id_cvr > 0) {
                            // Update data bảng còn lại cvr (NHAKHOA_MDL)
                            $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_id_cvr, $data_cl_cvr);
                        } else {
                            // Thêm mới data bảng còn lại cvr (NHAKHOA_MDL)
                            $res = $this->orders_model->CommonCreat(TBL_CL, $data_cl_cvr);
                        }
                    } else {
                        redirect(ORDER_ADMIN_BASE_URL);
                    }
                }
                //==============================================================
                //==============================================================
                if ($quyenloi_bt_2 != '') {
                    if ($st_dbt_2 <= $st_bh_gh_2) {
                        if (isset($CL_id_2) && $CL_id_2 > 0) {
                            // Update data bảng còn lại
                            $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_id_2, $data_cl_2);
                        } else {
                            // Thêm mới data bảng còn lại
                            $res = $this->orders_model->CommonCreat(TBL_CL, $data_cl_2);
                        }
                    } else {
                        redirect(ORDER_ADMIN_BASE_URL);
                    }
                }
                //==============================================================
                //==============================================================
                // Xử lý qlcl cha
                // Nếu st dbt nhỏ hơn stcl
                if (isset($CL_id) && $CL_id > 0) {
                    // Update data bảng còn lại cha (NHAKHOA_MDL)
                    $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_id, $data_cl);
                } else {
                    // Thêm mới data bảng còn lại cha (NHAKHOA_MDL)
                    $res = $this->orders_model->CommonCreat(TBL_CL, $data_cl);
                }
                //==============================================================
                // Update bồi thường
                $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                //==============================================================
                // Nếu bt là case con
                /*
                 * if ($case_id > 0) {
                  // Lấy bt cha
                  $data_get = array(
                  'BT_id' => $case_id,
                  'get_one' => TRUE,
                  );
                  $bt_t = $this->orders_model->get_orders_by_options($data_get);
                  if (is_object($bt_t)) {
                  $t_st_ycbt_t = $bt_t->st_ycbt_t;
                  $t_st_dbt_t = $bt_t->st_dbt_t;
                  //======================================================
                  $data_update = array(
                  'st_ycbt_t' => $t_st_ycbt_t + $st_ycbt_t,
                  'st_ycbt_t' => $t_st_dbt_t + $st_dbt_t,
                  );
                  $this->orders_model->CommonUpdate(TBL_ORDERS, $case_id, $data_update);
                  }
                  }
                  //==============================================================
                 */

                //==============================================================
            } else {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
            //==================================================================
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
    }

    //end
    //==========================================================================
    function do_edit_orders_daduyet_thaisan_mdl($options = array()) {
        $data = array();
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $goi_bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $bt_status = $bt->order_status;
            $st_ycbt = $bt->so_tien_ycbt;
            $st_dbt = $bt->so_tien_dbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $sn_dbt = $bt->so_ngay_dbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            //$case_id = $bt->parent_id;
            //$st_ycbt_t = $bt->st_ycbt_t;
            //$st_dbt_t = $bt->st_dbt_t;
            //==================================================================
            if ($bt_status == BT_THANHTOAN) {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
            // Số tiền bh gh tổng (THAISAN_MDL)
            $st_bh_gh = $goi_bh->$quyenloi_bt;
            // Số tiền dưỡng nhi
            $field_tsmdl_dn = tsmdl_dn;
            $st_bh_gh_tsmdl_dn = $goi_bh->$field_tsmdl_dn;
            //==================================================================
            $quyenloi_bt_2 = $this->input->post('quyenloi_bt_2', TRUE);
            $tsmdl_dn = $this->input->post('tsmdl_dn', TRUE);
            //==================================================================
            // Nếu qlbt có cvr
//            echo $nkmdl_cvr;
//            die;
            if ($tsmdl_dn == TRUE) {
                // St_dbt dưỡng nhi
                $st_dbt_tsmdl_dn = $this->input->post('tsmdl_dn_st_dbt', TRUE);
                //==============================================================
            } else {
                $st_dbt_tsmdl_dn = 0;
            }
            //==================================================================
            // Nếu có chọn qlbt con
//            echo $quyenloi_bt_2;
//            die;
            if ($quyenloi_bt_2 != '') {
                // Số tiền bh gh qlbt
                $st_bh_gh_2 = $goi_bh->$quyenloi_bt_2;
                //==============================================================
                // Lấy field st_dbt của qlbt
                $st_dbt_2_filed = $quyenloi_bt_2 . '_st_dbt';
                // Lấy st_dbt của qlbt con được chọn
                $st_dbt_2 = $this->input->post($st_dbt_2_filed, TRUE);
                //==============================================================
            } else {
                redirect(ORDER_ADMIN_BASE_URL);
                //$st_dbt_2 = 0;
            }
//            echo $st_dbt_2;
//            die;
            //==================================================================
            // Lấy qlcl của ql bh cha (THAISAN_MDL)
            $data_get = array(
                'CL_quyenloi_bt' => $quyenloi_bt,
                'CL_kh_id' => $kh_id,
                'get_row' => TRUE,
            );
            $qlbt_cl = $this->orders_model->get_ql_cl($data_get);
            if (is_object($qlbt_cl)) {
                $CL_id = $qlbt_cl->id;
                $CL_st_dbt = $qlbt_cl->CL_st_dbt;
                $CL_st_bh_cl = $qlbt_cl->CL_st_bh_cl;
                $st_bh_gh = $CL_st_bh_cl;
                $CL_sn_dbt = $qlbt_cl->CL_sn_dbt;
                $CL_sn_bh_cl = $qlbt_cl->CL_sn_bh_cl;
                //==========================================================
                $data_cl['CL_st_dbt'] = (int) $CL_st_dbt + (int) $st_dbt;
                $data_cl['CL_st_bh_cl'] = (int) $CL_st_bh_cl - (int) $st_dbt;
            } else {
                // Thêm data bảng còn lại NHAKHOA_MDL
                $data_cl = array(
                    'CL_quyenloi_bt' => $quyenloi_bt,
                    'CL_kh_id' => $kh_id,
                    'CL_st_dbt' => (int) $st_dbt,
                    'CL_st_bh_cl' => (int) $st_bh_gh - (int) $st_dbt,
                    'date_created' => date('Y-m-d H:i:s'),
                    'time_created' => strtotime(date('Y-m-d')),
                );
            }
            $data_cl['date_updated'] = date('Y-m-d H:i:s');
            $data_cl['time_updated'] = strtotime(date('Y-m-d'));
            //==================================================================
            // Nếu st_dbt < st_bh_gh
//            echo $st_dbt.'/'.$st_bh_gh;
//            die;
            if ($st_dbt <= $st_bh_gh) {
                // Lấy qlcl của dưỡng nhi nếu có qlbt dưỡng nhi(THAISAN_MDL)
                if ($quyenloi_bt_2 == tsmdl_st || $quyenloi_bt_2 == tsmdl_sm) {
                    if ($tsmdl_dn == TRUE) {
                        $data_get = array(
                            'CL_quyenloi_bt' => tsmdl_dn,
                            'CL_kh_id' => $kh_id,
                            'get_row' => TRUE,
                        );
                        $qlbt_cl_dn = $this->orders_model->get_ql_cl($data_get);
                        if (is_object($qlbt_cl_dn)) {

                            $CL_id_tsmdl_dn = $qlbt_cl_dn->id;
                            $CL_st_dbt_tsmdl_dn = $qlbt_cl_dn->CL_st_dbt;
                            $CL_st_bh_cl_tsmdl_dn = $qlbt_cl_dn->CL_st_bh_cl;
                            $st_bh_gh_tsmdl_dn = $CL_st_bh_cl_tsmdl_dn;
                            //======================================================
                            $data_cl_dn['CL_st_dbt'] = (int) $CL_st_dbt_tsmdl_dn + (int) $st_dbt_tsmdl_dn;
                            $data_cl_dn['CL_st_bh_cl'] = (int) $CL_st_bh_cl_tsmdl_dn - (int) $st_dbt_tsmdl_dn;
                        } else {
                            // Thêm data bảng còn lại dưỡng nhi
                            $data_cl_dn = array(
                                'CL_quyenloi_bt' => tsmdl_dn,
                                'CL_kh_id' => $kh_id,
                                'CL_st_dbt' => (int) $st_dbt_tsmdl_dn,
                                'CL_st_bh_cl' => (int) $st_bh_gh_tsmdl_dn - (int) $st_dbt_tsmdl_dn,
                                'date_created' => date('Y-m-d H:i:s'),
                                'time_created' => strtotime(date('Y-m-d')),
                            );
                        }
                        $data_cl_dn['date_updated'] = date('Y-m-d H:i:s');
                        $data_cl_dn['time_updated'] = strtotime(date('Y-m-d'));
                    }
                    //==========================================================
                }
                //==============================================================
                //==============================================================
                // Nếu có chọn qlbt con
                if ($quyenloi_bt_2 != '') {
                    // Lấy data quyền lợi còn lại con của qlbt được chọn
                    $data_get = array(
                        'CL_quyenloi_bt' => $quyenloi_bt_2,
                        'CL_kh_id' => $kh_id,
                        'get_row' => TRUE,
                    );
                    $qlbt_cl_2 = $this->orders_model->get_ql_cl($data_get);

                    if (is_object($qlbt_cl_2)) {
                        $CL_id_2 = $qlbt_cl_2->id;
                        $CL_st_dbt_2 = $qlbt_cl_2->CL_st_dbt;
                        $CL_st_bh_cl_2 = $qlbt_cl_2->CL_st_bh_cl;
                        $st_bh_gh_2 = $CL_st_bh_cl_2;
                        $CL_sn_dbt_2 = $qlbt_cl_2->CL_sn_dbt;
                        $CL_sn_bh_cl_2 = $qlbt_cl_2->CL_sn_bh_cl;
                        // end
                        $data_cl_2['CL_st_dbt'] = (int) $CL_st_dbt_2 + (int) $st_dbt_2;
                        $data_cl_2['CL_st_bh_cl'] = (int) $CL_st_bh_cl_2 - (int) $st_dbt_2;
                    } else {
                        // Thêm data bảng còn lại
                        $data_cl_2 = array(
                            'CL_quyenloi_bt' => $quyenloi_bt_2,
                            'CL_kh_id' => $kh_id,
                            'CL_st_dbt' => (int) $st_dbt_2,
                            'CL_st_bh_cl' => (int) $st_bh_gh_2 - (int) $st_dbt_2,
                            'date_created' => date('Y-m-d H:i:s'),
                            'time_created' => strtotime(date('Y-m-d')),
                        );
                    }
                    //==========================================================
                    $data_cl_2['date_updated'] = date('Y-m-d H:i:s');
                    $data_cl_2['time_updated'] = strtotime(date('Y-m-d'));
                    //end
                }
                //==============================================================
                //==============================================================
                // Update bồi thường
                $post_data = array(
                    'user4' => $this->phpsession->get('user_id'),
                    'order_status' => BT_THANHTOAN,
                    'ghichu_thanhtoan_bt' => $this->input->post('ghichu_thanhtoan_bt', TRUE),
                    'date_thanhtoan' => $datetime_now,
                    'time_thanhtoan' => $time_now,
                    'editor' => $this->phpsession->get('user_id'),
                );

                $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                $post_data['updated_time'] = strtotime(date('Y-m-d'));
                //==============================================================
                //==============================================================
//                echo '<pre>';
//                print_r($data_cl_dn);
//                print_r($data_cl_2);
//                print_r($data_cl);
//                print_r($post_data);
//                die;
                if ($tsmdl_dn == TRUE) {
                    if ($st_dbt_tsmdl_dn <= $st_bh_gh_tsmdl_dn) {
                        if (isset($CL_id_tsmdl_dn) && $CL_id_tsmdl_dn > 0) {
                            // Update data bảng còn lại dưỡng nhi (THAISAN_MDL)
                            $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_id_tsmdl_dn, $data_cl_dn);
                        } else {
                            // Thêm mới data bảng còn lại dưỡng nhi (THAISAN_MDL)
                            $res = $this->orders_model->CommonCreat(TBL_CL, $data_cl_dn);
                        }
                    } else {
                        redirect(ORDER_ADMIN_BASE_URL);
                    }
                }
                //==============================================================
                //==============================================================
                if ($quyenloi_bt_2 != '') {
                    if ($st_dbt_2 <= $st_bh_gh_2) {
                        if (isset($CL_id_2) && $CL_id_2 > 0) {
                            // Update data bảng còn lại
                            $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_id_2, $data_cl_2);
                        } else {
                            // Thêm mới data bảng còn lại
                            $res = $this->orders_model->CommonCreat(TBL_CL, $data_cl_2);
                        }
                    } else {
                        redirect(ORDER_ADMIN_BASE_URL);
                    }
                }
                //==============================================================
                //==============================================================
                // Xử lý qlcl cha
                // Nếu st dbt nhỏ hơn stcl
                if (isset($CL_id) && $CL_id > 0) {
                    // Update data bảng còn lại cha (THAISAN_MDL)
                    $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_id, $data_cl);
                } else {
                    // Thêm mới data bảng còn lại cha (THAISAN_MDL)
                    $res = $this->orders_model->CommonCreat(TBL_CL, $data_cl);
                }
                //==============================================================
                // Update bồi thường
                $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                //==============================================================
                // Nếu bt là case con
                /*
                 * if ($case_id > 0) {
                  // Lấy bt cha
                  $data_get = array(
                  'BT_id' => $case_id,
                  'get_one' => TRUE,
                  );
                  $bt_t = $this->orders_model->get_orders_by_options($data_get);
                  if (is_object($bt_t)) {
                  $t_st_ycbt_t = $bt_t->st_ycbt_t;
                  $t_st_dbt_t = $bt_t->st_dbt_t;
                  //======================================================
                  $data_update = array(
                  'st_ycbt_t' => $t_st_ycbt_t + $st_ycbt_t,
                  'st_ycbt_t' => $t_st_dbt_t + $st_dbt_t,
                  );
                  $this->orders_model->CommonUpdate(TBL_ORDERS, $case_id, $data_update);
                  }
                  }
                  //==============================================================
                 */

                //==============================================================
            } else {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
            //==================================================================
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
    }

    //end
    //==========================================================================
    function do_edit_orders_daduyet_dtngt_ob($options = array()) {
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        $data = array();
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $goi_bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $bt_status = $bt->order_status;
            $st_ycbt = $bt->so_tien_ycbt;
            $st_dbt = $bt->so_tien_dbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $sn_dbt = $bt->so_ngay_dbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            //$case_id = $bt->parent_id;
            //$st_ycbt_t = $bt->st_ycbt_t;
            //$st_dbt_t = $bt->st_dbt_t;
            //==================================================================
            if ($bt_status == BT_THANHTOAN) {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
            // Số tiền bh gh tổng (DTNGT)
            $st_bh_gh = $goi_bh->$quyenloi_bt;
            $field_slk = dtngt_slk;
            $slk_bh_gh = $goi_bh->$field_slk;
            $slk_bh_dbt = 0;
            //==================================================================
            $quyenloi_bt_2 = $this->input->post('quyenloi_bt_2', TRUE);
            $quyenloi_bt_3 = $this->input->post('quyenloi_bt_3', TRUE);
            //==================================================================
            if ($quyenloi_bt_2 != '') {
                $st_bh_gh_2 = $goi_bh->$quyenloi_bt_2;
                //==============================================================
                if ($quyenloi_bt_2 == dtngt_st1lk) {
                    // Lấy field st_dbt của qlbt
                    $st_dbt_2_filed = $quyenloi_bt_2 . '_st_dbt';
                    // Lấy st_dbt của qlbt con được chọn
                    $st_dbt_2 = $this->input->post($st_dbt_2_filed, TRUE);
                    //==========================================================
                } else {
                    // Nếu có chọn qlbt con
                    if ($quyenloi_bt_3 != '') {
                        $st_bh_gh_3 = $goi_bh->$quyenloi_bt_3;
                        //======================================================
                        // Lấy field st_dbt của qlbt
                        $st_dbt_3_filed = $quyenloi_bt_3 . '_st_dbt';
                        // Lấy st_dbt của qlbt con được chọn
                        $st_dbt_3 = $this->input->post($st_dbt_3_filed, TRUE);
                    } else {
                        redirect(ORDER_ADMIN_BASE_URL);
                    }
                }
            } else {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
            // Lấy qlcl của ql bh cha (DTNGT)
            $data_get = array(
                'CL_quyenloi_bt' => $quyenloi_bt,
                'CL_kh_id' => $kh_id,
                'get_row' => TRUE,
            );
            $qlbt_cl = $this->orders_model->get_ql_cl($data_get);
            if (is_object($qlbt_cl)) {
                $CL_id = $qlbt_cl->id;
                $CL_st_dbt = $qlbt_cl->CL_st_dbt;
                $CL_st_bh_cl = $qlbt_cl->CL_st_bh_cl;
                $st_bh_gh = $CL_st_bh_cl;
                $CL_sn_dbt = $qlbt_cl->CL_sn_dbt;
                $CL_sn_bh_cl = $qlbt_cl->CL_sn_bh_cl;
                $slk_bh_dbt = $CL_slk_bh_dbt = $qlbt_cl->CL_slk_dbt;
                $slk_bh_gh = $CL_slk_bh_cl = $qlbt_cl->CL_slk_bh_cl;
                //==========================================================
                $data_cl['CL_st_dbt'] = (int) $CL_st_dbt + (int) $st_dbt;
                $data_cl['CL_st_bh_cl'] = (int) $CL_st_bh_cl - (int) $st_dbt;
            } else {
                // Thêm data bảng còn lại DTNGT
                $data_cl = array(
                    'CL_quyenloi_bt' => $quyenloi_bt,
                    'CL_kh_id' => $kh_id,
                    'CL_st_dbt' => (int) $st_dbt,
                    'CL_st_bh_cl' => (int) $st_bh_gh - (int) $st_dbt,
                    'date_created' => date('Y-m-d H:i:s'),
                    'time_created' => strtotime(date('Y-m-d')),
                );
            }
            $data_cl['date_updated'] = date('Y-m-d H:i:s');
            $data_cl['time_updated'] = strtotime(date('Y-m-d'));
            //==================================================================
            // Nếu st_dbt < st_bh_gh
//            echo $st_dbt.'/'.$st_bh_gh;
//            die;
            if ($st_dbt <= $st_bh_gh) {
                //==============================================================
                // Lấy data quyền lợi còn lại con của qlbt được chọn
                $data_get = array(
                    'CL_quyenloi_bt' => $quyenloi_bt_2,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_cl_2 = $this->orders_model->get_ql_cl($data_get);
                //==============================================================
                //end
                // Nếu qlbt chọn là khám ngoại trú
                if ($quyenloi_bt_2 == dtngt_st1lk) {
                    if (is_object($qlbt_cl_2)) {
                        $CL_id_2 = $qlbt_cl_2->id;
                        $CL_st_dbt_2 = $qlbt_cl_2->CL_st_dbt;
                        $CL_st_bh_cl_2 = $qlbt_cl_2->CL_st_bh_cl;
                        $st_bh_gh_2 = $CL_st_bh_cl_2;
                        $CL_sn_dbt_2 = $qlbt_cl_2->CL_sn_dbt;
                        $CL_sn_bh_cl_2 = $qlbt_cl_2->CL_sn_bh_cl;
                        // end
                        $data_cl_2['CL_st_dbt'] = (int) $CL_st_dbt_2 + (int) $st_dbt_2;
                        //$data_cl_2['CL_st_bh_cl'] = (int) $CL_st_bh_cl_2 - (int) $st_dbt_2;
                        //======================================================
                    } else {
                        // Thêm data bảng còn lại
                        $data_cl_2 = array(
                            'CL_quyenloi_bt' => $quyenloi_bt_2,
                            'CL_kh_id' => $kh_id,
                            'CL_st_dbt' => (int) $st_dbt_2,
                            //'CL_st_bh_cl' => (int) $st_bh_gh_2 - (int) $st_dbt_2,
                            'date_created' => date('Y-m-d H:i:s'),
                            'time_created' => strtotime(date('Y-m-d')),
                        );
                    }
                    //==========================================================
                    // Lấy slk còn lại của bt
                    $data_cl['CL_slk_dbt'] = (int) $slk_bh_dbt + 1;
                    $data_cl['CL_slk_bh_cl'] = (int) $slk_bh_gh - 1;
                    //======================================================
                } else {
                    // Nếu qlbt chọn là nha khoa trong ngoại trú
                    if (is_object($qlbt_cl_2)) {
                        $CL_id_2 = $qlbt_cl_2->id;
                        $CL_st_dbt_2 = $qlbt_cl_2->CL_st_dbt;
                        $CL_st_bh_cl_2 = $qlbt_cl_2->CL_st_bh_cl;
                        $st_bh_gh_2 = $CL_st_bh_cl_2;
                        $CL_sn_dbt_2 = $qlbt_cl_2->CL_sn_dbt;
                        $CL_sn_bh_cl_2 = $qlbt_cl_2->CL_sn_bh_cl;
                        // end
                        $data_cl_2['CL_st_dbt'] = (int) $CL_st_dbt_2 + (int) $st_dbt_3;
                        $data_cl_2['CL_st_bh_cl'] = (int) $CL_st_bh_cl_2 - (int) $st_dbt_3;
                    } else {
                        // Thêm data bảng còn lại
                        $data_cl_2 = array(
                            'CL_quyenloi_bt' => $quyenloi_bt_2,
                            'CL_kh_id' => $kh_id,
                            'CL_st_dbt' => (int) $st_dbt_3,
                            'CL_st_bh_cl' => (int) $st_bh_gh_2 - (int) $st_dbt_3,
                            'date_created' => date('Y-m-d H:i:s'),
                            'time_created' => strtotime(date('Y-m-d')),
                        );
                    }
                    //==========================================================
                    // Lấy data quyền lợi còn lại con của nha khoa trong ngoại trú
                    $data_get = array(
                        'CL_quyenloi_bt' => $quyenloi_bt_3,
                        'CL_kh_id' => $kh_id,
                        'get_row' => TRUE,
                    );
                    $qlbt_cl_3 = $this->orders_model->get_ql_cl($data_get);
                    //==========================================================
                    if (is_object($qlbt_cl_3)) {
                        $CL_id_3 = $qlbt_cl_3->id;
                        $CL_st_dbt_3 = $qlbt_cl_3->CL_st_dbt;
                        $CL_st_bh_cl_3 = $qlbt_cl_3->CL_st_bh_cl;
                        $st_bh_gh_3 = $CL_st_bh_cl_3;
                        $CL_sn_dbt_3 = $qlbt_cl_3->CL_sn_dbt;
                        $CL_sn_bh_cl_3 = $qlbt_cl_3->CL_sn_bh_cl;
                        // end
                        $data_cl_3['CL_st_dbt'] = (int) $CL_st_dbt_3 + (int) $st_dbt_3;
                        $data_cl_3['CL_st_bh_cl'] = (int) $CL_st_bh_cl_3 - (int) $st_dbt_3;
                    } else {
                        // Thêm data bảng còn lại
                        $data_cl_3 = array(
                            'CL_quyenloi_bt' => $quyenloi_bt_3,
                            'CL_kh_id' => $kh_id,
                            'CL_st_dbt' => (int) $st_dbt_3,
                            'CL_st_bh_cl' => (int) $st_bh_gh_3 - (int) $st_dbt_3,
                            'date_created' => date('Y-m-d H:i:s'),
                            'time_created' => strtotime(date('Y-m-d')),
                        );
                    }
                    //==========================================================
                }
                $data_cl_2['date_updated'] = date('Y-m-d H:i:s');
                $data_cl_2['time_updated'] = strtotime(date('Y-m-d'));
                //==============================================================
                //==============================================================
                // Update bồi thường
                $post_data = array(
                    'user4' => $this->phpsession->get('user_id'),
                    'order_status' => BT_THANHTOAN,
                    'ghichu_thanhtoan_bt' => $this->input->post('ghichu_thanhtoan_bt', TRUE),
                    'date_thanhtoan' => $datetime_now,
                    'time_thanhtoan' => $time_now,
                    'editor' => $this->phpsession->get('user_id'),
                );

                $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                $post_data['updated_time'] = strtotime(date('Y-m-d'));
                //==============================================================
                //==============================================================
//                echo '<pre>';
//                print_r($data_cl_3);
//                print_r($data_cl_2);
//                print_r($data_cl);
//                print_r($post_data);
//                die;
                //==============================================================
                //==============================================================
                if (isset($data_cl_2) && !empty($data_cl_2)) {
                    if (isset($CL_id_2) && $CL_id_2 > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_id_2, $data_cl_2);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_cl_2);
                    }
                } else {
                    redirect(ORDER_ADMIN_BASE_URL);
                }
                //==============================================================
                //==============================================================
                if (isset($data_cl_3) && !empty($data_cl_3)) {
                    if (isset($CL_id_3) && $CL_id_3 > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_id_3, $data_cl_3);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_cl_3);
                    }
                }
                //==============================================================
                //==============================================================
                // Xử lý qlcl cha
                // Nếu st dbt nhỏ hơn stcl
                if (isset($CL_id) && $CL_id > 0) {
                    // Update data bảng còn lại cha (DTNGT)
                    $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_id, $data_cl);
                } else {
                    // Thêm mới data bảng còn lại cha (DTNGT)
                    $res = $this->orders_model->CommonCreat(TBL_CL, $data_cl);
                }
                //==============================================================
                // Update bồi thường
                $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                //==============================================================
                // Nếu bt là case con
                /*
                 * if ($case_id > 0) {
                  // Lấy bt cha
                  $data_get = array(
                  'BT_id' => $case_id,
                  'get_one' => TRUE,
                  );
                  $bt_t = $this->orders_model->get_orders_by_options($data_get);
                  if (is_object($bt_t)) {
                  $t_st_ycbt_t = $bt_t->st_ycbt_t;
                  $t_st_dbt_t = $bt_t->st_dbt_t;
                  //======================================================
                  $data_update = array(
                  'st_ycbt_t' => $t_st_ycbt_t + $st_ycbt_t,
                  'st_ycbt_t' => $t_st_dbt_t + $st_dbt_t,
                  );
                  $this->orders_model->CommonUpdate(TBL_ORDERS, $case_id, $data_update);
                  }
                  }
                 */

                //==============================================================
                //==============================================================
            } else {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
            //==================================================================
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
    }

    //end
    //==========================================================================
    function do_edit_orders_daduyet_dtnt_ob($options = array()) {
        $data = array();
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $goi_bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $bt_status = $bt->order_status;
            $st_ycbt = $bt->so_tien_ycbt;
            $st_dbt = $bt->so_tien_dbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $sn_dbt = $bt->so_ngay_dbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            //$case_id = $bt->parent_id;
            //$st_ycbt_t = $bt->st_ycbt_t;
            //$st_dbt_t = $bt->st_dbt_t;
            //==================================================================
            if ($bt_status == BT_THANHTOAN) {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
            // Số tiền bh gh tổng (DTNT)
            $st_bh_gh = $st_bh_gh_t = $goi_bh->$quyenloi_bt;
            //==================================================================
            $st_bh_tvpn_n = $goi_bh->dtnt_tvpn;
            $sn_bh_tvpn = $goi_bh->dtnt_tvpn_sn;
            $st_bh_tvpn = (int) $st_bh_tvpn_n * (int) $sn_bh_tvpn;
            $st_bh_tgpn_n = $goi_bh->dtnt_tgpn;
            $sn_bh_tgpn = $goi_bh->dtnt_tgpn_sn;
            $st_bh_tgpn = (int) $st_bh_tgpn_n * (int) $sn_bh_tgpn;
            $st_bh_cppt = $goi_bh->dtnt_cppt;
            $st_bh_tk_nv = $goi_bh->dtnt_tk_nv;
            $st_bh_sk_nv = $goi_bh->dtnt_sk_nv;
            $st_bh_cpyt_tn = $goi_bh->dtnt_cpyt_tn;
            $st_bh_tcvp_n = $goi_bh->dtnt_tcvp;
            $sn_bh_tcvp = $goi_bh->dtnt_tcvp_sn;
            $st_bh_tcvp = (int) $st_bh_tcvp_n * (int) $sn_bh_tcvp;
            $st_bh_tcmt = $goi_bh->dtnt_tcmt;
            $st_bh_xct = $goi_bh->dtnt_xct;
            //==================================================================
            // Lấy qlcl của ql bh cha (DTNT)
            $data_get = array(
                'CL_quyenloi_bt' => $quyenloi_bt,
                'CL_kh_id' => $kh_id,
                'get_row' => TRUE,
            );
            $qlbt_cl = $this->orders_model->get_ql_cl($data_get);
            if (is_object($qlbt_cl)) {
                $CL_dtnt_id = $qlbt_cl->id;
                $CL_st_dbt = $qlbt_cl->CL_st_dbt;
                $CL_st_bh_cl = $qlbt_cl->CL_st_bh_cl;
                $CL_sn_dbt = $qlbt_cl->CL_sn_dbt;
                $CL_sn_bh_cl = $qlbt_cl->CL_sn_bh_cl;
                $slk_bh_dbt = $CL_slk_bh_dbt = $qlbt_cl->CL_slk_dbt;
                $slk_bh_gh = $CL_slk_bh_cl = $qlbt_cl->CL_slk_bh_cl;
                //--------------------------------------------------------------
                $st_bh_gh_t = $st_bh_gh = $CL_st_bh_cl;
                //==============================================================
                $data_dtnt['CL_st_dbt'] = (int) $CL_st_dbt + (int) $st_dbt;
                $data_dtnt['CL_st_bh_cl'] = (int) $CL_st_bh_cl - (int) $st_dbt;
            } else {
                // Thêm data bảng còn lại DTNT
                $data_dtnt = array(
                    'CL_quyenloi_bt' => $quyenloi_bt,
                    'CL_kh_id' => $kh_id,
                    'CL_st_dbt' => (int) $st_dbt,
                    'CL_st_bh_cl' => (int) $st_bh_gh_t - (int) $st_dbt,
                    'date_created' => date('Y-m-d H:i:s'),
                    'time_created' => strtotime(date('Y-m-d')),
                );
            }
            $data_dtnt['date_updated'] = date('Y-m-d H:i:s');
            $data_dtnt['time_updated'] = strtotime(date('Y-m-d'));
            //==================================================================
            $dtnt_tvpn = $this->input->post('dtnt_tvpn', TRUE);
            $dtnt_tgpn = $this->input->post('dtnt_tgpn', TRUE);
            $dtnt_cppt = $this->input->post('dtnt_cppt', TRUE);
            $dtnt_tk_nv = $this->input->post('dtnt_tk_nv', TRUE);
            $dtnt_sk_nv = $this->input->post('dtnt_sk_nv', TRUE);
            $dtnt_cpyt_tn = $this->input->post('dtnt_cpyt_tn', TRUE);
            $dtnt_tcvp = $this->input->post('dtnt_tcvp', TRUE);
            $dtnt_tcmt = $this->input->post('dtnt_tcmt', TRUE);
            $dtnt_xct = $this->input->post('dtnt_xct', TRUE);
            // Nếu còn qlbt tiền viện phí
            if ($dtnt_tvpn == TRUE) {
                $dtnt_tvpn_st_dbt = $this->input->post(dtnt_tvpn . '_st_dbt');
                $dtnt_tvpn_sn_dbt = $this->input->post(dtnt_tvpn . '_sn_dbt');
                //==============================================================
                // Nếu qlbt chọn là Tiền viện phí/năm
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_tvpn,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_tvpn = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_tvpn)) {
                    $CL_tvpn_id = $qlbt_tvpn->id;
                    $CL_tvpn_st_dbt = $qlbt_tvpn->CL_st_dbt;
                    $CL_tvpn_st_bh_cl = $qlbt_tvpn->CL_st_bh_cl;
                    $CL_tvpn_sn_dbt = $qlbt_tvpn->CL_sn_dbt;
                    $CL_tvpn_sn_bh_cl = $qlbt_tvpn->CL_sn_bh_cl;
                    // end
                    $data_tvpn['CL_st_dbt'] = (int) $CL_tvpn_st_dbt + (int) $dtnt_tvpn_st_dbt;
                    $data_tvpn['CL_st_bh_cl'] = (int) $CL_tvpn_st_bh_cl - (int) $dtnt_tvpn_st_dbt;
                    $data_tvpn['CL_sn_dbt'] = (int) $CL_tvpn_sn_dbt + (int) $dtnt_tvpn_sn_dbt;
                    $data_tvpn['CL_sn_bh_cl'] = (int) $CL_tvpn_sn_bh_cl - (int) $dtnt_tvpn_sn_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_tvpn = array(
                        'CL_quyenloi_bt' => dtnt_tvpn,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_tvpn_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_tvpn - (int) $dtnt_tvpn_st_dbt,
                        'CL_sn_dbt' => (int) $dtnt_tvpn_sn_dbt,
                        'CL_sn_bh_cl' => (int) $sn_bh_tvpn - (int) $dtnt_tvpn_sn_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_tvpn_st_dbt = $dtnt_tvpn_sn_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt tiền giường phòng ngày
            if ($dtnt_tgpn == TRUE) {
                $dtnt_tgpn_st_dbt = $this->input->post(dtnt_tgpn . '_st_dbt');
                $dtnt_tgpn_sn_dbt = $this->input->post(dtnt_tgpn . '_sn_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_tgpn,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_tgpn = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_tgpn)) {
                    $CL_tgpn_id = $qlbt_tgpn->id;
                    $CL_tgpn_st_dbt = $qlbt_tgpn->CL_st_dbt;
                    $CL_tgpn_st_bh_cl = $qlbt_tgpn->CL_st_bh_cl;
                    $CL_tgpn_sn_dbt = $qlbt_tgpn->CL_sn_dbt;
                    $CL_tgpn_sn_bh_cl = $qlbt_tgpn->CL_sn_bh_cl;
                    // end
                    $data_tgpn['CL_st_dbt'] = (int) $CL_tgpn_st_dbt + (int) $dtnt_tgpn_st_dbt;
                    $data_tgpn['CL_st_bh_cl'] = (int) $CL_tgpn_st_bh_cl - (int) $dtnt_tgpn_st_dbt;
                    $data_tgpn['CL_sn_dbt'] = (int) $CL_tgpn_sn_dbt + (int) $dtnt_tgpn_sn_dbt;
                    $data_tgpn['CL_sn_bh_cl'] = (int) $CL_tgpn_sn_bh_cl - (int) $dtnt_tgpn_sn_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_tgpn = array(
                        'CL_quyenloi_bt' => dtnt_tgpn,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_tgpn_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_tgpn - (int) $dtnt_tgpn_st_dbt,
                        'CL_sn_dbt' => (int) $dtnt_tgpn_sn_dbt,
                        'CL_sn_bh_cl' => (int) $sn_bh_tgpn - (int) $dtnt_tgpn_sn_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_tgpn_st_dbt = $dtnt_tgpn_sn_dbt = 0;
            }
            //==================================================================
            // Nếu còn qlbt Chi phí phẫu thuật
            if ($dtnt_cppt == TRUE) {
                $dtnt_cppt_st_dbt = $this->input->post(dtnt_cppt . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_cppt,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_cppt = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_cppt)) {
                    $CL_cppt_id = $qlbt_cppt->id;
                    $CL_cppt_st_dbt = $qlbt_cppt->CL_st_dbt;
                    $CL_cppt_st_bh_cl = $qlbt_cppt->CL_st_bh_cl;
                    // end
                    $data_cppt['CL_st_dbt'] = (int) $CL_cppt_st_dbt + (int) $dtnt_cppt_st_dbt;
                    $data_cppt['CL_st_bh_cl'] = (int) $CL_cppt_st_bh_cl - (int) $dtnt_cppt_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_cppt = array(
                        'CL_quyenloi_bt' => dtnt_cppt,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_cppt_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_cppt - (int) $dtnt_cppt_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_cppt_st_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Quyền lợi khám trước khi nhập viện
            if ($dtnt_tk_nv == TRUE) {
                $dtnt_tk_nv_st_dbt = $this->input->post(dtnt_tk_nv . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_tk_nv,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_tk_nv = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_tk_nv)) {
                    $CL_tk_nv_id = $qlbt_tk_nv->id;
                    $CL_tk_nv_st_dbt = $qlbt_tk_nv->CL_st_dbt;
                    $CL_tk_nv_st_bh_cl = $qlbt_tk_nv->CL_st_bh_cl;
                    // end
                    $data_tk_nv['CL_st_dbt'] = (int) $CL_tk_nv_st_dbt + (int) $dtnt_tk_nv_st_dbt;
                    $data_tk_nv['CL_st_bh_cl'] = (int) $CL_tk_nv_st_bh_cl - (int) $dtnt_tk_nv_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_tk_nv = array(
                        'CL_quyenloi_bt' => dtnt_tk_nv,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_tk_nv_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_tk_nv - (int) $dtnt_tk_nv_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_tk_nv_st_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Quyền lợi khám sau khi nhập viện
            if ($dtnt_sk_nv == TRUE) {
                $dtnt_sk_nv_st_dbt = $this->input->post(dtnt_sk_nv . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_sk_nv,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_sk_nv = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_sk_nv)) {
                    $CL_sk_nv_id = $qlbt_sk_nv->id;
                    $CL_sk_nv_st_dbt = $qlbt_sk_nv->CL_st_dbt;
                    $CL_sk_nv_st_bh_cl = $qlbt_sk_nv->CL_st_bh_cl;
                    // end
                    $data_sk_nv['CL_st_dbt'] = (int) $CL_sk_nv_st_dbt + (int) $dtnt_sk_nv_st_dbt;
                    $data_sk_nv['CL_st_bh_cl'] = (int) $CL_sk_nv_st_bh_cl - (int) $dtnt_sk_nv_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_sk_nv = array(
                        'CL_quyenloi_bt' => dtnt_sk_nv,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_sk_nv_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_sk_nv - (int) $dtnt_sk_nv_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_sk_nv_st_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Chi phí y tế tại nhà
            if ($dtnt_cpyt_tn == TRUE) {
                $dtnt_cpyt_tn_st_dbt = $this->input->post(dtnt_cpyt_tn . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_cpyt_tn,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_cpyt_tn = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_cpyt_tn)) {
                    $CL_cpyt_tn_id = $qlbt_cpyt_tn->id;
                    $CL_cpyt_tn_st_dbt = $qlbt_cpyt_tn->CL_st_dbt;
                    $CL_cpyt_tn_st_bh_cl = $qlbt_cpyt_tn->CL_st_bh_cl;
                    // end
                    $data_cpyt_tn['CL_st_dbt'] = (int) $CL_cpyt_tn_st_dbt + (int) $dtnt_cpyt_tn_st_dbt;
                    $data_cpyt_tn['CL_st_bh_cl'] = (int) $CL_cpyt_tn_st_bh_cl - (int) $dtnt_cpyt_tn_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_cpyt_tn = array(
                        'CL_quyenloi_bt' => dtnt_cpyt_tn,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_cpyt_tn_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_cpyt_tn - (int) $dtnt_cpyt_tn_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_cpyt_tn_st_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Trợ cấp viện phí
            if ($dtnt_tcvp == TRUE) {
                $dtnt_tcvp_st_dbt = $this->input->post(dtnt_tcvp . '_st_dbt');
                $dtnt_tcvp_sn_dbt = $this->input->post(dtnt_tcvp . '_sn_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_tcvp,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_tcvp = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_tcvp)) {
                    $CL_tcvp_id = $qlbt_tcvp->id;
                    $CL_tcvp_st_dbt = $qlbt_tcvp->CL_st_dbt;
                    $CL_tcvp_st_bh_cl = $qlbt_tcvp->CL_st_bh_cl;
                    $CL_tcvp_sn_dbt = $qlbt_tcvp->CL_sn_dbt;
                    $CL_tcvp_sn_bh_cl = $qlbt_tcvp->CL_sn_bh_cl;
                    // end
                    $data_tcvp['CL_st_dbt'] = (int) $CL_tcvp_st_dbt + (int) $dtnt_tcvp_st_dbt;
                    $data_tcvp['CL_st_bh_cl'] = (int) $CL_tcvp_st_bh_cl - (int) $dtnt_tcvp_st_dbt;
                    $data_tcvp['CL_sn_dbt'] = (int) $CL_tcvp_sn_dbt + (int) $dtnt_tcvp_sn_dbt;
                    $data_tcvp['CL_sn_bh_cl'] = (int) $CL_tcvp_sn_bh_cl - (int) $dtnt_tcvp_sn_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_tcvp = array(
                        'CL_quyenloi_bt' => dtnt_tcvp,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_tcvp_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_tcvp - (int) $dtnt_tcvp_st_dbt,
                        'CL_sn_dbt' => (int) $dtnt_tcvp_sn_dbt,
                        'CL_sn_bh_cl' => (int) $sn_bh_tcvp - (int) $dtnt_tcvp_sn_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_tcvp_st_dbt = $dtnt_tcvp_sn_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Trợ cấp mai táng
            if ($dtnt_tcmt == TRUE) {
                $dtnt_tcmt_st_dbt = $this->input->post(dtnt_tcmt . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_tcmt,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_tcmt = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_tcmt)) {
                    $CL_tcmt_id = $qlbt_tcmt->id;
                    $CL_tcmt_st_dbt = $qlbt_tcmt->CL_st_dbt;
                    $CL_tcmt_st_bh_cl = $qlbt_tcmt->CL_st_bh_cl;
                    // end
                    $data_tcmt['CL_st_dbt'] = (int) $CL_tcmt_st_dbt + (int) $dtnt_tcmt_st_dbt;
                    $data_tcmt['CL_st_bh_cl'] = (int) $CL_tcmt_st_bh_cl - (int) $dtnt_tcmt_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_tcmt = array(
                        'CL_quyenloi_bt' => dtnt_tcmt,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_tcmt_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_tcmt - (int) $dtnt_tcmt_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_tcmt_st_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Xe cứu thương
            if ($dtnt_xct == TRUE) {
                $dtnt_xct_st_dbt = $this->input->post(dtnt_xct . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_xct,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_xct = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_xct)) {
                    $CL_xct_id = $qlbt_xct->id;
                    $CL_xct_st_dbt = $qlbt_xct->CL_st_dbt;
                    $CL_xct_st_bh_cl = $qlbt_xct->CL_st_bh_cl;
                    // end
                    $data_xct['CL_st_dbt'] = (int) $CL_xct_st_dbt + (int) $dtnt_xct_st_dbt;
                    $data_xct['CL_st_bh_cl'] = (int) $CL_xct_st_bh_cl - (int) $dtnt_xct_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_xct = array(
                        'CL_quyenloi_bt' => dtnt_xct,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_xct_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_xct - (int) $dtnt_xct_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_xct_st_dbt = 0;
            }
            //==================================================================
            //==================================================================
            // Nếu tổng số tiền bồi thường nhỏ hơn số tiền còn lại
            $so_tien_dbt_t = $dtnt_tvpn_st_dbt + $dtnt_tgpn_st_dbt + $dtnt_cppt_st_dbt + $dtnt_tk_nv_st_dbt + $dtnt_sk_nv_st_dbt +
                    $dtnt_cpty_tn_st_dbt + $dtnt_tcvp_st_dbt + $dtnt_tcmt_st_dbt + $dtnt_xct_st_dbt;
            if ((int) $so_tien_dbt_t < $st_bh_gh_t) {

//                echo '<pre>';
//                print_r($data_dtnt);
//                print_r($data_tvpn);
//                print_r($data_tgpn);
//                print_r($data_cppt);
//                print_r($data_tk_nv);
//                print_r($data_sk_nv);
//                print_r($data_cpyt_tn);
//                print_r($data_tcvp);
//                print_r($data_tcmt);
//                print_r($data_xct);
//                print_r($post_data);
//                die;
                // Update qlbt
                if (isset($data_dtnt) && !empty($data_dtnt)) {
                    if (isset($CL_dtnt_id) && $CL_dtnt_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_dtnt_id, $data_dtnt);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_dtnt);
                    }
                }
                //==============================================================
                if (isset($data_tvpn) && !empty($data_tvpn)) {
                    if (isset($CL_tvpn_id) && $CL_tvpn_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tvpn_id, $data_tvpn);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tvpn);
                    }
                }
                //==============================================================
                if (isset($data_tgpn) && !empty($data_tgpn)) {
                    if (isset($CL_tgpn_id) && $CL_tgpn_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tgpn_id, $data_tgpn);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tgpn);
                    }
                }
                //==============================================================
                if (isset($data_cppt) && !empty($data_cppt)) {
                    if (isset($CL_cppt_id) && $CL_cppt_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_cppt_id, $data_cppt);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_cppt);
                    }
                }
                //==============================================================
                if (isset($data_tk_nv) && !empty($data_tk_nv)) {
                    if (isset($CL_tk_nv_id) && $CL_tk_nv_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tk_nv_id, $data_tk_nv);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tk_nv);
                    }
                }
                //==============================================================
                if (isset($data_sk_nv) && !empty($data_sk_nv)) {
                    if (isset($CL_sk_nv_id) && $CL_sk_nv_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_sk_nv_id, $data_sk_nv);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_sk_nv);
                    }
                }
                //==============================================================
                if (isset($data_cpyt_tn) && !empty($data_cpyt_tn)) {
                    if (isset($CL_cpyt_tn_id) && $CL_cpyt_tn_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_cpyt_tn_id, $data_cpyt_tn);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_cpyt_tn);
                    }
                }
                //==============================================================
                if (isset($data_tcvp) && !empty($data_tcvp)) {
                    if (isset($CL_tcvp_id) && $CL_tcvp_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tcvp_id, $data_tcvp);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tcvp);
                    }
                }
                //==============================================================
                if (isset($data_tcmt) && !empty($data_tcmt)) {
                    if (isset($CL_tcmt_id) && $CL_tcmt_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tcmt_id, $data_tcmt);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tcmt);
                    }
                }
                //==============================================================
                if (isset($data_xct) && !empty($data_xct)) {
                    if (isset($CL_xct_id) && $CL_xct_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_xct_id, $data_xct);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_xct);
                    }
                }
                //==============================================================
                // Update bồi thường
                $post_data = array(
                    'user4' => $this->phpsession->get('user_id'),
                    'order_status' => BT_THANHTOAN,
                    'ghichu_thanhtoan_bt' => $this->input->post('ghichu_thanhtoan_bt', TRUE),
                    'date_thanhtoan' => $datetime_now,
                    'time_thanhtoan' => $time_now,
                    'editor' => $this->phpsession->get('user_id'),
                );

                $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                $post_data['updated_time'] = strtotime(date('Y-m-d'));
                //==============================================================
                $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                // Nếu bt là case con
                /*
                 * if ($case_id > 0) {
                  // Lấy bt cha
                  $data_get = array(
                  'BT_id' => $case_id,
                  'get_one' => TRUE,
                  );
                  $bt_t = $this->orders_model->get_orders_by_options($data_get);
                  if (is_object($bt_t)) {
                  $t_st_ycbt_t = $bt_t->st_ycbt_t;
                  $t_st_dbt_t = $bt_t->st_dbt_t;
                  //======================================================
                  $data_update = array(
                  'st_ycbt_t' => $t_st_ycbt_t + $st_ycbt_t,
                  'st_ycbt_t' => $t_st_dbt_t + $st_dbt_t,
                  );
                  $this->orders_model->CommonUpdate(TBL_ORDERS, $case_id, $data_update);
                  }
                  }
                 */

                //==============================================================
                redirect(ORDER_ADMIN_BASE_URL);
            } else {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
    }

    //end
    //==========================================================================
    function do_edit_orders_daduyet_tsqlnt($options = array()) {
        $data = array();
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        if (isset($options['bt']) && is_object($options['bt']) && isset($options['goi_bh']) && is_object($options['goi_bh'])) {
            $bt = $options['bt'];
            $goi_bh = $options['goi_bh'];
            $bt_id = $bt->id;
            $kh_id = $bt->OD_kh_dbh;
            $bt_status = $bt->order_status;
            $st_ycbt = $bt->so_tien_ycbt;
            $st_dbt = $bt->so_tien_dbt;
            $sn_ycbt = $bt->so_ngay_ycbt;
            $sn_dbt = $bt->so_ngay_dbt;
            $quyenloi_bt = $bt->quyenloi_bt;
            $phanloai_thaisan = $bt->phanloai_thaisan;
            //$case_id = $bt->parent_id;
            //$st_ycbt_t = $bt->st_ycbt_t;
            //$st_dbt_t = $bt->st_dbt_t;
            //==================================================================
            if ($bt_status == BT_THANHTOAN) {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
            // Check phân loại thai sản và st còn lại của ql sinh thường hoặc sinh mổ
            $loai_thaisan = get_field_thaisan($phanloai_thaisan);
            $st_tsqlnt_ss = $goi_bh->$loai_thaisan;
            //==================================================================
            // Số tiền bh gh tổng THAISAN QLNT
            $st_tsqlnt_bh_gh = $goi_bh->$quyenloi_bt;
            //==================================================================
            // Số tiền bh gh tổng (DTNT)
            $filed_dtnt = DTNT_OB;
            $st_dtnt_bh_gh = $goi_bh->$filed_dtnt;
            //==================================================================
            $st_bh_tvpn_n = $goi_bh->dtnt_tvpn;
            $sn_bh_tvpn = $goi_bh->dtnt_tvpn_sn;
            $st_bh_tvpn = (int) $st_bh_tvpn_n * (int) $sn_bh_tvpn;
            $st_bh_tgpn_n = $goi_bh->dtnt_tgpn;
            $sn_bh_tgpn = $goi_bh->dtnt_tgpn_sn;
            $st_bh_tgpn = (int) $st_bh_tgpn_n * (int) $sn_bh_tgpn;
            $st_bh_cppt = $goi_bh->dtnt_cppt;
            $st_bh_tk_nv = $goi_bh->dtnt_tk_nv;
            $st_bh_sk_nv = $goi_bh->dtnt_sk_nv;
            $st_bh_cpyt_tn = $goi_bh->dtnt_cpyt_tn;
            $st_bh_tcvp_n = $goi_bh->dtnt_tcvp;
            $sn_bh_tcvp = $goi_bh->dtnt_tcvp_sn;
            $st_bh_tcvp = (int) $st_bh_tcvp_n * (int) $sn_bh_tcvp;
            $st_bh_tcmt = $goi_bh->dtnt_tcmt;
            $st_bh_xct = $goi_bh->dtnt_xct;
            $st_bh_ktdk = $goi_bh->tsqlnt_ktdk;
            $st_bh_dn = $goi_bh->tsqlnt_dn;
            //==================================================================
            // Lấy qlcl của ql bh cha THAISAN QLNT
            $data_get = array(
                'CL_quyenloi_bt' => $quyenloi_bt,
                'CL_kh_id' => $kh_id,
                'get_row' => TRUE,
            );
            $qlbt_cl = $this->orders_model->get_ql_cl($data_get);
            if (is_object($qlbt_cl)) {
                $CL_tsqlnt_id = $qlbt_cl->id;
                $CL_st_dbt = $qlbt_cl->CL_st_dbt;
                $CL_st_bh_cl = $qlbt_cl->CL_st_bh_cl;
                //--------------------------------------------------------------
                $st_tsqlnt_bh_gh = $CL_st_bh_cl;
                //==============================================================
                $data_tsqlnt['CL_st_dbt'] = (int) $CL_st_dbt + (int) $st_dbt;
                $data_tsqlnt['CL_st_bh_cl'] = (int) $CL_st_bh_cl - (int) $st_dbt;
            } else {
                // Thêm data bảng còn lại THAISAN QLNT
                $data_tsqlnt = array(
                    'CL_quyenloi_bt' => $quyenloi_bt,
                    'CL_kh_id' => $kh_id,
                    'CL_st_dbt' => (int) $st_dbt,
                    'CL_st_bh_cl' => (int) $st_tsqlnt_bh_gh - (int) $st_dbt,
                    'date_created' => date('Y-m-d H:i:s'),
                    'time_created' => strtotime(date('Y-m-d')),
                );
            }
            $data_tsqlnt['date_updated'] = date('Y-m-d H:i:s');
            $data_tsqlnt['time_updated'] = strtotime(date('Y-m-d'));
            //==================================================================
            // Lấy qlcl của ql bh cha DTNT
            $data_get = array(
                'CL_quyenloi_bt' => $filed_dtnt,
                'CL_kh_id' => $kh_id,
                'get_row' => TRUE,
            );
            $qlbt_cl = $this->orders_model->get_ql_cl($data_get);
            if (is_object($qlbt_cl)) {
                $CL_dtnt_id = $qlbt_cl->id;
                $CL_st_dbt = $qlbt_cl->CL_st_dbt;
                $CL_st_bh_cl = $qlbt_cl->CL_st_bh_cl;
                //--------------------------------------------------------------
                $st_dtnt_bh_gh = $CL_st_bh_cl;
                //==============================================================
                $data_dtnt['CL_st_dbt'] = (int) $CL_st_dbt + (int) $st_dbt;
                $data_dtnt['CL_st_bh_cl'] = (int) $CL_st_bh_cl - (int) $st_dbt;
            } else {
                // Thêm data bảng còn lại DTNT
                $data_dtnt = array(
                    'CL_quyenloi_bt' => $filed_dtnt,
                    'CL_kh_id' => $kh_id,
                    'CL_st_dbt' => (int) $st_dbt,
                    'CL_st_bh_cl' => (int) $st_dtnt_bh_gh - (int) $st_dbt,
                    'date_created' => date('Y-m-d H:i:s'),
                    'time_created' => strtotime(date('Y-m-d')),
                );
            }
            $data_dtnt['date_updated'] = date('Y-m-d H:i:s');
            $data_dtnt['time_updated'] = strtotime(date('Y-m-d'));
            //==================================================================
            // Lấy qlcl của ql sinh thường hoặc sinh mổ
            $data_get = array(
                'CL_quyenloi_bt' => $loai_thaisan,
                'CL_kh_id' => $kh_id,
                'get_row' => TRUE,
            );
            $qlbt_cl = $this->orders_model->get_ql_cl($data_get);
            if (is_object($qlbt_cl)) {
                $CL_ss_id = $qlbt_cl->id;
                $CL_st_dbt = $qlbt_cl->CL_st_dbt;
                $CL_st_bh_cl = $qlbt_cl->CL_st_bh_cl;
                //--------------------------------------------------------------
                $st_tsqlnt_ss = $CL_st_bh_cl;
                //==============================================================
                $data_ss['CL_st_dbt'] = (int) $CL_st_dbt + (int) $st_dbt;
                $data_ss['CL_st_bh_cl'] = (int) $CL_st_bh_cl - (int) $st_dbt;
            } else {
                // Thêm data bảng còn lại ql sinh thường hoặc sinh mổ
                $data_ss = array(
                    'CL_quyenloi_bt' => $loai_thaisan,
                    'CL_kh_id' => $kh_id,
                    'CL_st_dbt' => (int) $st_dbt,
                    'CL_st_bh_cl' => (int) $st_tsqlnt_ss - (int) $st_dbt,
                    'date_created' => date('Y-m-d H:i:s'),
                    'time_created' => strtotime(date('Y-m-d')),
                );
            }
            $data_ss['date_updated'] = date('Y-m-d H:i:s');
            $data_ss['time_updated'] = strtotime(date('Y-m-d'));
            //==================================================================
            // Lấy số tiền tổng có thể bồi thường
            $a_st_bh_gh = array($st_tsqlnt_ss, $st_tsqlnt_bh_gh, $st_dtnt_bh_gh);
            $st_bh_gh_max = min($a_st_bh_gh);
            //==================================================================
            //==================================================================
            $dtnt_tvpn = $this->input->post('dtnt_tvpn', TRUE);
            $dtnt_tgpn = $this->input->post('dtnt_tgpn', TRUE);
            $dtnt_cppt = $this->input->post('dtnt_cppt', TRUE);
            $dtnt_tk_nv = $this->input->post('dtnt_tk_nv', TRUE);
            $dtnt_sk_nv = $this->input->post('dtnt_sk_nv', TRUE);
            $dtnt_cpyt_tn = $this->input->post('dtnt_cpyt_tn', TRUE);
            $dtnt_tcvp = $this->input->post('dtnt_tcvp', TRUE);
            $dtnt_tcmt = $this->input->post('dtnt_tcmt', TRUE);
            $dtnt_xct = $this->input->post('dtnt_xct', TRUE);
            $tsqlnt_ktdk = $this->input->post('tsqlnt_ktdk', TRUE);
            $tsqlnt_dn = $this->input->post('tsqlnt_dn', TRUE);
            // Nếu còn qlbt tiền viện phí
            if ($dtnt_tvpn == TRUE) {
                $dtnt_tvpn_st_dbt = $this->input->post(dtnt_tvpn . '_st_dbt');
                $dtnt_tvpn_sn_dbt = $this->input->post(dtnt_tvpn . '_sn_dbt');
                //==============================================================
                // Nếu qlbt chọn là Tiền viện phí/năm
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_tvpn,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_tvpn = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_tvpn)) {
                    $CL_tvpn_id = $qlbt_tvpn->id;
                    $CL_tvpn_st_dbt = $qlbt_tvpn->CL_st_dbt;
                    $CL_tvpn_st_bh_cl = $qlbt_tvpn->CL_st_bh_cl;
                    $CL_tvpn_sn_dbt = $qlbt_tvpn->CL_sn_dbt;
                    $CL_tvpn_sn_bh_cl = $qlbt_tvpn->CL_sn_bh_cl;
                    // end
                    $data_tvpn['CL_st_dbt'] = (int) $CL_tvpn_st_dbt + (int) $dtnt_tvpn_st_dbt;
                    $data_tvpn['CL_st_bh_cl'] = (int) $CL_tvpn_st_bh_cl - (int) $dtnt_tvpn_st_dbt;
                    $data_tvpn['CL_sn_dbt'] = (int) $CL_tvpn_sn_dbt + (int) $dtnt_tvpn_sn_dbt;
                    $data_tvpn['CL_sn_bh_cl'] = (int) $CL_tvpn_sn_bh_cl - (int) $dtnt_tvpn_sn_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_tvpn = array(
                        'CL_quyenloi_bt' => dtnt_tvpn,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_tvpn_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_tvpn - (int) $dtnt_tvpn_st_dbt,
                        'CL_sn_dbt' => (int) $dtnt_tvpn_sn_dbt,
                        'CL_sn_bh_cl' => (int) $sn_bh_tvpn - (int) $dtnt_tvpn_sn_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_tvpn_st_dbt = $dtnt_tvpn_sn_dbt = 0;
            }
//            echo '<pre>';
//            print_r($data_tvpn);
//            die;
            //======================================================
            // Nếu còn qlbt tiền giường phòng ngày
            if ($dtnt_tgpn == TRUE) {
                $dtnt_tgpn_st_dbt = $this->input->post(dtnt_tgpn . '_st_dbt');
                $dtnt_tgpn_sn_dbt = $this->input->post(dtnt_tgpn . '_sn_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_tgpn,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_tgpn = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_tgpn)) {
                    $CL_tgpn_id = $qlbt_tgpn->id;
                    $CL_tgpn_st_dbt = $qlbt_tgpn->CL_st_dbt;
                    $CL_tgpn_st_bh_cl = $qlbt_tgpn->CL_st_bh_cl;
                    $CL_tgpn_sn_dbt = $qlbt_tgpn->CL_sn_dbt;
                    $CL_tgpn_sn_bh_cl = $qlbt_tgpn->CL_sn_bh_cl;
                    // end
                    $data_tgpn['CL_st_dbt'] = (int) $CL_tgpn_st_dbt + (int) $dtnt_tgpn_st_dbt;
                    $data_tgpn['CL_st_bh_cl'] = (int) $CL_tgpn_st_bh_cl - (int) $dtnt_tgpn_st_dbt;
                    $data_tgpn['CL_sn_dbt'] = (int) $CL_tgpn_sn_dbt + (int) $dtnt_tgpn_sn_dbt;
                    $data_tgpn['CL_sn_bh_cl'] = (int) $CL_tgpn_sn_bh_cl - (int) $dtnt_tgpn_sn_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_tgpn = array(
                        'CL_quyenloi_bt' => dtnt_tgpn,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_tgpn_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_tgpn - (int) $dtnt_tgpn_st_dbt,
                        'CL_sn_dbt' => (int) $dtnt_tgpn_sn_dbt,
                        'CL_sn_bh_cl' => (int) $sn_bh_tgpn - (int) $dtnt_tgpn_sn_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_tgpn_st_dbt = $dtnt_tgpn_sn_dbt = 0;
            }
            //==================================================================
            // Nếu còn qlbt Chi phí phẫu thuật
            if ($dtnt_cppt == TRUE) {
                $dtnt_cppt_st_dbt = $this->input->post(dtnt_cppt . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_cppt,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_cppt = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_cppt)) {
                    $CL_cppt_id = $qlbt_cppt->id;
                    $CL_cppt_st_dbt = $qlbt_cppt->CL_st_dbt;
                    $CL_cppt_st_bh_cl = $qlbt_cppt->CL_st_bh_cl;
                    // end
                    $data_cppt['CL_st_dbt'] = (int) $CL_cppt_st_dbt + (int) $dtnt_cppt_st_dbt;
                    $data_cppt['CL_st_bh_cl'] = (int) $CL_cppt_st_bh_cl - (int) $dtnt_cppt_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_cppt = array(
                        'CL_quyenloi_bt' => dtnt_cppt,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_cppt_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_cppt - (int) $dtnt_cppt_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_cppt_st_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Quyền lợi khám trước khi nhập viện
            if ($dtnt_tk_nv == TRUE) {
                $dtnt_tk_nv_st_dbt = $this->input->post(dtnt_tk_nv . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_tk_nv,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_tk_nv = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_tk_nv)) {
                    $CL_tk_nv_id = $qlbt_tk_nv->id;
                    $CL_tk_nv_st_dbt = $qlbt_tk_nv->CL_st_dbt;
                    $CL_tk_nv_st_bh_cl = $qlbt_tk_nv->CL_st_bh_cl;
                    // end
                    $data_tk_nv['CL_st_dbt'] = (int) $CL_tk_nv_st_dbt + (int) $dtnt_tk_nv_st_dbt;
                    $data_tk_nv['CL_st_bh_cl'] = (int) $CL_tk_nv_st_bh_cl - (int) $dtnt_tk_nv_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_tk_nv = array(
                        'CL_quyenloi_bt' => dtnt_tk_nv,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_tk_nv_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_tk_nv - (int) $dtnt_tk_nv_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_tk_nv_st_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Quyền lợi khám sau khi nhập viện
            if ($dtnt_sk_nv == TRUE) {
                $dtnt_sk_nv_st_dbt = $this->input->post(dtnt_sk_nv . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_sk_nv,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_sk_nv = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_sk_nv)) {
                    $CL_sk_nv_id = $qlbt_sk_nv->id;
                    $CL_sk_nv_st_dbt = $qlbt_sk_nv->CL_st_dbt;
                    $CL_sk_nv_st_bh_cl = $qlbt_sk_nv->CL_st_bh_cl;
                    // end
                    $data_sk_nv['CL_st_dbt'] = (int) $CL_sk_nv_st_dbt + (int) $dtnt_sk_nv_st_dbt;
                    $data_sk_nv['CL_st_bh_cl'] = (int) $CL_sk_nv_st_bh_cl - (int) $dtnt_sk_nv_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_sk_nv = array(
                        'CL_quyenloi_bt' => dtnt_sk_nv,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_sk_nv_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_sk_nv - (int) $dtnt_sk_nv_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_sk_nv_st_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Chi phí y tế tại nhà
            if ($dtnt_cpyt_tn == TRUE) {
                $dtnt_cpyt_tn_st_dbt = $this->input->post(dtnt_cpyt_tn . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_cpyt_tn,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_cpyt_tn = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_cpyt_tn)) {
                    $CL_cpyt_tn_id = $qlbt_cpyt_tn->id;
                    $CL_cpyt_tn_st_dbt = $qlbt_cpyt_tn->CL_st_dbt;
                    $CL_cpyt_tn_st_bh_cl = $qlbt_cpyt_tn->CL_st_bh_cl;
                    // end
                    $data_cpyt_tn['CL_st_dbt'] = (int) $CL_cpyt_tn_st_dbt + (int) $dtnt_cpyt_tn_st_dbt;
                    $data_cpyt_tn['CL_st_bh_cl'] = (int) $CL_cpyt_tn_st_bh_cl - (int) $dtnt_cpyt_tn_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_cpyt_tn = array(
                        'CL_quyenloi_bt' => dtnt_cpyt_tn,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_cpyt_tn_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_cpyt_tn - (int) $dtnt_cpyt_tn_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_cpyt_tn_st_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Trợ cấp viện phí
            if ($dtnt_tcvp == TRUE) {
                $dtnt_tcvp_st_dbt = $this->input->post(dtnt_tcvp . '_st_dbt');
                $dtnt_tcvp_sn_dbt = $this->input->post(dtnt_tcvp . '_sn_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_tcvp,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_tcvp = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_tcvp)) {
                    $CL_tcvp_id = $qlbt_tcvp->id;
                    $CL_tcvp_st_dbt = $qlbt_tcvp->CL_st_dbt;
                    $CL_tcvp_st_bh_cl = $qlbt_tcvp->CL_st_bh_cl;
                    $CL_tcvp_sn_dbt = $qlbt_tcvp->CL_sn_dbt;
                    $CL_tcvp_sn_bh_cl = $qlbt_tcvp->CL_sn_bh_cl;
                    // end
                    $data_tcvp['CL_st_dbt'] = (int) $CL_tcvp_st_dbt + (int) $dtnt_tcvp_st_dbt;
                    $data_tcvp['CL_st_bh_cl'] = (int) $CL_tcvp_st_bh_cl - (int) $dtnt_tcvp_st_dbt;
                    $data_tcvp['CL_sn_dbt'] = (int) $CL_tcvp_sn_dbt + (int) $dtnt_tcvp_sn_dbt;
                    $data_tcvp['CL_sn_bh_cl'] = (int) $CL_tcvp_sn_bh_cl - (int) $dtnt_tcvp_sn_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_tcvp = array(
                        'CL_quyenloi_bt' => dtnt_tcvp,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_tcvp_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_tcvp - (int) $dtnt_tcvp_st_dbt,
                        'CL_sn_dbt' => (int) $dtnt_tcvp_sn_dbt,
                        'CL_sn_bh_cl' => (int) $sn_bh_tcvp - (int) $dtnt_tcvp_sn_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_tcvp_st_dbt = $dtnt_tcvp_sn_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Trợ cấp mai táng
            if ($dtnt_tcmt == TRUE) {
                $dtnt_tcmt_st_dbt = $this->input->post(dtnt_tcmt . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_tcmt,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_tcmt = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_tcmt)) {
                    $CL_tcmt_id = $qlbt_tcmt->id;
                    $CL_tcmt_st_dbt = $qlbt_tcmt->CL_st_dbt;
                    $CL_tcmt_st_bh_cl = $qlbt_tcmt->CL_st_bh_cl;
                    // end
                    $data_tcmt['CL_st_dbt'] = (int) $CL_tcmt_st_dbt + (int) $dtnt_tcmt_st_dbt;
                    $data_tcmt['CL_st_bh_cl'] = (int) $CL_tcmt_st_bh_cl - (int) $dtnt_tcmt_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_tcmt = array(
                        'CL_quyenloi_bt' => dtnt_tcmt,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_tcmt_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_tcmt - (int) $dtnt_tcmt_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_tcmt_st_dbt = 0;
            }
            //======================================================
            // Nếu còn qlbt Xe cứu thương
            if ($dtnt_xct == TRUE) {
                $dtnt_xct_st_dbt = $this->input->post(dtnt_xct . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => dtnt_xct,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_xct = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_xct)) {
                    $CL_xct_id = $qlbt_xct->id;
                    $CL_xct_st_dbt = $qlbt_xct->CL_st_dbt;
                    $CL_xct_st_bh_cl = $qlbt_xct->CL_st_bh_cl;
                    // end
                    $data_xct['CL_st_dbt'] = (int) $CL_xct_st_dbt + (int) $dtnt_xct_st_dbt;
                    $data_xct['CL_st_bh_cl'] = (int) $CL_xct_st_bh_cl - (int) $dtnt_xct_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_xct = array(
                        'CL_quyenloi_bt' => dtnt_xct,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $dtnt_xct_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_xct - (int) $dtnt_xct_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $dtnt_xct_st_dbt = 0;
            }
            //==================================================================
            //==================================================================
            // Nếu còn qlbt khám thai định kỳ
            if ($tsqlnt_ktdk == TRUE) {
                $tsqlnt_ktdk_st_dbt = $this->input->post(tsqlnt_ktdk . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => tsqlnt_ktdk,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_ktdk = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_ktdk)) {
                    $CL_ktdk_id = $qlbt_ktdk->id;
                    $CL_ktdk_st_dbt = $qlbt_ktdk->CL_st_dbt;
                    $CL_ktdk_st_bh_cl = $qlbt_ktdk->CL_st_bh_cl;
                    // end
                    $data_ktdk['CL_st_dbt'] = (int) $CL_ktdk_st_dbt + (int) $tsqlnt_ktdk_st_dbt;
                    $data_ktdk['CL_st_bh_cl'] = (int) $CL_ktdk_st_bh_cl - (int) $tsqlnt_ktdk_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_ktdk = array(
                        'CL_quyenloi_bt' => tsqlnt_ktdk,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $tsqlnt_ktdk_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_ktdk - (int) $tsqlnt_ktdk_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $tsqlnt_ktdk_st_dbt = 0;
            }
            //==================================================================
            // Nếu còn qlbt dưỡng nhi
            if ($tsqlnt_dn == TRUE) {
                $tsqlnt_dn_st_dbt = $this->input->post(tsqlnt_dn . '_st_dbt');
                //==============================================================
                // Lấy data qlcl
                $data_get = array(
                    'CL_quyenloi_bt' => tsqlnt_dn,
                    'CL_kh_id' => $kh_id,
                    'get_row' => TRUE,
                );
                $qlbt_dn = $this->orders_model->get_ql_cl($data_get);
                if (is_object($qlbt_dn)) {
                    $CL_dn_id = $qlbt_dn->id;
                    $CL_dn_st_dbt = $qlbt_dn->CL_st_dbt;
                    $CL_dn_st_bh_cl = $qlbt_dn->CL_st_bh_cl;
                    // end
                    $data_dn['CL_st_dbt'] = (int) $CL_dn_st_dbt + (int) $tsqlnt_dn_st_dbt;
                    $data_dn['CL_st_bh_cl'] = (int) $CL_dn_st_bh_cl - (int) $tsqlnt_dn_st_dbt;
                } else {
                    // Thêm data bảng còn lại
                    $data_dn = array(
                        'CL_quyenloi_bt' => tsqlnt_dn,
                        'CL_kh_id' => $kh_id,
                        'CL_st_dbt' => (int) $tsqlnt_dn_st_dbt,
                        'CL_st_bh_cl' => (int) $st_bh_dn - (int) $tsqlnt_dn_st_dbt,
                        'date_created' => date('Y-m-d H:i:s'),
                        'time_created' => strtotime(date('Y-m-d')),
                    );
                }
            } else {
                $tsqlnt_dn_st_dbt = 0;
            }
            //==================================================================
            //==================================================================
            // Nếu tổng số tiền bồi thường nhỏ hơn số tiền còn lại
            $so_tien_dbt_t = $dtnt_tvpn_st_dbt + $dtnt_tgpn_st_dbt + $dtnt_cppt_st_dbt + $dtnt_tk_nv_st_dbt + $dtnt_sk_nv_st_dbt +
                    $dtnt_cpty_tn_st_dbt + $dtnt_tcvp_st_dbt + $dtnt_tcmt_st_dbt + $dtnt_xct_st_dbt +
                    $tsqlnt_ktdk_st_dbt + $tsqlnt_dn_st_dbt;
//            echo $so_tien_dbt_t;
//            die;
            if ((int) $so_tien_dbt_t < $st_tsqlnt_bh_gh) {

//                echo '<pre>';
//                print_r($data_dtnt);
//                print_r($data_tvpn);
//                print_r($data_tgpn);
//                print_r($data_cppt);
//                print_r($data_tk_nv);
//                print_r($data_sk_nv);
//                print_r($data_cpyt_tn);
//                print_r($data_tcvp);
//                print_r($data_tcmt);
//                print_r($data_xct);
//                print_r($data_ktdk);
//                print_r($data_dn);
//                print_r($data_tsqlnt);
//                print_r($data_ss);
//                print_r($post_data);
//                die;
                // Update qlbt
                if (isset($data_dtnt) && !empty($data_dtnt)) {
                    if (isset($CL_dtnt_id) && $CL_dtnt_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_dtnt_id, $data_dtnt);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_dtnt);
                    }
                }
                //==============================================================
                // Update THAISAN QLNT
                if (isset($data_tsqlnt) && !empty($data_tsqlnt)) {
                    if (isset($CL_tsqlnt_id) && $CL_tsqlnt_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tsqlnt_id, $data_tsqlnt);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tsqlnt);
                    }
                }
                //==============================================================
                // Update THAISAN QLNT
                if (isset($data_tsqlnt) && !empty($data_tsqlnt)) {
                    if (isset($CL_tsqlnt_id) && $CL_tsqlnt_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tsqlnt_id, $data_tsqlnt);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tsqlnt);
                    }
                }
                //==============================================================
                // Update ql sinh được chọn: sinh thường hay sinh mổ
                if (isset($data_ss) && !empty($data_ss)) {
                    if (isset($CL_ss_id) && $CL_ss_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_ss_id, $data_ss);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_ss);
                    }
                }
                //==============================================================
                if (isset($data_tgpn) && !empty($data_tgpn)) {
                    if (isset($CL_tgpn_id) && $CL_tgpn_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tgpn_id, $data_tgpn);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tgpn);
                    }
                }
                //==============================================================
                if (isset($data_cppt) && !empty($data_cppt)) {
                    if (isset($CL_cppt_id) && $CL_cppt_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_cppt_id, $data_cppt);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_cppt);
                    }
                }
                //==============================================================
                if (isset($data_tk_nv) && !empty($data_tk_nv)) {
                    if (isset($CL_tk_nv_id) && $CL_tk_nv_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tk_nv_id, $data_tk_nv);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tk_nv);
                    }
                }
                //==============================================================
                if (isset($data_sk_nv) && !empty($data_sk_nv)) {
                    if (isset($CL_sk_nv_id) && $CL_sk_nv_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_sk_nv_id, $data_sk_nv);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_sk_nv);
                    }
                }
                //==============================================================
                if (isset($data_cpyt_tn) && !empty($data_cpyt_tn)) {
                    if (isset($CL_cpyt_tn_id) && $CL_cpyt_tn_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_cpyt_tn_id, $data_cpyt_tn);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_cpyt_tn);
                    }
                }
                //==============================================================
                if (isset($data_tcvp) && !empty($data_tcvp)) {
                    if (isset($CL_tcvp_id) && $CL_tcvp_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tcvp_id, $data_tcvp);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tcvp);
                    }
                }
                //==============================================================
                if (isset($data_tcmt) && !empty($data_tcmt)) {
                    if (isset($CL_tcmt_id) && $CL_tcmt_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_tcmt_id, $data_tcmt);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_tcmt);
                    }
                }
                //==============================================================
                if (isset($data_xct) && !empty($data_xct)) {
                    if (isset($CL_xct_id) && $CL_xct_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_xct_id, $data_xct);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_xct);
                    }
                }
                //==============================================================
                if (isset($data_ktdk) && !empty($data_ktdk)) {
                    if (isset($CL_ktdk_id) && $CL_ktdk_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_ktdk_id, $data_ktdk);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_ktdk);
                    }
                }
                //==============================================================
                if (isset($data_dn) && !empty($data_dn)) {
                    if (isset($CL_dn_id) && $CL_dn_id > 0) {
                        // Update data bảng còn lại
                        $res = $this->orders_model->CommonUpdate(TBL_CL, $CL_dn_id, $data_dn);
                    } else {
                        // Thêm mới data bảng còn lại
                        $res = $this->orders_model->CommonCreat(TBL_CL, $data_dn);
                    }
                }
                //==============================================================
                // Update bồi thường
                $post_data = array(
                    'user4' => $this->phpsession->get('user_id'),
                    'order_status' => BT_THANHTOAN,
                    'ghichu_thanhtoan_bt' => $this->input->post('ghichu_thanhtoan_bt', TRUE),
                    'date_thanhtoan' => $datetime_now,
                    'time_thanhtoan' => $time_now,
                    'editor' => $this->phpsession->get('user_id'),
                );

                $post_data['updated_date'] = (date('Y-m-d H:i:s'));
                $post_data['updated_time'] = strtotime(date('Y-m-d'));
                //==============================================================
                $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $post_data);
                //==============================================================
                // Nếu bt là case con
                /*
                 * if ($case_id > 0) {
                  // Lấy bt cha
                  $data_get = array(
                  'BT_id' => $case_id,
                  'get_one' => TRUE,
                  );
                  $bt_t = $this->orders_model->get_orders_by_options($data_get);
                  if (is_object($bt_t)) {
                  $t_st_ycbt_t = $bt_t->st_ycbt_t;
                  $t_st_dbt_t = $bt_t->st_dbt_t;
                  //======================================================
                  $data_update = array(
                  'st_ycbt_t' => $t_st_ycbt_t + $st_ycbt_t,
                  'st_ycbt_t' => $t_st_dbt_t + $st_dbt_t,
                  );
                  $this->orders_model->CommonUpdate(TBL_ORDERS, $case_id, $data_update);
                  }
                  }
                 */

                //==============================================================
                redirect(ORDER_ADMIN_BASE_URL);
            } else {
                redirect(ORDER_ADMIN_BASE_URL);
            }
            //==================================================================
        } else {
            redirect(ORDER_ADMIN_BASE_URL);
        }
    }

    //end
    //==========================================================================
    //==========================================================================
    private function _do_add_orders() {
        $data = array();
        $this->form_validation->set_rules('kh_dbh', 'CMT/Tên của người được bảo hiểm', 'trim|required|xss_clean');
        $this->form_validation->set_rules('phanloai_bt', 'Phân loại bồi thường', 'trim|required|xss_clean');

        $this->form_validation->set_rules('so_tien_ycbt', 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('so_ngay_ycbt', 'Số ngày yêu cầu bồi thường', 'trim|required|integer|xss_clean|max_length[4]');

        $this->form_validation->set_rules('phuongthuc_bt', 'Phương thức bồi thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('csyt', 'Cơ sở y tế', 'trim|required|xss_clean');
        //----------------------------------------------------------------------
        $this->form_validation->set_rules('phuongthuc_nhan_hs', 'Hình thức nhận hồ sơ', 'trim|required|xss_clean');
        $this->form_validation->set_rules('hinhthuc_nhantien_bt', 'Hình thức nhận tiền bồi thường', 'trim|required|xss_clean');
        //$this->form_validation->set_rules('ghichu_nhantien', 'Thông tin tài khoản', 'trim|xss_clean');
        $this->form_validation->set_rules('nth_fullname', 'Người thụ hưởng: Họ và tên', 'trim|xss_clean');
        $this->form_validation->set_rules('nth_bank', 'Người thụ hưởng: Ngân hàng', 'trim|xss_clean');
        $this->form_validation->set_rules('nth_stk', 'Người thụ hưởng: Số tài khoản', 'trim|xss_clean');
        $this->form_validation->set_rules('nth_bank_address', 'Người thụ hưởng: Ghi chú', 'trim|xss_clean');
        //----------------------------------------------------------------------
        $this->form_validation->set_rules('date_nhan_bt', 'Ngày nhận bồi thường', 'trim|required|xss_clean');
        $this->form_validation->set_rules('date_xayra_tonthat', 'Ngày xảy ra tổn thất', 'trim|required|xss_clean');
        //======================================================================
        $case = $this->input->post('case', TRUE);
        if ($case != '' && $case > 0) {
            $this->form_validation->set_rules('case', 'Mã Case bồi thường', 'trim|required|xss_clean|max_length[6]|min_length[6]');
        } else {
            $this->form_validation->set_rules('case', 'Mã Case bồi thường', 'trim|xss_clean');
        }
        $this->form_validation->set_rules('bt_code', 'Mã Hồ sơ bồi thường', 'trim|required|xss_clean|max_length[12]|min_length[11]');
        //======================================================================
        $phanloai_bt = $this->input->post('phanloai_bt', TRUE);
        // Nếu bt là THAISAN_QLNT
        if ($phanloai_bt == BT_THAISAN_QLNT) {
            $this->form_validation->set_rules('qlbt_thaisan', 'Chọn loại thai sản', 'trim|required|xss_clean');
            $qlbt_thaisan = $this->input->post('qlbt_thaisan', TRUE);
            //==================================================================
            $data['phanloai_thaisan'] = $qlbt_thaisan;
        }
        //======================================================================
        // Lấy chứng từ
        $data_get = array(
            'parent_id' => $phanloai_bt,
        );
        $ct = $this->products_coupon_model->get_products_coupon($data_get);
        if (!empty($ct)) {
            $ct_num = count($ct);
            //end
            $ct_bt = $this->input->post('ct_bt', TRUE);

            // Nếu đủ chứng từ
            if (is_array($ct_bt) && !empty($ct_bt) && $ct_num == count($ct_bt)) {
                //$this->form_validation->set_rules('so_tien_ycbt', 'Số tiền yêu cầu bồi thường', 'trim|required|xss_clean');
                //$this->form_validation->set_rules('so_ngay_ycbt', 'Số ngày yêu cầu bồi thường', 'trim|required|integer|xss_clean|max_length[4]');

                $so_ngay_ycbt = $this->input->post('so_ngay_ycbt', TRUE);
                // Nếu số ngày < 0
                if ($so_ngay_ycbt < 0) {
                    redirect(ORDER_ADMIN_BASE_URL);
                }
                //end
                $data['order_status'] = BT_CHODUYET;
                //$data['so_tien_ycbt'] = $this->input->post('so_tien_ycbt');
                //$data['so_ngay_ycbt'] = $this->input->post('so_ngay_ycbt');
                $data['ghichu_hoso'] = $this->input->post('ghichu_hoso');
                //==============================================================
                //$data['date_xuly_bt'] = date('Y-m-d H:i:s');
                //$data['time_xuly_bt'] = strtotime(date('Y-m-d'));
            } else {
                $this->form_validation->set_rules('chungtu_bosung_txt', 'Chứng từ chờ bổ sung', 'trim|required|xss_clean');
                if (!empty($ct_bt)) {
                    $str_ct_bt_id = '';
                    foreach ($ct_bt as $k => $v) {
                        $str_ct_bt_id .= $v . ',';
                    }
                    $data['chungtu_bosung'] = substr($str_ct_bt_id, 0, -1);
                }
                $data['order_status'] = BT_CHOBOSUNG;
                $data['chungtu_bosung_txt'] = $this->input->post('chungtu_bosung_txt');
            }
        }
        //======================================================================
        if ($this->form_validation->run()) {


            $datetime_now = date('Y-m-d H:i:s');
            $date_now = date('Y-m-d');
            $time_now = strtotime($date_now);

            //==================================================================
            $date_xayra_tonthat = $this->input->post('date_xayra_tonthat');
            $date_nhan_bt = $this->input->post('date_nhan_bt');
            $date_xuly_bt = $this->input->post('date_xuly_bt');
            $so_tien_ycbt = $this->input->post('so_tien_ycbt', TRUE);
            $post_data = array(
                'phanloai_bt' => $phanloai_bt,
                'quyenloi_bt' => get_phanloai_bt_txt($phanloai_bt),
                'OD_kh_dbh' => $this->input->post('kh_dbh', TRUE),
                //'parent_id' => $this->input->post('case', TRUE),
                'so_tien_ycbt' => $so_tien_ycbt,
                //'st_ycbt_t' => $so_tien_ycbt,
                'so_ngay_ycbt' => $this->input->post('so_ngay_ycbt', TRUE),
                'phuongthuc_nhan_hs' => $this->input->post('phuongthuc_nhan_hs', TRUE),
                'phuongthuc_bt' => $this->input->post('phuongthuc_bt', TRUE),
                'csyt' => $this->input->post('csyt', TRUE),
                'hinhthuc_nhantien_bt' => $this->input->post('hinhthuc_nhantien_bt', TRUE),
                //'ghichu_hinhthuc_nhantien_bt' => $this->input->post('ghichu_nhantien', TRUE),
                'nth_fullname' => $this->input->post('nth_fullname', TRUE),
                'nth_bank' => $this->input->post('nth_bank', TRUE),
                'nth_stk' => $this->input->post('nth_stk', TRUE),
                'nth_bank_address' => $this->input->post('nth_bank_address', TRUE),
                //--------------------------------------------------------------
                'date_xayra_tonthat' => $date_xayra_tonthat,
                'time_xayra_tonthat' => strtotime($date_xayra_tonthat),
                'date_nhan_bt' => $date_nhan_bt,
                'time_nhan_bt' => strtotime($date_nhan_bt),
                'date_xuly_bt' => $date_xuly_bt,
                'time_xuly_bt' => strtotime($date_xuly_bt),
                //--------------------------------------------------------------
                'editor' => $this->phpsession->get('user_id'),
                'creator' => $this->phpsession->get('user_id'),
                'user1' => $this->phpsession->get('user_id'),
            );

            //==================================================================
            $post_data = array_merge($post_data, $data);
            $post_data['updated_date'] = $datetime_now;
            $post_data['updated_time'] = $time_now;
            $post_data['created_date'] = $datetime_now;
            $post_data['created_time'] = $time_now;
            // Thêm data
            $bt_id = $this->orders_model->insert($post_data);
            //==================================================================
            $bt_code = $this->input->post('bt_code', TRUE);
            if ($bt_code > 0 && $bt_code != '') {
                $data_update = array(
                    'bt_code' => $bt_code,
                );
            } else {
                // Lấy mã bt id
                $bt_id_fm = get_code_bt($bt_id);
                $code = substr($date_nhan_bt, 2, 2) . substr($date_nhan_bt, 5, 2) . substr($date_nhan_bt, 8, 2) . $bt_id_fm;
                $data_update = array(
                    'bt_code' => $code,
                );
            }
            //==================================================================
            // Nếu là bt có case con
            if ($case != '' && $case > 0) {
                $data_update['case'] = $case;
            } else {
                $data_update['case'] = $bt_id_fm;
            }
            $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $data_update);
            //==================================================================
            redirect(ORDERS_ADMIN_BASE_URL);
        }
        return FALSE;
    }

    //==========================================================================
    function huy_bt($id = 0) {
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        $a_bt_huy = get_a_bt_huy();
        //======================================================================
        $bt_id = $this->input->post('id');
        if ($bt_id > 0) {
            // Lấy thông tin bt
            $data_get = array(
                //'join_' . TBL_CUSTOMERS => TRUE,
                //'join_' . TBL_PRODUCTS . '_dbh' => TRUE,
                'BT_id' => $bt_id,
                'get_one' => TRUE,
            );
            $bt = $this->orders_model->get_orders_by_options($data_get);

            if (is_object($bt)) {
                $status = $bt->order_status;
                //==============================================================
                $data['bt'] = $bt;
                //==============================================================
                // Nếu bt đang chờ duyệt
                if (in_array($status, $a_bt_huy)) {
                    // Khi submit
                    if (isset($_POST['submit'])) {
                        $data_update = array(
                            'user6' => $this->phpsession->get('user_id'),
                            'order_status' => BT_HUY,
                            'updated_date' => $datetime_now,
                            'updated_time' => $time_now,
                            'editor' => $this->phpsession->get('user_id'),
                        );
                        //==========================================
//                        echo '<pre>';
//                        print_r($data_update);
//                        die;
                        $this->orders_model->CommonUpdate(TBL_ORDERS, $bt_id, $data_update);
                        redirect(ORDERS_ADMIN_BASE_URL);
                    }
                    //==========================================================
                } else {
                    redirect(ORDERS_ADMIN_BASE_URL);
                }
            } else {
                redirect(ORDERS_ADMIN_BASE_URL);
            }
        } else {
            redirect(ORDERS_ADMIN_BASE_URL);
        }
        //======================================================================
        $tpl = 'ad/ad_bt_form_huy';
        $data['scripts'] = $this->scripts_select2();
        //end
        $data['header'] = 'Hủy bồi thường';
        $data['button_name'] = 'Hủy bồi thường';
        $data['submit_uri'] = ORDER_ADMIN_BASE_URL . '/huy-bt';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        // =====================================================================    
        $this->_view_data['title'] = 'Xem thông tin bồi thường' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    //==========================================================================
    //==========================================================================
    public function delete() {
        $options = array();
        if ($this->is_postback()) {
            $id = $this->input->post('id');
            $this->orders_model->delete($id);
            $options['warning'] = 'Đã xóa thành công';
        }
        $lang = $this->phpsession->get('orders_lang');
        redirect(ORDER_ADMIN_BASE_URL . '/' . $lang);
    }

    //==========================================================================
    private function _get_scripts() {
        $scripts = '<script type="text/javascript" src="/plugins/tiny_mce/tiny_mce.js?v=20111006"></script>';
        $scripts .= '<script language="javascript" type="text/javascript" src="/plugins/tiny_mce/plugins/imagemanager/js/mcimagemanager.js?v=20111006"></script>';
        $scripts .= '<script type="text/javascript">enable_advanced_wysiwyg("wysiwyg");</script>';
        return $scripts;
    }

    //==========================================================================
    function change_status() {
        $id = $this->input->post('id');
        $orders = $this->orders_model->get_orders(array('id' => $id));
        $status = $orders->status == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
        $this->orders_model->update(array('id' => $id, 'status' => $status));
    }

    //==========================================================================
    public function up() {
        $orders_id = $this->input->post('id');
        $this->orders_model->update(array('id' => $orders_id, 'updated_time' => date('Y-m-d H:i:s')));
        $lang = $this->phpsession->get('orders_lang');
        redirect(ORDER_ADMIN_BASE_URL . '/' . $lang);
    }

    //==========================================================================
    function export($options = array()) {
        $options = array();
//        if($this->phpsession->get('customer_name_search') != '')
//            $options['keyword'] = $this->phpsession->get('customer_name_search');
//        if(is_array($this->phpsession->get('date_filter'))){
//            $options = array_merge($options, $this->phpsession->get('date_filter'));
//        }
        $options = array_merge($options, $this->_get_data_from_filter());
        $options['id'] = $this->input->post('id');

        $orders = $this->orders_model->get_orders($options);

        if (count($orders) > 0) {

            //load our new PHPExcel library
            $this->load->library('excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Order excel');
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A1', 'STT');
            $this->excel->getActiveSheet()->setCellValue('B1', 'MÃ ĐƠN HÀNG');
            $this->excel->getActiveSheet()->setCellValue('C1', 'NGÀY MUA HÀNG');
            $this->excel->getActiveSheet()->setCellValue('D2', 'HỌ VÀ TÊN');
            $this->excel->getActiveSheet()->setCellValue('D1', 'THÔNG TIN KHÁCH HÀNG');
            $this->excel->getActiveSheet()->setCellValue('E2', 'ĐỊA CHỈ GIAO HÀNG');
            $this->excel->getActiveSheet()->setCellValue('F2', 'SỐ ĐIỆN THOẠI');
            $this->excel->getActiveSheet()->setCellValue('G2', 'EMAIL');
            $this->excel->getActiveSheet()->setCellValue('H2', 'NGÀY GIỜ YÊU CẦU GIAO HÀNG');
            $this->excel->getActiveSheet()->setCellValue('I2', 'GHI CHÚ');
            $this->excel->getActiveSheet()->setCellValue('J1', 'CÁC MẶT HÀNG/SỐ LƯỢNG');
            $this->excel->getActiveSheet()->setCellValue('K1', 'THÀNH TIỀN');
            $this->excel->getActiveSheet()->setCellValue('L1', 'HÌNH THỨC THANH TOÁN');
            $this->excel->getActiveSheet()->setCellValue('M1', 'TÌNH TRẠNG ĐƠN HÀNG');

            $this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('G2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('H2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('I2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('J2')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('K2')->getFont()->setBold(true);
            //merge cell A1 until D1
            $this->excel->getActiveSheet()->mergeCells('D1:I1');
            //set aligment to center for that merged cell (A1 to D1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $stt = 1;
            $row = 3;
            $total_m = 0;

            foreach ($orders as $order):
                //các mặt hàng

                $order_details = $this->orders_details_model->get_orders_details(array('id' => $order->id));

                $order_detail_product = '';
                foreach ($order_details as $product):
                    $order_detail_product .= '[' . $product->product_name;
                    $order_detail_product .= '/' . $product->quantity . '] ';

                endforeach;
                $sale_date = date('d/m/Y', strtotime($order->sale_date));

                $delivery_form = '';
                if ($order->kind_pay == 1) {
                    $delivery_form = 'Thanh toán chuyển khoản';
                } elseif ($order->kind_pay == 2) {
                    $delivery_form = 'Thanh toán trực tiếp';
                } else {
                    $delivery_form = 'Thanh toán kiểu khác';
                }

                $total_m += $order->total;
                $total = get_price_in_vnd($order->total);

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $stt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $order->id);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $sale_date);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $order->fullname);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $order->address);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $order->tel);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $order->email);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $order->reserve_time);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $order->message);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $order_detail_product);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $total);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $delivery_form);

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, $order->order_status);

                $stt++;
                $row++;
            endforeach;
            $row++;
            $this->excel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->mergeCells('B' . $row . ':Z' . $row);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, "TỔNG TIỀN:");

            if ($total_m > 0)
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, get_price_in_vnd($total_m) . ' VNĐ');
            else
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, '0 VNĐ');

            $filename = 'dang_sach_hoa_don.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
        }else {
            //hoa don khong co san pham
            return $this->browse(array('error' => 'Mục bạn đã chọn hiện thời không có hóa đơn!'));
        }
    }

    function import($options = array()) {
        //load our new PHPExcel library
        $this->load->library('excel');

        $path = "test.xlsx";

        $objPHPExcel = PHPExcel_IOFactory::load($path);
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $worksheetTitle = $worksheet->getTitle();
            $highestRow = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
            $nrColumns = ord($highestColumn) - 64;
            echo "<br>The worksheet " . $worksheetTitle . " has ";
            echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
            echo ' and ' . $highestRow . ' row.';
            echo '<br>Data: <table border="1"><tr>';
            for ($row = 1; $row <= $highestRow; ++$row) {
                echo '<tr>';
                for ($col = 0; $col < $highestColumnIndex; ++$col) {
                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                    $val = $cell->getValue();
                    $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                    echo '<td>' . $val . '<br>(Typ ' . $dataType . ')</td>';
                }
                echo '</tr>';
            }
            echo '</table>';
        }

        for ($row = 2; $row <= $highestRow; ++$row) {
            $val = array();
            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, $row);
                $val[] = $cell->getValue();
            }

            $Connection = "INSERT INTO " . $this->db->dbprefix . "customers (fullname, email, phone) VALUES ('" . $val[1] . "','" . $val[2] . "','" . $val[3] . "')";
        }
    }

    //==========================================================================
    //==========================================================================
    //==========================================================================
    private function scripts() {
        $scripts = '<script type="text/javascript">
                    $(".js-example-disabled-results").select2();
                    </script>';
        return $scripts;
    }

}
