<?php
$selected_df = 'selected="selected"';
if (isset($csyt) && is_object($csyt)) {
//    echo '<pre>';
//    print_r($csyt);
//    die;
    $id = $csyt->id;
    $name = $csyt->name;
    $name_en = $csyt->name_en;
    $csyt_code = $csyt->csyt_code;
    $username = $csyt->username;
    $email = $csyt->email;
    $password = $csyt->password;
    $address = $csyt->address;
    $address_en = $csyt->address_en;
    $phone = $csyt->phone;
    $hotline = $csyt->hotline;
    $fax = $csyt->fax;
    $website = $csyt->website;
    $contact = $csyt->contact;
    $type_id = $csyt->type_id;
    $city_id = $csyt->city_id;
}
$type_id = isset($type_id)?$type_id:'';
$city_id = isset($city_id)?$city_id:'';
//==============================================================================
$a_phanloai_csyt = get_a_csyt_type();
//==============================================================================
echo form_open($submit_uri);
if (isset($id))
    echo form_hidden('id', $id);
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'products_style');
?>

<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Thêm/sửa Cơ sơ y tế"</small>
    <span class="fright"><a href="<?php echo PRODUCTS_STYLE_ADMIN_BASE_URL; ?>" class="button close" style="padding:5px;" title="Đóng"><em></em><span>Đóng</span></a></span>
    <br class="clear"/>
</div>

<div class="form_content">
    <?php $this->load->view('powercms/message'); ?>
    <ul class="tabs">
        <li><a href="#tab1">Nội dung</a></li>
    </ul>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <div class="col-sm-6 col-xs-12">
                <table>
                    <tr><td class="title">Cơ sơ y tế: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'name', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($name) ? $name : set_value('name'))); ?></td>
                    </tr>
                    <tr><td class="title">Mã cơ sơ y tế: </td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'csyt_code', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($csyt_code) ? $csyt_code : set_value('csyt_code'))); ?></td>
                    </tr>
                    <tr><td class="title">Phân loại: (<span>*</span>)</td></tr>
                    <tr>
                        <td>
                            <select name="type" class="js-example-disabled-results" style="margin-left: 10px !important; padding-left: 10px !important">
                                <option value="">-- Loại cơ sở y tế --</option>
                                <?php
                                if (isset($a_phanloai_csyt) && !empty($a_phanloai_csyt)) {
                                    foreach ($a_phanloai_csyt as $k=>$v) {
                                        $selected = ($k == $type_id) ? $selected_df : '';
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>     
                    <tr><td class="title">Email: </td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'email', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($email) ? $email : set_value('email'))); ?></td>
                    </tr>
                    <tr><td class="title">Địa chỉ: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'address', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($address) ? $address : set_value('address'))); ?></td>
                    </tr>                    
                    <tr><td class="title">Tỉnh thành: (<span>*</span>)</td></tr>
                    <tr>
                        <td>
                            <select name="cities" class="js-example-disabled-results" style="margin-left: 10px !important; padding-left: 10px !important">
                                <option value="">-- Chọn tỉnh thành --</option>
                                <?php
                                if (isset($a_cities) && !empty($a_cities)) {
                                    foreach ($a_cities as $index) {
                                        $ct_id = $index->id;
                                        $ct_name = $index->name;
                                        $selected = ($ct_id == $city_id) ? $selected_df : '';
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $ct_id ?>"><?php echo $ct_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>     
                    <tr><td class="title">Điện thoại: </td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'phone', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($phone) ? $phone : set_value('phone'))); ?></td>
                    </tr>
                    <tr><td class="title">Hotline: </td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'hotline', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($hotline) ? $hotline : set_value('hotline'))); ?></td>
                    </tr>
                    <tr><td class="title">Fax: </td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'fax', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($fax) ? $fax : set_value('fax'))); ?></td>
                    </tr>
                </table>
            </div>

            <div class="col-sm-6 col-xs-12">
                <table>
                    <tr><td class="title">Website:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'website', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($website) ? $website : set_value('website'))); ?></td>
                    </tr>
                    <tr><td class="title">Cơ sơ y tế - Tiếng Anh: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'name_en', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($name_en) ? $name_en : set_value('name_en'))); ?></td>
                    </tr>
                    <tr><td class="title">Địa chỉ - Tiếng Anh: (<span>*</span>)</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'address_en', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($address_en) ? $address_en : set_value('address_en'))); ?></td>
                    </tr>  
                    <tr><td class="title">Tên đăng nhập:</td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'username', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($username) ? $username : set_value('username'))); ?></td>
                    </tr>
                    <tr><td class="title">Password: </td></tr>
                    <tr>
                        <td><?php echo form_input(array('name' => 'password', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset($password) ? $password : set_value('password'))); ?></td>
                    </tr>
                    <tr><td class="title">Người liên hệ:</td></tr>
                    <tr>
                        <td>
                            <textarea name="contact" rows="5" cols="66" value="<?php echo isset($contact) ? $contact : '' ?>"><?php echo isset($contact) ? $contact : '' ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>

        </div>

    </div>
    <br class="clear"/>
    <div style="margin-top: 10px;"></div>
    <?php echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn')); ?>
    <a style="margin-left: 20px;" class="btn btn-primary" href="<?php echo site_url(PRODUCTS_STYLE_ADMIN_BASE_URL) ?>">Quay lại <span class="glyphicon glyphicon-share-alt"></span></a>
    <br class="clear"/>&nbsp;
</div>
<?php echo form_close(); ?>