<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
$ss_kh_parent_id = $this->phpsession->get('ss_kh_parent_id');
//end
$a_qlbh = array(NOITRU => 'Nội trú', NGOAITRU => 'Ngoại trú');
?>
<div class="box_filter" style="margin-bottom: 20px">

    <form action="<?php echo site_url(URL_BT_CN) ?>" method="get">
    </form>
</div>
<table id="_table-edit" class="table table-bordered table-hover">
    <section class="header_tbl box-typical box-typical-dashboard panel panel-default scrollable lobipanel lobipanel-sortable" data-inner-id="UNp4cd47l6" data-index="0">
        <header class="box-typical-header panel-heading ui-sortable-handle">
            <h3 class="panel-title" style="max-width: calc(100% - 180px);"><span class="glyphicon glyphicon-th text-primary"></span> Thông tin bảo hiểm:</h3>
        </header>
    </section>
    <thead>
        <tr>
            <th>Chỉ tiêu</th>
            <th>Giá trị</th>
            <th>Chỉ tiêu</th>
            <th>Giá trị</th>
        </tr>
    </thead>
    <?php
    //==========================================================================
    $bt_tv_tttbvv_tn = $bt_tv_tttbvv_tn_tv = $bt_tv_tttbvv_tn_tt = $bt_tv_tttbvv_ob = $bt_tv_tttbvv_ob_tv = $bt_tv_tttbvv_ob_tt = '';
    $tv_tttbvv_tn_st_ycbt = $tv_tttbvv_tn_tv_st_ycbt = $tv_tttbvv_tn_tt_st_ycbt = $tv_tttbvv_ob_st_ycbt = $tv_tttbvv_ob_tv_st_ycbt = $tv_tttbvv_ob_tt_st_ycbt = '';
    $tv_tttbvv_tn_st_dbt = $tv_tttbvv_tn_tv_st_dbt = $tv_tttbvv_tn_tt_st_dbt = $tv_tttbvv_ob_st_dbt = $tv_tttbvv_ob_tv_st_dbt = $tv_tttbvv_ob_tt_st_dbt = '';
    //==========================================================================
    $bt_tcl_nn = '';
    $tcl_nn_st_ycbt = '';
    $tcl_nn_st_dbt = '';
    $tcl_nn_sn_dbt = $tcl_nn_sn_cl = 0;
    //==========================================================================
    $bt_cpyt_tn = '';
    $cpyt_tn_st_ycbt = '';
    $cpyt_tn_st_dbt = '';
    //==========================================================================
    $bt_nkmdl_db = $bt_nkmdl_cb = $bt_nkmdl_cvr = '';
    $nkmdl_db_st_ycbt = $nkmdl_cb_st_ycbt = $nkmdl_cvr_st_ycbt = '';
    $nkmdl_db_st_dbt = $nkmdl_cb_st_dbt = $nkmdl_cvr_st_dbt = '';
    //==========================================================================
    $bt_tsmdl = $bt_tsmdl_ktdk = $bt_tsmdl_st = $bt_tsmdl_sm = $bt_tsmdl_dn = '';
    $tsmdl_st_ycbt = $tsmdl_ktdk_st_ycbt = $tsmdl_st_st_ycbt = $tsmdl_sm_st_ycbt = $tsmdl_dn_st_ycbt = '';
    $tsmdl_st_dbt = $tsmdl_ktdk_st_dbt = $tsmdl_st_st_dbt = $tsmdl_sm_st_dbt = $tsmdl_dn_st_dbt = '';
    //==========================================================================
    $bt_dtngt_ob = $bt_dtngt_ob_st1lk = $bt_dtngt_ob_nkngt = $bt_dtngt_ob_slk = $bt_dtngt_ob_nk = $bt_dtngt_ob_cvr = '';
    $dtngt_ob_st_ycbt = $dtngt_ob_st1lk_st_ycbt = $dtngt_ob_nkngt_st_ycbt = $dtngt_ob_nk_st_ycbt = $dtngt_ob_cvr_st_ycbt = '';
    $dtngt_ob_st_dbt = $dtngt_ob_st1lk_st_dbt = $dtngt_ob_nkngt_st_dbt = $dtngt_ob_nk_st_dbt = $dtngt_ob_cvr_st_dbt = '';
    //==========================================================================
    $bt_dtnt_ob = $bt_dtnt_ob_tvpn = $bt_dtnt_ob_tvpn_sn = $bt_dtnt_ob_tgpn = $bt_dtnt_ob_tgpn_sn = $bt_dtnt_ob_cppt = '';
    $bt_dtnt_ob_tk_nv = $bt_dtnt_ob_sk_nv = $bt_dtnt_ob_cpyt_tn = $bt_dtnt_tcvp = $bt_dtnt_ob_tcvp_sn = $bt_dtnt_ob_tcmt = $bt_dtnt_ob_xct = '';
    $bt_thaisan_qlnt = $bt_tsqlnt_ktdk = $bt_tsqlnt_st = $bt_tsqlnt_sm = $bt_tsqlnt_dn = '';
    //--------------------------------------------------------------------------
    $dtnt_ob_st_ycbt = $dtnt_ob_tvpn_st_ycbt = $dtnt_ob_tvpn_sn_cl = $dtnt_ob_tgpn_st_ycbt = $dtnt_ob_tgpn_sn_cl = $dtnt_ob_cppt_st_ycbt = '';
    $dtnt_ob_tk_nv_st_ycbt = $dtnt_ob_sk_nv_st_ycbt = $dtnt_ob_cpyt_tn_st_ycbt = $dtnt_ob_tcvp = $dtnt_ob_tcvp_sn_cl = $dtnt_ob_tcmt_st_ycbt = $dtnt_ob_xct_st_ycbt = '';
    $tsqlnt_st_ycbt = $tsqlnt_ktdk_st_ycbt = $tsqlnt_st_st_ycbt = $tsqlnt_sm_st_ycbt = $tsqlnt_dn_st_ycbt = '';
    //--------------------------------------------------------------------------
    $dtnt_ob_st_dbt = $dtnt_ob_tvpn_st_dbt = $dtnt_ob_tvpn_sn_dbt = $dtnt_ob_tgpn_st_dbt = $dtnt_ob_tgpn_sn_dbt = $dtnt_ob_cppt_st_dbt = '';
    $dtnt_ob_tk_nv_st_dbt = $dtnt_ob_sk_nv_st_dbt = $dtnt_ob_cpyt_tn_st_dbt = $dtnt_ob_tcvp = $dtnt_ob_tcvp_sn_dbt = $dtnt_ob_tcmt_st_dbt = $dtnt_ob_xct_st_dbt = '';
    $tsqlnt_st_dbt = $tsqlnt_ktdk_st_dbt = $tsqlnt_st_st_dbt = $tsqlnt_sm_st_dbt = $tsqlnt_dn_st_dbt = '';
    //==========================================================================
    if (isset($bh) && !empty($bh)) {
//        echo '<pre>';
//        print_r($bh);
//        die;
        foreach ($bh as $index) {
            $id = $index->id;
            $kh_mbh = $index->kh_mbh;
            $dn_mbh = $index->congty_canhan;
            $parent_id = $index->parent_id;
            $phongban = $index->phongban;
            $ma_nv = $index->code;
            $sothe_bh = $index->sothe_bh;
            $daily = $index->daily;
            //------------------------------------------------------------------
            $goi_bh_dm = $index->goi_bh_dm;
            $so_ngay_ycbt = $index->so_ngay_ycbt;
            $ten_dn_bbh = $index->ten_dn_bbh;
            $sohopdong = $index->sohopdong;
            $giay_chungnhan = $index->giay_chungnhan;
            $ngay_batdau = $index->ngay_batdau;
            $ngay_ketthuc = $index->ngay_ketthuc;
            $ngay_thanhtoan = $index->ngay_thanhtoan;
            $ngay_ketthuc = $index->ngay_ketthuc;
            //==================================================================
            $tv_tttbvv_tn_st_cl = $TV_TTTBVV_TN = $index->TV_TTTBVV_TN;
            $tv_tttbvv_tn_tv_st_cl = $tv_tttbvv_tn_tv = $index->tv_tttbvv_tn_tv;
            $tv_tttbvv_tn_tt_st_cl = $tv_tttbvv_tn_tt = $index->tv_tttbvv_tn_tt;
            $tcl_nn_st = $TCL_NN = $index->TCL_NN;
            $tcl_nn_sn_cl = $tcl_nn_sn = $index->tcl_nn_sn;
            $tcl_nn_st_cl = (int) $tcl_nn_st * (int) $tcl_nn_sn_cl;
            $cpyt_tn_st_cl = $CPYT_TN = $index->CPYT_TN;
            $tv_tttbvv_ob_st_cl = $TV_TTTBVV_OB = $index->TV_TTTBVV_OB;
            $dtnt_ob_st_cl = $DTNT_OB = $index->DTNT_OB;
            $dtnt_ob_dbh_st_cl = $dtnt_dbh = $index->dtnt_dbh;
            $dtnt_ob_tvpn = $dtnt_tvpn = $index->dtnt_tvpn;
            $dtnt_ob_tvpn_sn_cl = $dtnt_tvpn_sn = $index->dtnt_tvpn_sn;
            $dtnt_ob_tgpn = $dtnt_tgpn = $index->dtnt_tgpn;
            $dtnt_ob_tgpn_sn_cl = $dtnt_tgpn_sn = $index->dtnt_tgpn_sn;
            $dtnt_ob_cppt_st_cl = $dtnt_cppt = $index->dtnt_cppt;
            $dtnt_ob_tk_nv_st_cl = $dtnt_tk_nv = $index->dtnt_tk_nv;
            $dtnt_ob_sk_nv_st_cl = $dtnt_sk_nv = $index->dtnt_sk_nv;
            $dtnt_ob_cpyt_tn_st_cl = $dtnt_cpyt_tn = $index->dtnt_cpyt_tn;
            $dtnt_ob_tcvp = $dtnt_tcvp = $index->dtnt_tcvp;
            $dtnt_ob_tcvp_sn_cl = $dtnt_tcvp_sn = $index->dtnt_tcvp_sn;
            $dtnt_ob_tcmt_st_cl = $dtnt_tcmt = $index->dtnt_tcmt;
            $dtnt_ob_xct_st_cl = $dtnt_xct = $index->dtnt_xct;
            $tsqlnt_st_cl = $THAISAN_QLNT = $index->THAISAN_QLNT;
            $tsqlnt_ktdk_st_cl = $tsqlnt_ktdk = $index->tsqlnt_ktdk;
            $tsqlnt_st_st_cl = $tsqlnt_st = $index->tsqlnt_st;
            $tsqlnt_sm_st_cl = $tsqlnt_sm = $index->tsqlnt_sm;
            $tsqlnt_dn_st_cl = $tsqlnt_dn = $index->tsqlnt_dn;
            $dtngt_ob_st_cl = $DTNGT_OB = $index->DTNGT_OB;
            $dtngt_dbh = $index->dtngt_dbh;
            $dtngt_st1lk = $index->dtngt_st1lk;
            $dtngt_ob_slk_cl = $dtngt_slk = $index->dtngt_slk;
            $dtngt_ob_nkngt_st_cl = $dtngt_nkngt = $index->dtngt_nkngt;
            $dtngt_ob_nk_st_cl = $dtngt_nk = $index->dtngt_nk;
            $dtngt_ob_cvr_st_cl = $dtngt_cvr = $index->dtngt_cvr;
            $tsmdl_st_cl = $THAISAN_MDL = $index->THAISAN_MDL;
            $tsmdl_dbh = $index->tsmdl_dbh;
            $tsmdl_ktdk_st_cl = $tsmdl_ktdk = $index->tsmdl_ktdk;
            $tsmdl_st_st_cl = $tsmdl_st = $index->tsmdl_st;
            $tsmdl_sm_st_cl = $tsmdl_sm = $index->tsmdl_sm;
            $tsmdl_dn_st_cl = $tsmdl_dn = $index->tsmdl_dn;
            $nkmdl_st_cl = $NHAKHOA_MDL = $index->NHAKHOA_MDL;
            $nkmdl_dbh = $index->nkmdl_dbh;
            $nkmdl_cb_st_cl = $nkmdl_cb = $index->nkmdl_cb;
            $nkmdl_db_st_cl = $nkmdl_db = $index->nkmdl_db;
            $nkmdl_cvr_st_cl = $nkmdl_cvr = $index->nkmdl_cvr;
            //==================================================================
            if (isset($qlbt_cl) && !empty($qlbt_cl)) {
                foreach ($qlbt_cl as $ql) {
                    $CL_quyenloi_bt = $ql->CL_quyenloi_bt;
                    $CL_st_dbt = $ql->CL_st_dbt;
                    $CL_st_cl = $ql->CL_st_bh_cl;
                    $CL_sn_dbt = $ql->CL_sn_dbt;
                    $CL_sn_cl = $ql->CL_sn_bh_cl;
                    $CL_slk_dbt = $ql->CL_slk_dbt;
                    $CL_slk_cl = $ql->CL_slk_bh_cl;
                    //==========================================================
                    if ($CL_quyenloi_bt == TV_TTTBVV_TN) {
                        $bt_tv_tttbvv_tn = 1;
                        $tv_tttbvv_tn_st_cl = $CL_st_cl;
                        $tv_tttbvv_tn_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tv_tttbvv_tn_tv) {
                        $bt_tv_tttbvv_tn_tv = 1;
                        $tv_tttbvv_tn_tv_st_cl = $CL_st_cl;
                        $tv_tttbvv_tn_tv_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tv_tttbvv_tn_tt) {
                        $bt_tv_tttbvv_tn_tt = 1;
                        $tv_tttbvv_tn_tt_st_cl = $CL_st_cl;
                        $tv_tttbvv_tn_tt_st_dbt = $CL_st_dbt;
                    }
                    //==================================================================
                    if ($CL_quyenloi_bt == TV_TTTBVV_OB) {
                        $bt_tv_tttbvv_ob = 1;
                        $tv_tttbvv_ob_st_cl = $CL_st_cl;
                        $tv_tttbvv_ob_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tv_tttbvv_ob_tv) {
                        $bt_tv_tttbvv_ob_tv = 1;
                        $tv_tttbvv_ob_tv_st_cl = $CL_st_cl;
                        $tv_tttbvv_ob_tv_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tv_tttbvv_ob_tt) {
                        $bt_tv_tttbvv_ob_tt = 1;
                        $tv_tttbvv_ob_tt_st_cl = $CL_st_cl;
                        $tv_tttbvv_ob_tt_st_dbt = $CL_st_dbt;
                    }
                    //==================================================================
                    if ($CL_quyenloi_bt == TCL_NN) {
                        $bt_tcl_nn = 1;
                        $tcl_nn_st_cl = $CL_st_cl;
                        $tcl_nn_st_dbt = $CL_st_dbt;
                        $tcl_nn_sn_cl = $CL_sn_cl;
                        $tcl_nn_sn_dbt = $CL_sn_dbt;
                    }
                    //==================================================================
                    if ($CL_quyenloi_bt == CPYT_TN) {
                        $bt_cpyt_tn = 1;
                        $cpyt_tn_st_cl = $CL_st_cl;
                        $cpyt_tn_st_dbt = $CL_st_dbt;
                    }
                    //==================================================================
                    if ($CL_quyenloi_bt == NHAKHOA_MDL) {
                        $bt_nkmdl = 1;
                        $nkmdl_st_cl = $CL_st_cl;
                        $nkmdl_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == nkmdl_cb) {
                        $bt_nkmdl_cb = 1;
                        $nkmdl_cb_st_cl = $CL_st_cl;
                        $nkmdl_cb_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == nkmdl_db) {
                        $bt_nkmdl_db = 1;
                        $nkmdl_db_st_cl = $CL_st_cl;
                        $nkmdl_db_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == nkmdl_cvr) {
                        $bt_nkmdl_cvr = 1;
                        $nkmdl_cvr_st_cl = $CL_st_cl;
                        $nkmdl_cvr_st_dbt = $CL_st_dbt;
                    }
                    //==================================================================
                    if ($CL_quyenloi_bt == THAISAN_MDL) {
                        $bt_tsmdl = 1;
                        $tsmdl_st_cl = $CL_st_cl;
                        $tsmdl_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tsmdl_ktdk) {
                        $bt_tsmdl_ktdk = 1;
                        $tsmdl_ktdk_st_cl = $CL_st_cl;
                        $tsmdl_ktdk_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tsmdl_st) {
                        $bt_tsmdl_st = 1;
                        $tsmdl_st_st_cl = $CL_st_cl;
                        $tsmdl_st_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tsmdl_sm) {
                        $bt_tsmdl_sm = 1;
                        $tsmdl_sm_st_cl = $CL_st_cl;
                        $tsmdl_sm_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tsmdl_dn) {
                        $bt_tsmdl_dn = 1;
                        $tsmdl_dn_st_cl = $CL_st_cl;
                        $tsmdl_dn_st_dbt = $CL_st_dbt;
                    }
                    //==================================================================
                    if ($CL_quyenloi_bt == DTNGT_OB) {
                        $bt_dtngt_ob = 1;
                        $dtngt_ob_st_cl = $CL_st_cl;
                        $dtngt_ob_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == dtngt_st1lk) {
                        $bt_dtngt_ob_st1lk = 1;
                        $dtngt_ob_st1lk_st_cl = $CL_st_cl;
                        $dtngt_ob_st1lk_st_dbt = $CL_st_dbt;
                        $dtngt_ob_slk_dbt = $CL_slk_dbt;
                        $dtngt_ob_slk_cl = $CL_slk_cl;
                    }
                    if ($CL_quyenloi_bt == dtngt_nkngt) {
                        $bt_dtngt_ob_nkngt = 1;
                        $dtngt_ob_nkngt_st_cl = $CL_st_cl;
                        $dtngt_ob_nkngt_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == dtngt_nk) {
                        $bt_dtngt_ob_nk = 1;
                        $dtngt_ob_nk_st_cl = $CL_st_cl;
                        $dtngt_ob_nk_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == dtngt_cvr) {
                        $bt_dtngt_ob_cvr = 1;
                        $dtngt_ob_cvr_st_cl = $CL_st_cl;
                        $dtngt_ob_cvr_st_dbt = $CL_st_dbt;
                    }
                    //==================================================================
                    if ($CL_quyenloi_bt == DTNT_OB) {
                        $bt_dtnt_ob = 1;
                        $dtnt_ob_st_cl = $CL_st_cl;
                        $dtnt_ob_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == dtnt_tvpn) {
                        $bt_dtnt_ob_tvpn = 1;
                        $dtnt_ob_tvpn_st_cl = $CL_st_cl;
                        $dtnt_ob_tvpn_st_dbt = $CL_st_dbt;
                        $dtnt_ob_tvpn_sn_cl = $CL_sn_cl;
                        $dtnt_ob_tvpn_sn_dbt = $CL_sn_dbt;
                    }
                    if ($CL_quyenloi_bt == dtnt_tgpn) {
                        $bt_dtnt_ob_tgpn = 1;
                        $dtnt_ob_tgpn_st_cl = $CL_st_cl;
                        $dtnt_ob_tgpn_st_dbt = $CL_st_dbt;
                        $dtnt_ob_tgpn_sn_cl = $CL_sn_cl;
                        $dtnt_ob_tgpn_sn_dbt = $CL_sn_dbt;
                    }
                    if ($CL_quyenloi_bt == dtnt_cppt) {
                        $bt_dtnt_ob_cppt = 1;
                        $dtnt_ob_cppt_st_cl = $CL_st_cl;
                        $dtnt_ob_cppt_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == dtnt_tk_nv) {
                        $bt_dtnt_ob_tk_nv = 1;
                        $dtnt_ob_tk_nv_st_cl = $CL_st_cl;
                        $dtnt_ob_tk_nv_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == dtnt_sk_nv) {
                        $bt_dtnt_ob_sk_nv = 1;
                        $dtnt_ob_sk_nv_st_cl = $CL_st_cl;
                        $dtnt_ob_sk_nv_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == dtnt_cpyt_tn) {
                        $bt_dtnt_ob_cpyt_tn = 1;
                        $dtnt_ob_cpyt_tn_st_cl = $CL_st_cl;
                        $dtnt_ob_cpyt_tn_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == dtnt_tcvp) {
                        $bt_dtnt_ob_tcvp = 1;
                        $dtnt_ob_tcvp_st_cl = $CL_st_cl;
                        $dtnt_ob_tcvp_st_dbt = $CL_st_dbt;
                        $dtnt_ob_tcvp_sn_cl = $CL_sn_cl;
                        $dtnt_ob_tcvp_sn_dbt = $CL_sn_dbt;
                    }
                    if ($CL_quyenloi_bt == dtnt_tcmt) {
                        $bt_dtnt_ob_tcmt = 1;
                        $dtnt_ob_tcmt_st_cl = $CL_st_cl;
                        $dtnt_ob_tcmt_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == dtnt_xct) {
                        $bt_dtnt_ob_xct = 1;
                        $dtnt_ob_xct_st_cl = $CL_st_cl;
                        $dtnt_ob_xct_st_dbt = $CL_st_dbt;
                    }
                    //==================================================================
                    if ($CL_quyenloi_bt == THAISAN_QLNT) {
                        $bt_thaisan_qlnt = 1;
                        $tsqlnt_st_cl = $CL_st_cl;
                        $tsqlnt_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tsqlnt_ktdk) {
                        $bt_tsqlnt_ktdk = 1;
                        $tsqlnt_ktdk_st_cl = $CL_st_cl;
                        $tsqlnt_ktdk_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tsqlnt_st) {
                        $bt_tsqlnt_st = 1;
                        $tsqlnt_st_st_cl = $CL_st_cl;
                        $tsqlnt_st_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tsqlnt_sm) {
                        $bt_tsqlnt_sm = 1;
                        $tsqlnt_sm_st_cl = $CL_st_cl;
                        $tsqlnt_sm_st_dbt = $CL_st_dbt;
                    }
                    if ($CL_quyenloi_bt == tsqlnt_dn) {
                        $bt_tsqlnt_dn = 1;
                        $tsqlnt_dn_st_cl = $CL_st_cl;
                        $tsqlnt_dn_st_dbt = $CL_st_dbt;
                    }
                    //==================================================================
                }
            }
            //==================================================================
            ?>
            <tbody>
                <tr>
                    <td>Gói BH </td>
                    <td><?php echo $goi_bh_dm ?></td>
                    <td>Ngày bắt đầu </td>
                    <td><?php echo substr($ngay_batdau, 0, 10) ?></td>
                </tr>
                <tr>
                    <td>Bên mua BH </td>
                    <td><?php echo $dn_mbh ?></td>
                    <td>Ngày kết thúc </td>
                    <td><?php echo substr($ngay_ketthuc, 0, 10) ?></td>                    
                </tr>
                <tr>
                    <td>Công ty BH </td>
                    <td><?php echo $ten_dn_bbh ?></td>
                    <td>Ngày thanh toán </td>
                    <td><?php echo substr($ngay_thanhtoan, 0, 10) ?></td>
                </tr>
                <tr>
                    <td>Số hợp đồng </td>
                    <td><?php echo $sohopdong ?></td>
                    <td>Môi giới </td>
                    <td><?php echo $daily ?></td>
                </tr>
            </tbody>
            <?php
        }
    } else {
        ?>
        <tbody>
            <tr>
                <td colspan="10" class="text-danger">Không có dữ liệu.</td>
            </tr>
        </tbody>
        <?php
    }
    ?>
</table>
<br><br><br>
<?php
if (isset($bh) && !empty($bh)) {
    ?>
    <table id="_table-edit" class="table table-bordered table-hover">
        <thead class="thead1">
            <tr>
                <th class="text-danger">Quyền lợi bảo hiểm</th>
                <th class="text-success">Hạn mức BH (STBH)</th>
                <th class="text-info">Số tiền đã BT</th>
                <th class="text-warning">Số tiền BH còn lại</th>
                <th class="text-danger">Ghi chú</th>
            </tr>
        </thead>
        <tbody class="tbody_qlbh">
            <?php
            if ($TV_TTTBVV_TN > 0) {
                ?>
                <tr>
                    <td style="text-align: left">
                        <br><strong>1. TỬ VONG, TTTBVV (Do tai nạn)</strong><br><br>
                    </td>
                    <td><br><strong><?php echo get_price_in_vnd($TV_TTTBVV_TN) ?></strong> <br><br> </td>
                    <td><br><strong><?php echo get_price_in_vnd($tv_tttbvv_tn_st_dbt) ?></strong> <br><br></td>
                    <td><br><strong><?php echo get_price_in_vnd($tv_tttbvv_tn_st_cl) ?></strong><br><br></td>
                    <td></td>
                </tr>
                <?php
            }
            ?>
            <?php
            if ($TCL_NN > 0) {
                ?>
                <tr>
                    <td style="text-align: left">
                        <br><strong>2. TRỢ CẤP LƯƠNG NGÀY NGHỈ</strong>
                        <br><br> Số tiền/ngày <br><br> Số ngày 
                    </td>
                    <td><br><br><?php echo get_price_in_vnd($TCL_NN) ?> <br><br> <?php echo ($tcl_nn_sn) ?></td>
                    <td><br><br><?php echo get_price_in_vnd($tcl_nn_st_dbt) ?><br><br><?php echo ($tcl_nn_sn_dbt) ?></td>
                    <td><br><br><br><br><?php echo ($tcl_nn_sn_cl) ?></td>
                    <td></td>
                </tr>
                <?php
            }
            ?>
            <?php
            if ($CPYT_TN > 0) {
                ?>
                <tr>
                    <td style="text-align: left">
                        <br><strong>3. CHI PHÍ Y TẾ DO TAI NẠN</strong><br><br>
                    </td>
                    <td><br><strong><?php echo get_price_in_vnd($CPYT_TN) ?></strong> <br><br> </td>
                    <td><br><strong><?php echo get_price_in_vnd($cpyt_tn_st_dbt) ?></strong> <br><br></td>
                    <td><br><strong><?php echo get_price_in_vnd($cpyt_tn_st_cl) ?></strong><br><br></td>
                    <td></td>
                </tr>
                <?php
            }
            ?>
            <?php
            if ($TV_TTTBVV_OB > 0) {
                ?>
                <tr>
                   <td style="text-align: left">
                        <br><strong>4. TỬ VONG, TTTBVV (Do ốm bệnh)</strong><br><br>
                    </td>
                    <td><br><strong><?php echo get_price_in_vnd($TV_TTTBVV_OB) ?></strong> <br><br> </td>
                    <td><br><strong><?php echo get_price_in_vnd($tv_tttbvv_ob_st_dbt) ?></strong> <br><br></td>
                    <td><br><strong><?php echo get_price_in_vnd($tv_tttbvv_ob_st_cl) ?></strong><br><br></td>
                    <td></td>
                </tr>
                <?php
            }
            ?>
            <?php
            if ($DTNT_OB > 0) {
                ?>
                <tr>
                    <td style="text-align: left">
                        <br><strong>5. ĐIỀU TRỊ NỘI TRÚ (Do ốm bệnh)</strong>
                        <br><br> 5.1 Tiền viện phí ngày <br><br> -- Số ngày nằm viện
                        <br><br> 5.2 Tiền giường, phòng / ngày <br> -- Số ngày
                        <br><br> 5.3 Chi phí phẫu thuật
                        <br><br> 5.4 Quyền lợi khám trước khi nhập viện
                        <br><br> 5.5 Quyền lợi khám sau khi nhập viện
                        <br><br> 5.6 Chi phí y tá tại nhà
                        <br><br> 5.7 Trợ cấp viện phí <br> - Số ngày trợ cấp
                        <br><br> 5.8 Trợ cấp mai táng
                        <br><br> 5.9 Xe cứu thương
                        <?php
                        if ($THAISAN_QLNT > 0) {
                            ?>
                            <br><br> <strong>5.10 THAI SẢN (Trong quyền lợi nội trú):</strong>
                            <br><br> - Chi phí khám thai định kỳ
                            <br><br> - Sinh thường
                            <br><br> - Sinh mổ
                            <br><br> - Dưỡng nhi
                            <?php
                        }
                        ?>
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($DTNT_OB) ?></strong>
                        <br><br> <?php echo get_price_in_vnd($dtnt_tcvp) ?> <br><br>  <?php echo get_price_in_vnd($dtnt_tcvp_sn) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_tgpn) ?> <br> <?php echo get_price_in_vnd($dtnt_tgpn_sn) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_cppt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_tk_nv) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_sk_nv) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_cpyt_tn) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_tcvp) ?> <br> <?php echo get_price_in_vnd($dtnt_tcvp_sn) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_tcmt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_xct) ?>
                        <?php
                        if ($THAISAN_QLNT > 0) {
                            ?>
                            <br><br> <?php echo get_price_in_vnd($THAISAN_QLNT) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_ktdk) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_st) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_sm) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_dn) ?>
                            <?php
                        }
                        ?>
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($dtnt_ob_st_dbt) ?></strong>
                        <br><br> <?php // echo get_price_in_vnd($dtnt_tcvp_sn)         ?> <br><br>  <?php echo get_price_in_vnd($dtnt_ob_tcvp_sn_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_tgpn_sn_dbt) ?> <br> <?php echo get_price_in_vnd($dtnt_ob_tgpn_sn_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_cppt_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_tk_nv_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_sk_nv_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_cpyt_tn_st_dbt) ?>
                        <br><br> <?php // echo get_price_in_vnd($dtnt_tcvp)         ?> <br> <?php echo get_price_in_vnd($dtnt_ob_tcvp_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_tcmt_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_xct_st_dbt) ?>
                        <?php
                        if ($THAISAN_QLNT > 0) {
                            ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_st_dbt) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_ktdk_st_dbt) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_st_st_dbt) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_sm_st_dbt) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_dn_st_dbt) ?>
                            <?php
                        }
                        ?>

                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($dtnt_ob_st_cl) ?></strong>
                        <br><br> <?php // echo get_price_in_vnd($dtnt_tcvp_sn)         ?> <br><br>  <?php echo get_price_in_vnd($dtnt_ob_tcvp_sn_cl) ?>
                        <br><br> <?php // echo get_price_in_vnd($dtnt_ob_tgpn_sn_cl)         ?> <br> <?php echo get_price_in_vnd($dtnt_ob_tgpn_sn_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_cppt_st_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_tk_nv_st_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_sk_nv_st_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_cpyt_tn_st_cl) ?>
                        <br><br> <?php // echo get_price_in_vnd($dtnt_tcvp)         ?> <br> <?php echo get_price_in_vnd($dtnt_ob_tcvp_sn_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_tcmt_st_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($dtnt_ob_xct_st_cl) ?>
                        <?php
                        if ($THAISAN_QLNT > 0) {
                            ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_st_cl) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_ktdk_st_cl) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_st_st_cl) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_sm_st_cl) ?>
                            <br><br> <?php echo get_price_in_vnd($tsqlnt_dn_st_cl) ?>
                            <?php
                        }
                        ?>
                    </td>
                    <td>-</td>
                </tr>
                <?php
            }
            ?>   
            <?php
            if ($DTNGT_OB > 0) {
                ?>
                <tr>
                    <td style="text-align: left">
                        <br><strong>6. ĐIỀU TRỊ NGOẠI TRÚ (Do ốm bệnh)</strong>
                        <br><br> 6.1 Số tiền cho 1 lần khám
                        <br><br> 6.2 Số lần khám
                        <br><br> 6.3 Nha khoa (trong OP)
                        <br><br> 6.4 Cạo vôi răng
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($DTNGT_OB) ?></strong>
                        <br><br> <?php echo get_price_in_vnd($dtngt_st1lk) ?>
                        <br><br> <?php echo get_price_in_vnd($dtngt_slk) ?>
                        <br><br> <?php echo get_price_in_vnd($dtngt_nkngt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtngt_cvr) ?>
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($dtngt_ob_st_dbt) ?></strong>
                        <br><br> <?php // echo get_price_in_vnd($dtngt_ob_st_dbt)         ?>
                        <br><br> <?php echo get_price_in_vnd($dtngt_ob_slk_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtngt_ob_nkngt_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($dtngt_ob_cvr_st_dbt) ?> 
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($dtngt_ob_st_cl) ?></strong>
                        <br><br> <?php // echo get_price_in_vnd($dtngt_ob_st_cl)         ?>
                        <br><br> <?php echo get_price_in_vnd($dtngt_ob_slk_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($dtngt_ob_nkngt_st_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($dtngt_ob_cvr_st_cl) ?> 
                    </td>
                    <td></td>
                </tr>
                <?php
            }
            ?>    
            <?php
            if ($THAISAN_MDL > 0) {
                ?>
                <tr>
                    <td style="text-align: left">
                        <br><strong>7. THAI SẢN (Mua độc lập)</strong>
                        <br><br> 7.1 Chi phí khám thai định kỳ
                        <br><br> 7.2 Sinh thường
                        <br><br> 7.3 Sinh mổ
                        <br><br> 7.4 Dưỡng nhi
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($THAISAN_MDL) ?></strong>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_ktdk) ?>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_st) ?>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_sm) ?>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_dn) ?>
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($tsmdl_st_dbt) ?></strong>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_ktdk_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_st_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_sm_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_dn_st_dbt) ?>
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($tsmdl_st_cl) ?></strong>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_ktdk_st_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_st_st_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_sm_st_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($tsmdl_dn_st_cl) ?>
                    </td>
                    <td></td>
                </tr>
                <?php
            }
            ?>
            <?php
            if ($NHAKHOA_MDL > 0) {
                ?>
                <tr>
                    <td style="text-align: left">
                        <br><strong>8. NHA KHOA (Mua độc lập)</strong>
                        <br><br> 8.1 Nha khoa cơ bản
                        <br><br> 8.2 Nha khoa đặc biệt
                        <br><br> 8.3 Cạo vôi răng
                        <br><br><br><br>
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($NHAKHOA_MDL) ?></strong>
                        <br><br> <?php echo get_price_in_vnd($nkmdl_cb) ?>
                        <br><br> <?php echo get_price_in_vnd($nkmdl_db) ?>
                        <br><br> <?php echo get_price_in_vnd($nkmdl_cvr) ?>
                        <br><br><br><br>
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($nkmdl_st_dbt) ?></strong>
                        <br><br> <?php echo get_price_in_vnd($nkmdl_cb_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($nkmdl_db_st_dbt) ?>
                        <br><br> <?php echo get_price_in_vnd($nkmdl_cvr_st_dbt) ?>
                        <br><br><br><br>
                    </td>
                    <td>
                        <br><strong><?php echo get_price_in_vnd($nkmdl_st_cl) ?></strong>
                        <br><br> <?php echo get_price_in_vnd($nkmdl_cb_st_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($nkmdl_db_st_cl) ?>
                        <br><br> <?php echo get_price_in_vnd($nkmdl_cvr_st_cl) ?>
                        <br><br><br><br>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
}
?>
<div class="col-sm-12 text-center">
    <div class="pagination">
        <?php // echo $this->pagination->create_links(); ?>
    </div>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>