<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
/*
 * Added 1/4/2014 by Dungnm
 */

class MY_Model extends CI_Model {

    protected $table_name;

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        if (!isset($this->table_name) || $this->table_name = '') {
            $this->table_name = get_Class($this);
        }
        //$this->table_name .= $this->db->dbprefix;
        $this->open();
    }

    //==========================================================================
    function get_ql_cl($options = array()) {
        $this->filter_data_ql_cl($options);

        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_CL, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_CL)->first_row();
//            $a = $this->db->last_query();
//            echo '<pre>';
//            print_r($a);
//            die;
        } else {
            if (isset($options['limit']) && isset($options['offset'])) {
                $this->db->limit($options['limit'], $options['offset']);
            } else if (isset($options['limit'])) {
                $this->db->limit($options['limit']);
            }

            return $q = $this->db->get(TBL_CL)->result();
        }

        $q->free_result();
    }

    //end
    //==========================================================================
    function filter_data_ql_cl($options = array()) {
        // join
        $select = '' . TBL_CL . '.*,';

        $this->db->select($select);
        // Join
        if (isset($options['join_' . TBL_CUSTOMERS])) {
            $this->db->join(TBL_CUSTOMERS, TBL_ORDERS . '.OD_kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }

        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $this->db->join(TBL_PRODUCTS_SIZE, TBL_PRODUCTS . '.goi_bh = ' . TBL_PRODUCTS_SIZE . '.id', 'left');
        }
        //end
        // where
        if (isset($options['CL_kh_id'])) {
            $this->db->where(TBL_CL . '.CL_kh_id = ', $options['CL_kh_id']);
        }
        if (isset($options['CL_quyenloi_bt'])) {
            $this->db->where(TBL_CL . '.CL_quyenloi_bt = ', $options['CL_quyenloi_bt']);
        }
        if (isset($options['phanloai_kh'])) {
            $this->db->where(TBL_CUSTOMERS . '.phanloai_kh = ', $options['phanloai_kh']);
        }
    }

    //end
    //==========================================================================
    function CommonGet($tbl = '', $options = array()) {

        if (isset($options['select'])) {
            $this->db->select($options['select']);
        } else {
            $this->db->select('*');
        }
        //======================================================================
        $this->GetDataOptionsCommon($tbl, $options);
        //======================================================================
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get($tbl, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get($tbl)->first_row();
        } else {
            // Limit
            if (isset($options['offset']) && isset($options['limit'])) {
                if ($options['offset'] > 1) {
                    //$this->db->limit($options['limit'], ($options['offset'] - 1) * $options['limit']);
                    $this->db->limit($options['limit'], $options['offset']);
                } else {

                    $this->db->limit($options['limit'], 0);
                }
            }
            return $q = $this->db->get($tbl)->result();
        }

        $q->free_result();
    }

    //==========================================================================
    function GetDataOptionsCommon($tbl = '', $options = array()) {

        if (isset($options['where'])) {
            $this->db->where($options['where']);
        }

        if (isset($options['like']) && !empty($options['like'])) {
            $this->db->where($options['like']);
        }

        if (isset($options['id']) && !empty($options['id'])) {
            $this->db->where($tbl . '.id', $options['id']);
        }

        if (isset($options['skip_id']) && !empty($options['skip_id'])) {
            $this->db->where($tbl . '.id != ', $options['skip_id']);
        }

        if (isset($options['a_cat_id'])) {
            $this->db->where_in($tbl . '.cat_id', $options['a_cat_id']);
        }

        // order by
        if (isset($options['a_order_by']) && !empty($options['a_order_by'])) {
            foreach ($options['a_order_by'] as $k => $v) {
                $this->db->order_by($k, $v);
            }
        } else if (isset($options['order_by'])) {

            $this->db->order_by($options['order_by']);
        } else {
            if ($tbl == TBL_PROVINCES || $tbl == TBL_WARDS) {
                //$this->db->order_by('ordering desc, totime_updated desc,id desc');
                $this->db->order_by('ordering desc');
            } else {
                $this->db->order_by('id desc');
            }
        }
        //limit
        if (isset($options['limit'])) {
            $this->db->limit($options['limit']);
        }
        // Get selectbox
        if (isset($options['get_selectbox'])) {
            $q = $this->db->get($tbl);
            return $q->result();
        }
    }

    //==========================================================================
    function CommonCreat($tbl = '', $data = array()) {
        if ($tbl != '') {
            $q = $this->db->insert($tbl, $data);
            if ($this->db->affected_rows($q) == 1) {
                return $this->db->insert_id();
            } else {
                return FALSE;
            }
            $q->free_result();
        } else {
            return FALSE;
        }
    }

    /*
     * -------------------------------------------------------------------------
     */

    /**
     * Count Common 
     * update: 19/09/2014
     * @param $data
     */
    function CommonCount($tbl = '', $options = array()) {
        if ($tbl != '') {
            $this->GetDataOptionsCommon($tbl, $options);
            return $q = $this->db->count_all_results($tbl);
            $q->free_result();
        } else {
            return FALSE;
        }
    }

    /*
     * -------------------------------------------------------------------------
     */

    /**
     * Update Common
     * update:
     * @param $data
     */
    function CommonUpdate($tbl = '', $id = 0, $data = array()) {
        if ($id > 0 && $tbl != '') {
            $this->db->where('id', $id);
            return ($q = $this->db->update($tbl, $data) == 1) ? TRUE : FALSE;
            $q->free_result();
        } else {
            return FALSE;
        }
    }

    /*
     * -------------------------------------------------------------------------
     */

    public function insert($data) {
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function update($data, $condition = array()) {
        //update theo array $condition
        if (!empty($condition) && is_array($condition)) {
            foreach ($condition as $key => $value)
                if (is_numeric($key))
                    $this->db->where($value);
                else
                    $this->db->where($key, $value);
        }else {
            //neu khong khai bao $condition thi trong $data phai co id//update theo id
            $this->db->where('id', $data['id']);
        }

        $this->db->update($this->table_name, $data);
    }

    public function delete($condition) {
        //delete theo array $condition
        if (!empty($condition) && is_array($condition)) {
            foreach ($condition as $key => $value)
                if (is_numeric($key))
                    $this->db->where($value);
                else
                    $this->db->where($key, $value);
        }elseif (is_numeric($condition)) {
            //delete theo id neu $condition = $id//number
            $this->db->where('id', $condition);
        }

        $this->db->delete($this->table_name);
    }

    public function count($condition = NULL) {
        if (!empty($condition) && is_array($condition)) {
            foreach ($condition as $key => $value)
                if (is_numeric($key))
                    $this->db->where($value);
                else
                    $this->db->where($key, $value);
        }else {
            //neu khong khai bao $condition: dem tat ca
            return $this->db->count_all($this->table_name);
        }
        return $this->db->count_all_results($this->table_name);
    }

    public function excuteQuery($sql) {
        $r = $this->db->query($sql);
        if (empty($r) || !is_object($r)) {
            return NULL;
        }
        return $r->result_array();
    }

    private $isConnected = false;

    public function open() {
        if ($this->isConnected) {
            if (empty($this->db)) {
                $CI = & get_instance();
                $CI->load->database();
                $this->db = $CI->db;
            }
            return;
        }

        $this->isConnected = true;
        $CI = & get_instance();
        if (empty($CI->db)) {
            $CI->load->database();
            $this->db = $CI->db;
        }

        //class model to table name:
        $s = '_Model';
        if (strpos($this->table_name, $s)) {
            //ten class model co cau truc: Tentable_Model, Ten_Table_Model
            $this->table_name = str_replace($s, '', $this->table_name);
            //ten table khong viet hoa
            $this->table_name = strtolower($this->table_name);
        }
        //$this->table_name = $this->dbprefix($this->table_name);
    }

    public function close() {
        if (!empty($this->db))
            $this->db->close();
        $this->isConnected = false;
    }

    public function dbprefix($table) {
        if (!$this->isConnected)
            $this->open();
        if (empty($this->db->dbprefix) || startsWith($table, $this->db->dbprefix))
            return $table;
        return $this->db->dbprefix($table);
    }

    public function beginTransaction() {
        $this->db->trans_begin();
    }

    public function finishTransaction() {
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
    }

    public function commit() {
        $this->db->trans_commit();
    }

    public function rollback() {
        $this->db->trans_rollback();
    }

}
