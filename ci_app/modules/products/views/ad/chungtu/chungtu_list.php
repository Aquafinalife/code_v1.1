<div class="page_header">
    <h1 class="fleft">Chứng từ</h1>
    <small class="fleft">"Thêm/sửa Chứng từ"</small>
    <span class="fright"><a href="<?php echo PRODUCTS_COUPON_ADMIN_BASE_URL; ?>" class="button close" style="padding:5px;" title="Đóng"><em></em><span>Đóng</span></a></span>
    <span class="fright"><a class="button add" href="<?php echo PRODUCTS_COUPON_ADMIN_ADD_URL; ?>"><em>&nbsp;</em>Thêm Chứng từ</a></span>
    <br class="clear"/>
</div>
<?php
echo form_open('', array('id' => 'submit_form'), array('id' => 0, 'from_list' => TRUE));
echo isset($uri) ? form_hidden('uri', $uri) : NULL;
echo form_hidden('back_url', PRODUCTS_COUPON_ADMIN_BASE_URL);
echo form_close();
//==============================================================================
$a_phanloai_bt = get_a_phanloai_bt();
?>
<form class="_col-sm-12 mb20 mt20" action="<?php echo site_url(PRODUCTS_COUPON_ADMIN_BASE_URL) ?>" method="get">
    <?php
    $G_pl_bt = isset($_GET['pl_bt'])?$_GET['pl_bt']:'';
    ?>
    <div class="col-sm-4">
        <select name="pl_bt" class="form-control js-example-disabled-results">
            <option value="">-- Chọn loại bồi thường --</option>
            <?php
            if (isset($a_phanloai_bt) && !empty($a_phanloai_bt)) {
                foreach ($a_phanloai_bt as $k => $v) {
                    $selected = ($G_pl_bt == $k) ? 'selected="selected"' : '';
                    ?>
                    <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                    <?php
                }
            }
            ?>
        </select>
    </div>
    <input type="submit" value="Tìm kiếm" class="btn btn-primary btn-sm"/>
</form>
<div class="form_content">    
    <table class="list" style="width: 100%;margin: 10px 0;">
        <?php $this->load->view('powercms/message'); ?>
        <tr>
            <th class="left">ID</th>
            <th class="left">TÊN CHỨNG TỪ</th>
            <th class="left">PHÂN LOẠI CHỨNG TỪ</th>
            <!--<th class="center" style="width: 7%">TRẠNG THÁI</th>-->
            <th class="center" style="width: 5%">CHỨC NĂNG</th>
        </tr>
        <?php
        if (isset($chungtu) && !empty($chungtu)) {
            $stt = 0;
            foreach ($chungtu as $index):
                $id = $index->id;
                $name = $index->name;
                $parent_id = $index->parent_id;
                $loai_ct = get_phanloai_bt($parent_id);
                $check = $index->status == STATUS_ACTIVE ? 'checked' : '';
                $style = $stt++ % 2 == 0 ? 'even' : 'odd';
                ?>
                <tr class="<?php echo $style; ?>">
                    <td class="left"><?php echo 'CT-' . $id; ?></td>
                    <td class="left"><?php echo $name; ?></td>
                    <td class="left"><?php echo $loai_ct; ?></td>
                    <td class="center">
                        <a class="edit" title="Sửa" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id; ?>, '<?php echo PRODUCTS_COUPON_ADMIN_EDIT_URL; ?>', 'edit');"><em>&nbsp;</em></a>
                        <a class="del" title="Xóa" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $id; ?>, '<?php echo PRODUCTS_COUPON_ADMIN_DELETE_URL; ?>', 'delete');"><em>&nbsp;</em></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php } ?>
        <tr class="list-footer">
            <th colspan="9">
                <div class="pagination col-sm-12 text-center mt20">
                    <div class="text-center">
                        <?php
                        if (isset($pagination)) {
                            echo $pagination;
                        }
                        ?>
                    </div>
                </div>
            </th>
        </tr>
    </table>
    <br class="clear"/>&nbsp;
</div>