<?php
if(isset($mb) && is_object($mb)){
    $id = $mb->id;
    $mb_name = $mb->name;
    $mb_mabenh = $mb->ma_benh;
}
?>
<?php 
echo form_open($submit_uri); 
if (isset($id)) echo form_hidden('id', $id);
echo form_hidden('is_add_edit_category', TRUE);
echo form_hidden('form', 'products_origin');
?>

<div class="page_header">
    <h1 class="fleft"><?php if(isset($header)) echo $header;?></h1>
    <small class="fleft">"Thêm/sửa xuất xứ"</small>
    <span class="fright"><a href="<?php echo PRODUCTS_ORIGIN_ADMIN_BASE_URL; ?>" class="button close" style="padding:5px;" title="Đóng"><em></em><span>Đóng</span></a></span>
    <br class="clear"/>
</div>

<div class="form_content">
    <?php $this->load->view('powercms/message'); ?>
    <ul class="tabs">
        <li><a href="#tab1">Nội dung</a></li>
    </ul>
<div class="tab_container">
    <div id="tab1" class="tab_content">
        <table>
            <tr><td class="title">Tên bệnh: (<span>*</span>)</td></tr>
            <tr>
                <td><?php echo form_input(array('name' => 'name', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset ($mb_name) ? $mb_name : set_value('name'))); ?></td>
            </tr>
            <tr><td class="title">Mã bệnh: (<span>*</span>)</td></tr>
            <tr>
                <td><?php echo form_input(array('name' => 'ma_benh', 'size' => '50', 'maxlength' => '255', 'style' => 'width:560px;', 'value' => isset ($mb_mabenh) ? $mb_mabenh : set_value('ma_benh'))); ?></td>
            </tr>
        </table>
    </div>

</div>
    <br class="clear"/>
    <div style="margin-top: 10px;"></div>
    <?php echo form_submit(array('name' => 'btnSubmit', 'value' => $button_name, 'class' => 'btn')); ?>
    <a style="margin-left: 20px;" class="btn btn-primary" href="<?php echo site_url(PRODUCTS_ORIGIN_ADMIN_BASE_URL)?>">Quay lại <span class="glyphicon glyphicon-share-alt"></span></a>
    <br class="clear"/>&nbsp;
</div>
<?php echo form_close(); ?>