<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('url_title')) {

    function url_title($str, $separator = 'dash', $lowercase = FALSE) {
        $separator = ($separator == 'dash') ? '-' : '_';
        $str = strip_tags($str);
        $from = array('—', 'À', 'Á', 'Ả', 'Ã', 'Ạ', 'Â', 'Ầ', 'Ấ', 'Ẩ', 'Ẫ', 'Ậ', 'Ă', 'Ằ', 'Ắ', 'Ẳ', 'Ẵ', 'Ặ', 'à', 'á', 'ả', 'ã', 'ạ', 'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ', 'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ'
            , 'È', 'É', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê', 'Ề', 'Ế', 'Ể', 'Ễ', 'Ệ', 'è', 'é', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ'
            , 'Ì', 'Í', 'Ỉ', 'Ĩ', 'Ị', 'ì', 'í', 'ỉ', 'ĩ', 'ị'
            , 'Ò', 'Ó', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ồ', 'Ố', 'Ổ', 'Ỗ', 'Ộ', 'ò', 'ó', 'ỏ', 'õ', 'ọ', 'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ'
            , 'Ơ', 'Ờ', 'Ớ', 'Ở', 'Ỡ', 'Ợ', 'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ'
            , 'Ù', 'Ú', 'Ủ', 'Ũ', 'Ụ', 'ù', 'ú', 'ủ', 'ũ', 'ụ'
            , 'Ư', 'Ừ', 'Ứ', 'Ử', 'Ữ', 'Ự', 'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự'
            , 'Ỳ', 'Ý', 'Ỷ', 'Ỹ', 'Ỵ', 'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ'
            , 'Đ', 'đ'
            , '(', ')', '[', ']', '{', '}', '~', '`', '!', '@'
            , '#', '$', '%', '^', '&', '*', '-', '_', '+', '='
            , '|', '\\', ':', ';', '"', '\'', '<', '>', ',', '.'
            , '/', '?', '“', '”'
            , '     ', '    ', '   ', '  ', ' '
        );
        $to = array(' ', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'
            , 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'
            , 'I', 'I', 'I', 'I', 'I', 'i', 'i', 'i', 'i', 'i'
            , 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'
            , 'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o'
            , 'U', 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 'u'
            , 'U', 'U', 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 'u', 'u'
            , 'Y', 'Y', 'Y', 'Y', 'Y', 'y', 'y', 'y', 'y', 'y'
            , 'D', 'd'
            , '', '', '', '', '', '', '', '', '', ''
            , '', '', '', '', '', '', '', '', '', ''
            , ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '
            , ' ', ' ', ' ', ' '
            , ' ', ' ', ' ', ' ', $separator
        );
        $str = trim(str_replace($from, $to, $str));

        // remove disallowed characters
        $pattern = '/[^A-Za-z 0-9~%.:_\-]/i';
        $replacement = '';
        $str = preg_replace($pattern, $replacement, $str);

        if ($lowercase === TRUE)
            $str = strtolower($str);
        return $str;
    }

}

if (!function_exists('current_full_url')) {

    function current_full_url() {
        $CI = & get_instance();

        $url = $CI->config->site_url($CI->uri->uri_string());
        return $_SERVER['QUERY_STRING'] ? $url . '?' . $_SERVER['QUERY_STRING'] : $url;
    }

}

if (!function_exists('get_a_customers_type')) {

    function get_a_customers_type() {
        $a = array(
            CANHAN => 'Cá nhân',
            DOANHNGHIEP => 'Doanh nghiệp',
        );
        return $a;
    }

}
if (!function_exists('get_customers_type')) {

    function get_customers_type($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case CANHAN:
                $output = 'Cá nhân';
                break;
            case DOANHNGHIEP:
                $output = 'Doanh nghiệp';
                break;
            default:
                $output = 'Cá nhân';
                break;
        }
        return $output;
    }

}

if (!function_exists('get_customers_type1')) {

    function get_customers_type1($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case VIP:
                $output = 'Vip';
                break;
            case THUONG:
                $output = 'Thường';
                break;
            default:
                $output = 'Thường';
                break;
        }
        return $output;
    }

}

if (!function_exists('get_gender')) {

    function get_gender($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case NAM:
                $output = 'Nam';
                break;
            case NU:
                $output = 'Nữ';
                break;
            case KHAC:
                $output = 'Khác';
                break;
            default:
                $output = 'Khác';
                break;
        }
        return $output;
    }

}

if (!function_exists('get_ma_benh')) {

    function get_ma_benh($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case 'TV_TTTBVV_TN':
                $output = 'Tử vong, TTTBVV(Do tai nạn)';
                break;
            case 'TCL_NN':
                $output = 'Trợ cấp lương ngày nghỉ';
                break;
            case 'CPYT_TN':
                $output = 'Chi phí y tế do tai nạn';
                break;
            case 'TV_TTTBVV_OB':
                $output = 'Tử vong, TTTBVV (Do ốm bệnh)';
                break;
            case 'DTNT_OB':
                $output = 'Điều trị nội trú (Do ốm bệnh)';
                break;
            case 'DTNGT_OB':
                $output = 'ĐIỀU TRỊ NGOẠI TRÚ (Do ốm bệnh)';
                break;
            case 'THAISAN_QLNT':
                $output = 'THAI SẢN (Trong quyền lợi nội trú)';
                break;
            case 'dtnt_cppt':
                $output = 'Chi phí phẫu thuật';
                break;
            default:
                $output = 'Tử vong, TTTBVV(Do tai nạn)';
                break;
        }
        return $output;
    }

}

if (!function_exists('get_bt_status')) {

    function get_bt_status($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case BT_CHOBOSUNG:
                $output = 'Chờ bổ sung chứng từ';
                break;
            case BT_CHODUYET:
                $output = 'Giải quyết bồi thường';
                break;
            case BT_PHEDUYET:
                $output = 'Phê duyệt bồi thường';
                break;
            case BT_DADUYET:
                $output = 'Đã duyệt bồi thường';
                break;
            case BT_THANHTOAN:
                $output = 'Thanh toán';
                break;
            case BT_DATRATIEN:
                $output = 'Đã trả tiền và trả hồ sơ gốc';
                break;
            default:
                $output = 'Empty';
                break;
        }
        return $output;
    }

}
if (!function_exists('get_ndbh_quanhe')) {

    function get_ndbh_quanhe($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case BANTHAN:
                $output = 'Bản thân';
                break;
            case NGUOITHAN:
                $output = 'Người thân';
                break;
            case NHANVIEN:
                $output = 'Nhân viên';
                break;
            default:
                $output = 'Người thân';
                break;
        }
        return $output;
    }

}
if (!function_exists('get_qlbh')) {

    function get_qlbh($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case NOITRU:
                $output = 'Nội trú';
                break;
            case NGOAITRU:
                $output = 'Ngoại trú';
                break;
            default:
                $output = 'Khác';
                break;
        }
        return $output;
    }

}
if (!function_exists('get_phuongthuc_bt')) {

    function get_phuongthuc_bt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case BT_HOANTRA:
                $output = 'Trường hợp thanh toán sau';
                break;
            case BT_BAOLANH_VIENPHI:
                $output = 'Bảo lãnh viện phí';
                break;
            default:
                $output = '';
                break;
        }
        return $output;
    }

}
if (!function_exists('get_hinhthuc_nhan_bt')) {

    function get_hinhthuc_nhan_bt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case TIEN_MAT:
                $output = 'Tiền mặt';
                break;
            case CHUYEN_KHOAN:
                $output = 'Chuyển khoản';
                break;
            default:
                $output = 'Tiền mặt';
                break;
        }
        return $output;
    }

}
//end
if (!function_exists('get_a_orders_status')) {

    function get_a_orders_status() {
        $a = array(
            BT_CHOBOSUNG => 'Chờ bổ sung chứng từ',
            BT_CHODUYET => 'Giải quyết bồi thường',
            BT_PHEDUYET => 'Phê duyệt bồi thường',
            BT_DADUYET => 'Đã duyệt bồi thường',
            BT_THANHTOAN => 'Bồi thường thanh toán',
            BT_DATRATIEN => 'Đã trả tiền và trả hồ sơ gốc',
            BT_DONG => 'Bồi thường đóng',
            BT_HUY => 'Bồi đã hủy',
        );
        return $a;
    }

}
//end
if (!function_exists('get_a_phanloai_bt1')) {

    function get_a_phanloai_bt1() {
        $a = array(
            BT_DTNT_OB, BT_DTNGT_OB,
        );
        return $a;
    }

}
//end
if (!function_exists('get_a_phanloai_bt2')) {

    function get_a_phanloai_bt2() {
        $a = array(
            BT_TVTTTBVV_OB, BT_TVTTTBVV_TN,
        );
        return $a;
    }

}
//end
if (!function_exists('get_a_phanloai_bt')) {

    function get_a_phanloai_bt() {
        $a = array(
            BT_DTNT_OB => 'ĐIỀU TRỊ NỘI TRÚ (Do ốm bệnh)',
            BT_THAISAN_QLNT => 'THAI SẢN (Trong quyền lợi nội trú) ',
            BT_DTNGT_OB => 'ĐIỀU TRỊ NGOẠI TRÚ (Do ốm bệnh) ',
            BT_THAISAN_MDL => ' THAI SẢN (Mua độc lập) ',
            BT_TVTTTBVV_TN => 'TỬ VONG, TTTBVV (Do tai nạn) ',
            BT_TCL_NN => 'TRỢ CẤP LƯƠNG NGÀY NGHỈ',
            BT_CPYT_TN => 'CHI PHI Y TẾ DO TAI NẠN',
            BT_TVTTTBVV_OB => 'TỬ VONG, TTTBVV (Do ốm bệnh) ',
            BT_NHAKHOA_MDL => 'NHA KHOA (Mua độc lập) ',
        );
        return $a;
    }

}
//end
if (!function_exists('get_phanloai_bt')) {

    function get_phanloai_bt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case BT_DTNT_OB:
                $output = 'ĐIỀU TRỊ NỘI TRÚ (Do ốm bệnh)';
                break;
            case BT_DTNGT_OB:
                $output = 'ĐIỀU TRỊ NGOẠI TRÚ (Do ốm bệnh)';
                break;
            case BT_THAISAN_QLNT:
                $output = 'THAI SẢN (Trong quyền lợi nội trú)';
                break;
            case BT_THAISAN_MDL:
                $output = 'THAI SẢN (Mua độc lập)';
                break;
            case BT_TVTTTBVV_TN:
                $output = 'TỬ VONG, TTTBVV (Do tai nạn)';
                break;
            case BT_TCL_NN:
                $output = 'TRỢ CẤP LƯƠNG NGÀY NGHỈ';
                break;
            case BT_CPYT_TN:
                $output = 'CHI PHI Y TẾ DO TAI NẠN';
                break;
            case BT_TVTTTBVV_OB:
                $output = 'TỬ VONG, TTTBVV (Do ốm bệnh)';
                break;
            case BT_NHAKHOA_MDL:
                $output = 'NHA KHOA (Mua độc lập)';
                break;
            default:
                $output = 'Empty';
                break;
        }
        return $output;
    }

}

//end
//==============================================================================
if (!function_exists('get_a_phuongthuc_bt')) {

    function get_a_phuongthuc_bt() {
        $a = array(
            BT_HOANTRA => 'Trường hợp thanh toán sau',
            BT_BAOLANH_VIENPHI => 'Bảo lãnh viện phí',
        );
        return $a;
    }

}
//==============================================================================
if (!function_exists('get_a_nhantien_bt')) {

    function get_a_nhantien_bt() {
        $a = array(
            BT_CHUYENKHOAN => 'Chuyển khoản',
            BT_TIENMAT => 'Tiền mặt',
        );
        return $a;
    }

}
//==============================================================================
if (!function_exists('get_hinhthuc_nhantien_bt')) {

    function get_hinhthuc_nhantien_bt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case BT_CHUYENKHOAN:
                $output = 'Chuyển khoản';
                break;
            case BT_TIENMAT:
                $output = 'Tiền mặt';
                break;
            default:
                $output = 'Empty';
                break;
        }
        return $output;
    }

}
//==============================================================================
if (!function_exists('get_a_nhan_hoso')) {

    function get_a_nhan_hoso() {
        $a = array(
            NHS_TRUCTIEP => 'Trực tiếp',
            NHS_ONSITE => 'Onsite',
            NHS_BUUDIEN => 'Bưu điện',
        );
        return $a;
    }

}
//==============================================================================
if (!function_exists('get_nhan_hoso')) {

    function get_nhan_hoso($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case NHS_TRUCTIEP:
                $output = 'Trực tiếp';
                break;
            case NHS_ONSITE:
                $output = 'Onsite';
                break;
            case NHS_BUUDIEN:
                $output = 'Bưu điện';
                break;
            default:
                $output = 'Empty';
                break;
        }
        return $output;
    }

}
//==============================================================================
if (!function_exists('get_phanloai_bt_txt')) {

    function get_phanloai_bt_txt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case BT_DTNT_OB:
                $output = 'DTNT_OB';
                break;
            case BT_DTNGT_OB:
                $output = 'DTNGT_OB';
                break;
            case BT_THAISAN_QLNT:
                $output = 'THAISAN_QLNT';
                break;
            case BT_THAISAN_MDL:
                $output = 'THAISAN_MDL';
                break;
            case BT_TVTTTBVV_TN:
                $output = 'TV_TTTBVV_TN';
                break;
            case BT_TCL_NN:
                $output = 'TCL_NN';
                break;
            case BT_CPYT_TN:
                $output = 'CPYT_TN';
                break;
            case BT_TVTTTBVV_OB:
                $output = 'TV_TTTBVV_OB';
                break;
            case BT_NHAKHOA_MDL:
                $output = 'NHAKHOA_MDL';
                break;
            default:
                $output = 'Empty';
                break;
        }
        return $output;
    }

}
//end
if (!function_exists('get_field_st_dbt')) {

    function get_field_st_dbt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case BT_DTNT_OB:
                $output = 'DTNT_OB';
                break;
            case BT_DTNGT_OB:
                $output = 'DTNGT_OB';
                break;
            case BT_THAISAN_QLNT:
                $output = 'THAISAN_QLNT';
                break;
            case BT_THAISAN_MDL:
                $output = 'THAISAN_MDL';
                break;
            case BT_TVTTTBVV_TN:
                $output = 'TV_TTTBVV_TN';
                break;
            case BT_TCL_NN:
                $output = 'TCL_NN';
                break;
            case BT_CPYT_TN:
                $output = 'CPYT_TN';
                break;
            case BT_TVTTTBVV_OB:
                $output = 'TV_TTTBVV_OB';
                break;
            case BT_NHAKHOA_MDL:
                $output = 'NHAKHOA_MDL';
                break;
            default:
                $output = 'Empty';
                break;
        }
        return $output;
    }

}
//end
if (!function_exists('get_orders_status')) {

    function get_orders_status($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case BT_CHOBOSUNG:
                $output = 'Chờ bổ sung chứng từ';
                break;
            case BT_CHODUYET:
                $output = 'Giải quyết bồi thường';
                break;
            case BT_PHEDUYET:
                $output = 'Phê duyệt bồi thường';
                break;
            case BT_DADUYET:
                $output = 'Đã duyệt bồi thường';
                break;
            case BT_THANHTOAN:
                $output = 'Thanh toán';
                break;
            case BT_DATRATIEN:
                $output = 'Đã trả tiền';
                break;
            case BT_HUY:
                $output = 'Đã hủy';
                break;
            default:
                $output = 'Empty';
                break;
        }
        return $output;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_phanloai_bt1')) {

    function get_a_phanloai_bt1() {
        $a = array(
            'TV_TTTBVV_TN', 'TCL_NN', 'CPYT_TN', 'TV_TTTBVV_OB', 'DTNGT_OB',
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_qlbt_TV_TTTBVV_TN')) {

    function get_a_qlbt_TV_TTTBVV_TN() {
        $a = array(
            'tv_tttbvv_tn_tv', 'tv_tttbvv_tn_tt',
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_loai_benh_DTNT_OB')) {

    function get_a_loai_benh_DTNT_OB() {
        $a = array(
            'dtnt_tvpn', 'dtnt_tgpn', 'dtnt_cppt', 'dtnt_tk_nv', 'dtnt_sk_nv',
            'dtnt_cpyt_tn', 'dtnt_tcvp', 'dtnt_tcmt', 'dtnt_xct',
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_loai_benh_DTNGT_OB')) {

    function get_a_loai_benh_DTNGT_OB() {
        $a = array(
            //'DTNGT_OB','dtngt_dbh', 'dtngt_nk', 'dtngt_cvr',
            'dtngt_dbh', 'dtngt_nk', 'dtngt_cvr',
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_loai_benh_THAISAN_QLNT')) {

    function get_a_loai_benh_THAISAN_QLNT() {
        $a = array(
            'tsqlnt_ktdk', 'tsqlnt_st', 'tsqlnt_sm', 'tsqlnt_dn',
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_loai_benh_THAISAN_MDL')) {

    function get_a_loai_benh_THAISAN_MDL() {
        $a = array(
            'tsmdl_ktdk', 'tsmdl_st', 'tsmdl_sm', 'tsmdl_dn',
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_loai_benh_NHAKHOA_MDL')) {

    function get_a_loai_benh_NHAKHOA_MDL() {
        $a = array(
            'nkmdl_cb', 'nkmdl_db',
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_loai_benh_sn')) {

    function get_a_loai_benh_sn() {
        $a = array('TCL_NN', 'dtnt_tvpn', 'dtnt_tgpn', 'dtnt_cpyt_tn', 'dtnt_tcvp');
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_lydo_khong_bt')) {

    function get_a_lydo_khong_bt() {
        $a = array(
            BT_TUCHOI_TOANBO => 'TOÀN BỘ - Từ Chối Toàn Bộ Bồi Thường',
            '1' => '1.Vượt quá số tiền tối da cho một lần khám',
            '2' => '2.Vượt quá số lượng hàng năm theo loại Quyền Lợi',
            '3' => '3.Các chi phí xét nghiệm không thuộc phạm vi bảo hiểm ',
            '4' => '4.Không thuộc quyền lợi khám thai định kỳ',
            '5' => '5.Vượt quá số tiền tối đa cho một ngày',
            '6' => '6.Không cung cấp được hóa đơn, biên lại tài chính/phiếu thu',
            '7' => '7.Không thuộc quyền lợi chăm sóc răng cơ bản',
            '8' => '8.Thuốc điều trị không thuộc phạm vi bảo hiểm ( thuốc bổ, thuốc dự phòng, thuốc không phù hợp với chuẩn đoán...)',
            '9' => '9.Hóa đơn/biên lại thiếu thông tin trên các mục',
            '10' => '10.Kiểm tra và khám sức khỏe',
            '11' => '11.Đơn thuốc kê sản phẩm mỹ phẩm',
            '12' => '12.Đơn thuốc/phiếu khám thiếu thông tin trên các mục',
            '13' => '13.Chi phí dịch vụ',
            '14' => '14.Không cung cấp bảng kê chi tiết chi phí phát sinh/bảng kê không trùng với số tiền phát sinh',
            '15' => '15.Chi phí điều trị bất hợp lý, không cần thiết về mặt y tế, không theo thông lệ hợp lý và không theo chỉ định của bác sĩ',
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_lydo_khong_bt')) {

    function get_lydo_khong_bt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case '1':
                $output = 'Lý do 1';
                break;
            case '2':
                $output = 'Lý do 2';
                break;
            case '3':
                $output = 'Lý do 3';
                break;
            default:
                $output = 'Empty';
                break;
        }
        return $output;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_quyenloi_bt_not_select')) {

    function get_a_quyenloi_bt_not_select() {
        $a = array(TCL_NN, CPYT_TN,);
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_loai_benh_sn_field')) {

    function get_loai_benh_sn_field($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case 'TCL_NN':
                $output = 'tcl_nn_sn';
                break;
            case 'dtnt_tvpn':
                $output = 'dtnt_tvpn_sn';
                break;
            case 'dtnt_tvpn':
                $output = 'dtnt_tvpn_sn';
                break;
            case 'dtnt_cpyt_tn':
                $output = 'dtnt_cpyt_tn_sn';
                break;
            case 'dtnt_tcvp':
                $output = 'dtnt_tcvp_sn';
                break;
            default:
                $output = '';
                break;
        }
        return $output;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_status_bt1')) {

    function get_a_status_bt1() {
        $a = array(BT_CHOBOSUNG, BT_THANHTOAN);
        return $a;
    }

}
//end
if (!function_exists('get_a_qlnt')) {

    function get_a_qlnt() {
        $a = array('tsqlnt_ktdk', 'tsqlnt_st', 'tsqlnt_sm', 'tsqlnt_dn');
        return $a;
    }

}
//end
if (!function_exists('get_a_loai_benh_slk')) {

    function get_a_loai_benh_slk() {
        $a = array('DTNGT_OB');
        return $a;
    }

}
//end
if (!function_exists('get_loai_benh_slk_field')) {

    function get_loai_benh_slk_field($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case 'DTNGT_OB':
                $output = 'dtngt_slk';
                break;
            default:
                $output = '';
                break;
        }
        return $output;
    }

}
//end
if (!function_exists('get_loai_benh_st1lk_field')) {

    function get_loai_benh_st1lk_field($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case 'DTNGT_OB':
                $output = 'dtngt_st1lk';
                break;
            default:
                $output = '';
                break;
        }
        return $output;
    }

}
//end
if (!function_exists('get_ten_qlbt')) {

    function get_ten_qlbt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case 'TV_TTTBVV_TN':
                $output = 'Tử vong, TTTBVV (Do tai nạn)';
                break;
            case 'TCL_NN':
                $output = 'Trợ cấp lương ngày nghỉ';
                break;
            case 'CPYT_TN':
                $output = 'Chi phí y tế do tai nạn';
                break;
            case 'TV_TTTBVV_OB':
                $output = 'Tử vong, TTTBVV (Do ốm bệnh)';
                break;
            case 'DTNT_OB':
                $output = 'Điều trị nội trú (Do ốm bệnh)';
                break;
            case 'dtnt_dbh':
                $output = 'DTNT_OB - Đồng bảo hiểm';
                break;
            case 'dtnt_tvpn':
                $output = 'DTNT_OB - Tiền viện phí ngày';
                break;
            case 'dtnt_tgpn':
                $output = 'DTNT_OB - Tiền giường, phòng / ngày';
                break;
            case 'dtnt_cppt':
                $output = 'DTNT_OB - Chi phí phẫu thuật';
                break;
            case 'dtnt_tk_nv':
                $output = 'Quyền lợi khám trước khi nhập viện';
                break;
            case 'dtnt_sk_nv':
                $output = 'Quyền lợi khám sau khi nhập viện';
                break;
            case 'dtnt_cpyt_tn':
                $output = 'DTNT_OB - Chi phí y tá tại nhà';
                break;
            case 'dtnt_tcvp':
                $output = 'DTNT_OB - Trợ cấp viện phí';
                break;
            case 'dtnt_tcmt':
                $output = 'DTNT_OB - Trợ cấp mai táng';
                break;
            case 'dtnt_xct':
                $output = 'DTNT_OB - Xe cứu thương';
                break;
            case 'THAISAN_QLNT':
                $output = 'THAI SẢN (Trong quyền lợi nội trú)';
                break;
            case 'tsqlnt_ktdk':
                $output = 'THAISAN_QLNT - Chi phí khám thai định kỳ';
                break;
            case 'tsqlnt_st':
                $output = 'THAISAN_QLNT - Sinh thường';
                break;
            case 'tsqlnt_sm':
                $output = 'THAISAN_QLNT - Sinh mổ';
                break;
            case 'tsqlnt_dn':
                $output = 'THAISAN_QLNT - Dưỡng nhi';
                break;
            case 'DTNGT_OB':
                $output = 'ĐIỀU TRỊ NGOẠI TRÚ (Do ốm bệnh)';
                break;
            case 'dtngt_dbh':
                $output = 'DTNGT_OB - Đồng bảo hiểm';
                break;
            case 'dtngt_nk':
                $output = 'DTNGT_OB - Nha khoa';
                break;
            case 'dtngt_cvr':
                $output = 'DTNGT_OB - Cạo vôi răng';
                break;
            case 'THAISAN_MDL':
                $output = 'THAI SẢN (Mua độc lập)';
                break;
            case 'tsmdl_dbh':
                $output = 'THAISAN_MDL - Đồng bảo hiểm';
                break;
            case 'tsmdl_ktdk':
                $output = 'THAISAN_MDL - Chi phí khám thai định kỳ';
                break;
            case 'tsmdl_st':
                $output = 'THAISAN_MDL - Sinh thường';
                break;
            case 'tsmdl_sm':
                $output = 'THAISAN_MDL - Sinh mổ';
                break;
            case 'tsmdl_dn':
                $output = 'THAISAN_MDL - Dưỡng nhi';
                break;
            case 'NHAKHOA_MDL':
                $output = 'NHA KHOA (Mua độc lập)';
                break;
            case 'nkmdl_cb':
                $output = 'Nha khoa cơ bản (tiền khám chữa răng,trám răng,cạo vôi răng)';
                break;
            case 'nkmdl_db':
                $output = 'Nha khoa đặc biệt (tiền làm răng thẩm mỹ,cầu răng,đồng bảo hiểm 50%)';
                break;
            default:
                $output = 'Empty';
                break;
        }
        return $output;
    }

}
if (!function_exists('get_qlbt_field_by_qlbt')) {

    function get_qlbt_field_by_qlbt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case 'tv_tttbvv_tn_tv':
                $output = TV_TTTBVV_TN;
                break;
            case 'tv_tttbvv_tn_tt':
                $output = TV_TTTBVV_TN;
                break;
            case 'tv_tttbvv_ob_tv':
                $output = TV_TTTBVV_OB;
                break;
            case 'tv_tttbvv_ob_tt':
                $output = TV_TTTBVV_OB;
                break;
            case TCL_NN:
                $output = TCL_NN;
                break;
            case nkmdl_db:
                $output = NHAKHOA_MDL;
                break;
            case nkmdl_cb:
                $output = NHAKHOA_MDL;
                break;
            case nkmdl_cvr:
                $output = NHAKHOA_MDL;
                break;
            default:
                $output = $int;
                break;
        }
        return $output;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_bt_status1')) {

    function get_a_bt_status1() {
        $a = array(
            BT_PHEDUYET, BT_DADUYET, BT_THANHTOAN, BT_DATRATIEN,
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_bt_thanhtoan')) {

    function get_a_bt_thanhtoan() {
        $a = array(
            BT_THANHTOAN, BT_DATRATIEN, BT_DONG
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_bt_huy')) {

    function get_a_bt_huy() {
        $a = array(
            BT_CHOBOSUNG, BT_CHODUYET, BT_PHEDUYET,
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_a_mqh')) {

    function get_a_mqh() {
        $a = array(
            DN_NV => 'Doanh nghiệp - Nhân viên',
            DN_NT => 'Doanh nghiệp - Người thân',
            DN_NT_BM => 'Doanh nghiệp - Người thân - Bố mẹ',
            DN_NT_CC => 'Doanh nghiệp - Người thân - Con cái',
            DN_NT_VC => 'Doanh nghiệp - Người thân - Vợ chồng',
            CN_CN => 'Cá nhân - Cá nhân',
            CN_NT => 'Cá nhân - Người thân',
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_mqh')) {

    function get_mqh($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case DN_NV:
                $output = 'Nhân viên';
                break;
            case DN_NT:
                $output = 'Người thân';
                break;
            case CN_CN:
                $output = 'Cá nhân';
                break;
            case CN_NT:
                $output = 'Người thân';
                break;
            default:
                $output = '';
                break;
        }
        return $output;
    }

}
//==============================================================================
if (!function_exists('get_a_csyt_type')) {

    function get_a_csyt_type() {
        $a = array(
            '1' => 'Bệnh viện công - Trung Ương',
            '10' => 'Bệnh viện công - Tuyến Tỉnh',
            '11' => 'Bệnh viện công - Bán Công',
            '8' => 'Phòng khám công - Trung tâm y tế dự phòng thành phố',
            '2' => 'Bệnh viện công - khoa tự nguyện',
            '9' => 'Bệnh viện ngành',
            '3' => 'Bệnh viện tư nhân',
            '4' => 'Bệnh viện quốc tế',
            '12' => 'Phòng khám tư nhân',
            '5' => 'Phòng khám nha khoa',
            '6' => 'Phòng khám đa khoa',
            '7' => 'Phòng khám chuyên khoa',
        );
        return $a;
    }

}
//end
//==============================================================================
if (!function_exists('get_field_thaisan')) {

    function get_field_thaisan($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case SINH_THUONG:
                $output = tsqlnt_st;
                break;
            case SINH_MO:
                $output = tsqlnt_sm;
                break;
            default:
                $output = $int;
                break;
        }
        return $output;
    }

}
//==============================================================================
if (!function_exists('get_phanloai_thaisan')) {

    function get_phanloai_thaisan($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case SINH_THUONG:
                $output = 'Sinh thường';
                break;
            case SINH_MO:
                $output = 'Sinh mổ';
                break;
            default:
                $output = $int;
                break;
        }
        return $output;
    }

}
//==============================================================================
//end
if (!function_exists('get_code_bt')) {

    function get_code_bt($int = '') {
        $output = $output1 = '';

        if ($int < 10) {
            $output = '00000' . $int;
        } elseif ($int < 100) {
            $output = '0000' . $int;
        } elseif ($int < 1000) {
            $output = '000' . $int;
        } elseif ($int < 10000) {
            $output = '00' . $int;
        } elseif ($int < 100000) {
            $output = '0' . $int;
        } else {
            $output = $int;
        }
        return $output;
    }

}
//==============================================================================
//end
/* End of file PIXELPLANT_url_helper.php */
/* Location: ./application/helpers/PIXELPLANT_url_helper.php */ 