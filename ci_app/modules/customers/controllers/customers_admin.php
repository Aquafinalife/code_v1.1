<?php

class Customers_Admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        //modules::run('auth/auth/validate_login', $this->router->fetch_module());
        $this->_layout = 'admin_ui/layout/main';
        //$this->_view_data['url'] = CUSTOM_ADMIN_BASE_URL;        
        $this->load->model('products/products_model');
        $this->ad_check_login();
        //======================================================================
        $ss_ql_all = $this->phpsession->get('ss_ql_all');
        $ss_ql_kh = $this->phpsession->get('ss_ql_kh');
        if ($ss_ql_all != QL_CHECKED) {
            if ($ss_ql_kh != QL_CHECKED) {
                redirect('dashboard');
            }
        }
    }

    //==========================================================================
    // Lấy thông tin kh
    function get_kh($offset = 0, $options = array()) {
        $data = array();

        // Lấy thông tin bh
        $limit = LIMIT_DEFAULT;
        $data_get = array(
                //'join_' . TBL_DN_BBH => TRUE,
        );
        //end
        /*
         * // Lấy tất cả kh
          $kh_all = $this->customers_model->get_kh($data_get);
          if (!empty($kh_all)) {
          $data['kh_all'] = $kh_all;
          }
         */
        // Filter
        //======================================================================
        //$data_filter = $this->get_data_filter_bt();
        $key = isset($_GET['key']) ? $_GET['key'] : '';
        if ($key != '') {
            $filter_key_search = "(`" . DB_PREFIX . TBL_CUSTOMERS . "`.`congty_canhan` LIKE '%" . $key . "%'";
            $filter_key_search .= " OR `" . DB_PREFIX . TBL_CUSTOMERS . "`.`mst_cmt` LIKE '%" . $key . "%'";
            $filter_key_search .= " OR `" . DB_PREFIX . TBL_CUSTOMERS . "`.`id` LIKE '%" . $key . "%')";

            $data_get['like'] = $filter_key_search;
        }
        //----------------------------------------------------------------------
        $kh = isset($_GET['kh']) ? $_GET['kh'] : '';
        if ($kh != '' && $kh > 0) {
            $data_get['kh_id'] = $kh;
        }
        //----------------------------------------------------------------------
        $pl_kh = isset($_GET['pl_kh']) ? $_GET['pl_kh'] : '';
        if ($pl_kh != '' && $pl_kh > 0) {
            $data_get['phanloai_kh'] = $pl_kh;
        }
        $data_get['url_export'] = '?kh_dbh=' . $kh_dbh . '&goi_bh=' . $goi_bh . '&cn=' . $cn . '&pb=' . $pb . '';
        //======================================================================
        //$data_get = array_merge($data_get, $data_filter);
        // end 
        $total_row = $this->customers_model->count_kh($data_get);
        //end
        // pagination
        $paging_config = array(
            'base_url' => site_url('dashboard') . '/' . $this->uri->segment(2),
            'total_rows' => $total_row,
            'per_page' => $limit,
            'uri_segment' => 3,
            'use_page_numbers' => TRUE,
            'first_link' => __('IP_paging_first'),
            'last_link' => __('IP_paging_last'),
            'num_links' => 1,
        );
        if (isset($_GET) && $_GET != '') {
            $paging_config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $paging_config['first_url'] = $paging_config['base_url'] . '?' . http_build_query($_GET);

        $this->pagination->initialize($paging_config);
        $data_get['offset'] = ($offset > 0) ? ($offset - 1) * ($limit) : $offset;
        $data_get['limit'] = $limit;
        $data['pagination'] = $this->pagination->create_links();
        //======================================================================
        // Lấy kh
        $kh = $this->customers_model->get_kh($data_get);
        if (!empty($kh)) {
            $data['kh'] = $kh;
        }
        //======================================================================
        $data['total_rows'] = $total_row;
        $data['total_pages'] = ceil($total_row / $limit);
        $data['page'] = ($offset > 0) ? $offset : 1;

        $data['url_export'] = $data_get['url_export'];
        $data['scripts'] = $this->scripts_select2();
        //======================================================================
        $tpl = 'ad/kh/ad_list_kh';
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Khách hàng' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    function ad_get_reports() {
        $data = array();

        $phanloai_kh = isset($_GET['phanloai_kh']) ? $_GET['phanloai_kh'] : '';
        // Nếu kh là doanh nghiệp
        if ($phanloai_kh == DOANHNGHIEP) {
            $tpl = 'admin/reports/reports_list_dn';
            // Lấy doanh nghiệp 
            $customers_b = $this->customers_model->get_customers(array('phanloai_kh' => DOANHNGHIEP));
            if (!empty($customers_b)) {
                $option_dn = '';
                foreach ($customers_b as $index) {
                    $id = $index->id;
                    $parent_id = $index->parent_id;
                    $congty_canhan = $index->congty_canhan;
                    $selected = ($_GET['dn'] == $id) ? 'selected="selected"' : '';
                    if ($parent_id == FALSE) {
                        $option_dn .= '<option ' . $selected . ' value="' . $id . '">' . $congty_canhan . '</option>';
                    }
                }
                $data['selectbox_dn'] = '<select type="2" selectbox_name="dn" name="dn" class="js-example-disabled-results" id="filter_doanhnghiep">
                            <option value="">Chọn doanh nghiệp mua BH</option>
                            ' . $option_dn . '
                        </select>';
                // Nếu tìm theo 1 doanh nghiệp
                //==============================================================
                if ((isset($_GET['cn']) && $_GET['cn'] != '') || (isset($_GET['dn']) && $_GET['dn'] != '')) {
                    if (isset($_GET['dn']) && $_GET['dn'] != '') {
                        $dn_id = $_GET['dn'];
                    }

                    if (!empty($customers_b)) {
                        $option_cn = '';
                        foreach ($customers_b as $index) {
                            $id = $index->id;
                            $congty_canhan = $index->congty_canhan;
                            $parent_id = $index->parent_id;
                            $selected = ($_GET['cn'] == $id) ? 'selected="selected"' : '';
                            if ($dn_id == $parent_id) {
                                $option_cn .= '<option ' . $selected . ' value="' . $id . '">' . $congty_canhan . '</option>';
                            }
                        }
                        $data['selectbox_cn'] = '<select type="2" selectbox_name="cn" name="cn" class="js-example-disabled-results" id="filter_chinhanh">
                            <option value="">Chọn công ty con/Chi nhánh BH</option>
                            ' . $option_cn . '
                        </select>';
                    }
                }
                //==============================================================
            }
            //==================================================================
            // Nếu tìm theo 1 doanh nghiệp
            $dn_id = isset($_GET['dn']) ? $_GET['dn'] : '';
            if ($dn_id > 0 && $dn_id != '') {
                $cn_id = isset($_GET['cn']) ? $_GET['cn'] : '';
                // Nếu tìm theo 1 chi nhánh
                if ($cn_id > 0) {
                    $dn_id = $cn_id;
                }
                // Lấy thông tin doanh nghiệp
                $data_get = array(
                    'id' => $dn_id,
                    'get_one' => TRUE,
                    'skip_join' => TRUE,
                );
                $dn_mbh = $this->customers_model->get_customers($data_get);

                if (is_object($dn_mbh)) {
                    $data['dn_mbh'] = $data['kh_mbh'] = $dn_mbh;
                    // Lấy id khách hàng được mua BH bới dn này
                    $data_get = array(
                        'custom_buy_id' => $dn_id,
                    );

                    $products = $this->products_model->get_products_by_options($data_get);

                    if (!empty($products)) {
                        $data['products'] = $products;
                        $str_goi_id = $str_kh_dbh_id = '';
                        foreach ($products as $index) {
                            $goi_id = $index->size;
                            $kh_dbh_id = $index->custom_duoc_id;
                            $str_goi_id .= $goi_id . ',';
                            $str_kh_dbh_id .= $kh_dbh_id . ',';
                        }

                        $str_goi_id .= 0;
                        $str_kh_dbh_id .= 0;
                        $a_goi_id = array_unique(@explode(',', $str_goi_id));
                        $a_kh_dbh_id = array_unique(@explode(',', $str_kh_dbh_id));

                        // Lấy thông tin khách hàng được bh
                        $data_get = array(
                            'a_id' => $a_kh_dbh_id,
                        );
                        $kh_dbh = $this->customers_model->get_customers($data_get);
                        if (!empty($kh_dbh)) {
                            $data['kh_dbh'] = $kh_dbh;
                        }
                        // Lấy thông tin các gói
                        $data_get = array(
                            'a_id' => $a_goi_id,
                        );
                        $goi_bh = $this->products_model->get_products_size_by_options($data_get);
                        if (!empty($goi_bh)) {
                            $data['goi_bh'] = $goi_bh;
                        }
                    }
                }
            }
        } else {

            $tpl = 'admin/reports/reports_list_cn';
            $key = isset($_GET['key']) ? $_GET['key'] : '';
            if ($key != '') {
                // Lấy kh được bh theo cmt/hộ chiếu
                $data_get = array(
                    'mst_cmt' => trim($key),
                    'get_one' => TRUE,
                    'skip_join' => TRUE,
                );
                $kh_dbh = $this->customers_model->get_customers($data_get);

                if (is_object($kh_dbh)) {
                    $data['kh_dbh'] = $kh_dbh;
                    $kh_dbh_id = $kh_dbh->id;

                    // Lấy thông tin bảo hiểm
                    $data_get = array(
                        'custom_duoc_id' => $kh_dbh_id,
                        'get_one' => TRUE,
                    );
                    $products = $this->products_model->get_products_by_options($data_get);

                    if (is_object($products)) {
                        $kh_mbh_id = $products->custom_buy_id;
                        $goi_id = $products->size;

                        // Lấy thông tin kh mua bh
                        $data_get = array(
                            'id' => $kh_mbh_id,
                            'get_one' => TRUE,
                            'skip_join' => TRUE,
                        );
                        $kh_mbh = $this->customers_model->get_customers($data_get);
                        if (is_object($kh_mbh)) {
                            $data['kh_mbh'] = $kh_mbh;
                        }
                        //end
                        // Lấy thông tin gói
                        $data_get = array(
                            'id' => $goi_id,
                            'get_one' => TRUE,
                        );
                        $packages = $this->products_model->get_products_size_by_options($data_get);
                        if (is_object($packages)) {
                            $data['goi_bh'] = $packages;
                        }
                        //end
                    }
                }
            }
        }
        //======================================================================
        // Lấy toàn bộ khách hàng cá nhân được bh
        $ca_nhan_dbh = $this->customers_model->get_canhan_dbh();
        if (!empty($ca_nhan_dbh)) {
//            echo '<pre>';
//            print_r($ca_nhan_dbh);
//            die;
            $data['ca_nhan_dbh'] = $ca_nhan_dbh;
        }
        // Lấy toàn bộ kh cá nhân 
//        $data_get = array(
//            'skip_join' => TRUE,
//            'phanloai_kh'=>CANHAN,
//        );
//        $ca_nhan = $this->customers_model->get_customers($data_get);
//        if(!empty($ca_nhan)){
//            $data['ca_nhan'] = $ca_nhan;
//        }       
        //end
        //======================================================================
        $data['scripts'] = $this->scripts();
        $this->_view_data['main_content'] = $this->load->view($tpl, $data, TRUE);
        $this->_view_data['title'] = 'Báo cáo' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function scripts() {
        $scripts = '<script type="text/javascript">
                    $(".js-example-disabled-results").select2();
                    </script>';
        return $scripts;
    }

    //==========================================================================

    function browse($para1 = DEFAULT_LANGUAGE, $para2 = 1) {
        $options = array('lang' => $para1, 'page' => $para2);
        $options = array_merge($options, $this->_get_data_from_filter());
        $this->phpsession->save('customers_lang', $options['lang']);
        $total_row = $this->customers_model->get_kh($options);
        $total_pages = (int) ($total_row / FAQ_ADMIN_POST_PER_PAGE);
        if ($total_pages * FAQ_ADMIN_POST_PER_PAGE < $total_row)
            $total_pages++;
        if ((int) $options['page'] > $total_pages)
            $options['page'] = $total_pages;
        $options['offset'] = $options['page'] <= DEFAULT_PAGE ? DEFAULT_OFFSET : ((int) $options['page'] - 1) * FAQ_ADMIN_POST_PER_PAGE;
        $options['limit'] = FAQ_ADMIN_POST_PER_PAGE;

        $config = prepare_pagination(
                array(
                    'total_rows' => $total_row,
                    'per_page' => $options['limit'],
                    'offset' => $options['offset'],
                    'js_function' => 'change_page_admin'
                )
        );
        $this->pagination->initialize($config);

        $options['customerss'] = $this->customers_model->get_customers($options);
        $options['post_uri'] = 'customers_admin';
        $options['total_rows'] = $total_row;
        $options['total_pages'] = $total_pages;
        $options['page_links'] = $this->pagination->create_ajax_links();

        if ($options['lang'] <> DEFAULT_LANGUAGE) {
            $options['uri'] = CUSTOM_ADMIN_BASE_URL . '/' . $options['lang'];
        } else {
            $options['uri'] = CUSTOM_ADMIN_BASE_URL;
        }

        if (isset($options['error']) || isset($options['succeed']) || isset($options['warning']))
            $options['options'] = $options;

        $this->_view_data['main_content'] = $this->load->view('admin/customers_list', $options, TRUE);
        $this->_view_data['title'] = 'Quản lý khách hàng' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _get_data_from_filter() {
        $options = array();

        if ($this->is_postback()) {
            $options['search'] = $this->db->escape_str($this->input->post('search', TRUE));
            $this->phpsession->save('customers_search_options', $options);
        } else {
            $temp_options = $this->phpsession->get('customers_search_options');
            if (is_array($temp_options)) {
                $options['search'] = $temp_options['search'];
            } else {
                $options['search'] = '';
            }
        }
//        $options['offset'] = $this->uri->segment(3);
        return $options;
    }

    //==========================================================================
    private function _get_posted_customers_data() {

        $post_data = array(
//            'fullname' => my_trim($this->input->post('fullname', TRUE)),
//            'cmt' => my_trim($this->input->post('cmt', TRUE)),
//            'DOB' => my_trim($this->input->post('DOB', TRUE)),
//            'phone' => my_trim($this->input->post('phone', TRUE)),
//            'email' => my_trim($this->input->post('email', TRUE)),
//            'address' => my_trim($this->input->post('address', TRUE)),

            'phanloai_kh' => my_trim($this->input->post('phanloai_kh', TRUE)),
            //'phanloai_bh' => my_trim($this->input->post('phanloai_bh', TRUE)),
            //'hopdong_baohiem' => my_trim($this->input->post('hopdong_baohiem', TRUE)),
            'congty_canhan' => my_trim($this->input->post('congty_canhan', TRUE)),
            'parent_id' => my_trim($this->input->post('parent_id', TRUE)),
            //'DOB' => my_trim($this->input->post('ngaysinh', TRUE)),
            //'congtycon_chinhanh' => my_trim($this->input->post('congtycon_chinhanh', TRUE)),
            'phongban' => my_trim($this->input->post('phongban', TRUE)),
            'diachi' => my_trim($this->input->post('diachi', TRUE)),
            'mst_cmt' => my_trim($this->input->post('mst_cmt', TRUE)),
            'ngaycap' => my_trim($this->input->post('ngaycap', TRUE)),
            'noicap' => my_trim($this->input->post('noicap', TRUE)),
            'email' => my_trim($this->input->post('email', TRUE)),
            'dienthoai' => my_trim($this->input->post('dienthoai', TRUE)),
            'didong' => my_trim($this->input->post('didong', TRUE)),
            'phanloai_kh1' => my_trim($this->input->post('phanloai_kh1', TRUE)),
            'gioitinh' => my_trim($this->input->post('gioitinh', TRUE)),
                //
//            'ndbh_phanloai_kh' => my_trim($this->input->post('ndbh_phanloai_kh', TRUE)),
//            'ndbh_hopdong_baohiem' => my_trim($this->input->post('ndbh_hopdong_baohiem', TRUE)),
//            'ndbh_congty_canhan' => my_trim($this->input->post('ndbh_congty_canhan', TRUE)),
//            'ndbh_congtycon_chinhanh' => my_trim($this->input->post('ndbh_congtycon_chinhanh', TRUE)),
//            'ndbh_phongban' => my_trim($this->input->post('ndbh_phongban', TRUE)),
//            'ndbh_diachi' => my_trim($this->input->post('ndbh_diachi', TRUE)),
//            'ndbh_mst_cmt' => my_trim($this->input->post('ndbh_mst_cmt', TRUE)),
//            'ndbh_ngaycap' => my_trim($this->input->post('ndbh_ngaycap', TRUE)),
//            'ndbh_noicap' => my_trim($this->input->post('ndbh_noicap', TRUE)),
//            'ndbh_email' => my_trim($this->input->post('ndbh_email', TRUE)),
//            'ndbh_dienthoai' => my_trim($this->input->post('ndbh_dienthoai', TRUE)),
//            'ndbh_didong' => my_trim($this->input->post('ndbh_didong', TRUE)),
//            'ndbh_phanloai_kh1' => my_trim($this->input->post('ndbh_phanloai_kh1', TRUE)),
//            'ndbh_quanhe' => my_trim($this->input->post('ndbh_quanhe', TRUE)),
        );

        if ($this->phpsession->get('roles_publisher') == STATUS_ACTIVE) {
            $post_data['status'] = STATUS_ACTIVE;
        } else {
            $post_data['status'] = STATUS_INACTIVE;
        }
        $created_date = datetimepicker_array2($this->input->post('created_date', TRUE));
        $post_data['created_date'] = date('Y-m-d H:i:s', mktime($created_date['hour'], $created_date['minute'], $created_date['second'], $created_date['month'], $created_date['day'], $created_date['year']));
        $post_data['updated_date'] = date('Y-m-d H:i:s');
        $post_data['editor'] = $this->phpsession->get('user_id');

        $ngaycap = datetimepicker_array2($this->input->post('ngaycap', TRUE));
        //$ndbh_ngaycap = datetimepicker_array2($this->input->post('ndbh_ngaycap', TRUE));
        $ngaysinh = datetimepicker_array2($this->input->post('ngaysinh', TRUE));
        $post_data['ngaycap'] = date('Y-m-d H:i:s', mktime($ngaycap['hour'], $ngaycap['minute'], $ngaycap['second'], $ngaycap['month'], $ngaycap['day'], $ngaycap['year']));
        //$post_data['ndbh_ngaycap'] = date('Y-m-d H:i:s', mktime($ndbh_ngaycap['hour'], $ndbh_ngaycap['minute'], $ndbh_ngaycap['second'], $ndbh_ngaycap['month'], $ndbh_ngaycap['day'], $ndbh_ngaycap['year']));
        $post_data['ngaysinh'] = date('Y-m-d H:i:s', mktime($ngaysinh['hour'], $ngaysinh['minute'], $ngaysinh['second'], $ngaysinh['month'], $ngaysinh['day'], $ngaysinh['year']));

        return $post_data;
    }

    //Thêm kh cn
    //==========================================================================
    function add_kh_cn() {

        $options = $data = array();
        if ($this->is_postback()) {
            if (!$this->_do_add_customers())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        //======================================================================
        // Lấy kh dn
        $data_get = array(
            'phanloai_kh' => DOANHNGHIEP,
        );
        $kh_dn = $this->customers_model->get_kh($data_get);
        if (!empty($kh_dn)) {
            $options['kh_dn'] = $kh_dn;
        }
        //end
        //======================================================================
        // Lấy tỉnh thành
        $data_get = array();
        $cities = $this->customers_model->CommonGet(products_state);
        if (!empty($cities)) {
            $options['a_cities'] = $cities;
        }
        //======================================================================
        // Lấy các phòng ban
        $data_get = array();
        $phongban = $this->products_color_model->get_products_color($data_get);
        if (!empty($phongban)) {
            $options['a_phongban'] = $phongban;
        }
        //end
        //======================================================================
        // Lấy ngân hàng
        $data_get = array();
        $nh = $this->customers_model->CommonGet(TBL_NGANHANG, $data_get);
        if (!empty($nh)) {
            $options['a_nh'] = $nh;
        }
        //======================================================================
        // Chuan bi du lieu chinh de hien thi        
        $options['header'] = 'Thêm khách hàng cá nhân';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = site_url(URL_ADD_KH_CN);
        $options['scripts'] = $this->scripts_select2();
        //======================================================================
        $this->_view_data['main_content'] = $this->load->view('ad/kh/ad_form_kh_cn', $options, TRUE);
        $this->_view_data['title'] = 'Thêm khách hàng cá nhân';
        $this->load->view($this->_layout, $this->_view_data);
    }

    //end
    //==========================================================================
    function get_data_kh_pb($options = array()) {
        $data = array();
        // Lấy kh dn
        //$customers_b = $this->customers_model->get_customers(array('phanloai_kh' => DOANHNGHIEP, 'skip_id' => $id));
        $data_get = array(
            'phanloai_kh' => DOANHNGHIEP,
            'skip_id' => $id,
        );
        $kh_dn = $this->customers_model->get_kh($data_get);
        if (!empty($kh_dn)) {
            $options['kh_dn'] = $kh_dn;
        }
        //end
        // Lấy các phòng ban
        $data_get = array();
        $phongban = $this->products_color_model->get_products_color($data_get);
        if (!empty($phongban)) {
            $options['a_phongban'] = $phongban;
        }
        //end
        return $data;
    }

    //==========================================================================
    //Thêm kh cn
    function add_kh_dn() {
        $options = $data = array();

        if ($this->is_postback()) {
            if (!$this->_do_add_customers())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        //======================================================================
        // Lấy kh dn
        $data_get = array(
            'phanloai_kh' => DOANHNGHIEP,
        );
        $kh_dn = $this->customers_model->get_kh($data_get);
        if (!empty($kh_dn)) {
            $options['kh_dn'] = $kh_dn;
        }
        //end
        //======================================================================
        // Lấy tỉnh thành
        $data_get = array();
        $cities = $this->customers_model->CommonGet(products_state);
        if (!empty($cities)) {
            $options['a_cities'] = $cities;
        }
        //======================================================================
        // Chuan bi du lieu chinh de hien thi
        $options['scripts'] = $this->_get_scripts();
        $options['header'] = 'Thêm khách hàng';
        $options['button_name'] = 'Lưu dữ liệu';
        $options['submit_uri'] = site_url(URL_ADD_KH_DN);
        //======================================================================
        $options['scripts'] = $this->scripts();
        $this->_view_data['main_content'] = $this->load->view('ad/kh/ad_form_kh_dn', $options, TRUE);
        $this->_view_data['title'] = 'Thêm khách hàng doanh nghiệp';
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    //==========================================================================
    private function _do_add_customers($uri = 0) {

        $this->form_validation->set_rules('phanloai_kh', 'Phân loại khách hàng', 'trim|required|xss_clean|max_length[255]');
        $this->form_validation->set_rules('cities', 'Tỉnh thành', 'trim|required|xss_clean|max_length[255]');

        $sothe_bh = $this->input->post('sothe_bh', TRUE);
        if ($sothe_bh != '') {
            $this->form_validation->set_rules('sothe_bh', 'Số thẻ bảo hiểm', 'trim|xss_clean|max_length[255]|callback_check_sothe_bh');
        }

        $phanloai_kh = $this->input->post('phanloai_kh');
        if ($phanloai_kh == CANHAN) {
            $this->form_validation->set_rules('code', 'Mã khách hàng', 'trim|xss_clean');
            $this->form_validation->set_rules('congty_canhan', 'Họ và tên', 'trim|required|xss_clean');
            $this->form_validation->set_rules('ngaysinh', 'Ngày sinh', 'trim|required|xss_clean');
            $this->form_validation->set_rules('parent_id', 'Trực thuộc công ty', 'trim|xss_clean');
            $this->form_validation->set_rules('phongban', 'Phòng ban', 'trim|xss_clean');
            $this->form_validation->set_rules('diachi', 'Địa chỉ', 'trim|required|xss_clean');
            $this->form_validation->set_rules('mst_cmt', 'CMTND - Hộ Chiếu', 'trim|required|callback_check_mst_cmt');
            //$this->form_validation->set_rules('mst_cmt', 'CMTND - Hộ Chiếu', 'trim');
            $this->form_validation->set_rules('ngaycap', 'Ngày cấp', 'trim|xss_clean');
            $this->form_validation->set_rules('noicap', 'Nơi cấp', 'trim|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|valid_email');
            $this->form_validation->set_rules('dienthoai', 'Điện thoại', 'trim|xss_clean');
            $this->form_validation->set_rules('didong', 'Di động', 'trim|xss_clean');
            $this->form_validation->set_rules('phanloai_kh1', 'Phân loại khách hàng', 'trim|required|xss_clean');
            $this->form_validation->set_rules('gioitinh', 'Giới tính', 'trim|xss_clean');
            //------------------------------------------------------------------
            $this->form_validation->set_rules('tk_fullname', 'Tên chủ tài khoản', 'trim|xss_clean');
            $this->form_validation->set_rules('tk_bank', 'Ngân hàng', 'trim|xss_clean');
            $this->form_validation->set_rules('tk_stk', 'Số tài khoản', 'trim|xss_clean');
            $this->form_validation->set_rules('tk_ghichu', 'Ghi chú tài khoản người thụ hưởng', 'trim|xss_clean');
            //------------------------------------------------------------------
        } else {
            $this->form_validation->set_rules('congty_canhan', 'Tên Công ty / Chi nhánh', 'trim|required|xss_clean');
            $this->form_validation->set_rules('parent_id', 'Trực thuộc công ty', 'trim|xss_clean');
            $this->form_validation->set_rules('diachi', 'Địa chỉ', 'trim|required|xss_clean');
            $this->form_validation->set_rules('mst_cmt', 'ĐKKD - MST', 'trim|required|callback_check_mst_cmt');
            $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|valid_email');
            $this->form_validation->set_rules('dienthoai', 'Điện thoại', 'trim|xss_clean');
            $this->form_validation->set_rules('didong', 'Di động', 'trim|xss_clean');
            $this->form_validation->set_rules('phanloai_kh1', 'Phân loại khách hàng', 'trim|required|xss_clean');
        }
        //======================================================================
        $datetime_now = date('Y-m-d H:i:s');
        $date_now = date('Y-m-d');
        $time_now = strtotime($date_now);
        //======================================================================
        if ($this->form_validation->run($this)) {

            $post_data = array(
                'phanloai_kh' => my_trim($this->input->post('phanloai_kh', TRUE)),
                'congty_canhan' => my_trim($this->input->post('congty_canhan', TRUE)),
                'code' => my_trim($this->input->post('code', TRUE)),
                'sothe_bh' => my_trim($this->input->post('sothe_bh', TRUE)),
                'parent_id' => my_trim($this->input->post('parent_id', TRUE)),
                'ngaysinh' => my_trim($this->input->post('ngaysinh', TRUE)),
                'phongban' => my_trim($this->input->post('phongban', TRUE)),
                'diachi' => my_trim($this->input->post('diachi', TRUE)),
                'city_id' => my_trim($this->input->post('cities', TRUE)),
                'gioitinh' => my_trim($this->input->post('gioitinh', TRUE)),
                'mst_cmt' => my_trim($this->input->post('mst_cmt', TRUE)),
                'ngaycap' => my_trim($this->input->post('ngaycap', TRUE)),
                'noicap' => my_trim($this->input->post('noicap', TRUE)),
                'email' => my_trim($this->input->post('email', TRUE)),
                'dienthoai' => my_trim($this->input->post('dienthoai', TRUE)),
                'didong' => my_trim($this->input->post('didong', TRUE)),
                'phanloai_kh1' => my_trim($this->input->post('phanloai_kh1', TRUE)),
                'ghichu' => my_trim($this->input->post('ghichu', TRUE)),
                //--------------------------------------------------------------
                'tk_fullname' => my_trim($this->input->post('tk_fullname', TRUE)),
                'tk_bank' => my_trim($this->input->post('tk_bank', TRUE)),
                'tk_stk' => my_trim($this->input->post('tk_stk', TRUE)),
                'tk_ghichu' => my_trim($this->input->post('tk_ghichu', TRUE)),
                //--------------------------------------------------------------
                'editor' => $this->phpsession->get('user_id'),
                'date_updated' => $datetime_now,
                'time_updated' => $time_now,
            );
            $id = $this->input->post('id', TRUE);
            //==================================================================
            // Update data
            if ($id > 0) {
                $post_data['id'] = $id;
//                echo '<pre>';
//                print_r($post_data);
//                die;
                $this->customers_model->update($post_data);
                redirect(URL_AD_KH);
            } else {
                // Creat data
                $post_data['creator'] = $this->phpsession->get('user_id');
                $post_data['status'] = ACTIVE;
                $post_data['date_created'] = $datetime_now;
                $post_data['time_created'] = $time_now;

                $insert_id = $this->customers_model->insert($post_data);
                redirect(URL_AD_KH);
            }
        }
        return FALSE;
    }

    //end
    //==========================================================================
    public function check_mst_cmt($str) {
        $id = $this->input->post('id');
        $phanloai_kh = $this->input->post('phanloai_kh');
//        echo $phanloai_kh;
//        die;
        if ($phanloai_kh == CANHAN) {
            $msg = '<strong>CMTND - Hộ Chiếu</strong> đã tồn tại';
        } else {
            $msg = '<strong>ĐKKD - MST</strong> đã tồn tại';
        }
        //======================================================================
        if ($str != '') {
            // Lấy thông tin kh
            $data_get = array(
                'mst_cmt' => $str,
            );
            // Khi sửa
            if ($id != '' && $id > 0) {
                $data_get['skip_id'] = $id;
            }
            //end
            //==================================================================
            $count_kh = $this->customers_model->count_kh($data_get);
            if ($count_kh >= 1) {
                $this->form_validation->set_message('check_mst_cmt', $msg);
                return FALSE;
            } else {
                return TRUE;
            }
        }
        //$this->form_validation->set_message('check_mst_cmt', '<strong>CMTND - Hộ Chiếu</strong> đã tồn tại');
        return FALSE;
    }

    //end
    //==========================================================================
    public function check_sothe_bh($str) {
        $id = $this->input->post('id');
        $msg = '<strong>Số thẻ Bảo Hiểm</strong> đã tồn tại';
        //======================================================================
        if ($str != '') {
            // Lấy thông tin kh
            $data_get = array(
                'where' => array(
                    'sothe_bh' => $str,
                ),
            );
            // Khi sửa
            if ($id != '' && $id > 0) {
                $data_get['skip_id'] = $id;
            }
            //end
            //==================================================================
            $kh = $this->customers_model->CommonGet(TBL_CUSTOMERS, $data_get);

            if (count($kh) >= 1) {
                $this->form_validation->set_message('check_sothe_bh', $msg);
                return FALSE;
            } else {
                return TRUE;
            }
        }
        return FALSE;
    }

    //end
    //==========================================================================
    public function check_email($str) {
        return TRUE;
        $id = $this->input->post('id');
        if ($str != '') {
            // Lấy thông tin kh
            $data_get = array(
                'email' => $str,
            );
            // Khi sửa
            if ($id != '' && $id > 0) {
                $data_get['skip_id'] = $id;
            }
            //end
            //==================================================================
            $count_kh = $this->customers_model->count_kh($data_get);

            if ($count_kh >= 1) {
                $this->form_validation->set_message('check_email', '<strong>Email</strong> đã tồn tại');
                return FALSE;
            } else {
                return TRUE;
            }
        }
        $this->form_validation->set_message('check_email', '<strong>Email</strong> đã tồn tại');
        return FALSE;
    }

    //==========================================================================
    // Sửa thông tin kh
    function edit() {
        $options = array();

        if (!$this->is_postback()) {
            redirect(URL_AD_KH);
        }
        //======================================================================
        if ($this->is_postback() && !$this->input->post('from_list')) {

            if (!$this->_do_add_customers())
                $options['error'] = validation_errors();
            if (isset($options['error']))
                $options['options'] = $options;
        }
        //======================================================================
        $id = $this->input->post('id');
        //======================================================================
        $data_get = array(
            'id' => $id,
        );
        // Get customers
        $kh = $this->customers_model->get_kh($data_get);
        if (is_object($kh)) {
            $options['kh'] = $kh;
            $phanloai_kh = $kh->phanloai_kh;
        }
        //======================================================================
        $options['header'] = 'Xem chi tiết khách hàng';
        $options['button_name'] = 'Sửa Khách hàng';
        $options['submit_uri'] = CUSTOM_ADMIN_BASE_URL . '/edit';
        // Lấy kh dn
        //$customers_b = $this->customers_model->get_customers(array('phanloai_kh' => DOANHNGHIEP, 'skip_id' => $id));
        $data_get = array(
            'phanloai_kh' => DOANHNGHIEP,
            'skip_id' => $id,
        );
        $kh_dn = $this->customers_model->get_kh($data_get);
        if (!empty($kh_dn)) {
            $options['kh_dn'] = $kh_dn;
        }
        //end
        //======================================================================
        // Lấy các phòng ban
        $data_get = array();
        $phongban = $this->products_color_model->get_products_color($data_get);
        if (!empty($phongban)) {
            $options['a_phongban'] = $phongban;
        }
        //end
        //======================================================================
        if (isset($phanloai_kh) && $phanloai_kh == CANHAN) {
            $tpl = 'ad/kh/ad_form_kh_cn';
            // Lấy ngân hàng
            $data_get = array();
            $nh = $this->customers_model->CommonGet(TBL_NGANHANG, $data_get);
            if (!empty($nh)) {
                $options['a_nh'] = $nh;
            }
            //======================================================================
        } else {
            $tpl = 'ad/kh/ad_form_kh_dn';
        }
        //end
        //======================================================================
        // Lấy tỉnh thành
        $data_get = array();
        $cities = $this->customers_model->CommonGet(products_state);
        if (!empty($cities)) {
            $options['a_cities'] = $cities;
        }
        //======================================================================
        $options['scripts'] = $this->scripts_select2();
        $this->_view_data['main_content'] = $this->load->view($tpl, $options, TRUE);
        $this->_view_data['title'] = 'Xem thông tin của khách' . DEFAULT_TITLE_SUFFIX;
        $this->load->view($this->_layout, $this->_view_data);
    }

    //==========================================================================
    private function _get_edit_form_data() {
        $id = $this->input->post('id');
        // khi vừa vào trang sửa
        if ($this->input->post('from_list')) {
            $customers = $this->customers_model->get_customers(array('id' => $id));
            $id = $customers->id;
            $phone = $customers->phone;
            $DOB = $customers->DOB;
            $address = $customers->address;
            $fullname = $customers->fullname;
            $cmt = $customers->cmt;
            $email = $customers->email;
            $created_date = $customers->created_date;
        }
        // khi submit
        else {
            $id = $id;
            $phone = my_trim($this->input->post('phone', TRUE));
            $DOB = my_trim($this->input->post('DOB', TRUE));
            $address = my_trim($this->input->post('address', TRUE));
            $fullname = my_trim($this->input->post('fullname', TRUE));
            $cmt = my_trim($this->input->post('cmt', TRUE));
            $email = my_trim($this->input->post('email', TRUE));
            $created_date = my_trim($this->input->post('created_date', TRUE));
        }
        $options = array();
        $options['id'] = $id;
        $options['phone'] = $phone;
        $options['DOB'] = $DOB;
        $options['address'] = $address;
        $options['fullname'] = $fullname;
        $options['cmt'] = $cmt;
        $options['email'] = $email;
        $options['created_date'] = $created_date;
        $options['header'] = 'Xem chi tiết khách hàng';
        $options['button_name'] = 'Sửa Khách hàng';
        $options['submit_uri'] = CUSTOM_ADMIN_BASE_URL . '/edit';
        $options['scripts'] = $this->_get_scripts();
        return $options;
    }

    //==========================================================================
    private function _do_edit_customers() {
        $post_data = $this->_get_posted_customers_data();
        $post_data['id'] = $this->input->post('id');

        $this->customers_model->update($post_data);
        $lang = $this->phpsession->get('customers_lang');
        redirect(CUSTOM_ADMIN_BASE_URL . '/' . $lang);
    }

    //==========================================================================
    public function delete() {
        $options = array();
        if ($this->is_postback()) {
            $id = $this->input->post('id');
            $this->customers_model->delete($id);
            $options['warning'] = 'Đã xóa thành công';
        }
        $lang = $this->phpsession->get('customers_lang');
        redirect(CUSTOM_ADMIN_BASE_URL . '/' . $lang);
    }

    //==========================================================================
    private function _get_scripts() {
        $scripts = '<script type="text/javascript" src="/plugins/tinymce/tinymce.min.js?v=4.1.7"></script>';
        $scripts .= '<link rel="stylesheet" type="text/css" href="/plugins/fancybox/source/jquery.fancybox.css" media="screen" />';
        $scripts .= '<script type="text/javascript" src="/plugins/fancybox/source/jquery.fancybox.pack.js"></script>';
        $scripts .= '<script type="text/javascript">$(".iframe-btn").fancybox({"width":900,"height":500,"type":"iframe","autoScale":false});</script>';
        $scripts .= '<style type=text/css>.fancybox-inner {height:500px !important;}</style>';
        $scripts .= '<script type="text/javascript">enable_tiny_mce();</script>';

        return $scripts;
    }

    //==========================================================================
    function change_status() {
        $id = $this->input->post('id');
        $customers = $this->customers_model->get_customers(array('id' => $id));
        $status = $customers->status == STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
        $this->customers_model->update(array('id' => $id, 'status' => $status));
    }

    public function up() {
        $customers_id = $this->input->post('id');
        $this->customers_model->update(array('id' => $customers_id, 'updated_time' => date('Y-m-d H:i:s')));
        $lang = $this->phpsession->get('customers_lang');
        redirect(CUSTOM_ADMIN_BASE_URL . '/' . $lang);
    }

    function export($options = array()) {
        $options = array();
//        if($this->phpsession->get('customer_name_search') != '')
//            $options['keyword'] = $this->phpsession->get('customer_name_search');
//        if(is_array($this->phpsession->get('date_filter'))){
//            $options = array_merge($options, $this->phpsession->get('date_filter'));
//        }
        $options = array_merge($options, $this->_get_data_from_filter());
        $customers = $this->customers_model->get_customers($options);

        if (count($customers) > 0) {

            //load our new PHPExcel library
            $this->load->library('excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Order excel');
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A1', 'STT');
            $this->excel->getActiveSheet()->setCellValue('B1', 'MÃ KHÁCH HÀNG');
            $this->excel->getActiveSheet()->setCellValue('C1', 'HỌ VÀ TÊN');
            $this->excel->getActiveSheet()->setCellValue('D1', 'EMAIL');
            $this->excel->getActiveSheet()->setCellValue('E1', 'ĐIỆN THOẠI');
            $this->excel->getActiveSheet()->setCellValue('F1', 'ĐỊA CHỈ');

            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);

            //set aligment to center for that merged cell (A1 to D1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $stt = 1;
            $row = 3;
            $total_m = 0;

            foreach ($customers as $order):

                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $stt);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $order->id);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $order->fullname);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $order->email);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $order->phone);
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $order->address);

                $stt++;
                $row++;
            endforeach;
            $row++;
            $this->excel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->mergeCells('B' . $row . ':Z' . $row);

            $filename = 'dang_sach_khach_hang.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
        }else {
            //hoa don khong co san pham
            return $this->list_customers(array('error' => 'Mục bạn đã chọn hiện thời không có khách hàng nào!'));
        }
    }

}
