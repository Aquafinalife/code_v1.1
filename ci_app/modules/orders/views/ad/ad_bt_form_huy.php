<div class="page_header">
    <h1 class="fleft"><?php if (isset($header)) echo $header; ?></h1>
    <small class="fleft">"Nội dung bồi thường"</small>
    <span class="fright"><a class="button close" href="<?php echo ORDER_ADMIN_BASE_URL; ?>/" title="Đóng"><em>&nbsp;</em>Đóng</a></span>
    <br class="clear"/>
</div>
<div class="main_content col-sm-12">
    <form action="<?php echo ORDER_ADMIN_BASE_URL . '/huy-bt'; ?>" method="post" accept-charset="utf-8">
        <?php
        //==========================================================================
        if (isset($bt) && is_object($bt)) {
            $id = $bt->id;
            $bt_code = $bt->bt_code;
            $case = $bt->case;
            ?>
            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
            <input type="submit" name="submit" value="Xác nhận Hủy bồi thường" class="btn btn-default">
            <a style="margin-left: 20px;" class="btn btn-primary" href="<?php echo site_url(ORDERS_ADMIN_BASE_URL)?>">Quay lại <span class="glyphicon glyphicon-share-alt"></span></a>
                <?php
            }
            ?>
    </form>
</div>
